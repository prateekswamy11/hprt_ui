//
//  PrintCopiesSelectionProtocol.swift
//  Kodak Smile
//
//  Created by maximess142 on 29/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

protocol PrintCopiesSelectionProtocol
{
    func printCopiesSelected(numberOfCopies : Int)
    func printBtnClickedAfterSelectingCopies()
}
