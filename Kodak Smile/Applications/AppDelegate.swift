//
//  AppDelegate.swift
//  Polaroid MINT
//
//  Created by maximess142 on 16/07/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CoreData
import GoogleSignIn
import Alamofire
import UserNotifications
import Fabric
import Crashlytics
//import Appsee
import CopilotAPIAccess

var AFManager = SessionManager()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PrintResponceDelegate, ResponceDelegate, PrintErrorProtocol {
    var isProgressEnable = false
    var isZIPSNAPConnected = false
    var window: UIWindow?
    let notificationCenter = UNUserNotificationCenter.current()
    var isConnectedToPrinter : Bool = false
    var isGivenToPrint: Bool = false
    var isPrintingError : Bool = false
    var isDisplayPrintDetail = false
    var totalPrintCount = 0
    var arrPrintTypeCode = [PrintTypeCode]()
    var currentPrintProcess : Float = 0.0
    var printTypeCode : PrintTypeCode?
    var OBJ_CUR_PRINT_OPER:RequestOperation!
    var currentAccessory:EAAccessory?
    var accessoryList = NSMutableArray()
    lazy var labelPrintProgress = UILabel()
    var timer: Timer?
    var printCopies:Int = -1
    
    var size_of_upload_SNAP_To_UPA : NSInteger!
    var data_of_upload_SNAP_To_UPA : NSMutableData!
    var time : Float = 0.0
    var isConnectInProgress = false
    var Product_ID:String = ""
    var tmd_version_URL:String = ""
    var cnx_version_URL:String = ""
    var firmware_version_URL:String = ""
    
    var ZIP_Product_ID:String = ""
    var ZIP_firmware_URL:String = ""


    
    // Variables added by Maximess
    var isPrintingInProgress = false
    var totalCountOfPrint : Int = 0
    var numberOfPrintedImages : Int = 0
    var isFirmwareUpdateAvailable = false
    
    var easyARKey = "4RquRRVGh4XpwOytHfipc59IzffA58WH2zwuYJgTsx3RbeHSBjYkfvPOnWTB8O4OYqrR4VGGm0Av9WfN2oVrHxvqe0qN4oXj4NmJdqm5VsHpOWXr9z60QQWNDWimTHpCJ0fyFINGruyEZGJ0RcMzwFXJGzQ0roTRBlZp0ga9y6caTyQLMLmXig6Oors9kEJ7IMgdglgh"


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIViewController.swizzlePresentationStyle()
        UIApplication.shared.statusBarStyle = .lightContent
                Thread.sleep(forTimeInterval: 3.0)
        
        Fabric.with([Crashlytics.self])
//        DropboxClientsManager.setupWithAppKey("7sj2fpgzxfhvok8")
        
//        GIDSignIn.sharedInstance().clientID = "129929426917-5j38lvdgojul9o54bqqlc1fbgm2u50uu.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().clientID = "770157866693-hckvhlj9n6ajaigm5s90sqv47k28vto5.apps.googleusercontent.com"
        
        if UserDefaults.standard.value(forKey: "UUID") == nil {
            UserDefaults.standard.setValue(UIDevice.current.identifierForVendor?.uuidString, forKey: "UUID")
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkStatus), name: NSNotification.Name("UpdateUserData"), object: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        print(dbUrl?.absoluteString as Any)
        
//        Appsee.start(DEV_APPSEE_KEY)
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 36000
        configuration.timeoutIntervalForResource = 36000
        AFManager = Alamofire.SessionManager(configuration: configuration, delegate: SessionDelegate(), serverTrustPolicyManager: nil)
        
        // Local notification for AI
//        notificationCenter.delegate = self
//        
//        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
//        
//        notificationCenter.requestAuthorization(options: options) {
//            (didAllow, error) in
//            if !didAllow {
//                print("User has declined notifications")
//            }
//        }
        
        
       
         //Check AR sdk is initialized or not
//        if !easyar_Engine.initialize(easyARKey){
//            print("EASYAR INITIALIZATION FAILED")
//        }
        
        Copilot.setup(analyticsProviders: [FirebaseAnalyticsEventLogProvider()])
        AppManager.shared.appLaunched()
        
        EADSessionController.shared()?.delegate = self
        PrinterQueueManager.sharedController.errorDelegate = self
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
       
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
     //   easyar_Engine.onPause()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        //        PredictionController().getPredictionFromImageAsset(assetArray: image_For_AI)
        
        // Make the device in always 3 minutes power off status
//        if(appDelegate().isConnectedToPrinter == true && isPrintingInProgress == true)
//        {
//            api_changePowerOffStatus(status: 0)
//        }
//
//        // old code use for printer will on mode in background and foreground of app
//
//        if(appDelegate().isConnectedToPrinter == true && isPrintingInProgress == false)
//        {
//            api_changePowerOffStatus(status: 0)
//        }
//        easyar_Engine.onPause()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
//        if(appDelegate().isConnectedToPrinter == false && isPrintingInProgress == false)
//        {
//            let accessoryList = EAAccessoryManager.shared().connectedAccessories
//
//            // let accessoryList = appDelegate().accessoryList
//            if(accessoryList.count > 0)
//            {
//                for i in 0..<accessoryList.count
//                {
//                    let acessory = accessoryList[i]
//                    //                NSUserDefaults.standardUserDefaults().setObject(accessory, forKey: "lastConnectedAccessory")
//                    let acesLastConnected = UserDefaults.standard.value(forKey: "lastConnectedAccessory") as? String
//                    if(acessory.serialNumber == acesLastConnected)
//                    {
//                        appDelegate().currentAccessory = acessory
//                        Accessory_Manager.shared.setUpSession(accessory: acessory)
//                        appDelegate().isConnectedToPrinter = true
//                        break
//                    }
//
//                }
//            }
//
//        }
//
//        // Make the device in always ON status
//        if(appDelegate().isConnectedToPrinter == true && isPrintingInProgress == true)
//        {
//            api_changePowerOffStatus(status: 0)
//        }
//
//        //old code use for printer will on mode in background and foreground of app
//
//        if(appDelegate().isConnectedToPrinter == true && isPrintingInProgress == false)
//        {
//            api_changePowerOffStatus(status: 0)
//        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  //  easyar_Engine.onResume()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
        UserDefaults.standard.set(false, forKey: "cancelFirmwarePopUP")

    }
    
    // MARK: - PrintResponceDelegate
    
    func receivedPrintingError(errorCode: Int) {
        customLoader.hideActivityIndicator()
        
        let errorHandlerHelper = PrintErrorCodesHandler()
        if currentVC != "InstalationProgressViewController"{
            errorHandlerHelper.currentViewController = self.getCurrentViewController()!
            errorHandlerHelper.showErrorMsgWithCode(errorCode: errorCode)
            
            // Remove the delegate when error is shown, assign it again when next print command is given. This is done because we are constantly hitting GetAccessoryInfo, so it will show the error again and again which is undesired. We require the error to show only when print is given
            PrinterQueueManager.sharedController.errorDelegate = nil
        }else{
            currentVC = ""
        }
       
    }
    
    @objc func image_upload_successfully() {   }
    
    @objc func printing_Started(_ totalCopies: Int) {
       
    }
    
    @objc func checkStatus() {
        PredictionController().checkStatus()
    }
    
    @objc func  getPercentage(){
        // Logic by Maximess
        numberOfPrintedImages = numberOfPrintedImages + 1
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "updatePrintingCount"), object: nil)
        
        //        if totalCountOfPrint == numberOfPrintedImages
        //        {
        //            isPrintingInProgress = false
        //        }
        
    }
    
    @objc func get_PrintingPercentage(_ no_Of_Copy: Int, Percentage: String) {
        
        if(Percentage == "100") {
            
            if let deviceID = AppManager.shared.kodakManager.lastConnectedKodak?.kodakID {
                let consumableUsageAnalyticsEvent = ConsumableUsageAnalyticsEvent(thingId: deviceID, consumableType: AnalyticsValue.consumableUsage.paper.rawValue, screenName: "")
                Copilot.instance.report.log(event: consumableUsageAnalyticsEvent)
            }
        }
      
    }
    
    @objc func updatePrinting()  {
     
    }
    
    @objc func ErrorAndMessageAck_Print(_ Id: String, Msg: String) {
        
        let printFailedAnalyticsEvent = PrintFailedAnalyticsEvent(failureReason: Msg, thingID: AppManager.shared.kodakManager.lastConnectedKodak?.kodakID, errorCode: Id)
        Copilot.instance.report.log(event: printFailedAnalyticsEvent)
        print("ErrorAndMessageAck_Print ", Msg)
        
    }
    
    
    
    
    func updatePrintCount(_ arrPrintTypes: [PrintTypeCode])
    {
    }
    
    func printing_Finish(_ arrayPrintType: [PrintTypeCode], PrintNumberForType: Int, totalCopies: Int)
    {
       
    }
    
    
    
    //MARK: Auto connect
    
    
    
    @objc func getPrintCountResponse(_ PrintCount: String)
    {
    }
    
    @objc func AccessoriesConnect(_ notification: Notification)
    {
    }
    
    
    // MARK: - Core Data stack
    
    func getCurrentViewController() -> UIViewController? {
        
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                
                customLoader.hideActivityIndicator()
                currentController = currentController.presentedViewController
                
            }
            customLoader.hideActivityIndicator()
            return currentController
        }
        customLoader.hideActivityIndicator()
        return nil
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
            fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
