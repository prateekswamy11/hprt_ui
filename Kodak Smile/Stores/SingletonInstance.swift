//
//  SingletonInstance.swift
//  Polaroid MINT
//
//  Created by maximess142 on 16/07/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation

class SingletonInstance: NSObject {

    static let shared = SingletonInstance()
    
    // Create your singleton variables here.
    // e.g.
    // var appColor : UIColor?
    // var user: User?
    
    override init() {
        super.init()
    }
}
