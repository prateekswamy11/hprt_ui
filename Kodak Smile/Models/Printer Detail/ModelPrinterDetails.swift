//
//  ModelPrinterDetails.swift
//  Kodak Smile
//
//  Created by maximess142 on 10/04/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

var modelPrinterDetails = ModelPrinterDetail()

class ModelPrinterDetail: NSObject
{
    var firmwareVersion = String()
    var batteryPercentage = Int()
    var errorCode = Int()
    var errorHexString = String()
    var photosPrinted = Int()
    var autoPowerOffStatus = Int()
    
    func setModelPrinterDetails(_ dictPrinterData: NSMutableDictionary)
    {
        guard let batteryPercentage = dictPrinterData.value(forKey: "Battery") as? Int else {
            return
        }
        let batteryPerStr  =   "\(dictPrinterData.value(forKey: "Battery")!)"
        modelPrinterDetails.batteryPercentage = Int(batteryPerStr)!
        modelPrinterDetails.firmwareVersion = dictPrinterData.value(forKey: "FirmWareVersion") as! String
        modelPrinterDetails.errorCode = dictPrinterData.value(forKey: "ErrorCode") as! Int
        modelPrinterDetails.errorHexString = dictPrinterData.value(forKey: "ErrorHexString") as! String
        modelPrinterDetails.photosPrinted = dictPrinterData.value(forKey: "PhotoPrinted") as! Int
        modelPrinterDetails.autoPowerOffStatus = dictPrinterData.value(forKey: "PowerStatus") as! Int
        //print(modelPrinterDetails)
    }
}
