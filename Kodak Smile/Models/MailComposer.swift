//
//  MailComposer.swift
//  Polaroid MINT
//
//  Created by maximess142 on 10/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class MailComposer: NSObject
{
    func openContactMailer(viewController : UIViewController)
    {
        print("Contact Us In Progress")
        let mailComposeViewController = configuredMailComposeViewController(viewController: viewController)
        if MFMailComposeViewController.canSendMail() {
            viewController.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    //MARK:- functions
    func configuredMailComposeViewController(viewController : UIViewController) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = viewController // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        mailComposerVC.setToRecipients(["kodak@caglobal.com"])
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email".localisedString(), message: "Your device could not send e-mail.  Please check e-mail configuration and try again.".localisedString(), delegate: self, cancelButtonTitle: "OK".localisedString())
        sendMailErrorAlert.show()
    }    
}

extension UIViewController : MFMailComposeViewControllerDelegate
{
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
