//
//  Storyboards.swift
//  Polaroid MINT
//
//  Created by maximess142 on 25/07/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import UIKit

struct storyboards
{
    static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    static let cameraStoryboard = UIStoryboard(name: "Camera", bundle: nil)
    static let imgPreviewStoryboard = UIStoryboard(name: "ImagePreview", bundle: nil)
    static let connectivityStoryboard = UIStoryboard(name: "Connectivity", bundle: nil)
    static let settingsStoryboard = UIStoryboard(name: "Settings", bundle: nil)
    static let onboardingStoryboard = UIStoryboard(name: "Onboarding", bundle: nil)
    static let printerOnBoardingStoryboard = UIStoryboard(name: "PrinterOnBoarding", bundle: nil)
    static let popUpStoryboard = UIStoryboard(name: "PopUps", bundle: nil)
    static let arStoryboard = UIStoryboard(name: "AR", bundle: nil)
    static let collageStoryboard = UIStoryboard(name: "Collage", bundle: nil)
    static let QuickTips = UIStoryboard(name: "QuickTips", bundle: nil)
    static let firmwareUpdateStoryboard = UIStoryboard(name: "FirmwareUpdate", bundle: nil)

    static let splash = UIStoryboard(name: "Splash", bundle: nil)
    /*static let settingsStoryboard = UIStoryboard(name: "Settings", bundle: nil)
    static let editImageStoryboard = UIStoryboard(name: "EditImage", bundle: nil)
    static let firmwareUpdateStoryboard = UIStoryboard(name: "FirmwareUpdate", bundle: nil)
    static let syncStoryboard = UIStoryboard(name: "Sync", bundle: nil)
    
    static let collageStoryboard = UIStoryboard(name: "Collage", bundle: nil)
    static let socialStoryboard = UIStoryboard(name: "Social", bundle: nil)*/
}
