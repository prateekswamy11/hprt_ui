//
//  UIViewController+Extension.swift
//  Polaroid MINT
//
//  Created by maximess142 on 26/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import CopilotAPIAccess

extension UIViewController
{
    func showAlert(title : String?, message : String?)
    {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK".localisedString(), style: .default) { (action) in
            
        }
        
        alertView.addAction(okAction)
        
        self.present(alertView, animated: true, completion: nil)
    }
    
    func makeRootViewController(viewcontroller : UIViewController)
    {
        let nvc:UINavigationController = UINavigationController()
        nvc.setNavigationBarHidden(true, animated: false)
        nvc.viewControllers = [viewcontroller]
        UIApplication.shared.keyWindow?.rootViewController = nvc
    }
    
    func goToHomeController() {
        //Check if onboarding Skipped already then, go to home
        // Else Go to Onboarding Screen.
        if UserDefaults.standard.bool(forKey: "onboardingSkipped")
        {
            DispatchQueue.main.async {
                print("User Information was Saved. Directly starting Home View Controller")
                let mainVC = storyboards.mainStoryboard.instantiateInitialViewController()
                self.navigationController?.present(mainVC! , animated: false, completion: nil)
            }
           
        }
        else
        {
            print("Not skipped. Go to onboardingScreens.")
            let onboardingStartedEvent = OnboardingStartedAnalayticsEvent(flowID: "fromPush", screenName: "onboarding_started")
            Copilot.instance.report.log(event: onboardingStartedEvent)
            UserDefaults.standard.set(true, forKey: "onboarding_started")
            DispatchQueue.main.async {
            let printerOnboardingVC = storyboards.printerOnBoardingStoryboard.instantiateInitialViewController()
            self.navigationController?.present(printerOnboardingVC!, animated: false, completion: nil)
            }
        }
    }
    
    func moveToLegalPage()  {
        if UserDefaults.standard.bool(forKey: "isAcceptTermsOfUse") {
            goToHomeController()
            return
        }else if UserDefaults.standard.bool(forKey: "switchStateKodak"){
            goToHomeController()
            return
        }
        let legalVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
        legalVC.isFrom = "DoItLater"
        self.navigationController?.pushViewController(legalVC, animated: true)
        
    }
}
