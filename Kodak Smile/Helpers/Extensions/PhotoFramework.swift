//
//  PhotoFramework.swift
//  Kodak Smile
//
//  Created by maximess172 on 17/03/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation
import Photos
class getRecentPic{
    
    func checkPhotoLibraryPermission() -> Bool {
           let status = PHPhotoLibrary.authorizationStatus()
           switch status {
           case .authorized:
               //handle authorized status
               print("authorized")
               return true
           case .denied, .restricted :
               //handle denied status
               print("denied")
               return false
           case .notDetermined:
               return false
           }
       }
func queryLastPhoto(resizeTo size: CGSize?, queryCallback: @escaping ((UIImage?) -> Void)) {
    let fetchOptions = PHFetchOptions()
    fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]

    let requestOptions = PHImageRequestOptions()
    requestOptions.isSynchronous = true

    let fetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
    if let asset = fetchResult.firstObject {
        let manager = PHImageManager.default()

        let targetSize = size == nil ? CGSize(width: asset.pixelWidth, height: asset.pixelHeight) : size!

        manager.requestImage(for: asset,
                             targetSize: targetSize,
                             contentMode: .aspectFit,
                             options: requestOptions,
                             resultHandler: { image, info in
                                queryCallback(image)
        })
    }

}
}
