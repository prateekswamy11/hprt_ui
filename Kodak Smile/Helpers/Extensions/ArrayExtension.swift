//
//  ArrayExtension.swift
//  Kodak Smile
//
//  Created by MAXIMESS152 on 02/10/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

extension Array where Element: Hashable {
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}
