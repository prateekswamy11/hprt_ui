//
//  UIPickerViewExtension.swift
//  Kodak Smile
//
//  Created by maximess152 on 24/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation

class RDPickerView: UIPickerView
{
    @IBInspectable var selectorColor: UIColor? = nil
    override func didAddSubview(_ subview: UIView) {
        super.didAddSubview(subview)
        if let color = selectorColor
        {
            if subview.bounds.height <= 1.5
            {
                subview.backgroundColor = color
            }
        }
    }
}
