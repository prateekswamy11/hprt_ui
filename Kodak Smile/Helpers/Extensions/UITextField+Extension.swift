//
//  UITextField+Extension.swift
//  Polaroid MINT
//
//  Created by maximess142 on 16/07/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

// Add padding to textfield
class PaddingTextField: UITextField {
    
    //MARK: - ViewLifeCycle Function -
    override func awakeFromNib() {
        // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
        if shadow == false {
            layer.masksToBounds = true
        }
    }
    let lineView = UIView()
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 0
    
    enum LINE_POSITION {
        case LINE_POSITION_TOP
        case LINE_POSITION_BOTTOM
    }
    
//    @IBInspectable var padding_right: CGFloat {
//        get {
//            print("WARNING no getter for UITextField.padding_right")
//            return 0
//        }
//        set (f) {
//            layer.sublayerTransform = CATransform3DMakeTranslation(-f, 0, 0)
//        }
//    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:bounds.origin.x + paddingLeft, y:bounds.origin.y,width:
            bounds.width - paddingRight ,height: bounds.size.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat)
    {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat)
    {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    func setLineColor(color:UIColor){
        lineView.backgroundColor = color
    }
    func addLineToView(view : UIView, position : LINE_POSITION, color: UIColor, width: Double) {
        
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        view.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutFormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutFormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutFormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        default:
            break
        }
    }
    func isPasswordFieldValidated() -> Bool {
          if !self.text!.isEmpty {
              let uppercase = NSPredicate(format:"SELF MATCHES %@", ".*[A-Z]+.*").evaluate(with: self.text)
              let lowercase = NSPredicate(format:"SELF MATCHES %@", ".*[a-z]+.*").evaluate(with: self.text)
              return (self.text!.count >= minPasswordCharacters) && uppercase && lowercase
          }
          return false
      }
}
