//
//  PrintData.h
//  PolaroidZip
//
//  Created by Lcom32 on 7/15/15.
//  Copyright (c) 2015 Lanet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PrintData : NSObject

- (NSData *)PrintImage:(NSData *)image;

@end
