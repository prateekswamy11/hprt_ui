//
//  CustomDialogClass.swift
//  CollectionViewDemoApp
//
//  Created by LaNet on 9/7/16.
//  Copyright © 2016 LaNet. All rights reserved.
//

import UIKit

@objc protocol DialogDelegate{
    @objc optional func okClick()
    @objc optional func cancelClick()
    @objc optional func remove()
}

class CustomDialogClass: UIViewController {
    
    var delegate:DialogDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //    override var prefersStatusBarHidden: Bool {
    //        return true
    //    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    //MARK:- Shared Intanse Methods
    var sharedInstance:CustomDialogClass {
        struct Singleton {
            static let instance = CustomDialogClass()
        }
        return Singleton.instance
    }
    
    func  displayAlert(strMsg : String,btnOkText : String,btnCancelText: String,type:String )
    {
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        let vwMain = UIView()
        vwMain.frame = CGRect(x:0,y: 0,width: screenWidth,height: screenHeight)
        
        
        let DynamicView=UIView(frame: CGRect(x:(16 * screenHeight )/736,y: (screenWidth * 200)/414,width:(380 * screenWidth)/414, height: (200 * screenHeight )/736))
        DynamicView.layer.cornerRadius = 3
        
        
        let lbTitle = UILabel()
        lbTitle.frame = CGRect(x:10,y: 0,width: DynamicView.frame.width - 20 ,height: (150 * screenHeight)/736)
        lbTitle.text = strMsg
        lbTitle.numberOfLines = 0
        lbTitle.textAlignment = .center
        lbTitle.backgroundColor = UIColor.clear
        lbTitle.font = UIFont(name: "Avenir", size: 18)
        lbTitle.textColor = UIColor(red: 52/255, green: 52/255, blue: 52/255, alpha: 1)
        if type == "two"
        {
            let btnOK = UIButton()
            btnOK.frame = CGRect(x:0,y: ((DynamicView.frame.height) - ((50 * screenWidth)/414) ), width: DynamicView.frame.width/2,height: (50 * screenWidth)/414)
            
            btnOK.backgroundColor = UIColor(red: 52/255, green: 52/255, blue: 52/255, alpha: 1)
            btnOK.setTitle(btnOkText, for: .normal)
            btnOK.addTarget(self, action: #selector(Ok), for: .touchUpInside)
            btnOK.titleLabel?.font = UIFont(name: "Dense", size: 25)
            //        btnOK.vie
            btnOK.titleLabel!.lineBreakMode = .byWordWrapping;
            
            btnOK.titleLabel?.textColor = UIColor(red: 123/255, green: 123/255, blue: 123/255, alpha: 1)
            btnOK.titleLabel!.textAlignment = .center
            btnOK.titleLabel!.numberOfLines = 1;
            btnOK.titleLabel!.adjustsFontSizeToFitWidth = true;
            btnOK.titleLabel!.lineBreakMode = .byClipping;
            btnOK.titleEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
            
            
            
            let btnCancel = UIButton()
            btnCancel.frame = CGRect(x:(btnOK.frame.width - 2 ), y:((DynamicView.frame.height) - ((50 * screenWidth)/414) ), width:(DynamicView.frame.width/2) + 2, height:(50 * screenWidth)/414)
            btnCancel.backgroundColor = UIColor(red: 0/255, green: 110/255, blue: 177/255, alpha: 1)
            
            btnCancel.layer.borderColor = UIColor.black.cgColor
            btnCancel.setTitle(btnCancelText, for: .normal)
            btnCancel.addTarget(self, action: #selector(cancel), for: .touchUpInside)
            btnCancel.titleLabel?.font = UIFont(name: "Dense", size: 25)
            btnCancel.titleLabel?.textColor = UIColor(red: 252/255, green: 250/255, blue: 251/255, alpha: 1)
            btnCancel.titleLabel!.lineBreakMode = .byWordWrapping;
            btnCancel.titleLabel!.textAlignment = .center
            btnCancel.titleLabel!.numberOfLines = 1;
            btnCancel.titleLabel!.adjustsFontSizeToFitWidth = true;
            btnCancel.titleLabel!.lineBreakMode = .byClipping;
            btnCancel.titleEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
            
            
            
            DynamicView.addSubview(btnCancel)
            DynamicView.addSubview(btnOK)
        }
        else
        {
            let btnOK = UIButton()
            btnOK.frame = CGRect(x:0,y: ((DynamicView.frame.height) - ((50 * screenWidth)/414) ), width: DynamicView.frame.width, height: (50 * screenWidth)/414)
            
            btnOK.backgroundColor = UIColor(red: 52/255, green: 52/255, blue: 52/255, alpha: 1)
            
            btnOK.setTitle(btnOkText, for: .normal)
            btnOK.addTarget(self, action: #selector(removeView), for: .touchUpInside)
            btnOK.titleLabel?.font = UIFont(name: "Dense", size: 25)
            btnOK.titleLabel?.textColor = UIColor(red: 123/255, green: 123/255, blue: 123/255, alpha: 1)
            DynamicView.addSubview(btnOK)
        }
        DynamicView.backgroundColor=UIColor.white
        vwMain.backgroundColor = UIColor(displayP3Red: 50/255, green: 50/255, blue: 50/255, alpha: 0.70)//UIColor.init(colorLiteralRed: 50/255, green: 50/255, blue: 50/255, alpha: 0.70)
        DynamicView.addSubview(lbTitle)
        vwMain.addSubview(DynamicView)
        // self.view.backgroundColor = UIColor.orangeColor()
        self.view.addSubview(vwMain)
        vwMain.tag = 456321
        appDelegate().window?.addSubview(vwMain)
        
        
    }
    
    @objc func removeView()
    {
        appDelegate().window?.viewWithTag(456321)?.removeFromSuperview()
    }
    @objc func cancel()
    {
        delegate?.cancelClick!()
        appDelegate().window?.viewWithTag(456321)?.removeFromSuperview()
        //  delegate?.cancelClick!()
        
    }
    
    @objc func Ok()
    {
        delegate?.okClick!()
        appDelegate().window?.viewWithTag(456321)?.removeFromSuperview()
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
