//
//  RequestOperation.swift
//  Kodak Smile
//
//  Created by maximess152 on 22/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation

class RequestOperation: NSObject
{
    var imgNsdata : NSData?
    var img : Data?
    var type : RequestType
    var no_Of_Copy : Int?
    var DeviceName : String?
    var ResponseDelegate:ResponceDelegate
    var PrintResponseDelegate:PrintResponceDelegate?
    var oldStickerId : String?
    var newStickerId : String?
    var imgSticketDict = [String]()
    var oldBorderId : String?
    var newBorderId : String?
    var imgBorderDict = [String]()
    var UpdateVersionCode : String?
    var arrayPrintType : [PrintTypeCode]?
    var TagNumber : Int?
    var totalCopies : Int?
    var TotalNumberOfCopiesAddes : Int?
    
    init(type : RequestType,ResponseDelegate:ResponceDelegate)
    {
        self.ResponseDelegate = ResponseDelegate
        self.type = type
    }
    
    init(type : RequestType,ResponseDelegate:ResponceDelegate , PrintResponseDelegate:PrintResponceDelegate) {
        self.ResponseDelegate = ResponseDelegate
        self.type = type
        self.PrintResponseDelegate = PrintResponseDelegate
    }
}

protocol PrintResponceDelegate{
    func printing_Finish(_ arrayPrintType : [PrintTypeCode] , PrintNumberForType : Int , totalCopies:Int)
    func printing_Started(_ totalCopies : Int)
    func get_PrintingPercentage(_ no_Of_Copy:Int,Percentage:String)
    func ErrorAndMessageAck_Print(_ Id:String , Msg : String)
}

@objc protocol ResponceDelegate{
    @objc optional func getBetteryLavelResponse(_ Level:String)
    @objc optional func sendDeviceNameSaved()
    @objc optional func image_upload_successfully()
    @objc optional func GetStickerResponse(_ Ids:[String])
    @objc optional func AddStickerSuceess()
    @objc optional func GetBorderResponse(_ Ids:[String])
    @objc optional func AddBorderSuceess()
    @objc optional func getPrintCountResponse(_ PrintCount:String)
    @objc optional func GetVersionResponse(_ Firmware:String ,Conexan:String,  TMD: String)
    @objc optional func Update_Version_Suceess()
    @objc optional func ErrorAndMessageAck(_ Id:String , Msg : String)
    @objc optional func GetPageTypeResponse(_ Type:String)
    @objc optional func getAutoPowerOffResponse(_ Type:String)
    @objc optional func getZIPVersionResponse(_ Ids:[String])
    @objc optional func firmwareUpdateFinished()
    @objc optional func ZIP_accessory_info(_ dict:NSMutableDictionary)
}

enum PrintTypeCode {
    case mossaicEffect_9
    case mossaicEffect_4
    case blurEffect
    case secretView
    case simple
    case burstImage
    case photoBooth
}

enum RequestType {
    case getBetteryLevel
    case sendDeviceName
    case printImage
    case getSticker
    case addSticker
    case getBorder
    case addBorder
    case printCount
    case getVersion
    case updateVersion
    case getZIPInfo
    case getPageType
    case getPowerOffStatus
    case updateZIPFirmware
    case get_ZIP_INFO
    case get_ZIP_FIRMWARE
}

enum StickrType:String{
    case Type_160_X_160 = "00"
    case Type_160_X_160_45 = "01"
    case Type_1024_X_1024 = "02"
    case Type_1024_X_1024_45 = "03"
}

enum BorderType:String{
    case Type_320_X_240 = "00"
    case Type_1200_X_900 = "01"
}

