//
//  Accessory_Manager.swift
//  SnapTouchPrinter
//
//  Created by peerbits_11 on 22/04/17.
//  Copyright © 2017 Rajumar MacMini13. All rights reserved.
//

import UIKit
import ExternalAccessory

protocol AccessoryDelegate{
    //  func accessorysUpdated()
    func selectedDeviceForPrint(selectedDevice: EAAccessory)
}

enum Accessory_Status {
    case Available
    case Busy
}

extension EAAccessory
{
    var status: Accessory_Status? {
        
        get {
            print("eccessory status=\(String(describing: self.status))")
            return self.status
        }
        set {
        }
    }
}

class Accessory_Manager: NSObject{
    static let shared = Accessory_Manager()
    
    var arrayDeviceList = [EAAccessory]()
    var selectedAccessory : EAAccessory?
    var eaSessionController = EADSessionController.shared()
    var supportedProtocolsStrings = Array<Any>()
    
    var delegate : AccessoryDelegate?
    var manager = EAAccessoryManager.shared()
    
    override init() {
        
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(Accessory_Manager._accessoryDidConnect(notification:)), name: NSNotification.Name.EAAccessoryDidConnect, object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(Accessory_Manager._accessoryDidDisconnect(notification:)), name: NSNotification.Name.EAAccessoryDidDisconnect, object: nil)
        
        
        EAAccessoryManager.shared().registerForLocalNotifications()
        
        arrayDeviceList = EAAccessoryManager.shared().connectedAccessories
        
        let mainBundle = Bundle.main
        supportedProtocolsStrings = mainBundle.object(forInfoDictionaryKey: "UISupportedExternalAccessoryProtocols") as! Array<Any>
        // self.scanForUnpairedDevices()
        
        
        
    }
    
    
    
    @objc func _accessoryDidConnect(notification : Notification) {
        
        let connectedAccessory = notification.userInfo?[EAAccessoryKey]
        //appDelegate().Accessory_Firmwareupdate=connectedAccessory as? EAAccessory
        arrayDeviceList.append(connectedAccessory as! EAAccessory)
        print(arrayDeviceList)
        //appDelegate().isupdateFirmwarerequired=false
        // appDelegate().isZIPSNAPConnected = true
        
    }
    
    
    func getDevices(){
        self.arrayDeviceList = EAAccessoryManager.shared().connectedAccessories
    }
    @objc func _accessoryDidDisconnect(notification : Notification) {
        
        let disconnectedAccessory = notification.userInfo?[EAAccessoryKey] as! EAAccessory
        if (selectedAccessory != nil) && disconnectedAccessory.connectionID == selectedAccessory?.connectionID {
            selectedAccessory = nil;
        }
        
        var disconnectedAccessoryIndex = 0
        
        for accessory in arrayDeviceList {
            if disconnectedAccessory.connectionID == accessory.connectionID {
                break
            }
            disconnectedAccessoryIndex += 1
        }
        
        if disconnectedAccessoryIndex < arrayDeviceList.count {
            arrayDeviceList.remove(at: disconnectedAccessoryIndex)
            print(arrayDeviceList)
        } else {
            print("could not find disconnected accessory in accessory list")
        }
       // appDelegate().isupdateFirmwarerequired=true
        //appDelegate().Accessory_Firmwareupdate=nil
        //        if delegate != nil {
        //            delegate?.accessorysUpdated()
        //        }
    }
    
    
    func setUpSession(accessory : EAAccessory) {
        
        var index = 0
        
        //EADSessionController.shared()?.closeSession()
        eaSessionController?.closeSession()
        
        selectedAccessory = accessory
        let protocolStrings = selectedAccessory?.protocolStrings
        
        for _string in protocolStrings! {
            
            if (_string != "")
            {
                eaSessionController?.setupController(for: selectedAccessory, withProtocolString: selectedAccessory?.protocolStrings[index])
                
                eaSessionController?.openSession()
                
                break
            }
            
            index += 1
        }
        
    }
    
}

