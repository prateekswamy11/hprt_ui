//com.polaroid.mobileprinter
//com.NewSnaptouch.app
import Foundation
import UIKit
import CopilotAPIAccess

var newFileData : String = "gjg"
@objc class MySwiftClassName: NSObject {
    @objc var mySwiftVar = newFileData
}

///convert data to bytes array
extension Data {
    init<T>(fromArray values: [T]) {
        var values = values
        self.init(buffer: UnsafeBufferPointer(start: &values, count: values.count))
    }
    
    func toArray<T>(type: T.Type) -> [T] {
        return self.withUnsafeBytes {
            [T](UnsafeBufferPointer(start: $0, count: self.count/MemoryLayout<T>.stride))
        }
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

class PrinterQueueManager: NSObject{
    static let sharedController = PrinterQueueManager()
    var stickertKey : Int!
    var BorderKey : Int!
    var timer : Timer!
    var Zip_Timer : Timer!
    var upload_zips_file_data = [String]()
    static var CurrentRequest : RequestOperation!
    static var RequestArray = [RequestOperation](){
        didSet{
            PrinterQueueManager.sharedController.send_request()
        }
    }
    var obj_RequestOperation: RequestOperation!
    //    weak var errorDelegate: PrintErrorProtocol?
    var errorDelegate: PrintErrorProtocol?
    var Is_Busy = false{
        didSet{
            if self.Is_Busy == false{
                //EADSessionController.sharedController().delegate = nil
                if PrinterQueueManager.RequestArray.count != 0
                {
                    PrinterQueueManager.RequestArray.removeFirst()
                    self.send_request()
                    //PrinterQueueManager.RequestArray.removeFirst()
                }
            }
        }
    }
    
    override init() {
        super.init()
        timer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(PrinterQueueManager.Timer_ticker), userInfo: nil, repeats: true)
    }
    
    func getErroFromPrinter(message: String, errorCode: Int) {
        errorDelegate?.receivedPrintingError(errorCode: errorCode)
        Copilot.instance.report.log(event:
            ThingConnectionFailedAnalyticsEvent(failureReason: message))
    }
    
    @objc func readDataResponse(_ buf : NSMutableArray) {
        print("Count of request -->",PrinterQueueManager.RequestArray.count)
        EADSessionController.shared().delegate = self
        print("READ RESPONSE ____________>>>>>>>>>>>", buf[0],buf[1],buf[2],buf[3],buf[4],buf[5],buf[6],buf[7],buf[8],buf[9])
        //print("Camera version: \(appDelegate().Product_ID)")
        buf[5] = buf[33]
        let PrintTimer = 120
        let ZIP_PrintTimer  = 5
        //Print Ready StartofSendAck
        //ZIP-SNAP
        // Start of ack when upgrade ready finished
        if (buf[4] as! String == "0x01" && buf[5] as! String == "0x00" && buf[6] as! String == "0x02" && buf[7] as! String == "0x00" && (buf[8] as! String == "0x02" || buf[8] as! String == "0x01") && appDelegate().isConnectedToPrinter == true)
        {
            var filedata = Data()
            // change by akshay
            let fileFirmware: String = ZIP_FIRMWARE_FILE_NAME
            let pathFirmware: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/\(fileFirmware)"
            print("pathFirmware : \(pathFirmware)")
            if FileManager.default.fileExists(atPath: pathFirmware)
            {
                filedata = try! Data(contentsOf: URL(fileURLWithPath: pathFirmware))
            }
            EADSessionController.shared().write(filedata)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "addProgressViewToUpdate"), object:nil);
        }
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x00" && buf[7]as! String == "0x02" &&  appDelegate().isConnectedToPrinter == true)
        {
            UserDefaults.standard.set(ZIP_PrintTimer, forKey: "zip_last_responce_time")
            UserDefaults.standard.synchronize()
            appDelegate().isProgressEnable = false
            //appDelegate().addTimerForProgressUpdate()
        }
            //get acessory info
            // after 255 imaqge printing on get acessory info condition make false to solve this issue remove 8th and 9tn byte
        else if (buf[6]as! String == "0x01" && buf[7]as! String == "0x02" && appDelegate().isConnectedToPrinter == true)
        {
            if( self.obj_RequestOperation.type == RequestType.getZIPInfo)
            {
                var dict = NSMutableDictionary()
                let status = self.remove_spe_char_from_string(buf[10] as! NSString)
                let battery = self.remove_spe_char_from_string(buf[12] as! NSString)
                let firmwareVersion = String(self.remove_spe_char_from_string(buf[21] as! NSString)) + "." + String(self.remove_spe_char_from_string(buf[22] as! NSString))  + String(self.remove_spe_char_from_string(buf[23] as! NSString))
                let powerStatus = self.remove_spe_char_from_string(buf[14] as! NSString)
                let photoPrinted = self.remove_spe_char_from_string(buf[10] as! NSString)
                let errorCode = self.remove_spe_char_from_string(buf[8] as! NSString)
                dict = ["Ststus":status,"Battery":battery,"FirmWareVersion":firmwareVersion,"PowerStatus":powerStatus,"PhotoPrinted":photoPrinted, "ErrorCode":errorCode, "ErrorHexString" : buf[8] as! NSString]
                self.obj_RequestOperation.ResponseDelegate.ZIP_accessory_info!(dict)
            }
            else
            {
                let flt  =  self.remove_spe_char_from_string(buf[21] as! NSString)
                let flt1 = ((self.remove_spe_char_from_string(buf[22] as! NSString )))
                let flt2 = ((self.remove_spe_char_from_string(buf[23] as! NSString)))
                let flt3 = ((self.remove_spe_char_from_string(buf[24] as! NSString)))
                let flt4 = ((self.remove_spe_char_from_string(buf[25] as! NSString)))
                let flt5 = ((self.remove_spe_char_from_string(buf[26] as! NSString)))
                if flt == 1{
                    appDelegate().ZIP_Product_ID = "0"
                }else if flt == 2{
                    appDelegate().ZIP_Product_ID = "1"
                }
                self.obj_RequestOperation.ResponseDelegate.GetVersionResponse?("v\(flt).\(flt1)\(flt2)", Conexan: "v\(flt3).\(flt4)", TMD: "v\(flt3).\(flt4).\(flt5)")
            }
            Is_Busy = false
        }
        else if (buf[6]as! String == "0x00" && buf[7]as! String == "0x02" && appDelegate().isConnectedToPrinter == true)
        {
            Is_Busy = false
        }
            //update firmware
            //StartOfSendAck :->>  when sucessfully deliverd the Upgrade Ready Request
        else if (buf[6]as! String == "0x02" && buf[7]as! String == "0x00" && appDelegate().isConnectedToPrinter == true)
        {
            // Data Sending completed and start to print
            EADSessionController.shared().write(obj_RequestOperation.img)
            appDelegate().isProgressEnable = true
        }
            //UPgrade ack : start
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x03" && buf[7]as! String == "0x02" && buf[8]as! String == "0x00"  && appDelegate().isConnectedToPrinter == true)
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "addProgressViewToUpdate"), object:nil);
        }
            //UPgrade ack : finish
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x03" && buf[7]as! String == "0x02" && buf[8]as! String == "0x02"  && appDelegate().isConnectedToPrinter == true)
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "FirmwareUpdateFaild"), object:nil);
        }
            //UPgrade ack : error
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x03" && buf[7]as! String == "0x02" && buf[8]as! String == "0x01"  && appDelegate().isConnectedToPrinter == true)
        {
            self.obj_RequestOperation.ResponseDelegate.firmwareUpdateFinished!()
            Is_Busy = false
        }
            //ZIP Print Ready EndOfRecievedACK
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x02" && buf[7]as! String == "0x01" && buf[8] as! String == "0x00" && appDelegate().isConnectedToPrinter == true){
        }
            // print finish
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x00" && buf[7]as! String == "0x03" && buf[8]as! String == "0x00"  && appDelegate().isConnectedToPrinter == true)
        {
            print("PRINT FINISH CALLED >>>>")
            IS_PRINT_ERROR = false
            IS_OUT_OF_PAPER_POP_SHOWN = false
            // show app rating popup.
            show_AppRating_Popup = 1 + show_AppRating_Popup
            SingletonClass.sharedInstance.showRatingsPopup()
            finalCopiesToCheckdelete = finalCopiesToCheckdelete + 1
            if finalCopiesToCheckdelete != 1 && finalCopiesToCheckdelete != 0{
                noOfCopies = noOfCopies - finalCopiesToCheckdelete
            }
            //Akshay Solve crash detect on crashanalitics
            if(obj_RequestOperation != nil && obj_RequestOperation.no_Of_Copy > 0)
            {
                if self.obj_RequestOperation.PrintResponseDelegate != nil
                {
                    self.obj_RequestOperation.PrintResponseDelegate?.get_PrintingPercentage(obj_RequestOperation.no_Of_Copy!, Percentage: "75")
                }
                obj_RequestOperation.no_Of_Copy! = obj_RequestOperation.no_Of_Copy! - 1
                if(obj_RequestOperation.no_Of_Copy == 0)
                {
                    Is_Busy = false
                }
            }
            // When print is completed, reduce the count by 1
            appDelegate().totalCountOfPrint = appDelegate().totalCountOfPrint - 1
            // Check if all images are printed and fire the notification
            if appDelegate().totalCountOfPrint == 0
            {
                isPrintDoneCheckOutOfPaper = true
                IS_HOLD_ON_POP_SHOWN = false
                appDelegate().isPrintingInProgress = false
                NotificationCenter.default.post(name: Notification.Name(rawValue: "PrintFinished"), object:nil);
            }
        }
            // print error
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x00" && buf[7]as! String == "0x03" && buf[8]as! String == "0x01"  && appDelegate().isConnectedToPrinter == true)
        {
            print("PRINT ERROR CALLED >>>>")
            appDelegate().isPrintingInProgress = false
            Is_Busy = false
            IS_PRINT_ERROR = true
            NotificationCenter.default.post(name: Notification.Name(rawValue: "PrintFinished"), object:nil);
            PrinterQueueManager.RequestArray.removeAll()
            self.call_Api_Get_ZIP_Firmware_TMD_Version()
        }
        //ZIP  COMPLETE
        // Snaptouch
        //image print
        if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x00" && buf[8] as! String == "0x00" && buf[9]as! String == "0x00"){
            print_Image_Loader_Flag = true
            UserDefaults.standard.set(PrintTimer, forKey: "last_responce_time")
            UserDefaults.standard.synchronize()
            EADSessionController.shared().write(obj_RequestOperation.img )
        }
            //Print Ready EndOfRecievedACK
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x01" && buf[8] as! String == "0x00" && buf[9]as! String == "0x00"){
            print_Image_Loader_Flag = true
            UserDefaults.standard.set(PrintTimer, forKey: "last_responce_time")
            UserDefaults.standard.synchronize()
            self.obj_RequestOperation.ResponseDelegate.image_upload_successfully?()
        }
            //Print Ready PrintStart
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x00" && buf[7]as! String == "0x02"){
            print_Image_Loader_Flag = true
            UserDefaults.standard.set(PrintTimer, forKey: "last_responce_time")
            UserDefaults.standard.synchronize()
            self.obj_RequestOperation.PrintResponseDelegate?.printing_Started(obj_RequestOperation.totalCopies!)
            self.obj_RequestOperation.PrintResponseDelegate?.get_PrintingPercentage(obj_RequestOperation.no_Of_Copy!, Percentage: "0")
            
        }
            //Print Ready PrintingPercentage
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x05" && buf[7]as! String == "0x00"){
            UserDefaults.standard.set(PrintTimer, forKey: "last_responce_time")
            UserDefaults.standard.synchronize()
            if (buf[8]as! String == "0x01"){
                self.obj_RequestOperation.PrintResponseDelegate?.get_PrintingPercentage(obj_RequestOperation.no_Of_Copy!, Percentage: "25")
            }else if (buf[8]as! String == "0x02"){
                self.obj_RequestOperation.PrintResponseDelegate?.get_PrintingPercentage(obj_RequestOperation.no_Of_Copy!, Percentage: "50")
            }else if (buf[8]as! String == "0x03"){
                self.obj_RequestOperation.PrintResponseDelegate?.get_PrintingPercentage(obj_RequestOperation.no_Of_Copy!, Percentage: "75")
            }
            else
            {
                self.obj_RequestOperation.PrintResponseDelegate?.get_PrintingPercentage(obj_RequestOperation.no_Of_Copy!, Percentage: "100")
            }
        }
            //Send Device Name StartofSendAck
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x00" && buf[8] as! String == "0x07" && buf[9]as! String == "0x00"){
            if(appDelegate().isConnectedToPrinter != true)
            {
                if let deviceName = obj_RequestOperation.DeviceName
                {
                    self.send_devicename(deviceName)
                }
                else{
                    self.send_devicename(UIDevice.current.name)
                }
            }
        }
            //Send Device Name EndOfRecievedACK
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x01" && buf[8] as! String == "0x07" && buf[9]as! String == "0x00"){
            self.obj_RequestOperation.ResponseDelegate.sendDeviceNameSaved?()
            Is_Busy = false
        }
            //GetBatteryLevelResponse
        else if (buf[4]as! String == "0x01" && buf[6]as! String == "0x0f"){
            if (buf[8]as! String == "0x00"){
                self.obj_RequestOperation.ResponseDelegate.getBetteryLavelResponse?("5")
            }else if (buf[8]as! String == "0x01"){
                self.obj_RequestOperation.ResponseDelegate.getBetteryLavelResponse?("20")
            }else if (buf[8]as! String == "0x02"){
                self.obj_RequestOperation.ResponseDelegate.getBetteryLavelResponse?("60")
            }else if (buf[8]as! String == "0x03"){
                self.obj_RequestOperation.ResponseDelegate.getBetteryLavelResponse?("100")
            }else{
                self.obj_RequestOperation.ResponseDelegate.getBetteryLavelResponse?("0")
            }
            Is_Busy = false
        }
            //GetPageTypeResponse
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x0d" && buf[7]as! String == "0x00"){
            if (buf[8]as! String == "0x00"){
                self.obj_RequestOperation.ResponseDelegate.GetPageTypeResponse?("Normal")
            }else if (buf[8]as! String == "0x01"){
                self.obj_RequestOperation.ResponseDelegate.GetPageTypeResponse?("Rainbow")
            }
            Is_Busy = false
        }
            //print_Count response //ana
        else if ( buf[6]as! String == "0x00" && buf[7]as! String == "0x04"){
            Is_Busy = false
        }
            //ana : get response for auto power off details
        else if ( buf[6]as! String == "0x10" && buf[7]as! String == "0x00"){
            if (buf[8]as! String == "0x00"){
                self.obj_RequestOperation.ResponseDelegate.getAutoPowerOffResponse?("3")
            }else if (buf[8]as! String == "0x01"){
                self.obj_RequestOperation.ResponseDelegate.getAutoPowerOffResponse?("5")
            }else if (buf[8]as! String == "0x02"){
                self.obj_RequestOperation.ResponseDelegate.getAutoPowerOffResponse?("10")
            }
            Is_Busy = false
        }
            //GetStickerResponse
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x0a" && buf[7]as! String == "0x01"){
            print("GetStickerResponse ack")
            self.obj_RequestOperation.ResponseDelegate.GetStickerResponse?([buf[8]as! String,buf[9]as! String,buf[10]as! String,buf[11]as! String,buf[12]as! String])
            Is_Busy = false
        }
            //AddSticker StartofSendAck
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x00" && buf[8] as! String == "0x03" && buf[9]as! String == "0x00"){
            print("AddSticker ack")
            if obj_RequestOperation.imgSticketDict.count != 0 {
                self.stickertKey = 4 - obj_RequestOperation.imgSticketDict.count
                let strStickertKey = String(self.stickertKey)
                
                self.call_Api_Update_Sticker_ready(obj_RequestOperation.newStickerId!, SubStickerId: strStickertKey , img: obj_RequestOperation.imgSticketDict[0])
            }
        }
            //AddStickerReady StartofSendAck
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x00" && buf[8] as! String == "0x08" && buf[9]as! String == "0x00"){
            print("AddStickerReady ack")
            if let filename = Bundle.main.path(forResource: obj_RequestOperation.imgSticketDict[0], ofType: "jpg"){
                EADSessionController.shared().write(try? Data(contentsOf: URL(fileURLWithPath: filename)))
                
            }
        }
            //AddStickerReady EndOfRecievedACK
            //&& buf[9]as! String == "0x00"
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x01" && buf[8] as! String == "0x08" ){
            obj_RequestOperation.imgSticketDict.remove(at: 0)
            if obj_RequestOperation.imgSticketDict.count != 0 {
                self.stickertKey = 4 - obj_RequestOperation.imgSticketDict.count
                let strStickertKey = String(self.stickertKey)
                self.call_Api_Update_Sticker_ready(obj_RequestOperation.newStickerId!, SubStickerId: strStickertKey , img: obj_RequestOperation.imgSticketDict[0])
            }else{
                self.obj_RequestOperation.ResponseDelegate.AddStickerSuceess?()
                Is_Busy = false
            }
            
        }
            //GetBorderResponse
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x0c" && buf[7]as! String == "0x01"){
            self.obj_RequestOperation.ResponseDelegate.GetBorderResponse?([buf[8]as! String,buf[9]as! String,buf[10]as! String,buf[11]as! String,buf[12]as! String])
            Is_Busy = false
        }
            //AddBorder StartofSendAck
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x00" && buf[8] as! String == "0x05" && buf[9]as! String == "0x00"){
            if obj_RequestOperation.imgBorderDict.count != 0 {
                self.BorderKey = 2 - obj_RequestOperation.imgBorderDict.count
                let strBorderKey = String(self.BorderKey)
                self.call_Api_Update_Border_ready(obj_RequestOperation.newBorderId!, SubBorderId: strBorderKey , img: obj_RequestOperation.imgBorderDict[0])
            }
        }
            //AddBorderReady StartofSendAck
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x00" && buf[8] as! String == "0x09" && buf[9]as! String == "0x00"){
            
            if let filename = Bundle.main.path(forResource: obj_RequestOperation.imgBorderDict[0], ofType: "jpg"){
                EADSessionController.shared().write(try? Data(contentsOf: URL(fileURLWithPath: filename)))
            }
        }
            //AddBorderReady EndOfRecievedACK
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x01" && buf[8] as! String == "0x09" && buf[9]as! String == "0x00"){
            obj_RequestOperation.imgBorderDict.remove(at: 0)
            if obj_RequestOperation.imgBorderDict.count != 0 {
                self.BorderKey = 2 - obj_RequestOperation.imgBorderDict.count
                let strBorderKey  = String( self.BorderKey)
                
                self.call_Api_Update_Border_ready(obj_RequestOperation.newBorderId!, SubBorderId: strBorderKey , img: obj_RequestOperation.imgBorderDict[0])
            }else{
                self.obj_RequestOperation.ResponseDelegate.AddBorderSuceess?()
                Is_Busy = false
            }
        }
            //GetFirmwareTMDVersionResponse
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x02" && buf[7]as! String == "0x05")
        {
            let flt  =  self.remove_spe_char_from_string(buf[8] as! NSString)
            let flt1 = ((self.remove_spe_char_from_string(buf[9] as! NSString )))
            let flt2 = ((self.remove_spe_char_from_string(buf[10] as! NSString)))
            let flt3 = ((self.remove_spe_char_from_string(buf[11] as! NSString)))
            let flt4 = ((self.remove_spe_char_from_string(buf[12] as! NSString)))
            let flt5 = ((self.remove_spe_char_from_string(buf[13] as! NSString)))
            let flt6 = ((self.remove_spe_char_from_string(buf[14] as! NSString)))
            self.obj_RequestOperation.ResponseDelegate.GetVersionResponse?("v\(flt).\(flt1)\(flt2)", Conexan: "v\(flt3).\(flt4)", TMD: "v\(flt5).\(flt6)")
            Is_Busy = false
        }
            //EnterFirmwareUpdatePage EndOfRecievedACK
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x01" && buf[8]as! String == "0x0b"){
            Is_Busy = false
        }
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x02" && buf[7]as! String == "0x03"){
            self.calculate_pass_data((buf[8]as! String).replacingOccurrences(of: "0x", with: "", options: NSString.CompareOptions.literal, range: nil))
            self.call_Api_EnterFirmwareUpgrad_ready(self.upload_zips_file_data[0])
        }
        else if (buf[4]as! String == "0x00" && buf[5]as! String == "0x00" && buf[6]as! String == "0x02" && buf[7]as! String == "0x01" && buf[8]as! String == "0x01"){
            EADSessionController.shared().cancelwriteData()
            Is_Busy = false
        }
            //EnterFirmwareUpdatePage StartofSendAck
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x00" && buf[8]as! String == "0x0b")
        {
            self.call_Api_EnterFirmwareUpgrad_ready(self.upload_zips_file_data[0])
        }
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x00" && (buf[8]as! String == "0x01" || buf[8]as! String == "0x0c" || buf[8]as! String == "0x02")){
            var filedata = Data()
            var file = String()
            switch buf[8] as! String{
            case "0x0c" :
                file = newTMDfileName
            case "0x01":
                file = newCNXfileName
            case "0x02":
                file = newFIRMfileName
            default:
                file = ""
            }
            let fileConexant: String = file
            let pathConexant: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] +  "/\(fileConexant)"
            if FileManager.default.fileExists(atPath: pathConexant)
            {
                filedata = try! Data(contentsOf: URL(fileURLWithPath: pathConexant))
            }
            print(fileConexant)
            EADSessionController.shared().write(filedata)
        }
        else if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x01" && buf[7]as! String == "0x01" && (buf[8]as! String == "0x02" || buf[8]as! String == "0x01" || buf[8]as! String == "0x0c")){
            self.upload_zips_file_data.remove(at: 0)
            if self.upload_zips_file_data.count != 0{
                self.call_Api_EnterFirmwareUpgrad_ready(self.upload_zips_file_data[0])
                //print("calling -> 5   call_Api_EnterFirmwareUpgrad_ready")
            }else{
                self.obj_RequestOperation.ResponseDelegate.Update_Version_Suceess?()
                Is_Busy = false
            }
        }
            //Firmware update cancel
        else if (buf[4]as! String == "0x01" && buf[6]as! String == "0x01"  && buf[7]as! String == "0x01" && buf[8]as! String == "0x0d"){
            print("Firmware update cancel")
            NotificationCenter.default.post(name: Notification.Name(rawValue: "FirmwareUpdateCancel"), object: nil)
        }
        else if ((buf[6]as! String == "0x04" && buf[7]as! String == "0x00" && appDelegate().isConnectedToPrinter == true) || (buf[6]as! String == "0x01" && buf[7]as! String == "0x02" && buf[8]as! String != "0x00"  && appDelegate().isConnectedToPrinter == true))
        {
            self.handleErrorCodes(errorCode: buf[8]as! String)
        }
        else
        {
            if (buf[4]as! String == "0x01" && buf[5]as! String == "0x00" && buf[6]as! String == "0x02" && buf[7]as! String == "0x01" && appDelegate().isConnectedToPrinter)
            {
                print("ACkolage is getting for zip for printing")
                // Added by Pratiksha to get exact real time start of printing
                appDelegate().isPrintingInProgress = true
            }
        }
    }
    
    func remove_spe_char_from_string(_ text1:NSString)->Int{
        return Int(text1.replacingOccurrences(of: "0x", with: "") as String, radix: 16)!
    }
}

