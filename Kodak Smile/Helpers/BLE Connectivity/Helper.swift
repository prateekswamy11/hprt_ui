//
//  Utilities.swift
//  SnapTouchPrinter
//
//  Created by peerbits_11 on 22/04/17.
//  Copyright © 2017 Rajumar MacMini13. All rights reserved.
//

import UIKit
import Foundation

//
// --------------------- Declarations --------------
// 

let App_Name = "Polaroid MINT"
//
let Error_UnAuthorised_Album = NSLocalizedString("ALERT_Authorizeto_AcessPhotos", comment:"")
let Alert_Busy = NSLocalizedString("MSG_Printer_BUSY", comment:"")
let Alert_Reboot_Printer = NSLocalizedString("MSG_Please_Reboot_printer", comment:"")
let Alert_Paper_Empty = NSLocalizedString("Paper Empty in printer", comment:"")
let Alert_Paper_Mismatch = NSLocalizedString("MSG_UseSmartSheet_Only", comment:"")
let Alert_Data_Error = NSLocalizedString("MSG_Retry_SendPhoto", comment:"")
let Alert_Cover_Open = NSLocalizedString("Please check cover of printer", comment:"")
let Alert_Battery_Low = NSLocalizedString("MSG_NeedtoChargePrinter", comment:"")
let Alert_High_Temperature = NSLocalizedString("MSG_High_Temperature", comment:"")
let Alert_Low_Temperature = NSLocalizedString("MSG_Low_Temperature", comment:"")
let Alert_Cooling_Mode = NSLocalizedString("MSG_PrinterIN_CoolingMode", comment:"")
let Alert_Cancel_Transfer = NSLocalizedString("MSG_TransferedCancel", comment:"")
let Alert_Wrong_Customer = NSLocalizedString("Wrong Customer Code", comment:"")
let Alert_Switch_Printer = NSLocalizedString("CONFIRM_switchPrinter", comment:"")


//
// --------------------- Functions --------------
//

/*
// Function to Open Bluetooth Screen
func openBluetoothSettings(){
    let urlBTSet = URL(string: "App-Prefs:root=Bluetooth")
    let objApp = UIApplication.shared
    objApp.openURL(urlBTSet!)
}
*/

func showAlert(message : String) {
    UIAlertView(title: App_Name, message: message, delegate: nil, cancelButtonTitle: "OK".localisedString()).show()
}

func api_changePowerOffStatus(status:NSInteger)
{
    let RequestData: NSMutableData = NSMutableData()
    
    RequestData.append(hexa_to_8_Bit_NSDATA("0x1B")! as Data)
    RequestData.append(hexa_to_8_Bit_NSDATA("0x2A")! as Data)
    RequestData.append(hexa_to_8_Bit_NSDATA("0x43")! as Data)
    RequestData.append(hexa_to_8_Bit_NSDATA("0x41")! as Data)
    RequestData.append(hexa_to_8_Bit_NSDATA("0x00")! as Data)
    RequestData.append(hexa_to_8_Bit_NSDATA("0x00")! as Data)
    RequestData.append(hexa_to_8_Bit_NSDATA("0x01")! as Data)
    RequestData.append(hexa_to_8_Bit_NSDATA("0x01")! as Data)
    RequestData.append(hexa_to_8_Bit_NSDATA("0x01")! as Data)
    
    if status==0{
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")! as Data)
        
    }
    if status==3
    {
        RequestData.append(hexa_to_8_Bit_NSDATA("0x04")! as Data)
    }
    if status==5
    {
        RequestData.append(hexa_to_8_Bit_NSDATA("0x08")! as Data)
    }
    if status==10
    {
        RequestData.append(hexa_to_8_Bit_NSDATA("0x0C")! as Data)
    }
    //                while RequestData.length < 34
    //                {
    //            RequestData.append(hexa_to_8_Bit_NSDATA(hexa: "0x00")! as Data)
    //                }
    //  Accessory_Manager.shared.setUpSession(accessory: RequestData.)
    EADSessionController.shared().isImagePrinting = false
    //EADSessionController.shared().delegate = self
    EADSessionController.shared().write(RequestData as Data?)
    
    
}

// Function to showHidePassword using button
func showHidePassword(_ sender: UIButton,_ txtFieldPassword: UITextField)
{
    if !sender.isSelected{
        sender.isSelected = true
        txtFieldPassword.isSecureTextEntry = false
    }else{
        sender.isSelected = false
        txtFieldPassword.isSecureTextEntry = true
    }
}

// Function to disable button
func disableBtnAnimated(_ sender:UIButton)
{
    sender.isEnabled = false
    UIView.animate(withDuration: 1, delay: 0.1, options: .beginFromCurrentState, animations: {
        sender.alpha = 0.30
    }, completion: nil)
}

// Function to enable button
func enableBtnAnimated(_ sender:UIButton)
{
    sender.isEnabled = true
    UIView.animate(withDuration: 1, delay: 0.1, options: .beginFromCurrentState, animations: {
        sender.alpha = 1
    }, completion: nil)
}

// UIImage Extention
extension UIImage {
    
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImageOrientation.up {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        } else {
            return self
        }
    }

}
