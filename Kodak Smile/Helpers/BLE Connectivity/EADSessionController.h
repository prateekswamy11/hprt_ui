
#import <Foundation/Foundation.h>
#import <ExternalAccessory/ExternalAccessory.h>
#import <UIKit/UIKit.h>

extern NSString *EADSessionDataReceivedNotification;

@protocol OperationProtocolDelegate <NSObject>
@required

- (void) readDataResponse :(NSMutableArray *)arrResponse;

@end

@protocol OperationUploadProtocolDelegate <NSObject>
@required

- (void) UploadDataResponse :(NSMutableArray *)arrResponse;
- (void) UploadDataResponseImage :(NSMutableData *)arrResponse;
- (void) UploadDataResponseImageSize :(long)Size;

@end

@interface EADSessionController : NSObject <EAAccessoryDelegate, NSStreamDelegate>
{
    id <OperationProtocolDelegate> _delegate;
    id <OperationUploadProtocolDelegate> _Uploaddelegate;

    EAAccessory *_accessory;
    EASession *_session;
    NSString *_protocolString;

    NSMutableData *_writeData;
    NSMutableData *_readData;
    BOOL isImageData;
}
@property (nonatomic,strong) id delegate;
@property (nonatomic,strong) id Uploaddelegate;

+ (EADSessionController *)sharedController;

- (void)setupControllerForAccessory:(EAAccessory *)accessory withProtocolString:(NSString *)protocolString;

- (BOOL)openSession;
- (void)closeSession;

- (void)writeData:(NSData *)data;
- (void)CancelwriteData;
- (void)send_images:(NSArray *)photos;
- (BOOL) writeTCPRawData:(NSData*) data;
- (NSString *)intToBinary21:(NSString *)hexNumber;
-(long)getbinaty_to_decimal:(NSMutableString *)str_size;

- (NSUInteger)readBytesAvailable;
- (NSData *)readData:(NSUInteger)bytesToRead;

@property (nonatomic, readonly) EAAccessory *accessory;
@property (nonatomic, readonly) NSString *protocolString;
@property (nonatomic, strong) NSMutableData *arrResposeImage;
@property  int DataLength;

@property BOOL isImagePrinting;

@property (nonatomic, strong) NSMutableData *imageData;

@end
