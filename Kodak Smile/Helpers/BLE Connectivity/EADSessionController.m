

#import "EADSessionController.h"
//#import "Tbl_devices.h"
//#import "CoreData.h"

NSString *EADSessionDataReceivedNotification = @"EADSessionDataReceivedNotification";

@implementation EADSessionController

@synthesize accessory = _accessory;
@synthesize protocolString = _protocolString;
@synthesize DataLength;
@synthesize delegate;

 uint8_t byteIndex=0;
NSInteger writeResult = 0;
bool image_UPLD = false;
NSUInteger image_UPLD_size=0;
#define WRITE_LEN 4096
#define READ_LEN 34
///

/*
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
 */
///

#pragma mark Internal

// low level write method - write data to the accessory while there is space available and data to write


//added by ana
- (instancetype)init
{
    self = [super init];
    if (self) {
        DataLength=0;
        
    }
    return self;
}

- (BOOL) writeTCPRawData: (NSData*) data
{
    //writeResult = 0;
    //NSLog(@"writeTCPRawData Data length : %ld%%",data.length);
    int offset = 0;
    int len = WRITE_LEN;
    uint8_t buf[WRITE_LEN];
    uint8_t* writeBytes = (uint8_t*)[data bytes];
    BOOL ret = false;
     //NSLog(@"writeTCPRawData _writeData length : %ld%%",_writeData.length);
    while (([[_session outputStream] hasSpaceAvailable]) && ([_writeData length] > 0) )
    {
        //NSLog(@"outputStream : %ld%%",[[_session outputStream] hasSpaceAvailable]);
         //MARK:- error solve change by maximess
        // NSInteger bytesWritten = [[_session outputStream] write:[_writeData bytes] maxLength:[_writeData length]];
        
        //NSLog(@"NSInteger bytesWritten : %ld%%",bytesWritten);
        
        len = (int)[data length] - offset;
        //NSLog(@" len length : %ld%%",len);
        if (len > WRITE_LEN)
        {
            len = WRITE_LEN;
        }
         //MARK:- error solve change by maximess
        if  (len <= 0) //(len == -1)
        {
            ret = true;
            break;
        }
        memcpy(buf, writeBytes + offset, len);
        //NSLog(@"writeBytes : %s%%",writeBytes);
        //NSLog(@"writeBytes : %ld%%",writeBytes);
        
        int bytesWriiten = (int)[[_session outputStream] write:(const uint8_t*)buf maxLength:len];
        
        NSUserDefaults * getValue = [NSUserDefaults standardUserDefaults];
        BOOL * value = [getValue boolForKey:@"clearValues"];
        
        if (value == 1){
            
            NSUserDefaults * setValue =[NSUserDefaults standardUserDefaults];
            [setValue setBool: false forKey:@"clearValues"];
            [setValue synchronize];
            
            writeResult = 0;
            DataLength = 0;
        }
        
        //NSLog(@"bytesWriiten : %ld%%",bytesWriiten);
        
        if(_writeData.length > 0)
        {
            [_writeData replaceBytesInRange:NSMakeRange(0, bytesWriiten) withBytes:NULL length:0];
            
            //NSLog(@"Total Percentage of Written Data : %ld%%",writeResult *100/DataLength);
    
            NSString *inStr = [NSString stringWithFormat: @"%ld", (long)writeResult *100/DataLength];
            NSDictionary *dict = @{ @"percent" : inStr};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FWsendDataPercent" object:self userInfo:dict];
            
            if (_isImagePrinting == true)
            {
  
                float progress = writeResult *100/DataLength;
                
//                NSDictionary* dict = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:progress]forKey:@"progress"];
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"ProgressNotification" object:self userInfo:dict];
//
//                NSDictionary* dict2 = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:progress]forKey:@"percentage"];
//
//                 [[NSNotificationCenter defaultCenter] postNotificationName:@"updateProgressNotification" object:self userInfo:dict2];
//
                //NSNotificationCenter.defaultCenter().postNotificationName("updateProgressNotification", object:myDict);
            }
            
        }
        else
        {
            return false;
        }
        
//        //NSLog(@"Total Percentage of Written Data : %d%%",writeResult *100/DataLength);
        
        
        if (bytesWriiten < 0)
        {
            ret = false;
            break;
        }
        else
        {
            offset += bytesWriiten;
            writeResult+=bytesWriiten;
            //NSLog(@"Written Data : %d \noffset Data : %d",writeResult,offset);
        }
    
    }
    return ret;
}
                
// low level read method - read data while there is data and space available in the input buffer
- (void)_readData
{
    // Read Data
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setInteger:60 forKey:@"last_responce_time"];
    [userdefaults synchronize];
    
    #define EAD_INPUT_BUFFER_SIZE 42
    uint8_t buf[EAD_INPUT_BUFFER_SIZE];
    while ([[_session inputStream] hasBytesAvailable])
    {
        NSInteger bytesRead = [[_session inputStream] read:buf maxLength:EAD_INPUT_BUFFER_SIZE];
        if (_readData == nil) {
            _readData = [[NSMutableData alloc] init];
        }
        [_readData appendBytes:(void *)buf length:bytesRead];
//        //NSLog(@"_readData : %lu",(unsigned long)_readData.length);
        if (image_UPLD == true && _readData.length >= image_UPLD_size){
            if ([self.Uploaddelegate respondsToSelector:@selector(UploadDataResponseImage:)]) {
                [self.Uploaddelegate UploadDataResponseImage:_readData];
                image_UPLD = false;
            }
        }
    }
    _writeData =nil;
    
    NSMutableArray *arrRespose = [[NSMutableArray alloc] initWithCapacity:34];
   // //NSLog(@"buff ---  %s",buf);
    
    
//    //NSLog(@"------------------------------\n ");
    for (int i=0; i<34; i++)
    {
        [arrRespose addObject:[NSString stringWithFormat:@"0x%02x", (unsigned int)buf[i]]];
      //  //NSLog(@"Int Value :%d -----> Value  : %i ---> Hex Value : 0x%02x",i+8,buf[i],(unsigned int) buf[i]);
    }

    //BOOL flag = [self.delegate respondsToSelector:@selector(readDataResponse:)];

    if ([self.delegate respondsToSelector:@selector(readDataResponse:)] && image_UPLD == false) {
        [self.delegate readDataResponse:arrRespose];
    }

   //NSLog(@"buff ---  %@",arrRespose);
 //NSLog(@"buff ---  %s",buf);

    //========================================================================
    // Upload
    if (buf[4] == 0x01 && buf[5] == 0x00 && buf[6] == 0x11)
    {
        
        if (buf[7] == 0x00){
            image_UPLD = true;
            image_UPLD_size = 0;
            self.arrResposeImage = [[NSMutableData alloc] init];
            NSMutableString *str_size = [NSMutableString string];
            for (int i=9; i<12; i++)
            {
                [str_size appendString:[self intToBinary21:[NSString stringWithFormat:@"0x%02x", (unsigned int)buf[i]]]];
            }
            
            const char* utf8String = [str_size UTF8String];
            const char* endPtr = NULL;
            long int foo = strtol(utf8String, &endPtr, 2);
            //NSLog(@"foo %ld",foo);
            image_UPLD_size = foo;
            _readData = nil;
            //NSLog(@"foo : %ld",foo);

            if ([self.Uploaddelegate respondsToSelector:@selector(UploadDataResponseImageSize:)]) {
                [self.Uploaddelegate UploadDataResponseImageSize:image_UPLD_size];
            }

        }

        if ([self.Uploaddelegate respondsToSelector:@selector(UploadDataResponse:)]) {
            [self.Uploaddelegate UploadDataResponse:arrRespose];
        }
    }
    
    //========================================================================
//    if (buf[6] == 0x00 && buf[7]== 0x02)
//    {
//        [self closeSession];
//        [self setupControllerForAccessory:nil withProtocolString:nil];
//    }
    
    
    //Upload from snaptouch to UPA
    else if (buf[4] == 0x01 && buf[5] == 0x00 && buf[6] == 0x11 && buf[7] == 0x02){
        //NSLog(@"Request for Upload from snaptouch to UPA");
        //NSLog(@"Uploadcount : 0x%02x", (unsigned int) buf[8]);
    }
}

#pragma mark Public Methods

+ (EADSessionController *)sharedController
{
    static EADSessionController *sessionController = nil;
    if (sessionController == nil) {
        sessionController = [[EADSessionController alloc] init];
    }
  
    return sessionController;
}

- (void)dealloc
{
    [self closeSession];
    [self setupControllerForAccessory:nil withProtocolString:nil];
}

// initialize the accessory with the protocolString
- (void)setupControllerForAccessory:(EAAccessory *)accessory withProtocolString:(NSString *)protocolString
{
    _accessory = accessory;
    _protocolString = [protocolString copy];
}

// open a session with the accessory and set up the input and output stream on the default run loop
- (BOOL)openSession
{
    [_accessory setDelegate:self];
    _session = [[EASession alloc] initWithAccessory:_accessory forProtocol:_protocolString];

    if (_session)
    {
        [[_session inputStream] setDelegate:self];
        [[_session inputStream] scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [[_session inputStream] open];
        
        [[_session outputStream] setDelegate:self];
        [[_session outputStream] scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [[_session outputStream] open];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        dict[@"accessoryList"] = _accessory;
        
       // [[[UIApplication sharedApplication] delegate] ]
        
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AccessoryConnectFinish" object:self userInfo:nil];
            
            if(_accessory != nil)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AccessoryConnect" object:self userInfo:dict];

            }
            
        });
     
        

    }
    else
    {
        //NSLog(@"creating session failed");
    }

    return (_session != nil);
}

// close the session with the accessory.
- (void)closeSession
{
    //NSLog(@"close session and printer...megha");

    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Accessories_dis_connected" object:nil];
    [[_session inputStream] close];
    [[_session inputStream] removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [[_session inputStream] setDelegate:nil];
    [[_session outputStream] close];
    [[_session outputStream] removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [[_session outputStream] setDelegate:nil];

    _session = nil;
    _writeData = nil;
    _readData = nil;
}
// high level write data method
- (void)writeData:(NSData *)data
{
     //NSLog(@" writeData Data length : %ld%%",data.length);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TestNotification" object:nil];
    if (_writeData == nil) {
        _writeData = [[NSMutableData alloc] init];
    }
    
    [_writeData appendData:data];
    //MARK:- error solve change by maximess
    DataLength=_writeData.length;
    
//    NSInteger myInteger = _writeData.length;
//    int myInt = (int) myInteger;
//    DataLength=myInt;
    if (DataLength <= 50){
        //NSLog(@"data : %@",data);
    }
    //NSLog(@" _writeData.length : %ld%%",_writeData.length);
    [self writeTCPRawData:_writeData];
    
}
- (void)CancelwriteData
{
    _writeData = nil;
}

// high level read method
- (NSData *)readData:(NSUInteger)bytesToRead
{
    NSData *data = nil;
    if ([_readData length] >= bytesToRead) {
        NSRange range = NSMakeRange(0, bytesToRead);
        data = [_readData subdataWithRange:range];
        [_readData replaceBytesInRange:range withBytes:NULL length:0];
    }
    
    //NSLog(@"READ DATA %@",_readData);
    return data;
}

// get number of bytes read into local buffer
- (NSUInteger)readBytesAvailable
{
    return [_readData length];
}

#pragma mark EAAccessoryDelegate
- (void)accessoryDidDisconnect:(EAAccessory *)accessory
{
    //NSLog(@"Disconnected session and printer...");

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MMMM 'at' HH:mm a"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AccessoryDisConnect" object:nil];
    //NSLog(@"Current Date: %@", [formatter stringFromDate:[NSDate date]]);

    //NSLog(@"updating the status of devies");
    
//    CoreData *data = [[CoreData alloc]init];
//    Tbl_devices *devices = [data fetchDevice:@"Tbl_devices" withUniqueID:_accessory.serialNumber];
//    if(devices != nil)
//    {
//        devices.device_status = @"disConnected";
//        devices.device_last_connected_dateTime = [formatter stringFromDate:[NSDate date]];
//        devices.device_firmware_version = devices.device_firmware_version;
//        devices.device_tmd_version = devices.device_tmd_version;
//        devices.device_unique_id = devices.device_unique_id;
//        devices.device_name = devices.device_name;
//        devices.device_battery = devices.device_battery;
//        devices.device_auto_power_off_status = devices.device_auto_power_off_status;
//        [data saveContext];
//
//    }

    // do something ...
}

#pragma mark NSStreamDelegateEventExtensions

// asynchronous NSStream handleEvent method
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode
{
    switch (eventCode)
    {
        case NSStreamEventNone:
            break;
        case NSStreamEventOpenCompleted:
            break;
        case NSStreamEventHasBytesAvailable:
            [self _readData];
            break;
        case NSStreamEventHasSpaceAvailable:
            [self writeTCPRawData:_writeData];
            break;
        case NSStreamEventErrorOccurred:
            break;
        case NSStreamEventEndEncountered:
            break;
        default:
            break;
    }
}


//C_RATHOD

-(void)send_images:(NSArray *)photos
{
    EADSessionController *sessionController = [EADSessionController sharedController];
    for ( UIImage *image in photos )
    {
        _writeData = nil;
        _readData = nil;
        NSData *imageData=UIImageJPEGRepresentation(image, 1.0);
        //NSLog(@"SIZE OF IMAGE SENDED FOR UPLOAD >>>>>>>>>>>>>> %lu",(unsigned long)imageData.length);
        [sessionController writeData:imageData];
        
    }
}


-(void)send_devicename{
    
    NSMutableData *RequestData=[[NSMutableData alloc]init];
    
    NSString *Device_Name  = [UIDevice currentDevice].name;
    
    [RequestData appendData:[Device_Name dataUsingEncoding:NSUTF8StringEncoding]];
    
    EADSessionController *sessionController = [EADSessionController sharedController];
    
    [sessionController writeData:RequestData];
    
}

//C_RATHOD

-(long)getbinaty_to_decimal:(NSMutableString *)str_size{
    const char* utf8String = [str_size UTF8String];
    const char* endPtr = NULL;
    long int foo = strtol(utf8String, &endPtr, 2);
    
    //NSLog(@"foo %ld",foo);
    return foo;

}
- (NSString *)intToBinary21:(NSString *)hexNumber
{
    
    unsigned int number = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexNumber];
    [scanner scanHexInt:&number];
    
    //NSLog(@"number %d",number);
    
    
    
    int bits =  sizeof(number) * 8;
//    int bitcnt=bits;
    //NSLog(@"bits %d",bits);
    
    NSMutableString *binaryStr = [NSMutableString string];
    
    for (; bits > 0; bits--, number >>= 1)
    {
        [binaryStr insertString:((number & 1) ? @"1" : @"0") atIndex:0];
    }
    
//    if (bitcnt == 32)
//    {
        binaryStr=[[binaryStr substringFromIndex:24] mutableCopy];
//    }
//    else if(bitcnt ==16)
//    {
//        binaryStr = [[@"00000000" stringByAppendingString:binaryStr] mutableCopy];
//    }
//    else if (bitcnt == 8)
//    {
//        binaryStr = [[@"0000000000000000" stringByAppendingString:binaryStr] mutableCopy];
//    }
//    [arrStr addObject:[binaryStr substringToIndex:8]];
//    [arrStr addObject:[binaryStr substringWithRange:NSMakeRange(8, 8)]];
//    [arrStr addObject:[binaryStr substringFromIndex:16]];
    
    //NSLog(@"%@",binaryStr);
    
    return binaryStr;
}


@end

