//
//  PrinterQueueManagerExtension.swift
//  Kodak Smile
//
//  Created by maximess152 on 22/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation

extension PrinterQueueManager {
    @objc func Timer_ticker() {
        let timer = UserDefaults.standard.integer(forKey: "last_responce_time")
        if timer == 0
        {
            self.timer.invalidate()
            self.Is_Busy = false
            UserDefaults.standard.removeObject(forKey: "last_responce_time")
            UserDefaults.standard.synchronize()
        }
        UserDefaults.standard.set((timer - 1), forKey: "last_responce_time")
        UserDefaults.standard.synchronize()
    }
    
    @objc func ZIP_Timer_ticker() {
        let zip_timer = UserDefaults.standard.integer(forKey: "zip_last_responce_time")
        if zip_timer == 0
        {
            self.Zip_Timer.invalidate()
            self.Is_Busy = false
            UserDefaults.standard.removeObject(forKey: "zip_last_responce_time")
            UserDefaults.standard.synchronize()
            if(PrinterQueueManager.RequestArray.count > 0)
            {
                let currReq = PrinterQueueManager.CurrentRequest;
                let incomingReq = PrinterQueueManager.RequestArray[0]
                if(currReq != incomingReq)
                {
                    self.call_Api_Print_Image(incomingReq.img!, Copy: incomingReq.no_Of_Copy!)
                }
            }
        }
        UserDefaults.standard.set((zip_timer - 1), forKey: "zip_last_responce_time")
        UserDefaults.standard.synchronize()
    }
    
    //    ======================================================== Print Image CAll ========================================================        //
    
    func call_Api_Print_Image(_ Img:Data,Copy:Int) {
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x00", "0x00"]
        RequestData = self.createBufferArray(bufferArray: array)
        //MARK:- error solve change by maximess
        let imageSize = SessionData.sharedInstance().getfilesize(Img) as Data
        let image = UIImage(data: Img)
        let data = UIImagePNGRepresentation(image!) as NSData?
        // Decimal to binary
        let d1 = data?.length
        let b1 = String(d1!, radix: 2)
        b1.data(using:.utf8)
        let nsdata = imageSize as NSData
        RequestData.append(imageSize)
        if Copy > 9{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x\(Copy)")!)
        }else{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x0\(Copy)")!)
        }
        while RequestData.length < 34 {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func PrintImageForZIPSnap(_ Img:Data,Copy:Int)
    {
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x00", "0x00"]
        RequestData = self.createBufferArray(bufferArray: array)
        RequestData.append(SessionData.sharedInstance().getfilesize(Img) as Data) //9,10,11
        if Copy > 9{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x0A")!)
        }else{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x0\(Copy)")!)
        }
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!) //13
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!) //14
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!) //15
        EADSessionController.shared().delegate = self
        EADSessionController.shared().isImagePrinting = true
        EADSessionController.shared().dataLength = Int32(Img.count)
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func send_Image(_ Img:UIImage) {
        EADSessionController.shared().delegate = self
        EADSessionController.shared().send_images([Img])
    }

    func call_Api_Get_Sticker() {
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x0A", "0x00"]
        RequestData = self.createBufferArray(bufferArray: array)
        while RequestData.length < 34
        {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
   
    func call_Api_Update_Sticker(_ oldStickerId : String , newStickerId : String , count:Int)
    {
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x0A", "0x02"]
        RequestData = self.createBufferArray(bufferArray: array)
        
        RequestData.append(decimalToBinaryConvertor(oldStickerId, toSize: 8))
        RequestData.append(decimalToBinaryConvertor(newStickerId, toSize: 8))
        if count > 9{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x\(count)")!)
        }else{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x0\(count)")!)
        }
        while RequestData.length < 34
        {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func call_Api_Update_Sticker_ready(_ newStickerId : String ,SubStickerId :String , img : String) {
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x0A", "0x03"]
        RequestData = self.createBufferArray(bufferArray: array)
        let hexNewStickerId = "0x" + String(format:"%02X", Int(newStickerId)!)
        //RequestData.append(decimalToBinaryConvertor(newStickerId, toSize: 8))
        print("hexNewStickerId = \(hexNewStickerId)")
        RequestData.append(hexa_to_8_Bit_NSDATA(hexNewStickerId)!)
        let strSubStickerId : String = SubStickerId
        if Int(SubStickerId)! > 9{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x\(strSubStickerId)")!)
        }else{
            let str = "0x0\(strSubStickerId)"
            print("str = \(str)")
            RequestData.append(hexa_to_8_Bit_NSDATA(str)!)
        }
        if let filename = Bundle.main.path(forResource: img, ofType: "jpg"){
        let imageSize  = SessionData.sharedInstance().getfilesize(try? Data(contentsOf: URL(fileURLWithPath: filename))) as Data
            print("imageSize.length= \(imageSize.count)")
            print("imageSize= \(imageSize)")
            RequestData.append(imageSize)
        }
        while RequestData.length < 34 {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
   
    func call_Api_Get_Border() {
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x0C", "0x00"]
        RequestData = self.createBufferArray(bufferArray: array)
        while RequestData.length < 34
        {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func call_Api_Update_Border(_ oldBorderId : String , newBorderId : String , count:Int)
    {
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x0C", "0x02"]
        RequestData = self.createBufferArray(bufferArray: array)
        RequestData.append(decimalToBinaryConvertor(oldBorderId, toSize: 8))
        RequestData.append(decimalToBinaryConvertor(newBorderId, toSize: 8))
        if count > 9{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x\(count)")!)
        }else{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x0\(count)")!)
        }
        while RequestData.length < 34
        {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func call_Api_Update_Border_ready(_ newBorderId : String ,SubBorderId :String , img : String) {
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x0c", "0x03"]
        RequestData = self.createBufferArray(bufferArray: array)
        let hexNewBorderId = "0x" + String(format:"%02X", Int(newBorderId)!)
        print("hexNewBorderId = \(hexNewBorderId)")
        RequestData.append(hexa_to_8_Bit_NSDATA(hexNewBorderId)!)
        if Int(SubBorderId)! > 9{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x\(SubBorderId)")!)
        }else{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x0\(SubBorderId)")!)
        }
        if let filename = Bundle.main.path(forResource: img, ofType: "jpg"){
            RequestData.append(SessionData.sharedInstance().getfilesize(try? Data(contentsOf: URL(fileURLWithPath: filename))) as Data)
        }
        while RequestData.length < 34 {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func createBufferArray(bufferArray: [String]) -> NSMutableData {
        let RequestData: NSMutableData = NSMutableData()
        RequestData.append(hexa_to_8_Bit_NSDATA("0x1B")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x2A")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x43")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x41")!)
        RequestData.append(hexa_to_8_Bit_NSDATA(bufferArray[0])!)
        RequestData.append(hexa_to_8_Bit_NSDATA(bufferArray[1])!)
        RequestData.append(hexa_to_8_Bit_NSDATA(bufferArray[2])!)
        RequestData.append(hexa_to_8_Bit_NSDATA(bufferArray[3])!)
        return RequestData
    }
    
    func call_Api_Get_Print_Count() {
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x01", "0x00", "0x00", "0x04"]
        RequestData = self.createBufferArray(bufferArray: array)
        while RequestData.length < 34
        {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func call_Api_Get_Firmware_TMD_Version()
    {
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x02", "0x04"]
        RequestData = self.createBufferArray(bufferArray: array)
        while RequestData.length < 34
        {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func call_Api_Get_ZIP_Firmware_TMD_Version()
    {
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x01", "0x00"]
        RequestData = self.createBufferArray(bufferArray: array)
        while RequestData.length < 16
        {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func call_Api_EnterFirmwareUpdatePage(_ classification : String)
    {
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x02", "0x06"]
        RequestData = self.createBufferArray(bufferArray: array)
        if Int(classification)! > 9{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x\(classification)")!)
        }else{
            RequestData.append(hexa_to_8_Bit_NSDATA("0x0\(classification)")!)
        }
        while RequestData.length < 34
        {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func getFWFile(file: String) -> Data {
        var filedata = Data()
        let fileFirmware: String = file
        let pathFirmware: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/\(fileFirmware)"
        print("fileFirmware pathFirmware : \(pathFirmware)")
        if FileManager.default.fileExists(atPath: pathFirmware)
        {
            print("send PRINTER.BRN")
            do{
                filedata = try Data(contentsOf: URL(fileURLWithPath: pathFirmware))
                return filedata
            }
            catch let error {
                print("parse error: \(error.localizedDescription)")
                return filedata
            }
        }
        return filedata
    }
    
    func call_Api_EnterFirmwareUpgrad_ready(_ classification : String)
    {
        var filedata = Data()
        //Akshay Solve crash detect on crashanalitics Add try catch
        if classification == "00"{
            filedata = self.getFWFile(file: newFIRMfileName)
        }else if classification == "01"{
            filedata = self.getFWFile(file: newCNXfileName)
        }else if classification == "02"{
            filedata = self.getFWFile(file: newTMDfileName)
        }
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x02", "0x\(classification)"]
        RequestData = self.createBufferArray(bufferArray: array)
        RequestData.append(SessionData.sharedInstance().getfilesize_4(filedata) as Data)
        while RequestData.length < 34
        {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func call_Api_EnterFirmwareUpgrad_readyZIP(_ classification : String)
    {
        var filedata = Data()
        //change by akshay
        let fileFirmware: String = ZIP_FIRMWARE_FILE_NAME
        let pathFirmware: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/\(fileFirmware)"
        print("pathFirmware : \(pathFirmware)")
        if FileManager.default.fileExists(atPath: pathFirmware)
        {
            filedata = try! Data(contentsOf: URL(fileURLWithPath: pathFirmware))
        }
        if FileManager.default.fileExists(atPath: pathFirmware)
        {
            filedata = try! Data(contentsOf: URL(fileURLWithPath: pathFirmware))
        }
        var RequestData: NSMutableData = NSMutableData()
        let array = ["0x00", "0x00", "0x03", "0x00"]
        RequestData = self.createBufferArray(bufferArray: array)
        RequestData.append(SessionData.sharedInstance().getfilesize(filedata) as Data)
        //check for which file we send tmd or firmware
        if ZIP_SELECTED_FIRMWARE_FILE == "Firmware"
        {
            // firmware
            RequestData.append(hexa_to_8_Bit_NSDATA("0x02")!)
        }
        else{
            // tmd
            RequestData.append(hexa_to_8_Bit_NSDATA("0x01")!)
        }
        while RequestData.length < 16
        {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
}
