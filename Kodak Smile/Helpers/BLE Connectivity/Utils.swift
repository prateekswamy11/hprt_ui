//
//  Utils.swift
//  SnapTouchApp
//
//  Created by LaNet on 5/24/16.
//  Copyright © 2016 LaNet. All rights reserved.
//


import Foundation
import UIKit
import AssetsLibrary
import Photos
import PhotosUI

class Utils
{
    static func getColorByRGB(_ R: CGFloat, G: CGFloat, B: CGFloat) -> UIColor
    {
        return UIColor(red: R/255.0, green: G/255.0, blue: B/255.0, alpha: 1.0)
    }
    
    static func verifyEmail(_ email:String)->Bool
    {
        let ns:NSString = "[A-Za-z0-9\\.]+@[A-Za-z0-9]+\\.[A-Za-z.]{2,6}"
        let pr:NSPredicate = NSPredicate(format: "SELF MATCHES %@",ns)
        return pr.evaluate(with: email)
    }
    
    static func verifyText(_ text:String)->Bool
    {
        if(text != "" && !text.isEmpty)
        {
            return true;
        }
        return false
    }
    
    static func verifyText1(_ text:String)->Bool
    {
        let ns:NSString = "[A-Za-z ]+"
        let pr:NSPredicate = NSPredicate(format: "SELF MATCHES %@",ns)
        return pr.evaluate(with: text)
    }
    
    static func verifyNumber(_ txtNumber:String,length:Int)->Bool
    {
        let ns:NSString = "[0-9]{\(length)}" as NSString
        let pr:NSPredicate = NSPredicate(format: "SELF MATCHES %@",ns)
        return pr.evaluate(with: txtNumber)
    }
    
    static func AlertMessage(message msg :String)
    {
        let alert = UIAlertView(title: AppName, message: msg, delegate: self, cancelButtonTitle: lbl_OK)
        alert.show()
    }
    
    static func DisplayAlert(message msg : String , vc :UIViewController )
    {
        //vc.dismissViewControllerAnimated(true, completion: nil)
        let alert = UIAlertController(title: AppName, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: lbl_OK, style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        DispatchQueue.main.async(execute: {
            vc.present(alert, animated: true, completion: nil)
        })
    }
    
    static func saveNSDefaultObject(_ keyName:String,arr:NSMutableArray) {
        let data = NSKeyedArchiver.archivedData(withRootObject: arr)
        UserDefaults.standard.set(data, forKey: keyName)
        UserDefaults.standard.synchronize();
    }
    
    static func getArray(_ keyName:String) -> NSMutableArray? {
        let arr: NSMutableArray = NSMutableArray()
        if let data = UserDefaults.standard.object(forKey: keyName) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? NSMutableArray
        }
        return arr
    }
    
    static func getFullDate(_ dateString:String)-> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let date = dateFormatter.date(from: dateString)
        
        // dateFormatter.dateFormat = "yyyy - MMM  (EEEE - HH:mm a)"///this is you want to convert format
        dateFormatter.dateFormat = "EEEE, MMM dd "///this is you want to convert format
        
        return dateFormatter.string(from: date!)
    }
    
    func schemeAvailable(_ scheme: String) -> Bool {
        if let url = URL(string: scheme) {
            return UIApplication.shared.canOpenURL(url)
        }
        return false
    }
    
    static func startActivityIndicatorOnView(_ myview : UIView,centerPoint:CGPoint,style:UIActivityIndicatorViewStyle,color:UIColor?)
    {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle:style)
        activityIndicator.center = centerPoint
        if (color != nil)
        {
            activityIndicator.color = color
        }
        myview.addSubview(activityIndicator)
        activityIndicator.layer.zPosition = 1
        activityIndicator.startAnimating()
    }
    static func stopActivityIndicatorOnView(_ myview : UIView)
    {
        for tmpViews in myview.subviews
        {
            if (tmpViews.isKind(of: UIActivityIndicatorView.self))
            {
                let activityIndicator = tmpViews as! UIActivityIndicatorView
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
            }
        }
    }
    
    func convertTextToQRCode(_ text: String, withSize size: CGSize) -> UIImage {
        let data = text.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        let filter = CIFilter(name: "CIQRCodeGenerator")!
        filter.setValue(data, forKey: "inputMessage")
        filter.setValue("L", forKey: "inputCorrectionLevel")
        let qrcodeCIImage = filter.outputImage!
        let cgImage = CIContext(options:nil).createCGImage(qrcodeCIImage, from: qrcodeCIImage.extent)
        UIGraphicsBeginImageContext(CGSize(width: size.width * UIScreen.main.scale, height: size.height * UIScreen.main.scale))
        let context = UIGraphicsGetCurrentContext()
        context!.interpolationQuality = .none
        context!.draw(cgImage!, in: context!.boundingBoxOfClipPath)
        let preImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let qrCodeImage = UIImage(cgImage: preImage!.cgImage!, scale: 1.0/UIScreen.main.scale, orientation: .downMirrored)
        return qrCodeImage
    }
}

extension UIView {
    class func loadFromNibNamed(_ nibNamed: String, bundle : Bundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
}




extension UITextField
{
    func isEmpty()->Bool
    {
        let trimmedString = self.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if trimmedString!.characters.count == 0
        {
            return true
        }
        else
        {
            return false
        }
    }
}
