//
//  ConnectionStatus.swift
//  SnapTouchApp
//
//  Created by LaNet on 7/27/16.
//  Copyright © 2016 LaNet. All rights reserved.
//

import Foundation
import UIKit

@objc protocol PrinterStatusDelegate{
    @objc optional func printerStatusDisconnected()
    @objc optional func printerStatusConnected()
}

class ConnectionStatus:NSObject
{
    override init()
    {
        super.init()
    }
    static let sharedController = ConnectionStatus()
    
    var printerStatusDelegate:PrinterStatusDelegate?{
        didSet{
            NotificationCenter.default.removeObserver(self)
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(AccessoriesConnect),
                name: NSNotification.Name(rawValue: "AccessoryConnect"),
                object: nil)
            
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(AccessoriesDisConnect),
                name: NSNotification.Name(rawValue: "AccessoryDisConnect"),
                object: nil)
        }
    }
    
    @objc func AccessoriesConnect(_ dict : Notification)
    {
        
        if(dict.userInfo != nil)
        {
            let currAcesssory = dict.userInfo!["accessoryList"] as! EAAccessory
            appDelegate().currentAccessory = currAcesssory
            
            if(appDelegate().currentAccessory?.protocolStrings[0]  == MFI_PROTOCOL_STRING)
            {
                appDelegate().isZIPSNAPConnected = true
                appDelegate().isConnectedToPrinter = true
            }
            else
            {
                appDelegate().isConnectedToPrinter = true
                appDelegate().isConnectInProgress = false
                appDelegate().isPrintingError = false
                appDelegate().isZIPSNAPConnected = false
            }
            
            AppManager.shared.kodakManager.kodakDeviceFound(deviceID: CONNECT_DEVICE_MAC_ADDRESS)
        }
        
        //        appDelegate().isConnectedToPrinter = true
        appDelegate().isConnectInProgress = false
        print_Image_Loader_Flag = false
        if(appDelegate().currentAccessory != nil)
        {
            if(appDelegate().currentAccessory?.protocolStrings[0] == MFI_PROTOCOL_STRING)
            {
                appDelegate().isZIPSNAPConnected = true
                appDelegate().isConnectedToPrinter = true
                
            }
        }
        
        self.printerStatusDelegate!.printerStatusConnected?()
    }
    
    @objc func AccessoriesDisConnect()
    {
        UserDefaults.standard.set(false, forKey: "cancelFirmwarePopUP")
        PrinterQueueManager.RequestArray.removeAll()
        PrinterQueueManager.sharedController.Is_Busy = false
        
        appDelegate().isConnectedToPrinter = false
        appDelegate().isConnectInProgress = false
        appDelegate().isPrintingError = false
        appDelegate().currentAccessory = nil
        appDelegate().isPrintingInProgress = false
        //appDelegate().removeProgressView()
        
        print_Image_Loader_Flag = true
        self.printerStatusDelegate!.printerStatusDisconnected?()
        NotificationCenter.default.post(name: NSNotification.Name("AccessoryDisConnectAfterCallBack"), object: nil)
    }
}
