//
//  SessionData.h
//  PolaroidZip
//
//  Created by Lcom32 on 7/16/15.
//  Copyright (c) 2015 Lanet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EADSessionController.h"

@interface SessionData : NSObject
{
    NSMutableArray *_accessoryList;
    
    EAAccessory *_selectedAccessory;
    EADSessionController *_eaSessionController;
}
+ (instancetype)sharedInstance;
-(void)setupSessionManager;
- (NSMutableData *)getimagesize:(UIImage *)Image;
- (NSMutableData *)getTestize:(NSString *)Device_Name;
- (NSMutableData *)getfilesize:(NSData *)filedata;
- (NSMutableData *)getfilesize_4:(NSData *)filedata;
-(void)setupSesssion:(EAAccessory *)_Accessory;
@property (nonatomic, strong) NSMutableData *imageData;
@property (nonatomic, strong) NSMutableData *RequestData;

@end
