//
//  AppConstants.swift
//

import Foundation
import UIKit

var ZIP_SELECTED_FIRMWARE_FILE = String()
var ZIP_FIRMWARE_FILE_NAME = String()
var numberOfColumnsInGrid : CGFloat = 2
var selectedGridCellIndex = -1
var First_Time_Check_Camera_Status = 0
var isSortByOldestFirst = Bool()
var is_Showing_Social_Photos = false
var array_Social_PhotoUrl = [String]()
var status_Bar_Hidden : Bool = true
var is_Force_Firmware_Available = ""
var is_Firmware_PopUp_Shown = false            // Variable to show firmware available popup once as, the response for firmware comes twice.
var IS_FORCE_UPDATE = String()
//Hide printing loader on PrintLoaderVC
var print_Image_Loader_Flag = false

//Save imageScrollView in collage

//var selected_image_Scroll_Views = ImageScrollView()

let No_Accessory_Found = "No Device Found. \n Make sure Bluetooth is turned on in your camera's settings."

let Seleted_Image_Count = "You can only print 1 images"
let No_Seleted_Image = "Please select at least one image"
// Live URL "http://polaroidzip.us-east-2.elasticbeanstalk.com/"
// Test URL

var unique_user_id = UIDevice.current.identifierForVendor?.uuidString ?? "1234"

var newCNXfileName = String()
var newFIRMfileName = String()
var newTMDfileName = String()

//C_RATHOD
func hexa_to_8_Bit_NSDATA(_ hexa:String) -> Data?{
    if let bitSeries = hexa_to_8_Bit(hexa)
    {
        var value: UInt = strtoul(bitSeries.utf8String, nil, 2)
        //print("value : \(value)")
        //MARK:- error solve change by maximess
        //return Data(bytes: UnsafePointer<UInt8>(&value), count: 1)
        var data1 = Data(buffer: UnsafeBufferPointer(start: &value, count: 1))
        //data.copyBytes(to: UInt8(&value), count: data!.count)
        //var data = data1.withUnsafeBytes {
        // [UInt32](UnsafeBufferPointer(start: $0, count: 1))
        //}
        //Convert NSString to NSData
        let myNSData = bitSeries.data(using: String.Encoding.utf8.rawValue)!

        //Convert NSData to [UInt8] array
        let myArray = [UInt8](myNSData)

        //Convert [UInt8] to NSData
        let resultNSData = NSData(bytes: myArray, length: myArray.count)
        let data = NSData(bytes: &value, length: 1)
        //print("data : \(data as Data)")
        return data as Data  //as NSData

    }else{
        return nil
    }
}

//PrintPreview
var arrAwardImg = NSMutableArray()
var arrAwardTitle = NSMutableArray()
var arrAwardNoOfAward = NSMutableArray()


func hexa_to_8_Bit(_ hexa:String) -> NSString?{
    var retuStr = ""
    for i in hexa.replacingOccurrences(of: "0x", with: "", options: NSString.CompareOptions.literal, range: nil).characters{
        if var int_hexa = Int("\(i)", radix: 16){
            var data = ""
            while int_hexa != 0 {
                if int_hexa % 2 == 0{
                    data = "0\(data)"
                }else{
                    data = "1\(data)"
                }
                int_hexa /= 2
            }
            for _ in data.characters.count  ..< 4 {
                data = "0\(data)"
            }
            retuStr = "\(retuStr)\(data)"
            //print("hexa_to_8_Bit \(retuStr)")
        }else{
            return nil
        }
    }
    return retuStr as NSString
}
//C_RATHOD

func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

func doOnMainQueue(_ closure:@escaping ()->()) {
    DispatchQueue.main.async(execute: closure)
}

func appDelegate() -> AppDelegate {
    return (UIApplication.shared.delegate as! AppDelegate)
}


enum PRINT_TYPE: Int {
    case mosaic_AWARD = 0
    case secret_VIEW_AWARD = 1
    case weekend_AWARD = 2
    case midnight_AWARD = 3
    case sunrise_AWARD_ = 4
    case photobooth_AWARD = 5
    case printed_PICTURES_AWARD = 6
    case burst_PICTURES_AWARD = 7
    case first_UPA_CONNECTED = 8
    case new_BORDER_STICKERS_AWARD = 9
    case eleven_TH_PHOTO_AWARD = 10
}

enum BROWSE_TYPE: Int {
    case about = 1
    case legal = 2
    case support = 3
}

typealias FinishedHandler = () -> Void
typealias SuccessHandler = (_ result: AnyObject) -> Void
typealias ErrorHandler = (_ result: AnyObject, _ error: NSError) -> Void
typealias CompletionHandler = (_ success: Bool) -> Void

let UTF8StringEncoding: UInt = 4
//let AppName = "Polaroid Print App"
//if(appDelegate().isZIPSNAPConnected == true)
//{
//    let AppName = "Polaroid Print App - ZIP"
//
//}
//else
//{
//    let AppName = "Polaroid Print App"
//}

/*
let AppName: String = {
    if appDelegate().isZIPSNAPConnected == true {
        return "Polaroid Print App - ZIP"
    } else {
        return "Polaroid Print App"
    }
}()
 */

//MARK : URL
let WEBURL = ""
let Domain = "http://54.208.19.36/"
//let Domain = "http://polaroidzip.us-east-2.elasticbeanstalk.com/"
//let Domain = "http://192.168.200.67/snaptouch/"

//MARK:- Preference Keys
let kUserName: String = "userName"
let kProfileId: String = "ProfileId"
let kProfileFullName: String = "ProfileFullName"
let kProfileUrl: String = "ProfileUrl"
let kPushDeviceToken:String = "deviceToken"
let kAuthenticationToken = "authenticationToken"


//MARK:- Interent related message
let kInternetNotConnected = "Please check your internet connection"

let kInternetGone = "InternetGone"
let kInternetAvailable = "InternetAvailable"
let kInternetAlert = "Lost Network Connection, Try Again Later"
let kLocationAlert = "Not Able to track your Location, Please Enable Location from settings"
let kappDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)

//MARK:- Registration error msg

let kErrEmail = "Please enter proper email address."
let kErrFeilds = "Please fill all required feilds."
let kErrPassMatched = "Password and Conform Password do not matched."
let kErrPasslength = "Password must be more then 6 characters."
let kErrName = "Please enter proper name."

let kisSaveRewardData = "saveRewardData"


#if (arch(i386) || arch(x86_64)) && os(iOS)
let DEVICE_IS_SIMULATOR = true
#else
let DEVICE_IS_SIMULATOR = false
#endif
//FBLogin
var flgInternetChk : Bool = false
struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}
struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
}

let TabBG = UIColor(red: 0.0/255.0, green: 176.0/255.0, blue:171.0/255.0, alpha: 1.0)
let TabSelectedColor = UIColor(red: 6.0/255.0, green: 97.0/255.0, blue: 170.0/255.0, alpha: 1.0)
let TopViewColor = UIColor(red: 240.0/255.0, green: 149/255.0, blue: 28/255.0, alpha: 1.0)
let appTextPurpleColor = UIColor(red: 125.0/255.0, green: 135.0/255.0, blue: 226.0/255.0, alpha: 1.0)
//MARK:- error solve change by maximess

let placeHolderImg : UIImage = UIImage(imageLiteralResourceName: "logo")

let AppName: String = NSLocalizedString("Alert", comment: "")

let Lbl_Cancel = NSLocalizedString("Cancel", comment: "")//"Cancel".localized_CLS()

let lbl_UPDATES = NSLocalizedString("UPDATES", comment: "")//"UPDATES".localized_CLS()

let lbl_FIRMWARE_VERSION = NSLocalizedString("FIRMWARE_VERSION", comment: "")//"FIRMWARE_VERSION".localized_CLS()

let lbl_TMD_VERSION = NSLocalizedString("TMD_VERSION", comment: "")//"TMD_VERSION".localized_CLS()
let btn_INSTALL_UPDATES = NSLocalizedString("INSTALL_UPDATES", comment: "")// "INSTALL_UPDATES".localized_CLS()
let lbl_MAKE_SURE_BLUETOOTH_ON = NSLocalizedString("MAKE_SURE_BLUETOOTH_ON", comment: "")// "MAKE_SURE_BLUETOOTH_ON".localized_CLS()

let btn_SYNC = NSLocalizedString("SYNC", comment: "")//"SYNC".localized_CLS()

let lbl_OK = NSLocalizedString("OK", comment: "")//"OK".localized_CLS()

let lbl_CONEXANT_VERSION = NSLocalizedString("CONEXANT_VERSION", comment: "")// "CONEXANT_VERSION".localized_CLS() //
let lbl_PRINTING = NSLocalizedString("PRINTING", comment: "")//"PRINTING".localized_CLS()

let lbl_No_ACESSORY_CONNECTED = NSLocalizedString("NO_ACCESORY_CONNECT", comment: "")// "NO_ACCESORY_CONNECT".localized_CLS()

let lbl_IMG_ADDED_PRINT_QUE = NSLocalizedString("IMG_ADDED_PRINT_QUE", comment: "")// "IMG_ADDED_PRINT_QUE".localized_CLS()

//PrintQueueManager
let lbl_Image_Processing_Error = NSLocalizedString("Image_Processing_Error", comment: "")// "Image_Processing_Error".localized_CLS()
let lbl_Printer_is_Busy = NSLocalizedString("Printer_is_Busy", comment: "")// "Printer_is_Busy".localized_CLS()
let lbl_Paper_Jam = NSLocalizedString("Paper_Jam", comment: "")//"Paper_Jam".localized_CLS()
let lbl_Paper_Empty = NSLocalizedString("Paper_Empty", comment: "")//"Paper_Empty".localized_CLS()
let lbl_Paper_Mismatch = NSLocalizedString("Paper_Mismatch", comment: "")//"Paper_Mismatch".localized_CLS()
let lbl_Data_Error_Please_Resend_again = NSLocalizedString("Data_Error_Please_Resend_again", comment: "")// "Data_Error_Please_Resend_again".localized_CLS()
let lbl_Cover_open = NSLocalizedString("Cover_open", comment: "")//"Cover_open".localized_CLS()
let lbl_System_Error_Please_reboot = NSLocalizedString("System_Error_Please_reboot", comment: "")// "System_Error_Please_reboot".localized_CLS()
let lbl_Battery_Low = NSLocalizedString("Battery_Low", comment: "")//"Battery_Low".localized_CLS()
let lbl_Battery_Fault = NSLocalizedString("Battery_Fault", comment: "")//"Battery_Fault".localized_CLS()
let lbl_High_Temperature = NSLocalizedString("High_Temperature", comment: "")// "High_Temperature".localized_CLS()
let lbl_Low_Temperature = NSLocalizedString("Low_Temperature", comment: "")// "Low_Temperature".localized_CLS()
let lbl_Cooling_Mode = NSLocalizedString("Cooling_Mode", comment: "")//"Cooling_Mode".localized_CLS()
let lbl_Trasfer_Cancel = NSLocalizedString("Trasfer_Cancel", comment: "")//"Trasfer_Cancel".localized_CLS()
let lbl_Wrong_Customer = NSLocalizedString("Wrong_Customer", comment: "")//"Wrong_Customer".localized_CLS()
let lbl_Paper_Misfeed = NSLocalizedString("Paper_Misfeed", comment: "")//"Wrong_Customer".localized_CLS()
let lbl_Out_of_Paper = NSLocalizedString("Out_of_Paper", comment: "")//"Out_of_Paper".localized_CLS()
let lbl_Total_Prints = NSLocalizedString("Total_Prints", comment: "")//"Total_Prints".localized_CLS()

