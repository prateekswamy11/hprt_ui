//
//  PrintData.m
//  PolaroidZip
//
//  Created by Lcom32 on 7/15/15.
//  Copyright (c) 2015 Lanet. All rights reserved.
//

#import "PrintData.h"
#import "EADSessionController.h"
#import "SessionData.h"

@implementation PrintData

- (NSData *)PrintImage:(NSData *)image
{
//    NSMutableData* imagedata = [UIImageJPEGRepresentation(image, 0.0) mutableCopy];
    
    NSMutableData* imagedata = (NSMutableData *)image;
    NSMutableData *RequestData=[[NSMutableData alloc]init];
    
    NSString *bitSeries = @"";
    NSData *data = [[NSData alloc]init];
    //    1
    bitSeries = @"00011011";
    data=[self stringToData:bitSeries];
    //NSLog(@"1 : %@", data);
    [RequestData appendData:data];
    //    2
    bitSeries = @"00101010";
    data=[self stringToData:bitSeries];
    //NSLog(@"2 : %@", data);
    [RequestData appendData:data];
    //    3
    bitSeries = @"01000011";
    data=[self stringToData:bitSeries];
    //NSLog(@"3 : %@", data);
    [RequestData appendData:data];
    //    4
    bitSeries = @"01000001";
    data=[self stringToData:bitSeries];
    //NSLog(@"4 : %@", data);
    [RequestData appendData:data];
    //    5
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"5 : %@", data);
    [RequestData appendData:data];
    //    6
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"6 : %@", data);
    [RequestData appendData:data];
    //    7
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"7 : %@", data);
    [RequestData appendData:data];
    //    8
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"8 : %@", data);
    [RequestData appendData:data];
    //    9
    
    NSMutableArray *arrstr = [self intToBinary:(int)[imagedata length]];
    
    bitSeries = [arrstr objectAtIndex:0];
    data=[self stringToData:bitSeries];
    //NSLog(@"9 : %@", data);
    [RequestData appendData:data];
    
    bitSeries = [arrstr objectAtIndex:1];
    data=[self stringToData:bitSeries];
    //NSLog(@"9 : %@", data);
    [RequestData appendData:data];
    
    bitSeries =[arrstr objectAtIndex:2];
    data=[self stringToData:bitSeries];
    //NSLog(@"9 : %@", data);
    [RequestData appendData:data];
    
    //    10
    bitSeries = @"00000001";
    data=[self stringToData:bitSeries];
    //NSLog(@"10 : %@", data);
    [RequestData appendData:data];
    //    11
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"11 : %@", data);
    [RequestData appendData:data];
    //    12
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"12 : %@", data);
    [RequestData appendData:data];
    
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"12 : %@", data);
    [RequestData appendData:data];
    
    //    13
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"13 : %@", data);
    [RequestData appendData:data];
    
    //NSLog(@"Final Data is : %@", RequestData);
    
    return RequestData;
    
//    if (imagedata !=nil)
//    {
//        SessionData *obj=[SessionData sharedInstance];
//        obj.imageData=imagedata;
//        obj.RequestData = RequestData;
//        [obj setupSessionManager];
//    }
//    else
//    {
//        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Select Image." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
//    }
    
    //ana commemted
    
    
    
    
//    EADSessionController *sessionController = [EADSessionController sharedController];
//    [sessionController openSession];
//    
//    sessionController.imageData=imagedata;
//    [sessionController writeData:finalData];
    
    
}

-(NSData *)stringToData : (NSString *)bitSeries
{
    uint8_t value = strtoul([bitSeries UTF8String], NULL, 2);
    //NSLog(@"bitSeries : %@ -------> Value : %u",bitSeries, value);
    NSData *data = [NSData dataWithBytes:&value length:sizeof(value)];
    return data;
}

- (NSMutableArray *)intToBinary:(int)number
{
    int bits =  sizeof(number) * 8;
    int bitcnt=bits;
    
    NSMutableArray *arrStr=[[NSMutableArray alloc]init];
    NSMutableString *binaryStr = [NSMutableString string];
    
    for (; bits > 0; bits--, number >>= 1)
    {
        [binaryStr insertString:((number & 1) ? @"1" : @"0") atIndex:0];
    }
    
    if (bitcnt == 32)
    {
        binaryStr=[[binaryStr substringFromIndex:8] mutableCopy];
    }
    else if(bitcnt ==16)
    {
        binaryStr = [[@"00000000" stringByAppendingString:binaryStr] mutableCopy];
    }
    else if (bitcnt == 8)
    {
        binaryStr = [[@"0000000000000000" stringByAppendingString:binaryStr] mutableCopy];
    }
    [arrStr addObject:[binaryStr substringToIndex:8]];
    [arrStr addObject:[binaryStr substringWithRange:NSMakeRange(8, 8)]];
    [arrStr addObject:[binaryStr substringFromIndex:16]];
    
    //NSLog(@"%@",arrStr);
    
    return arrStr;
}



@end
