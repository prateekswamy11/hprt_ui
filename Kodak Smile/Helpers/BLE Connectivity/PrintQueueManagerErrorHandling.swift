//
//  PrintQueueManagerErrorHandling.swift
//  Kodak Smile
//
//  Created by maximess152 on 22/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation

extension PrinterQueueManager {
    func handleErrorCodes(errorCode: String) {
        var str_msg = ""
        if(errorCode == "0x00")
        {
            str_msg = lbl_Image_Processing_Error //"Image Processing Error";
            errorDelegate?.receivedPrintingError(errorCode: 0)
        }
        if(errorCode == "0x01")
        {
            str_msg = lbl_Printer_is_Busy //"Printer is Busy";
            self.getErroFromPrinter(message: lbl_Printer_is_Busy, errorCode: 1)
        }
        else if(errorCode == "0x02")
        {
            checkCommunicationHiccup = true
            str_msg = lbl_Paper_Jam //"Paper Jam";
            self.getErroFromPrinter(message: lbl_Paper_Jam, errorCode: 2)
        }
        else if(errorCode == "0x03")
        {
            str_msg = lbl_Paper_Empty // "Paper Empty";
            self.getErroFromPrinter(message: lbl_Paper_Empty, errorCode: 3)
        }
        else if(errorCode == "0x04")
        {
            checkCommunicationHiccup = true
            str_msg = lbl_Paper_Mismatch //"Paper Mismatch";
            self.getErroFromPrinter(message: lbl_Paper_Mismatch, errorCode: 4)
        }
        else if(errorCode == "0x05")
        {
            str_msg = lbl_Data_Error_Please_Resend_again //"Data Error \n Please Resend again.";
            self.getErroFromPrinter(message: lbl_Data_Error_Please_Resend_again, errorCode: 5)
        }
        else if(errorCode == "0x06")
        {
            str_msg = lbl_Cover_open //"Cover open";
            self.getErroFromPrinter(message: lbl_Cover_open, errorCode: 6)
        }
        else if(errorCode == "0x07")
        {
            str_msg = lbl_System_Error_Please_reboot //"System Error.\n Please reboot the Printer";
            self.getErroFromPrinter(message: lbl_System_Error_Please_reboot, errorCode: 7)
        }
        else if(errorCode == "0x08")
        {
            str_msg = lbl_Battery_Low  //"Battery Low";
            self.getErroFromPrinter(message: lbl_Battery_Low, errorCode: 8)
        }
        else if(errorCode == "0x09")
        {
            str_msg = lbl_Battery_Fault //"Battery Fault";
            self.getErroFromPrinter(message: lbl_Battery_Fault, errorCode: 9)
        }
        else if(errorCode == "0x0A") || (errorCode == "0x0a")
        {
            str_msg = lbl_High_Temperature   //"High Temperature";
            self.getErroFromPrinter(message: lbl_High_Temperature, errorCode: 10)
        }
        else if(errorCode == "0x0B") || (errorCode == "0x0b")
        {
            str_msg = lbl_Low_Temperature //"Low Temperature";
            self.getErroFromPrinter(message: lbl_Low_Temperature, errorCode: 10)
        }
        else if(errorCode == "0x0C") || (errorCode == "0x0c")
        {
            str_msg =  lbl_Cooling_Mode //"Cooling Mode";
            self.getErroFromPrinter(message: lbl_Cooling_Mode, errorCode: 10)
        }
        else if(errorCode == "0x0D") || (errorCode == "0x0d")
        {
            str_msg = lbl_Trasfer_Cancel //"Trasfer Cancel";
            self.getErroFromPrinter(message: lbl_Trasfer_Cancel, errorCode: 13)
        }
        else if(errorCode == "0x0E")  || (errorCode == "0x0e")
        {
            str_msg = lbl_Wrong_Customer  //"Wrong Customer";
            self.getErroFromPrinter(message: lbl_Wrong_Customer, errorCode: 14)
        }
        else if(errorCode == "0x0F")  || (errorCode == "0x0f")
        {
            checkCommunicationHiccup = true
            str_msg = lbl_Paper_Misfeed  //"Paper Misfeed";
            self.getErroFromPrinter(message: lbl_Paper_Misfeed, errorCode: 15)
        }
        if self.obj_RequestOperation.type == RequestType.printImage{
            self.obj_RequestOperation.PrintResponseDelegate?.ErrorAndMessageAck_Print(errorCode, Msg: str_msg)
        }else{
            self.obj_RequestOperation.ResponseDelegate.ErrorAndMessageAck?(errorCode, Msg: str_msg)
        }
        // Added by Pratiksha - As printing error is occurred, change the variable status
        appDelegate().isPrintingInProgress = false
        Is_Busy = false
        NotificationCenter.default.post(name: Notification.Name(rawValue: "PrintFinished"), object:nil);
        PrinterQueueManager.RequestArray.removeAll()
    }
    
    //MARK - changes by maximess date - 4 Oct 2017
    func send_request()
    {
        let array = PrinterQueueManager.RequestArray
        print(" PrinterQueueManager.RequestArray.count :  \(PrinterQueueManager.RequestArray.count)  Is_Busy  : \(Is_Busy)")
        if PrinterQueueManager.RequestArray.count > 0 && Is_Busy == false
        {
            EADSessionController.shared().delegate = self
            Is_Busy = true
            PrinterQueueManager.CurrentRequest = PrinterQueueManager.RequestArray[0]
            obj_RequestOperation = PrinterQueueManager.RequestArray[0]
            switch(obj_RequestOperation.type) {
            case RequestType.getBetteryLevel:
                self.call_Api_Printer_Bettery_level()
            case RequestType.sendDeviceName:
                if !appDelegate().isConnectedToPrinter
                {
                    self.call_Api_Send_Device_Name(obj_RequestOperation.DeviceName!)
                }
            case RequestType.getZIPInfo, RequestType.get_ZIP_FIRMWARE, RequestType.get_ZIP_INFO, RequestType.getVersion:
                self.call_Api_Get_ZIP_Firmware_TMD_Version()
            case RequestType.updateZIPFirmware:
                self.call_Api_EnterFirmwareUpgrad_readyZIP("")
            case RequestType.printImage:
                if appDelegate().isConnectedToPrinter
                {
                    // self.PrintImageForZIPSnap(obj_RequestOperation.img!, Copy: obj_RequestOperation.no_Of_Copy!)
                    if let imgData = obj_RequestOperation.img{
                        self.PrintImageForZIPSnap(imgData , Copy: obj_RequestOperation.no_Of_Copy!)
                    }
                }
                else
                {
                    //                    self.call_Api_Print_Image(obj_RequestOperation.img!, Copy: obj_RequestOperation.no_Of_Copy!)
                    if let imgData = obj_RequestOperation.img{
                        self.PrintImageForZIPSnap(imgData , Copy: obj_RequestOperation.no_Of_Copy!)
                    }
                }
            case RequestType.getSticker:
                self.call_Api_Get_Sticker()
            case RequestType.addSticker:
                self.call_Api_Update_Sticker(obj_RequestOperation.oldStickerId!, newStickerId: obj_RequestOperation.newStickerId!,count: obj_RequestOperation.imgSticketDict.count)
            case RequestType.getBorder:
                self.call_Api_Get_Border()
            case RequestType.addBorder:
                self.call_Api_Update_Border(obj_RequestOperation.oldBorderId!, newBorderId: obj_RequestOperation.newBorderId!, count: obj_RequestOperation.imgBorderDict.count)
            case RequestType.printCount:
                self.call_Api_Get_Print_Count()
            case RequestType.updateVersion:
                self.calculate_pass_data((obj_RequestOperation.UpdateVersionCode)!)
                self.call_Api_EnterFirmwareUpdatePage((obj_RequestOperation.UpdateVersionCode)!)
            case RequestType.getPageType:
                self.call_Api_Get_Page_Type()
            case RequestType.getPowerOffStatus:
                self.call_api_for_get_power_off()
            }
            UserDefaults.standard.set(10, forKey: "last_responce_time")
            UserDefaults.standard.synchronize()
            if (!self.timer.isValid){
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PrinterQueueManager.Timer_ticker), userInfo: nil, repeats: true)
            }
        }
    }
    
    func addTimerForZIPRequest()
    {
        Zip_Timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PrinterQueueManager.ZIP_Timer_ticker), userInfo: nil, repeats: true)
    }
    
    //    ======================================================== Bettery Level    ========================================================        //
    
    func call_Api_Printer_Bettery_level() {
        
        let RequestData: NSMutableData = NSMutableData()
        
        RequestData.append(hexa_to_8_Bit_NSDATA("0x1B")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x2A")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x43")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x41")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x0E")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        
        while RequestData.length < 34 {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    //    ======================================================== Send Devaice Name    ========================================================        //
    
    func call_Api_Send_Device_Name(_ name:String) {
        print("name : \(name)")
        let RequestData: NSMutableData = NSMutableData()
        
        RequestData.append(hexa_to_8_Bit_NSDATA("0x1B")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x2A")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x43")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x41")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x08")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        
        print("name : \(name)")
        print("SessionData.sharedInstance().getTestize(name) : \(SessionData.sharedInstance().getTestize(name))")
        RequestData.append(SessionData.sharedInstance().getTestize(name) as Data)
        
        while RequestData.length < 34 {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
        
    }
    
    func send_devicename(_ name:String) {
        let RequestData: NSMutableData = NSMutableData()
        RequestData.append(name.data(using: String.Encoding.utf8)!)
        
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func call_Api_Get_Page_Type()
    {
        let RequestData: NSMutableData = NSMutableData()
        RequestData.append(hexa_to_8_Bit_NSDATA("0x1B")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x2A")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x43")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x41")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x0D")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        while RequestData.length < 34
        {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func call_api_for_get_power_off()
    {
        let RequestData: NSMutableData = NSMutableData()
        RequestData.append(hexa_to_8_Bit_NSDATA("0x1B")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x2A")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x43")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x41")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x10")!)
        RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        while RequestData.length < 34
        {
            RequestData.append(hexa_to_8_Bit_NSDATA("0x00")!)
        }
        EADSessionController.shared().delegate = self
        EADSessionController.shared().write(RequestData as Data!)
    }
    
    func calculate_pass_data(_ SubCMDID : String) {
        self.upload_zips_file_data.removeAll()
        if SubCMDID == "00"{
            self.upload_zips_file_data.append("00")
        }else if SubCMDID == "01"{
            self.upload_zips_file_data.append("01")
        }else if SubCMDID == "02"{
            self.upload_zips_file_data.append("02")
        }else if SubCMDID == "03"{
            self.upload_zips_file_data.append("00")
            self.upload_zips_file_data.append("01")
        }else if SubCMDID == "04"{
            self.upload_zips_file_data.append("00")
            self.upload_zips_file_data.append("02")
        }else if SubCMDID == "05"{
            self.upload_zips_file_data.append("01")
            self.upload_zips_file_data.append("02")
        }else if SubCMDID == "06"{
            self.upload_zips_file_data.append("00")
            self.upload_zips_file_data.append("01")
            self.upload_zips_file_data.append("02")
        }
    }
    
    //decimal to binary convertor
    
    func decimalToBinaryConvertor(_ numb : String, toSize: Int) -> Data {
        let decimalToBinary =  numb.decimalToBinary
        var padded = decimalToBinary
        for  _ in 0..<toSize - (decimalToBinary.characters.count) {
            padded = "0" + padded
        }
        let strBinary : NSString =  NSString(string: padded)
        var value: UInt = strtoul(strBinary.utf8String, nil, 2)
        //MARK:- error solve change by maximess
        //return Data(bytes: UnsafePointer<UInt8>(&value), count: 1)
        return Data(bytes: &value,
                    count: MemoryLayout.size(ofValue: value))
    }
}
