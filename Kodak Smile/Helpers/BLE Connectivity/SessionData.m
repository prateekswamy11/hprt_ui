//
//  SessionData.m
//  PolaroidZip
//
//  Created by Lcom32 on 7/16/15.
//  Copyright (c) 2015 Lanet. All rights reserved.
//


//#import "PolaroidPrintApp-Swift.h"
//#import "SnapTouchApp-Swift.h"
#import "SessionData.h"
//#import "CoreData.h"
//#import "Tbl_devices.h"

@implementation SessionData

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void)setupSessionManager
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_accessoryDidConnect:) name:EAAccessoryDidConnectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_accessoryDidDisconnect:) name:EAAccessoryDidDisconnectNotification object:nil];
    [[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
    
    [[EADSessionController sharedController] closeSession];
    _eaSessionController =[EADSessionController sharedController];
    _accessoryList = [[NSMutableArray alloc] initWithArray:[[EAAccessoryManager sharedAccessoryManager] connectedAccessories]];
    
     _accessoryList = [[NSMutableArray alloc] initWithArray:[[EAAccessoryManager sharedAccessoryManager] connectedAccessories]];
    
    if ([_accessoryList count] == 0)
    {
       // ObjectiveCLocalization *obj = [[ObjectiveCLocalization alloc] init];

        NSLog(@"No Accessories connected.");
        //[[[UIAlertView alloc]initWithTitle:obj.applicationName message:obj.lbl_no_connected_acessory delegate:nil cancelButtonTitle:obj.lb_OK otherButtonTitles:nil, nil]show];

    }
    else
    {
        //NSLog(@"Accessories connected.");
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        dict[@"accessoryList"] = _accessoryList;
        //CoreData *data = [[CoreData alloc]init];
        //[data updateCountOfPrinting:8 withPrintCount:1];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"AccessoryList" object:self userInfo:dict];

//        [self setupSesssion];
        
    }
    
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//    dict[@"accessoryList"] = _accessoryList;
//    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"AccessoryConnect" object:self userInfo:dict];
}

-(void)request_send_devicename{
    NSMutableData *RequestData=[[NSMutableData alloc]initWithCapacity:34];
    
    NSString *bitSeries = @"";
    NSData *data = [[NSData alloc]init];
    //    1
    bitSeries = @"00011011";
    data=[self stringToData:bitSeries];
    //NSLog(@"1 : %@", data);
    [RequestData appendData:data];
    //    2
    bitSeries = @"00101010";
    data=[self stringToData:bitSeries];
    //NSLog(@"2 : %@", data);
    [RequestData appendData:data];
    //    3
    bitSeries = @"01000011";
    data=[self stringToData:bitSeries];
    //NSLog(@"3 : %@", data);
    [RequestData appendData:data];
    //    4
    bitSeries = @"01000001";
    data=[self stringToData:bitSeries];
    //NSLog(@"4 : %@", data);
    [RequestData appendData:data];
    //    5
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"5 : %@", data);
    [RequestData appendData:data];
    //    6
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"6 : %@", data);
    [RequestData appendData:data];
    //    7
    bitSeries = @"00001000";
    data=[self stringToData:bitSeries];
    //NSLog(@"7 : %@", data);
    [RequestData appendData:data];
    //    8
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"8 : %@", data);
    [RequestData appendData:data];
    
    NSString *Device_Name  = @"test ios device name";
    NSData *data1 = [Device_Name dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableData *imagedata = [data1 mutableCopy];
    
    NSMutableArray *arrstr = [self intToBinary2:(int)[imagedata length]];
    
    bitSeries = [arrstr objectAtIndex:0];
    data=[self stringToData:bitSeries];
    //NSLog(@"9 : %@", data);
    [RequestData appendData:data];
    
    bitSeries = [arrstr objectAtIndex:1];
    data=[self stringToData:bitSeries];
    //NSLog(@"9 : %@", data);
    [RequestData appendData:data];
    
    bitSeries =[arrstr objectAtIndex:2];
    data=[self stringToData:bitSeries];
    //NSLog(@"9 : %@", data);
    [RequestData appendData:data];
    
    while (RequestData.length < 34) {
        bitSeries = @"00000000";
        data=[self stringToData:bitSeries];
        [RequestData appendData:data];
    }
    
    EADSessionController *sessionController = [EADSessionController sharedController];
    [sessionController writeData:RequestData];
    
}

-(NSData *)stringToData : (NSString *)bitSeries
{
    uint8_t value = strtoul([bitSeries UTF8String], NULL, 2);
    //NSLog(@"bitSeries : %@ -------> Value : %u",bitSeries, value);
    NSData *data = [NSData dataWithBytes:&value length:sizeof(value)];
    return data;
}

- (NSMutableData *)getimagesize:(UIImage *)Image
{
    
    NSMutableData *RequestData=[[NSMutableData alloc]init];
    NSString *bitSeries = @"";
    NSData *data = [[NSData alloc]init];
    
    
    NSMutableData* imagedata = [UIImageJPEGRepresentation(Image, 1.0) mutableCopy];
    
    NSMutableArray *arrstr = [self intToBinary2:(int)[imagedata length]];
    
    bitSeries = [arrstr objectAtIndex:0];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    bitSeries = [arrstr objectAtIndex:1];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    bitSeries =[arrstr objectAtIndex:2];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    return RequestData;
    
}
- (NSMutableData *)getfilesize:(NSData *)filedata
{
    
    NSMutableData *RequestData=[[NSMutableData alloc]init];
    NSString *bitSeries = @"";
    NSData *data = [[NSData alloc]init];
    
    NSMutableArray *arrstr = [self intToBinary2:(int)[filedata length]];
    
    bitSeries = [arrstr objectAtIndex:0];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    bitSeries = [arrstr objectAtIndex:1];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    bitSeries =[arrstr objectAtIndex:2];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    return RequestData;
    
}
- (NSMutableData *)getfilesize_4:(NSData *)filedata
{
    
    NSMutableData *RequestData=[[NSMutableData alloc]init];
    NSString *bitSeries = @"";
    NSData *data = [[NSData alloc]init];
    
    NSMutableArray *arrstr = [self intToBinary2_4:(int)[filedata length]];
    
    bitSeries = [arrstr objectAtIndex:0];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    bitSeries = [arrstr objectAtIndex:1];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    bitSeries =[arrstr objectAtIndex:2];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    bitSeries =[arrstr objectAtIndex:3];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    return RequestData;
    
}
- (NSMutableData *)getTestize:(NSString *)Device_Name
{
    
    NSMutableData *RequestData=[[NSMutableData alloc]init];
    NSString *bitSeries = @"";
    NSData *data = [[NSData alloc]init];
    
    
    NSData *data1 = [Device_Name dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableData *imagedata = [data1 mutableCopy];
    
    NSMutableArray *arrstr = [self intToBinary2:(int)[imagedata length]];
    
    bitSeries = [arrstr objectAtIndex:0];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    bitSeries = [arrstr objectAtIndex:1];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    bitSeries =[arrstr objectAtIndex:2];
    data=[self stringToData:bitSeries];
    [RequestData appendData:data];
    
    return RequestData;
    
}
- (NSMutableArray *)intToBinary2:(int)number
{
    int bits =  sizeof(number) * 8;
    int bitcnt=bits;
    
    NSMutableArray *arrStr=[[NSMutableArray alloc]init];
    NSMutableString *binaryStr = [NSMutableString string];
    
    for (; bits > 0; bits--, number >>= 1)
    {
        [binaryStr insertString:((number & 1) ? @"1" : @"0") atIndex:0];
    }
    
    if (bitcnt == 32)
    {
        binaryStr=[[binaryStr substringFromIndex:8] mutableCopy];
    }
    else if(bitcnt ==16)
    {
        binaryStr = [[@"00000000" stringByAppendingString:binaryStr] mutableCopy];
    }
    else if (bitcnt == 8)
    {
        binaryStr = [[@"0000000000000000" stringByAppendingString:binaryStr] mutableCopy];
    }
    [arrStr addObject:[binaryStr substringToIndex:8]];
    [arrStr addObject:[binaryStr substringWithRange:NSMakeRange(8, 8)]];
    [arrStr addObject:[binaryStr substringFromIndex:16]];
    
    //NSLog(@"%@",arrStr);
    
    return arrStr;
}
- (NSMutableArray *)intToBinary2_4:(int)number
{
    int bits =  sizeof(number) * 8;
    int bitcnt=bits;
    
    NSMutableArray *arrStr=[[NSMutableArray alloc]init];
    NSMutableString *binaryStr = [NSMutableString string];
    
    for (; bits > 0; bits--, number >>= 1)
    {
        [binaryStr insertString:((number & 1) ? @"1" : @"0") atIndex:0];
    }
    
    if (bitcnt == 32)
    {
//        binaryStr=[[binaryStr substringFromIndex:8] mutableCopy];
    }
    else if(bitcnt ==24)
    {
        binaryStr = [[@"00000000" stringByAppendingString:binaryStr] mutableCopy];
    }
    else if(bitcnt ==16)
    {
        binaryStr = [[@"0000000000000000" stringByAppendingString:binaryStr] mutableCopy];
    }
    else if (bitcnt == 8)
    {
        binaryStr = [[@"000000000000000000000000" stringByAppendingString:binaryStr] mutableCopy];
    }
    [arrStr addObject:[binaryStr substringToIndex:8]];
    [arrStr addObject:[binaryStr substringWithRange:NSMakeRange(8, 8)]];
    [arrStr addObject:[binaryStr substringWithRange:NSMakeRange(16, 8)]];
    [arrStr addObject:[binaryStr substringFromIndex:24]];
    
    //NSLog(@"%@",arrStr);
    
    return arrStr;
}


-(void)setupSesssion
{
    int ii=0;
    _selectedAccessory = [_accessoryList objectAtIndex:ii];
    NSArray *protocolStrings = [_selectedAccessory protocolStrings];
    for(NSString *protocolString in protocolStrings)
    {
        if (![protocolString isEqualToString:@""])
        {
            
            [_eaSessionController setupControllerForAccessory:_selectedAccessory withProtocolString:[[_selectedAccessory protocolStrings] objectAtIndex:ii]];
            
            EADSessionController *sessionController = [EADSessionController sharedController];
            [sessionController openSession];
            
            break;
        }
        ii++;
    }
}


-(void)setupSesssion:(EAAccessory *)_Accessory
{
    NSArray *protocolStrings = [_Accessory protocolStrings];
    
    if (protocolStrings.count > 0)
    {
        [_eaSessionController setupControllerForAccessory:_Accessory withProtocolString:[protocolStrings objectAtIndex:0]];
        EADSessionController *sessionController = [EADSessionController sharedController];
        BOOL isopen =  [sessionController openSession];
    }
}

#pragma mark Internal

- (void)_accessoryDidConnect:(NSNotification *)notification
{
    EAAccessory *connectedAccessory = [[notification userInfo] objectForKey:EAAccessoryKey];
    [_accessoryList addObject:connectedAccessory];
    
    if ([_accessoryList count] == 0)
    {
        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"No Accessories connected." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        
        EADSessionController *sessionController = [EADSessionController sharedController];
        [sessionController closeSession];
        [sessionController setupControllerForAccessory:nil withProtocolString:nil];
    }
    else
    {
//         [self setupSesssion:connectedAccessory];
//        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//        dict[@"accessoryList"] = _accessoryList;
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"AccessoryConnect" object:self userInfo:dict];
    }
    
}

- (void)_accessoryDidDisconnect:(NSNotification *)notification
{
    EAAccessory *disconnectedAccessory = [[notification userInfo] objectForKey:EAAccessoryKey];
    
    int disconnectedAccessoryIndex = 0;
    for(EAAccessory *accessory in _accessoryList)
    {
        if ([disconnectedAccessory connectionID] == [accessory connectionID]) {
            
            EADSessionController *sessionController = [EADSessionController sharedController];
            [sessionController closeSession];
            [sessionController setupControllerForAccessory:nil withProtocolString:nil];
            
//            [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Accessories dis-connected." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];

            
            break;
        }
        disconnectedAccessoryIndex++;
    }
    
    if (disconnectedAccessoryIndex < [_accessoryList count])
    {
        [_accessoryList removeObjectAtIndex:disconnectedAccessoryIndex];
    
    }
    else
    {
        //NSLog(@"could not find disconnected accessory in accessory list");
    }
    
    if ([_accessoryList count] == 0)
    {
//        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"No Accessories connected." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        
        
    }
    else
    {
//        [self setupSesssion];
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    dict[@"accessoryList"] = _accessoryList;
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"AccessoryConnect" object:self userInfo:dict];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AccessoryDisConnect" object:self userInfo:dict];

    

}


-(void) addDeviceDetail:(EAAccessory *)_Accessory
{
//    Tbl_devices *model = [[Tbl_devices alloc]init];
//    model.device_name = _Accessory.name;
//    model.device_tmd_version = @"-";
//    model.device_unique_id = _Accessory.serialNumber;
//    model.device_battery =
    
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_Accessory.serialNumber forKey:@"device_unique_id"];
    [dict setValue:@"Connected" forKey:@"device_status"];
    [dict setValue:_Accessory.name forKey:@"device_name"];
    [dict setValue:@"-" forKey:@"device_battery"];
    [dict setValue:@"-" forKey:@"device_tmd_version"];
    [dict setValue:@"-" forKey:@"photo_printed"];
    [dict setValue:@"-" forKey:@"device_auto_power_off_status"];
    
  //  CoreData *data = [[CoreData alloc]init];
    //[data saveDeviceInfo:@"Tbl_devices" withArrOfDictionary:dict];
    
    
}




@end
