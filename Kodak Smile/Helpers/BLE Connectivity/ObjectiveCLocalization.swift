//
//  ObjectiveCLocalization.swift
//  SnapTouchApp
//
//  Created by LaNet on 10/17/16.
//  Copyright © 2016 SnapTouch. All rights reserved.
//

import UIKit

@objc class ObjectiveCLocalization: NSObject {

    let lbl_no_connected_acessory = lbl_No_ACESSORY_CONNECTED
    let applicationName = AppName
    let lb_OK = lbl_OK
}
