//
//  PhotoAlbumViewController.swift
//  Snaptouch_Polaroid
//
//  Created by MAXIMESS114 on 12/12/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit
import Photos

var PolariodSnaptouchSaveAlbum = "SMILE"
var PolaroidSnaptouchEditAlbum = "SMILE"

class saveImageInCustomAlbum:NSObject{

    var assetCollection: PHAssetCollection!

    override init() {
        super.init()

        //self.createPhotoLibraryAlbum(name: PolariodSnaptouchSaveAlbum)
        self.createPhotoLibraryAlbum(name: PolaroidSnaptouchEditAlbum)
    }

    func saveImageInAlbum(name:String, withImage Image:UIImage){
        if let assetCollection = fetchAssetCollectionForAlbum(name:name) {
            self.assetCollection = assetCollection
            print("Image",Image)
            self.save(image: Image)
            return
        }
    }

    //Fetch album froPHAssetCollection
    func fetchAssetCollectionForAlbum(name:String) -> PHAssetCollection? {

        print("11 fetchAssetCollectionForAlbum(name:String) -> PHAssetCollection?")

        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", name)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)

        if let _: AnyObject = collection.firstObject {
            return collection.firstObject
        }
        return nil
    }

    func isAlbumPresent(name:String)->Bool{
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", name)
        let collection:PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)

        if let first_Obj:AnyObject = collection.firstObject{
            //found the album
            self.assetCollection = collection.firstObject as! PHAssetCollection
            return true
        }
        else { return false }
    }

    //Create custom album in photo gallery
    func createPhotoLibraryAlbum(name: String) {

        var albumPlaceholder: PHObjectPlaceholder?

        PHPhotoLibrary.shared().performChanges({

            if !self.isAlbumPresent(name:name)
            {
                // Request creating an album with parameter name
                let createAlbumRequest = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: name)
                // Get a placeholder for the new album
                albumPlaceholder = createAlbumRequest.placeholderForCreatedAssetCollection
            }

        }, completionHandler: { success, error in
            if success {
//                guard let placeholder = albumPlaceholder else {
//                    fatalError("Album placeholder is nil")
//                }
//
//                let fetchResult = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [placeholder.localIdentifier], options: nil)
//
//                guard let album: PHAssetCollection = fetchResult.firstObject else {
//                    // FetchResult has no PHAssetCollection
//                    return
//                }
//
//                // Saved successfully!
                print("Album saved successfully")
            }
            else if let e = error {
                // Save album failed with error
                print(e)

            }
            else {
                // Save album failed with no error
                print("Save album failed with no error")

            }
        })
    }

    //Save image in custom photo album
    func save(image: UIImage) {

        if assetCollection == nil {
            return                          // if there was an error upstream, skip the save
        }

        var assetIdentifier : String?
        PHPhotoLibrary.shared().performChanges({
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            let assetPlaceHolder = assetChangeRequest.placeholderForCreatedAsset
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
            let enumeration: NSArray = [assetPlaceHolder!]
            albumChangeRequest!.addAssets(enumeration)

            assetIdentifier = assetPlaceHolder?.localIdentifier
            print("check save(image: UIImage)")
        }, completionHandler: {
            (isSuccess, error) -> Void in
            if error == nil
            {
                DispatchQueue.main.async() {
                   
                   // Fire notification to capture the saved image's asset identifier for collage, As we are now going to image preview screen after saving collage.
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getAssetIdentifierAfterSave"), object: assetIdentifier, userInfo: ["image" : image])
                }
            }
        })
    }
    
}
