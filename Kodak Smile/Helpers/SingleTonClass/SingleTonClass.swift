//
//  SingleTonClass.swift
//  Kodak Smile
//
//  Created by MAXIMESS194 on 7/22/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AssetsLibrary
import StoreKit

class SingletonClass: NSObject
{
    static let sharedInstance = SingletonClass()
    
    let upload_similar = BASE_URL_SERVER_END+SIMILAR_TARGET
    var actionInitiatedByUser = false
    var targetId :String?
    var results : Any?
    var parameters_id : [String:String]?
    var timestamp : Int?
    var targetAlreadyPresent : Bool?
    
    
      func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
            options = JSONSerialization.WritingOptions.prettyPrinted
        }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: options)
            if let string = String(data: data, encoding: String.Encoding.utf8) {
                return string
            }
        } catch {
            print(error)
        }
        
        return ""
    }
    
    func sizePerMB(url: URL?) -> Double {
        guard let filePath = url?.path else {
            return 0.0
        }
        do {
            let attribute = try FileManager.default.attributesOfItem(atPath: filePath)
            if let size = attribute[FileAttributeKey.size] as? NSNumber {
                return size.doubleValue / 1000000.0
            }
        } catch {
            print("Error: \(error)")
        }
        return 0.0
    }
    func ccSha256(data: Data) -> Data {
        var digest = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
        
        _ = digest.withUnsafeMutableBytes { (digestBytes) in
            data.withUnsafeBytes { (stringBytes) in
                CC_SHA256(stringBytes, CC_LONG(data.count), digestBytes)
            }
        }
        return digest
    }
    
    func healthCheck_EasyARForDeleteTarget(){
       
            let uploadURLAR = BASE_URL_SERVER_END+HEALTH_CHECK
            DispatchQueue.global(qos: .background).async {
                Alamofire.request(uploadURLAR,method: .get, parameters:nil, encoding: JSONEncoding.default, headers: nil).responseJSON
                    { response in
                        switch(response.result)
                        {
                        case .success(_):
                            //                        customLoader.hideActivityIndicator()
                            print("response.result.value\(response.result.value)")
                            if response.result.value != nil
                            {
                                print("response.result.value\(response.result.value)")
                                
                                let dict = response.result.value! as! [String:Any]
                                SingletonClass.sharedInstance.timestamp = dict["timestamp"] as? Int
                                UserDefaults.standard.set(SingletonClass.sharedInstance.timestamp, forKey: "timestamp")
                               SingletonClass.sharedInstance.deleteTarget_EasyAR()
                                
                            }
                            break
                            
                        case .failure(let error):
                            customLoader.hideActivityIndicator()
                            var error = error.localizedDescription
                            if error.contains("JSON could not") {
                                error = "Could not connect to server at the moment."
                            }
                        }
                }
            }
       
    }
   
    func deleteTarget_EasyAR(){
        print("targetId\(SingletonClass.sharedInstance.targetId ?? "")")
        DispatchQueue.global(qos: .background).async {
            //        customLoader.showActivityIndicator(viewController: self.view)
                 let rawdata = "appKey" + "\(cloud_key)" + "timestamp" + "\(SingletonClass.sharedInstance.timestamp ?? 0)" + "\(cloud_secret)"
                print("rawdata\(rawdata)")
                  let data = SingletonClass.sharedInstance.ccSha256(data: (rawdata.data(using: .utf8)!))
                print("data\(data)")
                   let upload_delete = BASE_URL_SERVER_END+DELETE_TARGET+"\(SingletonClass.sharedInstance.targetId ?? "")" + "?appKey=\(cloud_key)&" + "timestamp=\(SingletonClass.sharedInstance.timestamp ?? 0)&" + "signature=\(data.map { String(format: "%02hhx", $0) }.joined())"
                print("upload_delete\(upload_delete)")
                  let parameter = [:] as [String : Any]
                Alamofire.request(upload_delete,method: .delete, parameters:parameter, encoding: JSONEncoding.default, headers: nil).responseJSON
                    { response in
                        switch(response.result)
                        {
                        case .success(_):
                            if response.result.value != nil
                            {
                                  let dict = response.result.value! as! [String:Any]
                                    let status = dict["statusCode"] as? Int
                                    print("status\(status)")
                                if status == 0{
                                    print("Data deleted successfully")
                                }else{
//                                    SingletonClass.sharedInstance.healthCheck_EasyARForDeleteTarget()
                                }
                            }
                            
                            break
                            
                        case .failure(let error):
                            customLoader.hideActivityIndicator()
                            var error = error.localizedDescription
                              if error.contains("JSON could not") {
                                error = "Could not connect to server at the moment."
                            }
                        }
                }
           
        }
    }
    
//    Convert string to dict
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
//    Show rating popup
    func showRatingsPopup()
    {
        // show App rating popup
        let lastDate = UserDefaults.standard.value(forKey: "SaveDateAtShowInAppPopup") as? NSDate
        let currentDate = NSDate()
        
        if show_AppRating_Popup == 2
        {
            show_AppRating_Popup = 0
            
            if lastDate == nil
            {
                UserDefaults.standard.setValue(currentDate, forKey: "SaveDateAtShowInAppPopup")
                UserDefaults.standard.synchronize()
                //showAppRatingAlert()
                
                if #available( iOS 10.3,*){
                    SKStoreReviewController.requestReview()
                }
            }
        }
    }
    func imageIsNullOrNot(imageName : UIImage)-> Bool
    {
        
        let size = CGSize(width: 0, height: 0)
        if (imageName.size.width == size.width)
        {
            return false
        }
        else
        {
            return true
        }
    }
}
