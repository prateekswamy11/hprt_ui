//
//  CustomLoaderView.swift
//  SmartSafetyAlert
//
//  Created by maximess142 on 09/05/18.
//  Copyright © 2018 maximess134. All rights reserved.
//

import UIKit
let customLoader = CustomLoaderView()

class CustomLoaderView: UIView {
    
    var isShowingLoader = false
    var viewParent = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var indicatorContainerView = UIView()
    
    //Gif Loader ImageView variable.
    var imgViewGifLoader = UIImageView()
    var animatedImages = [UIImage]()
    
    func showConnectivityActivityIndicator(viewController: UIView)
    {
        self.customActivityIndicator(viewController, "connecting_printer_000")
    }
    
    func showActivityIndicator(viewController: UIView)
    {
        self.customFastActivityIndicator(viewController, "Spiner")
    }
    
    func customActivityIndicator(_ viewController: UIView,_ colorName: String){
        viewParent = viewController
        animatedImages.removeAll()
        
        for index in 0...71{
            let imageName = "\(colorName)\(index)"
            let bundlePath = Bundle.main.path(forResource: imageName, ofType: "png")
            guard let imgLoader = UIImage(contentsOfFile: bundlePath!) else{
                return
            }
            animatedImages.append(imgLoader)
        }
        
        imgViewGifLoader.frame = CGRect(x: 0, y: 0, width: viewController.frame.width, height: viewController.frame.height)
        imgViewGifLoader.animationImages = animatedImages
        viewParent.addSubview(imgViewGifLoader)
        imgViewGifLoader.startAnimating()
        
        viewParent.isUserInteractionEnabled = false
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func customFastActivityIndicator(_ viewController: UIView,_ colorName: String){
        viewParent = viewController
        animatedImages.removeAll()
        
        for index in 0...26{
            let imageName = "\(colorName)\(index)"
            let bundlePath = Bundle.main.path(forResource: imageName, ofType: "png")
            guard let imgLoader = UIImage(contentsOfFile: bundlePath!) else{
                return
            }
            animatedImages.append(imgLoader)
        }
        
        imgViewGifLoader.frame = CGRect(x: viewController.center.x - (30.0), y: viewController.center.y - (30.0), width: 60.0, height: 60.0)
        imgViewGifLoader.animationImages = animatedImages
        viewParent.addSubview(imgViewGifLoader)
        imgViewGifLoader.startAnimating()
        
        if !(viewController.parentViewController is DownloadFirmwareViewController) {
            viewParent.isUserInteractionEnabled = false
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
    }
    
    func hideActivityIndicator()
    {
        DispatchQueue.main.async {
            self.isShowingLoader = false
            self.imgViewGifLoader.stopAnimating()
            self.imgViewGifLoader.removeFromSuperview()
            self.removeFromSuperview()
        
            self.viewParent.isUserInteractionEnabled = true
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    }
    func startGifLoader(viewController: UIView,gif: UIImage,width: CGFloat,height: CGFloat)
    {
        viewParent = viewController
        
        imgViewGifLoader = UIImageView(gifImage: gif)
        imgViewGifLoader.frame = CGRect(x: viewController.center.x - (width/2), y: viewController.center.y - (height/2), width: width, height: width)
        viewParent.addSubview(imgViewGifLoader)
        
        imgViewGifLoader.startAnimatingGif()
        
        viewParent.isUserInteractionEnabled = false
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopGifLoader()
    {
        imgViewGifLoader.stopAnimatingGif()
        imgViewGifLoader.removeFromSuperview()
        
        viewParent.isUserInteractionEnabled = true
        UIApplication.shared.endIgnoringInteractionEvents()
    }
}
