//
//  ProgressBarView.swift
//  progressBar
//
//  Created by ashika shanthi on 1/4/18.
//  Copyright © 2018 ashika shanthi. All rights reserved.
//

import UIKit
class ProgressBarView: UIView {
    
    var bgPath: UIBezierPath!
    var shapeLayer: CAShapeLayer!
    var progressLayer: CAShapeLayer!
    
    var progress: Float = 0 {
        willSet(newValue)
        {
            shapeLayer.strokeEnd = CGFloat(newValue)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        bgPath = UIBezierPath()
        self.simpleShape()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        bgPath = UIBezierPath()
        self.simpleShape()
    }
    
    func simpleShape()
    {
        createCirclePath()
        shapeLayer = CAShapeLayer()
        shapeLayer.path = bgPath.cgPath
        shapeLayer.lineWidth = 70.0
        shapeLayer.fillColor = nil
        shapeLayer.strokeColor = UIColor(red: 252, green: 183, blue: 20, a: 1.0).cgColor
        shapeLayer.strokeEnd = 0.0
    
//        progressLayer = CAShapeLayer()
//        progressLayer.path = bgPath.cgPath
////        progressLayer.lineCap = kCALineCapRound
//        progressLayer.lineCap = kCALineCapSquare
//        progressLayer.lineWidth = 7
//        progressLayer.fillColor = nil
//        progressLayer.strokeColor = UIColor(red: 0.0, green: 155.0, blue: 250.0, alpha: 1.0).cgColor
//        progressLayer.strokeEnd = 0.0
        
        self.layer.addSublayer(shapeLayer)
//        self.layer.addSublayer(progressLayer)
    }
    
    private func createCirclePath()
    {
//        let startAngle = (-CGFloat.pi/2)
        let startAngle = CGFloat(-1.5)
        let endAngle = 2 * CGFloat.pi + startAngle
        let x = self.frame.width/2
        let y = self.frame.height/2
        let center = CGPoint(x: x, y: y)
        print(x,y,center)
//        bgPath.addArc(withCenter: center, radius: x/CGFloat(2), startAngle: CGFloat(0), endAngle: CGFloat(0), clockwise: true)
//        bgPath.addArc(withCenter: center, radius: x/CGFloat(2), startAngle: startAngle, endAngle: endAngle, clockwise: true)
        bgPath.addArc(withCenter: center, radius: 35, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        bgPath.close()
        
        // WHITE BACKGROUND
        let circlePath = UIBezierPath(arcCenter: center, radius: CGFloat(35), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        let bgShapeLayer = CAShapeLayer()
        bgShapeLayer.path = circlePath.cgPath
        
        bgShapeLayer.fillColor = nil
        bgShapeLayer.strokeColor = UIColor.white.cgColor
        bgShapeLayer.lineWidth = 70.0
        self.layer.addSublayer(bgShapeLayer)
    }
}
