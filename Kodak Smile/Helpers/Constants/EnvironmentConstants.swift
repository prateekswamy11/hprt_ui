//
//  EnvironmentConstants.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 17/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
//MARK: - Copilot User id -
var COPILOT_USER_ID = String()
//URL
let DEV_URL = "http://polaroidapps.ml/dev/kodak-api/kodak_smile/"
//let DEV_URL = "http://polaroidapps.ml/dev/polaroid-new/mint/"
let LIVE_URL = "http://smile.kodakphotoplus.com/kodak-api/kodak_smile/"
let MAIN_URL = LIVE_URL
let FIRMWARE_URL = "get_firmware/1.0"
// Appsee Keys.
//let LIVE_APPSEE_KEY = "67ad6ed5a54b47e5ba5f5a5cd78139dc"
//let DEV_APPSEE_KEY = "43fd94f417554dfeb2553e0e7c28fc6c"
let LIVE_AR_URL_FFMPEG = "http://ec2-18-219-217-24.us-east-2.compute.amazonaws.com:8087/" //FFMPEG CDN
//"http://ec2-18-219-217-24.us-east-2.compute.amazonaws.com:8066/"
let LIVE_AR_URL_CDN = "http://ec2-18-220-133-122.us-east-2.compute.amazonaws.com:8087/" // CDN
// AR URLS
let LIVE_AR_URL = "http://ec2-18-220-133-122.us-east-2.compute.amazonaws.com:8080/" // S3
//"http://ec2-18-220-133-122.us-east-2.compute.amazonaws.com:8080/"
let DEV_AR_URL = "https://maximessai.ml:8087/"
let UPLOAD_SUB_URL = "upload_photo_new"//"upload_photo"
let COMPARE_SUB_URL = "compare_image"

//Easy AR
let LiveAppName = "Live_KodakSmileIOS\(SingletonClass.sharedInstance.timestamp ?? 0)"
let DevAppName = "Dev_KodakSmileIOS\(SingletonClass.sharedInstance.timestamp ?? 0)"
let BASE_URL_SERVER_END = "http://972bab462692504b82d2b4123c74a956.na1.crs.easyar.com:8888/"
let cloud_server_address = "972bab462692504b82d2b4123c74a956.na1.crs.easyar.com:8080"
let cloud_key = "329ccc267b687fa44c63eed56db6fd0a"
let cloud_secret = "o2KlhlQ1720NRBx6h2Kyy4ae47vTHSsJk8uNdojXf9Xbs668P8EQ29SoneExrjS39oMc2SlkGIztuFBfNlyCbzpQlNDpJfmGtIbefwjJRGNsPxmfr7snAgUtQGj7ScXl"
let HEALTH_CHECK = "ping"
let DELETE_TARGET = "target/"
let NEW_TARGET = "targets"
let SIMILAR_TARGET = "similar"
let MODIFY_TARGET = "target/"
class EnvironmentConstants: NSObject {

}
enum EventsUrl {
    static let productionUrl = "http://smile.smilecdn.site/api/"
    static let devUrl = "http://ec2-52-15-189-116.us-east-2.compute.amazonaws.com/api/"
    static let main = productionUrl
    static let stickers = main + "stickers"
    static let frames = main + "frames"
    static let message = main + "event-message"
}
