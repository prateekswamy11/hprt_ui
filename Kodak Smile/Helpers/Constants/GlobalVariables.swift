//
//  GlobalVariables.swift
//  Polaroid MINT
//
//  Created by maximess142 on 12/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

var show_AppRating_Popup = Int()

class GlobalVariables: NSObject {

}

// Connected device mac address
var CONNECT_DEVICE_MAC_ADDRESS = String()
var IS_POP_SHOWN = Bool()
var IS_HOLD_ON_POP_SHOWN = Bool()
var IS_OUT_OF_PAPER_POP_SHOWN = Bool()
var IS_PRINT_ERROR = Bool()
var IS_GET_FIRMWARE = Bool()
var isPrintDoneCheckOutOfPaper = Bool()
var currentVC = String()

struct EditFunction {
    // structure definition goes here
    let filter = "filter"
    let brightness = "brightness"
    let contrast = "contrast"
    let satusartion = "satusartion"
    let warm = "warm"
    let highlights = "highlights"
    let shadow = "shadow"
    
}
