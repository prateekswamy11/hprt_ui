//
//  AnimationClass.h
//  SnapTouchApp
//
//  Created by LaNet on 6/29/16.
//  Copyright © 2016 LaNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//@protocol AnimationProtocol <SObject>
//
//-(void) changeTextColor;

//@end
typedef void (^FilterHandler)(UIImage *img);

@interface AnimationClass : NSObject
//- (instancetype)sharedInstance;
//ana
@property (strong, nonatomic) CIContext* ciContext;



- (UIImage *)applyMosaicFilter:(UIImage*)fromImage withScale:(double)scale;
-(NSMutableArray *)splitImage :(UIImage *)image withColums:(NSNumber *)splitColumns WithRows:(NSNumber *)splitRows withSelectedImageSize :(UIImage *)selectedImage;
-(UIImage *)SaveEditedImage:(UIView *)view saveImage:(BOOL) flag;
-(void)createFilterImage:(int)index indensity:(float)indensity image:(UIImage*)image  sizeOF:(CGSize)size flag:(BOOL)flag filterName :(NSString*)filterName : (FilterHandler)handler;
//stcikers and text

-(UIView *)addStickerViewToParentView:(UIView *)view withImage:(UIImage *)image viewController:(UIViewController *)vc;
-(UIView *)addTextViewToParentView:(UIView *)parentView withText:(NSString *)strText andFont:(NSString *)fontName andFontSize:(CGFloat)fontSize viewController:(UIViewController *)vc;
-(void) setTextViewColor : (UIColor *)color;
//@property (strong,nonatomic) AnimationProtocol<id> animProtocol;
-(UIFont *)fontToFitHeightLabel:(UILabel *)label;
- (CGSize) aspectScaledImageSizeForImageView:(UIImageView *)iv image:(UIImage *)im;
-(CGFloat)getImageHeightByWidth:(CGFloat)imageWidth height:(CGFloat)imageHeight andNewWidth:(CGFloat)newWidth;
-(UIFont *)findAdaptiveFontWithName:(NSString *)fontName forUILabelSize:(CGSize)labelSize withMinimumSize:(NSInteger)minSize andString:(NSString *)str;

- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize ;
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size ;

-(UIImage*)verticalFlip :(UIImage *)imgToFlip;

-(UIImage*)horizontalFlip:(UIImage *)imgToFlip;

-(NSMutableArray *)getSteakersImages :(NSString *)strImageName;
-(NSMutableArray *)getBorderImagesFromSingleImage:(NSString *)strImageName;
- (NSData *)compressImageThumbnail:(UIImage *)image ;

//poloridZipSnap
-(NSData *)stringToData : (NSString *)bitSeries;
- (NSMutableArray *)intToBinary:(int)number;




@property (strong,nonatomic) UITextView *textView;
@property (strong,nonatomic) UILabel *lblSticker;
@property (strong,nonatomic) UITextField *txtFieldSticker;

@end
