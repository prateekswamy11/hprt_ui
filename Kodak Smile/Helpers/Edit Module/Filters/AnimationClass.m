//
//  AnimationClass.m
//  SnapTouchApp
//
//  Created by LaNet on 6/29/16.
//  Copyright © 2016 LaNet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimationClass.h"
#import "LUTToNSDataConverter.h"
#import "AssetsLibrary/AssetsLibrary.h"
#import <Foundation/Foundation.h>


typedef void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *asset);
typedef void (^ALAssetsLibraryAccessFailureBlock)(NSError *error);
typedef void (^FilterImageCompletionHandler) (UIImage *image);

@implementation AnimationClass
@synthesize textView,lblSticker,txtFieldSticker;

/*
- (instancetype)sharedInstance
{
    static AnimationClass *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AnimationClass alloc] init];
        
 
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(chnageTextColor:)
                                                     name:@"CHANGE_TEXT_COLOR"
                                                   object:nil];
        
        // Do any other initialisation stuff here
    });
    return sharedInstance;
    
    
}*/


- (UIImage *)applyMosaicFilter:(UIImage*)fromImage withScale:(double)scale
{
    /*
     Makes an image blocky by mapping the image to colored squares whose color is defined by the replaced pixels.
     Parameters
     
     inputImage: A CIImage object whose display name is Image.
     
     inputCenter: A CIVector object whose attribute type is CIAttributeTypePosition and whose display name is Center.
     Default value: [150 150]
     
     inputScale: An NSNumber object whose attribute type is CIAttributeTypeDistance and whose display name is Scale.
     Default value: 8.00
     */
    
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIFilter *filter= [CIFilter filterWithName:@"CIPixellate"];
    CIImage *inputImage = [[CIImage alloc] initWithImage:fromImage];
    CIVector *vector = [CIVector vectorWithX:fromImage.size.width /2.0f Y:fromImage.size.height /2.0f];
    [filter setDefaults];
    [filter setValue:vector forKey:@"inputCenter"];
    [filter setValue:[NSNumber numberWithDouble:scale] forKey:@"inputScale"];
    [filter setValue:inputImage forKey:@"inputImage"];
    
    CGImageRef cgiimage = [context createCGImage:filter.outputImage fromRect:filter.outputImage.extent];
    UIImage *newImage = [UIImage imageWithCGImage:cgiimage scale:1.0f orientation:fromImage.imageOrientation];
    
    CGImageRelease(cgiimage);
    
    return newImage;
}


-(NSMutableArray *)splitImage :(UIImage *)image withColums:(NSNumber *)splitColumns WithRows:(NSNumber *)splitRows withSelectedImageSize :(UIImage *)selectedImage
{
    NSMutableArray *arraySplitImages = [[NSMutableArray alloc]init];
    int rows = [splitRows intValue];
    int columns = [splitColumns intValue];
    
    CGFloat xPadding = 2.0;
    CGFloat yPadding = 2.0;
    //    UIView *splitView = [[UIView alloc]initWithFrame:CGRectMake(splitFrame.origin.x, splitFrame.origin.y, (splitFrame.size.width)+(xPadding*(rows-1)), (splitFrame.size.height)+(yPadding*(rows-1)))];
    
    CGSize imageSize = selectedImage.size;
    CGFloat xPos = 0.0, yPos = 0.0, imgvwYPos=0.0,imgvwXPos = 0.0;
    CGFloat width = (imageSize.width/rows)*image.scale;
    CGFloat height = (imageSize.height/columns)*image.scale;
    
    if (arraySplitImages.count > 0)
    {
        [arraySplitImages removeAllObjects];
    }
    for (int aIntY = 0; aIntY < columns; aIntY++)
    {
        xPos = 0.0;
        imgvwXPos = 0.0;
        for (int aIntX = 0; aIntX < rows; aIntX++)
        {
            CGRect rect = CGRectMake(xPos, yPos, width, height);
            CGImageRef cImage = CGImageCreateWithImageInRect([ image CGImage],  rect);
            
            UIImage *aImgRef = [[UIImage alloc] initWithCGImage:cImage];
            
            //            UIImageView *aImgView = [[UIImageView alloc] initWithFrame:CGRectMake(imgvwXPos, imgvwYPos, splitFrame.size.width/rows, splitFrame.size.height/columns)];
            //            [aImgView setClipsToBounds:YES];
            //            [aImgView setImage:aImgRef];
            [arraySplitImages addObject:aImgRef];
            //            [splitView addSubview:aImgView];
            xPos += width;
            //            imgvwXPos = imgvwXPos + (splitFrame.size.width/rows)+ xPadding;
        }
        yPos += height;
        //        imgvwYPos = imgvwYPos + (splitFrame.size.height/columns)+ yPadding;
    }
    return arraySplitImages;
}






-(UIImage *)SaveEditedImage:(UIView *)view saveImage:(BOOL) flag
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(round(view.bounds.size.width), round(view.bounds.size.height)),NO,0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:context];
    UIImage *capturedScreen = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return capturedScreen;
}



-(void)dealloc
{
    //    [super dealloc];
    [EAGLContext setCurrentContext:nil];
}



-(void)createFilterImage:(int)index indensity:(float)indensity image:(UIImage*)image  sizeOF:(CGSize)size flag:(BOOL)flag filterName :(NSString*)filterName : (FilterHandler)handler;
{
    @autoreleasepool
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            // do work here
            
            NSData *colorCubeData= [LUTToNSDataConverter colorCubeDataFromLUTNamed:filterName  interpolatedWithIdentityLUTNamed:@"Identity" withIntensity:0.7 cacheIdentityLUT:YES];
            NSLog(@"%@", filterName);
            
            CIFilter *filter = [CIFilter filterWithName: @"CIColorCube"];
            
            CIImage *coreImage = [CIImage imageWithCGImage:image.CGImage];
            // CIImage *coreImage = [[[CIImage alloc]initWithImage:image]autorelease];
            
            CIImage *currentImage = coreImage;
            
            if(colorCubeData !=nil && filter!=nil)
            {
                [filter setValue:colorCubeData forKey:@"inputCubeData"];
                [filter setValue:[NSNumber numberWithInteger:64] forKey:@"inputCubeDimension"];
                [filter setValue:coreImage forKey:kCIInputImageKey];
            }
            
            CIContext *context = [CIContext contextWithOptions:nil];
            //UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
            
            currentImage = [filter outputImage];
            CGImageRef cgImage = [context createCGImage:currentImage fromRect:[currentImage extent]];
            UIImage *filteredImage = [UIImage imageWithCGImage:cgImage];
            CFRelease(cgImage);
            //UIGraphicsEndImageContext();
            
            if(filteredImage != nil)
            {
                handler(filteredImage);
            }
        });
    }
}




-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



-(CGFloat)getImageHeightByWidth:(CGFloat)imageWidth height:(CGFloat)imageHeight andNewWidth:(CGFloat)newWidth
{
    CGFloat scale = newWidth/imageWidth;
    CGFloat newHeight = imageHeight * scale;
    return newHeight;
}



- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}

//flip

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

//compression of image

- (NSData *)compressImageThumbnail:(UIImage *)image{
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1280;
    float maxWidth = 816;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    
    NSData *imageData1 = UIImagePNGRepresentation(image);
    float imgLength = roundf(imageData1.length/100) ;
    float compressionQuality = 0.6;
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContextWithOptions(rect.size,NO,0);
    [image drawInRect:rect];
    //    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(image,compressionQuality);
    UIGraphicsEndImageContext();
    
    //    NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/test.jpeg"];
    //
    //    NSLog(@"PATH >>>> %@",jpgPath);
    //
    //    [imageData writeToFile:jpgPath atomically:YES];
    
    
    return imageData;
}

- (void)PrintImage:(NSMutableData *)imagedata //numberofprint :(int)printcount
{
    
    NSMutableData *RequestData=[[NSMutableData alloc]init];
    
    NSString *bitSeries = @"";
    NSData *data = [[NSData alloc]init];
    //    1
    bitSeries = @"00011011";
    data=[self stringToData:bitSeries];
    //NSLog(@"1 : %@", data);
    [RequestData appendData:data];
    //    2
    bitSeries = @"00101010";
    data=[self stringToData:bitSeries];
    //NSLog(@"2 : %@", data);
    [RequestData appendData:data];
    //    3
    bitSeries = @"01000011";
    data=[self stringToData:bitSeries];
    //NSLog(@"3 : %@", data);
    [RequestData appendData:data];
    //    4
    bitSeries = @"01000001";
    data=[self stringToData:bitSeries];
    //NSLog(@"4 : %@", data);
    [RequestData appendData:data];
    //    5
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"5 : %@", data);
    [RequestData appendData:data];
    //    6
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"6 : %@", data);
    [RequestData appendData:data];
    //    7
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"7 : %@", data);
    [RequestData appendData:data];
    //    8
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"8 : %@", data);
    [RequestData appendData:data];
    //    9
    
    NSMutableArray *arrstr = [self intToBinary:(int)[imagedata length]];
    bitSeries = [arrstr objectAtIndex:0];
    data=[self stringToData:bitSeries];
    //NSLog(@"9 : %@", data);
    [RequestData appendData:data];
    
    bitSeries = [arrstr objectAtIndex:1];
    data=[self stringToData:bitSeries];
    //NSLog(@"9 : %@", data);
    [RequestData appendData:data];
    
    bitSeries =[arrstr objectAtIndex:2];
    data=[self stringToData:bitSeries];
    //NSLog(@"9 : %@", data);
    [RequestData appendData:data];
    //    10
    bitSeries = @"00000001";
    data=[self stringToData:bitSeries];
    //NSLog(@"10 : %@", data);
    [RequestData appendData:data];
    //    11
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"11 : %@", data);
    [RequestData appendData:data];
    //    12
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"12 : %@", data);
    [RequestData appendData:data];
    
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"12 : %@", data);
    [RequestData appendData:data];
    
    //    13
    bitSeries = @"00000000";
    data=[self stringToData:bitSeries];
    //NSLog(@"13 : %@", data);
    [RequestData appendData:data];
    
    //NSLog(@"Final Data is : %@", RequestData);
    
    
}




-(NSData *)stringToData : (NSString *)bitSeries
{
    uint8_t value = strtoul([bitSeries UTF8String], NULL, 2);
    //NSLog(@"bitSeries : %@ -------> Value : %u",bitSeries, value);
    NSData *data = [NSData dataWithBytes:&value length:sizeof(value)];
    return data;
}

- (NSMutableArray *)intToBinary:(int)number
{
    int bits =  sizeof(number) * 8;
    int bitcnt=bits;
    
    NSMutableArray *arrStr=[[NSMutableArray alloc]init];
    NSMutableString *binaryStr = [NSMutableString string];
    
    for (; bits > 0; bits--, number >>= 1)
    {
        [binaryStr insertString:((number & 1) ? @"1" : @"0") atIndex:0];
    }
    
    if (bitcnt == 32)
    {
        binaryStr=[[binaryStr substringFromIndex:8] mutableCopy];
    }
    else if(bitcnt ==16)
    {
        binaryStr = [[@"00000000" stringByAppendingString:binaryStr] mutableCopy];
    }
    else if (bitcnt == 8)
    {
        binaryStr = [[@"0000000000000000" stringByAppendingString:binaryStr] mutableCopy];
    }
    [arrStr addObject:[binaryStr substringToIndex:8]];
    [arrStr addObject:[binaryStr substringWithRange:NSMakeRange(8, 8)]];
    [arrStr addObject:[binaryStr substringFromIndex:16]];
    
    //NSLog(@"%@",arrStr);
    
    return arrStr;
}


@end
