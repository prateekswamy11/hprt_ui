//
//  iOSVersionChecker.swift
//  Polaroid MINT
//
//  Created by maximess142 on 16/07/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

// check current ios version
func SYSTEM_VERSION_EQUAL_TO(version: String) -> Bool {
    return UIDevice.current.systemVersion.compare(version, options: .numeric) == ComparisonResult.orderedSame
}

func SYSTEM_VERSION_GREATER_THAN(version: String) -> Bool {
    return UIDevice.current.systemVersion.compare(version, options: .numeric) == ComparisonResult.orderedDescending
}

func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(version: String) -> Bool {
    return UIDevice.current.systemVersion.compare(version, options: .numeric) != ComparisonResult.orderedAscending
}

func SYSTEM_VERSION_LESS_THAN(version: String) -> Bool {
    return UIDevice.current.systemVersion.compare(version, options: .numeric) == ComparisonResult.orderedAscending
}

func SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(version: String) -> Bool {
    return UIDevice.current.systemVersion.compare(version, options: .numeric) != ComparisonResult.orderedDescending
}


// check which iphone
func getCurrentIphone() -> String {
    
    var str = String()
    if UIDevice().userInterfaceIdiom == .phone {
        print("UIScreen.main.nativeBounds.height = \(UIScreen.main.nativeBounds.height)")
        switch UIScreen.main.nativeBounds.height {
        case 1136:
            print("iPhone 5 or 5S or 5C")
            str = "5"
        case 1334:
            print("iPhone 6/6S/7/8")
            str =  "6"
        case 1920.0:
            print("iPhone 6+/6S+/7+/8+")
            str =  "6+"
        case 2436:
            print("iPhone X")
            str =  "X"
        case 960.0:
            print("ipad")
            str =  "ipad"
        default:
            print("UIScreen.main.nativeBounds.height = \(UIScreen.main.nativeBounds.height)")
            print("unknown")
            str =  "unknown"
        }
    }
    return str
    
}
