//
//  WebserviceModelClass.swift
//  Snaptouch_Polaroid
//
//  Created by maximess120 on 18/12/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration

class WebserviceModelClass: NSObject {
    var responseDict = [String:Any]()
    
    //MARK: - Check Internet connection
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func showAlertOn(error: String, completionHandler:@escaping (Bool) -> () = { _ in }) {
        
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            completionHandler(true)
        }))
        
        self.getCurrentViewController()!.present(alert, animated: true, completion: nil)
    }
    
    func getCurrentViewController() -> UIViewController? {
        
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
    }
 
    
    //MARK: - Post method model function
    func postDataFor(module:String,subUrl:String,parameter:[String:Any], completionHandler:@escaping ((Bool,String,[String:Any])) -> ())
    {
        var appVersion = String()
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appVersion = version
        }
        
        let extraPara : [String:Any] = ["build_app_version":appVersion,"app_version_number":1.0,"platform":"iOS"]
        
        let mainParamter = extraPara.reduce(parameter) { (partialResult , pair) in
            var partialResult = partialResult //without this line we could not modify the dictionary
            partialResult[pair.0] = pair.1
            return partialResult
        }
        
        let cleanUrl = subUrl.replacingOccurrences(of: " ", with: "%20")
        let urlString = MAIN_URL + cleanUrl
        
        print("Main parameter:->\(mainParamter)")
        print("Main Url:->\(urlString)")
        
        let url = URL(string: urlString)
        Alamofire.request(url!,method: .post, parameters: mainParamter, encoding: JSONEncoding.default, headers: nil).responseJSON
            { response in
                
                print(response)
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let dict = response.result.value! as! NSDictionary
                        print("Response New:-",dict)
                        
                        guard let responseStatus = dict.value(forKey: "status") as? Bool else {
                            return
                        }
                        
                        var successMsg = String()
                        
                        if let msg = dict.value(forKey: "message") as? String
                        {
                            successMsg = msg
                        }
                        
                        if (responseStatus == false)
                        {
                            completionHandler((false,successMsg,self.responseDict))
                        }
                        else if (responseStatus == true)
                        {
                            if dict.value(forKey: "data") != nil {
                                self.responseDict = dict as! [String:Any]
                            }
                            
                            completionHandler((true,successMsg,self.responseDict))
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    
                    var error = error.localizedDescription
                    
                    if error.contains("JSON could not") {
                        error = "Could not connect to server at the moment."
                    }
                    
                    completionHandler((false, error + " Please try again later.",self.responseDict))
                }
        }
    }
    
    //MARK:- Added for AI
    
    func postDataTo(module:String, subUrl:String,parameter:[String:Any], completionHandler:@escaping (Bool,String,[String:Any]) -> () =  {_, _, _   in })
    {
        
        let urlString = "http://ec2-18-220-133-122.us-east-2.compute.amazonaws.com/" + subUrl
        print("url:-------- \(urlString)")
        //        let manager = Alamofire.SessionManager.default
        //        manager.session.configuration.timeoutIntervalForRequest = 15*60
        
        // Alamofire.SessionManager.default.session.configuration.timeoutIntervalForRequest = 15*60
        //Alamofire.SessionManager.default.session.configuration.timeoutIntervalForResource = 15*60
        
        AFManager.request(urlString, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: nil).responseJSON
            { response in
                
                switch(response.result)
                {
                case .success(_):
                    
                    if response.result.value != nil
                    {
                        let dict = response.result.value! as! NSDictionary
                        
                        let response = dict.value(forKey: "response_code") as! Int
                        
                        var successMsg = String()
                        
                        if let msg = dict.value(forKey: "message") as? String{
                            successMsg = msg
                        }
                        
                        if (response == 400 || response == 404 || response == 401)
                        {
                            print("@@Error \(subUrl) -> \(successMsg)")
                            SMLIsUploadStatusChecked = true
                            completionHandler(false,successMsg,self.responseDict)
                        }
                        else if (response == 200)
                        {
                            print("@@Success \(subUrl) -> \(successMsg)")
                            
                            if let data  = dict as? [String:Any]{
                                self.responseDict = data
                            }
                            completionHandler(true,successMsg,self.responseDict)
                            
                        }else{
                            completionHandler(false,successMsg,self.responseDict)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    print(".failure(let error):")
//                    if response.response?.statusCode == 502 || response.response?.statusCode == 500 {
                        completionHandler(false,"Server down. Please try again later.",self.responseDict)
//                    }
//                    else {
//                        completionHandler(false, error.localizedDescription + " Please try again later.",self.responseDict)
//                    }
                }
        }
    }
    
    //MARK: - Global function for get data
    
    func getDataFor(module:String,subUrl:String, completionHandler:@escaping ((Bool,String,[String:Any])) -> ())
    {
    
        let cleanUrl = subUrl.replacingOccurrences(of: " ", with: "%20")
        
        //Live old url
        let urlString = MAIN_URL + cleanUrl
        
        
        //new url
//         let urlString = "http://139.59.73.204/snaptouch/" + cleanUrl
        
        print("Main Url:->\(urlString)")
        
        let url = URL(string: urlString)
        Alamofire.request(url!,method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON
            { response in
                
                print(response)
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let dict = response.result.value! as! NSDictionary
                        
                        
                        var successMsg = "Success"
                        
                       
                                self.responseDict = dict as! [String:Any]
                        
                            
                            completionHandler((true,successMsg,self.responseDict))
                        
                    }
                    
                    break
                    
                case .failure(let error):
                    
                    var error = error.localizedDescription
                    
                    if error.contains("JSON could not") {
                        error = "Could not connect to server at the moment."
                    }
                    
                    completionHandler((false, error + " Please try again later.",self.responseDict))
                }
        }
        
    }
    
    
    func getDataForomUrl(module:String,url:String, completionHandler:@escaping ((Bool,String,[String:Any])) -> ())
    {
        
        let cleanUrl = url.replacingOccurrences(of: " ", with: "%20")
        let urlString = cleanUrl
        
        
        print("Main Url:->\(urlString)")
        
        let url = URL(string: urlString)
        Alamofire.request(url!,method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON
            { response in
                
                print(response)
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let dict = response.result.value! as! NSDictionary
                        
                        
                        var successMsg = "Success"
                        
                        
                        self.responseDict = dict as! [String:Any]
                        
                        
                        completionHandler((true,successMsg,self.responseDict))
                        
                    }
                    
                    break
                    
                case .failure(let error):
                    
                    var error = error.localizedDescription
                    
                    if error.contains("JSON could not") {
                        error = "Could not connect to server at the moment."
                    }
                    
                    completionHandler((false, error + " Please try again later.",self.responseDict))
                }
        }
        
    }
    
    
}
