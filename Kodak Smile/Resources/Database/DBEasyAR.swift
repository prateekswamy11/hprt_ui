//
//  DBEasyAR.swift
//  Kodak Smile
//
//  Created by MAXIMESS194 on 7/23/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation
import CoreData
import Photos
//import CoreML


@objc class DBEasyAR: NSObject {
    
    var appDelegateVar = appDelegate()
  
    
  @objc class func AddInputsIN_Coredata(){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let Entity = NSEntityDescription.entity(forEntityName: "SaveARVideo", in: managedContext)!
        let saveARVideo = NSManagedObject(entity: Entity, insertInto: managedContext)

    guard let videoUrl = UserDefaults.standard.object(forKey: "video_url_AR") else { return}
        DispatchQueue.global(qos: .background).async {
//           get MP4 video
        if let url = URL(string: videoUrl as! String),
            let urlData = NSData(contentsOf: url) {
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
            let tempVideoPath = documentsPath.stringByAppendingPathComponent("ARVideos")
            let filePath = "\(tempVideoPath)/\(UserDefaults.standard.object(forKey: "image_id_AR") as? String ?? "temp").mp4"
            print("filePath\(filePath)")
            saveARVideo.setValue(filePath, forKey:"video_offline")
            guard let image_id = UserDefaults.standard.object(forKey: "image_id_AR") else {
                return
            }
            guard let uid = UserDefaults.standard.object(forKey: "uid") else {return}
            saveARVideo.setValue(videoUrl, forKey:"video_url_offline")
            saveARVideo.setValue(uid, forKey:"uid_offline")
            saveARVideo.setValue(image_id, forKey:"image_id_offline")
            if filePath == ""{
                  saveARVideo.setValue("0", forKey:"status_offline")
            }else{
                 saveARVideo.setValue("1", forKey:"status_offline")
            }
          
            
            do {
                try managedContext.save()
                print("saved")
            } catch {
                print(error)
            }
            DispatchQueue.main.async {
                urlData.write(toFile: filePath, atomically: true)
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                }) { completed, error in
                    if completed {
                        print("Video is saved!")
                    }
                }
            }
        }
    }
    }
    
    
    
  @objc class func fetchVideoFromDB(){
        //        Fetch Video from Core data db
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let VideoFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "SaveARVideo")
        let VideoResults = try! managedContext.fetch(VideoFetch)
   
//    Match image id and get offline stored video
    for result in VideoResults
    {
        if let image_id_offline = (result as AnyObject).value(forKey: "image_id_offline") as? String{
            if let image_id_AR = UserDefaults.standard.object(forKey: "image_id_AR") as? String{
                if(image_id_AR == image_id_offline){
                    guard let video_data = (result as AnyObject).value(forKey: "video_offline") as? String else {return}
                    guard let uid = (result as AnyObject).value(forKey: "uid_offline") as? String else {return}
                    guard let video_url = (result as AnyObject).value(forKey: "video_url_offline") as? String else {return}
                    UserDefaults.standard.set(video_url, forKey: "Video_urlFromOffline")
                    UserDefaults.standard.set(uid, forKey: "uid_Fromoffline")
                    UserDefaults.standard.set(video_data, forKey: "VideoFromOffline")
                    let status = (result as AnyObject).value(forKey: "status_offline") as? String
                }
            }
        }
    }
//    Check video is available or not and if available ,status = 1
    for result in VideoResults
    {
        let video_offline = (result as AnyObject).value(forKey: "video_offline") as? String
        if video_offline == "" {
           DBEasyAR.AddInputsIN_Coredata()
        }
    }
    }
    
    
    
    
    
}
