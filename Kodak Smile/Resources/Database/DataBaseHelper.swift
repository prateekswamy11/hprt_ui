//
//  DataBaseHelper.swift
//  ObjectAndFacesDetection
//
//  Created by MacMini003 on 22/10/18.
//  Copyright © 2018 MAXIMESS142. All rights reserved.
//

import UIKit
import CoreData
//import CoreML

class DataBaseHelper: NSObject {
    
    var appDelegateVar = appDelegate()
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    //    private func persistentContainer.viewContext() -> NSManagedObjectContext {
    //        let appDelegate: AppDelegate
    //        if Thread.current.isMainThread {
    //            appDelegate = UIApplication.shared.delegate as! AppDelegate
    //        } else {
    //            appDelegate = DispatchQueue.main.sync {
    //                return UIApplication.shared.delegate as! AppDelegate
    //            }
    //        }
    //        return appDelegate.persistentContainer.viewContext
    //    }
    
    // insert face data
    func insertAssetId(assetId: String, fromAssetId: String, imageData: String) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        //        print(dbUrl?.absoluteString as Any)
        let key = self.fetchP_Key()
        let managedContext = self.persistentContainer.newBackgroundContext()
//        appDelegate.persistentContainer.performBackgroundTask { (managedContext) in
            let fetchRequest =
                NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
            fetchRequest.predicate = NSPredicate(format: "asset_id = %@", assetId)
            do {
                let test = try managedContext.fetch(fetchRequest)
                if test.isEmpty {
                    let table = NSEntityDescription.entity(forEntityName: "FaceDetectionTable", in: managedContext)
                    let tableData = NSManagedObject(entity: table!, insertInto: managedContext)
                    if let id = assetId as? String {
                        tableData.setValue(assetId, forKey: "asset_id")
                    }
                    if let fId = fromAssetId as? String {
                        tableData.setValue(fromAssetId, forKey: "from_asset_id")
                    }
                    if let data = imageData as? String {
                        tableData.setValue(imageData, forKey: "image_data")
                    }
                    tableData.setValue([], forKey: "vector_data")
                    tableData.setValue(false, forKeyPath: "is_deleted")
                    if let pk = key as? Int {
                        tableData.setValue(key, forKey: "p_key")
                    }
                    do {
                        try managedContext.save()
                    }
                    catch {
                        print("\(error.localizedDescription)---error in saving insertAssetId")
                    }
                }
            }
            catch {
                print("\(error.localizedDescription)---error in fetching data from table")
            }
//        }
    }
    
    //insert all photos with data
    func insertAllPhotos(assetId: String, faceId: String,objects: String, location: String) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        //        print(dbUrl?.absoluteString as Any)
        let locationData = location.components(separatedBy: ",")
        let managedContext = self.persistentContainer.newBackgroundContext()
//        appDelegate.persistentContainer.performBackgroundTask { (managedContext) in
            let fetchRequest =
                NSFetchRequest<NSManagedObject>(entityName: "AllImagesTable")
            fetchRequest.predicate = NSPredicate(format: "asset_id = %@", assetId)
            do {
                let test = try managedContext.fetch(fetchRequest)
                if test.isEmpty {
                    let table = NSEntityDescription.entity(forEntityName: "AllImagesTable", in: managedContext)
                    let tableData = NSManagedObject(entity: table!, insertInto: managedContext)
                    if let id = assetId as? String {
                        tableData.setValue(id, forKey: "asset_id")
                    }
                    if let fId = faceId as? String {
                        tableData.setValue(fId, forKey: "face_id")
                    }
                    tableData.setValue(false, forKeyPath: "is_deleted")
                    if let obj = objects as? String {
                        tableData.setValue(obj, forKey: "objects")
                    }
                    
                    if locationData.indices.contains(2) {
                        if locationData[2].contains("+"){
                            let countryArray = locationData[2].components(separatedBy: "+")
                            let joinCountry = countryArray.joined(separator: ",")
                            tableData.setValue(joinCountry, forKey: "country")
                        }else {
                            tableData.setValue(locationData[2], forKey: "country")
                        }
                    }
                    else {
                        tableData.setValue("", forKey: "country")
                    }
                    if locationData.indices.contains(1) {
                        if locationData[1].contains("+"){
                            let stateArray = locationData[1].components(separatedBy: "+")
                            let joinState = stateArray.joined(separator: ",")
                            tableData.setValue(joinState, forKey: "state")
                        }else {
                            tableData.setValue(locationData[1], forKey: "state")
                        }
                    }
                    else {
                        tableData.setValue("", forKey: "state")
                    }
                    if locationData.indices.contains(0) {
                        tableData.setValue(locationData[0], forKey: "city")
                    }
                    else {
                        tableData.setValue("", forKey: "city")
                    }
                    
                    do {
                        try managedContext.save()
                    }
                    catch {
                        print("\(error.localizedDescription)---error in saving insertAllPhotos")
                    }
                }
            }
            catch {
                print("\(error.localizedDescription)---error in fetching data from table")
            }
//        }
    }
    
    //insert clustered data (API approach)
    func insertClusteredData(assetId: String, fromAssetId: String, ImageData: String, ofClass: String, vetcor: [Double], object: String, location: [String]) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        //        print(dbUrl?.absoluteString as Any)
        
        let managedContext = self.persistentContainer.newBackgroundContext()
//        appDelegate.persistentContainer.performBackgroundTask { (managedContext) in
            let fetchRequest =
                NSFetchRequest<NSManagedObject>(entityName: "ClusterTable")
            fetchRequest.predicate = NSPredicate(format: "asset_id = %@", assetId)
            do {
                let test = try managedContext.fetch(fetchRequest)
                if test.isEmpty {
                    let table = NSEntityDescription.entity(forEntityName: "ClusterTable", in: managedContext)
                    let tableData = NSManagedObject(entity: table!, insertInto: managedContext)
                    tableData.setValue(assetId, forKey: "asset_id")
                    tableData.setValue(fromAssetId, forKey: "from_asset_id")
                    tableData.setValue(ImageData, forKey: "image_data")
                    tableData.setValue(ofClass, forKey: "of_class")
                    tableData.setValue(object, forKey: "objects")
                    if location.indices.contains(0) {
                        tableData.setValue(location[0], forKey: "city")
                    }
                    else {
                        tableData.setValue("", forKey: "city")
                    }
                    if location.indices.contains(1) {
                        tableData.setValue(location[1], forKey: "state")
                    }
                    else {
                        tableData.setValue("", forKey: "state")
                    }
                    if location.indices.contains(2) {
                        tableData.setValue(location[2], forKey: "country")
                    }
                    else {
                        tableData.setValue("", forKey: "country")
                    }
                    tableData.setValue(false, forKeyPath: "is_deleted")
                    do {
                        try managedContext.save()
                    }
                    catch {
                        print("\(error.localizedDescription)---error in saving insertClusteredData")
                    }
                }
            }
            catch {
                print("\(error.localizedDescription)---error in fetching data from table")
            }
//        }
        //        return true
    }
    
    func insertUserData(imageData: String, ofClass: String, assetId: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let managedContext = self.persistentContainer.viewContext
        appDelegate.persistentContainer.performBackgroundTask { (managedContext) in
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "UsersTable")
            fetchRequest.predicate = NSPredicate(format: "of_class = %@", ofClass)
            do {
                let test = try managedContext.fetch(fetchRequest)
                if test.isEmpty {
                    let table = NSEntityDescription.entity(forEntityName: "UsersTable", in: managedContext)
                    let tableData = NSManagedObject(entity: table!, insertInto: managedContext)
                    tableData.setValue(ofClass, forKey: "of_class")
                    tableData.setValue(imageData, forKey: "image_data")
                    tableData.setValue(assetId, forKey: "asset_id")
                    do {
                        try managedContext.save()
                    }
                    catch {
                        print("\(error.localizedDescription)---error in saving insertUserData")
                    }
                }
            }
            catch {
                print("\(error.localizedDescription)---error in fetching data from table")
            }
        }
    }
    
    func checkAssetIdAvailability(assetID: String) -> Bool {
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "AllImagesTable")
        fetchRequest.predicate = NSPredicate(format: "asset_id = %@", assetID)
        do {
            let result = try managedContext.fetch(fetchRequest)
            if !result.isEmpty {
                return true
            }
        }
        catch let error as NSError {
            print("error in checking asset id \(error.localizedDescription)")
        }
        return false
    }
    
    func checkAssetIdAvailabilityFaceCropping(assetID: String) -> Bool {
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
        fetchRequest.predicate = NSPredicate(format: "from_asset_id = %@", assetID)
        do {
            let result = try managedContext.fetch(fetchRequest)
            if !result.isEmpty {
                return true
            }
        }
        catch let error as NSError {
            print("error in checking asset id \(error.localizedDescription)")
        }
        return false
    }
    
    func checkAssetIdAvailabilityFaceDetection(assetID: String) -> Bool {
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
        fetchRequest.predicate = NSPredicate(format: "asset_id = %@", assetID)
        do {
            let result = try managedContext.fetch(fetchRequest)
            if !result.isEmpty {
                return true
            }
        }
        catch let error as NSError {
            print("error in checking asset id \(error.localizedDescription)")
        }
        return false
    }
    
    func fetchAssetId(p_key: Int) -> String {
        var id = ""
        var key = p_key
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
        if self.fetchP_Key() <= p_key {
            key = self.fetchP_Key() - 1
        }
        //        else {
        //
        //        }
        fetchRequest.predicate = NSPredicate(format: "p_key == \(key)")
        do {
            let result = try managedContext.fetch(fetchRequest)
            if !result.isEmpty {
                for data in result {
                    id = data.value(forKey: "asset_id") as! String
                }
            }
        }
        catch let error as NSError {
            print("error in checking asset id \(error.localizedDescription)")
        }
        //        }
        
        return id.replacingOccurrences(of: "/", with: "#")
    }
    
    func fetchOnlyImageDataFromDB(offset: Int, limit: Int) -> [String: Any]
    {
        
        //        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        //        print(dbUrl?.absoluteString as Any)
        var data = [String:Any]()
        //1
        let managedContext = self.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
        fetchRequest.fetchOffset = offset
        fetchRequest.fetchLimit = limit
        //        fetchRequest.predicate = NSPredicate(format: "entityId > %@", After)
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {
                let assetId = result.value(forKey: "asset_id") as! String
                let vector = result.value(forKey: "image_data") as! String
                let resultData = assetId.replacingOccurrences(of: "/", with: "#")
                data[resultData] = vector // data[assetId]
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return data
    }
    
    func fetchPerticularP_key(assetId: String) -> Int {
        var key = 0
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
        fetchRequest.predicate = NSPredicate(format: "asset_id = %@", assetId)
        do {
            let test = try managedContext.fetch(fetchRequest)
            if !test.isEmpty {
                key = test[test.count - 1].value(forKey: "p_key") as! Int
            }
        }
        catch {
            print("\(error.localizedDescription)---error in getting key")
        }
        return key
    }
    
    // fecting image data
    func fetchOnlyImageDataFromDB() -> [String: Any]
    {
        var data = [String:Any]()
        //1
        let managedContext = self.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
        
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {
                if let asset = result.value(forKey: "asset_id") as? String {
                    let assetId = asset
                    let vector = result.value(forKey: "image_data") as! String
                    let resultData = assetId.replacingOccurrences(of: "/", with: "#")
                    data[resultData] = vector // data[assetId]
                }
                
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return data
    }
    
    // fetching base64 of perticular image
    func fetchImageDataFromDB(assetId: String) -> String
    {
        var data = String()
        //1
        
        
        let managedContext = self.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
        fetchRequest.predicate = NSPredicate(format: "asset_id = %@", assetId)
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {
                //                var faceId = ""
                //                if let faceData = result.value(forKey: "face_id") {
                //                    faceId = faceData as! String
                //                }
                let imageData = result.value(forKey: "image_data") as! String
                data.append(imageData)
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return data
    }
    
    // Fetch location,object details of image
    func fetchDataFromDB(assetId: String) -> [String: String]
    {
        var data = [String: String]()
        //1
        //        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        let managedContext = self.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "AllImagesTable")
        fetchRequest.predicate = NSPredicate(format: "asset_id = %@", assetId)
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {
                //                var faceId = ""
                //                if let faceData = result.value(forKey: "face_id") {
                //                    faceId = faceData as! String
                //                }
                data["object"] = result.value(forKey: "objects") as? String
                data["city"] = result.value(forKey: "city") as? String
                data["state"] = result.value(forKey: "state") as? String
                data["country"] = result.value(forKey: "country") as? String
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return data
    }
    
    //fetch auto Suggestions from ClusterTable
    
    func fetchSuggestions(ofClass: String, search: String, tableName: String, previousSearch: [String], assetIdArray:[String]) -> [String] {
        var data = [String]()
        let stringToSearch = search.uppercased()
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: tableName)
        let prediateForDeleted = NSPredicate(format: "is_deleted == %@", NSNumber(value: false))
        let predicateForClass = NSPredicate(format: "of_class = %@", ofClass)
        
        let predicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateForClass, prediateForDeleted])
        let predicateObject = NSPredicate(format: "objects CONTAINS[C] %@", stringToSearch)
        let predicateCity = NSPredicate(format: "city CONTAINS[C] %@", stringToSearch)
        let predicateState = NSPredicate(format: "state CONTAINS[C] %@", stringToSearch)
        let predicateCountry = NSPredicate(format: "country CONTAINS[C] %@", stringToSearch)
        let predicate2 = NSCompoundPredicate(orPredicateWithSubpredicates: [predicateCity,predicateState, predicateObject, predicateCountry])
        if tableName != "ClusterTable" {
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [prediateForDeleted, predicate2])
        }
        else {
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
        }
        //        fetchRequest.predicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicateObject, predicateCity, predicateState, predicateCountry])
        //        fetchRequest.predicate = predicateObject
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {   var assetId = ""
                if tableName != "ClusterTable" {
                    assetId = (result.value(forKey: "asset_id") as? String)!
                }
                else {
                    assetId = (result.value(forKey: "from_asset_id") as? String)!
                }
                if let object = result.value(forKey: "objects") as? String {
                    if object.contains(",") {
                        let array = object.components(separatedBy: ",")
                        for string in array {
                            if !data.contains(string) && !string.isEmpty && string.hasPrefix(stringToSearch) && !previousSearch.contains(string.localizedCapitalized) {
                                if assetIdArray.isEmpty {
                                    data.append(string.localizedCapitalized)
                                }else if assetIdArray.contains(assetId) {
                                    data.append(string.localizedCapitalized)
                                }
                            }
                        }
                    }
                    else {
                        if !data.contains(object) && !object.isEmpty && object.hasPrefix(stringToSearch) && !previousSearch.contains(object.localizedCapitalized) {
                            if assetIdArray.isEmpty {
                                data.append(object.localizedCapitalized)
                            }else if assetIdArray.contains(assetId) {
                                data.append(object.localizedCapitalized)
                            }
                        }
                    }
                }
                if let city = result.value(forKey: "city") as? String {
                    if !data.contains(city) && !city.isEmpty && city.hasPrefix(stringToSearch) && !previousSearch.contains(city.localizedCapitalized) {
                        if assetIdArray.isEmpty {
                            data.append(city.localizedCapitalized)
                        }else if assetIdArray.contains(assetId) {
                            data.append(city.localizedCapitalized)
                        }
                    }
                }
                if let state = result.value(forKey: "state") as? String {
                    if state.contains(",") {
                        let array = state.components(separatedBy: ",")
                        for string in array {
                            if !data.contains(string) && !string.isEmpty && string.hasPrefix(stringToSearch) && !previousSearch.contains(string.localizedCapitalized) {
                                if assetIdArray.isEmpty {
                                    if string.count == 2{
                                        data.append(string)
                                    }else{
                                        data.append(string.localizedCapitalized)
                                    }
                                }else if assetIdArray.contains(assetId) {
                                    if string.count == 2{
                                        data.append(string)
                                    }else{
                                        data.append(string.localizedCapitalized)
                                    }
                                }
                            }
                        }
                    }
                    else {
                        if !data.contains(state) && !state.isEmpty && state.hasPrefix(stringToSearch) && !previousSearch.contains(state.localizedCapitalized) {
                            if assetIdArray.isEmpty {
                                data.append(state.localizedCapitalized)
                            }else if assetIdArray.contains(assetId) {
                                data.append(state.localizedCapitalized)
                            }
                        }
                    }
                }
//                    if !data.contains(state) && !state.isEmpty && state.hasPrefix(stringToSearch) && !previousSearch.contains(state.localizedCapitalized) {
//                        if assetIdArray.isEmpty {
//                            data.append(state.localizedCapitalized)
//                        }else if assetIdArray.contains(assetId) {
//                            data.append(state.localizedCapitalized)
//                        }
//                    }
//                }
                if let country = result.value(forKey: "country") as? String {
                    if country.contains(",") {
                        let array = country.components(separatedBy: ",")
                        for string in array {
                            if !data.contains(string) && !string.isEmpty && string.hasPrefix(stringToSearch) && !previousSearch.contains(string.localizedCapitalized) {
                                if assetIdArray.isEmpty {
                                    if string.count == 2{
                                        data.append(string)
                                    }else{
                                        data.append(string.localizedCapitalized)
                                    }
                                }else if assetIdArray.contains(assetId) {
                                    if string.count == 2{
                                        data.append(string)
                                    }else{
                                        data.append(string.localizedCapitalized)
                                    }
                                }
                            }
                        }
                    }
                    else {
                        if !data.contains(country) && !country.isEmpty && country.hasPrefix(stringToSearch) && !previousSearch.contains(country.localizedCapitalized) {
                            if assetIdArray.isEmpty {
                                data.append(country.localizedCapitalized)
                            }else if assetIdArray.contains(assetId) {
                                data.append(country.localizedCapitalized)
                            }
                        }
                    }
//                    if !data.contains(country) && !country.isEmpty && country.hasPrefix(stringToSearch) && !previousSearch.contains(country.localizedCapitalized) {
//                        if assetIdArray.isEmpty {
//                            data.append(country.localizedCapitalized)
//                        }else if assetIdArray.contains(assetId) {
//                            data.append(country.localizedCapitalized)
//                        }
//                    }
                }
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        //        for text in previousSearch {
        //            for string in data {
        //                if string.contains(text) {
        //                    if let index = data.index(of: text) {
        //                        data.remove(at: index)
        //                    }
        //                }
        //            }
        //        }
        return Array(Set(data))
    }
    
    // fetching clustered data of perticular class
    func fetchClusteringFromDB(ofClass: String, search: [String], tableName: String, previousData: [String]) -> [String]
    {
        var data = [String]()
        var searchKeyword = ""
        if !search.isEmpty {
            searchKeyword = search.last!
        }
        
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: tableName)
        let prediateForDeleted = NSPredicate(format: "is_deleted == %@", NSNumber(value: false))
        let predicateForClass = NSPredicate(format: "of_class = %@", ofClass)
        let predicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateForClass, prediateForDeleted])
        if !search.isEmpty && !search.contains("") {
//            for keyword in search {
                let predicateObject = NSPredicate(format: "objects CONTAINS[C] %@", searchKeyword)
                let predicateCity = NSPredicate(format: "city CONTAINS[C] %@", searchKeyword)
                let predicateState = NSPredicate(format: "state CONTAINS[C] %@", searchKeyword)
                let predicateCountry = NSPredicate(format: "country CONTAINS[C] %@", searchKeyword)
                let predicate2 = NSCompoundPredicate(orPredicateWithSubpredicates: [predicateCity,predicateState, predicateObject, predicateCountry])
                if ofClass != "1111" {
                    fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
                }
                else {
                    fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2, prediateForDeleted])
                }
                do {
                    let test = try managedContext.fetch(fetchRequest)
                    if !test.isEmpty {
                        do
                        {
                            let results = try managedContext.fetch(fetchRequest)
                            var imageData1 = [String]()
//                            var imageData2 = [String]()
                            for result in results
                            {
                                var image = ""
                                if tableName == "AllImagesTable" {
                                    image = result.value(forKey: "asset_id") as! String
                                }
                                else {
                                    image = result.value(forKey: "from_asset_id") as! String
                                }
                                if previousData.isEmpty {
                                    data.append(image)
                                }else if previousData.contains(image) {
                                    data.append(image)
                                }
//                                data.append(image)
                            }
//                            data = imageData1
//                            let dataSet1 = Set(data)
//                            let dataSet2 = Set(imageData1)
//                            data = Array(dataSet1.intersection(dataSet2))
                        }
                        catch let error as NSError {
                            print("Could not fetch. \(error), \(error.userInfo)")
                        }
                    }
                }
                catch {
                    print("error in fetching data from table")
                }
//            }
//            let listSet = Set(data)
//            let findListSet = Set(previousData)
//
//            let allElemsContained = findListSet.isSubset(of: listSet)
//            if !allElemsContained {
//                data.removeAll()
//            }
        }
        else {
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
            do {
                let test = try managedContext.fetch(fetchRequest)
                if !test.isEmpty {
                    do
                    {
                        let results = try managedContext.fetch(fetchRequest)
                        
                        for result in results
                        {
                            var image = ""
                            if tableName == "AllImagesTable" {
                                image = result.value(forKey: "asset_id") as! String
                            }
                            else {
                                image = result.value(forKey: "from_asset_id") as! String
                            }
                            data.append(image)
                        }
                    }
                    catch let error as NSError {
                        print("Could not fetch. \(error), \(error.userInfo)")
                    }
                }
            }
            catch {
                print("error in fetching data from table")
            }
        }
        
        return data
    }
    
    func fetchDeletedImages() -> [String] {
        var data = [String]()
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
        fetchRequest.predicate = NSPredicate(format: "is_deleted == %@", NSNumber(value: true))
        do {
            let results = try managedContext.fetch(fetchRequest)
            for result in results {
                data.append(result.value(forKey: "asset_id") as! String)
            }
        }
        catch{
            print("error in fetching data from table")
        }
        return data
    }
    
    //fetching all images data
    func fetchAllImagesDataFromDB() -> [String]
    {
        var data = [String]()
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "AllImagesTable")
        fetchRequest.predicate = NSPredicate(format: "is_deleted == %@", NSNumber(value: false))
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for result in results
            {
                let assetId = result.value(forKey: "asset_id") as! String
                data.append(assetId)
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return data
    }
    
    func fetchP_Key() -> Int {
        var key = 0
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
        fetchRequest.propertiesToFetch = ["p_key"]
        do {
            let test = try managedContext.fetch(fetchRequest)
            if test.indices.contains(0) {
                key = test[test.count - 1].value(forKey: "p_key") as! Int
                print("last primary key--------\(key)")
                //                for result in test {
                //                    key = result.value(forKey: "p_key") as! Int
                ////                    print(result)
                //                }
            }
        }
        catch {
            print("error in fetching data from table")
        }
        return key + 1
    }
    
    
    func fetchFaceDetectionStatus(assetId: String) -> Int {
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "AllImagesTable")
        fetchRequest.predicate = NSPredicate(format: "asset_id = %@", assetId)
        var status = Int()
//        fetchRequest.propertiesToFetch = ["p_key"]
        do {
            let test = try managedContext.fetch(fetchRequest)
            if test.indices.contains(0) {
//                for result in test {
//                    status = test[0].value(forKey: "face_detection_status") as! Int
                if let newstatus = test[0].value(forKey: "face_detection_status") as? Int
                {
                    status = newstatus
                }                
//                }
            }
        }
        catch {
            print("error in fetching data from table")
        }
        return status
    }
    
    func fetchLastAssetId() -> String {
        var key = ""
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
        fetchRequest.propertiesToFetch = ["asset_id"]
        do {
            let test = try managedContext.fetch(fetchRequest)
            if test.indices.contains(0) {
                key = test[test.count - 1].value(forKey: "asset_id") as! String
                print(key)
                //                for result in test {
                //                    key = result.value(forKey: "p_key") as! Int
                ////                    print(result)
                //                }
            }
        }
        catch {
            print("error in fetching data from table")
        }
        return key.replacingOccurrences(of: "/", with: "#")
    }
    
    // fetching all clustered face data
    func fetchUsersDataFromDB() -> [UIImage]
    {
        var data = [UIImage]()
        var faceIdArray = [String]()
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "UsersTable")     //ClusterTable
        fetchRequest.predicate = NSPredicate(format: "asset_id != %@", "")
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for result in results
            {
                let faceId = result.value(forKey: "of_class") as! String
                //                let assetId = result.value(forKey: "asset_id") as! String
                //                let vector = result.value(forKey: "vector_data") as! [Double]
                //                let name = result.value(forKey: "name") as! String
                let img = self.convertBase64ToImage(imageString: result.value(forKey: "image_data") as! String)
                let image = img
                //                let resultData:[String: Any] = ["asset_Id": assetId,
                //                                                "image": image,
                //                                                "ofClass": faceId]
                faceIdArray.append(faceId)
                if faceIdArray.count == 1 {
                    data.append(image)
                }
                else {
                    var count = 0
                    for name in faceIdArray {
                        if name == faceId {
                            count += 1
                        }
                    }
                    if count < 2 {
                        data.append(image)
                    }
                }
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return data
    }
    
    func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    //updating face count in an image
    func updateFacesCount(assetId: String, count: Int) {
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "AllImagesTable")
        fetchRequest.predicate = NSPredicate(format: "asset_id = %@", assetId)
        do {
            let test = try managedContext.fetch(fetchRequest)
            if test.indices.contains(0) {
                let update = test[0]
                update.setValue(count, forKey: "face_count")
                do {
                    try managedContext.save()
                }
                catch {
                    print("error in saving face count")
                }
            }
        }
        catch{
            print("error in fetching data from table")
        }
    }
    
    func updateFacesDetectionStatus(assetId: String, status: Int) { // 1 for cropped, 2 for go through, 3 by default
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "AllImagesTable")
        fetchRequest.predicate = NSPredicate(format: "asset_id = %@", assetId)
        do {
            let test = try managedContext.fetch(fetchRequest)
            if test.indices.contains(0) {
                let update = test[0]
                update.setValue(status, forKey: "face_detection_status")
                do {
                    try managedContext.save()
                }
                catch {
                    print("error in saving face count")
                }
            }
        }
        catch{
            print("error in fetching data from table")
        }
    }
    
    // updating face_id of an image
    func updateDataWithFaceId(assetId : String, faceId: String, name: String)
    {
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
        fetchRequest.predicate = NSPredicate(format: "from_asset_id = %@", assetId)
        do {
            let test = try managedContext.fetch(fetchRequest)
            if test.indices.contains(0) {
                for data in test {
                    let update = data
                    update.setValue(faceId, forKey: "face_id")
                    do {
                        try managedContext.save()
                    }
                    catch {
                        print("error in saving image data")
                    }
                }
            }
        }
        catch {
            print("error in fetching data from table")
        }
        
        let fetchRequestForAllImages =
            NSFetchRequest<NSManagedObject>(entityName: "AllImagesTable")
        fetchRequestForAllImages.predicate = NSPredicate(format: "asset_id = %@", assetId)
        do {
            let test = try managedContext.fetch(fetchRequestForAllImages)
            if test.indices.contains(0) {
                let update = test[0]
                update.setValue(faceId, forKey: "face_id")
                do {
                    try managedContext.save()
                }
                catch {
                    print("error in saving image data")
                }
            }
        }
        catch {
            print("error in fetching data from table")
        }
    }
    
    func deleteImageData(assetId: String) {
        let managedContext = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ClusterTable")
        fetchRequest.predicate = NSPredicate(format: "from_asset_id = %@", assetId)
        do {
            let test = try managedContext.fetch(fetchRequest)
            if test.indices.contains(0) {
                for data in test {
                    managedContext.delete(data)
                }
                do {
                    try managedContext.save()
                }
                catch {
                    print("error in delete")
                }
            }
        }
        catch{
            print("no data")
        }
        
        let fetchRequestForAll = NSFetchRequest<NSManagedObject>(entityName: "AllImagesTable")
        fetchRequestForAll.predicate = NSPredicate(format: "asset_id = %@", assetId)
        do {
            let test = try managedContext.fetch(fetchRequestForAll)
            if test.indices.contains(0) {
                for data in test {
                    managedContext.delete(data)
                }
                do {
                    try managedContext.save()
                }
                catch {
                    print("error in delete")
                }
            }
        }
        catch {
            print("no data")
        }
        
        let fetchRequestForFace = NSFetchRequest<NSManagedObject>(entityName: "FaceDetectionTable")
        fetchRequestForFace.predicate = NSPredicate(format: "from_asset_id = %@", assetId)
        do {
            let test = try managedContext.fetch(fetchRequestForFace)
            if test.indices.contains(0) {
                for data in test {
                    managedContext.delete(data)
                }
                do {
                    try managedContext.save()
                }
                catch {
                    print("error in delete")
                }
            }
        }
        catch {
            print("no data")
        }
        let fetchRequestForUsers = NSFetchRequest<NSManagedObject>(entityName: "UsersTable")
        fetchRequestForUsers.predicate = NSPredicate(format: "asset_id = %@", assetId)
        do {
            let test = try managedContext.fetch(fetchRequestForUsers)
            if test.indices.contains(0) {
                for data in test {
                    managedContext.delete(data)
                }
                do {
                    try managedContext.save()
                }
                catch {
                    print("error in delete")
                }
            }
        }
        catch{
            print("no data")
        }
        NotificationCenter.default.post(name: NSNotification.Name("updateDataAfterDelete"), object: nil)
    }
}

extension Array where Element : Equatable {
    var unique: [Element] {
        var uniqueValues: [Element] = []
        forEach { item in
            if !uniqueValues.contains(item) {
                uniqueValues += [item]
            }
        }
        return uniqueValues
    }
}
