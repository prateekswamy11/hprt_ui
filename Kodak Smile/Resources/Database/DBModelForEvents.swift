//
//  DBModelForEvents.swift
//  Kodak Smile
//
//  Created by MAXIMESS152 on 01/10/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation
import CoreData

class DBModelForEvents{
    
    let appDelegateVar = appDelegate()
    static let shared = DBModelForEvents()
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    func saveEventData(id: Int, eventVersion: Double, name: String, updateVersion: Double){
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Events")
        fetchRequest.predicate = NSPredicate(format: "id == \(id)")
        do {
            let test = try managedContext.fetch(fetchRequest)
            if test.isEmpty {
                let table = NSEntityDescription.entity(forEntityName: "Events", in: managedContext)
                let tableData = NSManagedObject(entity: table!, insertInto: managedContext)
                tableData.setValue(id, forKey: "id")
                tableData.setValue(eventVersion, forKey: "event_version")
                tableData.setValue(name, forKey: "name")
                tableData.setValue(updateVersion, forKey: "update_version")
                do {
                    try managedContext.save()
                }catch let error as NSError {
                    print("Could not fetch. \(error), \(error.userInfo)")
                }
            }
        }catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func saveStickersFrames(id: Int, eventId: Int, name: String, thumbnail: String, image: String, eventName: String, isSticker: Bool){
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: isSticker ? "EventStickers": "EventFrames")
            fetchRequest.predicate = NSPredicate(format: "id == \(id)")
            do {
                let test = try managedContext.fetch(fetchRequest)
                if test.isEmpty{
                    let table = NSEntityDescription.entity(forEntityName: isSticker ? "EventStickers": "EventFrames", in: managedContext)
                    let tableData = NSManagedObject(entity: table!, insertInto: managedContext)
                    tableData.setValue(id, forKey: "id")
                    tableData.setValue(eventId, forKey: "event_id")
                    tableData.setValue(name, forKey: "name")
                    tableData.setValue(thumbnail, forKey: "thumbnail")
                    tableData.setValue(image, forKey: "image")
                    tableData.setValue(0, forKey: "is_downloaded")
                    tableData.setValue(eventName, forKey: "event_name")
                    do {
                        try managedContext.save()
                    }catch let error as NSError {
                        print("Could not fetch. \(error), \(error.userInfo)")
                    }
                }
            }catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
    }
    
    func fetchEventID() -> [Int]{
        var ids = [Int]()
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Events")
        do{
            let results = try managedContext.fetch(fetchRequest)
            for data in results{
                if let id = data.value(forKey: "id") as? Int{
                    ids.append(id)
                }
            }
        }catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return ids
    }
    
    func fetchEventStickerID(eventId: Int, isSticker: Bool) -> [Int]{
        var ids = [Int]()
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: isSticker ? "EventStickers": "EventFrames")
        fetchRequest.predicate = NSPredicate(format: "event_id == \(eventId)")
        do{
            let results = try managedContext.fetch(fetchRequest)
            for data in results{
                if let id = data.value(forKey: "id") as? Int{
                    ids.append(id)
                }
            }
        }catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return ids
    }
    
    func fetchEventVersion() -> String{
        var version = String()
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Events")
        do{
            let results = try managedContext.fetch(fetchRequest)
//            for data in results{
            if !results.isEmpty{
                if let id = results[0].value(forKey: "update_version") as? String{
                    version = id
                }
            }else{
                version = "0.0"
            }
        }catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return version
    }
    
    func fetchStickersFrmaesThumbnail(isSticker: Bool)-> [[String: Any]]{
        var stickers = [[String: Any]]()
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSManagedObject>(entityName: isSticker ? "EventStickers": "EventFrames")
        do{
            let results = try managedContext.fetch(fetchrequest)
            for data in results{
                let dict = ["id": data.value(forKey: "id") as! Int,
                            "thumbnail": data.value(forKey: "thumbnail") as! String,
                            "isDownloaded": data.value(forKey: "is_downloaded") as! Int,
                            "image": data.value(forKey: "image") as! String,
                            "name": data.value(forKey: "name") as! String,
                            "eventName": data.value(forKey: "event_name") as! String] as [String : Any]
                if WebserviceModelClass().isInternetAvailable(){
                    stickers.append(dict)
                }else if data.value(forKey: "is_downloaded") as! Int == 1{
                    stickers.append(dict)
                }
            }
        }catch let error as NSError{
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return stickers
    }
    
    func fetchStickerFrameImageUrl(id: Int, isSticker: Bool)-> String{
        var url: String = ""
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: isSticker ? "EventStickers": "EventFrames")
        fetchRequest.predicate = NSPredicate(format: "id== \(id)")
        do{
            let results = try managedContext.fetch(fetchRequest)
            for data in results{
                url = data.value(forKey: "image") as! String
            }
        }catch let error as NSError{
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return url
    }
    
    func fetchDownloadedStickerFrame(id: Int, isSticker: Bool)-> String{
        var imgName: String = ""
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchReuest = NSFetchRequest<NSManagedObject>(entityName: isSticker ? "EventStickers": "EventFrames")
        fetchReuest.predicate = NSPredicate(format: "id== \(id)")
        do{
            let results = try managedContext.fetch(fetchReuest)
            if !results.isEmpty{
                imgName = results[0].value(forKey: "downloaded_image_name") as! String
            }
        }catch let error as NSError{
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return imgName
    }
    
    func fetchStickerFrameDownloadStste(id: Int, isSticker: Bool)-> Int{
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: isSticker ? "EventStickers": "EventFrames")
        fetchRequest.predicate = NSPredicate(format: "id== \(id)")
        do{
            let results = try managedContext.fetch(fetchRequest)
            if !results.isEmpty{
                return results[0].value(forKey: "is_downloaded") as! Int
            }
        }catch let error as NSError{
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return 0
    }
    
    func fetchStickerFrameupdateVersion(id: Int, isSticker: Bool)-> Int{
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: isSticker ? "EventStickers": "EventFrames")
        fetchRequest.predicate = NSPredicate(format: "id== \(id)")
        do{
            let results = try managedContext.fetch(fetchRequest)
            if !results.isEmpty{
                return results[0].value(forKey: "is_downloaded") as! Int
            }
        }catch let error as NSError{
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return 0
    }
    
    func updateStickerFrameDownloadState(id: Int, imgName: String, isSticker: Bool, completion: @escaping(Bool)->()){
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managesContext = delegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: isSticker ? "EventStickers": "EventFrames")
        fetchRequest.predicate = NSPredicate(format: "id == \(id)")
        do{
            let results = try managesContext.fetch(fetchRequest)
            if results.indices.contains(0){
                results[0].setValue(imgName, forKey: "downloaded_image_name")
                results[0].setValue(1, forKey: "is_downloaded")
                do{
                    try managesContext.save()
                    completion(true)
                }catch let error as NSError{
                    print("Could not fetch. \(error), \(error.userInfo)")
                }
            }
        }catch let error as NSError{
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func deleteEvent(id: Int){
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSManagedObject>(entityName: "Events")
        fetchrequest.predicate = NSPredicate(format: "id == \(id)")
        do{
            let test = try managedContext.fetch(fetchrequest)
            if !test.isEmpty{
                for event in test{
                    managedContext.delete(event)
                }
                do{
                    try managedContext.save()
                }catch let error as NSError{
                    print("Could not fetch. \(error), \(error.userInfo)")
                }
            }
        }catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func deleteEventStickers(id: Int){
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSManagedObject>(entityName: "EventStickers")
        fetchrequest.predicate = NSPredicate(format: "id == \(id)")
        self.deleteEventsData(id: id)
        do{
            let test = try managedContext.fetch(fetchrequest)
            if !test.isEmpty{
                for event in test{
                    managedContext.delete(event)
                }
                do{
                    try managedContext.save()
                }catch let error as NSError{
                    print("Could not fetch. \(error), \(error.userInfo)")
                }
            }
        }catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func deleteEventFrames(id: Int){
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSManagedObject>(entityName: "EventFrames")
        fetchrequest.predicate = NSPredicate(format: "id == \(id)")
        self.deleteEventsData(id: id)
        do{
            let test = try managedContext.fetch(fetchrequest)
            if !test.isEmpty{
                for event in test{
                    managedContext.delete(event)
                }
                do{
                    try managedContext.save()
                }catch let error as NSError{
                    print("Could not fetch. \(error), \(error.userInfo)")
                }
            }
        }catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func deleteEventsData(id: Int){
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchRequestForStickers = NSFetchRequest<NSManagedObject>(entityName: "EventStickers")
        let fetchRequetstForFrames = NSFetchRequest<NSManagedObject>(entityName: "EventFrames")
        fetchRequestForStickers.predicate = NSPredicate(format: "event_id == \(id)")
        fetchRequetstForFrames.predicate = NSPredicate(format: "event_id == \(id)")
        do{
           let test = try managedContext.fetch(fetchRequestForStickers)
            if !test.isEmpty{
                for data in test{
                    managedContext.delete(data)
                }
                do{
                    try managedContext.save()
                }catch let error as NSError{
                    print("Could not fetch. \(error), \(error.userInfo)")
                }
            }
        }catch let error as NSError{
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        do{
            let test = try managedContext.fetch(fetchRequetstForFrames)
            if !test.isEmpty{
                for data in test{
                    managedContext.delete(data)
                }
                do{
                    try managedContext.save()
                }catch let error as NSError{
                    print("Could not fetch. \(error), \(error.userInfo)")
                }
            }
        }catch let error as NSError{
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
