//
//  DBLayer.swift
//  FacebookSwift
//
//  Created by maximess142 on 31/01/18.
//  Copyright © 2018 maximess142. All rights reserved.
//

import UIKit
import CoreData

class DBLayer: NSObject
{
    // Facebook
    //MARK: - INSERT
    func insertFacebookAlbum(facebookAlbumPara: FacebookAlbum_Para) //-> Para_Project
    {
        //Prepare Values to save
        //Greater improvement in performance
        //let projectId = getMaxID("Projects",idName: "projectId") + 1
        
        let album_id = facebookAlbumPara.fbAlbumId
        let album_name = facebookAlbumPara.fbAlbumName
        let album_photos_count = facebookAlbumPara.fbAlbumPhotosCount
        let album_thumbnail_url = facebookAlbumPara.fbAlbumThumbnailUrl
        
        //1
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        print(dbUrl?.absoluteString as Any)
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let entity =  NSEntityDescription.entity(forEntityName: "Facebook",
                                                 in:managedContext)
        
        let facebook = NSManagedObject(entity: entity!,
                                       insertInto: managedContext)
        
        //3
        facebook.setValue(album_id, forKey: "album_id")
        facebook.setValue(album_name, forKey: "album_name")
        facebook.setValue(album_photos_count, forKey: "album_photos_count")
        facebook.setValue(album_thumbnail_url, forKey: "album_thumbnail_url")
        self.insertFacebookPhotosInAlbum(facebookAlbumPara: facebookAlbumPara)
        
        //4
        do
        {
            try managedContext.save()
        }
            //5
        catch
        {
            print("Error in storing Facebook Album")
        }
    }
    
    func insertFacebookPhotosInAlbum(facebookAlbumPara: FacebookAlbum_Para)
    {
        //1
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        print(dbUrl?.absoluteString as Any)
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let entity =  NSEntityDescription.entity(forEntityName: "FacebookAlbumPhotos",
                                                 in:managedContext)
        
        let photoUrlArray = facebookAlbumPara.fbAlbumPhotoLinkArray
        let album_id = facebookAlbumPara.fbAlbumId
        
        for photoUrl in photoUrlArray
        {
            let facebook = NSManagedObject(entity: entity!,
                                           insertInto: managedContext)
            
            //3
            facebook.setValue(album_id, forKey: "album_id")
            facebook.setValue(photoUrl, forKey: "photo_url")
            
            //4
            do
            {
                try managedContext.save()
            }
                //5
            catch
            {
                print("Error in storing Facebook Album")
            }
        }
    }
    
    // MARK: - Instagram
    func insertInstagramPhotos(urlArray : [String]) //-> Para_Project
    {
        //1
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        print(dbUrl?.absoluteString as Any)
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let entity =  NSEntityDescription.entity(forEntityName: "Instagram",
                                                 in:managedContext)
        
        for photoUrl in urlArray
        {
            let instagram = NSManagedObject(entity: entity!,
                                            insertInto: managedContext)
            
            //3
            instagram.setValue(photoUrl, forKey: "photo_url")
            
            //4
            do
            {
                try managedContext.save()
            }
                //5
            catch
            {
                print("Error in storing Facebook Album")
            }
        }
    }
    
    
    // MARK: Google Photos
    // Google
    //MARK: - INSERT
    
    func insertGooglePhotosUrl(urlArray : [String]) //-> Para_Project
    {
        //1
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        print(dbUrl?.absoluteString as Any)
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let entity =  NSEntityDescription.entity(forEntityName: "GooglePhotosUrl",
                                                 in:managedContext)
        
        for photoUrl in urlArray
        {
            let GooglePhotos = NSManagedObject(entity: entity!,
                                            insertInto: managedContext)
            
            //3
            GooglePhotos.setValue(photoUrl, forKey: "photo_url")
            
            //4
            do
            {
                try managedContext.save()
            }
                //5
            catch
            {
                print("Error in storing Google Album")
            }
        }
    }
    
    func insertGoogleAlbum(glAlbumPara: GooglePhotosPara) //-> Para_Project
    {
        //Prepare Values to save
        //Greater improvement in performance
        //let projectId = getMaxID("Projects",idName: "projectId") + 1
        
        let album_id = glAlbumPara.googleAlbumId
        let album_name = glAlbumPara.googleAlbumName
        
        //1
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        print(dbUrl?.absoluteString as Any)
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let entity =  NSEntityDescription.entity(forEntityName: "Google",
                                                 in:managedContext)
        
        let google = NSManagedObject(entity: entity!,
                                     insertInto: managedContext)
        
        //3
        google.setValue(album_id, forKey: "album_id")
        google.setValue(album_name, forKey: "album_name")
        self.insertGooglePhotosInAlbum(glAlbumPara: glAlbumPara)
        
        //4
        do
        {
            try managedContext.save()
        }
            //5
        catch
        {
            print("Error in storing Google Album", error)
        }
    }
    
    func insertGooglePhotosInAlbum(glAlbumPara: GooglePhotosPara)
    {
        //1
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        print(dbUrl?.absoluteString as Any)
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let entity =  NSEntityDescription.entity(forEntityName: "GoogleAlbumPhotos",
                                                 in:managedContext)
        
        /*
         let photoUrlArray = glAlbumPara.googleAlbumPhotoLinkArray
         let album_id = glAlbumPara.googleAlbumId
         
         for photoUrl in photoUrlArray
         {
         let google = NSManagedObject(entity: entity!,
         insertInto: managedContext)
         
         //3
         google.setValue(album_id, forKey: "album_id")
         google.setValue(photoUrl, forKey: "photo_url")
         
         //4
         do
         {
         try managedContext.save()
         }
         //5
         catch
         {
         print("Error in storing Google Album")
         }
         }*/
        
        //managedContext.reset()
        
        let jsonArray = glAlbumPara.googleAlbumPhotoLinkArray//JSON data to be imported into Core Data
        let moc =  managedContext //Our primary context on the main queue
        
        let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateMOC.parent = moc
        
        privateMOC.perform {
            for jsonObject in jsonArray {
                let album_id = glAlbumPara.googleAlbumId
                let photoUrl = jsonObject
                let google = NSManagedObject(entity: entity!,
                                             insertInto: privateMOC)//Managed object that matches the incoming JSON structure
                //update MO with data from the dictionary
                
                //3
                google.setValue(album_id, forKey: "album_id")
                google.setValue(photoUrl, forKey: "photo_url")
            }
            do {
                try privateMOC.save()
                moc.performAndWait {
                    do {
                        try moc.save()
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            } catch {
                fatalError("Failure to save context: \(error)")
            }
//            NotificationCenter.default.post(name: Notification.Name(rawValue: "googleImagesInserted"), object: nil)
        }
    }
    
    // MARK: - Dropbox
    func insertDropboxFilepath(urlArray : String) //-> Para_Project
    {
        //1
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        print(dbUrl?.absoluteString as Any)
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let entity =  NSEntityDescription.entity(forEntityName: "Dropbox",
                                                 in:managedContext)
        
        let dropbox = NSManagedObject(entity: entity!,
                                      insertInto: managedContext)
        
        //3
        let dropboxData = self.fetchDropboxPhotosNamesFromDB()
        if dropboxData.count > 0 {
            if !dropboxData.contains(urlArray) {
                dropbox.setValue(urlArray, forKey: "file_path")
                do
                {
                    try managedContext.save()
                }
                    //5
                catch
                {
                    print("Error in storing Dropbox Album")
                }
            }
        }
        else {
            dropbox.setValue(urlArray, forKey: "file_path")
            do
            {
                try managedContext.save()
            }
                //5
            catch
            {
                print("Error in storing Dropbox Album")
            }
        }
    }
    
    func insertDropboxPhotos(url : String, filePath: String) //-> Para_Project
    {
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        print(dbUrl?.absoluteString as Any)
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Dropbox")
//        for i in 0..<urlArray.count {
            fetchRequest.predicate = NSPredicate(format: "file_path = %@", filePath)
            do {
                let test = try managedContext.fetch(fetchRequest)
                if test.indices.contains(0) {
                    let update = test[0]
                    update.setValue(url, forKey: "image")
                    do {
                        try managedContext.save()
                    }
                    catch {
                        print("error in saving image data")
                    }
                }
            }
            catch {
                print("error in fetching data from table")
        }
        
//        }
//        var photoUrlArray = [String]()
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {
                if result.value(forKey: "image") as? String == nil {
                    guard let appDelegate =
                        UIApplication.shared.delegate as? AppDelegate else {
                            return
                    }
                    let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
                    print(dbUrl?.absoluteString as Any)
                    
                    let managedContext = appDelegate.persistentContainer.viewContext
                    
                    //2
                    let entity =  NSEntityDescription.entity(forEntityName: "Dropbox",
                                                             in:managedContext)
                    
//                    for photoUrl in urlArray
//                    {
                        let dropbox = NSManagedObject(entity: entity!,
                                                      insertInto: managedContext)
                        
                        //3
                        dropbox.setValue(url, forKey: "image")
                        
                        //4
                        do
                        {
                            try managedContext.save()
                            
                        }
                            //5
                        catch
                        {
                            print("Error in storing Dropbox image")
                        }
                }
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
    // MARK: - Fetch
    
    func fetchGooglePhotosFromDB() -> [String]
    {
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "GooglePhotosUrl")
        
        var photoUrlArray = [String]()
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {
                let photoUrl = result.value(forKey: "photo_url") as! String
                
                photoUrlArray.append(photoUrl)
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return photoUrlArray
    }
    
    func fetchFacebookAlbumFromDB() -> [FacebookAlbum_Para]
    {
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Facebook")
        
        var fbAlbumArray = [FacebookAlbum_Para]()
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {
                let albumPara = FacebookAlbum_Para()
                
                albumPara.fbAlbumId = result.value(forKey: "album_id") as! String
                albumPara.fbAlbumName = result.value(forKey: "album_name") as! String
                albumPara.fbAlbumPhotosCount = result.value(forKey: "album_photos_count") as! Int
                albumPara.fbAlbumThumbnailUrl = result.value(forKey: "album_thumbnail_url") as! String
                albumPara.fbAlbumPhotoLinkArray = self.fetchFacebookPhotosAlbumWise(album_id: albumPara.fbAlbumId)
                
                fbAlbumArray.append(albumPara)
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return fbAlbumArray
    }
    
    func fetchFacebookPhotosAlbumWise(album_id : String) -> [String]
    {
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "FacebookAlbumPhotos")
        fetchRequest.predicate = NSPredicate(format: "album_id == %@", album_id)
        
        var photoUrlArray = [String]()
        
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {
                let photo_url = result.value(forKey: "photo_url") as! String
                
                photoUrlArray.append(photo_url)
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return photoUrlArray
    }
    
    func fetchInstagramPhotosFromDB() -> [String]
    {
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Instagram")
        
        var photoUrlArray = [String]()
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {
                let photoUrl = result.value(forKey: "photo_url") as! String
                
                photoUrlArray.append(photoUrl)
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return photoUrlArray
    }
    
    // MARK: - Fetch
    func fetchGoogleAlbumFromDB() -> [GooglePhotosPara]
    {
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Google")
        
        var fbAlbumArray = [GooglePhotosPara]()
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {
                let albumPara = GooglePhotosPara()
                
                albumPara.googleAlbumId = result.value(forKey: "album_id") as! String
                albumPara.googleAlbumName = result.value(forKey: "album_name") as! String
                albumPara.googleAlbumPhotoLinkArray = self.fetchGooglePhotosAlbumWise(album_id: albumPara.googleAlbumId)
                
                fbAlbumArray.append(albumPara)
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return fbAlbumArray
    }
    
    func fetchGooglePhotosAlbumWise(album_id : String) -> [String]
    {
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "GoogleAlbumPhotos")
        fetchRequest.predicate = NSPredicate(format: "album_id == %@", album_id)
        
        var photoUrlArray = [String]()
        
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            print("google pics in foler = ", results.count)
            for result in results
            {
                let photo_url = result.value(forKey: "photo_url") as! String
                
                photoUrlArray.append(photo_url)
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return photoUrlArray
    }
    
    func fetchDropboxPhotosNamesFromDB() -> [String]
    {
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let dbUrl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
        print(dbUrl?.absoluteString as Any)
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Dropbox")
        
        var photoUrlArray = [String]()
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {
                if let value = result.value(forKey: "file_path") {
                    let photoUrl = value as! String
                    //                let image = self.base64Convert(base64String: photoUrl)
                    photoUrlArray.append(photoUrl)
                }
                
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return photoUrlArray
    }
    
    func fetchDropboxPhotosFromDB() -> [UIImage]
    {
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Dropbox")
        
        var photoUrlArray = [UIImage]()
        //3
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for result in results
            {
                if let photo = result.value(forKey: "image") {
                    let photoUrl = photo as! String
                    let image = self.base64Convert(base64String: photoUrl)
                    photoUrlArray.append(image)
                }
                
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return photoUrlArray
    }
    
    func base64Convert(base64String: String?) -> UIImage {
        if (base64String?.isEmpty)! {
            return #imageLiteral(resourceName: "no_image_found")
        }else {
            // !!! Separation part is optional, depends on your Base64String !!!
//            let temp = base64String?.components(separatedBy: ",")
            let dataDecoded : Data = Data(base64Encoded: base64String!, options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            return decodedimage!
        }
    }
    
    //MARK:- DELETE
    func deleteFacebookAlbums()
    {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Facebook")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    func deleteFacebookPhotos()
    {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "FacebookAlbumPhotos")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    func deleteInstagramPhotos()
    {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Instagram")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    //MARK:- DELETE
    
    
    func deleteGooglePhotosUrl()
    {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "GooglePhotosUrl")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    func deleteGoogleAlbums()
    {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Google")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    func deleteGooglePhotos()
    {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "GoogleAlbumPhotos")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    func deleteDropboxAlbums()
    {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Dropbox")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
//    func deleteDropboxNilItems()
//    {
//        let delegate = UIApplication.shared.delegate as! AppDelegate
//        let context = delegate.persistentContainer.viewContext
//
//        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Dropbox")
//        deleteFetch.predicate = NSPredicate(format: "file_path = %@", "")
//        do {
//            let test = try context.fetch(deleteFetch)
//            if test.indices.contains(0) {
//                let update = test[0]
//                update.setValue(url, forKey: "image")
//                do {
//                    try context.save()
//                }
//                catch {
//                    print("error in saving image data")
//                }
//            }
//        }
//        catch {
//            print("error in fetching data from table")
//        }
//        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
//
//        do {
//            try context.execute(deleteRequest)
//            try context.save()
//        } catch {
//            print ("There was an error")
//        }
//    }
}
