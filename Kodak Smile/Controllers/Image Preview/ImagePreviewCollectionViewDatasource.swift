//
//  ImagePreviewCollectionViewDatasource.swift
//  Polaroid MINT
//
//  Created by maximess142 on 22/08/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import Toast_Swift

extension ImagePreviewViewController : UICollectionViewDataSource, UICollectionViewDelegate
{
    //MARK:- Custom delegate
    func isCurrentEditOptionEnabled(viewIndex: Int) {
        viewFilterContainer.isHidden = viewIndex == 1 ? false : true
        viewSubEditContainer.isHidden = viewIndex == 2 ? false : true
        viewSubStyleContainer.isHidden = viewIndex == 3 ? false : true
        viewSubARContainer.isHidden = viewIndex == 4 ? false : true
    }
    
    //MARK:- Collectionview delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == collectionViewEditMainOptions {
            return mainEditOptionsArray.count
        }else if collectionView == collectionViewFilter {
            if isNativeFiltersMode {
                return nativeFiltersArray.count
            }else {
                return filterNameList.count
            }
        }else if collectionView == collectionViewEditSubOptions {
            return editSubOptionsNameArray.count
        }
        else if collectionView == collectionViewStyleSubOptions {
            return styleSubOptionsNameArray.count
        }else if collectionView == collectionViewBorders {
            return self.framesImages.count + self.eventFrames.count
        }else if collectionView == collectionViewDoodle {
            return doodleColorsArray.count
        }else if collectionView == collectionViewStickers {
            //            return self.stickersArray.count
            return self.eventSticker.count + self.stickersArray.count
        }else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == collectionViewEditMainOptions
        {
            let cell : ImagePreviewMainCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagePreviewMainCollectionViewCell", for: indexPath) as! ImagePreviewMainCollectionViewCell
            let indexObject = mainEditOptionsArray[indexPath.row]
            if paraEditObject.currentEditOptionIndex == indexPath.item {
                cell.viewMainOptionSelected.isHidden = false
                cell.lblEditOptionName.textColor = UIColor.init(red: 0, green: 0, blue: 0)
            }else {
                cell.viewMainOptionSelected.isHidden = true
                cell.lblEditOptionName.textColor = UIColor.init(red: 252, green: 183, blue: 20)
            }
            cell.lblEditOptionName.text = indexObject.localisedString()
            switch paraEditObject.currentEditOptionIndex {
            case 0:
                self.isCurrentEditOptionEnabled(viewIndex: 1)
            case 1:
               self.isCurrentEditOptionEnabled(viewIndex: 2)
            case 2:
               self.isCurrentEditOptionEnabled(viewIndex: 3)
            case 3:
               self.isCurrentEditOptionEnabled(viewIndex: 4)
            default:
                viewFilterContainer.isHidden = false
            }
            return cell
        }
        else if collectionView == collectionViewFilter
        {
            let cell : FilterCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionViewCell", for: indexPath) as! FilterCollectionViewCell
            
            if paraEditObject.selectedFilterIndex == 0
            {
                self.sliderFilterOpacity.isHidden = true
                self.gradientViewLeftToRight.isHidden = true
            }
            else
            {
                self.sliderFilterOpacity.isHidden = false
                self.gradientViewLeftToRight.isHidden = false
                if !UserDefaults.standard.bool(forKey: "overlaySwipeOpacity") {
                    //Comment by dhanraj after discuss with amyn.
                    self.viewSwipeDownOverlay.isHidden = false
                    self.viewSwipeDownOverlay.alpha = 0.8
                    let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(_:)))
                    swipe.direction = UISwipeGestureRecognizerDirection.down
                    self.viewSwipeDownOverlay.addGestureRecognizer(swipe)
                    self.viewSwipeDownOverlay.isUserInteractionEnabled = true
                    UserDefaults.standard.set(true, forKey: "overlaySwipeOpacity")
                }
            }
            if isNativeFiltersMode
            {
                cell.lblFilterName.text = nativeFiltersArray[indexPath.item].localisedString()
                cell.lblFilterNameSelected.text = nativeFiltersArray[indexPath.item].localisedString()
            }
            else
            {
                cell.lblFilterName.text = filterEffectsNameList[indexPath.item].localisedString()
                cell.lblFilterNameSelected.text = filterEffectsNameList[indexPath.item].localisedString()
            }
            cell.imageViewSelected.isHidden = true
            cell.viewBarSelected.isHidden = true
            cell.lblFilterNameSelected.isHidden = true
            if paraEditObject.selectedFilterIndex == indexPath.item
            {
                cell.viewBorder.layer.borderWidth = 2
                cell.viewBorder.layer.borderColor = UIColor.red.cgColor
                cell.lblFilterName.textColor = UIColor.black
            }
            else
            {
                cell.viewBorder.layer.borderWidth = 0
                cell.lblFilterName.textColor = UIColor(red: 160, green: 160, blue: 160)
                //                cell.imageViewDeselected.isHidden = false
                //                cell.lblFilterName.isHidden = false
            }
            if arrFilterPreviewImages.count > indexPath.row
            {
                cell.imageViewSelected.image = arrFilterPreviewImages[indexPath.row]
                cell.imageViewDeselected.image = arrFilterPreviewImages[indexPath.row]
            }
            else
            {
                cell.imageViewSelected.image = filterThumbnailImage
                cell.imageViewDeselected.image = filterThumbnailImage
                cell.activityIndicatorImg.startAnimating()
                if !isNativeFiltersMode
                {
                  if arrFilterPreviewImages.count > indexPath.row
                    {
                        cell.imageViewSelected.image = arrFilterPreviewImages[indexPath.row]
                        cell.imageViewDeselected.image = arrFilterPreviewImages[indexPath.row]
                    }
                }
                else
                {
                    self.applyNativeFilterToImage(withIndex: indexPath.row, completion: {img, error in
                        if let filteredImg = img
                        {
                            //print("Applying filter")
                            cell.activityIndicatorImg.stopAnimating()
                            cell.imageViewSelected.image = filteredImg
                            cell.imageViewDeselected.image = filteredImg
                        }
                    })
                }
            }
            return cell
        }
        else if collectionView == collectionViewEditSubOptions
        {
            let cell : EditSubCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditSubCollectionViewCell", for: indexPath) as! EditSubCollectionViewCell
            cell.lblOptionName.text = editSubOptionsNameArray[indexPath.item].localisedString()
            cell.imgViewOptionIcon.image = editSubOptionsIconArray[indexPath.item]
            return cell
        }
        else if collectionView == collectionViewStyleSubOptions
        {
            let cell : EditSubCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditSubCollectionViewCell", for: indexPath) as! EditSubCollectionViewCell
            cell.lblOptionName.text = styleSubOptionsNameArray[indexPath.item].localisedString()
            cell.imgViewOptionIcon.image = styleSubOptionsIconArray[indexPath.item]
            return cell
        }
        else if collectionView == collectionViewBorders
        {
            let cell : FrameThumbnailCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FrameThumbnailCollectionViewCell", for: indexPath) as! FrameThumbnailCollectionViewCell
            if indexPath.row != 0
            {
                if self.eventFrames.indices.contains(indexPath.item - 1){
                    if DBModelForEvents.shared.fetchStickerFrameDownloadStste(id: self.eventFrames[indexPath.item - 1]["id"] as! Int, isSticker: false) != 0{
                        cell.imgViewFrameThumbnail.image = self.getSavedImage(named: DBModelForEvents.shared.fetchDownloadedStickerFrame(id: self.eventFrames[indexPath.item - 1]["id"] as! Int, isSticker: false))
                        cell.imgViewFrameSelected.image = self.getSavedImage(named: DBModelForEvents.shared.fetchDownloadedStickerFrame(id: self.eventFrames[indexPath.item - 1]["id"] as! Int, isSticker: false))
                        cell.imgIsDownloaded.isHidden = true
                        self.framesThumbnails.append(cell.imgViewFrameThumbnail.image!)
                    }else if WebserviceModelClass().isInternetAvailable(){
                        cell.imgViewFrameThumbnail.af_setImage(
                            withURL: URL(string: self.eventFrames[indexPath.item - 1]["thumbnail"] as! String)!,
                            imageTransition: .crossDissolve(0.2),
                            completion: {(img) in
                                if let image = img.value{
                                    self.framesThumbnails.append(image)
                                }
                        })
                        cell.imgViewFrameSelected.af_setImage(
                            withURL: URL(string: self.eventFrames[indexPath.item - 1]["thumbnail"] as! String)!,
                            imageTransition: .crossDissolve(0.2),
                            completion: nil)
                        cell.imgIsDownloaded.isHidden = false
                    }else{
                        cell.imgIsDownloaded.isHidden = false
                        cell.imgViewFrameThumbnail.image = self.framesThumbnails[indexPath.item - 1]
                        cell.imgViewFrameSelected.image = self.framesThumbnails[indexPath.item - 1]
                    }
                    if paraEditObject.selectedFramesIndex == indexPath.row
                    {
                        cell.showSelectedCell(strFilterName: self.eventFrames[indexPath.row - 1]["name"] as! String)
                    }
                    else
                    {
                        cell.showNormalCell(strFilterName: self.eventFrames[indexPath.row - 1]["name"] as! String)
                    }
                    if self.downloadingFrames.contains(indexPath.item){
                        cell.loader.isHidden = false
                    }else{
                        cell.loader.isHidden = true
                    }
                }else {
                    cell.imgViewFrameThumbnail.image = self.framesImages[indexPath.item -  self.eventFrames.count]
                    cell.imgViewFrameSelected.image = self.framesImages[indexPath.item -  self.eventFrames.count]
                    if paraEditObject.selectedFramesIndex == indexPath.row
                    {
                        cell.showSelectedCell(strFilterName: self.framesNameArray[indexPath.row - self.eventFrames.count - 1])
                    }
                    else
                    {
                        cell.showNormalCell(strFilterName: self.framesNameArray[indexPath.row - self.eventFrames.count - 1])
                    }
                }
            }
            else
            {
                cell.showNoFrameCell()
                cell.lblFrameName.text = "None".localisedString()
            }
            return cell
        }
        else if collectionView == collectionViewDoodle
        {
            let cell : DoodleColorCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DoodleColorCollectionViewCell", for: indexPath) as! DoodleColorCollectionViewCell
            let indexColor = doodleColorsArray[indexPath.row]
            cell.viewColorNormal.backgroundColor = indexColor
            cell.viewColorSelected.backgroundColor = indexColor
            if paraEditObject.selectedDoodleColorIndex == indexPath.row
            {
                cell.viewBorder.borderColor = indexColor
                //                cell.viewColorSelectedContainer.isHidden = false
            }
            else
            {
                cell.viewBorder.borderColor = UIColor.clear
                cell.viewColorSelectedContainer.isHidden = true
            }
            if paraEditObject.selectedDoodleColorIndex == indexPath.item{
                cell.viewBorder.shadowOpacity = 1.0
                cell.viewBorder.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
                cell.viewBorder.shadowOffset = CGSize(width: 0.0, height: 0.5)
                cell.viewBorder.shadowRadius = 0.8
            }else{
                cell.viewBorder.shadowOpacity = 0.0
            }
            
            return cell
        }
        else if collectionView == collectionViewStickers
        {
            let cell : StickerCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerCollectionViewCell", for: indexPath) as! StickerCollectionViewCell
            //            cell.imageViewSticker.image = self.stickerImages![indexPath.item]      //UIImage.init(named: (stickersArray[indexPath.item]))
            if self.eventSticker.indices.contains(indexPath.item){
                if DBModelForEvents.shared.fetchStickerFrameDownloadStste(id: self.eventSticker[indexPath.item]["id"] as! Int, isSticker: true) != 0{
                    cell.imageViewSticker.image = self.getSavedImage(named: DBModelForEvents.shared.fetchDownloadedStickerFrame(id: self.eventSticker[indexPath.item]["id"] as! Int, isSticker: true))
                    cell.imgIsDownloaded.isHidden = true
                    self.stickersThumbnails.append(cell.imageViewSticker.image!)
                }else if WebserviceModelClass().isInternetAvailable(){
                    cell.imgIsDownloaded.isHidden = false
                    cell.imageViewSticker.af_setImage(
                        withURL: URL(string: self.eventSticker[indexPath.item]["thumbnail"] as! String)!,
                        imageTransition: .crossDissolve(0.2),
                        completion: {(img) in
                            if let image = img.value{
                                self.stickersThumbnails.append(image)
                            }
                    })
                }else{
                    cell.imgIsDownloaded.isHidden = false
                    cell.imageViewSticker.image = self.stickersThumbnails[indexPath.item]
                }
                if self.downloadingStickers.contains(indexPath.item){
                    cell.loader.isHidden = false
                }else{
                    cell.loader.isHidden = true
                }
            }else {
                cell.imageViewSticker.image = self.stickerImages[indexPath.item -  self.eventSticker.count]
            }
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        // Main collection view
        if collectionView == collectionViewEditMainOptions
        {
            self.isFilterSelected = false
            self.sliderFilterOpacity.isHidden = true
            self.lblSliderValue.isHidden = true
            self.gradientViewLeftToRight.isHidden = true
            //Add gesture on Filter
            if indexPath.row == 0
            {
                if isFilterOptionClicked || isRotateOptionClicked{
                   self.isEditOptionClicked = false
                }
                self.disableScrollZoom()
                self.changeGestureOnImage(pan: false, pinch: false, rotate: false)
                //Pratiksha Added this logic to enable userinteraction of stickers in filter explicitely
                // as this option does not have done and cancel option.
                self.viewTextStickerCombined.isUserInteractionEnabled = true
                if paraEditObject.selectedFilterIndex == 0
                {
                    self.sliderFilterOpacity.isHidden = true
                    self.lblSliderValue.isHidden = true
                    self.gradientViewLeftToRight.isHidden = true
                }
                else
                {
                    self.isFilterSelected = true
                    self.sliderFilterOpacity.isHidden = false
                    self.lblSliderValue.isHidden = false
                    self.gradientViewLeftToRight.isHidden = false
                }
                //Akshay
//                editedImageExceptCurrentEffect = applyAllEditEffectsExceptCurrent(shadows: self.paraEditObject.shadowsValue,
//                                                                                  highlights: self.paraEditObject.highlightsValue,
//                                                                                  contrast: self.paraEditObject.contrastValue,
//                                                                                  saturation: self.paraEditObject.saturationValue,
//                                                                                  brightness: self.paraEditObject.brightnessValue,
//                                                                                  warm: self.paraEditObject.warmValue,
//                                                                                  filter: paraEditObject.filterValue, currentApplyEffect: EditFunction().filter)
            }else if indexPath.row == 1{
                // Edit-->Image not get resize on edit option(CR) add zoom option on client request on 2 march 2020
                self.isEditOptionClicked = true
                self.enableScrollZoom()
            }else if indexPath.row == 2{
               if isFilterOptionClicked || isRotateOptionClicked{
                   self.isEditOptionClicked = false
                }
                self.disableScrollZoom()
            }
            else if indexPath.row == 3
            {
                if isFilterOptionClicked || isRotateOptionClicked{
                   self.isEditOptionClicked = false
                }
                self.disableScrollZoom()
//                if !UserDefaults.standard.bool(forKey: "ARCameraOverlayViewController"){
//                    self.showArScanElementsOverlay()
//                }
//                    let SelectVideoVC = storyboards.arStoryboard.instantiateViewController(withIdentifier: "SelectVideoViewController") as! SelectVideoViewController
                    let videoAssets = getVideoAssetsAlbumWise()
//                    if videoAssets?.count ?? 0 > 0
//                    {
//                        SelectVideoVC.imagesArray = videoAssets
//                        self.navigationController?.pushViewController(SelectVideoVC, animated: true)
//                    }
//                    else
//                    {
//                        self.view.makeToast("There are no videos on your device".localisedString(), duration: 2.0, position: .bottom)
//                    }
                

            }
//            else
//            {
//                self.changeGestureOnImage(pan: false, pinch: false, rotate: false)
//            }
            paraEditObject.currentEditOptionIndex = indexPath.row
            self.collectionViewEditMainOptions.reloadData()
        }
            // Filter collection view
        else if collectionView == collectionViewFilter
        {
            self.isFilterSelected = true
            paraEditObject.selectedFilterIndex = indexPath.item
            //                DispatchQueue.main.async (execute: {
            self.applySelectedFilterToImage()
            //                })
            
            self.collectionViewFilter.reloadData()
        }
            // Edit Sub options
        else if collectionView == collectionViewEditSubOptions
        {
            // print(enumSubEditOptions.RawValue)
            paraEditObject.selectedSubEditOption = indexPath.item
            
            // Scale
            if indexPath.row == 0
            {
                isFilterOptionClicked = true
                self.enableScrollZoom()
                self.setUpScaleView()
                self.showCancelDoneButn()
            }
                // Rotate
            else if indexPath.row == 1
            {
                isRotateOptionClicked = true
                self.enableScrollZoom()
                self.setUpRotateView()
                self.showCancelDoneButn()
            }
                // Brightness
            else if indexPath.row == 2
            {
                 isFilterOptionClicked = false
                 isRotateOptionClicked = false
                self.disableScrollZoom()
                self.setUpBrightnessFilter()
                self.showCancelDoneButn()
            }
                // Contrast
            else if indexPath.row == 3
            {
                 isFilterOptionClicked = false
                 isRotateOptionClicked = false
                self.disableScrollZoom()
                self.setUpContrastFilter()
                self.showCancelDoneButn()
            }
                // Warm
            else if indexPath.row == 4
            {
                 isFilterOptionClicked = false
                 isRotateOptionClicked = false
                self.disableScrollZoom()
                self.setUpWarmFilter()
                self.showCancelDoneButn()
            }
                // Saturation
            else if indexPath.row == 5
            {
                 isFilterOptionClicked = false
                 isRotateOptionClicked = false
                self.disableScrollZoom()
                self.setUpSaturationFilter()
                self.showCancelDoneButn()
            }
                // Highlights
            else if indexPath.row == 6
            {
                 isFilterOptionClicked = false
                 isRotateOptionClicked = false
                self.disableScrollZoom()
                self.setUpHighlightsFilter()
                self.showCancelDoneButn()
            }
                // Shadows
            else if indexPath.row == 7
            {
                 isFilterOptionClicked = false
                 isRotateOptionClicked = false
                self.disableScrollZoom()
                self.setUpShadowsFilter()
                self.showCancelDoneButn()
            }
            else
            {
                self.view.makeToast("Coming soon".localisedString(), duration: 2.0, position: .bottom)
            }
        }
            // Edit Style options
        else if collectionView == collectionViewStyleSubOptions
        {
            paraEditObject.selectedSubStyleOption = indexPath.item
           // Stickers
            if indexPath.row == 0
            {
                self.setUpStickersView()
                //                self.showCancelDoneButn()
            }
                // Text
            else if indexPath.row == 1
            {
                self.setUpTextView()
                //self.showCancelDoneButn()
            }
                // Doodle
            else if indexPath.row == 2
            {
                self.setUpDoodleView()
                self.showCancelDoneButn()
            }
                // Borders
            else if indexPath.row == 3
            {
                self.enableScrollZoom()
                self.setUpFramesView()
                self.showCancelDoneButn()
            }
            else
            {
                self.view.makeToast("Coming soon".localisedString(), duration: 2.0, position: .bottom)
            }
        }
            // Borders view
        else if collectionView == collectionViewBorders
        {
            if self.eventFrames.indices.contains(indexPath.row - 1) && DBModelForEvents.shared.fetchStickerFrameDownloadStste(id: self.eventFrames[indexPath.item - 1]["id"] as! Int, isSticker: false) == 0 {
                self.collectionViewBorders.reloadData()
                if WebserviceModelClass().isInternetAvailable(){
                    self.downloadingFrames.append(indexPath.item)
                    self.applySelectedFrameToImage(index: indexPath.row)
                }else{
                    self.collectionViewBorders.bringSubview(toFront: self.lblNoInternetToast)
                    self.showNoInternetToast()
                }
            }else{
                paraEditObject.selectedFramesIndex = indexPath.row
                self.collectionViewBorders.reloadData()
                self.applySelectedFrameToImage(index: indexPath.row)
            }
        }
            // Doodle view
        else if collectionView == collectionViewDoodle
        {
            paraEditObject.selectedDoodleColorIndex = indexPath.row
            self.applySelectedColorToDoodle()
            self.collectionViewDoodle.reloadData()
        }
            // Sticker view
        else if collectionView == collectionViewStickers
        {
            paraEditObject.selectedStickerIndex = indexPath.row
            if self.eventSticker.indices.contains(indexPath.row) && DBModelForEvents.shared.fetchStickerFrameDownloadStste(id: self.eventSticker[indexPath.item]["id"] as! Int, isSticker: true) == 0 {
                if WebserviceModelClass().isInternetAvailable(){
                    self.downloadingStickers.append(indexPath.item)
                    self.collectionViewStickers.reloadData()
                    self.applyStickerToView(index: indexPath.row)
                }
                else{
                    self.collectionViewStickers.bringSubview(toFront: self.lblNoInternetToast)
                    self.showNoInternetToast()
                }
            }else{
                self.collectionViewStickers.reloadData()
                self.applyStickerToView(index: indexPath.row)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //add here
        print("willDisplay")
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        self.collectionViewEditMainOptions.selectItem(at: selectedIndexPath, animated: false, scrollPosition: [])
    }
    
    func showArScanElementsOverlay() {
//        if let childOverlayViewVC = storyboards.arStoryboard.instantiateViewController(withIdentifier: "ARCameraOverlayViewController") as? ARCameraOverlayViewController
//        {
//            UserDefaults.standard.set(true, forKey: "ARCameraOverlayViewController")
//            self.present(childOverlayViewVC, animated: false, completion: nil)
//        }
    }
    
    func disableScrollZoom(){
        self.scrollViewForZooming.isScrollEnabled = false
        self.scrollViewForZooming.isUserInteractionEnabled = false
    }
    
    func enableScrollZoom(){
        self.scrollViewForZooming.isScrollEnabled = true
        self.scrollViewForZooming.isUserInteractionEnabled = true
    }
    
    func showNoInternetToast(){
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.lblNoInternetToast.isHidden = false
        }) { finished in
            UIView.transition(with: self.view, duration: 2.0, options: .transitionCrossDissolve, animations: {
                self.lblNoInternetToast.isHidden = true
            }, completion: nil)
        }
    }
}

extension ImagePreviewViewController : UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        switch collectionView {
        case collectionViewEditMainOptions:
            return CGSize(width: collectionView.frame.size.width / CGFloat(self.mainEditOptionsArray.count), height: 40)
            
        case collectionViewFilter:
            return CGSize(width: 76, height: collectionView.frame.size.height)
            
        case collectionViewEditSubOptions:
            return CGSize(width: 76, height: collectionView.frame.size.height)
            
        case collectionViewStyleSubOptions:
            return CGSize(width: collectionView.frame.size.width * 0.23, height: collectionView.frame.size.height)
            
        case collectionViewBorders:
            return CGSize(width: 80, height: collectionView.frame.size.height)
            
        case collectionViewDoodle:
            return CGSize(width: 35, height: 35)
            
        case collectionViewStickers:
            return CGSize(width: (collectionView.frame.size.width - 50) / 5, height: (collectionView.frame.size.width - 50) / 5)
            
        default:
            return CGSize(width: 0, height: 0)
        }
    }
}
