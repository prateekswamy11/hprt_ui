//
//  ImageEdit+SaveIntermediateChanges.swift
//  Polaroid MINT
//
//  Created by maximess142 on 27/08/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import CopilotAPIAccess

extension ImagePreviewViewController
{
    func captureImageInView(inView: UIView) -> UIImage?
    {
        let renderer = UIGraphicsImageRenderer(size: inView.bounds.size)
        let image = renderer.image { ctx in
            inView.drawHierarchy(in: inView.bounds, afterScreenUpdates: true)
        }
        let data = image.highestQualityJPEGNSData
        let iageHigh = UIImage(data: data)
        return iageHigh
    }
    
    @IBAction func btnCancelSubEditClicked(_ sender: Any)
    {
        if isEditOptionClicked {
            if isFilterOptionClicked || isRotateOptionClicked{
                if checkZoomScaleOnFilterAndRotate != 0.0{
                    if checkZoomScale != 0.0 && checkZoomScaleOnFilterAndRotate > checkZoomScale{
                        checkZoomScaleOnFilterAndRotate = checkZoomScaleOnFilterAndRotate - checkZoomScale
                        self.scrollViewForZooming.setZoomScale(CGFloat(checkZoomScale), animated: true)
//                        checkZoomScale = checkZoomScaleOnFilterAndRotate
                        checkZoomScaleOnFilterAndRotate = 0.0
                    }else if checkZoomScale != 0.0 && checkZoomScaleOnFilterAndRotate < checkZoomScale{
                        self.scrollViewForZooming.setZoomScale(CGFloat(checkZoomScaleOnFilterAndRotate), animated: true)
                    }else if checkZoomScale == 0.0{
                        self.scrollViewForZooming.setZoomScale(0.0, animated: true)
                    }
                }else{
                    print("Filter or Rotate not zoomed")
                }
            }
        }
        else{
            if isFilterOptionClicked || isRotateOptionClicked{
                self.scrollViewForZooming.setZoomScale(CGFloat(checkZoomScaleOnFilterAndRotate), animated: true)
            }
        }
        
        self.callCancelButnAction()
        self.disableScrollZoom()
        if paraEditObject.currentEditOptionIndex == 1
        {
            self.enableScrollZoom()
        }
    }
    
    func callCancelButnAction()
    {
        //Changed this sequence of function call
        self.resetEditSubViews()
        self.hideCancelDoneButn()
    }
    
    @IBAction func btnDoneSubEditClicked(_ sender: Any)
    {
        saveValueOfZoomOnDone = checkZoomScale
        self.disableScrollZoom()
        if paraEditObject.currentEditOptionIndex == 1
        {   self.enableScrollZoom()
            if (0...7).contains(paraEditObject.selectedSubEditOption) {
                self.enableStickerTextSubviews()
            }
        }
        else if paraEditObject.currentEditOptionIndex == 2
        {
            self.removeBordersFromAllSubViews()
            if paraEditObject.selectedSubStyleOption == 0
            {
                paraIntermediateViews.arrayTempStickerSubviews = [StickerCustomView]()
                let subViews = self.viewTextStickerCombined.subviews
                for subview in subViews
                {
                    if subview is StickerCustomView
                    {
                        if let view = subview as? StickerCustomView
                        {
                            let stickerImgView = StickerCustomView(frame: view.frame)
                            stickerImgView.image = view.image
                            // Add these 3 values to save rotation of older view
                            stickerImgView.bounds = view.bounds
                            stickerImgView.center = view.center
                            stickerImgView.transform = view.transform
                            paraIntermediateViews.arrayTempStickerSubviews.append(stickerImgView)
                        }
                    }
                }
            }
            else if paraEditObject.selectedSubStyleOption == 1
            {
                paraIntermediateViews.arrayTempTextSubviews = [TextCustomView]()
                let subViews = self.viewTextStickerCombined.subviews
                for subview in subViews
                {
                    if subview is TextCustomView
                    {
                        if let view = subview as? TextCustomView
                        {
                            let textLblView = TextCustomView(frame: view.frame)
                            textLblView.text = view.text // dict["text"] as? String
                            textLblView.font = view.font
                            textLblView.textColor = view.textColor
                            textLblView.colorIndex = view.colorIndex
                            textLblView.fontIndex = view.fontIndex
                            // Add these 3 values to save rotation of older view
                            textLblView.bounds = subview.bounds
                            textLblView.center = subview.center
                            textLblView.textAlignment = view.textAlignment
                            textLblView.transform = subview.transform
                            paraIntermediateViews.arrayTempTextSubviews.append(textLblView)
                        }
                    }
                }
            }
            else if paraEditObject.selectedSubStyleOption == 2
            {
                paraIntermediateViews.tempDoodleOption = paraEditObject.selectedDoodleOption
                paraIntermediateViews.tempDoodleColorIndex = paraEditObject.selectedDoodleColorIndex
                paraIntermediateViews.tempDoodleEraserWidth = paraEditObject.selectedDoodleEraserWidth
                paraIntermediateViews.tempDoodlePencilWidth = paraEditObject.selectedDoodlePencilWidth
                
                self.hideDoodleView()
            }
            else if paraEditObject.selectedSubStyleOption == 3
            {
                self.changeGestureOnImage(pan: false, pinch: false, rotate: false)
                // Make the selected border permanent
                paraIntermediateViews.tempBorderIndex = paraEditObject.selectedFramesIndex
                self.enableStickerTextSubviews()
            }
        }
        self.hideCancelDoneButn()
        var feature = ""
        var value = ""
        switch paraEditObject.currentEditOptionIndex {
        case 0:
            feature = AnalyticsValue.feature.filter.rawValue
            value = String(paraEditObject.selectedFilterIndex)
        case 1:
            feature = AnalyticsValue.feature.frames.rawValue
            value = String(paraEditObject.selectedFramesIndex)
        case 2:
            feature = AnalyticsValue.feature.stickers.rawValue
            value = String(paraEditObject.selectedStickerCatIndex)
        case 3:
            feature = AnalyticsValue.feature.brightness.rawValue
        case 4:
            feature = AnalyticsValue.feature.temperature.rawValue
        case 5:
            feature = AnalyticsValue.feature.adjust.rawValue
        case 6:
            feature = AnalyticsValue.feature.blur.rawValue
            value = String(paraEditObject.selectedFilterIndex)
        case 7:
            feature = AnalyticsValue.feature.text.rawValue
            value = String(paraEditObject.selectedTextColorIndex)
        case 8:
            feature = AnalyticsValue.feature.paint.rawValue
        default:
            print("No message for you")
        }
        let editPhotoAnalyticsEvent = EditPhotoAnalyticsEvent(screenName: AnalyticsEventName.Screen.editPhoto.rawValue, feature: feature, value: value)
        Copilot.instance.report.log(event: editPhotoAnalyticsEvent)
    }
    
    func showCancelDoneButn()
    {
        self.viewCancelDone.isHidden = false
        self.btnCross.isHidden = true
        self.btnOptions.isHidden = true
        // Hide opacity slider
        self.sliderFilterOpacity.isHidden = true
        self.lblSliderValue.isHidden = true
        self.gradientViewLeftToRight.isHidden = true
        
        if isComeFromMultiPrintScreen{
            self.hideMultiPrintSubviews()
        }else{
            self.btnPrint.isHidden = true
            self.lblPrint.isHidden = true
            self.imgPrintStatus.isHidden = true
        }
    }
    
    func hideCancelDoneButn()
    {
        self.viewCancelDone.isHidden = true
        self.btnCross.isHidden = false
        self.btnOptions.isHidden = false
        self.hideEditSubViews()
        self.btnTextDelete.isHidden = true
        if isComeFromMultiPrintScreen{
            self.showMultiPrintSubviews()
            self.btnCross.isHidden = true
            self.btnOptions.isHidden = true
        }else{
            self.lblPrint.isHidden = false
            self.btnPrint.isHidden = false
            self.imgPrintStatus.isHidden = false
        }
    }
    
    func hideEditOptions() {
        switch paraEditObject.selectedSubEditOption {
        case 0:
            self.viewGrid.isHidden = true
            self.viewScale.isHidden = true
            self.changeGestureOnImage(pan: false, pinch: false, rotate: false)
        case 1:
            self.viewGrid.isHidden = true
            self.viewRotate.isHidden = true
            self.changeGestureOnImage(pan: false, pinch: false, rotate: false)
        case 2:
            self.viewBrightness.isHidden = true
        case 3:
            self.viewContrast.isHidden = true
        case 4:
            self.viewWarm.isHidden = true
        case 5:
            self.viewSaturation.isHidden = true
        case 6:
            self.viewHighlights.isHidden = true
        case 7:
            self.viewShadows.isHidden = true
        default:
            print("default case")
        }
    }
    
    func hideStyleOptions() {
        switch paraEditObject.selectedSubStyleOption {
        case 0:
            self.viewStickers.isHidden = true
        case 1:
            self.viewText.isHidden = true
        case 2:
            self.viewDoodle.isHidden = true
            self.btnDoodleUndo.isHidden = true
            self.viewDoodleDrawing?.isUserInteractionEnabled = false
        case 3:
            self.viewBorders.isHidden = true
        default:
            print("default case")
        }
    }
    
    func hideEditSubViews() {
        switch paraEditObject.currentEditOptionIndex {
        case 1:
            self.hideEditOptions()
        case 2:
            self.hideStyleOptions()
            paraEditObject.selectedSubStyleOption = -1
            self.enableDisableGesturesOnTextView(isEnable: true)
            self.enableDisableGesturesOnStickerView(isEnable: true)
        default:
            print("default case")
        }
    }
    
    func removeStickersView() {
        self.removeAllStickersFromView()
        for subview in (paraIntermediateViews.arrayTempStickerSubviews)
        {
            let stickerImgView = StickerCustomView(frame: subview.frame)
            stickerImgView.image = subview.image
            // Add these 3 values to save rotation of older view
            stickerImgView.bounds = subview.bounds
            stickerImgView.center = subview.center
            stickerImgView.transform = subview.transform
            self.viewTextStickerCombined.addSubview(stickerImgView)
        }
        self.resetStickersView()
    }
    
    func removeTextViews() {
        self.removeAllTextsFromView()
        for subview in (paraIntermediateViews.arrayTempTextSubviews)
        {
            let textLblView = TextCustomView(frame: subview.frame)
            textLblView.text = subview.text
            textLblView.font = subview.font
            textLblView.textColor = subview.textColor
            textLblView.colorIndex = subview.colorIndex //(dict["selectedColorIndex"] as? Int)!
            textLblView.fontIndex = subview.fontIndex //(dict["selectedFontIndex"] as? Int)!
            // Add these 3 values to save rotation of older view
            textLblView.bounds = subview.bounds
            textLblView.center = subview.center
            textLblView.textAlignment = subview.textAlignment
            textLblView.transform = subview.transform
            self.viewTextStickerCombined.addSubview(textLblView)
        }
        self.resetTextView()
    }
    
    func removeDoodleViews() {
        if self.paraIntermediateViews.countTempDoodle > 0
        {
            for _ in 1...paraIntermediateViews.countTempDoodle {
                self.viewDoodleDrawing?.undoLatestStep()
            }
        }
        // Then reset the count
        paraIntermediateViews.countTempDoodle = 0
        self.resetDoodleView()
        paraEditObject.selectedDoodleOption = paraIntermediateViews.tempDoodleOption
        paraEditObject.selectedDoodleColorIndex = paraIntermediateViews.tempDoodleColorIndex
        paraEditObject.selectedDoodleEraserWidth = paraIntermediateViews.tempDoodleEraserWidth
        paraEditObject.selectedDoodlePencilWidth = paraIntermediateViews.tempDoodlePencilWidth
    }
    
    func removeBordersView() {
        paraEditObject.selectedFramesIndex = paraIntermediateViews.tempBorderIndex
        self.collectionViewBorders.reloadData()
        self.applySelectedFrameToImage(index: paraEditObject.selectedFramesIndex)
        self.resetBordersView()
    }
    
    func resetEditSubViews()
    {
        if paraEditObject.currentEditOptionIndex == 1
        {
            switch paraEditObject.selectedSubEditOption {
            case 0:
                self.resetScaleView()
            case 1:
                self.resetRotateView()
            case 2:
                self.resetBrightnessView()
            case 3:
                self.resetContrastView()
            case 4:
                self.resetWarmView()
            case 5:
                self.resetSaturationView()
            case 6:
                self.resetHighlightsView()
            case 7:
                self.resetShadowsView()
            default:
                print("default case")
            }
        }
        else if paraEditObject.currentEditOptionIndex == 2
        {
            switch paraEditObject.selectedSubStyleOption {
            case 0:
                self.removeStickersView()
            case 1:
                self.removeTextViews()
            case 2:
                self.removeDoodleViews()
            case 3:
                self.removeBordersView()
            default:
                print("default case")
            }
        }
    }
}
