
//
//  ImagePreview+Gestures.swift
//  Polaroid MINT
//
//  Created by maximess142 on 31/08/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import UIKit

extension ImagePreviewViewController : UIGestureRecognizerDelegate
{
    func initialiseGestureOnImage()
    {
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(handleRotate(recognizer:)))
        rotate.delegate = self
        self.imageViewPreview.addGestureRecognizer(rotate)

        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        pan.delegate = self
        self.imageViewPreview.addGestureRecognizer(pan)
        
        let pinchGesture = UIPinchGestureRecognizer(target: self,
                                                    action: #selector(pinchRecognized(pinch:)))
        pinchGesture.delegate = self
        self.imageViewPreview.addGestureRecognizer(pinchGesture)
        //self.viewImageContainer.addGestureRecognizer(pinchGesture)
    }
    
    private func insideDraggableArea(_ point : CGPoint) -> Bool {
        return  // point.x > 50 && point.x < 200 &&
            point.y > (UIScreen.main.bounds.height * 0.27) && point.y <= UIScreen.main.bounds.height
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: view)
            print(position)
        }
    }
    @objc func handlePan(_ gestureRecognizer: UIPanGestureRecognizer)
    {
        // Restrict movement to some offset so that it not get vanished while panning outside of superview's boundary.
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed
        {
            let point = gestureRecognizer.location(in:self.imageViewPreview.superview)//viewImageContainer
            if let superview = self.imageViewPreview.superview
            {
                let restrictByPoint : CGFloat = 30.0
                let superBounds = CGRect(x: superview.bounds.origin.x + restrictByPoint, y: superview.bounds.origin.y + restrictByPoint, width: superview.bounds.size.width - 2*restrictByPoint, height: superview.bounds.size.height - 2*restrictByPoint)
                print("superBounds\(superBounds)")
                if (superBounds.contains(point))
                {
                    let translation = gestureRecognizer.translation(in: self.imageViewPreview.superview)
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x + translation.x, y: gestureRecognizer.view!.center.y + translation.y)
                    gestureRecognizer.setTranslation(CGPoint.zero, in: self.imageViewPreview.superview)
                    print("gestureRecognizer\( gestureRecognizer.setTranslation(CGPoint.zero, in: self.imageViewPreview.superview))")
                    //Save current translation
                    paraEditObject.scaleTranslationValue = CGPoint(x: gestureRecognizer.view!.center.x + translation.x, y: gestureRecognizer.view!.center.y + translation.y)//self.translation(from: gestureRecognizer.view!.transform)
                }
            }
        }
    }
    //get current translation scale
    func translation(from transform: CGAffineTransform) -> CGPoint {
        return CGPoint(x: transform.tx, y: transform.ty)
    }
    
    @objc func pinchRecognized(pinch: UIPinchGestureRecognizer)
    {
        if(pinch.state == .began) {
            // Reset the last scale, necessary if there are multiple objects with different scales
            lastScale = pinch.scale
        }
        
        if (pinch.state == .began || pinch.state == .changed) {
            //crash on ios 10
            var currentScale = CGFloat()
            if SYSTEM_VERSION_LESS_THAN(version: "11.0")
            {
                currentScale = (pinch.view?.frame.size.width)! / (pinch.view?.bounds.size.width)!
            }
            else{
                currentScale = pinch.view!.layer.value(forKeyPath:"transform.scale")! as! CGFloat
            }
            // Constants to adjust the max/min values of zoom
            let kMaxScale:CGFloat = 3.5
            let kMinScale:CGFloat = 0.4
            var newScale = 1 -  (lastScale - pinch.scale)
            newScale = min(newScale, kMaxScale / currentScale)
            newScale = max(newScale, kMinScale / currentScale)
            let transform = (pinch.view?.transform)!.scaledBy(x: newScale, y: newScale);
            pinch.view?.transform = transform
            lastScale = pinch.scale  // Store the previous scale factor for the next pinch gesture call
            //store curreunt zoom scale
            //print("transform.a = \(transform.a) && transform.b = \(transform.b)")
            paraEditObject.scaleZoomValue = CGFloat(scale(from: transform))
        }
    }
    
    //get current zoom scale
    func scale(from transform: CGAffineTransform) -> Double {
        return sqrt(Double(transform.a * transform.a + transform.c * transform.c))
    }
    
    @objc func handleRotate(recognizer : UIRotationGestureRecognizer) {
        if let view = recognizer.view {
            view.transform = view.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
        }
    }
    
    //MARK:- UIGestureRecognizerDelegate Methods
    /*
     By default, once one gesture recognizer on a view “claims” the gesture, no others can recognize a gesture from that point on.
     
     So, for Simultaneous Gesture recognizers I have overridden a function in the UIGestureRecognizer delegate.
     */
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        // Edit - Pratiksha
        // This line will move current working sticker to front between all stickers
        //        self.imgViewMainImage.superview?.bringSubview(toFront: self)
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool
    {
        return true
    }
    
    // MARK: - Functions
    func changeGestureOnImage(pan: Bool, pinch: Bool, rotate: Bool)
    {

    }
}

