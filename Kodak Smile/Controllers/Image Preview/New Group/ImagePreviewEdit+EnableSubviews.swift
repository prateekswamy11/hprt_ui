//
//  ImagePreviewEdit+EnableSubviews.swift
//  Polaroid MINT
//
//  Created by maximess142 on 15/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import UIKit

extension ImagePreviewViewController
{
    func enableStickerTextSubviews()
    {
        self.viewTextStickerCombined.isUserInteractionEnabled = true
    }
    
    func disableStickerTextSubviews()
    {
        self.viewTextStickerCombined.isUserInteractionEnabled = false
    }
}
