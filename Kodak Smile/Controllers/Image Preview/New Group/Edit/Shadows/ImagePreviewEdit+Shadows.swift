//
//  ImagePreviewEdit+Shadows.swift
//  Polaroid MINT
//
//  Created by maximess142 on 25/01/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

extension ImagePreviewViewController
{
    func setUpShadowsFilter()
    {
        self.disableStickerTextSubviews()
        self.viewShadows.isHidden = false
        ///// save previous Contrast value
        paraEditObject.previousShadowsValue = paraEditObject.shadowsValue
        paraEditObject.previousShadowsSliderValue = viewShadowsSlider.value
        editedImageExceptCurrentEffect = applyAllEditEffectsExceptCurrent(shadows: self.paraEditObject.shadowsValue,
                                                                          highlights: self.paraEditObject.highlightsValue,
                                                                          contrast: self.paraEditObject.contrastValue,
                                                                          saturation: self.paraEditObject.saturationValue,
                                                                          brightness: self.paraEditObject.brightnessValue,
                                                                          warm: self.paraEditObject.warmValue,
                                                                          filter: paraEditObject.filterValue, currentApplyEffect: EditFunction().shadow)
    }
    
    func resetShadowsView()
    {
        self.enableStickerTextSubviews()
        paraEditObject.shadowsValue = paraEditObject.previousShadowsValue
        // Reset custom slider value
        viewShadowsSlider.set(value: paraEditObject.previousShadowsSliderValue, animated: true)
        applyEditEffects(shadows: self.paraEditObject.shadowsValue,
                         highlights: self.paraEditObject.highlightsValue,
                         contrast: self.paraEditObject.contrastValue,
                         saturation: self.paraEditObject.saturationValue,
                         brightness: self.paraEditObject.brightnessValue,
                         warm: self.paraEditObject.warmValue,
                         filter: paraEditObject.filterValue)
    }
}
