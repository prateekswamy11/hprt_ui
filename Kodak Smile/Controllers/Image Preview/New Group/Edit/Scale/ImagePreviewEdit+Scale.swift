//
//  ImagePreviewEdit+Scale.swift
//  Polaroid MINT
//
//  Created by maximess142 on 14/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import CopilotAPIAccess

extension ImagePreviewViewController
{
    func setUpScaleView()
    {
        self.disableStickerTextSubviews()

        self.viewGrid.isHidden = false
        
        self.viewScale.isHidden = false
        
        self.changeGestureOnImage(pan: true, pinch: true, rotate: false)
        
        ///// save previous Rotate value
        paraEditObject.previousScaleZoomValue = paraEditObject.scaleZoomValue
        paraEditObject.previousScaleTranslationValue = paraEditObject.scaleTranslationValue
        let pinchAnalyticsEvent = PinchAnalyticsEvent(state: AnalyticsValue.state.less.rawValue)
        Copilot.instance.report.log(event: pinchAnalyticsEvent)
        
    }
    
    func resetScaleView()
    {
        self.disableScrollZoom()
        self.enableStickerTextSubviews()
        
        self.changeGestureOnImage(pan: false, pinch: false, rotate: false)
        self.viewGrid.isHidden = true
        self.viewScale.isHidden = true
        
        ///// save previous Scale value
        if paraEditObject.scaleZoomValue != paraEditObject.previousScaleZoomValue {
        paraEditObject.scaleZoomValue = paraEditObject.previousScaleZoomValue
        paraEditObject.scaleTranslationValue = paraEditObject.previousScaleTranslationValue
        print("paraEditObject.rotateValue\(paraEditObject.rotateValue)")
        self.scrollViewForZooming.transform = CGAffineTransform.identity.rotated(by: CGFloat((paraEditObject.rotateValue * .pi) / 180)).scaledBy(x: paraEditObject.scaleZoomValue, y: paraEditObject.scaleZoomValue)//.translatedBy(x: paraEditObject.scaleTranslationValue.x, y: paraEditObject.scaleTranslationValue.y)
        
        self.scrollViewForZooming.center = paraEditObject.scaleTranslationValue
        }

    }
}
