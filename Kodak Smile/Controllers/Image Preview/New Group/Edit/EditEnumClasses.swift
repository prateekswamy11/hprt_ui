//
//  EditEnumClasses.swift
//  Polaroid MINT
//
//  Created by maximess142 on 27/08/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation

enum EditSubOptions: Int {
    case Scale
    case Rotate
    case Brightness
    case Contrast
    case Warm
    case Saturation
    case Highlights
    case Shadows
}
