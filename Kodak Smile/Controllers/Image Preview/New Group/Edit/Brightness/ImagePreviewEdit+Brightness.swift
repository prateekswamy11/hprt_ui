//
//  ImagePreviewEdit+Brightness.swift
//  Polaroid MINT
//
//  Created by maximess142 on 28/08/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation

extension ImagePreviewViewController
{
    func setUpBrightnessFilter()
    {
        self.disableStickerTextSubviews()
        
        self.viewBrightness.isHidden = false
        
        let aUIImage = imageViewPreview.image;
        let aCGImage = aUIImage?.cgImage;
        aCIImage = CIImage.init(cgImage: aCGImage!)
        context = CIContext(options: nil);
        
        brightnessFilter = CIFilter(name: "CIColorControls");
        brightnessFilter.setValue(aCIImage, forKey: "inputImage")
        
        ///// save previous brightness value
        paraEditObject.previousBrightnessValue = paraEditObject.brightnessValue
        paraEditObject.previousBrightnessSliderValue = viewBrightnessSlider.value
        
        editedImageExceptCurrentEffect = applyAllEditEffectsExceptCurrent(shadows: self.paraEditObject.shadowsValue,
                                                                          highlights: self.paraEditObject.highlightsValue,
                                                                          contrast: self.paraEditObject.contrastValue,
                                                                          saturation: self.paraEditObject.saturationValue,
                                                                          brightness: self.paraEditObject.brightnessValue,
                                                                          warm: self.paraEditObject.warmValue,
                                                                          filter: paraEditObject.filterValue, currentApplyEffect: EditFunction().brightness)
    }
    
    @IBAction func brightnessSliderValueChanged(_ sender: UISlider)
    {
        brightnessFilter.setValue(NSNumber(value: sender.value), forKey: kCIInputBrightnessKey);
        let outputImage = brightnessFilter.outputImage!;
        let imageRef = context.createCGImage(outputImage, from: outputImage.extent)
        newUIImage = UIImage.init(cgImage: imageRef!) //(CGImage: imageRef!)!
        imageViewPreview.image = newUIImage;
        
        //self.showSaveEditsOption()
    }
    
    func resetBrightnessView()
    {
        self.enableStickerTextSubviews()
        ///// save previous brightness value
        paraEditObject.brightnessValue = paraEditObject.previousBrightnessValue
        
        // Reset custom slider value
        viewBrightnessSlider.set(value: paraEditObject.previousBrightnessSliderValue, animated: true)
        
        applyEditEffects(shadows: self.paraEditObject.shadowsValue, highlights: self.paraEditObject.highlightsValue, contrast: self.paraEditObject.contrastValue, saturation: self.paraEditObject.saturationValue, brightness: self.paraEditObject.brightnessValue, warm: self.paraEditObject.warmValue, filter: paraEditObject.filterValue)
    }
}
