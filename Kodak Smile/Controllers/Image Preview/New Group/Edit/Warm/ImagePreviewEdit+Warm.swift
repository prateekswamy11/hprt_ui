//
//  ImagePreviewEdit+Warm.swift
//  Polaroid MINT
//
//  Created by maximess142 on 29/08/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation

extension ImagePreviewViewController
{
    func setUpWarmFilter()
    {
        self.disableStickerTextSubviews()
        
        self.viewWarm.isHidden = false
        
        
        ///// save previous Warm value
        paraEditObject.previousWarmValue = paraEditObject.warmValue
        paraEditObject.previousWarmSliderValue = viewWarmSlider.value
        
        editedImageExceptCurrentEffect = applyAllEditEffectsExceptCurrent(shadows: self.paraEditObject.shadowsValue,
                                                                          highlights: self.paraEditObject.highlightsValue,
                                                                          contrast: self.paraEditObject.contrastValue,
                                                                          saturation: self.paraEditObject.saturationValue,
                                                                          brightness: self.paraEditObject.brightnessValue,
                                                                          warm: self.paraEditObject.warmValue,
                                                                          filter: paraEditObject.filterValue, currentApplyEffect: EditFunction().warm)
    }
    
    func resetWarmView()
    {
        self.enableStickerTextSubviews()
        ///// save previous brightness value
        paraEditObject.warmValue = paraEditObject.previousWarmValue
        
        // Reset custom slider value
        viewWarmSlider.set(value: paraEditObject.previousWarmSliderValue, animated: true)
        
        applyEditEffects(shadows: self.paraEditObject.shadowsValue,
                         highlights: self.paraEditObject.highlightsValue,
                         contrast: self.paraEditObject.contrastValue,
                         saturation: self.paraEditObject.saturationValue,
                         brightness: self.paraEditObject.brightnessValue,
                         warm: self.paraEditObject.warmValue,
                         filter: paraEditObject.filterValue)
    }
}
