

//  Created by maximess142 on 17/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation

extension ImagePreviewViewController
{
    func setUpRotateView()
    {
        self.disableStickerTextSubviews()
        
        self.viewGrid.isHidden = false
        self.viewRotate.isHidden = false
        self.changeGestureOnImage(pan: true, pinch: true, rotate: false)
       
        ///// save previous Rotate value
        paraEditObject.previousScaleZoomValue = paraEditObject.scaleZoomValue
        paraEditObject.previousScaleTranslationValue = paraEditObject.scaleTranslationValue
       
        ///// save previous Rotate value
        paraEditObject.previousRotateSliderValue = viewRotateSlider.value
        paraEditObject.previousrotate90Value = paraEditObject.rotate90Value
    }
    
    @IBAction func btnRotateClicked(_ sender: UIButton)
    {
        // Reset the value changed by slider
        //paraEditObject.rotate90Value
        print("paraEditObject.rotateValue\(paraEditObject.rotateValue)")
              
        switch paraEditObject.rotateValue {
        case 1 ... 89:
            if sender.tag == 0{
                paraEditObject.rotateValue = 90.0
            }else{
                paraEditObject.rotateValue = 0.0
            }
        case 91 ... 179:
            if sender.tag == 0{
                paraEditObject.rotateValue = 180.0
            }else{
                paraEditObject.rotateValue = 90.0
            }
        case -89 ... -1:
            if sender.tag == 0{
                paraEditObject.rotateValue = 0.0
            }else{
                paraEditObject.rotateValue = -90.0
            }
        case -179 ... -91:
            if sender.tag == 0{
                paraEditObject.rotateValue = -90.0
            }else{
                paraEditObject.rotateValue = -180.0
            }
        case 0:
            if sender.tag == 0{
                paraEditObject.rotateValue = 90.0
            }else{
                paraEditObject.rotateValue = -90.0
            }
        case 90:
            if sender.tag == 0{
                paraEditObject.rotateValue = 180.0
            }else{
                paraEditObject.rotateValue = 0.0
            }
        case 180:
            if sender.tag == 0{
                paraEditObject.rotateValue = -90.0
            }else{
                paraEditObject.rotateValue = 90.0
            }
        case -90:
            if sender.tag == 0{
                paraEditObject.rotateValue = 0.0
            }else{
                paraEditObject.rotateValue = -180.0
            }
        case -180:
            if sender.tag == 0{
                paraEditObject.rotateValue = -90.0
            }else{
                paraEditObject.rotateValue = 90.0
            }
        default:
            print("failure")
        }
        
        self.scrollViewForZooming.transform = CGAffineTransform.identity.scaledBy(x: paraEditObject.scaleZoomValue, y: paraEditObject.scaleZoomValue).rotated(by: CGFloat((paraEditObject.rotateValue * .pi) / 180))
        // Reset the slider value
        self.viewRotateSlider.set(value: paraEditObject.rotateValue, animated: true)
    }
    
    func resetRotateView()
    {
        self.enableStickerTextSubviews()
        
        self.viewGrid.isHidden = true
        self.viewRotate.isHidden = true
        self.viewRotateSlider.set(value: paraEditObject.previousRotateSliderValue, animated: true)
        
        if paraEditObject.scaleZoomValue != paraEditObject.previousScaleZoomValue {
            paraEditObject.scaleZoomValue = paraEditObject.previousScaleZoomValue
            paraEditObject.scaleTranslationValue = paraEditObject.previousScaleTranslationValue
            
            self.scrollViewForZooming.transform = CGAffineTransform.identity.rotated(by: CGFloat((paraEditObject.previousRotateSliderValue * .pi) / 180)).scaledBy(x: paraEditObject.scaleZoomValue, y: paraEditObject.scaleZoomValue)
            self.scrollViewForZooming.center = paraEditObject.scaleTranslationValue
        }
        if paraEditObject.rotateValue != paraEditObject.previousRotateSliderValue{
            paraEditObject.rotateValue = paraEditObject.previousRotateSliderValue
            paraEditObject.rotate90Value = paraEditObject.previousrotate90Value
            self.scrollViewForZooming.transform = CGAffineTransform.identity.rotated(by: CGFloat((paraEditObject.previousRotateSliderValue * .pi) / 180)).scaledBy(x: paraEditObject.scaleZoomValue, y: paraEditObject.scaleZoomValue)
            self.scrollViewForZooming.center = paraEditObject.scaleTranslationValue
        }
        self.changeGestureOnImage(pan: false, pinch: false, rotate: false)
    }
}
