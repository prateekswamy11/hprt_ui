//
//  ImagePreviewEfit+Highlights.swift
//  Polaroid MINT
//
//  Created by maximess142 on 25/01/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

extension ImagePreviewViewController
{
    func setUpHighlightsFilter()
    {
        self.disableStickerTextSubviews()
        
        self.viewHighlights.isHidden = false

        ///// save previous Contrast value
        paraEditObject.previousHighlightsValue = paraEditObject.highlightsValue
        paraEditObject.previousHighlightsSliderValue = viewHighlightsSlider.value
        editedImageExceptCurrentEffect = applyAllEditEffectsExceptCurrent(shadows: self.paraEditObject.shadowsValue,
                                                                          highlights: self.paraEditObject.highlightsValue,
                                                                          contrast: self.paraEditObject.contrastValue,
                                                                          saturation: self.paraEditObject.saturationValue,
                                                                          brightness: self.paraEditObject.brightnessValue,
                                                                          warm: self.paraEditObject.warmValue,
                                                                          filter: paraEditObject.filterValue, currentApplyEffect: EditFunction().highlights)
    }
    
    func resetHighlightsView()
    {
        self.enableStickerTextSubviews()
        
        paraEditObject.highlightsValue = paraEditObject.previousHighlightsValue
        
        // Reset custom slider value
        viewHighlightsSlider.set(value: paraEditObject.previousHighlightsSliderValue, animated: true)
        
        applyEditEffects(shadows: self.paraEditObject.shadowsValue,
                         highlights: self.paraEditObject.highlightsValue,
                         contrast: self.paraEditObject.contrastValue,
                         saturation: self.paraEditObject.saturationValue,
                         brightness: self.paraEditObject.brightnessValue,
                         warm: self.paraEditObject.warmValue,
                         filter: paraEditObject.filterValue)
    }
}
