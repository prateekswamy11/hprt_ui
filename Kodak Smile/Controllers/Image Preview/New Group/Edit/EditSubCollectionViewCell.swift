//
//  EditSubCollectionViewCell.swift
//  Polaroid MINT
//
//  Created by maximess142 on 27/08/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class EditSubCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblOptionName: UILabel!
    @IBOutlet weak var imgViewOptionIcon: UIImageView!
    
}
