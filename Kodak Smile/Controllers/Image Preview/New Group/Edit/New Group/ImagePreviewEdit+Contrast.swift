//
//  ImagePreviewEdit+Contrast.swift
//  Polaroid MINT
//
//  Created by maximess142 on 12/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation

extension ImagePreviewViewController
{
    func setUpContrastFilter()
    {
        self.disableStickerTextSubviews()
        
        self.viewContrast.isHidden = false
        
        let aUIImage = imageViewPreview.image;
        let aCGImage = aUIImage?.cgImage;
        aCIImage = CIImage.init(cgImage: aCGImage!)
        context = CIContext(options: nil);
        
        contrastFilter = CIFilter(name: "CIColorControls");
        contrastFilter.setValue(aCIImage, forKey: "inputImage")
        
        ///// save previous Contrast value
        paraEditObject.previousContrastValue = paraEditObject.contrastValue
        paraEditObject.previousContrastSliderValue = viewContrastSlider.value
        
        editedImageExceptCurrentEffect = applyAllEditEffectsExceptCurrent(shadows: self.paraEditObject.shadowsValue,
                                                                          highlights: self.paraEditObject.highlightsValue,
                                                                          contrast: self.paraEditObject.contrastValue,
                                                                          saturation: self.paraEditObject.saturationValue,
                                                                          brightness: self.paraEditObject.brightnessValue,
                                                                          warm: self.paraEditObject.warmValue,
                                                                          filter: paraEditObject.filterValue, currentApplyEffect: EditFunction().contrast)
    }
    
    func resetContrastView()
    {
        ///// save previous contrast value
        paraEditObject.contrastValue = paraEditObject.previousContrastValue
        
        // Reset custom slider value
        viewContrastSlider.set(value: paraEditObject.previousContrastSliderValue, animated: true)
        
        applyEditEffects(shadows: self.paraEditObject.shadowsValue, highlights: self.paraEditObject.highlightsValue, contrast: self.paraEditObject.contrastValue, saturation: self.paraEditObject.saturationValue, brightness: self.paraEditObject.brightnessValue, warm: self.paraEditObject.warmValue, filter: paraEditObject.filterValue)
        
    }
}
