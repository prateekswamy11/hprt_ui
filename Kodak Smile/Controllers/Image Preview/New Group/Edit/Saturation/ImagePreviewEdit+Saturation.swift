//
//  ImagePreviewEdit+Saturation.swift
//  Polaroid MINT
//
//  Created by maximess142 on 14/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation

extension ImagePreviewViewController
{
    func setUpSaturationFilter()
    {
        self.disableStickerTextSubviews()
        
        self.viewSaturation.isHidden = false
        
        let aUIImage = imageViewPreview.image;
        let aCGImage = aUIImage?.cgImage;
        aCIImage = CIImage.init(cgImage: aCGImage!)
        context = CIContext(options: nil);
        
        saturationFilter = CIFilter(name: "CIColorControls");
        saturationFilter.setValue(aCIImage, forKey: "inputImage")
        
        ///// save previous brightness value
        paraEditObject.previousSaturationValue = paraEditObject.saturationValue
        paraEditObject.previousSaturationSliderValue = viewSaturationSlider.value
        
        editedImageExceptCurrentEffect = applyAllEditEffectsExceptCurrent(shadows: self.paraEditObject.shadowsValue,
                                                                          highlights: self.paraEditObject.highlightsValue,
                                                                          contrast: self.paraEditObject.contrastValue,
                                                                          saturation: self.paraEditObject.saturationValue,
                                                                          brightness: self.paraEditObject.brightnessValue,
                                                                          warm: self.paraEditObject.warmValue,
                                                                          filter: paraEditObject.filterValue, currentApplyEffect: EditFunction().satusartion)
    }
    
    func resetSaturationView()
    {
        self.enableStickerTextSubviews()
        
        ///// save previous saturation value
        paraEditObject.saturationValue = paraEditObject.previousSaturationValue
        
        // Reset custom slider value
        viewSaturationSlider.set(value: paraEditObject.previousSaturationSliderValue, animated: true)
        applyEditEffects(shadows: self.paraEditObject.shadowsValue, highlights: self.paraEditObject.highlightsValue, contrast: self.paraEditObject.contrastValue, saturation: self.paraEditObject.saturationValue, brightness: self.paraEditObject.brightnessValue, warm: self.paraEditObject.warmValue, filter: paraEditObject.filterValue)
    }
}
