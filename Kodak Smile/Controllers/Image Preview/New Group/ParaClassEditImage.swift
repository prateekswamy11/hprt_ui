//
//  ParaClassEditImage.swift
//  Snaptouch_Polaroid
//
//  Created by maximess142 on 13/12/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit

class ParaClassEditImage: NSObject
{
    var currentEditOptionIndex = 0 // By default filters
    
    // Filters
    var selectedFilterIndex = 0
    
    // Edit
    var selectedSubEditOption = 0
    
    // Style
    var selectedSubStyleOption = 0
    
    // Frames
    var selectedFramesIndex = 0
    
    // Stickers
    var selectedStickerCatIndex = 0
    var selectedStickerIndex = -1
    var selectedStickerCatString = ""
    var selectedStickerView : StickerCustomView?
    
    // Doodle
    var selectedDoodleColorIndex = 0
    var selectedDoodleOption = 0 // 0 - Pencil, 1 - Eraser
    var selectedDoodlePencilWidth : Float = 6.0
    var selectedDoodleEraserWidth : Float = 6.0
    
    // Text
    var selectedTextFontIndex = 0
    var selectedTextColorIndex = 0
    var textString = ""
    var selectedTextView : TextCustomView?
    
    //Adjust
    var isAdjustEnable = false
    
    //current applied edit parameters values
    var highlightsValue : CGFloat = 1
    var shadowsValue : CGFloat = 0
    
    var contrastValue : CGFloat = 1
    var brightnessValue : CGFloat  = 0
    var saturationValue : CGFloat = 1
    var warmValue : CGFloat = 0.5
    var rotateValue : Double = 0
    var scaleZoomValue : CGFloat = 1.0
    var scaleTranslationValue = CGPoint()
    var filterValue : Int = 0
    var rotate90Value : CGFloat = 0
    
    //previous applied edit parameters values
    var previousHighlightsValue : CGFloat = 1
    var previousShadowsValue : CGFloat = 0
    var previousContrastValue : CGFloat = 1
    var previousBrightnessValue : CGFloat  = 0
    var previousSaturationValue : CGFloat = 1
    var previousWarmValue : CGFloat = 0.5
    var previousScaleZoomValue : CGFloat = 1.0
    var previousScaleTranslationValue = CGPoint()
    var previousrotate90Value : CGFloat = 0
    
    //previous applied edit parameters Slider values
    var previousHighlightsSliderValue : Double = 0
    var previousShadowsSliderValue : Double = 0
    
    var previousContrastSliderValue : Double = 0
    var previousBrightnessSliderValue : Double  = 0
    var previousSaturationSliderValue : Double = 0
    var previousWarmSliderValue : Double = 0
    var previousRotateSliderValue : Double = 0
    
    var isImageEdited : Bool = false
    
    var isImageEdited_stickers : Bool = false
    var isImageEdited_doodle : Bool = false
    var isImageEdited_sliderValue : Bool = false
    var isImageEdited_previous : Bool = false
}


//
