//
//  ImagePreviewEdit+Filter.swift
//  Polaroid MINT
//
//  Created by maximess142 on 22/08/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import UIKit

extension ImagePreviewViewController
{
    class CustomSlider: UISlider {
        // Custom Class made for the vertical Slider
        override func trackRect(forBounds bounds: CGRect) -> CGRect {
            let customBounds = CGRect(origin: bounds.origin, size: CGSize(width: bounds.size.width, height: 5.0))
            super.trackRect(forBounds: customBounds)
            return customBounds
        }
    }
    
    // MARK: - Filters Functions
    func filter_setImage()              //withIndex index: Int,completion: @escaping (UIImage?, Error?) -> Void
    {
//       self.startLoader()
        DispatchQueue.global(qos: .background).sync {
            autoreleasepool {
                for index in 1..<self.filterEffectsNameList.count{
                    self.applyFilterToThumbnail(withIndex: index, completion: { (image, error) in
                        //                    DispatchQueue.main.async {
                        if let filterImg = image
                        {
                            self.arrFilterPreviewImages.append(filterImg)
                        }
                        //                    }
                    })
                }
            }
//            self.getStickerAndFrames()
            DispatchQueue.main.async(group: .none) {
           //     self.stopLoader()
                //                    self.getStickerImages()
                self.setupEditSliders()
                self.paraEditObject.scaleTranslationValue = self.imageViewPreview.center
            }
        }
    }
    
    func applyFilterToThumbnail(
        withIndex index: Int,
        completion: @escaping (UIImage?, Error?) -> Void)
    {
        if let lutImage = UIImage(named: self.filterNameList[index]){
            lutData64 = LUTConverter.cubeDataForLut64(lutImage: lutImage)!
        }// Or if you use the 64 x 64 lut image
        // Then use your 3D LUT data to initialize a EffectFilter.
        let effectFilter = EffectFilter(customFilter: lutData64, withDimension: .sixtyFour)
        // Apply an input intensity between 0 and 1.0
        // effectFilter.inputIntensity = 0.8
        let aUIImage = self.filterThumbnailImage.aspectFit(CGSize(width: 80, height: 80))
        let aCGImage = aUIImage?.cgImage
        if aCGImage != nil{
            let aCIImage = CIImage.init(cgImage: (aCGImage)!)
            effectFilter.inputImage = aCIImage
            let outputImage = effectFilter.outputImage
            
            let imageRef = context.createCGImage(outputImage!, from: outputImage!.extent)
            let filterImg  = UIImage.init(cgImage: imageRef!) //(CGImage: imageRef!)!
             completion(filterImg, nil)
        }
    }
    
    func setUISliderThumbValueWithLabel(slider: UISlider) -> CGPoint {
        // get thumbvalue of Slider and move Label According to Slider Movement.
        let slidertTrack : CGRect = slider.trackRect(forBounds: slider.bounds)
        let sliderFrm : CGRect = slider .thumbRect(forBounds: slider.bounds, trackRect: slidertTrack, value: slider.value)
        return CGPoint(x: slider.frame.maxX + 23, y: (slider.frame.maxY - 15) - sliderFrm.origin.x )
    }
    
    @IBAction func opacitySliderChanged(_ sender: UISlider)
    {
        // Filter is initialised when didSelect of filter, now only change the slider value and call the function again
        self.lblSliderValue.isHidden = false
        let newIntensity = round(10*sender.value)/10
        // Set Label Value Max = 100  / Min = 0
        let x = Int(round(100*sender.value))
        lblSliderValue.text = "\(x)"
        lblSliderValue.center = setUISliderThumbValueWithLabel(slider: sender)
        if newIntensity != selectedEffectFilter?.inputIntensity
        {
            autoreleasepool {
                self.selectedEffectFilter = EffectFilter(customFilter: selectedLutData64, withDimension: .sixtyFour)
                self.selectedEffectFilter?.inputImage = self.aCIImage
                self.selectedEffectFilter?.inputIntensity = newIntensity
                self.applyEditEffects(shadows: self.paraEditObject.shadowsValue,
                                      highlights: self.paraEditObject.highlightsValue,
                                      contrast: self.paraEditObject.contrastValue,
                                      saturation: self.paraEditObject.saturationValue,
                                      brightness: self.paraEditObject.brightnessValue,
                                      warm: self.paraEditObject.warmValue,
                                      filter: self.paraEditObject.filterValue)
            }
        }
    }
    
    func applySelectedFilterToImage()
    {
        self.lblSliderValue.isHidden = true
        let index = paraEditObject.selectedFilterIndex
        let originalImg = imgMainEdit
        self.paraEditObject.filterValue = index
        // Clicked for original image
        if index == 0
        {
            self.sliderFilterOpacity.value = 1
            
            //self.imageViewPreview.image = originalImg
            applyEditEffects(shadows: self.paraEditObject.shadowsValue,
                             highlights: self.paraEditObject.highlightsValue,
                             contrast: self.paraEditObject.contrastValue,
                             saturation: self.paraEditObject.saturationValue,
                             brightness: self.paraEditObject.brightnessValue,
                             warm: self.paraEditObject.warmValue,
                             filter: paraEditObject.filterValue)
        }
        else // apply LUT filter
        {
            DispatchQueue.main.async(execute: {
                self.view.isUserInteractionEnabled = false
                //self.activityIndicatorOnImg.startAnimating()
                UIApplication.shared.beginIgnoringInteractionEvents()
                autoreleasepool {
                    self.sliderFilterOpacity.value = 1
                    
                    let lutImage = UIImage(named: self.filterNameList[index])
                    //let lutData64
                    self.selectedLutData64 = Data()
                    self.selectedLutData64 = LUTConverter.cubeDataForLut64(lutImage: lutImage!)! // Or if you use the 64 x 64 lut image
                    // Then use your 3D LUT data to initialize a EffectFilter.
                    self.selectedEffectFilter = EffectFilter(customFilter: self.selectedLutData64, withDimension: .sixtyFour)
                    self.selectedEffectFilter?.inputImage = self.aCIImage
                    self.selectedEffectFilter?.inputIntensity = self.sliderFilterOpacity.value
                    self.applyEditEffects(shadows: self.paraEditObject.shadowsValue,
                                          highlights: self.paraEditObject.highlightsValue,
                                          contrast: self.paraEditObject.contrastValue,
                                          saturation: self.paraEditObject.saturationValue,
                                          brightness: self.paraEditObject.brightnessValue,
                                          warm: self.paraEditObject.warmValue,
                                          filter: self.paraEditObject.filterValue)
                    self.view.isUserInteractionEnabled = true
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            })
        }
        //self.showSaveEditsOption()
    }
    
    
    func applySelectedNativeFilterToImage()
    {
        let index = paraEditObject.selectedFilterIndex
        self.paraEditObject.filterValue = index
        // Clicked for original image
        if index == 0
        {
            //self.imageViewPreview.image = originalImg
            applyEditEffects(shadows: self.paraEditObject.shadowsValue,
                             highlights: self.paraEditObject.highlightsValue,
                             contrast: self.paraEditObject.contrastValue,
                             saturation: self.paraEditObject.saturationValue,
                             brightness: self.paraEditObject.brightnessValue,
                             warm: self.paraEditObject.warmValue,
                             filter: paraEditObject.filterValue)
        }
        else // apply LUT filter
        {
            DispatchQueue.main.async(execute: {
                self.view.isUserInteractionEnabled = false
                //self.activityIndicatorOnImg.startAnimating()
                UIApplication.shared.beginIgnoringInteractionEvents()
                autoreleasepool {
                    self.applyEditEffects(shadows: self.paraEditObject.shadowsValue,
                                          highlights: self.paraEditObject.highlightsValue,
                                          contrast: self.paraEditObject.contrastValue,
                                          saturation: self.paraEditObject.saturationValue,
                                          brightness: self.paraEditObject.brightnessValue,
                                          warm: self.paraEditObject.warmValue,
                                          filter: self.paraEditObject.filterValue)
                    self.view.isUserInteractionEnabled = true
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            })
        }
        //self.showSaveEditsOption()
    }
    
    func applyNativeFilterToImage(
        withIndex index: Int,
        completion: @escaping (UIImage?, Error?) -> Void)
    {
        DispatchQueue.global(qos: .background).sync {
            autoreleasepool {
                print(nativeFiltersArray[index])
                let filterEffect = CIFilter(name: nativeFiltersArray[index])!
                let beginImage = CIImage(cgImage: filterThumbnailImage.cgImage!)
                filterEffect.setValue(beginImage, forKey: "inputImage")
                let outputImage = filterEffect.outputImage!;
                let cgImage = self.context.createCGImage(outputImage, from: outputImage.extent)!
                let filterImg = UIImage.init(cgImage: cgImage) //(CGImage: imageRef!)!
                DispatchQueue.main.async {
                    self.arrFilterPreviewImages.append(filterImg)
                    completion(filterImg, nil)
                    print("Sending the image back with ", index)
                }
            }
        }
    }
}
