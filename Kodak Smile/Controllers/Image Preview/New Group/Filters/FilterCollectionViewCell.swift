//
//  FilterCollectionViewCell.swift
//  Polaroid MINT
//
//  Created by maximess142 on 22/08/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class FilterCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imageViewDeselected: UIImageView!
    @IBOutlet weak var lblFilterName: UILabel!
    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var lblFilterNameSelected: UILabel!
    @IBOutlet weak var imageViewSelected: UIImageView!
    @IBOutlet weak var viewBarSelected: UIView!
    @IBOutlet weak var activityIndicatorImg: UIActivityIndicatorView!
}
