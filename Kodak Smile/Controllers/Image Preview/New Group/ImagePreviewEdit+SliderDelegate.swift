//
//  ImagePreviewEdit+SliderDelegate.swift
//  Polaroid MINT
//
//  Created by maximess142 on 12/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import HSCenterSlider

extension ImagePreviewViewController: HSCenterSliderDelegate
{
    public func centerSlider(slider: HSCenterSlider, didChange value: Double) {
        if value >= -1 && value <= 1{
           slider.set(value: 0, animated: true)
        }
        let roundedValue = round(value)
        print(roundedValue, value)
        if slider == viewWarmSlider
        {
            // Logic for this slider is -
            // Every slider change will be of 1 unit = 0.5 / 100
            // And middle value is 0.5 so formula is as below
            let finalValue : CGFloat = 0.5 + CGFloat(roundedValue * 0.0025)
            paraEditObject.warmValue = finalValue
            //self.applyTemperatureFilterWithValue(r: finalValue)
            let r: CGFloat = finalValue,
            g: CGFloat = 0.5,
            b: CGFloat = 0.5,
            a: CGFloat = 1.38
            autoreleasepool {
                let warmParameters = ["inputRVector": CIVector(x: r, y: 0, z: 0, w: 0),"inputGVector": CIVector(x: 0, y: g, z: 0, w: 0),"inputBVector": CIVector(x: 0, y: 0, z: b, w: 0),"inputAVector": CIVector(x: 0, y: 0, z: 0, w: a)] as [String : Any]
                imageViewPreview.image = editedImageExceptCurrentEffect.af_imageFiltered(withCoreImageFilter: "CIColorMatrix", parameters: warmParameters)!
            }
        }
        else if slider == viewRotateSlider
        {
            // Logic for this slider is -
            // Directly applying slider value to function
            paraEditObject.rotateValue = roundedValue
            autoreleasepool {
                self.scrollViewForZooming.transform = CGAffineTransform.identity.rotated(by: CGFloat((roundedValue * .pi) / 180)).scaledBy(x: paraEditObject.scaleZoomValue, y: paraEditObject.scaleZoomValue)
            }
        }
        else if slider == viewBrightnessSlider
        {
            // Logic for this slider is -
            // Every slider change will be of 1 unit = 1 / 200
            // And middle value is 0 so formula is as below
            let finalValue : CGFloat = CGFloat(roundedValue / 200.0)
            paraEditObject.brightnessValue = finalValue //NSNumber(value: finalValue)
            autoreleasepool {
                let filterParameters = ["inputBrightness": finalValue]
                imageViewPreview.image = editedImageExceptCurrentEffect.af_imageFiltered(withCoreImageFilter: "CIColorControls", parameters: filterParameters)!
            }
            
        }
        else if slider == viewContrastSlider
        {
            // Logic for this slider is -
            // Every slider change will be of 1 unit = 1 / 100
            // And middle value is 1 so formula is as below
            let finalValue : CGFloat = 1 + CGFloat(roundedValue * 0.005)//0.01 is exact value
            paraEditObject.contrastValue = finalValue
            autoreleasepool {
                let filterParameters = ["inputContrast": finalValue]
                imageViewPreview.image = editedImageExceptCurrentEffect.af_imageFiltered(withCoreImageFilter: "CIColorControls", parameters: filterParameters)!
            }
        }
        else if slider == viewSaturationSlider
        {
            // Logic for this slider is -
            // Every slider change will be of 1 unit = 1 / 100
            // And middle value is 1 so formula is as below
            let finalValue : CGFloat = 1 + CGFloat(roundedValue * 0.01)// / 200.0)
            paraEditObject.saturationValue = finalValue
            autoreleasepool {
                let filterParameters = ["inputSaturation": finalValue]
                imageViewPreview.image = editedImageExceptCurrentEffect.af_imageFiltered(withCoreImageFilter: "CIColorControls", parameters: filterParameters)!
            }
        }
        else if slider == viewHighlightsSlider
        {
            // Logic for this slider is -
            // Every slider change will be of 1 unit = 1 / 100
            // And middle value is 1 so formula is as below
            var finalValue = CGFloat()
            if roundedValue >= 0
            {
                finalValue  = 1 - CGFloat(roundedValue / 200 )//CGFloat(roundedValue / 200 + 0.5)// / 200.0)0.01
            }
            else{
                finalValue  = 0.5 + CGFloat(roundedValue / 200 )
            }
            paraEditObject.highlightsValue = finalValue
            //print("hightlight = \(finalValue)")
            autoreleasepool {
                let highlightsParameters = ["inputHighlightAmount": finalValue]
                
                imageViewPreview.image = editedImageExceptCurrentEffect.af_imageFiltered(withCoreImageFilter: "CIHighlightShadowAdjust", parameters: highlightsParameters)!
            }
        }
        else if slider == viewShadowsSlider
        {
            // Logic for this slider is -
            // Every slider change will be of 1 unit = 1 / 100
            // And middle value is 0 so formula is as below
            let finalValue : CGFloat = CGFloat(roundedValue * 0.01)// / 200.0)
            paraEditObject.shadowsValue = finalValue
            autoreleasepool {
                let highlightsParameters = ["inputShadowAmount":finalValue]
                
                imageViewPreview.image = editedImageExceptCurrentEffect.af_imageFiltered(withCoreImageFilter: "CIHighlightShadowAdjust", parameters: highlightsParameters)!
            }
        }
    }
    
    func applyEditEffects(shadows: CGFloat,
                          highlights: CGFloat,
                          contrast: CGFloat,
                          saturation:CGFloat,
                          brightness:CGFloat,
                          warm:CGFloat,
                          filter:Int)  {
        var editedImage = UIImage()
        editedImage = imgOriginal
        if filter > 0
        {
            autoreleasepool {
                // Create filter
                //editedImage = CIFilter().applyToImage(withName: self.filterNameList[filter], editedImage)
                let outputImage = self.selectedEffectFilter!.outputImage
                let imageRef = context.createCGImage(outputImage!, from: outputImage!.extent)
                editedImage  = UIImage.init(cgImage: imageRef ?? UIImage.init() as! CGImage)
            }
        }
        autoreleasepool {
            if brightness != 0
            {
                let filterParametersBrightness = ["inputBrightness": brightness]
                editedImage = editedImage.af_imageFiltered(withCoreImageFilter: "CIColorControls", parameters: filterParametersBrightness)!
            }
        }
        autoreleasepool {
            if contrast != 1
            {
                let filterParametersContrast = ["inputContrast": contrast]
                editedImage = editedImage.af_imageFiltered(withCoreImageFilter: "CIColorControls", parameters: filterParametersContrast)!
            }
        }
        autoreleasepool {
            if saturation != 1
            {
                let filterParametersSaturation = ["inputSaturation": saturation]
                editedImage = editedImage.af_imageFiltered(withCoreImageFilter: "CIColorControls", parameters: filterParametersSaturation)!
            }
        }
        //warm
        if warm < 0.5 || warm > 0.5
        {
            let r: CGFloat = warm,
            g: CGFloat = 0.5,
            b: CGFloat = 0.5,
            a: CGFloat = 1.38
            autoreleasepool {
                let warmParameters = ["inputRVector": CIVector(x: r, y: 0, z: 0, w: 0),"inputGVector": CIVector(x: 0, y: g, z: 0, w: 0),"inputBVector": CIVector(x: 0, y: 0, z: b, w: 0),"inputAVector": CIVector(x: 0, y: 0, z: 0, w: a)] as [String : Any]
                
                editedImage = editedImage.af_imageFiltered(withCoreImageFilter: "CIColorMatrix", parameters: warmParameters)!
            }
        }
        autoreleasepool {
            if highlights != 1
            {
                let highlightsParameters = ["inputHighlightAmount": highlights]
                
                editedImage = editedImage.af_imageFiltered(withCoreImageFilter: "CIHighlightShadowAdjust", parameters: highlightsParameters)!
            }
        }
        autoreleasepool {
            if shadows != 0
            {
                let highlightsParameters = ["inputShadowAmount":shadows]
                
                editedImage = editedImage.af_imageFiltered(withCoreImageFilter: "CIHighlightShadowAdjust", parameters: highlightsParameters)!
            }
        }
        autoreleasepool {
            imageViewPreview.image = editedImage
        }
    }
    
    func applyAllEditEffectsExceptCurrent(shadows: CGFloat,
                                          highlights: CGFloat,
                                          contrast: CGFloat,
                                          saturation:CGFloat,
                                          brightness:CGFloat,
                                          warm:CGFloat,
                                          filter:Int,currentApplyEffect : String) -> UIImage {
        var editedImage = UIImage()
        editedImage = imgOriginal
        if filter > 0
        {
            autoreleasepool {
                // Create filter
                //editedImage = CIFilter().applyToImage(withName: self.filterNameList[filter], editedImage)
                if  EditFunction().filter != currentApplyEffect
                {
                    //editedImage = CIFilter().applyToImage(withName: self.filterNameList[filter], editedImage)
                    let outputImage = self.selectedEffectFilter!.outputImage
                    let imageRef = context.createCGImage(outputImage!, from: outputImage!.extent)
                    editedImage  = UIImage.init(cgImage: imageRef!)
                    
                }
            }
        }
        autoreleasepool {
            if EditFunction().brightness != currentApplyEffect
            {
                let filterParameters = ["inputBrightness": brightness]
                editedImage = editedImage.af_imageFiltered(withCoreImageFilter: "CIColorControls", parameters: filterParameters)!
            }
        }
        autoreleasepool {
            if EditFunction().contrast != currentApplyEffect
            {
                let filterParameters = ["inputContrast": contrast]
                editedImage = editedImage.af_imageFiltered(withCoreImageFilter: "CIColorControls", parameters: filterParameters)!
            }
        }
        autoreleasepool {
            if EditFunction().satusartion != currentApplyEffect
            {
                let filterParameters = ["inputSaturation": saturation]
                editedImage = editedImage.af_imageFiltered(withCoreImageFilter: "CIColorControls", parameters: filterParameters)!
            }
        }
        //warm
        if (warm < 0.5 || warm > 0.5) && EditFunction().warm != currentApplyEffect
        {
            let r: CGFloat = warm,
            g: CGFloat = 0.5,
            b: CGFloat = 0.5,
            a: CGFloat = 1.38
            autoreleasepool {
                let warmParameters = ["inputRVector": CIVector(x: r, y: 0, z: 0, w: 0),"inputGVector": CIVector(x: 0, y: g, z: 0, w: 0),"inputBVector": CIVector(x: 0, y: 0, z: b, w: 0),"inputAVector": CIVector(x: 0, y: 0, z: 0, w: a)] as [String : Any]
                
                editedImage = editedImage.af_imageFiltered(withCoreImageFilter: "CIColorMatrix", parameters: warmParameters)!
            }
        }
        autoreleasepool {
            if highlights != 0 && EditFunction().highlights != currentApplyEffect
            {
                let highlightsParameters = ["inputHighlightAmount": highlights]
                
                editedImage = editedImage.af_imageFiltered(withCoreImageFilter: "CIHighlightShadowAdjust", parameters: highlightsParameters)!
            }
        }
        autoreleasepool {
            if shadows != 0 && EditFunction().shadow != currentApplyEffect
            {
                let highlightsParameters = ["inputShadowAmount":shadows]
                
                editedImage = editedImage.af_imageFiltered(withCoreImageFilter: "CIHighlightShadowAdjust", parameters: highlightsParameters)!
            }
        }
        autoreleasepool {
            imageViewPreview.image = editedImage
            imgMainEdit = editedImage
        }
        return editedImage
    }
}
