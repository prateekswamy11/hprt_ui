//
//  ParaClassIntermediateViewChanges.swift
//  Polaroid MINT
//
//  Created by maximess142 on 27/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class ParaClassIntermediateViewChanges: NSObject {

    // Sticker
    var arrayTempStickerSubviews = [StickerCustomView]()
    
    // Text
    var arrayTempTextSubviews = [TextCustomView]()

    // Doodle
    var countTempDoodle = 0
    var tempDoodleColorIndex = 0
    var tempDoodleOption = 0 // 0 - Pencil, 1 - Eraser
    var tempDoodlePencilWidth : Float = 6.0
    var tempDoodleEraserWidth : Float = 6.0
    
    
    // Borders
    var tempBorderIndex = 0
}
