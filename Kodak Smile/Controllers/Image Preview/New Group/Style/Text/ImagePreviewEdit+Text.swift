//
//  ImagePreviewEdit+Text.swift
//  Polaroid MINT
//
//  Created by maximess142 on 21/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation

extension ImagePreviewViewController
{
    // MARK: - Set up Methods
    func setUpTextView()
    {
        self.viewText.isHidden = false
        self.viewTextStickerCombined.isHidden = false
        self.viewTextStickerCombined.isUserInteractionEnabled = true
        self.showTextViewController()
        self.showCancelDoneButn()
        self.enableDisableGesturesOnStickerView(isEnable: false)
    }
    
    func openTextViewFromEdit()
    {
        // If currently text view is not open, open it
        if paraEditObject.selectedSubStyleOption != 1
        {
            // Stickers view was causing problems if it's drawer is opened, so close it
            self.resetStickersView()
            // Calling this cancel butn action here
            self.callCancelButnAction()
            // Forcefully set the text option editable
            paraEditObject.currentEditOptionIndex = 2
            paraEditObject.selectedSubStyleOption = 1
            self.collectionViewEditMainOptions.reloadData()
            self.viewText.isHidden = false
            self.viewTextStickerCombined.isHidden = false
            self.viewTextStickerCombined.isUserInteractionEnabled = true
            self.showCancelDoneButn()
        }
    }
    
    func showTextViewController()
    {
        let viewTextViewController = self.storyboard!.instantiateViewController(withIdentifier: "TextViewController") as! TextViewController
        // As this is create mode, so make the replace view nil
        viewTextViewController.textViewToEdit = nil
        self.view.addSubview(viewTextViewController.view)
        self.addChildViewController(viewTextViewController)
        viewTextViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
    }
    
    @objc func addTextLblToView(notification : NSNotification)
    {
        if let dict = notification.object as? [String:Any]
        {
            print(dict)
            
            // -15.0 given to set the X and Y of label properly to show text after typed as it is in back
            let xCoordinateTxtLbl = dict["textViewXAxis"] as! CGFloat - viewFixPaperSizeBackground.frame.origin.x - 15.0
            let yCoordinateTxtLbl = dict["textViewYAxis"] as! CGFloat - viewFixPaperSizeBackground.frame.origin.y - 15.0
            
            // 30.0 added to give padding to text
            let textLblView = TextCustomView(frame: CGRect(x: xCoordinateTxtLbl,y: yCoordinateTxtLbl, width: dict["textViewWidth"] as! CGFloat + 30.0, height: dict["textViewHeight"] as! CGFloat + 30.0))
            
            textLblView.text = dict["text"] as? String
            let font = dict["font"] as! UIFont
            textLblView.font = font
            let color = dict["color"] as! UIColor
            textLblView.textColor = color
            
            if textLblView.calculateMaxLines() == 1 {
                textLblView.textAlignment = .center
                textLblView.bounds.size.width = textLblView.intrinsicContentSize.width + 100.0
            }else{
                textLblView.textAlignment = .left
            }
            
            self.paraEditObject.selectedTextColorIndex = dict["selectedColorIndex"] as! Int
            self.paraEditObject.selectedTextFontIndex = dict["selectedFontIndex"] as! Int
            self.paraEditObject.textString = textLblView.text!
            
            textLblView.colorIndex = (dict["selectedColorIndex"] as? Int)!
            textLblView.fontIndex = (dict["selectedFontIndex"] as? Int)!
            
            self.viewTextStickerCombined.addSubview(textLblView)
        }
        
    }
    
    @objc func editTextLblOnView(textLblToEdit : TextCustomView)
    {
        let viewTextViewController = self.storyboard!.instantiateViewController(withIdentifier: "TextViewController") as! TextViewController
        viewTextViewController.paraTextObj.selectedColorIndex = textLblToEdit.colorIndex
        viewTextViewController.paraTextObj.textString = textLblToEdit.text!
        viewTextViewController.textViewToEdit = textLblToEdit
        viewTextViewController.fontSizeClickPreview = textLblToEdit.font.pointSize
        viewTextViewController.paraTextObj.selectedFontIndex = textLblToEdit.fontIndex
        // MARK: - Pratiksha Here it is showing 0 0 index for both check this.
        print("font index", textLblToEdit.fontIndex)
        print("color index", textLblToEdit.colorIndex)
        self.view.addSubview(viewTextViewController.view)
        self.addChildViewController(viewTextViewController)
        viewTextViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
    }
    
    // MARK: - Action Methods
    @IBAction func btnTextDeleteClicked(_ sender: Any)
    {
        self.paraEditObject.selectedTextView?.removeFromSuperview()
        
        self.btnTextDelete.isHidden = true
    }
    
    @IBAction func btnAddTextClicked(_ sender: Any)
    {
        self.showTextViewController()
    }
    
    @objc func replaceTextLblToView(notification : NSNotification)
    {
        if let dict = notification.object as? [String:Any]
        {
            let subViews = self.viewTextStickerCombined.subviews
            let receivedView = dict["replaceView"] as? TextCustomView
            for subview in subViews
            {
                if subview is TextCustomView
                {
                    if let txtLbl = subview as? TextCustomView
                    {
                        if txtLbl == receivedView
                        {
                            txtLbl.text = dict["text"] as? String
                            let font = dict["font"] as! UIFont
                            txtLbl.font = font
                            let color = dict["color"] as! UIColor
                            txtLbl.textColor = color
                            // 30.0 added to give padding to text
                            txtLbl.bounds.size.width = dict["textViewWidth"] as! CGFloat + 30.0
                            txtLbl.bounds.size.height = dict["textViewHeight"] as! CGFloat + 30.0
                            if txtLbl.calculateMaxLines() == 1 {
                                txtLbl.textAlignment = .center
                                txtLbl.bounds.size.width = txtLbl.intrinsicContentSize.width + 100.0
                            }else{
                                txtLbl.textAlignment = .left
                            }
                            txtLbl.fontIndex = dict["selectedFontIndex"] as! Int
                            txtLbl.colorIndex = dict["selectedColorIndex"] as! Int
                        }
                    }
                }
            }
        }
        
    }
    
    // MARK: - Logical Methods
    
    func showTextGradientView()
    {
        self.btnTextDelete.isHidden = false
    }
    
    func hideTextGradientView()
    {
        self.btnTextDelete.isHidden = true
    }
    
    func removeAllTextsFromView()
    {
        let subViews = self.viewTextStickerCombined.subviews
        for subview in subViews
        {
            if subview is TextCustomView
            {
                if let view = subview as? TextCustomView
                {
                    view.removeFromSuperview()
                }
            }
        }
    }
    
    func enableDisableGesturesOnTextView(isEnable: Bool)
    {
        let subViews = self.viewTextStickerCombined.subviews
        for subview in subViews
        {
            if subview is TextCustomView
            {
                if let view = subview as? TextCustomView
                {
                    view.isUserInteractionEnabled = isEnable
                }
            }
        }
    }
    
    func resetTextView()
    {
        self.viewText.isHidden = true
        self.btnTextDelete.isHidden = true
    }
}
