//
//  TextViewController.swift
//  TextDemo
//
//  Created by maximess142 on 20/12/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit

class TextViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var txtViewTextToEnter: UITextView!
    @IBOutlet weak var collectionViewColor: UICollectionView!
    @IBOutlet weak var collectionViewText: UICollectionView!
    @IBOutlet weak var viewFontContainer: UIView!
    @IBOutlet weak var viewColorContainer: UIView!
    @IBOutlet weak var btnFontColorSelector: UIButton!
    @IBOutlet weak var btnColor: UIButton!
    @IBOutlet weak var btnText: UIButton!
     @IBOutlet weak var btnDone: UIButton!
     @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var collectionviewLeading: NSLayoutConstraint!
    @IBOutlet weak var collectionviewTrailing: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomContainerView: NSLayoutConstraint!
    @IBOutlet weak var constraintCollectionViewBottom: NSLayoutConstraint!

    //MARK:- Variables
    var viewWidth = Float()
    //    var conainerView: UIView!
    var paraTextObj = Para_TextClass()
    let colorsArray = [UIColor.white,
                       UIColor().hexStringToUIColor(hex: "F6F6A7"),
                       UIColor().hexStringToUIColor(hex: "FFFF2D"),
                       UIColor().hexStringToUIColor(hex: "f3c843"),
                       UIColor().hexStringToUIColor(hex: "f09235"),
                       UIColor().hexStringToUIColor(hex: "db5034"),
                       UIColor().hexStringToUIColor(hex: "db1b1b"),
                       UIColor().hexStringToUIColor(hex: "eb3498"),
                       UIColor().hexStringToUIColor(hex: "f988c6"),
                       UIColor().hexStringToUIColor(hex: "b86ad3"),
                       UIColor().hexStringToUIColor(hex: "7f3499"),
                       UIColor().hexStringToUIColor(hex: "353dc7"),
                       UIColor().hexStringToUIColor(hex: "1c25b3"),
                       UIColor().hexStringToUIColor(hex: "479fca"),
                       UIColor().hexStringToUIColor(hex: "11c6c3"),
                       UIColor().hexStringToUIColor(hex: "1d9952"),
                       UIColor().hexStringToUIColor(hex: "53b77e"),
                       UIColor().hexStringToUIColor(hex: "b8cb40"),
                       UIColor().hexStringToUIColor(hex: "ada9b4"),
                       UIColor().hexStringToUIColor(hex: "25242c"),
                       UIColor().hexStringToUIColor(hex: "000000")]
    let fontsArray = ["BlackJack", "FFF Tusj", "Graphik", "Helvetica", "Museo","Nexa Light", "NotoSans-Bold", "NotoSans", "OpenSans-Light","OpenSans","Sofia", "Avenir"]
    var editText = ""
    var textViewToEdit : TextCustomView?
    var fontSizeClickPreview = CGFloat(24.0)
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        self.viewFontContainer.frame.size.width = CGFloat(self.viewWidth)
        //        self.viewColorContainer.frame.size.width = CGFloat(self.viewWidth)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Set Up Functions
    func initialSetup() {
        self.btnDone.setTitle( "Done".localisedString(), for: .normal)
        self.btnCancel.setTitle( "Cancel".localisedString(), for: .normal)
        self.btnText.isSelected = true
        self.addBlurBackgroundEffect()
        UITextView.appearance().tintColor = self.colorsArray[paraTextObj.selectedColorIndex]
        txtViewTextToEnter.becomeFirstResponder()
        changeDoneButnColorOfKeypad()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        let color = self.colorsArray[paraTextObj.selectedColorIndex]
        self.txtViewTextToEnter.text = editText
        self.txtViewTextToEnter.textColor = color
        let fontName = self.fontsArray[paraTextObj.selectedFontIndex]
        self.txtViewTextToEnter.font = UIFont.init(name: fontName, size: fontSizeClickPreview)
        self.txtViewTextToEnter.tintColor = color
        self.txtViewTextToEnter.text = self.paraTextObj.textString
    }
    
    func addBlurBackgroundEffect()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.alpha = 0.4
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.insertSubview(blurEffectView, at: 0)
    }
    
    func changeDoneButnColorOfKeypad()
    {
        //KeyboardCustomisations().addColorToUIKeyboardButton()
    }
    
    @objc func keyboardWillShow(_ notification: Notification)
    {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            self.constraintBottomContainerView.constant = keyboardHeight + 10
            //            let keyboardRectangle = keyboardFrame.cgRectValue
            self.addViewOnKeypad(frame: keyboardFrame.cgRectValue)
        }
    }
    
    func addViewOnKeypad(frame:CGRect)
    {
        let spacingFromKeyboard : CGFloat = 10.0
        if getCurrentIphone() == "5"
        {
            self.constraintCollectionViewBottom.constant = (self.view.frame.size.height - frame.origin.y) + 2 + spacingFromKeyboard
        }
        else if getCurrentIphone() == "X"{
            self.constraintCollectionViewBottom.constant = (self.view.frame.size.height - frame.origin.y) - 35 + spacingFromKeyboard
        }
        else{
            self.constraintCollectionViewBottom.constant = (self.view.frame.size.height - frame.origin.y) + spacingFromKeyboard
        }
    }
    
    //Remove Self view from superview.
    func removeFromSuperView()
    {
        // self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    func addLabelOnSuperView()
    {
        let dict = ["color" : colorsArray[paraTextObj.selectedColorIndex],
                    "font" : txtViewTextToEnter.font,
                    "text" : txtViewTextToEnter.text,
                    "selectedColorIndex" : paraTextObj.selectedColorIndex,
                    "selectedFontIndex" : paraTextObj.selectedFontIndex,
                    "textViewXAxis" : txtViewTextToEnter.frame.origin.x,
                    "textViewYAxis" : txtViewTextToEnter.frame.origin.y,
                    "textViewWidth" : txtViewTextToEnter.bounds.width,
                    "textViewHeight" : txtViewTextToEnter.bounds.height]
            as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "addTextLblToView"), object: dict)
    }
    
    func replaceLabelOnSuperView()
    {
        print("selectedColorIndex", paraTextObj.selectedColorIndex)
        let dict = ["color" : colorsArray[paraTextObj.selectedColorIndex],
                    "font" : txtViewTextToEnter.font,
                    "text" : txtViewTextToEnter.text,
                    "selectedColorIndex" : paraTextObj.selectedColorIndex,
                    "selectedFontIndex" : paraTextObj.selectedFontIndex,
                    "replaceView" : textViewToEdit ?? "",
                    "textViewXAxis" : txtViewTextToEnter.frame.origin.x,
                    "textViewYAxis" : txtViewTextToEnter.frame.origin.y,
                    "textViewWidth" : txtViewTextToEnter.bounds.width,
                    "textViewHeight" : txtViewTextToEnter.bounds.height]
            as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "replaceTextLblToView"), object: dict)
    }
    
    // MARK: - IBActions
    
    @IBAction func btnBackClicked(_ sender: Any)
    {
        removeFromSuperView()
    }
    
    @IBAction func btnDoneClicked(_ sender: Any)
    {
        if txtViewTextToEnter.text == "" || txtViewTextToEnter.text == " "
        {
//            self.showAlert(title: "Done", message: "")
            removeFromSuperView()
        }
        else
        {
            if textViewToEdit?.text != "" && textViewToEdit?.text != nil
            {
                self.replaceLabelOnSuperView()
            }
            else
            {
                self.addLabelOnSuperView()
            }
            removeFromSuperView()
        }
    }
    
    @IBAction func btnFontClicked(_ sender: UIButton)
    {
        self.setupViews(isFont: true)
    }
    
    @IBAction func btnColorClicked(_ sender: Any)
    {
        self.setupViews(isFont: false)
    }

    //MARK:- Functions
    func setupViews(isFont: Bool) {
        self.viewFontContainer.isHidden = isFont == true ? false : true
        self.viewColorContainer.isHidden = isFont == false ? false : true
        self.btnText.backgroundColor = isFont == true ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0.9951658845, green: 0.7617972493, blue: 0.08426073939, alpha: 1)
        self.btnColor.backgroundColor = isFont == true ? #colorLiteral(red: 0.9951658845, green: 0.7617972493, blue: 0.08426073939, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.btnText.isSelected = isFont == false ? false : true
        self.btnColor.isSelected = isFont == true ? false : true
        self.btnFontColorSelector.isSelected = isFont == true ? false : true
    }
}
