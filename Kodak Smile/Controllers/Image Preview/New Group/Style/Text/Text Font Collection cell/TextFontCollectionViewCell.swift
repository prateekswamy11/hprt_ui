//
//  TextFontCollectionViewCell.swift
//  TextDemo
//
//  Created by maximess142 on 21/12/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit

class TextFontCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblFontPreview: UILabel!
    
    @IBOutlet weak var lblFontPreviewSelected: UILabel!
}
