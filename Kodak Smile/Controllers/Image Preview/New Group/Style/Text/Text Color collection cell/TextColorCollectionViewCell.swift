//
//  TextColorCollectionViewCell.swift
//  TextDemo
//
//  Created by maximess142 on 21/12/17.
//  Copyright © 2017 maximess142. All rights reserved.
//
import UIKit

class TextColorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewColor: UIView!
    @IBOutlet weak var viewBorder: UIView!
    
    @IBOutlet weak var viewColorSelected: UIView!
}
