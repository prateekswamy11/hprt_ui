//
//  TextViewController+TextViewDelegate.swift
//  TextDemo
//
//  Created by maximess142 on 20/12/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit

extension TextViewController : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let size = CGSize(width: textView.frame.width, height: .infinity)
        let estimatedSize = textView.sizeThatFits(size)
        if estimatedSize.height > viewColorContainer.frame.origin.y - 28.0{
            textView.font = UIFont(name: textView.font!.fontName, size: textView.font!.pointSize - 1)
            self.fontSizeClickPreview = textView.font!.pointSize
        }else{
            if text == "" && range.length > 0 && textView.font!.pointSize < CGFloat(24.0){
                textView.font = UIFont(name: textView.font!.fontName, size: textView.font!.pointSize + 0.1)
                self.fontSizeClickPreview = textView.font!.pointSize
            }
        }
        if range.lowerBound == 0 {
            textView.font = UIFont(name: textView.font!.fontName, size: 24.0)
            self.fontSizeClickPreview = textView.font!.pointSize
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.txtViewTextToEnter.resignFirstResponder()
    }
}
