//
//  TextViewControllerCollectionViewDatasource.swift
//  TextDemo
//
//  Created by maximess142 on 21/12/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit

extension TextViewController : UICollectionViewDataSource, UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == collectionViewColor
        {
            return colorsArray.count
        }
        else if collectionView == collectionViewText
        {
            return fontsArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == collectionViewColor
        {
            let cell : TextColorCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextColorCollectionViewCell", for: indexPath) as! TextColorCollectionViewCell
            cell.viewColor.backgroundColor = colorsArray[indexPath.row]
            cell.viewBorder.layer.borderColor = colorsArray[indexPath.row].cgColor
            //            cell.viewColorSelected.backgroundColor = colorsArray[indexPath.row]
            if paraTextObj.selectedColorIndex == indexPath.row
            {
                cell.viewBorder.isHidden = false
                //                cell.viewColorSelected.isHidden = false
                cell.viewColor.isHidden = false
            }
            else
            {
                cell.viewBorder.isHidden = true
                //                cell.viewColorSelected.isHidden = true
                cell.viewColor.isHidden = false
            }
            
            return cell
        }
        else //if collectionView == collectionViewText
        {
            let cell : TextFontCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextFontCollectionViewCell", for: indexPath) as! TextFontCollectionViewCell
            cell.lblFontPreview.font = UIFont.init(name: fontsArray[indexPath.item], size: 20)
            cell.lblFontPreviewSelected.font = UIFont.init(name: fontsArray[indexPath.item], size: 30)
            cell.lblFontPreviewSelected.text = "Aa".localisedString()
            cell.lblFontPreview.text = "Aa".localisedString()
            if paraTextObj.selectedFontIndex == indexPath.item
            {
                cell.lblFontPreviewSelected.isHidden = false
                cell.lblFontPreview.isHidden = true
                cell.borderWidth = 2
                cell.borderColor = UIColor.white
            }
            else
            {
                cell.borderWidth = 0
                cell.lblFontPreviewSelected.isHidden = true
                cell.lblFontPreview.isHidden = false
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == collectionViewColor
        {
            paraTextObj.selectedColorIndex = indexPath.item
            let color = colorsArray[paraTextObj.selectedColorIndex]
            self.txtViewTextToEnter.textColor = color
            self.txtViewTextToEnter.tintColor = color
            collectionView.reloadData()
        }
        else
        {
            paraTextObj.selectedFontIndex = indexPath.item
            self.txtViewTextToEnter.font = UIFont.init(name: fontsArray[paraTextObj.selectedFontIndex], size: self.fontSizeClickPreview)
            collectionView.reloadData()
        }
    }
}
