//
//  TextCustomView.swift
//  TextDemo
//
//  Created by maximess142 on 22/12/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit

class TextCustomView: UILabel, UIGestureRecognizerDelegate
{
    var fontIndex = 0
    var colorIndex = 0
    
    enum selectionStates{
        case nonSelected
        case selected
    }
    
    var selectionState = selectionStates.nonSelected
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        initCommon()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        initCommon()
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 20.0 , left: 20.0, bottom: 20.0, right: 20.0)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    func initCommon()
    {
        ///Akshay
        self.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        ////
        self.isUserInteractionEnabled = true
        self.contentMode = .scaleAspectFit
        self.numberOfLines = 0
        self.lineBreakMode = .byWordWrapping
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = 5.0
        
        /**
         UIRotationGestureRecognizer - Rotating Objects
         */
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(handleRotate(gestureRecognizer:)))
        rotate.delegate = self
        self.addGestureRecognizer(rotate)
        
        /**
         UIPanGestureRecognizer - Moving Objects
         Selecting transparent parts of the imageview won't move the object
         */
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        pan.delegate = self
        self.addGestureRecognizer(pan)
        
        /**
         UIPinchGestureRecognizer - Pinching Objects
         If it's a UITextView will make the font bigger so it doen't look pixlated
         */
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchRecognized(pinch:)))
        pinchGesture.delegate = self
        self.addGestureRecognizer(pinchGesture)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapRecognised(tap:)))
        tap.numberOfTapsRequired = 1
        tap.require(toFail: pan)
        self.addGestureRecognizer(tap)
    }
    
    // MARK: - Action Methods
    func applyBorderToTextView()
    {
        if let parent = self.parentViewController as? ImagePreviewViewController
        {
            // Remove all borders from sticker view
            parent.removeBordersFromAllSubViews()
            parent.btnTextDelete.isHidden = false
        }
        
        self.borderWidth = 2.0
        self.selectionState = .selected
    }
    
    @objc func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        let translation = gestureRecognizer.translation(in: self.parentViewController?.view)
        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x + translation.x, y: gestureRecognizer.view!.center.y + translation.y)
        gestureRecognizer.setTranslation(CGPoint.zero, in: self)
        self.applyBorderToTextView()
    }
    
    @objc func pinchRecognized(pinch: UIPinchGestureRecognizer) {
        pinch.view?.transform = (pinch.view?.transform.scaledBy(x: pinch.scale, y: pinch.scale))!
        pinch.scale = 1.0
        self.applyBorderToTextView()
    }
    
    @objc func handleRotate(gestureRecognizer : UIRotationGestureRecognizer) {
        guard gestureRecognizer.view != nil else { return }
        
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            gestureRecognizer.view?.transform = gestureRecognizer.view!.transform.rotated(by: gestureRecognizer.rotation)
            gestureRecognizer.rotation = 0
        }
        self.applyBorderToTextView()
    }
    
    @objc func tapRecognised(tap : UITapGestureRecognizer) {
        print("tap fetched")
        switch selectionState{
        case .nonSelected:
            self.applyBorderToTextView()
            break
        case .selected:
            if let parent = tap.view?.parentViewController as? ImagePreviewViewController {
                // Open text edit view in edit options
                parent.openTextViewFromEdit()
                parent.editTextLblOnView(textLblToEdit: self)
            }
            break
        }
    }
    
    
    //MARK:- UIGestureRecognizerDelegate Methods
    /*
     By default, once one gesture recognizer on a view “claims” the gesture, no others can recognize a gesture from that point on.
     
     So, for Simultaneous Gesture recognizers I have overridden a function in the UIGestureRecognizer delegate.
     */
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        // Edit - Pratiksha
        // This line will move current working sticker to front between all stickers
        self.superview?.bringSubview(toFront: self)
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
    func shouldRespondToGesture(_ gesture: UIGestureRecognizer, in frame: CGRect) -> Bool {
        return gesture.state == .began && frame.contains(gesture.location(in: self.superview))
    }
}

extension TextCustomView {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}
