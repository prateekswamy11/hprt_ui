//
//  StickerCustomView.swift
//  StickersDemo
//
//  Created by maximess142 on 08/12/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit

class StickerCustomView: UIImageView, UIGestureRecognizerDelegate
{
    // MARK: - Init Methods
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        initCommon()
    }
    
    func initCommon()
    {
        self.isUserInteractionEnabled = true
        self.contentMode = .scaleAspectFit
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = 5.0

        /**
         UIRotationGestureRecognizer - Rotating Objects
         */
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(handleRotate(recognizer:)))
        rotate.delegate = self
        self.addGestureRecognizer(rotate)
        
        /**
         UIPanGestureRecognizer - Moving Objects
         Selecting transparent parts of the imageview won't move the object
         */
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        pan.delegate = self
        self.addGestureRecognizer(pan)
        
        /**
         UIPinchGestureRecognizer - Pinching Objects
         If it's a UITextView will make the font bigger so it doen't look pixlated
         */
        let pinchGesture = UIPinchGestureRecognizer(target: self,
                                                    action: #selector(pinchRecognized(pinch:)))
        pinchGesture.delegate = self
        self.addGestureRecognizer(pinchGesture)
        
        // Added just for opening sticker view when clicked on any sticker
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapRecognised(tap:)))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
    }
    
    // MARK: - Logical Methods
    func applyBorderToStickerView()
    {
        if let parent = self.parentViewController as? ImagePreviewViewController
        {
            // Remove all borders from sticker view
            parent.removeBordersFromAllSubViews()
            parent.btnStickerDelete.isHidden = false
        }
        self.borderWidth = 2.0
    }

    func stickerSelectAction(gestureView:UIView)
    {
        if let parent = gestureView.parentViewController as? ImagePreviewViewController
        {
            self.applyBorderToStickerView()
            
            // Open sticker edit view in edit options
            parent.openStickersViewFromEdit()
        }
    }

    // MARK: - GestureRecogniser Methods
    @objc func handlePan(_ gestureRecognizer: UIPanGestureRecognizer)
    {
        // Allows smooth movement of stickers.
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed
        {
            self.stickerSelectAction(gestureView: gestureRecognizer.view!)
            let point = gestureRecognizer.location(in: self.superview)
            if let superview = self.superview
            {
                let restrictByPoint : CGFloat = 30.0
                let superBounds = CGRect(x: superview.bounds.origin.x + restrictByPoint, y: superview.bounds.origin.y, width: superview.bounds.size.width - 2*restrictByPoint, height: superview.bounds.size.height - restrictByPoint)
                if (superBounds.contains(point))
                {
                    let translation = gestureRecognizer.translation(in: self.superview)
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x + translation.x, y: gestureRecognizer.view!.center.y + translation.y)
                    gestureRecognizer.setTranslation(CGPoint.zero, in: self.superview)
                }
            }
        }
    }
    
    @objc func pinchRecognized(pinch: UIPinchGestureRecognizer)
    {
        if let view = pinch.view
        {
            self.stickerSelectAction(gestureView: pinch.view!)
            
            view.transform = view.transform.scaledBy(x: pinch.scale, y: pinch.scale)
            pinch.scale = 1
        }
    }
    
    @objc func handleRotate(recognizer : UIRotationGestureRecognizer) {
        if let view = recognizer.view {
            self.stickerSelectAction(gestureView: recognizer.view!)
            view.transform = view.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
        }
    }
    
    @objc func tapRecognised(tap : UITapGestureRecognizer)
    {
        self.stickerSelectAction(gestureView: tap.view!)
    }
    
    //MARK:- UIGestureRecognizerDelegate Methods
    /*
     By default, once one gesture recognizer on a view “claims” the gesture, no others can recognize a gesture from that point on.
     
     So, for Simultaneous Gesture recognizers I have overridden a function in the UIGestureRecognizer delegate.
     */
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        // Edit - Pratiksha
        // This line will move current working sticker to front between all stickers
        self.superview?.bringSubview(toFront: self)
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool
    {
        return true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initCommon()
    }
}


