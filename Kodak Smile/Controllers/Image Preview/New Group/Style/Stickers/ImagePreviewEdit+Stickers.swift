//
//  ImagePreviewEdit+Stickers.swift
//  Polaroid MINT
//
//  Created by maximess142 on 18/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import CopilotAPIAccess

extension ImagePreviewViewController
{
    // MARK: - Set up Methods
    func setUpStickersView()
    {
        self.viewStickers.isHidden = false
        self.viewTextStickerCombined.isHidden = false
        self.viewTextStickerCombined.isUserInteractionEnabled = true
        self.viewStickers.isUserInteractionEnabled = true
        setupSelectStickerView()
        self.showCancelDoneButn()
        self.enableDisableGesturesOnTextView(isEnable: false)
        self.collectionViewStickers.isScrollEnabled = false
        let indexPath = IndexPath(item: 0, section: 0)
        self.collectionViewStickers.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: false)
    }
    
    func getStickerAndFrames(){
        let opertaionQueue = OperationQueue()
        opertaionQueue.maxConcurrentOperationCount = 2
        let operationForSticker = BlockOperation {
            autoreleasepool{
                for name in self.stickersArray{
                    if self.stickerImages != nil{
                        if let sticker = UIImage(named: name){
                            self.stickerImages.append(sticker)
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.collectionViewStickers.delegate = self
                    self.collectionViewStickers.dataSource = self
                    self.collectionViewStickers.reloadData()
                }
            }
            print("stickers fetched")
            //            opertaionQueue.cancelAllOperations()
        }
        opertaionQueue.addOperation(operationForSticker)
        let operationForFrames = BlockOperation {
            autoreleasepool{
                if self.framesThumbnailListArray.count != 0{

                    DispatchQueue.main.async {
                        self.collectionViewBorders.delegate = self
                        self.collectionViewBorders.dataSource = self
                        self.collectionViewBorders.reloadData()
                    }
                }
                print("frames fetched")
                //            opertaionQueue.cancelAllOperations()
            }
        }
        opertaionQueue.addOperation(operationForFrames)
    }
    
    func openStickersViewFromEdit()
    {
        // If currently sticker view is not open, open it
        if paraEditObject.selectedSubStyleOption != 0
        {
            paraEditObject.currentEditOptionIndex = 2
            paraEditObject.selectedSubStyleOption = 0
            self.collectionViewEditMainOptions.reloadData()
            self.setUpStickersView()
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.viewStickers.center = self.trayDown
            })
        }
    }
    
    func setupSelectStickerView()
    {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.didPanTray(sender:)))
        self.viewStickers.addGestureRecognizer(pan)
        self.viewStickers.center = self.trayDown
    }
    
    // MARK: - Action Methods
    // Sticker view swipe up and down
    @objc func didPanTray(sender: UIPanGestureRecognizer) {
        let velocity = sender.velocity(in: self.view)
        let translation = sender.translation(in: self.view)
        print("translation \(translation)")
        if sender.state == UIGestureRecognizerState.began {
            trayOriginalCenter = viewStickers.center
        } else if sender.state == UIGestureRecognizerState.changed {
            if viewStickers.center.y >= self.trayUp.y && viewStickers.center.y <= self.trayDown.y
            {
                viewStickers.center = CGPoint(x: trayOriginalCenter.x, y: trayOriginalCenter.y + translation.y)
            }
        } else if sender.state == UIGestureRecognizerState.ended {
            if velocity.y > 0 {
                UIView.animate(withDuration: 0.1, animations: { () -> Void in
                    self.viewStickers.center = self.trayDown
                    self.collectionViewStickers.isScrollEnabled = false
                })
            } else {
                UIView.animate(withDuration: 0.1, animations: { () -> Void in
                    self.viewStickers.center = self.trayUp
                    self.collectionViewStickers.isScrollEnabled = true
                })
            }
        }
    }
    
    @IBAction func btnStickerDeleteClicked(_ sender: Any)
    {
        self.paraEditObject.selectedStickerView?.removeFromSuperview()
        self.btnStickerDelete.isHidden = true
    }
    
    @IBAction func sdrawButton(_ sender: AnyObject) {
        self.viewStickers.isHidden = false
        viewStickers.center.y += view.frame.height
        UIView.animate(withDuration: 0.7, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations:{
            self.viewStickers.center.y -= self.view.frame.height
        }, completion: nil)
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer)
    {
        if (sender.direction == .up) {
            UIView.animate(withDuration: 0.7, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations:{
                self.constraintStickerViewTop.constant = -200.0
            }, completion: nil)
        }
        else if (sender.direction == .down) {
            UIView.animate(withDuration: 0.7, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations:{
                self.constraintStickerViewTop.constant = 0.0
            }, completion: nil)
        }
    }
    
    @objc func draggedView(_ sender:UIPanGestureRecognizer){
        self.view.bringSubview(toFront: viewStickers)
        let translation = sender.translation(in: self.view)
        viewStickers.center = CGPoint(x: viewStickers.center.x + translation.x, y: viewStickers.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @IBAction func btnStickerTopClicked(_ sender: Any)
    {
        if (self.constraintStickerViewTop.constant == 0) {
            UIView.animate(withDuration: 0.7, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations:{
                self.constraintStickerViewTop.constant = -200.0
            }, completion: nil)
        }
        else if (self.constraintStickerViewTop.constant == -200.0) {
            UIView.animate(withDuration: 0.7, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations:{
                self.constraintStickerViewTop.constant = 0.0
            }, completion: nil)
        }
    }
    
    @objc func tapRecognised(tap : UITapGestureRecognizer)
    {
        self.removeBordersFromAllSubViews()
    }
    
    // MARK: - Logical Methods
    func removeBordersFromAllSubViews()
    {
        let subViews = self.viewTextStickerCombined.subviews
        for subview in subViews
        {
            if subview is StickerCustomView || subview is TextCustomView
            {
                if let view = subview as? StickerCustomView
                {
                    self.btnStickerDelete.isHidden = true
                    self.paraEditObject.selectedStickerView = view
                }
                if let view = subview as? TextCustomView
                {
                    self.btnTextDelete.isHidden = true
                    self.paraEditObject.selectedTextView = view
                    view.selectionState = .nonSelected
                }
                subview.layer.borderWidth = 0.0
            }
        }
    }
    
    func applyStickerToView(index: Int)
    {
        //select sticker tray down
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.viewStickers.center = self.trayDown
            
        })
        // Change these only when sticker from that category is added
        //Akshay Solve crash detect on crashanalitic
        if self.eventSticker.indices.contains(self.paraEditObject.selectedStickerIndex){
            if DBModelForEvents.shared.fetchStickerFrameDownloadStste(id: self.eventSticker[self.paraEditObject.selectedStickerIndex]["id"] as! Int, isSticker: true) != 1 && WebserviceModelClass().isInternetAvailable(){
                self.downloadImage(from: URL(string: self.eventSticker[self.paraEditObject.selectedStickerIndex]["image"] as! String)!, id: self.eventSticker[self.paraEditObject.selectedStickerIndex]["id"] as! Int, isSticker: true, index: index, completion: { (img) in
                    if let image = img{
//                        self.setSticker(image: image)
                    }else{
                        DispatchQueue.main.async {
                            self.showNoInternetToast()
                            let index = self.downloadingStickers.index(of: index)
                            self.downloadingStickers.remove(at: index!)
                            self.collectionViewStickers.reloadData()
                        }
                    }
//                    self.activityIndicator.stopAnimating()
                })
            }else if DBModelForEvents.shared.fetchStickerFrameDownloadStste(id: self.eventSticker[self.paraEditObject.selectedStickerIndex]["id"] as! Int, isSticker: true) == 1{
                self.setSticker(image: self.getSavedImage(named: DBModelForEvents.shared.fetchDownloadedStickerFrame(id: self.eventSticker[self.paraEditObject.selectedStickerIndex]["id"] as! Int, isSticker: true))!)
            }
            let stickerData = self.eventSticker[self.paraEditObject.selectedStickerIndex]
            let stickerId: Int = stickerData["id"] as! Int
            let stickerAnalyticsEvent = TapStickerAnalyticsEvent(sticker: stickerData["eventName"] as! String + "_sticker_" + String(stickerId), category: stickerData["eventName"] as! String)
            Copilot.instance.report.log(event: stickerAnalyticsEvent)
            print("copilot event hit------event")
        }else{
            guard let imgForSticker = UIImage.init(named: self.stickersArray[self.paraEditObject.selectedStickerIndex - self.eventSticker.count])
                else{
                    return
            }
            self.setSticker(image: imgForSticker)
            let id: Int = self.stickerImages.index(of: self.stickerImages[self.paraEditObject.selectedStickerIndex - self.eventSticker.count])!
            let stickerAnalyticsEvent = TapStickerAnalyticsEvent(sticker: "General_sticker_" + String(id), category: "General")
            Copilot.instance.report.log(event: stickerAnalyticsEvent)
            print("copilot event hit------event")
        }
    }
    
    func setSticker(image: UIImage){
        let x = viewTextStickerCombined.frame.midX
        let y = viewTextStickerCombined.frame.midY
        // For loading directly
        let stickerImgView = StickerCustomView(frame: CGRect(x: x - 80, y: y - 80, width: 160, height: 160))
        stickerImgView.image = image
        UIView.transition(with: self.viewTextStickerCombined,
                          duration:0.5,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.viewTextStickerCombined.addSubview(stickerImgView)
        },
                          completion: nil)
    }
    
    func removeAllStickersFromView()
    {
        let subViews = self.viewTextStickerCombined.subviews
        for subview in subViews
        {
            if subview is StickerCustomView
            {
                if let view = subview as? StickerCustomView
                {
                    view.removeFromSuperview()
                }
            }
        }
    }
    
    func enableDisableGesturesOnStickerView(isEnable: Bool)
    {
        let subViews = self.viewTextStickerCombined.subviews
        for subview in subViews
        {
            if subview is StickerCustomView
            {
                if let view = subview as? StickerCustomView
                {
                    view.isUserInteractionEnabled = isEnable
                }
            }
        }
    }
    
    func resetStickersView()
    {
        self.viewStickers.isHidden = true
        self.btnStickerDelete.isHidden = true
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL, id: Int, isSticker: Bool, index: Int, completion: @escaping (UIImage?) -> ()) {
        print("Download Started")
        getData(from: url) { data, response, error in
            print("error------------\(error)")
            if error == nil{
                guard let data = data, error == nil else { return }
                print(response?.suggestedFilename ?? url.lastPathComponent)
                print("Download Finished")
                DispatchQueue.main.async() {
                    if let image = UIImage(data: data){
                        print("Image------\(image)")
                        completion(image)
                        print(self.saveImage(image: image, name: url.pathComponents.last!, id: id, isSticker: isSticker, index: index))
                    }
                }
            }else{
                completion(nil)
            }
        }
    }
    
    func saveImage(image: UIImage, name: String, id : Int, isSticker: Bool, index: Int) -> Int {
        guard let data = UIImagePNGRepresentation(image) else {
            return 0
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return 0
        }
        do {
            try data.write(to: directory.appendingPathComponent(name)!)
            DBModelForEvents.shared.updateStickerFrameDownloadState(id: id, imgName: name, isSticker: isSticker) { (isSuccess) in
                if isSticker{
                    let index = self.downloadingStickers.index(of: index)
                    self.downloadingStickers.remove(at: index!)
                    self.collectionViewStickers.reloadData()
                }else{
                    let index = self.downloadingFrames.index(of: index)
                    self.downloadingFrames.remove(at: index!)
                    self.collectionViewBorders.reloadData()
                }
            }
            
//            self.collectionViewStickers.reloadData()
            return 1
        } catch {
            print("save event image error------\(error.localizedDescription)")
            return 0
        }
    }
    
    func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
}
