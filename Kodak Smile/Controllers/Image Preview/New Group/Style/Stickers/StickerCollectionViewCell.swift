//
//  StickerCollectionViewCell.swift
//  Polaroid MINT
//
//  Created by maximess142 on 18/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class StickerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewSticker: UIImageView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var imgIsDownloaded: UIImageView!
    
    override func prepareForReuse() {
        self.loader.isHidden = true
        self.imgIsDownloaded.isHidden = true
    }
}
