//
//  ImagePreviewEdit+Doodle.swift
//  Polaroid MINT
//
//  Created by maximess142 on 07/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation

extension ImagePreviewViewController
{
    func setUpDoodleView()
    {
        self.viewDoodle.isHidden = false

        self.viewDoodleDrawing?.isHidden = false
        self.viewDoodleDrawing?.isUserInteractionEnabled = true
        self.view.bringSubview(toFront: viewDoodleDrawing!)
        
        // Set up ACEDrawer view parameters
        viewDoodleDrawing?.lineColor = doodleColorsArray[paraEditObject.selectedDoodleColorIndex]
        viewDoodleDrawing?.lineWidth = 2.0
        viewDoodleDrawing?.delegate = self
        
        self.checkDoodleSelectedOption()
        
        self.btnCross.isHidden = true
        self.btnOptions.isHidden = true
    }
    
    func applySelectedColorToDoodle()
    {
        let color = doodleColorsArray[paraEditObject.selectedDoodleColorIndex]
        viewDoodleDrawing?.lineColor = color
    }
    
    @IBAction func btnDoodlePencilClicked(_ sender: Any)
    {
        paraEditObject.selectedDoodleOption = 0
        self.checkDoodleSelectedOption()
    }
    
    @IBAction func btnDoodleEraserClicked(_ sender: Any)
    {
        paraEditObject.selectedDoodleOption = 1
        self.checkDoodleSelectedOption()
    }
    
    @IBAction func doodleSliderValueChanged(_ sender: UISlider)
    {
        switch paraEditObject.selectedDoodleOption
        {
        // Pencil
        case 0:
            self.paraEditObject.selectedDoodlePencilWidth = sender.value
            
            self.viewDoodleDrawing?.lineWidth = CGFloat(self.paraEditObject.selectedDoodlePencilWidth)
            
        //Eraser
        case 1:
            self.paraEditObject.selectedDoodleEraserWidth = sender.value
            
            self.viewDoodleDrawing?.lineWidth = CGFloat(self.paraEditObject.selectedDoodleEraserWidth)
       
        default:
            print("Doodle option not set")
        }
    }
    
    @IBAction func btnDoodleUndoClicked(_ sender: Any)
    {
        // As we are undoing latest change, reduce the count by 1
        paraIntermediateViews.countTempDoodle -= 1
        
        // Check undo button status, until all latest changes are removed
        if paraIntermediateViews.countTempDoodle == 0
        {
            self.btnDoodleUndo.isHidden = true
        }
        else
        {
            self.btnDoodleUndo.isHidden = false
        }
        self.viewDoodleDrawing?.undoLatestStep()
    }
    
    func checkDoodleSelectedOption()
    {
        switch paraEditObject.selectedDoodleOption
        {
            // Pencil
        case 0:
            self.btnDoodlePencil.isSelected = true
            self.btnDoodleEraser.isSelected = false
            self.collectionViewDoodle.isHidden = false
            self.collectionViewDoodle.reloadData()

            self.sliderDoodle.setValue(paraEditObject.selectedDoodlePencilWidth, animated: true)
           
            // Set drawing tool to Pen
            self.viewDoodleDrawing?.drawTool = ACEDrawingToolTypePen
            self.viewDoodleDrawing?.lineWidth = CGFloat(self.paraEditObject.selectedDoodlePencilWidth)

            //Eraser
        case 1:
            self.btnDoodlePencil.isSelected = false
            self.btnDoodleEraser.isSelected = true
            self.collectionViewDoodle.isHidden = true
            self.sliderDoodle.setValue(paraEditObject.selectedDoodleEraserWidth, animated: true)
           
            // Set drawing tool to Erase
            self.viewDoodleDrawing?.drawTool = ACEDrawingToolTypeEraser
            self.viewDoodleDrawing?.lineWidth = CGFloat(self.paraEditObject.selectedDoodleEraserWidth)

        default:
            print("Doodle option not set")
        }
    }
    
    func resetDoodleView()
    {
        self.viewDoodle.isHidden = true
        self.btnDoodleUndo.isHidden = true
        self.viewDoodleDrawing?.isUserInteractionEnabled = false
        
        self.btnCross.isHidden = false
        self.btnOptions.isHidden = false
    }
    
    func hideDoodleView()
    {
        // Reset the count, these changes are now fixed and cannot be undoed.
        paraIntermediateViews.countTempDoodle = 0
        
        self.btnCross.isHidden = false
        self.btnOptions.isHidden = false
    }
    
}
