//
//  EditDoodle+ACEcustomDelegte.swift
//  Polaroid MINT
//
//  Created by maximess142 on 10/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation

extension ImagePreviewViewController : ACEDrawingViewDelegate
{
    // Custom delegates written by pratiksha
    func drawingViewWillBeginDraw() {
        print("I am drawing")
    }
    
    func drawingViewDidEndDraw()
    {
        print("ended drawing")
        
        // Increment count of current changes count
        if self.btnDoodlePencil.isSelected || paraIntermediateViews.countTempDoodle > 0 || self.btnDoodleEraser.isSelected{
            paraIntermediateViews.countTempDoodle += 1
        }
        
        // As the changes are added, show undo button.
        if paraIntermediateViews.countTempDoodle > 0 {
            self.btnDoodleUndo.isHidden = false
        } else {
            self.btnDoodleUndo.isHidden = true
        }
    }
    
}
