//
//  DoodleColorCollectionViewCell.swift
//  Polaroid MINT
//
//  Created by maximess142 on 07/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class DoodleColorCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var viewColorNormal: UIView!
    @IBOutlet weak var viewColorSelected: UIView!
    @IBOutlet weak var viewBorder: UIView!
    
    @IBOutlet weak var viewColorSelectedContainer: UIView!
}
