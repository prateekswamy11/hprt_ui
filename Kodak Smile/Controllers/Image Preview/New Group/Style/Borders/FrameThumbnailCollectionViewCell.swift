//
//  FrameThumbnailCollectionViewCell.swift
//  Snaptouch_Polaroid
//
//  Created by maximess142 on 15/12/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit

class FrameThumbnailCollectionViewCell: UICollectionViewCell {
 
    @IBOutlet weak var imgViewFrameThumbnail: UIImageView!
    
    @IBOutlet weak var imgViewImgPreview: UIImageView!
        
    //@IBOutlet weak var viewLblNoFrame: UIView!
    
    @IBOutlet weak var imgViewSelected: UIImageView!
    
    @IBOutlet weak var imgViewFrameSelected: UIImageView!
    
    @IBOutlet weak var viewBarSelected: UIView!
    
    @IBOutlet weak var lblFrameName: UILabel!
    
    @IBOutlet weak var lblFrameNameSelected: UILabel!
    
    
    @IBOutlet weak var imgViewNoFrame: UIImageView!
    
    @IBOutlet weak var viewCellNormal: UIView!
    
    @IBOutlet weak var viewCellSelected: UIView!
    
    @IBOutlet weak var viewNoFrame: UIView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var imgIsDownloaded: UIImageView!
    
    func showNoFrameCell()
    {
        self.viewCellNormal.isHidden = false
        self.viewCellSelected.isHidden = true
        
        self.viewNoFrame.isHidden = false
        self.imgViewFrameThumbnail.isHidden = true
        self.lblFrameName.text = "None".localisedString()
    }
    
    func showSelectedCell(strFilterName : String)
    {
        self.viewCellNormal.isHidden = true
        self.viewCellSelected.isHidden = false
        
        self.lblFrameNameSelected.text = strFilterName.localisedString()
    }
    
    func showNormalCell(strFilterName : String)
    {
        self.viewCellSelected.isHidden = true
        self.viewCellNormal.isHidden = false
        self.viewNoFrame.isHidden = true
        self.imgViewFrameThumbnail.isHidden = false

        self.lblFrameName.text = strFilterName.localisedString()
    }
    
    override func prepareForReuse() {
        self.loader.isHidden = true
        self.imgIsDownloaded.isHidden = true
    }
}
