//
//  ImagePreviewEdit+Borders.swift
//  Polaroid MINT
//
//  Created by maximess142 on 31/08/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import CopilotAPIAccess

extension ImagePreviewViewController
{
    func setUpFramesView()
    {
        self.disableStickerTextSubviews()
        
        self.viewBorders.isHidden = false
        self.imgViewFrame.isHidden = false
        
        self.changeGestureOnImage(pan: true, pinch: true, rotate: false)
        
        ///// save previous Rotate value
        paraEditObject.previousScaleZoomValue = paraEditObject.scaleZoomValue
        paraEditObject.previousScaleTranslationValue = paraEditObject.scaleTranslationValue
    }
    
    func applySelectedFrameToImage(index: Int)
    {
        if index != 0
        {
            if self.eventFrames.indices.contains(index - 1){
                if DBModelForEvents.shared.fetchStickerFrameDownloadStste(id: self.eventFrames[index - 1]["id"] as! Int, isSticker: false) != 1{
                    self.downloadImage(from: URL(string: self.eventFrames[index - 1]["image"] as! String)!, id: self.eventFrames[index - 1]["id"] as! Int, isSticker: false, index: index, completion: { (img) in
                        if let image = img{
//                            self.imgViewFrame.image = image
                            DispatchQueue.main.async {
                                self.collectionViewBorders.reloadData()
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.showNoInternetToast()
                                let index = self.downloadingFrames.index(of: index)
                                self.downloadingFrames.remove(at: index!)
                                self.collectionViewBorders.reloadData()
                            }
                        }
//                        self.activityIndicator.stopAnimating()
                    })
                }else{
                    self.imgViewFrame.image = self.getSavedImage(named: DBModelForEvents.shared.fetchDownloadedStickerFrame(id: self.eventFrames[index - 1]["id"] as! Int, isSticker: false))

                }
                let framesData = self.eventFrames[index - 1]
                let frameId: Int = framesData["id"] as! Int
                let frameAnalyticsEvent = TapFrameAnalyticsEvent(frame: framesData["eventName"] as! String + "_frame_" + String(frameId) , category: framesData["eventName"] as! String)
                Copilot.instance.report.log(event: frameAnalyticsEvent)
            }else{
                guard let imgForSticker = UIImage.init(named: self.framesThumbnailListArray[index - self.eventFrames.count])
                    else{
                        return
                }
                self.imgViewFrame.image = imgForSticker
                let id: Int = self.framesImages.index(of: self.framesImages[index - self.eventFrames.count])!
                let frameAnalyticsEvent = TapFrameAnalyticsEvent(frame: "General_frame_" + String(id), category: "General")
                Copilot.instance.report.log(event: frameAnalyticsEvent)
            }
 }
        else
        {
            self.imgViewFrame.image = nil
            let id: Int = 0
            let frameAnalyticsEvent = TapFrameAnalyticsEvent(frame: "General_frame_" + String(id), category: "General")
            Copilot.instance.report.log(event: frameAnalyticsEvent)
        }
    }
    
    func resetBordersView()
    {
        self.changeGestureOnImage(pan: false, pinch: false, rotate: false)
        ///// save previous Scale value
        if paraEditObject.scaleZoomValue != paraEditObject.previousScaleZoomValue || paraEditObject.scaleTranslationValue != paraEditObject.previousScaleTranslationValue{
            paraEditObject.scaleZoomValue = paraEditObject.previousScaleZoomValue
            paraEditObject.scaleTranslationValue = paraEditObject.previousScaleTranslationValue
            
            self.imageViewPreview.transform = CGAffineTransform.identity.rotated(by: CGFloat((paraEditObject.rotateValue * .pi) / 180)).scaledBy(x: paraEditObject.scaleZoomValue, y: paraEditObject.scaleZoomValue)//.translatedBy(x: paraEditObject.scaleTranslationValue.x, y: paraEditObject.scaleTranslationValue.y)
            
            self.imageViewPreview.center = paraEditObject.scaleTranslationValue
            self.enableStickerTextSubviews()
            self.collectionViewBorders.reloadData()
           
        }
    }
}
