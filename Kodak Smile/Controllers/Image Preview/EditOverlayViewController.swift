//
//  EditOverlayViewController.swift
//  Kodak Smile
//
//  Created by MacMini002 on 3/30/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

class EditOverlayViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var viewOverlayFineTune: UIView!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var lblFineTitle: UILabel!
    @IBOutlet weak var lblFineDetail: UILabel!
    @IBOutlet weak var viewOverlayPrint: UIView!
    @IBOutlet weak var btnGotIt: UIButton!
    @IBOutlet weak var lblPrintTitle: UILabel!
    @IBOutlet weak var lblPrintDetail: UILabel!
    @IBOutlet weak var constraintViewCenterWidth: NSLayoutConstraint!
    
    //MARK:- Varaibles
    var imgMainEdit = UIImage()
    var widthConstant = CGFloat()
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
        self.overlaydetectIphone5sDevice()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.showOverlay()
    }
    
    //MARK:- Actions
    @IBAction func btnOkClicked(_ sender: Any)
    {
        self.viewOverlayFineTune.removeFromSuperview()
        self.viewOverlayFineTune.alpha = 0.0
//        self.viewOverlayPrint.alpha = 1.0
        self.viewOverlayPrint.removeFromSuperview()
        removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    
    @IBAction func btnGotItClicked(_ sender: Any)
    {
        self.viewOverlayPrint.removeFromSuperview()
        removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    
    //MARK:- Functions
        func localizedStrings() {
            self.lblFineTitle.text = "Time to Fine-Tune".localisedString()
            self.lblFineDetail.text = "You can change the filter, edit, add stickers and AR elements to your photos".localisedString()
            self.btnOk.setTitle( "Ok".localisedString(), for: .normal)
            
            self.lblPrintTitle.text = "All Set to Print".localisedString()
            self.lblPrintDetail.text = "Tap the “Print” button to print your own SMILE Images".localisedString()
            self.btnGotIt.setTitle( "Got it".localisedString(), for: .normal)
        }
        
        func showOverlay() {
            constraintViewCenterWidth.constant = widthConstant
            self.viewOverlayFineTune.alpha = 1.0
            self.viewOverlayPrint.alpha = 0.0
        }
    
        func overlaydetectIphone5sDevice()
        {
            if getCurrentIphone() == "5"{
    //            constraintBtnGotItBottom.constant = 70.0
            }
        }
}
