//
//  ImagePreviewVC+MultiPrint.swift
//  Polaroid MINT
//
//  Created by MacMini001 on 13/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

extension ImagePreviewViewController
{
    func showMultiPrintSubviews()
    {
        self.viewMultiPrint.isHidden = false
    }
    
    func hideMultiPrintSubviews()
    {
        self.viewMultiPrint.isHidden = true
    }
}
