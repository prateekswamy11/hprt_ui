////
////  ImagePreviewARExtension.swift
////  Kodak Smile
////
////  Created by maximess152 on 27/01/20.
////  Copyright © 2020 maximess. All rights reserved.
////
//
//import Foundation
//import Alamofire
//
//extension ImagePreviewViewController {
//    
//    func similar_EasyAR(){
//        btnPrint.isEnabled = false
//        if WebserviceModelClass().isInternetAvailable() {
//            let rawdata = "appKey" + "\(cloud_key)" + "image" + "\(UserDefaults.standard.value(forKey:"AR_Image") ?? "")" + "timestamp" + "\(SingletonClass.sharedInstance.timestamp ?? 0)" + "\(cloud_secret)"
//            let data_raw = SingletonClass.sharedInstance.ccSha256(data: (rawdata.data(using: .utf8)!))
//            let requestParameters = ["appKey":cloud_key,"image" : UserDefaults.standard.value(forKey:"AR_Image") ?? "","timestamp": SingletonClass.sharedInstance.timestamp ?? 0,"signature": data_raw.map { String(format: "%02hhx", $0) }.joined()] as [String : Any]
//            DispatchQueue.global(qos: .background).async {
//                self.callWebAPIForAR_similar(requestParameters)
//            }
//        }else {
//            btnPrint.isEnabled = true
//            customLoader.hideActivityIndicator()
//            self.showNoInternetPopup()
//            return
//        }
//    }
//    
//    func callWebAPIForAR_similar(_ requestParameters : [String:Any])  {
//        Alamofire.request(SingletonClass.sharedInstance.upload_similar,method: .post, parameters:requestParameters, encoding: JSONEncoding.default, headers: nil).responseJSON
//            { response in
//                switch(response.result)
//                {
//                case .success(_):
//                    if response.result.value != nil
//                    {
//                        let dict = response.result.value! as? [String:Any]
//                        let dict1 = dict!["result"] as? [String:Any]
//                        let results = dict1!["results"] as? NSArray
//                        if results?.count == 0 {
//                            let image = UserDefaults.standard.value(forKey:"imgData_ToMo")
//                            let image1 = UIImage(data: image as! Data)
//                            self.uploadPhotoAndVideoToServer(image: image1!, videoData: UserDefaults.standard.value(forKey:"AR_Video")! as! Data)
//                        }else{
//                            let resultsDict = results?.firstObject as? [String:Any]
//                            SingletonClass.sharedInstance.targetId = resultsDict!["targetId"] as? String
//                            UserDefaults.standard.set(SingletonClass.sharedInstance.targetId, forKey: "targetId")
//                            let meta_d = resultsDict!["meta"] as? String
//                            UserDefaults.standard.set(meta_d, forKey: "meta_data")
//                            let dict = SingletonClass.sharedInstance.convertToDictionary(text: meta_d!)
//                            let user_id = dict!["user_id"] as? String
//                            //                            UserDefaults.standard.set(user_id, forKey: "user_id")
//                            let image = dict!["image_url"] as? String
//                            let url = URL(string:image!)
//                            if let data = try? Data(contentsOf: url!)
//                            {
//                                let image: UIImage = UIImage(data: data)!
//                                self.originalImg = image
//                            }
//                            if (UserDefaults.standard.value(forKey:"user_id") as? String == user_id){
//                                //user target image
//                                SingletonClass.sharedInstance.targetAlreadyPresent = true
//                                self.showARPopup(true)
//                            }else{
//                                self.showARPopup(false)
//                            }
//                        }
//                    }else{
//                        customLoader.hideActivityIndicator()
//                        self.btnPrint.isEnabled = true
//                        self.showErrorPopup()
//                    }
//                    break
//                case .failure(let error):
//                    customLoader.hideActivityIndicator()
//                    self.btnPrint.isEnabled = true
//                    var error = error.localizedDescription
//                    self.showErrorPopup()
//                    if error.contains("JSON could not") {
//                        error = "Could not connect to server at the moment."
//                    }
//                }
//        }
//    }
//    
//    @objc func saveImageOnPrintSuccess()
//    {
//        if  isComeFrom == "PopUpARElementAlreadyAttachedVC"{
//            self.CheckImageEdited()
//            print(paraEditObject.isImageEdited)
//            if !ischeckupdate_ARPopup && isComeFrom != "PopUpRetryPrintVC" && paraEditObject.isImageEdited{
//                self.save_ARImagesInGallery()
//            }
//        }else{
//            print("PrintSuccess")
//            //        Save image in gallery with AR logo after successful print,
//            self.saveImageInGallery()
//        }
//        //        isComeFrom = ""
//    }
//    @objc func saveImageOnRetryPrint()
//    {
//        //        Save edited image without AR logo on retry cross if print is not successfully done on flow of error popup
//        self.CheckImageEdited()
//        if !ischeckupdate_ARPopup && isComeFrom == "PopUpRetryPrintVC" && self.paraEditObject.isImageEdited{
//            self.save_ARImagesInGallery()
//        }else{
//            //        Save image with AR logo on retry cross if print is successfully done on flow of error popup
//            if finalCopiesToCheckdelete != 0{
//                saveARImageWithLogoOnErroePopup_multicopies()
//            }
//        }
//    }
//    
//    func saveARImageWithLogoOnErroePopup_multicopies(){
//        // Hide the sliders
//        self.sliderFilterOpacity.isHidden = true
//        self.lblSliderValue.isHidden = true
//        self.gradientViewLeftToRight.isHidden = true
//        
//        // Save Image in Gallery
//        var finalImage = UIImage()
//        finalImage = imgFinalCroped
//        let isImageEquale = areEqualImages(img1: imgOriginalCroped, img2: finalImage)
//        print("isImageEquale\(isImageEquale)")
//        let saveImage = saveImageInCustomAlbum()
//        saveImage.saveImageInAlbum(name: PolaroidSnaptouchEditAlbum, withImage: finalImage)
//    }
//    
//    @objc func updateNewTargetAfterDelete()
//    {
//        customLoader.showActivityIndicator(viewController: self.view)
//        let image = UserDefaults.standard.value(forKey:"imgData_ToMo")
//        let image1 = UIImage(data: image as! Data)
//        self.uploadPhotoAndVideoToServer(image: image1!, videoData: UserDefaults.standard.value(forKey:"AR_Video")! as! Data)
//        
//    }
//    @objc func enablePrintBtn()
//    {
//        btnPrint.isEnabled = true
//    }
//    func showARPopup(_ targetAlreadyPresent:Bool) {
////        customLoader.hideActivityIndicator()
////        //        Delete target image from Easy AR server to Update target
////        SingletonClass.sharedInstance.deleteTarget_EasyAR()
////      //  is_Ar_Video_Available = true
////        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpARElementAlreadyAttachedViewController") as! PopUpARElementAlreadyAttachedViewController
////        if targetAlreadyPresent == true {
////            alertPopUp.delegate = self
////        }
////        alertPopUp.view.frame = self.view.bounds
////        self.addChildViewController(alertPopUp)
////        self.view.addSubview(alertPopUp.view)
////        alertPopUp.didMove(toParentViewController: self)
//    }
//    
//    func printAfterResponseCompletion(_ status: Bool) {
//        if status{
//            // Disable print button once printing is started
//            self.btnPrint.isEnabled = false
//            //           Code to print image
//            paraEditObject.isImageEdited = false
//            self.startPrintingAnimation()
//            let printHandler = PrintImageHandler()
//            printHandler.printImageDataArray = self.initialisePrintArray()
//            printHandler.countOfCopies = self.countOfCopies
//            printHandler.isApplyForAll = true
//            printHandler.startPrinting()
//            
//        }
//        else{
//            showErrorPopup()
//        }
//    }
//    
//    func showErrorPopup()
//    {
//        customLoader.hideActivityIndicator()
//        //        Enable print button on error popup
//        btnPrint.isEnabled = true
//        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpARServerErrorViewController") as! PopUpARServerErrorViewController
//        alertPopUp.view.frame = self.view.bounds
//        self.addChildViewController(alertPopUp)
//        self.view.addSubview(alertPopUp.view)
//        alertPopUp.didMove(toParentViewController: self)
//    }
//    func showNoInternetPopup()
//    {
//        customLoader.hideActivityIndicator()
//        //        Enable print button on error popup
//        btnPrint.isEnabled = true
//        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
//        alertPopUp.view.frame = self.view.bounds
//        comeFromStrNoInternetSocialMedia = "NormalNointernet"
//        isComeFrom = "Rectangle"
//        self.addChildViewController(alertPopUp)
//        self.view.addSubview(alertPopUp.view)
//        alertPopUp.didMove(toParentViewController: self)
//    }
//    
//    func updateTarget_EasyAR(){
//        if WebserviceModelClass().isInternetAvailable() {
//            let uploadNEW_TARGET_URL = BASE_URL_SERVER_END+NEW_TARGET
//            let img_base64 = UserDefaults.standard.value(forKey:"AR_Image")!
//            let metaInStr = SingletonClass.sharedInstance.stringify(json:UserDefaults.standard.value(forKey: "meta")!)
//            let rawdata = "active" + "1" + "appKey" + "\(cloud_key)" + "image" + "\(img_base64)" + "meta" + "\(metaInStr)" + "name" + "\(LiveAppName)" + "size" + "20" + "timestamp" + "\(SingletonClass.sharedInstance.timestamp ?? 0)" + "type" + "ImageTarget" + "\(cloud_secret)"
//            let data = SingletonClass.sharedInstance.ccSha256(data: (rawdata.data(using: .utf8)!))
//            let parameters = ["active": "1","appKey":cloud_key,"image":img_base64,"meta":metaInStr,"name":"\(LiveAppName)","size":"20","timestamp":SingletonClass.sharedInstance.timestamp ?? 0,"type":"ImageTarget","signature":data.map { String(format: "%02hhx", $0) }.joined()] as [String : Any] //Optional
//            DispatchQueue.global(qos: .background).async {
//                Alamofire.request(uploadNEW_TARGET_URL,method: .post, parameters:parameters, encoding: JSONEncoding.default, headers: nil).responseJSON
//                    { response in
//                        switch(response.result)
//                        {
//                        case .success(_):
//                            if response.result.value != nil
//                            {
//                                let dict = response.result.value! as! NSDictionary
//                                let responseDict = dict as! [String:Any]
//                                let status = responseDict["statusCode"] as! Int
//                                if status == 0
//                                {
//                                    DispatchQueue.main.async {
//                                        self.printMethods()
//                                    }
//                                }else{
//                                    self.showErrorPopup()
//                                }
//                                print("dict\(dict)")
//                                let dict1 = dict["result"] as? [String:Any]
//                                
//                            }else{
//                                if WebserviceModelClass().isInternetAvailable() {
//                                    self.showErrorPopup()
//                                }else{
//                                    self.showNoInternetPopup()
//                                }
//                            }
//                            break
//                        case .failure(let error):
//                            var error = error.localizedDescription
//                            self.showErrorPopup()
//                            if error.contains("JSON could not") {
//                                error = "Could not connect to server at the moment."
//                            }
//                        }
//                }
//                
//            }
//        }else {
//            self.showNoInternetPopup()
//            return
//        }
//    }
//    
//    func printMethodsForFirstARprint(){
//        //        For Ar print after update btn click
//        if isComeFrom == "PopUpARElementAlreadyAttachedVC"{
//            // Disable print button once printing is started
//            self.btnPrint.isEnabled = false
//            //           Code to print image
//            paraEditObject.isImageEdited = false
//            printMethods()
//        }else{
//            customLoader.hideActivityIndicator()
//            //    Upload target api response is success , start printing
//            printMethods()
//        }
//    }
//    func printMethods(){
//        self.startPrintingAnimation()
//        let printHandler = PrintImageHandler()
//        printHandler.printImageDataArray = self.initialisePrintArray()
//        printHandler.countOfCopies = self.countOfCopies
//        printHandler.isApplyForAll = true
//        printHandler.startPrinting()
//    }
//    func healthCheck_EasyAR(){
//        if WebserviceModelClass().isInternetAvailable() {
//            let uploadURLAR = BASE_URL_SERVER_END+HEALTH_CHECK
//            DispatchQueue.global(qos: .background).async {
//                DispatchQueue.main.async {
//                    customLoader.showActivityIndicator(viewController: self.view)
//                }
//                Alamofire.request(uploadURLAR,method: .get, parameters:nil, encoding: JSONEncoding.default, headers: nil).responseJSON
//                    { response in
//                        switch(response.result)
//                        {
//                        case .success(_):
//                            //                        customLoader.hideActivityIndicator()
//                            if response.result.value != nil
//                            {
//                                let dict = response.result.value! as! [String:Any]
//                                SingletonClass.sharedInstance.timestamp = dict["timestamp"] as? Int
//                                self.similar_EasyAR()
//                            }else{
//                                if WebserviceModelClass().isInternetAvailable() {
//                                    self.showErrorPopup()
//                                }else{
//                                    self.showNoInternetPopup()
//                                }
//                            }
//                            break
//                            
//                        case .failure(let error):
//                            self.showErrorPopup()
//                            var error = error.localizedDescription
//                            if error.contains("JSON could not") {
//                                error = "Could not connect to server at the moment."
//                            }
//                        }
//                }
//            }
//        }else {
//            self.showNoInternetPopup()
//            return
//        }
//    }
//    
//    func uploadPhotoAndVideoToServer(image : UIImage, videoData : Data)  {
//        if WebserviceModelClass().isInternetAvailable() {
//            let uploadURL = LIVE_AR_URL_FFMPEG+UPLOAD_SUB_URL
//            //            Rotate image if video is landscape
//            var imageTosever = UIImage()
//            if video_Is_Landscape == "1"{
//                imageTosever = image.rotateImageByDegrees(270.0)
//            }else{
//                imageTosever = image
//            }
//            let imgData = UIImageJPEGRepresentation(imageTosever, 0.1)!
//            self.userID = UserDefaults.standard.value(forKey: "user_id") as! String//UIDevice.current.identifierForVendor?.uuidString ?? ""
//            let image_id = UserDefaults.standard.value(forKey:"image_id") as? String ?? ""
//            if image_id == ""{
//                SingletonClass.sharedInstance.parameters_id = ["user_id": self.userID,"Video_Is_Landscape": video_Is_Landscape] //Optional for extra parameter unique_user_id
//            }else{
//                SingletonClass.sharedInstance.parameters_id = ["user_id": self.userID,"Video_Is_Landscape": video_Is_Landscape,"_id":image_id] //Optional for extra parameter unique_user_id
//            }
//            let parameters = SingletonClass.sharedInstance.parameters_id
//            DispatchQueue.global(qos: .background).async {
//                Alamofire.upload(multipartFormData: { multipartFormData in
//                    // Upload image
//                    multipartFormData.append(imgData, withName: "photo",fileName: "imageFile.jpg", mimeType: "image/jpg")
//                    print("imgdata\(imgData)")
//                    // Upload video
//                    if getVideoExtension == "MOV"{
//                        multipartFormData.append(videoData, withName: "video", fileName: "videoFile.MOV", mimeType: "video/mov")
//                        print("videoData\(videoData)")
//                    }else{
//                        multipartFormData.append(videoData, withName: "video", fileName: "videoFile.mp4", mimeType: "video/mp4")
//                        print("videoData\(videoData)")
//                    }
//                    for (key, value) in parameters! {
//                        multipartFormData.append((value.data(using: String.Encoding.utf8)!), withName: key)
//                    }
//                    //Optional for extra parameters
//                },
//                                 
//                                 to:uploadURL)
//                { (result) in
//                    switch result {
//                    case .success(let upload, _, _):
//                        
//                        upload.uploadProgress(closure: { (progress) in
//                            print("Upload Progress: \(progress.fractionCompleted)")
//                        })
//                        
//                        upload.responseJSON { response in
//                            if response.result.value != nil
//                            {
//                                let dict = response.result.value! as! NSDictionary
//                                print("dict\(dict)")
//                                let responseDict = dict as! [String:Any]
//                                let status = responseDict["status"] as! Int
//                                if status == 200
//                                {
//                                    let dict = response.result.value! as! NSDictionary
//                                    let meta = dict["meta_data"] as? [String:Any]
//                                    UserDefaults.standard.set(meta, forKey: "meta")
//                                    let imageID = meta!["image_id"] as Any
//                                    UserDefaults.standard.set(imageID, forKey: "image_id")
//                                    //                                    Call update_target AR api
//                                    self.updateTarget_EasyAR()
//                                }
//                            }else{
//                                if WebserviceModelClass().isInternetAvailable() {
//                                    self.showErrorPopup()
//                                }else{
//                                    self.showNoInternetPopup()
//                                }
//                            }
//                        }
//                    case .failure(let encodingError):
//                        print("Error",encodingError)
//                        self.showErrorPopup()
//                    }
//                }
//            }
//        }else {
//            self.showNoInternetPopup()
//            return
//        }
//    }
//    
//    @objc func ReplaceVideoIfAbailable()
//    {
//        if SelectVideoVC != nil {
//            SelectVideoVC = nil
//        }
//        let SelectVideoVC = storyboards.arStoryboard.instantiateViewController(withIdentifier: "SelectVideoViewController") as? SelectVideoViewController
//        
//        let videoAssets = getVideoAssetsAlbumWise()
//        if videoAssets?.count ?? 0 > 0
//        {
//            SelectVideoVC!.imagesArray = videoAssets
//            self.navigationController?.pushViewController(SelectVideoVC!, animated: true)
//        }
//        else
//        {
//            self.view.makeToast("There are no videos on your device".localisedString(), duration: 2.0, position: .bottom)
//        }
//    }
//    
//    func checkARAttachedBeforePrinting(){
//        // Hide the sliders
//        self.sliderFilterOpacity.isHidden = true
//        self.lblSliderValue.isHidden = true
//        self.gradientViewLeftToRight.isHidden = true
//        //        Check print copies to call delete api on print error popups
//        finalCopiesToCheckdelete = 0
//        //      logic to check and save image on paper jam --> retry print --> crossbtn click
//        self.viewRemoveBtnAr.isHidden = true
//        self.viewArVideoAttachedIcon.isHidden = true
//        self.imgFinalCroped = self.captureImageInView(inView: self.viewFixPaperSizeBackground)!
//        if is_Ar_Video_Available{
//            if WebserviceModelClass().isInternetAvailable() {
//                customLoader.hideActivityIndicator()
//                self.viewArVideoAttachedIcon.isHidden = false
//                //                if isComeFrom == "PopUpRetryPrintVC"{
//                self.imgFinalCroped = self.captureImageInView(inView: self.viewFixPaperSizeBackground)!
//                //                }
//                self.viewRemoveBtnAr.isHidden = false
//            }else {
//                customLoader.hideActivityIndicator()
//                let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
//                alertPopUp.view.frame = self.view.bounds
//                isComeFrom = "Rectangle"
//                self.viewRemoveBtnAr.isHidden = false
//                self.viewArVideoAttachedIcon.isHidden = false
//                comeFromStrNoInternetSocialMedia = "NormalNointernet"
//                self.addChildViewController(alertPopUp)
//                self.view.addSubview(alertPopUp.view)
//                alertPopUp.didMove(toParentViewController: self)
//                return
//            }
//        }
//    }
//}
