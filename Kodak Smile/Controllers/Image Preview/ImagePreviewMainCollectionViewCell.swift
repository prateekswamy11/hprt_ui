//
//  ImagePreviewMainCollectionViewCell.swift
//  Polaroid MINT
//
//  Created by maximess142 on 22/08/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class ImagePreviewMainCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblEditOptionName: UILabel!
    @IBOutlet weak var viewMainOptionSelected: UIView!
}
