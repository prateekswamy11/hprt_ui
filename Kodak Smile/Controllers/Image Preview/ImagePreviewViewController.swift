//
//  ImagePreviewViewController.swift
//  Polaroid MINT
//
//  Created by maximess142 on 25/07/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import HSCenterSlider
import HSRange
import StoreKit
import Alamofire
import CopilotAPIAccess

// Delegate to save edited image back to multi print screen
protocol MultiPrintEditImageSaveDelegate{
    func getSelectedImageEditedBack(_ image:UIImage)
}
var imgMainEdit = UIImage()
var isImageSavingonPaperJam = false
var isComeFrom = ""
class ImagePreviewViewController: UIViewController, PrinterStatusDelegate, ResponceDelegate,UIScrollViewDelegate {
    
    //MARK:- Outlets
    
    @IBOutlet weak var btnSmileAspectRatio: UIButton!
    @IBOutlet weak var btnClassicAspectRatio: UIButton!
    @IBOutlet weak var constraintBottomViewFixSizePaperBackground: NSLayoutConstraint!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var aspectRatio: NSLayoutConstraint!
    @IBOutlet weak var imageARLogo: UIButton!
    @IBOutlet weak var viewRemoveBtnAr: UIView!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var viewArVideoAttachedIcon: UIView!
    @IBOutlet weak var viewFixPaperSizeBackground: UIView!
    @IBOutlet weak var imageViewPreview: UIImageView!
    @IBOutlet weak var viewGradient: UIView!
    @IBOutlet weak var viewImageContainer: UIView!
    @IBOutlet weak var viewPrintGradient: UIView!
    @IBOutlet weak var btnPrint: UIButton!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var btnOptions: UIButton!
    @IBOutlet weak var viewPrintingGIF: UIView!
    @IBOutlet weak var imgViewPrintingGIF: UIImageView!
    @IBOutlet weak var lblPrint: UILabel!
    @IBOutlet weak var viewSendToPrinter: UIView!
    @IBOutlet weak var lblSendingToPrinter: UILabel!
    @IBOutlet weak var lblSwipeDown: UILabel!
    @IBOutlet weak var imgPrintStatus: UIImageView!
    @IBOutlet weak var viewEdit: UIView!
    @IBOutlet weak var lblPrintingLabel: UILabel!
    @IBOutlet weak var gradientViewLeftToRight: leftToRightGradientView!
    @IBOutlet weak var sliderFilterOpacity: UISlider!
    @IBOutlet weak var lblSliderValue: UILabel!
    @IBOutlet weak var viewSwipeDownOverlay: UIView!
    @IBOutlet weak var lblAdjustBrightness: UILabel!
    @IBOutlet weak var lblAdjustContrast: UILabel!
    @IBOutlet weak var lblAdjustSaturation: UILabel!
    @IBOutlet weak var lblAdjustWarm: UILabel!
    @IBOutlet weak var lblAdjustHighlights: UILabel!
    @IBOutlet weak var lblAdjustShadows: UILabel!
    @IBOutlet weak var lblRotate: UILabel!
    @IBOutlet weak var lblPinchtoZoom: UILabel!
    @IBOutlet weak var btnAddText: UIButton!
    @IBOutlet weak var lblNoInternetToast: UILabel!
    @IBOutlet weak var lblBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var constraintTopViewFixPaperSizeBG: NSLayoutConstraint!
    @IBOutlet weak var stickerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewForZooming: UIScrollView!
    //@IBOutlet weak var lblPrinting: UILabel!
    @IBOutlet weak var viewMultiPrint: UIView!
    @IBOutlet weak var btnSaveMultiPrint: UIButton!
    
    // MARK:- Edit
    @IBOutlet weak var viewSubEditContainer: UIView!
    @IBOutlet weak var collectionViewEditSubOptions: UICollectionView!
    @IBOutlet weak var viewCancelDone: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    
    // Scale
    @IBOutlet weak var viewGrid: UIView!
    @IBOutlet weak var viewScale: UIView!
    
    // Brightness
    @IBOutlet weak var viewBrightness: UIView!
    @IBOutlet weak var sliderBrightness: UISlider!
    @IBOutlet weak var viewBrightnessSlider: HSCenterSlider!
    
    // Warm
    @IBOutlet weak var viewWarm: UIView!
    @IBOutlet weak var sliderWarm: UISlider!
    @IBOutlet weak var viewWarmSlider: HSCenterSlider!
    
    // Contrast
    @IBOutlet weak var viewContrast: UIView!
    @IBOutlet weak var viewContrastSlider: HSCenterSlider!
    
    // Saturation
    @IBOutlet weak var viewSaturation: UIView!
    @IBOutlet weak var viewSaturationSlider: HSCenterSlider!
    
    // Rotate
    @IBOutlet weak var viewRotate: UIView!
    @IBOutlet weak var viewRotateSlider: HSCenterSlider!
    
    // Highlights
    @IBOutlet weak var viewHighlights: UIView!
    @IBOutlet weak var viewHighlightsSlider: HSCenterSlider!
    
    // Shadows
    @IBOutlet weak var viewShadows: UIView!
    @IBOutlet weak var viewShadowsSlider: HSCenterSlider!
    
    // MARK:- Style
    @IBOutlet weak var viewTextStickerCombined: UIView!
    @IBOutlet weak var viewSubStyleContainer: UIView!
    @IBOutlet weak var collectionViewStyleSubOptions: UICollectionView!
    
    // Doodle
    @IBOutlet weak var viewDoodleDrawing = ACEDrawingView()
    @IBOutlet weak var viewDoodle: UIView!
    @IBOutlet weak var collectionViewDoodle: UICollectionView!
    @IBOutlet weak var btnDoodlePencil: UIButton!
    @IBOutlet weak var btnDoodleEraser: UIButton!
    @IBOutlet weak var sliderDoodle: UISlider!
    @IBOutlet weak var btnDoodleUndo: UIButton!
    
    // Stickers
    @IBOutlet weak var viewStickers: UIView!
    @IBOutlet weak var collectionViewStickers: UICollectionView!
    @IBOutlet weak var viewStickerDrawing: UIView!
    @IBOutlet weak var viewStickerGesture: UIView!
    @IBOutlet weak var btnStickerDelete: UIButton!
    @IBOutlet weak var constraintStickerViewTop: NSLayoutConstraint!
    //    var stickersArray = [#imageLiteral(resourceName: "hat.png"), #imageLiteral(resourceName: "hat-cap.png"), #imageLiteral(resourceName: "hat-party.png"), #imageLiteral(resourceName: "hat-sheriff.png"), #imageLiteral(resourceName: "hat-cylinder.png"), #imageLiteral(resourceName: "hair_9.png"), #imageLiteral(resourceName: "hair_8.png")]
    
    // Text
    @IBOutlet weak var viewTextDrawing: UIView!
    @IBOutlet weak var btnTextDelete: UIButton!
    @IBOutlet weak var viewText: UIView!
    
    // Borders
    @IBOutlet weak var viewBorders: UIView!
    @IBOutlet weak var collectionViewBorders: UICollectionView!
    @IBOutlet weak var imgViewFrame: UIImageView!
    @IBOutlet weak var viewSubARContainer: UIView!
    @IBOutlet weak var lblAddARElement: UILabel!
    @IBOutlet weak var lblFaceFilter: UILabel!
    @IBOutlet weak var lblMedia: UILabel!
    
    // MARK: - Edit Collection view
    @IBOutlet weak var collectionViewEditMainOptions: UICollectionView!
    @IBOutlet weak var viewFilterContainer: UIView!
    @IBOutlet weak var collectionViewFilter: UICollectionView!
    
    
    //MARK:- Variables
    var isCurrentImagePrinting = false
    var gradientLayer: CAGradientLayer!
    var imgOriginal = UIImage()
    var imgOriginalCroped = UIImage()
    var imgFinalCroped = UIImage()
    var imgFinalCropedWithoutLogo = UIImage()
    var arrayPrintTypeCodes = [PrintTypeCode]()
    let delegate = UIApplication.shared.delegate as! AppDelegate
    //var timer = Timer()
    var lastScale = CGFloat()
    var collectionViewArray = [UICollectionView]()
    var viewWidth = Float()
    var labelPositionisDown = true
    var size:CGFloat = 160
    var editedImageExceptCurrentEffect = UIImage()
    var dictPrinterData:NSMutableDictionary?
    var batteryPer = Int()
    var userID :String = ""
    var lutData64 = Data()
    var selectedEffectFilter : EffectFilter?
    var delegateMultiPrint: MultiPrintEditImageSaveDelegate?
    var isFilterSelected = false
    var countOfCopies = 1 // Set by default one copy
    var selectedDoodle = Int()
    var printTextArray = ["Beautiful! That’s a great one.",
                          "This one is worth a million words",
                          "It's even better in print",
                          "Snap big, print small",
                          "So fetch!",
                          "Pics or it didn't happen",
                          "Beautiful photo, the lighting is perfect!",
                          "Nice photo, love the composition",
                          "What a Masterpiece!",
                          "You really captured that moment!"]
    var shouldAddObeservers = true
    var is_Printing = false
    //var newZoomScaleA : CGFloat = 1.0
    var nativeFilter: CIFilter!;
    var printBtnClickCount = 0
    var originalImg = UIImage()
    let paraEditObject = ParaClassEditImage()
    var reachability : Reachability!
    var networkStatus : Reachability.NetworkStatus!
    // MARK: - Multi Print Screen Variables -
    var isComeFromMultiPrintScreen = false
    // MARK:- checkZoomScale
    var checkZoomScale = CGFloat()
    var checkZoomScaleOnFilterAndRotate = CGFloat()
    var saveValueOfZoomOnDone = CGFloat()
    let enumSubEditOptions = EditSubOptions(rawValue: 0)
    var editSubOptionsNameArray = ["Fit", "Rotate", "Brightness", "Contrast", "Warmth", "Saturation", "Highlights", "Shadows"]
    var editSubOptionsIconArray = [#imageLiteral(resourceName: "fit"), #imageLiteral(resourceName: "rotate"), #imageLiteral(resourceName: "brightness"), #imageLiteral(resourceName: "contrast"), #imageLiteral(resourceName: "warm"), #imageLiteral(resourceName: "saturation"), #imageLiteral(resourceName: "highlights"), #imageLiteral(resourceName: "shadows")]
    var aCIImage = CIImage()
    var brightnessFilter: CIFilter!
    var context = CIContext()
    var newUIImage = UIImage()
    var temperatureFilter: CIFilter!;
    var contrastFilter: CIFilter!;
    var saturationFilter: CIFilter!;
    var styleSubOptionsNameArray = ["Stickers", "Text", "Doodle", "Borders"]
    var styleSubOptionsIconArray = [#imageLiteral(resourceName: "stikers"), #imageLiteral(resourceName: "text"), #imageLiteral(resourceName: "doodle"), #imageLiteral(resourceName: "borders")]
    let doodleColorsArray = [UIColor.white,
                             UIColor().hexStringToUIColor(hex: "F6F6A7"),
                             UIColor().hexStringToUIColor(hex: "FFFF2D"),
                             UIColor().hexStringToUIColor(hex: "f3c843"),
                             UIColor().hexStringToUIColor(hex: "f09235"),
                             UIColor().hexStringToUIColor(hex: "db5034"),
                             UIColor().hexStringToUIColor(hex: "db1b1b"),
                             UIColor().hexStringToUIColor(hex: "eb3498"),
                             UIColor().hexStringToUIColor(hex: "f988c6"),
                             UIColor().hexStringToUIColor(hex: "b86ad3"),
                             UIColor().hexStringToUIColor(hex: "7f3499"),
                             UIColor().hexStringToUIColor(hex: "353dc7"),
                             UIColor().hexStringToUIColor(hex: "1c25b3"),
                             UIColor().hexStringToUIColor(hex: "479fca"),
                             UIColor().hexStringToUIColor(hex: "11c6c3"),
                             UIColor().hexStringToUIColor(hex: "1d9952"),
                             UIColor().hexStringToUIColor(hex: "53b77e"),
                             UIColor().hexStringToUIColor(hex: "b8cb40"),
                             UIColor().hexStringToUIColor(hex: "ada9b4"),
                             UIColor().hexStringToUIColor(hex: "25242c"),
                             UIColor().hexStringToUIColor(hex: "000000")]
    var imgToRotate = UIImage()
    var trayOriginalCenter: CGPoint!
    var trayDownOffset: CGFloat!
    var trayUp: CGPoint!
    var trayDown: CGPoint!
    var stickersArray = ["heart_160", "s100_160", "s121_160", "s130_160", "s131_160", "s144_160", "s15_160", "s182_160", "s1_160", "s20_160", "s60_160", "s72_160", "s73_160", "glasses_nerd", "glasses_normal","glasses_shutter_green","glasses_shutter_yellow","glasses_sun","glasses","hair","hair_2","hair_3","hair_4","hair_5","hair_6","hair_7","hair_8","hair_9","hat-cap","hat-cylinder","hat-party","hat-sheriff","hat","mustache-long","mustache1","mustache2","mustache3","s3_160", "s29_160","s30_160","s33_160","s35_160","s54_160","Baseball_NoBorder_1024","BaseballBall_NoBorder_1024","Football_NoBorder_1024","Lacrosse_NoBorder_1024","Skating_NoBorder_1024","Soccer_NoBorder_1024","Swimming_NoBorder_1024","Tennis_NoBorder_1024","TennisBall_NoBorder_1024","Volleyball_NoBorder_1024","SummerEmoji_01_1024","SummerEmoji_02_1024","SummerEmoji_03_1024","SummerEmoji_04_1024","SummerEmoji_05_1024","SummerEmoji_06_1024","SummerEmoji_07_1024","SummerEmoji_08_1024","SummerEmoji_09_1024","SummerEmoji_10_1024","s142_160","s42_160","s43_160","s44_160","s45_160","s47_160","s50_160","s53_160"
    ]
    var eventSticker: [[String: Any]] = []
    var eventFrames: [[String: Any]] = []
    var stickerImages : [UIImage] = [#imageLiteral(resourceName: "heart_160"),#imageLiteral(resourceName: "s100_160"),#imageLiteral(resourceName: "s121_160"),#imageLiteral(resourceName: "s130_160"),#imageLiteral(resourceName: "s131_160"),#imageLiteral(resourceName: "s144_160"),#imageLiteral(resourceName: "s15_160"),#imageLiteral(resourceName: "s182_160"),#imageLiteral(resourceName: "s1_160"),#imageLiteral(resourceName: "s20_160"),#imageLiteral(resourceName: "s60_160"),#imageLiteral(resourceName: "s72_160"),#imageLiteral(resourceName: "s73_160"),#imageLiteral(resourceName: "glasses_nerd"),#imageLiteral(resourceName: "glasses_normal"),#imageLiteral(resourceName: "glasses_shutter_green"),#imageLiteral(resourceName: "glasses_shutter_yellow"),#imageLiteral(resourceName: "glasses_sun"),#imageLiteral(resourceName: "glasses"),#imageLiteral(resourceName: "hair"),#imageLiteral(resourceName: "hair_2"),#imageLiteral(resourceName: "hair_3"),#imageLiteral(resourceName: "hair_4"),#imageLiteral(resourceName: "hair_5"),#imageLiteral(resourceName: "hair_6"),#imageLiteral(resourceName: "hair_7"),#imageLiteral(resourceName: "hair_8"),#imageLiteral(resourceName: "hair_9"),#imageLiteral(resourceName: "hat-cap"),#imageLiteral(resourceName: "hat-cylinder"),#imageLiteral(resourceName: "hat-party"),#imageLiteral(resourceName: "hat-sheriff"),#imageLiteral(resourceName: "hat"),#imageLiteral(resourceName: "mustache-long"),#imageLiteral(resourceName: "mustache1"),#imageLiteral(resourceName: "mustache2"),#imageLiteral(resourceName: "mustache3"),#imageLiteral(resourceName: "s3_160"),#imageLiteral(resourceName: "s29_160"),#imageLiteral(resourceName: "s30_160"),#imageLiteral(resourceName: "s33_160"),#imageLiteral(resourceName: "s35_160"),#imageLiteral(resourceName: "s54_160"),#imageLiteral(resourceName: "Baseball_NoBorder_1024"),#imageLiteral(resourceName: "BaseballBall_NoBorder_1024"),#imageLiteral(resourceName: "Football_NoBorder_1024"),#imageLiteral(resourceName: "Lacrosse_NoBorder_1024"),#imageLiteral(resourceName: "Skating_NoBorder_1024"),#imageLiteral(resourceName: "Soccer_NoBorder_1024"),#imageLiteral(resourceName: "Swimming_NoBorder_1024"),#imageLiteral(resourceName: "Tennis_NoBorder_1024"),#imageLiteral(resourceName: "TennisBall_NoBorder_1024"),#imageLiteral(resourceName: "Volleyball_NoBorder_1024"),#imageLiteral(resourceName: "SummerEmojiOutline_01_1024"),#imageLiteral(resourceName: "SummerEmojiOutline_02_1024"),#imageLiteral(resourceName: "SummerEmojiOutline_03_1024"),#imageLiteral(resourceName: "SummerEmojiOutline_04_1024"),#imageLiteral(resourceName: "SummerEmojiOutline_05_1024"),#imageLiteral(resourceName: "SummerEmojiOutline_06_1024"),#imageLiteral(resourceName: "SummerEmojiOutline_07_1024"),#imageLiteral(resourceName: "SummerEmojiOutline_08_1024"),#imageLiteral(resourceName: "SummerEmojiOutline_09_1024"),#imageLiteral(resourceName: "SummerEmojiOutline_10_1024"),#imageLiteral(resourceName: "s142_160"),#imageLiteral(resourceName: "s42_160"),#imageLiteral(resourceName: "s43_160"),#imageLiteral(resourceName: "s44_160"),#imageLiteral(resourceName: "s45_160"),#imageLiteral(resourceName: "s47_160"),#imageLiteral(resourceName: "s50_160"),#imageLiteral(resourceName: "s53_160")]
    var framesImages : [UIImage] = [#imageLiteral(resourceName: "icNoFrame"),#imageLiteral(resourceName: "boarder_32.png"),#imageLiteral(resourceName: "boarder_1.png"),#imageLiteral(resourceName: "boarder_2.png"),#imageLiteral(resourceName: "boarder_3.png"),#imageLiteral(resourceName: "boarder_4.png"),#imageLiteral(resourceName: "boarder_5.png"),#imageLiteral(resourceName: "boarder_6.png"),#imageLiteral(resourceName: "boarder_7.png"),#imageLiteral(resourceName: "boarder_8.png"),#imageLiteral(resourceName: "boarder_9.png"),#imageLiteral(resourceName: "boarder_10.png"),#imageLiteral(resourceName: "boarder_11.png"),#imageLiteral(resourceName: "boarder_12.png"),#imageLiteral(resourceName: "boarder_13.png"),#imageLiteral(resourceName: "boarder_14.png"),#imageLiteral(resourceName: "boarder_15.png"),#imageLiteral(resourceName: "boarder_16.png"),#imageLiteral(resourceName: "boarder_17.png"),#imageLiteral(resourceName: "boarder_18.png"),#imageLiteral(resourceName: "boarder_19.png"),#imageLiteral(resourceName: "boarder_20.png"),#imageLiteral(resourceName: "boarder_21.png"),#imageLiteral(resourceName: "boarder_22.png"),#imageLiteral(resourceName: "boarder_23.png"),#imageLiteral(resourceName: "boarder_24.png"),#imageLiteral(resourceName: "boarder_25.png"),#imageLiteral(resourceName: "boarder_26.png"),#imageLiteral(resourceName: "boarder_27.png"),#imageLiteral(resourceName: "boarder_28.png"),#imageLiteral(resourceName: "boarder_29.png"),#imageLiteral(resourceName: "boarder_30.png"),#imageLiteral(resourceName: "boarder_31.png")]
    let framesThumbnailListArray = ["icNoFrame","boarder_32.png","boarder_1.png","boarder_2.png","boarder_3.png","boarder_4.png","boarder_5.png","boarder_6.png","boarder_7.png","boarder_8.png","boarder_9.png","boarder_10.png","boarder_11.png","boarder_12.png","boarder_13.png","boarder_14.png","boarder_15.png","boarder_16.png","boarder_17.png","boarder_18.png","boarder_19.png","boarder_20.png","boarder_21.png","boarder_22.png","boarder_23.png","boarder_24.png","boarder_25.png","boarder_26.png","boarder_27.png","boarder_28.png","boarder_29.png","boarder_30.png","boarder_31.png"]
    
    let framesNameArray = ["Classic", "Woodstock", "Daisy", "Desk", "Washi", "Nordic", "Poppy", "Kyoto", "Cupcake", "Elettrico", "Wisteria", "Nelson", "Stellar", "Gamma", "Calypso", "Wiggle", "Chestnut", "Macchiato", "Hexstatic", "Confetti", "Chevron" , "Fireworks" , "Madeira", "Poolside", "Mellow", "Sunday", "Brighton", "Ananas", "Felicity", "Flopper", "Tropical", "Sunshine"]
    //    var paraEditObject = ParaClassEditImage()
    var paraIntermediateViews = ParaClassIntermediateViewChanges()
    var mainEditOptionsArray = ["Filter", "Edit", "Style", "AR"]
    
    // MARK:- Filter
    var selectedLutData64 = Data()
    let filterClass = AnimationClass()
    var filterThumbnailImage = UIImage()
    let filterEffectsNameList = ["Normal", "Cinder", "Rocksteady","Canary","Tuxedo","Ojai","Casablanca", "Sahara","Chantenay","Amaranth","Toffee","Salem","Palmdale","Hayse","Emperor","Maxim","Cottontail","Seventeen","Sleet","Fairweather","Wednesday","Texas","Oakley","Kalahari","Raymond","Boogie"]
    let filterNameList = ["Normal", "ZipLut_1", "Sin", "Lomo","AD1920","Lenin","SepiaHigh", "Winter","Pola669","PolaSX","Food","Glam","Chest","Front","K1","K2","K6","KDynamic","Fridge","Orchid","X400","Fixie","BW","Quozi","Celsius","Texas"]
    lazy var arrFilterPreviewImages = [UIImage]()
    var isNativeFiltersMode = false
    let nativeFiltersArray = ["Normal", "CIFalseColor", "CIPhotoEffectProcess", "CIPhotoEffectInstant", "CIPhotoEffectChrome", "CIPhotoEffectFade","CIPhotoEffectMono","CIPhotoEffectNoir","CIPhotoEffectTonal","CIPhotoEffectTransfer", "CISpotColor",  "CILinearToSRGBToneCurve",  "CIColorInvert","CIColorMonochrome","CIColorPosterize","CIMaximumComponent","CIMinimumComponent","CISepiaTone"] //18
    /*
     ["CIFalseColor", "CIPhotoEffectProcess", "CIPhotoEffectInstant", "CIFalseColor", "CIColorMonochrome", "CIPhotoEffectMono", "CIPhotoEffectTransfer", "CIPhotoEffectNoir", "CIPhotoEffectTonal", "CISepiaTone"]
     */
    lazy var activityIndicator : UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(activityIndicatorStyle: .white)
        loader.color = .black
        loader.startAnimating()
        return loader
    }()
    var downloadingStickers: [Int] = []
    var downloadingFrames: [Int] = []
    var stickersThumbnails: [UIImage] = []
    var framesThumbnails: [UIImage] = []
  //  var SelectVideoVC : SelectVideoViewController?
    var isCameFromOnboarding: Bool  = false
    var isEditOptionClicked : Bool = false
    var isFilterOptionClicked : Bool = false
    var isRotateOptionClicked : Bool = false
    //Hide Status bar
    override var prefersStatusBarHidden: Bool {
        return status_Bar_Hidden
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    } 
    
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.borderView.isHidden = true
    }
       
    
    override func viewDidAppear(_ animated: Bool) {
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.imagePreview.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
        self.setupImageView()
        collectionViewEditMainOptions.delegate = self
        collectionViewEditMainOptions.dataSource = self
        collectionViewFilter.delegate = self
        collectionViewFilter.dataSource = self
        //        self.setupGesturesOnImageview()
        self.setUpNotificationCenter()
        self.setupEditParameters()
        if self.imgOriginalCroped.size != CGSize(width: 0, height: 0){
             imgOriginalCroped = self.captureImageInView(inView: self.viewFixPaperSizeBackground)!
        }
        //Akshay - Ar
        /*
         if is_Ar_Video_Available{
         imgOriginal = image_attach_with_Video
         self.imageViewPreview.image = imgOriginal
         }
         */
        //        DispatchQueue.main.async {
        //            self.view.isUserInteractionEnabled = true
        //            self.stopLoader()
        //        }
        if !UserDefaults.standard.bool(forKey: "overlayPresent") {
            //            prepareChildOverlayView("ImagePreview")
            self.prepareChildEditOverlayView()
        }
        //        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.disableScrollZoom()
        //        Dont show Remove Ar , on saving image in gallery
        if isComeFrom == "PopUpRetryPrintVC" || isComeFrom == "GallertGrid"
        {
        //    is_Ar_Video_Available = false
            self.viewRemoveBtnAr.isHidden = true
            self.viewArVideoAttachedIcon.isHidden = true
            isComeFrom = ""
        }
        self.setupARObservers()
        self.checkPrinterStatus()
        //        AIModel().stopAIOperation(flag: ObjCBool(true))
        //        AIModel().stopAIThread()
        //Akshay - Ar
        /*
         if is_Ar_Video_Available{
         imgOriginal = image_attach_with_Video
         self.imageViewPreview.image = imgOriginal
         }
         */
        editedImageExceptCurrentEffect = imgMainEdit//imgOriginal
        let aUIImage = imgMainEdit //imgOriginal//editedImageExceptCurrentEffect
        print("aUIImage",aUIImage)
        guard let aCGImage = aUIImage.cgImage else {
            return
        }
        aCIImage = CIImage.init(cgImage: aCGImage)
        self.setupAR()
        self.collectionView(collectionViewEditMainOptions, didSelectItemAt: IndexPath(row: 0, section: 0))
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.view.isUserInteractionEnabled = true
        self.removeARObservers()
        self.checkPrinterStatus()
      //  self.stopLoader()
        
        //        AIModel().stopAIOperation(flag: ObjCBool(false))
        //        SMLFlagForAI = !SMLFlagForAI
        //        PredictionController().getPredictionFromImageAsset(assetArray: image_For_AI)
        //NotificationCenter.default.removeObserver(self)
        
    }
    
    //    deinit {
    //        print("deinit from image preview")
    //        PrinterQueueManager.sharedController.errorDelegate = nil
    //    }
    
    override func viewDidLayoutSubviews() {
        self.viewWidth = Float(self.btnPrint.frame.width)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - ScrollViewDelegate -
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageViewPreview
    }
    
     func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
          if isEditOptionClicked {
              if isFilterOptionClicked || isRotateOptionClicked{
                checkZoomScaleOnFilterAndRotate = scale
                 print("Filter/Rotate Zoomed")
              }else{
                checkZoomScale = scale
                print("Edit Zoomed")
              }
          }
      }
    
    func showPopupViewController(viewController : UIViewController)
    {
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
        rootViewController?.view.addSubview(viewController.view)
        rootViewController?.addChildViewController(viewController)
        viewController.view.frame = CGRect(x: 0, y: 0, width: rootViewController?.view.frame.width ?? 0.0, height: rootViewController?.view.frame.height ?? 0.0)
    }
    
    // MARK: - Action Methods
    
    @IBAction func smileBtnClicked(_ sender: Any) {
//        self.viewFixPaperSizeBackground.translatesAutoresizingMaskIntoConstraints = true
        viewFixPaperSizeBackground.heightAnchor.constraint(equalTo: viewFixPaperSizeBackground.widthAnchor, multiplier: 980.0/640.0).isActive = true
         viewFixPaperSizeBackground.widthAnchor.constraint(equalTo: viewFixPaperSizeBackground.heightAnchor, multiplier: 640.0/980.0).isActive = true

        self.viewFixPaperSizeBackground.layer.shadowOpacity = 1.0
        self.borderView.isHidden = true
        self.constraintTopViewFixPaperSizeBG.constant =  70
         print(self.constraintTopViewFixPaperSizeBG.constant)
        self.constraintBottomViewFixSizePaperBackground.constant = 50
//        viewFixPaperSizeBackground.leadingAnchor.constraint(equalTo:  viewFixPaperSizeBackground.superview!.leadingAnchor, constant: 48).isActive = false
//               viewFixPaperSizeBackground.trailingAnchor.constraint(equalTo:  viewFixPaperSizeBackground.superview!.trailingAnchor, constant: 48).isActive = false
//
        viewFixPaperSizeBackground.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 48.0).isActive = false
        viewFixPaperSizeBackground.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -48.0).isActive = false
        
    }
    
    @IBAction func classicBtnClicked(_ sender: Any) {
//         self.viewFixPaperSizeBackground.translatesAutoresizingMaskIntoConstraints = false
        viewFixPaperSizeBackground.heightAnchor.constraint(equalTo: viewFixPaperSizeBackground.widthAnchor, multiplier: 1488.0/1232.0).isActive = true
        
         viewFixPaperSizeBackground.widthAnchor.constraint(equalTo: viewFixPaperSizeBackground.heightAnchor, multiplier: 1232.0/1488.0).isActive = true

        self.viewFixPaperSizeBackground.layer.shadowOpacity = 0.0
        self.borderView.isHidden = false
        self.constraintTopViewFixPaperSizeBG.constant =  100
         print(self.constraintTopViewFixPaperSizeBG.constant)
        self.constraintBottomViewFixSizePaperBackground.constant = 80
        
        //add leading= 48 and trailing = 48 programatically to superview with bool values
//        let mylabelLeading = NSLayoutConstraint(item: viewFixPaperSizeBackground, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 48).isActive = true
//        let mylabelTrailing = NSLayoutConstraint(item: viewFixPaperSizeBackground, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 48).isActive = true
        viewFixPaperSizeBackground.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 48.0).isActive = true
        viewFixPaperSizeBackground.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -48.0).isActive = true
       // NSLayoutConstraint.activate(\[mylabelLeading,mylabelTrailing\])
//      viewFixPaperSizeBackground.leadingAnchor.constraint(equalTo:  viewFixPaperSizeBackground.superview!.leadingAnchor, constant: 48).isActive = true
//        viewFixPaperSizeBackground.trailingAnchor.constraint(equalTo:  viewFixPaperSizeBackground.superview!.trailingAnchor, constant: 48).isActive = true
    }
    @IBAction func removeBtnClick(_ sender: Any) {
        self.viewRemoveBtnAr.isHidden = true
        self.viewArVideoAttachedIcon.isHidden = true
      //  is_Ar_Video_Available = false
    }
    
    @IBAction func overlayDismissBtnClicked(_ sender: UIButton) {
        viewSwipeDownOverlay.isHidden = true
    }
    
    @IBAction func btnMenuClicked(_ sender: Any)
    {
        let alertView = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let saveAction = UIAlertAction(title: "Save to camera roll", style: .default) { (action) in
            let finalImage = self.getEditedImgWithoutSlider()
            let saveImage = saveImageInCustomAlbum()
            saveImage.saveImageInAlbum(name: PolaroidSnaptouchEditAlbum, withImage: finalImage)
        }
        let shareAction  = UIAlertAction(title: "Share", style: .default) { (action) in
            let finalImage = self.getEditedImgWithoutSlider()
            self.shareImage(image: finalImage)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertView.addAction(shareAction)
        alertView.addAction(saveAction)
        alertView.addAction(cancelAction)
        self.present(alertView, animated: true, completion: nil)
    }
    
    @IBAction func btnCrossClicked(_ sender: Any)
    {
        isEditOptionClicked = false
      //  is_Ar_Video_Available = false
        //        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.removeObserver(self)
        //if user coming from "Print my Picture VC"
        
        if isCameFromOnboarding{
            let HomeVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(HomeVC, animated: false)
        } else {
            if let navVC = self.navigationController
            {
                // Pop view controller to home.
                for item in navVC.viewControllers {
                    // Filter for your desired view controller:
                    if item.isKind(of: GalleryImageGridViewController.self) {
                        //isHomeVCInStack = true
                        self.navigationController?.popToViewController(item, animated: false)
                    }
                }
            }
            var isCameraVCInStack = false
            if let navVC = self.navigationController
            {
                // Pop view controller to home.
                for item in navVC.viewControllers {
                    // Filter for your desired view controller:
                    if item.isKind(of: CameraViewController.self) {
                        isCameraVCInStack = true
                        self.navigationController?.popToViewController(item, animated: true)
                    }
                }
                // When directly came from onboarding, home VC is not in stack so navigate by pushing to it
                if isCameraVCInStack == false
                {
                    // Pop view controller to home.
                    for item in navVC.viewControllers {
                        // Filter for your desired view controller:
                        if item.isKind(of: HomeViewController.self) {
                            self.navigationController?.popToViewController(item, animated: true)
                            break
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func btnPrintClicked(_ sender: Any)
    {

//        appDelegate().isGivenToPrint = true
//        if WebserviceModelClass().isInternetAvailable() && appDelegate().isConnectedToPrinter {
//         //   self.checkARAttachedBeforePrinting()
//            if self.printBtnClickCount < 1 {
//                self.printBtnClickCount += 1
//                PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.getZIPInfo, ResponseDelegate: self))
//            } else {
//                customLoader.showActivityIndicator(viewController: self.view)
//                // This is called to check battery percentage before starting printing
//                PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.getZIPInfo, ResponseDelegate: self))
//            }
//        }else if appDelegate().isConnectedToPrinter{
//         //   self.checkARAttachedBeforePrinting()
//            PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.getZIPInfo, ResponseDelegate: self))
//        } else {
//            let connectivityVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController
//            self.navigationController?.pushViewController(connectivityVC, animated: true)
//        }
//
//    }
//
//    @IBAction func goBackToMultiPrintVCBtnClicked(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
//    }
//
//    @IBAction func saveImageToMultiPrintVCBtnClicked(_ sender: UIButton) {
//        self.sliderFilterOpacity.isHidden = true
//        self.lblSliderValue.isHidden = true
//        self.gradientViewLeftToRight.isHidden = true
//        let finalImage = self.captureImageInView(inView: self.viewImageContainer)
//        self.delegateMultiPrint?.getSelectedImageEditedBack(finalImage!)
//        self.sliderFilterOpacity.isHidden = false
//        self.lblSliderValue.isHidden = false
//        self.gradientViewLeftToRight.isHidden = false
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMediaClicked(_ sender: Any) {
//        if is_Ar_Video_Available {
//            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpReplaceARVideoViewController") as! PopUpReplaceARVideoViewController
//            alertPopUp.view.frame = self.view.bounds
//            self.parent?.addChildViewController(alertPopUp)
//            self.parent?.view.addSubview(alertPopUp.view)
//            alertPopUp.didMove(toParentViewController: self)
//        }else{
//            let SelectVideoVC = storyboards.arStoryboard.instantiateViewController(withIdentifier: "SelectVideoViewController") as! SelectVideoViewController
//            
//            let videoAssets = getVideoAssetsAlbumWise()
//            if videoAssets?.count ?? 0 > 0
//            {
//                SelectVideoVC.imagesArray = videoAssets
//                self.navigationController?.pushViewController(SelectVideoVC, animated: true)
//            }
//            else
//            {
//                self.view.makeToast("There are no videos on your device".localisedString(), duration: 2.0, position: .bottom)
//            }
//        }
    }
    
    @IBAction func btnFaceFilterClicked(_ sender: Any) {
        self.view.makeToast("Coming soon".localisedString(), duration: 2.0, position: .bottom)
    }
}
