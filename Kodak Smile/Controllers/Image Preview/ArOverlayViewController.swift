//
//  ArOverlayViewController.swift
//  Kodak Smile
//
//  Created by MacBook003 on 10/04/19.
//  Copyright © 2019 maximess. All rights reserved.
//

//import UIKit
//import ARKit
//var isReachedToARScanOverlay = false
//class ArOverlayViewController: UIViewController , CameraPermissinNativePopupDelegate, DismissARAlertScan{
//    
//    //MARK:- Outlets
//    @IBOutlet weak var lblPrintSuccess: UILabel!
//    @IBOutlet weak var lblScanPicture: UILabel!
//    @IBOutlet weak var btnScanNow: UIButton!
//    @IBOutlet weak var btnScanLater: UIButton!
//    
//    //MARK:- Life cycle
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        isReachedToARScanOverlay = true
//        // Do any additional setup after loading the view.
//        self.localizedStrings()
//    }
//    
//    //MARK:- Actions
//    @IBAction func crossBtnClicked(_ sender: Any) {
//        if let navVC = self.navigationController
//        {
//            // Pop view controller to home.
//            for item in navVC.viewControllers {
//                // Filter for your desired view controller:
//                if item.isKind(of: GalleryImageGridViewController.self) {
//                    self.navigationController?.popToViewController(item, animated: false)
//                }
//            }
//        }
//    }
//    
//    @IBAction func scanImageNowBtnClicked(_ sender: Any) {
//        if #available(iOS 11.311.3, *) {
////            if !UserDefaults.standard.bool(forKey: "isARAlertScanFirstTimePopped"){
//////                if WebserviceModelClass().isInternetAvailable(){
////                    let vc = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpARAlertScanFirstTimeViewController") as! PopUpARAlertScanFirstTimeViewController
////                    self.addChildViewController(vc)
////                    self.view.addSubview(vc.view)
////                    vc.delegate = self
////                    UserDefaults.standard.set(true, forKey: "isARAlertScanFirstTimePopped")
////            }else{
//                if WebserviceModelClass().isInternetAvailable(){
//                    self.cameraPermission()
//                }else{
//                    let vc = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
//                    self.present(vc, animated: true, completion: nil)
//                }
////            }
//        } else {
//            // Fallback on earlier versions
//        }
//    }
//    
//    @IBAction func scanLaterBtnClicked(_ sender: Any) {
//           var isHomeVCInStack = false
//
//               if let navVC = self.navigationController
//               {
//                   // Pop view controller to home.
//                   for item in navVC.viewControllers {
//                       // Filter for your desired view controller:
//                       if item.isKind(of: HomeViewController.self) {
//                           isHomeVCInStack = true
//                           self.navigationController?.popToViewController(item, animated: true)
//                       }
//                       
//                   }
//                   // When directly came from onboarding, home VC is not in stack so navigate by pushing to it
//                   
//                   if isHomeVCInStack == false
//                   {
//                       //already authorized
//                       let homeVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                       self.navigationController?.pushViewController(homeVC, animated: false)
//                   }
//                   //                self.dismiss(animated: true, completion: nil)
//               }
//    }
//    
//    //MARK:- Function
//    func localizedStrings() {
//        self.lblPrintSuccess.text = "Print Successful".localisedString()
//        self.lblScanPicture.text = "Scan your printed picture in order to reveal the hidden AR element".localisedString()
//        self.btnScanNow.setTitle("Scan image now".localisedString(), for: .normal)
//        self.btnScanLater.setTitle("Scan later".localisedString(), for: .normal)
//    }
//    
//    @objc func dismissVcIfNoInternetOccurs(){
//        if let navVC = self.navigationController
//        {
//            // Pop view controller to home.
//            for item in navVC.viewControllers {
//                // Filter for your desired view controller:
//                if item.isKind(of: HomeViewController.self) {
//                    //isHomeVCInStack = true
//                    self.navigationController?.popToViewController(item, animated: false)
//                }
//            }
//        }
//    }
//    
//    @available(iOS 11.311.3, *)
//    func cameraPermission(){
//        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
//            DispatchQueue.main.async {
////                if UserDefaults.standard.bool(forKey: "isARAlertScanFirstTimePopped"){
//                    let ScanVC = storyboards.arStoryboard.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
//                    self.navigationController?.pushViewController(ScanVC, animated: false)
////                }else{
////                    let vc = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpARAlertScanFirstTimeViewController") as! PopUpARAlertScanFirstTimeViewController
////                    self.addChildViewController(vc)
////                    self.view.addSubview(vc.view)
////                    vc.delegate = self
////                    UserDefaults.standard.set(true, forKey: "isARAlertScanFirstTimePopped")
////                }
//            }
//        } else {
//            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpWeWouldLikeToOpenCameraViewController") as! PopUpWeWouldLikeToOpenCameraViewController
//            alertPopUp.delegate = self
//            self.present(alertPopUp, animated: true, completion: nil)
//            
//        }
//    }
//    
//    func showCameraPermissionPopup() {
//        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
//            DispatchQueue.main.async {
//                if (ARConfiguration.isSupported)  {
//                    if #available(iOS 11.311.3, *) {
//                      //  if UserDefaults.standard.bool(forKey: "isARAlertScanFirstTimePopped"){
//                            let ScanVC = storyboards.arStoryboard.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
//                            self.navigationController?.pushViewController(ScanVC, animated: false)
//                            isScanningAR = true
////                        }else{
////                            let vc = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpARAlertScanFirstTimeViewController") as! PopUpARAlertScanFirstTimeViewController
////                            self.addChildViewController(vc)
////                            self.view.addSubview(vc.view)
////                            vc.delegate = self
////                            UserDefaults.standard.set(true, forKey: "isARAlertScanFirstTimePopped")
////                        }
//                    }
//                }else{
//                    let alert = UIAlertController(title: "Alert", message: "Your Device does not support AR", preferredStyle: UIAlertControllerStyle.alert)
//                    let okAction = UIAlertAction(title: "OK", style: .default) {
//                        UIAlertAction in
//                        self.navigationController?.popViewController(animated: false)
//                    }
//                    
//                    alert.addAction(okAction)
//                    
//                    self.present(alert, animated: true, completion: nil)
//                    self.view.removeFromSuperview()
//                }
//            }
//        }else{
//            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
//                if granted {
//                    //access allowed
//                    DispatchQueue.main.async(execute: {
//                     //   if UserDefaults.standard.bool(forKey: "isARAlertScanFirstTimePopped"){
//                            if #available(iOS 11.311.3, *) {
//                                let ScanVC = storyboards.arStoryboard.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
//                                self.navigationController?.pushViewController(ScanVC, animated: false)
//                                isScanningAR = true
//                            }
////                        }else{
////                            let vc = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpARAlertScanFirstTimeViewController") as! PopUpARAlertScanFirstTimeViewController
////                            self.addChildViewController(vc)
////                            self.view.addSubview(vc.view)
////                            vc.delegate = self
////                            UserDefaults.standard.set(true, forKey: "isARAlertScanFirstTimePopped")
////                        }
//                    })
//                } else {
//                    DispatchQueue.main.async {
//                        if let navVC = self.navigationController
//                        {
//                            // Pop view controller to home.
//                            for item in navVC.viewControllers {
//                                // Filter for your desired view controller:
//                                if item.isKind(of: HomeViewController.self) {
//                                    //isHomeVCInStack = true
//                                    self.navigationController?.popToViewController(item, animated: false)
//                                    UserDefaults.standard.set(true, forKey: "isWeWouldLikeToCameraFirstTimePopped")
//                                }
//                            }
//                        }
//                    }
//                }
//            })
//        }
//    }
//    
//    func makeChanges() {
//        if #available(iOS 11.311.3, *) {
//            self.cameraPermission()
//        }
//    }
//}
