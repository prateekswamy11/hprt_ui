//
//  ImagePreviewVCSetupExtension.swift
//  Kodak Smile
//
//  Created by maximess152 on 27/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation
import HSCenterSlider
import HSRange

extension ImagePreviewViewController {
    func initialSetup() {
        self.disableScrollZoom()
        self.eventSticker = DBModelForEvents.shared.fetchStickersFrmaesThumbnail(isSticker: true)
        self.eventFrames = DBModelForEvents.shared.fetchStickersFrmaesThumbnail(isSticker: false)
        self.localizedStrings()
        self.setConstraintAccordingToDev()
        // Rotate Landscape image
        if imgMainEdit.size.width > imgMainEdit.size.height
        {
            imgMainEdit = imgMainEdit.rotateImageByDegrees(90.0)
        }
        // Get the image in landscape or portrait format to save the original image in a vaibale always
        imgOriginal = imgMainEdit
        self.imageViewPreview.image = imgMainEdit
        // Rotate slider vertical
        sliderFilterOpacity.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
        viewSwipeDownOverlay.isHidden = true
        self.checkIfComeFromMultiPrint()
       // self.startLoader()
        self.networkReachability()
        self.scrollViewForZooming.delegate = self
        self.scrollViewForZooming.minimumZoomScale = 1.0
        self.scrollViewForZooming.maximumZoomScale = 6.0
    }
    
    func setupARObservers() {
//        NotificationCenter.default.addObserver(self, selector: #selector(updateNewTargetAfterDelete), name: Notification.Name("updateNewTargetAfterdeletingOld"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(enablePrintBtn), name: Notification.Name("enablePrintBtnOnPopupCross"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(saveImageOnPrintSuccess), name: Notification.Name("saveImageOnPrintSuccess"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(saveImageOnRetryPrint), name: Notification.Name("saveImageOnRetryPrintWithoutLogo"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(isPrintingStarted), name: Notification.Name("notificationTocheckPrinting"), object: nil)
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(ReplaceVideoIfAbailable),
//            name: NSNotification.Name(rawValue: "ReplaceVideo"),
//            object: nil)
    }
    
    func setupAR() {
//        let aUIImage = imgOriginal//self.imgMainEdit //editedImageExceptCurrentEffect
//        print("aUIImage",aUIImage)
//        guard let aCGImage = aUIImage.cgImage else {
//            return
//        }
//        aCIImage = CIImage.init(cgImage: aCGImage)
//        if is_Ar_Video_Available == true
//        {
//            self.viewArVideoAttachedIcon.isHidden = false
//            self.viewRemoveBtnAr.isHidden = false
//        }
//        else{
//            self.viewArVideoAttachedIcon.isHidden = true
//            self.viewRemoveBtnAr.isHidden = true
//        }
//        if appDelegate().isPrintingInProgress && IS_HOLD_ON_POP_SHOWN == false{
//            let vc = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpHeldOnViewController") as! PopUpHeldOnViewController
//            self.addChildViewController(vc)
//            self.view.addSubview(vc.view)
//        }
//        if(appDelegate().isConnectedToPrinter == true && WebserviceModelClass().isInternetAvailable())
//        {
//            self.getFirmware()
//        }
    }
    
    func localizedStrings() {
        self.lblSendingToPrinter.text = "sending to printer".localisedString()+"..."
        self.lblAdjustContrast.text = "Adjust Contrast".localisedString()
        self.lblAdjustSaturation.text = "Adjust Saturation".localisedString()
        self.lblAdjustWarm.text = "Adjust Warmth".localisedString()
        self.lblAdjustBrightness.text = "Adjust Brightness".localisedString()
        self.lblAdjustHighlights.text = "Adjust Highlights".localisedString()
        self.lblAdjustShadows.text = "Adjust Shadows".localisedString()
        self.lblPinchtoZoom.text = "Pinch to Zoom In and Out".localisedString()
        self.lblRotate.text = "Rotate".localisedString() + " 90°"
        self.lblPrint.text = "Print".localisedString()
        self.lblAddARElement.text = "Add AR elements, print and scan to reveal".localisedString()
        self.lblFaceFilter.text = "Face Filters".localisedString()
        self.lblMedia.text = "Media".localisedString()
        self.btnDone.setTitle( "Done".localisedString(), for: .normal)
        self.btnCancel.setTitle( "Cancel".localisedString(), for: .normal)
        self.btnAddText.setTitle( "Add Text".localisedString(), for: .normal)
        self.lblSwipeDown.text = "Swipe down to control\nthe opacity level".localisedString()
        self.btnSaveMultiPrint.setTitle("Save".localisedString(), for: .normal)
        self.btnOptions.setTitle("Share".localisedString(), for: .normal)
        self.btnCross.setTitle("Close".localisedString(), for: .normal)
    }
    
    // function which is triggered when handleSwipeDown is called
    @objc func handleSwipe(_ sender: UISwipeGestureRecognizer) {
        viewSwipeDownOverlay.isHidden = true
    }
    
    // Top Constraints set of viewfixpapersizebg according to device to solve image flicker issue
    func setConstraintAccordingToDev(){
        if UIScreen.main.nativeBounds.height > 1335.0{
            self.constraintTopViewFixPaperSizeBG.constant = 70.0
        }else{
            self.constraintTopViewFixPaperSizeBG.constant = 57.0
        }
    }
    
    func setUpNotificationCenter() {
        if self.shouldAddObeservers == true
        {
            self.shouldAddObeservers = false
//            NotificationCenter.default.addObserver(
//                self,
//                selector: #selector(AccessoriesDisConnect),
//                name: NSNotification.Name(rawValue: "AccessoryDisConnect"),
//                object: nil)
//            
//            NotificationCenter.default.addObserver(
//                self,
//                selector: #selector(AccessoriesConnect),
//                name: NSNotification.Name(rawValue: "AccessoryConnect"),
//                object: nil)
//            
//            NotificationCenter.default.addObserver(
//                self,
//                selector: #selector(handlePrintCompletion),
//                name: NSNotification.Name(rawValue: "PrintFinished"),
//                object: nil)
//            
//            NotificationCenter.default.addObserver(
//                self,
//                selector: #selector(dismissStickerOverlayDismiss),
//                name: NSNotification.Name(rawValue: "StickerOverlayDismiss"),
//                object: nil)
            
            self.setUpEditNotificationObserver()
        }
    }
    
    func setupImageView()
    {
        var img = imgMainEdit
        // ---- Filter
        // Compress this image so that filter previews don't consume too much time to load.
        //Akshay Solve crash detect on crashanalitics Add try catch
        if let imgsData = UIImageJPEGRepresentation(imgMainEdit, 0.0)
        {
            img = UIImage(data: imgsData)!
            filterThumbnailImage = img
            filterThumbnailImage = self.filterThumbnailImage.aspectFit(CGSize(width: 80, height: 80))
        }
        self.arrFilterPreviewImages.append(filterThumbnailImage)
    }
    
    func setupGesturesOnImageview()
    {
        self.initialiseGestureOnImage()
        self.changeGestureOnImage(pan: false, pinch: false, rotate: false)
    }
    
    func setupEditParameters()
    {
        self.filter_setImage()
        if UIScreen.main.bounds.width <= 320 {
            self.lblPrintingLabel.font = UIFont.boldSystemFont(ofSize: 32)
            self.lblPrintingLabel.sizeToFit()
            size = 120
        }
        self.lblPrintingLabel.text = "Beautiful! That’s a great one." // Set dummy text to set adjustsFontSizeToFitWidth  properly
        //        self.lblPrintingLabel.adjustsFontSizeToFitWidth = true
        self.lblPrintingLabel.minimumScaleFactor = 0.2
        // Set up sticker view
        trayDownOffset = self.viewStickers.frame.height * 0.045
        trayUp = CGPoint(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height - UIScreen.main.bounds.height / 3)
        trayDown = CGPoint(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height - UIScreen.main.bounds.height * 0.045)
        // Added just for hiding the border on sticker/text view when clicked outside of bordered view
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapRecognised(tap:)))
        tap.numberOfTapsRequired = 1
        self.viewImageContainer.addGestureRecognizer(tap)
        // Rotate slider vertical
        sliderFilterOpacity.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
    }
    
    func setupEditSliders()
    {
        self.viewShadowsSlider.delegate = self
        self.viewShadowsSlider.tintColor = UIColor.black  //UIColor(red: 0.0/255, green: 180.0/255, blue: 179.0/255, alpha: 1.0)//UIColor.orange
        self.viewShadowsSlider.rangeValue = HSRange(low: -100, high: 100)
        self.viewHighlightsSlider.delegate = self
        self.viewHighlightsSlider.tintColor = UIColor.black  //UIColor(red: 0.0/255, green: 180.0/255, blue: 179.0/255, alpha: 1.0)//UIColor.orange
        self.viewHighlightsSlider.rangeValue = HSRange(low: -100, high: 100)
        self.viewSaturationSlider.delegate = self
        self.viewSaturationSlider.tintColor = UIColor.black  //UIColor(red: 0.0/255, green: 180.0/255, blue: 179.0/255, alpha: 1.0)//UIColor.orange
        self.viewSaturationSlider.rangeValue = HSRange(low: -100, high: 100)
        self.viewContrastSlider.delegate = self
        self.viewContrastSlider.tintColor = UIColor.black    //UIColor(red: 0.0/255, green: 180.0/255, blue: 179.0/255, alpha: 1.0)//UIColor.orange
        self.viewContrastSlider.rangeValue = HSRange(low: -100, high: 100)
        self.viewWarmSlider.delegate = self
        self.viewWarmSlider.tintColor = UIColor.black    //UIColor(red: 0.0/255, green: 180.0/255, blue: 179.0/255, alpha: 1.0)//UIColor.orange
        self.viewWarmSlider.rangeValue = HSRange(low: -100, high: 100)
        self.viewBrightnessSlider.delegate = self
        self.viewBrightnessSlider.tintColor = UIColor.black  //UIColor(red: 0.0/255, green: 180.0/255, blue: 179.0/255, alpha: 1.0)//UIColor.orange
        self.viewBrightnessSlider.rangeValue = HSRange(low: -100, high: 100)
        self.viewRotateSlider.delegate = self
        self.viewRotateSlider.tintColor = UIColor.black  //UIColor(red: 0.0/255, green: 180.0/255, blue: 179.0/255, alpha: 1.0)//UIColor.orange
        self.viewRotateSlider.rangeValue = HSRange(low: -180, high: 180)
        context = CIContext(options: nil);
    }
    
    func setUpEditNotificationObserver()
    {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.addTextLblToView),
            name: NSNotification.Name(rawValue: "addTextLblToView"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.replaceTextLblToView(notification:)),
            name: NSNotification.Name(rawValue: "replaceTextLblToView"),
            object: nil)
    }
    
    func prepareChildOverlayView() {
        if let childOverlayViewVC = storyboards.imgPreviewStoryboard.instantiateViewController(withIdentifier: "OverlayViewController") as? OverlayViewController
        {
            UserDefaults.standard.set(true, forKey: "overlayNormalPrint")
            
            childOverlayViewVC.isFrom = "Sticker"
            addChildViewController(childOverlayViewVC)
            self.view.addSubview(childOverlayViewVC.view)
        }
    }
    func prepareChildEditOverlayView() {
        if let childOverlayViewVC = storyboards.imgPreviewStoryboard.instantiateViewController(withIdentifier: "EditOverlayViewController") as? EditOverlayViewController
        {
            UserDefaults.standard.set(true, forKey: "overlayPresent")
            childOverlayViewVC.widthConstant = self.btnPrint.frame.width
            addChildViewController(childOverlayViewVC)
            self.view.addSubview(childOverlayViewVC.view)
        }
    }
    
    func showArScanOverlayView() {
//        if let childOverlayViewVC = storyboards.imgPreviewStoryboard.instantiateViewController(withIdentifier: "ArOverlayViewController") as? ArOverlayViewController
//        {
//            addChildViewController(childOverlayViewVC)
//            self.view.addSubview(childOverlayViewVC.view)
//        }
    }
    
    func removeARObservers() {
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "updateNewTargetAfterdeletingOld"), object: nil)
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "saveImageOnPrintSuccess"), object: nil)
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "saveImageOnRetryPrintWithoutLogo"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(isPrintingStarted), name: Notification.Name("notificationTocheckPrinting"), object: nil)
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "ReplaceVideo"), object: nil)
    }
}
