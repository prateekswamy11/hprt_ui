//
//  PopUpSelectCopiesViewController.swift
//  Kodak Smile
//
//  Created by MAXIMESS183 on 28/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import CopilotAPIAccess

var finalCopiesToCheckdelete = Int()
class PopUpSelectCopiesViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText1: UILabel!
    @IBOutlet weak var btnPrint: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var pickerSelectCopies: UIPickerView!
    
    //MARK:- Variables
    var printCopiesSelectDelegate : PrintCopiesSelectionProtocol?
    var countOfCopies = 1 // Set by default one copy
    var timeStampStatus = 0
    var selectedCopies = Int()
    let numberOfCopiesList = ["1 Copy", "2 Copies", "3 Copies", "4 Copies", "5 Copies", "6 Copies", "7 Copies", "8 Copies", "9 Copies", "10 Copies"]
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTitle.text = "Your print is ready!".localisedString()
        self.lblText1.text = "Would you like to print more copies?".localisedString()
        self.btnPrint.setTitle("Print".localisedString(), for: .normal)
        self.btnContinue.setTitle("Continue editing".localisedString(), for: .normal)
        pickerSelectCopies.selectRow(selectedCopies - 1, inComponent: 0, animated: false)
    }
    
    //MARK:- Actions
    @IBAction func btnPrintClicked(_ sender: Any)
    {
        finalCopiesToCheckdelete = 0
        self.dismiss(animated: false, completion: nil)
        printCopiesSelectDelegate?.printBtnClickedAfterSelectingCopies()
        let tapPrintAnalyticsEvent = TapPrintAnalyticsEvent(numOfCopies: self.countOfCopies, timestamp: timeStampStatus)
        Copilot.instance.report.log(event: tapPrintAnalyticsEvent)
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

//MARK:- Extensions
extension PopUpSelectCopiesViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
          return 1
     }
     
     func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         return numberOfCopiesList.count
     }
    
     func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         return numberOfCopiesList[row]
     }
     
     func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
         
         return NSAttributedString(string: numberOfCopiesList[row], attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 252, green: 183, blue: 20), NSAttributedString.Key.font:UIFont(name: "SFProText-Regular", size: 13) as Any])
     }
     
     func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
     {
         printCopiesSelectDelegate?.printCopiesSelected(numberOfCopies: row+1)
     }

     func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
     {
         var tView : UILabel?
         if tView == nil
         {
             tView = UILabel()
             tView?.font = UIFont(name: "SFProText-Regular", size: 13)
             tView?.textColor = UIColor.init(red: 252, green: 183, blue: 20)
             tView?.textAlignment = .center
         }
         
         tView?.text = numberOfCopiesList[row]
         return tView ?? UIView()
     }
}
