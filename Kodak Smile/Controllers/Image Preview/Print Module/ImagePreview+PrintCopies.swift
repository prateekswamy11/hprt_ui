//
//  ImagePreview+PrintCopies.swift
//  Kodak Smile
//
//  Created by maximess142 on 29/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

extension ImagePreviewViewController : PrintCopiesSelectionProtocol
{
    func printBtnClickedAfterSelectingCopies() {
        // Check if print count is 0, it will be zero if didSelect is not called. So set it to 1
        self.countOfCopies = (self.countOfCopies == 0 ? 1 : self.countOfCopies)
        // Start the printing process from select popupview
      //  self.startPrintProcess()
    }
    
    func printCopiesSelected(numberOfCopies: Int) {
        // Set number of copies
        self.countOfCopies = numberOfCopies
        noOfCopies = self.countOfCopies
    }
}
