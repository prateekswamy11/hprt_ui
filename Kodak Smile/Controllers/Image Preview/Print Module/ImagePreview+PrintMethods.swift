//
//  ImagePreview+PrintMethods.swift
//  Kodak Smile
//
//  Created by maximess142 on 27/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation
import UIKit
import StoreKit

extension ImagePreviewViewController
{
//    func startPrinting()
//    {
//        if (appDelegate().isConnectedToPrinter == true)
//        {
//            isCurrentImagePrinting = true // This var will ensure that we pop the imgPreviewVC only when current image printing is completed.
//            sliderFilterOpacity.isHidden = true
//            lblSliderValue.isHidden = true
//            gradientViewLeftToRight.isHidden = true
//            //self.addTimerForDismissVC()
//            
//            //self.startLoader()
//            self.showGifLoader()
//            
//            appDelegate().isPrintingError = false
//            //appDelegate().isPrintingInProgress = true // Changing this var when we get print ack for 'image receive' from printer
//            appDelegate().numberOfPrintedImages = 0
//            appDelegate().totalCountOfPrint = 0 // Update this var when number of copies for images is implemented
//            appDelegate().isConnectedToPrinter = true
//            
//            // Send original img for printing if no changes are made
//            var finalImage = UIImage()
////            if is_Ar_Video_Available{
////                finalImage = self.captureImageInView(inView: self.viewFixPaperSizeBackground)!//self.viewImageContainer
////            }else{
////                finalImage = imgFinalCroped
////            }
//             finalImage = imgFinalCroped
//            let isImageEquale = areEqualImages(img1: imgOriginalCroped, img2: finalImage)
//            
//            if isImageEquale == false{
//                let saveImage = saveImageInCustomAlbum()
//                saveImage.saveImageInAlbum(name: PolaroidSnaptouchEditAlbum, withImage: finalImage)
//            }
//            else
//            {
//                finalImage = imgOriginalCroped
//            }
//            
//            if (finalImage != nil)
//            {
//                appDelegate().arrPrintTypeCode.append(contentsOf: self.arrayPrintTypeCodes)
//            }
//            if (appDelegate().arrPrintTypeCode.count == 0)
//            {
//                appDelegate().arrPrintTypeCode.append(PrintTypeCode.simple)
//            }
//            
//            var paperSizeImage = UIImage()
//            var frame = CGRect()
//            
//            if finalImage != nil
//            {
//                if Int((finalImage.size.height)) > Int((finalImage.size.width))
//                {
//                    //potrait
//                    frame = CGRect(x: 0, y: 0, width: 640, height: 980)
//                    //frame = CGRect(x: 0, y: 0, width: (imagetest?.size.width)! * 5, height: (imagetest?.size.height)! * 5)
//                    
//                    paperSizeImage = (finalImage.scaled(to: frame.size, scalingMode: .aspectFit))
//                    
//                }
//                else{
//                    frame = CGRect(x: 0, y: 0, width: 980, height: 640)
//                    paperSizeImage = (finalImage.scaled(to: frame.size, scalingMode: .aspectFit))
//                }
//                
//            }
//            
//            let obj = RequestOperation(type: RequestType.printImage, ResponseDelegate: appDelegate(), PrintResponseDelegate: appDelegate())
//            obj.img = UIImageJPEGRepresentation(paperSizeImage, 1.0 )
//            
//            //printImageData //
//            obj.TagNumber = 1 //index+1
//            obj.arrayPrintType = appDelegate().arrPrintTypeCode
//            
//            obj.TotalNumberOfCopiesAddes = 1
//            obj.no_Of_Copy = 1
//            obj.totalCopies = 1
//            delegate.totalCountOfPrint = delegate.totalCountOfPrint + 1
//            print(obj.arrayPrintType!)
//            
//            appDelegate().printCopies = 1
//            //Accessory_Manager.shared.setUpSession(accessory:appDelegate().currentAccessory!)
//            
////            if is_Ar_Video_Available{
////                //self.uploadPhotoAndVideoToServer(image: paperSizeImage, videoData: video_Data, printingRequest: obj)
////                
////            }
////            else{
////                PrinterQueueManager.RequestArray.append(obj)
////            }
//            
//             PrinterQueueManager.RequestArray.append(obj)
//            //      Remove AR View
//            viewRemoveBtnAr.isHidden = true
//            print("PrinterQueueManager.RequestArray.count = \(PrinterQueueManager.RequestArray)")
//            //            }
//            //appDelegate().arrPrintTypeCode.removeAll()
//        }
//        else
//        {
//            
//            let connectivityVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController
//            
//            self.navigationController?.pushViewController(connectivityVC, animated: true)
//        }
//    }
//    
//    func ZIP_accessory_info(_ dict: NSMutableDictionary) {
//        
//        dictPrinterData = dict
//        
//        
//        let batteryPerStr  =   "\(dictPrinterData!.value(forKey: "Battery")!)"
//        print("batteryPerStr = \(batteryPerStr)")
//        batteryPer = Int(batteryPerStr)!
//        print("batteryPer = \(batteryPer)")
//        
//        customLoader.hideActivityIndicator()
//        PrinterQueueManager.sharedController.errorDelegate = self
//        
//        
//        //        It was saving image with ar icon in gallary
//        //        self.saveImageInGallery()
//        
//        let errorCode = dictPrinterData?.value(forKey: "ErrorCode") as? Int
//        if errorCode != 0
//        {
//            // Don't show the error code here as it would trigger automatically from printqueuemanager.
//            //self.receivedPrintingError(errorCode: errorCode ?? 0)
//        }
//        else if batteryPer < 11
//        {
//            // This is not an error, so explicitely show it
//            self.receivedPrintingError(errorCode: 8)
//        }
//        else
//        {
//            self.showNumberOfCopiesPopup()
//        }
//    }
//    
//    func startPrintProcess()
//    {
//        let printHandler = PrintImageHandler()
//        printHandler.printImageDataArray = self.initialisePrintArray()
//        printHandler.countOfCopies = self.countOfCopies
//        printHandler.isApplyForAll = true
//        
//        if is_Ar_Video_Available && printHandler.printImageDataArray.count == 1{
//            
//            var imagePrint =  printHandler.printImageDataArray[0]["image"] as! UIImage
//            print("imagePrint\(imagePrint)")
//            
//            if video_Is_Landscape == "1"{
//                imagePrint = imagePrint.rotateImageByDegrees(270.0)
//            }
//            if isInternetAvailable(){
//                //                self.uploadPhotoAndVideoToServer(image: imagePrint, videoData: video_Data)
//                let imgData = UIImageJPEGRepresentation(imagePrint, 0.1)!
//                //                UserDefaults.standard.set(imagePrint,forKey: "original_image")
//                UserDefaults.standard.set(imgData,forKey: "imgData_ToMo")
//                // convert the NSData to base64 encoding
//                let base64:String = imgData.base64EncodedString(options: .lineLength64Characters)
//                UserDefaults.standard.set(base64,forKey: "AR_Image")
//                UserDefaults.standard.set(video_Data, forKey: "AR_Video")
//                print("video_Data\(video_Data)")
//                self.healthCheck_EasyAR()
//            }
//            else{
//                
//                let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
//                isComeFrom = ""
//                comeFromStrNoInternetSocialMedia = "NoInternet"
//                self.present(alertPopUp, animated: true, completion: nil)
//            }
//        }
//        else{
//            
//            self.startPrintingAnimation()
//            printHandler.startPrinting()
//            self.finalSaveImageIngallery()
//        }
//    }
//    
//    func startPrintingAnimation()
//    {
//        isCurrentImagePrinting = true // This var will ensure that we pop the imgPreviewVC only when current image printing is completed.
//        sliderFilterOpacity.isHidden = true
//        lblSliderValue.isHidden = true
//        gradientViewLeftToRight.isHidden = true
//        //self.addTimerForDismissVC()
//        
//        self.setRandomPrintingText()
//        //self.startLoader()
//        self.showGifLoader()
//    }
//    
//    //    Function to save edited image in normal printing
//    func finalSaveImageIngallery(){
//        // Save image in gallery before starting print so that if error occurs still the image gets saved.
//        if is_Ar_Video_Available == true || paraEditObject.scaleZoomValue != paraEditObject.previousScaleZoomValue
//        {
//            print("Zoom In /Out in printing Image")
//        }
//        else{
//            
//            self.saveImageInGallery()
//            
//        }
//    }
//    
//    func saveImageInGallery()
//    {
//        // Hide the sliders
//        self.sliderFilterOpacity.isHidden = true
//        self.lblSliderValue.isHidden = true
//        self.gradientViewLeftToRight.isHidden = true
//        
//        // Save Image in Gallery
//        var finalImage = UIImage()
//        if is_Ar_Video_Available{
//            finalImage = self.captureImageInView(inView: self.viewFixPaperSizeBackground)!//self.viewImageContainer
//        }else{
//            finalImage = imgFinalCroped
//        }
//        //        let finalImage = self.captureImageInView(inView: self.viewFixPaperSizeBackground)//self.viewImageContainer
//        
//        let isImageEquale = areEqualImages(img1: imgOriginalCroped, img2: finalImage)
//        print("isImageEquale\(isImageEquale)")
//        CheckImageEdited()
//        
//        if !isImageEquale && self.paraEditObject.isImageEdited{
//            let saveImage = saveImageInCustomAlbum()
//            saveImage.saveImageInAlbum(name: PolaroidSnaptouchEditAlbum, withImage: finalImage)
//        }else if is_Ar_Video_Available == true && !isImageEquale && !isImageSavingonPaperJam{
//            let saveImage = saveImageInCustomAlbum()
//            saveImage.saveImageInAlbum(name: PolaroidSnaptouchEditAlbum, withImage: finalImage)
//        }
//        isImageSavingonPaperJam = false
//    }
//    
//    //    Function to save edited image On Print Success in AR printing and on cross of retry print popup after paper jam in AR printing
//    func save_ARImagesInGallery()
//    {
//        if paraEditObject.scaleZoomValue != paraEditObject.previousScaleZoomValue
//        {
//            print("Zoom In /Out in printing Image")
//        }
//        else{
//            
//            
//            // Hide the sliders
//            self.sliderFilterOpacity.isHidden = true
//            self.lblSliderValue.isHidden = true
//            self.gradientViewLeftToRight.isHidden = true
//            
//            //            if isComeFrom == "PopUpRetryPrintVC"{
//            let finalImage = imgFinalCroped
//            let isImageEquale = areEqualImages(img1: originalImg, img2: finalImage)
//            self.CheckImageEdited()
//            if !isImageEquale && paraEditObject.isImageEdited{
//                let saveImage = saveImageInCustomAlbum()
//                saveImage.saveImageInAlbum(name: PolaroidSnaptouchEditAlbum, withImage: finalImage)
//            }
//            
//            // By sushant - filter selection variable used to check if filter is selected or not, if selected show opacity slider while saving the image, if other options selected, don't show opacity slider.
//            if self.isFilterSelected{
//                self.sliderFilterOpacity.isHidden = false
//                self.lblSliderValue.isHidden = false
//                self.gradientViewLeftToRight.isHidden = false
//            }
//        }
//    }
//    func initialisePrintArray() -> [[String : Any]]
//    {
//        // Hide the sliders
//        self.sliderFilterOpacity.isHidden = true
//        self.lblSliderValue.isHidden = true
//        self.gradientViewLeftToRight.isHidden = true
//        
//        var finalImage = UIImage()
//        if is_Ar_Video_Available{
//            finalImage = self.captureImageInView(inView: self.viewFixPaperSizeBackground)!//self.viewImageContainer
//        }else{
//            finalImage = imgFinalCroped
//        }
//        //        var finalImage = self.captureImageInView(inView: self.viewFixPaperSizeBackground)//self.viewImageContainer
//        
//        let isImageEquale = areEqualImages(img1: imgOriginalCroped, img2: finalImage)
//        
//        // If no changes is made to image, don't send screenshotted image
//        if isImageEquale == false
//        {
//            
//        }
//        else
//        {
//            finalImage = imgOriginalCroped
//        }
//        
//        var imageDataArray = [[String : Any]]()
//        
//        //        if let image = finalImage
//        //        {
//        let imageDict = ["image" :finalImage,
//                         "copies" : self.countOfCopies,
//                         "isTimestampOn": false,
//                         "isColorAdjusted" : false] as [String : Any]
//        imageDataArray.append(imageDict)
//        //        }
//        
//        return imageDataArray
//    }
//    
//    func showNumberOfCopiesPopup()
//    {
//        isPrintDoneCheckOutOfPaper = true
//        let numberOfCopiesVC = storyboards.imgPreviewStoryboard.instantiateViewController(withIdentifier: "PopUpSelectCopiesViewController") as! PopUpSelectCopiesViewController
//        numberOfCopiesVC.printCopiesSelectDelegate = self
//        numberOfCopiesVC.selectedCopies = self.countOfCopies
//        self.present(numberOfCopiesVC, animated: false, completion: nil)
//    }
//    
//    // MARK: - Printer Connection Methods
//    @objc func AccessoriesDisConnect()
//    {
//        // If print queue is not finished
//        if appDelegate().totalCountOfPrint != 0 && !checkCommunicationHiccup{
//            let viewController = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpCommunicationHiccupViewController") as! PopUpCommunicationHiccupViewController
//            iscommunicationHiccup = true
//            let rootViewController = appDelegate().getCurrentViewController()
//            rootViewController?.view.addSubview(viewController.view)
//            rootViewController?.addChildViewController(viewController)
//            viewController.view.frame = CGRect(x: 0, y: 0, width: rootViewController?.view.frame.width ?? 0.0, height: rootViewController?.view.frame.height ?? 0.0)
//            
//        } else {
//            
//        }
//        appDelegate().isConnectedToPrinter = false
//        checkCommunicationHiccup = false
//        self.checkPrinterStatus()
//        UserDefaults.standard.set(false, forKey: "cancelFirmwarePopUP")
//    }
//    
//    @objc func AccessoriesConnect()
//    {
//        self.checkPrinterStatus()
//        PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.get_ZIP_FIRMWARE, ResponseDelegate: self))
//    }
//    
//    // MARK: - Printing Status Methods
//    func showOverlayCompletion(_ status: Bool) {
//        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { timer in
//            if ispaperjam || isOutOfPaperCheck || ispaperMisfeed || issomethingWentWrong || isBatterylow || isCoolingPrinter || iscommunicationHiccup{
//                print("No popup")
//                isImageSavingonPaperJam = true
//            }else{
//                isImageSavingonPaperJam = false
//                is_Ar_Video_Available = true
//                self.showArScanOverlayView()
//                //            if isComeFrom != "PopUpARElementAlreadyAttachedVC"{
//                print("saveImageOnPrintSuccess")
//                NotificationCenter.default.post(name: Notification.Name("saveImageOnPrintSuccess"), object: nil)
//                //            }
//                self.viewRemoveBtnAr.isHidden = true
//                is_Ar_Video_Available = false
//                //            SingletonClass.sharedInstance.showRatingsPopup()
//            }
//        }
//        ispaperjam = false
//        isOutOfPaperCheck = false
//        ispaperMisfeed = false
//        issomethingWentWrong = false
//        isCoolingPrinter = false
//        isBatterylow = false
//        iscommunicationHiccup = false
//    }
//    @objc func handlePrintCompletion()
//    {
//        // We can enable the print button now
//        self.lblPrint.text = "Print".localisedString()
//        self.lblPrint.textColor = UIColor.white
//        
//        self.btnPrint.isEnabled = true
//        self.checkPrinterStatus()
//        
//        //Change by dhanraj to check whether overlayStickerPresent ot not.
//        
//        //        else
//        //        {
//        // If current image is given for printing and user stays on this screen onl, then pop the vc when print is completed.
//        // Need this if else, otherwise user will automatically be redirected to home if he is middle of editing another image and printing gets completed.
//        
//        if isCurrentImagePrinting == true
//        {
//            if self.viewArVideoAttachedIcon.isHidden == false || is_Ar_Video_Available
//            {
//                self.showOverlayCompletion(true)
//            }
//            else{
//                if !UserDefaults.standard.bool(forKey: "overlayNormalPrint") && IS_PRINT_ERROR == false {
//                    prepareChildOverlayView()
//                }else{
//                    self.navigationController?.popViewController(animated: true)
//                }
//            }
//        }
//        else
//        {
//            // Do nothing, user is editing another image
//        }
//        
//    }
//    
//    @objc func dismissStickerOverlayDismiss(){
//        
////        SingletonClass.sharedInstance.showRatingsPopup()
//        
//        self.navigationController?.popViewController(animated: true)
//    }
//    
//    @objc func stopLoader(){
//        
//        customLoader.hideActivityIndicator()
//    }
//    
//    @objc func stopPrintGIF()
//    {
//        //NotificationCenter.default.post(name: Notification.Name("notificationToEndTimer"), object: nil)
//        self.btnCross.isHidden = false
//        
//    }
//    
//    func startLoader()  {
//        customLoader.showActivityIndicator(viewController: self.view)
//    }
//    
//    func showGifLoader()
//    {
//        self.startAnimationOnLabel()
//        self.viewPrintingGIF.isHidden = false
//        if(self.viewEdit.alpha == 1.0){
//            self.viewEdit.alpha = 0.5
//        }
//        
//        
//        self.showPrintingLabel()
//        
//        // Disable print button once printing is started
//        self.btnPrint.isEnabled = false
//        
//    }
//    
}

