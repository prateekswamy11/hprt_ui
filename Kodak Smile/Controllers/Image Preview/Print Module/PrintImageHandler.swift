//
//  PrintImageHandler.swift
//  Kodak Smile
//
//  Created by maximess142 on 29/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation
import Alamofire

var noOfCopies = 1
class PrintImageHandler: NSObject
{
    var printImageDataArray = [[String : Any]]()
    var countOfCopies = Int()
    var isApplyForAll = Bool()
    var timer = Timer()
    var arrayPrintTypeCodes = [PrintTypeCode]()
    
//    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK:- #startPrinting
    func startPrinting()
    {
        if (appDelegate().isConnectedToPrinter == true)
        {
            appDelegate().isPrintingError = false
            appDelegate().isPrintingInProgress = true // Changing this var when we get print ack for 'image receive' from printer
            appDelegate().numberOfPrintedImages = 0
            appDelegate().totalCountOfPrint = 0
            appDelegate().isConnectedToPrinter = true
            if (self.printImageDataArray.count > 0)
            {
                appDelegate().arrPrintTypeCode.append(contentsOf: self.arrayPrintTypeCodes)
            }
            if (appDelegate().arrPrintTypeCode.count == 0)
            {
                appDelegate().arrPrintTypeCode.append(PrintTypeCode.simple)
            }
            for index in 0..<self.printImageDataArray.count
            {
                let imagePrint =  self.printImageDataArray[index]["image"] as! UIImage
                // Start creating image print object
                let obj = RequestOperation(type: RequestType.printImage, ResponseDelegate: appDelegate(), PrintResponseDelegate: appDelegate())
                obj.img = UIImageJPEGRepresentation(imagePrint, 1.0)
                print("obj.img.length = \(obj.img)")
                obj.TagNumber = index+1
                obj.arrayPrintType = appDelegate().arrPrintTypeCode
                if self.isApplyForAll
                {
                    obj.TotalNumberOfCopiesAddes = self.countOfCopies
                    obj.no_Of_Copy = self.countOfCopies
                    obj.totalCopies = self.countOfCopies
                    appDelegate().totalCountOfPrint = appDelegate().totalCountOfPrint + self.countOfCopies
                }else
                {
                    if let copies = self.printImageDataArray[index]["copies"] as? Int
                    {
                        obj.TotalNumberOfCopiesAddes = copies
                        obj.no_Of_Copy = copies
                        obj.totalCopies = copies
                        appDelegate().totalCountOfPrint = appDelegate().totalCountOfPrint + copies
                    }
                }
                print(obj.arrayPrintType!)
                appDelegate().printCopies = self.countOfCopies
                print("obj.no_Of_Copy = \(obj.no_Of_Copy)")
                PrinterQueueManager.RequestArray.append(obj)
                print("PrinterQueueManager.RequestArray.count = \(PrinterQueueManager.RequestArray)")
            }
        }
    }
}
