//
//  ImagePreview+Printing.swift
//  Polaroid MINT
//
//  Created by maximess142 on 04/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import UIKit

// Delegate to check error popup ,and hide or show Ar print overlay and rating popup
protocol GoToErrorPopUpDelegate {
    func showOverlayCompletion(_ status:Bool)
}

var ispaperMisfeed = false
var issomethingWentWrong = false
var ispaperjam = false
var checkCommunicationHiccup = false
var iscommunicationHiccup = false
var isOutOfPaperCheck = false
var isScanningAR = false        // Check for out of paper popup not to show on AR flow.
var isOutOfPpr = false
var isBatterylow = false
var isCoolingPrinter = false
var Error_delegate : GoToErrorPopUpDelegate?

extension ImagePreviewViewController : PrintErrorProtocol
{
    func receivedPrintingError(errorCode: Int)
    {
        customLoader.hideActivityIndicator()
        isCurrentImagePrinting = false
        let errorHandlerHelper = PrintErrorCodesHandler()
        errorHandlerHelper.currentViewController = self
        errorHandlerHelper.showErrorMsgWithCode(errorCode: errorCode)
        // Remove the delegate when error is shown, assign it again when next print command is given. This is done because we are constantly hitting GetAccessoryInfo, so it will show the error again and again which is undesired. We require the error to show only when print is given
        PrinterQueueManager.sharedController.errorDelegate = nil
    }
}

class PrintErrorCodesHandler
{
    var currentViewController = UIViewController()
    func showErrorMsgWithCode(errorCode : Int)
    {
        switch errorCode
        {
        case 1 :
            let busyVC = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpHeldOnViewController") as! PopUpHeldOnViewController
            showPopupViewController(viewController: busyVC)
            break
        case 2 :
            ispaperjam = true
            let paperJamVC = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpPaperJamInstructionViewController") as! PopUpPaperJamInstructionViewController
            showPopupViewController(viewController: paperJamVC)
            Error_delegate?.showOverlayCompletion(true)
            break
        case 3:
            if isScanningAR{
                // Don't show pop up
                isScanningAR = false
                isOutOfPpr = true
            }else{
                if !isPrintDoneCheckOutOfPaper && noOfCopies != 0
                {
                    isOutOfPpr = false
                    let outOfPaperVC = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopupOutOfPaperViewController") as! PopupOutOfPaperViewController
                    isOutOfPaperCheck = true
                    if !(appDelegate().getCurrentViewController()!.childViewControllers.last is  OnboardingPageViewController) && appDelegate().isGivenToPrint {
                        if !(appDelegate().getCurrentViewController()!.childViewControllers.last is  ConnectivityViewController) {
                            showPopupViewController(viewController: outOfPaperVC)
                            appDelegate().isGivenToPrint = false
                        }
                    }
                }
                isPrintDoneCheckOutOfPaper = false
            }
            break
        case 4:
            ispaperMisfeed = true
            let paperMismatchVC = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpPaperDetectViewController") as! PopUpPaperDetectViewController
            showPopupViewController(viewController: paperMismatchVC)
            break
        case 8 :
            isBatterylow = true
            let batteryLowVC = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpLowBatteryViewController") as! PopUpLowBatteryViewController
            showPopupViewController(viewController: batteryLowVC)
            break
        case 10 :
            isCoolingPrinter = true
            let coolingVC = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpMintCoolViewController") as! PopUpMintCoolViewController
            showPopupViewController(viewController: coolingVC)
            break
        case 15:
            issomethingWentWrong = true
            let somethingVC = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpSomeWrongViewController") as! PopUpSomeWrongViewController
            somethingVC.strDescription = "Looks like Paper Misfeed"
            showPopupViewController(viewController: somethingVC)
            break
        default:
            print("Undefined error")
            issomethingWentWrong = true
            let somethingVC = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpSomeWrongViewController") as! PopUpSomeWrongViewController
            showPopupViewController(viewController: somethingVC)
            break
        }
    }
    
    func showPopupViewController(viewController : UIViewController)
    {
        let rootViewController = appDelegate().getCurrentViewController()
        rootViewController?.view.addSubview(viewController.view)
        rootViewController?.addChildViewController(viewController)
        viewController.view.frame = CGRect(x: 0, y: 0, width: rootViewController?.view.frame.width ?? 0.0, height: rootViewController?.view.frame.height ?? 0.0)        
    }
}
