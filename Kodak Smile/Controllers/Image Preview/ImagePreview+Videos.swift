//
//  ImagePreview+Videos.swift
//  Kodak Smile
//
//  Created by maximess142 on 19/04/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import Photos

extension ImagePreviewViewController
{
    func getVideoAssetsAlbumWise() -> PHFetchResult<PHAsset>?
    {
        var fetchResult: PHFetchResult<PHAsset>?
        fetchResult = PHAsset.fetchAssets(with: .video, options: nil)
        return fetchResult
    }
}
