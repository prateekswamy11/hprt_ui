//
//  ImagePreviewPrintingSetupExtension.swift
//  Kodak Smile
//
//  Created by maximess152 on 27/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation

extension ImagePreviewViewController {
    
    @objc func isPrintingStarted() {
        self.initialisePrintingVars()
    }
    
    func startAnimationOnLabel() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        if blurEffectView.alpha == 1.0 {
            blurEffectView.alpha = 0.7
        }
        blurEffectView.frame = imageViewPreview.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imgViewPrintingGIF.addSubview(blurEffectView)
        if !self.labelPositionisDown {
            self.lblBottomConstraint.constant = -size
            self.view.layoutIfNeeded()
            self.labelPositionisDown = !(self.labelPositionisDown)
        }
        customLoader.hideActivityIndicator()
        // TODO: Add proper animation so that text does not cut
        UIView.animate(withDuration: 20.0, animations: {
            if self.labelPositionisDown {
                self.lblBottomConstraint.constant = 20
                self.view.layoutIfNeeded()
                self.labelPositionisDown = !(self.labelPositionisDown)
            }
        }, completion: nil)
    }
    
    func stopAnimationOnLabel() {
        self.imgViewPrintingGIF.subviews.forEach({$0.removeFromSuperview()})
        self.lblPrintingLabel.isHidden = true
        self.viewPrintingGIF.isHidden = true
        self.viewEdit.alpha = 1.0
        self.btnPrint.isEnabled = true
    }
    
    func checkPrinterStatus()
    {
        if (appDelegate().isConnectedToPrinter == true)
        {
            self.btnPrint.isSelected = true
            self.imgPrintStatus.image = #imageLiteral(resourceName: "greenIcon")
        }
        else if (appDelegate().isConnectedToPrinter == false)
        {
            self.btnPrint.isSelected = false
            self.imgPrintStatus.image = #imageLiteral(resourceName: "redIcon")
            if UserDefaults.standard.value(forKey:"targetId") != nil && finalCopiesToCheckdelete == 0{
                //  Delete target from EasyAR server,\ if printer is not connected to device ,
                if WebserviceModelClass().isInternetAvailable() {
                    SingletonClass.sharedInstance.healthCheck_EasyARForDeleteTarget()
                    customLoader.hideActivityIndicator()
                }else {
                    customLoader.hideActivityIndicator()
                  //  self.showNoInternetPopup()
                }
            }
        }
        if appDelegate().isPrintingInProgress == true {
            self.lblPrint.text = "Printing...".localisedString()
            self.lblPrint.textColor = UIColor.white.withAlphaComponent(0.4)
            isOutOfPaper = false
        }
        else
        {
            self.lblPrint.text = "Print".localisedString()
            self.lblPrint.textColor = UIColor.white.withAlphaComponent(1.0)
            self.stopAnimationOnLabel()
        }
    }
    
    func initialisePrintingVars()
    {
        if (appDelegate().isConnectedToPrinter == true)
        {
            if (appDelegate().isPrintingInProgress == true)
            {
                // Disable print button once printing is started
                self.btnPrint.isEnabled = false
                self.btnPrint.isSelected = false
                self.showPrintingLabel()
            }
        }
    }
    
    func getEditedImgWithoutSlider() -> UIImage
    {
        self.sliderFilterOpacity.isHidden = true
        self.lblSliderValue.isHidden = true
        self.gradientViewLeftToRight.isHidden = true
        let finalImage = self.captureImageInView(inView: self.viewImageContainer)
        // By sushant - filter selection variable used to check if filter is selected or not, if selected show opacity slider while saving the image, if other options selected, don't show opacity slider.
        if self.isFilterSelected{
            self.sliderFilterOpacity.isHidden = false
            self.lblSliderValue.isHidden = false
            self.gradientViewLeftToRight.isHidden = false
        }
        return finalImage!
    }
    
    func shareImage(image : UIImage)
    {
        // set up activity view controllerƒ
        let imageToShare = [ image ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view  //self.view // so that iPads won't crash
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func areEqualImages(img1: UIImage, img2: UIImage) -> Bool {
        guard let data1 = UIImagePNGRepresentation(img1) else { return false }
        guard let data2 = UIImagePNGRepresentation(img2) else { return false }
        return data1 == data2
    }
    
    func checkIfComeFromMultiPrint()
    {
        self.btnPrint.isHidden = self.isComeFromMultiPrintScreen == true ? true : false
        self.lblPrint.isHidden = self.isComeFromMultiPrintScreen == true ? true : false
        self.imgPrintStatus.isHidden = self.isComeFromMultiPrintScreen == true ? true : false
        self.btnCross.isHidden = self.isComeFromMultiPrintScreen == true ? true : false
        self.btnOptions.isHidden = self.isComeFromMultiPrintScreen == true ? true : false
        self.mainEditOptionsArray = self.isComeFromMultiPrintScreen == true ? ["Filter", "Edit", "Style"] : ["Filter", "Edit", "Style", "AR"]
        if self.isComeFromMultiPrintScreen{
            self.showMultiPrintSubviews()
        }else{
            self.hideMultiPrintSubviews()
        }
    }
    
    func showPrintingLabel()
    {
        self.lblPrint.text = "Printing...".localisedString()
        isOutOfPaper = false
        self.lblPrint.textColor = UIColor.white.withAlphaComponent(0.4)
        self.imgPrintStatus.image = #imageLiteral(resourceName: "greenIcon")
    }
    
    func setRandomPrintingText()
    {
        let randomIndex = Int(arc4random_uniform(UInt32(printTextArray.count)))
        let printText = printTextArray[randomIndex]
        if UIScreen.main.bounds.width <= 320 {
            self.lblPrintingLabel.font = UIFont.boldSystemFont(ofSize: 32)
            self.lblPrintingLabel.sizeToFit()
            size = 120
        }
        self.lblPrintingLabel.isHidden = false
        self.lblPrintingLabel.text = printText.localisedString()
        self.lblPrintingLabel.adjustsFontSizeToFitWidth = true
        self.lblPrintingLabel.minimumScaleFactor = 0.2
    }
    
    func CheckImageEdited(){
        //        self.paraEditObject.currentEditOptionIndex == 0 &&
        if  self.paraEditObject.selectedFilterIndex == 0 && self.paraEditObject.selectedSubEditOption == 0 && self.paraEditObject.selectedSubStyleOption == 0 && self.paraEditObject.selectedFramesIndex == 0 && self.paraEditObject.selectedStickerCatIndex == 0 && self.paraEditObject.selectedStickerIndex == -1 && self.paraEditObject.selectedStickerCatString == "" && self.paraEditObject.selectedDoodleColorIndex == 0 && self.paraEditObject.selectedDoodleOption == 0 && self.paraEditObject.selectedDoodlePencilWidth == 6.0 &&  self.paraEditObject.selectedDoodleEraserWidth == 6.0 && self.paraEditObject.selectedTextFontIndex == 0 && self.paraEditObject.selectedTextColorIndex == 0 && self.paraEditObject.textString == "" && self.paraEditObject.isAdjustEnable == false && self.paraEditObject.highlightsValue == 1.0 && self.paraEditObject.shadowsValue == 0 && self.paraEditObject.contrastValue == 1.0 && self.paraEditObject.brightnessValue == 0.0 && self.paraEditObject.saturationValue == 1.0 && self.paraEditObject.warmValue == 0.5 && self.paraEditObject.rotateValue == 0.0 && self.paraEditObject.scaleZoomValue == 1.0 && self.paraEditObject.filterValue == 0 && self.paraEditObject.rotate90Value == 0.0 && self.paraEditObject.previousShadowsValue == 0.0 && self.paraEditObject.previousContrastValue == 1.0 && self.paraEditObject.previousBrightnessValue == 0.0 && self.paraEditObject.previousSaturationValue == 1.0 && self.paraEditObject.previousWarmValue == 0.5 && self.paraEditObject.previousScaleZoomValue == 1.0 && self.paraEditObject.previousrotate90Value == 0.0 && self.paraEditObject.previousHighlightsValue == 1.0 && self.paraEditObject.previousHighlightsSliderValue == 0.0 && self.paraEditObject.previousShadowsSliderValue == 0.0 && self.paraEditObject.previousContrastSliderValue == 0.0 && self.paraEditObject.previousBrightnessSliderValue == 0.0 && self.paraEditObject.previousSaturationSliderValue == 0.0 && self.paraEditObject.previousWarmSliderValue == 0.0 && self.paraEditObject.previousRotateSliderValue == 0.0{
            self.paraEditObject.isImageEdited = false
            
        }else {
            self.paraEditObject.isImageEdited = true
        }
    }
}
