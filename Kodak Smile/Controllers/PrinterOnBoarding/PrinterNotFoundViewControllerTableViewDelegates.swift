//
//  PrinterNotFoundViewControllerTableViewDelegates.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 26/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PrinterNotFoundViewControllerTableViewDelegates: NSObject {

}

extension PrinterNotFoundViewController: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.width * 0.31
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let otherCell : PrinterNotFoundTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PrinterNotFoundTableViewCell", for: indexPath) as! PrinterNotFoundTableViewCell
        
        otherCell.imgVwIcons.image = UIImage(named: iconImages[indexPath.row])
       
        otherCell.lblSuggestion.text = listNames[indexPath.row].localisedString()

        return otherCell
    }
}
