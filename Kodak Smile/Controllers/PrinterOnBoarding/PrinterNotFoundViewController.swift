//
//  PrinterNotFoundViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 26/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CopilotAPIAccess

protocol showDevicePicker
{
    func showBluetoothDevicePicker()
}

class PrinterNotFoundViewController: UIViewController {
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblNavigationTop: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnContactSupportBottom: NSLayoutConstraint!
    
    @IBOutlet weak var lblPrinterNotFoundUpper: UILabel!
    @IBOutlet weak var lblPrinterNotFound: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnSearchAgain: UIButton!
    @IBOutlet weak var btnContactSupport: UIButton!
    @IBOutlet weak var tblHelp: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    
    var delegate: showDevicePicker?
    
    let iconImages = ["printerTurnedOn","plugPowerSource","printerDevice"]
   // let iconImages = [#imageLiteral(resourceName: "printerTurnedOn"),#imageLiteral(resourceName: "plugPowerSource"),#imageLiteral(resourceName: "printerDevice")]
   // let bgColors = [#colorLiteral(red: 0.4039215686, green: 0.7529411765, blue: 0.4666666667, alpha: 1), #colorLiteral(red: 0.9450980392, green: 0.3450980392, blue: 0.2862745098, alpha: 1), #colorLiteral(red: 0, green: 0.5764705882, blue: 0.8392156863, alpha: 1)]
    
    let listNames = ["Make sure your printer is turned on","Plug your printer into a power source                  ", "Place your printer close to your mobile device"]
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
       // self.detectIphone5sDevice()
        self.tblHelp.tableFooterView = UIView(frame: .zero)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if comeFromOnboardingPageVC == "onboardingVC" || goToQuickTipsOnboarding == "quickTipsOnboarding"{
            self.willMove(toParentViewController: nil)
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
            
            self.btnBack.isHidden = false
        }else{
            self.btnBack.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if comeFromOnboardingPageVC == "onboardingVC"{
            self.btnBack.isHidden = true
        }else{
            self.btnBack.isHidden = false
        }
    }
    
    //Assign values to fields.
    func localizedStrings() {
        self.lblPrinterNotFoundUpper.text = "We couldn't find".localisedString()
        self.lblPrinterNotFound.text = "your printer".localisedString()
        
        self.btnSkip.titleLabel?.text = "Skip".localisedString()
        self.btnSearchAgain.titleLabel?.text = "search again".localisedString()
        self.btnContactSupport.titleLabel?.text = "Contact support".localisedString()
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblNavigationTop.constant = 30.0
            constraintBtnContactSupportBottom.constant = 20.0
        }else{
            print("Unknown")
        }
    }
    
    // MARK: - Action Methods
    @IBAction func backBtnClicked(_ sender:Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnskipClicked(_ sender: Any)
    {
        let tutorialSkipAnalyticsEvent = TutorialSkipAnalyticsEvent(screenNumber:"onboarding_skipped")
        Copilot.instance.report.log(event: tutorialSkipAnalyticsEvent)
        UserDefaults.standard.set(true, forKey: "onboardingSkipped")
        if comeFromOnboardingPageVC == "onboardingVC"
        {
            if SingletonClass.sharedInstance.actionInitiatedByUser {
                let quickTipsVC = storyboards.QuickTips.instantiateViewController(withIdentifier: "QuickTipsViewController") as! QuickTipsViewController
                self.navigationController?.pushViewController(quickTipsVC, animated: true)
            } else {
                let galleryVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(galleryVC, animated: true)
            }
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnSearchAgainClicked(_ sender: Any)
    {
        self.willMove(toParentViewController: nil)
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
        delegate?.showBluetoothDevicePicker()
    }
    
    @IBAction func btnContactSupportClicked(_ sender: Any)
    {
        
        Copilot.instance.report.log(event: ContactSupportAnalyticsEvent(
            supportCase: "I need help", thingId: "", screenName: AnalyticsEventName.Screen.findYourDevice.rawValue))
        MailComposer().openContactMailer(viewController: self)
    }
}
