//
//  OnboardingPageViewController.swift
//  Polaroid MINT
//
//  Created by MacMini001 on 29/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CopilotAPIAccess
//MARK: - Variables -
var subViewControllers:[UIViewController] = {
    return[
        storyboards.printerOnBoardingStoryboard.instantiateViewController(withIdentifier: "OnBoarding_OneViewController") as! OnBoarding_OneViewController,
        storyboards.printerOnBoardingStoryboard.instantiateViewController(withIdentifier: "OnBoarding_TwoViewController") as! OnBoarding_TwoViewController,
        storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController,
        storyboards.printerOnBoardingStoryboard.instantiateViewController(withIdentifier: "LoadPrinterViewController") as! LoadPrinterViewController,
        storyboards.printerOnBoardingStoryboard.instantiateViewController(withIdentifier: "TakePictureViewController") as! TakePictureViewController
    ]
}()
var comeFromOnboardingPageVC = String()

class OnboardingPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    let appearance = UIPageControl.appearance()
    
    //MARK: - View LifeCycle Functions -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        setViewControllers([subViewControllers[0]], direction: .forward, animated: true, completion: nil)
        let tutorialStartedAnalyticsEvent = TutorialEndedAnalyticsEvent(screenName: "quicktips_started")
        Copilot.instance.report.log(event: tutorialStartedAnalyticsEvent)
//        Copilot.instance.report.log(event: OnboardingStartedAnalayticsEvent(flowID: "fromPush"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addingNotificationObservers()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        comeFromOnboardingPageVC = ""
    }
    
    //MARK: - Functions to trigger notifications' functions -
    @objc func goTofindPrinterPage()
    {
        setViewControllers([subViewControllers[2]], direction: .forward, animated: true, completion: nil)
    }
    
    @objc func goToCameraVC()
    {
        let cameraVC = storyboards.cameraStoryboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
        cameraVC.comeFromStr = "Onboarding"
        self.navigationController?.pushViewController(cameraVC, animated: true)
    }
    
    @objc func goToLoadPrinterScreen()
    {
        setViewControllers([subViewControllers[3]], direction: .forward, animated: true, completion: nil)
    }
    
    @objc func goToTakePictureVC()
    {
        setViewControllers([subViewControllers[4]], direction: .forward, animated: true, completion: nil)
    }
    
    //MARK: - Functions -
    
    func addingNotificationObservers()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(goTofindPrinterPage), name: Notification.Name("notificationToFindPrinter"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(goToCameraVC), name: Notification.Name("notificationToShowCameraScreen"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(goToTakePictureVC), name: Notification.Name("notificationToShowTakePictureVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(goToLoadPrinterScreen), name: Notification.Name("notificationToLoadPrinterScreen"), object: nil)
        
        //flag to show if any screen comes from onboardingPageVC.
        comeFromOnboardingPageVC = "onboardingVC"
    }
    
    private func setupPageControl() {
        appearance.pageIndicatorTintColor = UIColor(red: 151/255, green: 156/255, blue: 142/255, alpha: 0.3)
        appearance.currentPageIndicatorTintColor = UIColor(red: 237/255, green: 0/255, blue: 0/255, alpha: 1.0)
        appearance.backgroundColor = UIColor.white
    }
    
    //MARK: - UIPageViewControllerDataSource -
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        self.setupPageControl()
        return subViewControllers.count
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        // Condition to hide overlay when user swipe in OnBoarding_OneViewController
        if let vc = viewController as? OnBoarding_OneViewController {
           vc.overlayViewOnPrinterOnboarding.isHidden = true
        } 
        
        let currentIndex = subViewControllers.index(of: viewController) ?? 0
        if currentIndex <= 0
        {
            return nil
        }
        return subViewControllers[currentIndex-1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = subViewControllers.index(of: viewController) ?? 0
        if currentIndex >= subViewControllers.count-1
        {
            return nil
        }
        return subViewControllers[currentIndex+1]
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        let currentIndex = subViewControllers.index(of: (pageViewController.viewControllers?.first)!)
        return currentIndex!
    }
    
}
