//
//  PrinterNotFoundTableViewCell.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 26/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PrinterNotFoundTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgVwIcons: UIImageView!
    @IBOutlet weak var lblSuggestion: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
