//
//  OnBoarding_TwoViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 26/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CopilotAPIAccess


class OnBoarding_TwoViewController: UIViewController {
    
//    let attributedString = NSMutableAttributedString(string: "Don’t have a printer yet? Kodakphotoplus.com".localisedString(), attributes: [
//        .font: UIFont.systemFont(ofSize: 12.0, weight: .regular),
//        .foregroundColor: UIColor(white: 1.0, alpha: 1.0),
//        .kern: 0.0
//        ])
   
    let attributedString = NSMutableAttributedString(string: "Don’t have a printer yet? Kodakphotoplus.com".localisedString())
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblNavigationTop: NSLayoutConstraint!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var constraintImgSmileWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintImgSmileHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnDontHaveBottom: NSLayoutConstraint!
    
    @IBOutlet weak var lblWelComeText: UILabel!
    @IBOutlet weak var lblConnectPrinter: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnFindPrinter: UIButton!
    @IBOutlet weak var btnDontHave: UIButton!
    @IBOutlet weak var lblDontHave: UILabel!
    @IBOutlet weak var lblClickHere: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
      
       // self.detectIphone5sDevice()
        // Do any additional setup after loading the view.
        
      //  self.lblConnectPrinter.text = "Before we get started,  let’s connect your printer".localisedString()
        
        attributedString.addAttributes([
            .font: UIFont.systemFont(ofSize: 12.0, weight: .bold),
            .foregroundColor: UIColor.init(red: 10, green: 165, blue: 190)
            ], range: NSRange(location: 26, length: 18))
    }
    
    override func viewWillAppear(_ animated: Bool) {
          self.localizedStrings()
    }
    //Assign values to fields.
    func localizedStrings() {
        // self.lblWelComeText.text = "Welcome".localisedString() + " \(userName)xyz!"
      
        if let userName = UserDefaults.standard.value(forKey: "userNameLoggedIn")
        {
            self.lblUserName.text = "\(String(describing: userName))!"
        }
        else
        {
            self.lblUserName.text = ""
        }
        
        
        self.lblWelComeText.text = "Bluetooth on? Great!".localisedString()
        self.lblConnectPrinter.text = "Now let’s get your printer connected".localisedString()
        self.btnSkip.setTitle("Skip".localisedString(), for: .normal)
        self.btnFindPrinter.setTitle("Find My Printer".localisedString(), for: .normal)
//        self.btnFindPrinter.setTitle("Find My Printer".localisedString(), for: .normal)
//        self.lblDontHave.text = "Don’t have a printer yet?".localisedString()
        
        self.lblDontHave.attributedText = attributedString
    }
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblNavigationTop.constant = 30.0
            constraintImgSmileWidth.constant = 110.0
            constraintImgSmileHeight.constant = 125.0
            constraintBtnDontHaveBottom.constant = 20.0
        }else{
            print("Unknown")
        }
    }
    
    // MARK: - Action Methods
    @IBAction func backBtnClicked(_ sender:Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnskipClicked(_ sender: Any)
    {
        let tutorialSkipAnalyticsEvent = TutorialSkipAnalyticsEvent(screenNumber:"onboarding_skipped")
        Copilot.instance.report.log(event: tutorialSkipAnalyticsEvent)
        UserDefaults.standard.set(true, forKey: "onboardingSkipped")
        
        if SingletonClass.sharedInstance.actionInitiatedByUser {
            let quickTipsVC = storyboards.QuickTips.instantiateViewController(withIdentifier: "QuickTipsViewController") as! QuickTipsViewController
            self.navigationController?.pushViewController(quickTipsVC, animated: true)
        } else {
            let galleryVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(galleryVC, animated: true)
        }
    
    }
    
    @IBAction func btnFindPrinterClicked(_ sender: Any)
    {
        NotificationCenter.default.post(name: Notification.Name("notificationToFindPrinter"), object: nil)
    }
    
    @IBAction func btnDontHaveClicked(_ sender: Any)
    {
        if !WebserviceModelClass().isInternetAvailable()
        {
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            //comeFromStrNoInternetSocialMedia = ""
            self.present(alertPopUp, animated: true, completion: nil)
        }
        else
        {
            let buyPrinterVC = storyboards.printerOnBoardingStoryboard.instantiateViewController(withIdentifier: "BuyPrinterViewController") as! BuyPrinterViewController
            
            self.navigationController?.pushViewController(buyPrinterVC, animated: true)
        }
    }

}
