//
//  LoadPrinterViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 26/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class LoadPrinterViewController: UIViewController {

    //MARK: - Outlets -
    @IBOutlet weak var constraintLblNavigationTop: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnGoToHomeBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintTopLoaderImgView: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightLoaderImgView: NSLayoutConstraint!
    
    @IBOutlet weak var lblLoadPrinterUpper: UILabel!
    @IBOutlet weak var lblLoadPrinter: UILabel!
    @IBOutlet weak var lblStep: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnPrintPicture: UIButton!
    @IBOutlet weak var btnGoToHome: UIButton!
    @IBOutlet weak var imgViewLoadPrinter: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
       // self.detectIphone5sDevice()
        self.showGifLoader()
        // Do any additional setup after loading the view.
    }
    
    //Assign values to fields.
    func localizedStrings() {
        self.lblLoadPrinterUpper.text = "Load your printer".localisedString()+" "+"with".localisedString()
        self.lblLoadPrinter.text = "KODAK SMILE ZINK\nPhoto Paper".localisedString()
//        self.lblStep.text = "Open the top and place the pack of paper inside with the blue sheet on the bottom".localisedString()
        self.lblStep.text = "Open the top and place the pack of KODAK SMILE ZINK Photos Paper inside, blue sheet facing down".localisedString()
        self.btnSkip.setTitle("Skip".localisedString(), for: .normal)
        self.btnPrintPicture.setTitle("Print My First Picture".localisedString(), for: .normal)
        self.btnGoToHome.setTitle("Go to Homepage".localisedString(), for: .normal)
        
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblNavigationTop.constant = 30.0
            constraintBtnGoToHomeBottom.constant = 20.0
            constraintTopLoaderImgView.constant = 0.0
            constraintHeightLoaderImgView.constant = 150.0
        }else{
            print("Unknown")
        }
    }
    
    func showGifLoader()
    {
        let gif = UIImage(gifName: "paper-load_17_12.gif")
        imgViewLoadPrinter.setGifImage(gif)
        imgViewLoadPrinter.startAnimatingGif()
        
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { timer in
        }
    }
    
    // MARK: - Action Methods
    @IBAction func backBtnClicked(_ sender:Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnskipClicked(_ sender: Any)
    {
        let tutorialSkipAnalyticsEvent = TutorialSkipAnalyticsEvent(screenNumber:"onboarding_skipped")
        Copilot.instance.report.log(event: tutorialSkipAnalyticsEvent)
        UserDefaults.standard.set(true, forKey: "onboardingSkipped")
        
        if SingletonClass.sharedInstance.actionInitiatedByUser {
            let quickTipsVC = storyboards.QuickTips.instantiateViewController(withIdentifier: "QuickTipsViewController") as! QuickTipsViewController
            self.navigationController?.pushViewController(quickTipsVC, animated: true)
        } else {
            let galleryVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(galleryVC, animated: true)
        }
    
    }
    
    @IBAction func btnPrintMyPictureClicked(_ sender: Any)
    {
    UserDefaults.standard.set(true, forKey: "onboardingSkipped")
        // code for sending user to edit screen with latest pic : Smile Sprint - 23/03/2020
    if getRecentPic().checkPhotoLibraryPermission(){
        let imagePreviewVC = storyboards.imgPreviewStoryboard.instantiateViewController(withIdentifier: "ImagePreviewViewController") as! ImagePreviewViewController
        getRecentPic().queryLastPhoto(resizeTo: nil){
            image in
            print(image)
            if image != nil {
                imgMainEdit = image!
                imagePreviewVC.isCameFromOnboarding = true
                isComeFrom = "LoadPrinter"
                self.navigationController?.pushViewController(imagePreviewVC, animated: true)
            }else{
                let HomeVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(HomeVC, animated: true)
            }
        }
        
    } else {
            let HomeVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
              self.navigationController?.pushViewController(HomeVC, animated: true)
        }
        
       
       
       
    }
    
    @IBAction func btnGoToHomePageClicked(_ sender: Any)
    {
        UserDefaults.standard.set(true, forKey: "onboardingSkipped")
        
        let galleryVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController")
//        self.navigationController?.pushViewController(galleryVC, animated: true)
        comeFromOnboardingPageVC = ""
//        self.makeRootViewController(viewcontroller: galleryVC)
        self.navigationController?.pushViewController(galleryVC, animated: false)
    }
}
