//
//  TakePictureViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 26/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import AVFoundation
import CopilotAPIAccess


class TakePictureViewController: UIViewController, CameraPermissinNativePopupDelegate {
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblNavigationTop: NSLayoutConstraint!
    @IBOutlet weak var constraintImgSmileWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintImgSmileHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintBtntakePictureBottom: NSLayoutConstraint!
    
    @IBOutlet weak var lblMessageLower: UILabel!
    @IBOutlet weak var lblSayChees: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnTakePicture: UIButton!
    var takePictureBtntab = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
       // self.detectIphone5sDevice()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblMessage.text = "Before we print, get creative:\nchoose an angle, check the lighting,\nit's your masterpiece.".localisedString()
    }
    //Assign values to fields.
    func localizedStrings() {
        self.lblSayChees.text = "Capture Your".localisedString()
        self.lblMessageLower.text = "SMILE Images".localisedString()
        
      
        self.btnSkip.setTitle("Skip".localisedString(), for: .normal)
       self.btnTakePicture.setTitle("Take a Picture".localisedString(), for: .normal)
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblNavigationTop.constant = 30.0
            constraintImgSmileWidth.constant = 110.0
            constraintImgSmileHeight.constant = 125.0
            constraintBtntakePictureBottom.constant = 30.0
        }else{
            print("Unknown")
        }
    }
    
    
    // MARK: - Action Methods
    @IBAction func backBtnClicked(_ sender:Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnskipClicked(_ sender: Any)
    {
        let tutorialSkipAnalyticsEvent = TutorialSkipAnalyticsEvent(screenNumber:"onboarding_skipped")
        Copilot.instance.report.log(event: tutorialSkipAnalyticsEvent)
        UserDefaults.standard.set(true, forKey: "onboardingSkipped")
        
        if SingletonClass.sharedInstance.actionInitiatedByUser {
            let quickTipsVC = storyboards.QuickTips.instantiateViewController(withIdentifier: "QuickTipsViewController") as! QuickTipsViewController
            self.navigationController?.pushViewController(quickTipsVC, animated: true)
        } else {
            let galleryVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(galleryVC, animated: true)
        }

    }
    
    @IBAction func btnTakePictureClicked(_ sender: Any)
    {
        
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            
            DispatchQueue.main.async {
                //already authorized
                UserDefaults.standard.set(true, forKey: "onboardingSkipped")
                let cameraVC = storyboards.cameraStoryboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                comeFromOnboardingPageVC = ""
                self.navigationController?.pushViewController(cameraVC, animated: false)
                let tutorialEndedAnalyticsEvent = TutorialEndedAnalyticsEvent(screenName: "quicktips_ended")
                Copilot.instance.report.log(event: tutorialEndedAnalyticsEvent)
            }
           
            
        } else  {
            DispatchQueue.main.async {
                let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpCameraAccessRequiredViewController") as! PopUpCameraAccessRequiredViewController
                alertPopUp.delegate = self
                self.present(alertPopUp, animated: true, completion: nil)
            }
        }
        
    }
    
    func showCameraPermissionPopup() {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
            DispatchQueue.main.async(execute: {
                UserDefaults.standard.set(true, forKey: "onboardingSkipped")
                let cameraVC = storyboards.cameraStoryboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                self.navigationController?.pushViewController(cameraVC, animated: false)
            })
        }else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    DispatchQueue.main.async(execute: {
                        UserDefaults.standard.set(true, forKey: "onboardingSkipped")
                        let cameraVC = storyboards.cameraStoryboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                        self.navigationController?.pushViewController(cameraVC, animated: false)
                    })
                } else {
                    //access denied
                    if UserDefaults.standard.bool(forKey: "enableCameraAccessCount") {
                        DispatchQueue.main.async {
                            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
                            UIApplication.shared.open(settingsUrl)
                        }
                    }
                    UserDefaults.standard.set(true, forKey: "enableCameraAccessCount")
                }
            })
        }
    }
}
