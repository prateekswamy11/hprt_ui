//
//  OnBoarding_ThreeViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 26/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class OnBoarding_ThreeViewController: UIViewController {
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblNavigationTop: NSLayoutConstraint!
    @IBOutlet weak var constraintImgSmileWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintImgSmileHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblWelComeText: UILabel!
    @IBOutlet weak var lblConnectPrinter: UILabel!
    @IBOutlet weak var btnSkip: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.detectIphone5sDevice()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.localizedStrings()

    }
    
    //Assign values to fields.
    func localizedStrings() {
        if let userName = UserDefaults.standard.value(forKey: "userNameLoggedIn")
        {
            self.lblWelComeText.text = "Welcome".localisedString() + " \(String(describing: userName))!"
        }
        else
        {
            self.lblWelComeText.text = "Welcome".localisedString() + ""
        }
        self.lblConnectPrinter.text = "You’re almost ready to create fun prints! Let’s connect your printer.".localisedString()
        self.btnSkip.titleLabel?.text = "Skip".localisedString()
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblNavigationTop.constant = 30.0
            constraintImgSmileWidth.constant = 110.0
            constraintImgSmileHeight.constant = 125.0
        }else{
            print("Unknown")
        }
    }
    
    // MARK: - Action Methods
    @IBAction func backBtnClicked(_ sender:Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnskipClicked(_ sender: Any)
    {
        let tutorialSkipAnalyticsEvent = TutorialSkipAnalyticsEvent(screenNumber:"onboarding_skipped")
        Copilot.instance.report.log(event: tutorialSkipAnalyticsEvent)
        UserDefaults.standard.set(true, forKey: "onboardingSkipped")
        
        if SingletonClass.sharedInstance.actionInitiatedByUser {
            let quickTipsVC = storyboards.QuickTips.instantiateViewController(withIdentifier: "QuickTipsViewController") as! QuickTipsViewController
            self.navigationController?.pushViewController(quickTipsVC, animated: true)
        } else {
            let galleryVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(galleryVC, animated: true)
        }
    
    }
}
