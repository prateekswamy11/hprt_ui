//
//  BuyPrinterViewController.swift
//  Kodak Smile
//
//  Created by MacMini001 on 18/01/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import WebKit

class BuyPrinterViewController: UIViewController{

    //MARK: - Outlets -
    @IBOutlet weak var webView: WKWebView!
    
    
    //MARK: - Variables -
    var url = "https://www.amazon.com/dp/B07NX47JPK?ref=myi_title_dp"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createWebView()
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
//        self.loaderIndicator.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createWebView() {
//        self.view.isUserInteractionEnabled = false
        customLoader.showActivityIndicator(viewController: self.view)
        let URL = NSURL(string: url)
        let requestURL = NSURLRequest(url: URL! as URL)
        self.webView.load(requestURL as URLRequest)
        self.webView.contentMode = .scaleAspectFit
    }
    
    // MARK: - IBActions -
    @IBAction func backBtnClicked(_ sender:Any)
    {
        if webView.isLoading {
            webView.stopLoading()
        }
        self.webView.uiDelegate = nil
        self.webView = nil
        self.navigationController?.popViewController(animated: true)
    }
}
extension BuyPrinterViewController : WKUIDelegate,WKNavigationDelegate
{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        customLoader.hideActivityIndicator()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        customLoader.stopGifLoader()
    }
}
