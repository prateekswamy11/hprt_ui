//
//  OnBoarding_OneViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 26/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import Photos
import CopilotAPIAccess


class OnBoarding_OneViewController: UIViewController {
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblNavigationTop: NSLayoutConstraint!
    @IBOutlet weak var constraintImgSmileWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintImgSmileHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblWelComeText: UILabel!
    @IBOutlet weak var lblConnectPrinter: UILabel!
    @IBOutlet weak var lblTurnOn: UILabel!
    @IBOutlet weak var lblBTText: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var overlayViewOnPrinterOnboarding: UIView!
    var checkPhotoPermissionPopUp = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.detectIphone5sDevice()
        // Do any additional setup after loading the view.
        self.initialiseTapGesture()
        // condition to check library permission
        if checkPhotoLibraryPermission() == true {
            
        }
        else if UserDefaults.standard.bool(forKey: "checkPhotoPermissionPopUp") == false{
            
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpPhotoAccessRequiredViewController") as! PopUpPhotoAccessRequiredViewController
            self.present(alertPopUp, animated: true, completion: nil)
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.localizedStrings()
        
    }
    
    func initialiseTapGesture(){
        // initalised tap for overlay during on boarding
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchTapped(_:)))
        self.overlayViewOnPrinterOnboarding.addGestureRecognizer(tap)
        overlayViewOnPrinterOnboarding.isHidden = false
    }
    
    func checkPhotoLibraryPermission() -> Bool {
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            print("authorized")
            
            return true
        case .denied, .restricted :
            //handle denied status
            UserDefaults.standard.set(true, forKey: "checkPhotoPermissionPopUp")
            print("denied")
            return false
        case .notDetermined:
            
            return false
        }
    }
    
    //Assign values to fields.
    func localizedStrings() {
        // self.lblWelComeText.text = "Welcome".localisedString() + " \(userName)Palash patil!"
        
        //        self.lblBTText.text = "Your phone uses BLUETOOTH to connect to your printer. Head over to your control center or settings and turn on your BLUETOOTH.".localisedString()
        
        self.lblWelComeText.text = "Welcome".localisedString()
        if let userName = UserDefaults.standard.value(forKey: "userNameLoggedIn")
        {
            self.lblUserName.text = "\(String(describing: userName))!"
        }
        else
        {
            self.lblUserName.text = ""
        }
        self.lblConnectPrinter.text = "Before we get started,\nlet’s connect your printer".localisedString()
        self.lblTurnOn.text = "Turn on your BLUETOOTH".localisedString()
        self.lblBTText.text = "Your phone uses BLUETOOTH to connect to your printer. Head over to your control center or settings and turn on your BLUETOOTH.".localisedString()
        self.btnSkip.setTitle("Skip".localisedString(), for: .normal)
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblNavigationTop.constant = 30.0
            constraintImgSmileWidth.constant = 110.0
            constraintImgSmileHeight.constant = 125.0
        }else{
            
            print("Unknown")
        }
    }
    
    // Function to handle tap gesture when user taps on overlay view
    
    @objc func touchTapped(_ sender: UITapGestureRecognizer) {
        overlayViewOnPrinterOnboarding.isHidden = true
    }
    
    
    // MARK: - Action Methods
    @IBAction func backBtnClicked(_ sender:Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnskipClicked(_ sender: UIButton)
    {
        let tutorialSkipAnalyticsEvent = TutorialSkipAnalyticsEvent(screenNumber:"onboarding_skipped")
        Copilot.instance.report.log(event: tutorialSkipAnalyticsEvent)
        UserDefaults.standard.set(true, forKey: "onboardingSkipped")
        
        if SingletonClass.sharedInstance.actionInitiatedByUser {
            let quickTipsVC = storyboards.QuickTips.instantiateViewController(withIdentifier: "QuickTipsViewController") as! QuickTipsViewController
            self.navigationController?.pushViewController(quickTipsVC, animated: true)
        } else {
            let galleryVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(galleryVC, animated: true)
        }
        
    }
    
}
