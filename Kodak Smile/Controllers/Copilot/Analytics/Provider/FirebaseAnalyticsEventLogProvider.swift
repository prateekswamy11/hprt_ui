//
//  FirebaseAnalyticsEventLogProvider.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/16/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import UIKit
import Foundation
import Firebase
import FirebaseAnalytics
import CopilotAPIAccess

class FirebaseAnalyticsEventLogProvider: EventLogProvider {
    var devGoogleFirebase = "GoogleService-Info-forFirebase-Dev"
    var prodGoogleFirebase = "GoogleService-Info-forFirebase-Prod"
    
    init() {
        if FirebaseApp.app() == nil {
            if let filePath = Bundle.main.path(forResource: prodGoogleFirebase , ofType: "plist"), let firebaseOptions = FirebaseOptions(contentsOfFile: filePath) {
                FirebaseApp.configure(options: firebaseOptions)
            }
        }
        self.updateFirebaseAnalyticsStatus(shouldLogEvents: true)
    }
    
    func enable() {
        updateFirebaseAnalyticsStatus(shouldLogEvents: true)
    }
    
    func disable() {
        updateFirebaseAnalyticsStatus(shouldLogEvents: false)
    }
    
    func setUserId(userId:String?){
        Analytics.setUserID(userId)
    }
    
    func transformParameters(parameters: Dictionary<String, String>) -> Dictionary<String, String> {
        return parameters
    }
    
    func logCustomEvent(eventName: String, transformedParams: Dictionary<String, String>) {
        Analytics.logEvent(eventName, parameters: transformedParams)
    }
    
    func logScreenLoadEvent(screenName: String, transformedParams: Dictionary<String, String>) {
        var eventParams = transformedParams
        eventParams.updateValue(screenName, forKey: AnalyticsConstants.screenNameKey)
        Analytics.logEvent(AnalyticsConstants.screenLoadEventName, parameters: eventParams)
    }
    
    var providerName: String {
        return "FirebaseAnalyticsEventLogProvider"
    }
    
    var providerEventGroups: [AnalyticsEventGroup] {
        return [.All]
    }

    private func updateFirebaseAnalyticsStatus(shouldLogEvents: Bool) {
        AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(shouldLogEvents)
    }
}
