//
//  ZipAnalyticsParameters.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/18/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation

//MARK: - Events
enum AnalyticsEventName {
    
    enum GeneralEvents: String {
        case screenLoad = "screen_load"
        case tapMenu = "tap_menu"
        case tapMenuItem = "tap_menu_item"
    }
    
    enum CustomEvents: String {
        case questionAnswered = "question_answered"
    }
    
    enum Screen: String {
        case welcome = "wel_come"
        case start
        case login
        case signUp = "sign_up"
        case home
        case multipleImagePreview = "multiple_image_preview"
        case imagePreview = "image_preview"
        case gallery
        case photo
        case findYourDevice = "find_your_device"
        case editPhoto = "edit_photo"
        case createCollage = "create_collage"
        case editCollage = "edit_collage"
        case ar_scan = "ar_scan"
        case printPreview = "print_preview"
        case camera
        case settings
        case tutorial
        case orderPaper = "order_paper"
        case updateFirmware = "update_firmware"
        case downloadFirmware = "download_firmware"
        case installFirmware = "install_firmware"
        case rectangleDetectionCroping = "rectangle_detection_croping"
        case scan
        case selectVideo = "select_video"
        case videoTrim = "video_trim"
        case videoCrop = "video_crop"
        case stickers_screen = "stickers_screen"
        case Frames_screen = "Frames_screen"
    }
}

//MARK: - Values
enum AnalyticsValue {
    
    enum EventOrigin: String {
        case app = "App"
        case user = "User"
        case server = "Server"
        case thing = "Thing"
    }
    
    enum MenuItem: String {
        case socialAccounts = "social_accounts"
        case orderPaper = "order paper"
        case legal
        case about
        case firmware
        case support
        case tips
        case logout
        case join_kodak
    }
    
    enum firmwareUpgradeComplete: String {
        case failure
        case success
    }
    
    enum errorReport: String {
        case loginFailed = "login failed"
        case accountCreationFailed = "account creation failed"
        case connectionFailed = "connection failed"
    }
    
    enum accountCreationFailed: String {
        case serverError = "Server Error"
        case userAlreadExist = "User Already Exists"
    }
    
    enum connectionFailed: String {
        case bleDeviceDisconnected = "BLE Device Disconnected"
        case noBluetooth = "No Bluetooth"
        case timeout = "Timeout"
        case connection = "Connection Failed"
    }
    
    enum thingConnetionFailedEvent: String {
        case internalError = "Internal Error"
        case missingParameters = "Missing Parameters"
    }
    
    enum signupMethod: String {
        case email
    }
    
    enum albumType: String {
        case all
        case facebook
        case instagram
        case google
        case snaptouch
        case screenshots
    }
    
    enum state: String {
        case more
        case less
    }
    
    enum byDate: String {
        case newest
        case oldest
    }
    
    enum feature: String {
        case filter
        case frames
        case stickers
        case brightness
        case temperature
        case adjust
        case blur
        case text
        case paint
    }
    
    enum format: String {
        case landscape
        case portrait
    }
    
    enum timestamp: String {
        case on
        case off
    }
    
    enum printFailureReason: String {
        case noDevice = "no device"
        case noPaper = "no paper"
    }
    
    enum collageLayout: String {
        case twoVertical = "2 verical"
        case twoHorizontal = "2 horizontal"
        case threeVertical = "3 verical"
        case threeHorizontal = "3 horizontal"
        case fourVertical = "4 verical"
        case fourHorizontal = "4 horizontal"
    }
    
    enum consumableUsage: String {
        case paper = "paper"
    }
}

