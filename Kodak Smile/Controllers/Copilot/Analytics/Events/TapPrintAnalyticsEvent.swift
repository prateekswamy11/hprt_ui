//
//  TapPrintAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/19/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess


struct TapPrintAnalyticsEvent: AnalyticsEvent {
    
    private let numOfCopies: Int?
    private let timestamp: Int
    
    init(numOfCopies:Int, timestamp :Int) {
        self.numOfCopies = numOfCopies
        self.timestamp = timestamp
    }
    
    var customParams: Dictionary<String, String> {
        var params = ["screen_name": AnalyticsEventName.Screen.printPreview.rawValue, "format": AnalyticsValue.format.portrait.rawValue]
        if let numOfCopies = self.numOfCopies {
            params.updateValue(String(numOfCopies), forKey: "num_of_copies")
        }
        if timestamp == 1 {
            params.updateValue(AnalyticsValue.timestamp.on.rawValue, forKey: "timestamp")
        } else {
            params.updateValue(AnalyticsValue.timestamp.off.rawValue, forKey: "timestamp")
        }
        return params
    }
    
    var eventName: String {
        return "tap_print"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All, .Engagement, .Main]
    }
}
