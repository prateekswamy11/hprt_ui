//
//  SaveCollageAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/20/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct SaveCollageAnalyticsEvent: AnalyticsEvent {
    
    let numOfPhotos: Int
    
    var customParams: Dictionary<String, String> {
        return ["screen_name": AnalyticsEventName.Screen.createCollage.rawValue, "num_of_photos": String(numOfPhotos)]
    }
    
    var eventName: String {
        return "save_collage"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All, .Engagement, .Main]
    }
}
