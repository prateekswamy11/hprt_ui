//
//  AccountCreationFailedAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/24/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct AccountCreationFailedAnalyticsEvent: AnalyticsEvent {
    
    let failureReason: String
    
    var customParams: Dictionary<String, String> {
        return ["failure_reason" : failureReason]
    }
    
    var eventName: String {
        return "account_creation_failed"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .Server
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All, .Engagement]
    }
}
