//
//  SortGalleryAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/20/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct SortGalleryAnalyticsEvent: AnalyticsEvent {
    
    let byDate: String
    
    var customParams: Dictionary<String, String> {
        return ["screen_name": AnalyticsEventName.Screen.gallery.rawValue, "by_date": byDate]
    }
    
    var eventName: String {
        return "sort_gallery"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
