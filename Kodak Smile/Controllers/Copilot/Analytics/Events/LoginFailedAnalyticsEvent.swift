//
//  LoginFailedAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/29/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct LoginFailedAnalyticsEvent: AnalyticsEvent {
    
    var customParams: Dictionary<String, String> {
        return [:]
    }
    
    var eventName: String {
        return "login_failed"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .Server
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
