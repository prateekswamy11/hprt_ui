//
//  TutorialStartedAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/20/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct TutorialStartedAnalyticsEvent: AnalyticsEvent {
    
    var customParams: Dictionary<String, String> {
        return ["screen_name": AnalyticsEventName.Screen.tutorial.rawValue]
    }
    
    var eventName: String {
        return "tutorial_started"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
