//
//  ForgotPasswordAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/29/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct ForgotPasswordAnalyticsEvent: AnalyticsEvent {
    
    var customParams: Dictionary<String, String> {
        return [:]
    }
    
    var eventName: String {
        return "forgot_password"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
