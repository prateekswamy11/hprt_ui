//
//  PrintFailedAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/27/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct PrintFailedAnalyticsEvent: AnalyticsEvent {
    

    let failureReason: String
    let thingID: String?
    let errorCode: String?

    var customParams: Dictionary<String, String> {
      var params = ["failure_reason": failureReason]
        if let thingID = self.thingID {
            params.updateValue(thingID, forKey: "thing_id")
        }
        if let errorCode = self.errorCode {
            params.updateValue(errorCode, forKey: "error_code")
        }
        return params
    }

    var eventName: String {
        return "print_failed"
    }

    var eventOrigin: AnalyticsEventOrigin {
        return .Thing
    }

    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
