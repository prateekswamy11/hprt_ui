//
//  ConnectAlbumAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/20/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct ConnectAlbumAnalyticsEvent: AnalyticsEvent {
    
    let albumType: String
    
    var customParams: Dictionary<String, String> {
        return ["screen_name": AnalyticsEventName.Screen.gallery.rawValue, "album_type": albumType]
    }
    
    var eventName: String {
        return "connect_album"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
