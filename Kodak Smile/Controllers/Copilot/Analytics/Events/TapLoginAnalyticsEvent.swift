//
//  TapLoginAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/29/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct TapLoginAnalyticsEvent: AnalyticsEvent {
    
    var customParams: Dictionary<String, String> {
        return [:]
    }
    
    var eventName: String {
        return "tap_login"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
