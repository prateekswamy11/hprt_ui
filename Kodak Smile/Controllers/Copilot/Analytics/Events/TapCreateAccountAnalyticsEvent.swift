//
//  TapCreateAccountAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/25/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct TapCreateAccountAnalyticsEvent: AnalyticsEvent {
    
    let screenName: String
    
    var customParams: Dictionary<String, String> {
        return ["screen_name" : screenName]
    }
    
    var eventName: String {
        return "tap_create_account"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
