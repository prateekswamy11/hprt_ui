//
//  TapFrameAnalyticsEvent.swift
//  Polaroid ZIP
//
//  Created by MAXIMESS183 on 11/10/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct TapFrameAnalyticsEvent: AnalyticsEvent {
    
    let frame : String
    let category : String
    let Frames_screen = "Frames_screen"
    
    var customParams: Dictionary<String, String> {
        return ["frame": frame, "category": category ,"screenName": Frames_screen]
    }
    
    var eventName: String {
        return "tap_frame"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .App
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
