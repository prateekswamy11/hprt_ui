//
//  SignupCustomAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/29/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct SignupCustomAnalyticsEvent: AnalyticsEvent {
    
    let dateOfBirth: String?
    let dateOfBirthKey = "date_of_birth"
    private let baseEvent = SignupAnalyticsEvent()
    
    var customParams: Dictionary<String, String> {
        var params: Dictionary<String, String> = baseEvent.customParams
        if let dateOfBirth = self.dateOfBirth {
            params[dateOfBirthKey] = dateOfBirth
        }
        return params
    }
    
    var eventName: String {
        return baseEvent.eventName
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return baseEvent.eventOrigin
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return baseEvent.eventGroups
    }
}
