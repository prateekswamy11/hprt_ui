//
//  TakePhotoAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/20/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct TakePhotoAnalyticsEvent: AnalyticsEvent {
    
    var customParams: Dictionary<String, String> {
        return ["screen_name": AnalyticsEventName.Screen.camera.rawValue]
    }
    
    var eventName: String {
        return "take_photo"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
     var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
