//
//  AlbumConnectedAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/20/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct AlbumConnectedAnalyticsEvent: AnalyticsEvent {
    
    let albumType: String
    
    var customParams: Dictionary<String, String> {
        return ["screen_name": AnalyticsEventName.Screen.gallery.rawValue, "album_type": albumType]
    }
    
    var eventName: String {
        return "album_connected"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .App
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
