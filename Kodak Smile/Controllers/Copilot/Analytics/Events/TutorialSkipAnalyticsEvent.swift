//
//  TutorialSkipAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/20/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct TutorialSkipAnalyticsEvent: AnalyticsEvent {
    
    let screenNumber: String
    
    var customParams: Dictionary<String, String> {
        return ["screen_name": AnalyticsEventName.Screen.tutorial.rawValue, "screen_number": screenNumber]
    }
    
    var eventName: String {
        return "tutorial_skip"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
