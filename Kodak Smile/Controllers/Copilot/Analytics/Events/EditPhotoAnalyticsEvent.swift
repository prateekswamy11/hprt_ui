//
//  EditPhotoAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/20/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct EditPhotoAnalyticsEvent: AnalyticsEvent {
    
    let screenName: String
    let feature: String
    let value: String
    
    var customParams: Dictionary<String, String> {
        return ["screen_name": screenName, "feature": feature]
    }
    
    var eventName: String {
        return "edit_photo"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
