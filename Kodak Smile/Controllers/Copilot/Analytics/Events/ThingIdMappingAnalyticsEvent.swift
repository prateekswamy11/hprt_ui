//
//  ThingIdMappingAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/27/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

// Event definition
struct ThingIdMappingAnalyticsEvent: AnalyticsEvent {
    let oldUniquePhysicalThingId: String
    let newUniquePhysicalThingId: String
    var customParams: Dictionary<String, String> {
        
        print("old_unique_physical_thing_id : physicalId :- \(oldUniquePhysicalThingId)")
        print("new_unique_physical_thing_id : MAC Id :- \(newUniquePhysicalThingId)")
        
        return ["old_unique_physical_thing_id": oldUniquePhysicalThingId,
                "new_unique_physical_thing_id": newUniquePhysicalThingId]
        
    }
    var eventName: String {
        return "thing_id_mapping"
    }
    var eventOrigin: AnalyticsEventOrigin {
        return .Thing
    }
    var eventGroups: [AnalyticsEventGroup] {
        return [.All, .Engagement, .Main]
    }
}
