//
//  TapStickerAnalyticsEvent.swift
//  Polaroid ZIP
//
//  Created by MAXIMESS183 on 11/10/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct TapStickerAnalyticsEvent: AnalyticsEvent {
    
    let sticker : String
    let category : String
    let stickers_screen = "stickers_screen"
    
    var customParams: Dictionary<String, String> {
        return ["sticker": sticker, "category": category ,"screenName": stickers_screen]
    }
    
    var eventName: String {
        return "tap_sticker"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .App
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
