//
//  BatteryLevelAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/27/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct BatteryLevelAnalyticsEvent: AnalyticsEvent {
    
    let value: String
    
    var customParams: Dictionary<String, String> {
        return ["value": value]
    }
    
    var eventName: String {
        return "battery_level"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .Thing
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
