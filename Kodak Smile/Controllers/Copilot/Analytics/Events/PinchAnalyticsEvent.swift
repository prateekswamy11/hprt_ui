//
//  PinchAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/20/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct PinchAnalyticsEvent: AnalyticsEvent {
    
    let state: String
    
    var customParams: Dictionary<String, String> {
        return ["screen_name": AnalyticsEventName.Screen.gallery.rawValue, "state": state]
    }
    
    var eventName: String {
        return "pinch"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
