//
//  ErrorReportAnalyticsEventErrorReportAnalyticsEvent.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/24/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct ErrorReportAnalyticsEvent: AnalyticsEvent {
    
    let errorType: String
    
    var customParams: Dictionary<String, String> {
        return ["error_type": errorType]
    }
    
    var eventName: String {
        return "error_report"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .Server
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
