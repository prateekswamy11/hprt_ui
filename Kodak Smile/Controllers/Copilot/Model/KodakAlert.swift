//
//  KodakAlert.swift
//  Kodak Smile
//
//  Created by Revital Pisman on 02/09/2018.
//  Copyright © 2018 maximess142. All rights reserved.
//

import Foundation

enum KodakAlert {
    case logoutConfirmation
}

extension KodakAlert: PopupRepresentable {
    
    var title: String? {
        var key: String?
        
        switch self {
        case .logoutConfirmation:
            key = "Logout".localisedString()
        default:
            break
        }
        
        if let key = key {
            return key
        }
        return nil
    }
    
    var message: String {
        switch self {
        case.logoutConfirmation:
            return "Are you sure you want to log out?".localisedString()
        default:
            return ""
        }
    }
}
