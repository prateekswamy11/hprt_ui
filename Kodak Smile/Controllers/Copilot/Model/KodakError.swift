//
//  KodakError.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/23/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

enum KodakError: Error {
    case server
    case communication
    case silentlogin
    case silentloginServer
    case signUp
    case signUpUserExists(existingUserMail: String)
    case login
}

extension KodakError {
    
    static func fromSilentLoginError(_ silentLoginError: LoginSilentlyError) -> KodakError {
        var error: KodakError
        switch silentLoginError {
        case .requiresRelogin:
            error = .silentlogin
        case .generalError( _):
            error = .silentlogin
        case .connectivityError( _):
            error = .communication
        }
        
        return error
    }
    
    static func fromRegisterError(_ registerError: SignupError, email:String) -> KodakError {
        var error: KodakError
        switch registerError {
        case .invalidParameters( _):
            error = .signUp
        case .userAlreadyExists:
            error = .signUpUserExists(existingUserMail: email)
        case .passwordPolicyViolation( _):
            error = .server
        default:
            error = .server
        }
        return error
    }
    
    static func fromRegisterAnonymouslyError(_ registerAnonymouslyError: SignupAnonymouslyError) -> KodakError {
        var error: KodakError
        switch registerAnonymouslyError {
        case .invalidApplicationId(debugMessage: _):
            error = .silentloginServer
        case .invalidParameters(debugMessage: _):
            error = .silentloginServer
        //Missing conset
        case .generalError(debugMessage: _):
            error = .silentloginServer
        //Server issue
        case .connectivityError(debugMessage: _):
            error = .silentloginServer
        }
        
        return error
    }
    
    static func fromElevateAnonymousError(_ elevateAnonymousError: ElevateAnonymousUserError, email:String) -> KodakError {
        var error: KodakError
        switch elevateAnonymousError {
        case .invalidEmail(debugMessage: _):
            error = .silentloginServer
        case .cannotElevateANonAnonymousUser(debugMessage: _):
            error = .silentloginServer
        case .invalidParameters(debugMessage: _):
            error = .silentloginServer
        case .requiresRelogin(debugMessage: _):
            error = .server
        case .generalError(debugMessage: _):
            error = .server
        case .passwordPolicyViolation(debugMessage: _):
            error = .server
        // server error
        case .connectivityError( _):
            error = .server
        case .userAlreadyExists(debugMessage: _):
            error = .signUpUserExists(existingUserMail: email)
        }
        
        return error
    }
    
    static func fromLoginError(_ loginError: LoginError) -> KodakError {
        var error: KodakError
        switch loginError {
        case .invalidParameters(debugMessage: _):
            error = .login
        case .generalError(debugMessage: _):
            error = .login
        case .connectivityError( _):
            error = .server
        default:
            error = .login
        }
        return error
    }
}

extension KodakError: PopupRepresentable {
    
    var title: String? {
        var key: String?
        
        switch self {
        case .silentloginServer:
            key = "Server Error".localisedString()
        case .server:
            key = "Server Error".localisedString()
        case .login:
            key = "Wrong Credentials".localisedString()
        default:
            break
        }
        
        if let key = key {
            return key
        }
        return nil
    }
    
    var message: String {
        switch self {
        case .silentloginServer:
            return "General Error".localisedString()
        case .server:
            return "Something went wrong.\nPlease try again later".localisedString()
        case .login:
            return "Please enter the correct email and\npassword and try again".localisedString()
        case .signUp:
            return "Account Creation Failed.\nPlease try again later".localisedString()
        case .signUpUserExists(let email):
            let errorMessage = "User %@ already exist.".localisedString()
            return String(format: errorMessage, email)
        case .communication:
            return "Please check your internet connectivity and try again".localisedString()
        default:
            return ""
        }
    }
}

