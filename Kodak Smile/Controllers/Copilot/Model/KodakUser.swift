//
//  KodakUser.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/24/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation


struct KodakUser {
    let id: String
    let firstName: String?
    let lastName: String?
    let email: String?
    let didAgreeToTOU: Bool
    let isAnonymous: Bool
}
