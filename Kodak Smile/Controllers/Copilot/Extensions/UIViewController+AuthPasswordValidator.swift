//
//  UIViewController+AuthPasswordValidator.swift
//  Polaroid MINT
//
//  Created by MacMini002 on 6/12/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

let minPasswordCharacters = 8

protocol AuthPasswordValidator {
    func handlePasswordFieldValidateState(_ isValidated: Bool)
    func isPasswordFieldValidated(_ password:String) -> Bool
}

extension AuthPasswordValidator where Self: UIViewController {
    
    func handlePasswordFieldValidateState(_ isValidated: Bool) {
        let errorMessage = isValidated ? "" : "At least 8 characters, at least one lowercase and one uppercase letter".localisedString()
        passwordErrorMsg = errorMessage
    }
    
    func isPasswordFieldValidated(_ password:String) -> Bool {
        if !password.isEmpty {
            let uppercase = NSPredicate(format:"SELF MATCHES %@", ".*[A-Z]+.*").evaluate(with: password)
            let lowercase = NSPredicate(format:"SELF MATCHES %@", ".*[a-z]+.*").evaluate(with: password)
            return (password.count >= minPasswordCharacters) && uppercase && lowercase
        }
        return false
    }
}
