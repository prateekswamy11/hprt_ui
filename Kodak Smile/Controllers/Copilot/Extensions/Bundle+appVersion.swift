//
//  Bundle+appVersion.swift
//  Polaroid MINT
//
//  Created by MacMini002 on 6/12/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

private let bundleShortVersionKey = "CFBundleShortVersionString"

extension Bundle {
    
    var applicationVersion: String? {
        return self.infoDictionary?[bundleShortVersionKey] as? String
    }
}
