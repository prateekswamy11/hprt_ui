//
//  PopupRepresentable.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/28/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation

public protocol PopupRepresentable {
    var title: String? { get }
    var message: String { get }
}
