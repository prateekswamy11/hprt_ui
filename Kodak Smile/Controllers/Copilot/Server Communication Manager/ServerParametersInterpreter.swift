//
//  ServerParametersInterpreter.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/28/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

public struct ServerParametersInterpreter {
    
    //Consts
    
    private let batteryStatusKey = "battery_status"
    
    func generatePolaroidUserFrom(serverUser: UserMe) -> KodakUser?{
        guard let userInfo = serverUser.userDetails.userInfo else { return nil }
//        let didAgreeToTOU = serverUser.accountStatus.termsOfUseApproved
        let firstName = userInfo.firstName
        let lastName = userInfo.lastName
        
        if UserDefaults.standard.value(forKey: "acceptTermsAndConditions") as? Bool == true {
            return KodakUser(id: serverUser.userDetails.id, firstName: firstName, lastName: lastName, email: serverUser.userDetails.email, didAgreeToTOU: true, isAnonymous: serverUser.accountStatus.credentialsType == .anonymous)
        } else {
            return KodakUser(id: serverUser.userDetails.id, firstName: firstName, lastName: lastName, email: serverUser.userDetails.email, didAgreeToTOU: false, isAnonymous: serverUser.accountStatus.credentialsType == .anonymous)
        }
    }
    
    func generateThingStatus(with batteryLevel: String) -> ThingStatus {
        let currentTimeInMilliseconds = Date()
        let batteryStatus = ThingReportedStatus(time: currentTimeInMilliseconds, name: batteryStatusKey, value: batteryLevel)
        let thingStatus = ThingStatus(lastSeen: currentTimeInMilliseconds, reportedStatuses: [batteryStatus])
        return thingStatus
    }
}
