//
//  DeviceInfoData.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/27/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation

struct DeviceInfoData {
    
    var serialID: String?
    var firmware: String?
    var model: String?
    var batteryLevel: String?
    
    var isComplete: Bool {
        return serialID != nil && firmware != nil && model != nil && batteryLevel != nil
    }
    
}
