//
//  AppUpgradeManager.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/23/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess
import CopilotLogger

public enum ApplicationUpgradeType {
    case required
    case notRequired
    case optional
}

protocol AppUpgradeManagerDelegate: class {
    func fetchUpgradeStatusFailed(error: KodakError)
    func upgradeRequired(upgradeType: ApplicationUpgradeType)
}

public class AppUpgradeManager {
    
    weak var delegate: AppUpgradeManagerDelegate?
    
    private var appStoreURL: URL? {
        return URL(string: "https://itunes.apple.com/app/id1447241173")
    }
    
    func visitStore() {
        if let url = appStoreURL, UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            ZLogManagerWrapper.sharedInstance.logFatal(message: "Cannot open application store page, invalid url")
        }
    }
    
    func checkIfUpgradeIsAvailable(with upgradeDelegate: AppUpgradeManagerDelegate) {
        delegate = upgradeDelegate
        AppManager.shared.appConfigurationManager.appVersionDelegate = self
        AppManager.shared.appConfigurationManager.refetchConfiguration()
    }
}

extension AppUpgradeManager : AppVersionStatusDelegate {
    
    func fetchUpgradeStatusFailed(error: KodakError) {
        DispatchQueue.main.async { [weak self] in
            self?.delegate?.fetchUpgradeStatusFailed(error: error)
        }
    }
    
    func upgradeRequired(upgradeType: ApplicationUpgradeType) {
        DispatchQueue.main.async { [weak self] in
            self?.delegate?.upgradeRequired(upgradeType: upgradeType)
        }
    }
}
