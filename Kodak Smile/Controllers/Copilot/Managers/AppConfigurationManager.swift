//
//  AppConfigurationManager.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/23/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotLogger
import CopilotAPIAccess

protocol AppVersionStatusDelegate: class {
    func fetchUpgradeStatusFailed(error: KodakError)
    func upgradeRequired(upgradeType: ApplicationUpgradeType)
}

class AppConfigurationManager {
    
    weak var appVersionDelegate: AppVersionStatusDelegate?
  //  var termsOfUseDocument: LegalDocument?
  //  var faqDocument: LegalDocument?
    var systemConfiguration: SystemConfiguration?
    
    init() {
        if let appVersion = Bundle.main.applicationVersion {
            systemConfiguration = SystemConfiguration(appVersion: appVersion)
            systemConfiguration?.delegate = self as? SystemConfigurationDelegate
        }else {
            ZLogManagerWrapper.sharedInstance.logError(message: "Failed to init systemConfiguration, the app version pulled from the info.plist doesn't exit")
            fatalError()
        }
    }
    
    internal func refetchConfiguration() {
        systemConfiguration?.refetchConfiguration()
    }
}

extension AppConfigurationManager: SystemConfigurationDelegate {
    
    func systemConfigurationReady(configuration: Configuration) {
//        if let termsOfUse = configuration.termsOfUse {
//            termsOfUseDocument = termsOfUse
//        }
        
//        if let faq = configuration.faq {
//            faqDocument = faq
//        }
    }
    
    func upgradeRequired(appVersionStatus: AppVersionStatus) {
        switch appVersionStatus.versionStatus {
        case .ok:
            appVersionDelegate?.upgradeRequired(upgradeType: .notRequired)
        case .newVersionRecommended:
            appVersionDelegate?.upgradeRequired(upgradeType: .optional)
        case .notSupported:
            appVersionDelegate?.upgradeRequired(upgradeType: .required)
        }
    }
    
    func versionCheckFailed(_ error: CheckAppVersionStatusError) {
        ZLogManagerWrapper.sharedInstance.logError(message: "Failed to check version: \(error.localizedDescription)")
        
        let error = KodakError.server
        appVersionDelegate?.fetchUpgradeStatusFailed(error: error)
    }
    
    func systemConfigurationFetchFailed(_ error: FetchConfigurationError) {
        ZLogManagerWrapper.sharedInstance.logError(message: "Couldn't fetch the system configuration: \(error.localizedDescription)")
    }
}
