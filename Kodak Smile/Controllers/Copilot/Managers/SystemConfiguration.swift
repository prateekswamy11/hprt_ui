//
//  SystemConfiguration.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/23/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess
import CopilotLogger

public protocol SystemConfigurationDelegate {
    func upgradeRequired(appVersionStatus: AppVersionStatus)
    func systemConfigurationReady(configuration: Configuration)
    func versionCheckFailed(_ error: CheckAppVersionStatusError)
    func systemConfigurationFetchFailed(_ error: FetchConfigurationError)
}

public struct SystemConfiguration {

    public var delegate: SystemConfigurationDelegate?
    private let appVersion: String
    
    public init(appVersion: String) {
        self.appVersion = appVersion
    }
    
    public func refetchConfiguration() {
        
        checkIfUpgradeRequired()
        getConfiguration()
    }
    
    private func checkIfUpgradeRequired() {
        guard let delegate = delegate else { return }
        
        Copilot.instance
            .manage
            .copilotConnect
            .app
            .checkAppVersionStatus()
            .build()
            .execute { (result) in
                switch result{
                case .success(let appVersionStatus):
                    ZLogManagerWrapper.sharedInstance.logInfo(message: "got success from checkIfUpgradeRequired")
                    
                    delegate.upgradeRequired(appVersionStatus: appVersionStatus)
                    
                case .failure(error: let error):
                    // TODO : Handle checkAppVersion failure "Server Error Root cause"
                    ZLogManagerWrapper.sharedInstance.logWarning(message: "An error occured while trying to fetch system configuration\(error.localizedDescription)")
                    if error.localizedDescription == "[CheckAppVersionStatus] Connectivity error: The request timed out."
                    {
                        print("The request timed out.")
                    }else if error.localizedDescription == "[CheckAppVersionStatus] Connectivity error: Could not connect to the server."
                    {
                        print("Could not connect to the server.")
                    }else if error.localizedDescription == "[CheckAppVersionStatus] Connectivity error: A server with the specified hostname could not be found."
                    {
                        print("A server with the specified hostname could not be found.")
                    }else if error.localizedDescription == "[CheckAppVersionStatus] Connectivity error: The network connection was lost."
                    {
                        print("A server with the specified hostname could not be found.")
                    }else if error.localizedDescription == "[CheckAppVersionStatus] Connectivity error: The Internet connection appears to be offline."
                    {
                        print("A server with the specified hostname could not be found.")
                    }else{
                        delegate.versionCheckFailed(error)
                    }
                }
        }
    }
    
    private func getConfiguration() {
        guard let delegate = delegate else { return }
        
        Copilot.instance
            .manage
            .copilotConnect
            .app
            .fetchConfig()
            .build()
            .execute { (response) in
                switch response {
                case .success(let configuration):
                // Configuration fetched with success
                    ZLogManagerWrapper.sharedInstance.logInfo(message: "got success from getConfiguration")
                    
                    delegate.systemConfigurationReady(configuration: configuration)
                case .failure(error: let fetchConfigurationError):
                    // Failed to fetch the configuration, handle the error
                    ZLogManagerWrapper.sharedInstance.logError(message: fetchConfigurationError.localizedDescription)
                    delegate.systemConfigurationFetchFailed(fetchConfigurationError)
                }
        }
    }
}
