//
//  ConnectionManager.swift
//  Polaroid MINT
//
//  Created by MacMini002 on 7/16/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation
import CopilotAPIAccess
import CopilotLogger


class ReachabilityHandler {
    
    var timer:Timer?
    
    // MARK: - LifeCycle
    
    init() {
        startInternetNotify()
    }
    
    func startInternetNotify () {
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.checkIntenetConntection), userInfo: nil, repeats: true)
        }
    }
    
    func stopInternetNotify () {
       self.timer?.invalidate()
    }
    
    func checkAppUpgrade() {
        
        // Stop internet notifyier
        self.stopInternetNotify()

        Copilot.instance
            .manage
            .copilotConnect
            .app
            .checkAppVersionStatus()
            .build()
            .execute { (result) in
                switch result{
                case .success(let appVersionStatus):
                    ZLogManagerWrapper.sharedInstance.logInfo(message: "got success from checkIfUpgradeRequired")
                    
                    switch appVersionStatus.versionStatus {
                    case .ok:
                        print("not require")
                    case .newVersionRecommended:
                        print("optional")
                        self.showUpdateAlert(message:"There is an app update available. Would you like to update?".localisedString())
                    case .notSupported:
                        print("require")
                        self.showUpdateAlert(message: "There is a required app update available. Please update the app to continue using it.".localisedString(), btnTitle: "OK")
                    }
                    
                case .failure(error: let error):
                    // TODO : Handle checkAppVersion failure
                    ZLogManagerWrapper.sharedInstance.logWarning(message: "An error occured while trying to fetch system configuration\(error.localizedDescription)")
                    self.startInternetNotify()
                }
        }
    }
    
    func getCurrentViewController() -> UIViewController {
    
        var vc = UIViewController()
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
                
            }
            vc = topController
        }
        return vc
    }
    
    func showUpdateAlert(message:String, btnTitle:String = "YES"){
        
        // create the alert
        let alert = UIAlertController(title: "New version available".localisedString(), message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: btnTitle.localisedString(), style: .destructive, handler: { action in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                AppManager.shared.appUpgradeManager.visitStore()
                // set observer for UIApplication.willEnterForegroundNotification
                NotificationCenter.default.addObserver(self, selector: #selector(self.willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
            })
        }))
        
        if let vc = getCurrentViewController() as? UIViewController {
            vc.present(alert, animated: true, completion: nil)
        }
        
        if btnTitle == "OK" { return }
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "NO".localisedString(), style: .cancel , handler: { action in
            
        }))
    }
    
    // my selector that was defined above
    @objc func willEnterForeground() {
        // do stuff
        NotificationCenter.default.removeObserver(self)
        self.startInternetNotify()
    }
    
    @objc func checkIntenetConntection()
    {
        if WebserviceModelClass().isInternetAvailable() {
            print(">>>>>>>>>>>> reachable <<<<<<<<<<")
            self.checkAppUpgrade()
        } else {
            print(">>>>>>>>>>>> not reachable <<<<<<<<<<")
        }
    }
}
