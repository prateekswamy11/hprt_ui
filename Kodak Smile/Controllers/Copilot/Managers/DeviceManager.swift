//
//  DeviceManager.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/27/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess
import CopilotLogger

class DeviceManager {

    public private (set) var lastConnectedKodak: KodakThing? = nil
    private (set) var isCurrentKodakNew = true
    private var deviceDataFetcher: DeviceDataFetcher?
    private var associatedDevices: [Thing]?
    private var associating = false
    
    var model: String?
    
    //MARK: - Init
    
    init() {
        loadLastConnectedKodak()
    }
    
    func kodakDeviceFound(deviceID: String) {
        
        // Check if the device id is different from the current kodak id. If so, replace the device and report notify about it
        isCurrentKodakNew = isNewKodak(kodakID: deviceID)
        if isCurrentKodakNew {
            newKodakFound(kodakID: deviceID)
        }
        let thingConnectedEvent = ThingConnectedAnalyticsEvent(thingID: deviceID)
        Copilot.instance.report.log(event:thingConnectedEvent)
        
        deviceDataFetcher = DeviceDataFetcher.init(delegate: self)
        deviceDataFetcher?.fetchDeviceInfo()
    }
    
    func kodakDeviceDisconnected(deviceID: String) {
        disAssosiateKodak(id: deviceID)
    }
    
    //MARK: - Thing
    
    func deviceInfoReceived(data: DeviceInfoData) {
        guard
            let kodakId = data.serialID,
            let firmware = data.firmware,
            let model = data.model,
            let batteryLevel = data.batteryLevel
            else {
                ZLogManagerWrapper.sharedInstance.logFatal(message: "Trying to update kodak with missing data")
                return
        }
        let thingInfoEvent = ThingInfoAnalyticsEvent(thingFirmware: firmware, thingModel: model, thingId: kodakId)
        Copilot.instance.report.log(event: thingInfoEvent)
        let batteryLevelAnalyticsEvent = BatteryLevelAnalyticsEvent(value: batteryLevel)
        Copilot.instance.report.log(event: batteryLevelAnalyticsEvent)

        if self.associatedDevices != nil {
            updateCloud(kodakId: kodakId, firmware: firmware, model: model, batteryLevel: batteryLevel)
        } else {
            fetchAssociatedDevices(completion: { [weak self] success in
                self?.updateCloud(kodakId: kodakId, firmware: firmware, model: model, batteryLevel: batteryLevel)
            })
        }
    }
    
    private func updateCloud(kodakId:String, firmware: String, model:String, batteryLevel:String){
        var associated : [Thing] = []
        if let devices = self.associatedDevices{
            associated = devices
        }
        if associated.contains(where: { thing in thing.thingInfo.physicalId == kodakId}) {
            // Existing Kodak - update
            updateKodak(id: kodakId, firmware: firmware, batteryLevel: batteryLevel)
        } else {
            // New kodak - assosiate
            if !self.associating{
                checkIfCanAssociateKodak(id: kodakId, completion: { [weak self] success in
                    if success {
                        self?.assosiateKodak(id: kodakId, firmware: firmware, model: model, completion: { [weak self] success in
                            if success {
                                // After a successful associate, update the thing
                                self?.updateKodak(id: kodakId, firmware: firmware, batteryLevel: batteryLevel)
                            }
                        })
                    }
                })
            }
        }
    }
    
    private func fetchAssociatedDevices(completion: @escaping (Bool)->()){
        Copilot.instance
            .manage
            .copilotConnect
            .thing
            .fetchThings()
            .build()
            .execute { (response) in
                switch response {
                case .success(let things):
                    //Fetched the things associated with the user
                    self.associatedDevices = things
                    ZLogManagerWrapper.sharedInstance.logDebug(message: "Fetched thing succsfully")
                    completion(true)
                case .failure(error: let fetchError):
                    // Failed to fetch things, handle the error
                    ZLogManagerWrapper.sharedInstance.logDebug(message: "Error fetching things kodak: \(fetchError.localizedDescription)")
                    completion(false)
                }
        }
    }
    private func updateKodak(id: String, firmware: String, batteryLevel: String) {
        Copilot.instance
            .manage
            .copilotConnect
            .thing
            .updateThing(withPhysicalId : id)
            .with(name: "")
            .with(firmware: firmware)
            .with(status: ServerParametersInterpreter().generateThingStatus(with: batteryLevel))
             .build().execute { (response) in
                switch response {
                case .success( _):
                // Updated thing with success
                    ZLogManagerWrapper.sharedInstance.logDebug(message: "Kodak updated successfully")
                case .failure(error: let updateError):
                    // Failed to update thing, handle error
                    ZLogManagerWrapper.sharedInstance.logFatal(message: "Error updating kodak: \(updateError.localizedDescription)")
                }
        }
    }
    
    private func assosiateKodak(id: String, firmware: String, model: String, completion: @escaping (Bool)->()) {
        self.associating = true
        
        Copilot.instance
            .manage
            .copilotConnect
            .thing
            .associateThing(withPhysicalId: id, firmware: firmware, model: model)
            .build()
            .execute { (response) in
                switch response {
                case .success(let thing):
                // Associated thing with success
                    ZLogManagerWrapper.sharedInstance.logDebug(message: "Kodak assosiated successfully")
                    
                    let onboardingEndedEvent = OnboardingEndedAnalyticsEvent()
                    Copilot.instance.report.log(event: onboardingEndedEvent)
                    
                    if let _ = self.associatedDevices {
                        self.associatedDevices?.append(thing)
                    }
                    else{
                        self.fetchAssociatedDevices(completion: {success in})
                    }
                    completion(true)
                    
                case .failure(error: let associateError):
                    // Failed to associate thing, handle the error
                    ZLogManagerWrapper.sharedInstance.logFatal(message: "Error assosiating kodak: \(associateError.localizedDescription)")
                    completion(false)
                }
                self.associating = false
        }
    }
    
    private func disAssosiateKodak(id: String) {
        
        Copilot.instance
            .manage
            .copilotConnect
            .thing
            .disassociateThing(withPhysicalId: id)
            .build()
            .execute { (response) in
                switch response {
                case .success():
                    ZLogManagerWrapper.sharedInstance.logDebug(message: "Kodak disassosiated successfully")
                    break
                    
                case .failure(error: let disassociateError):
                    ZLogManagerWrapper.sharedInstance.logFatal(message: "Error disassosiating kodak: \(disassociateError.localizedDescription)")
                }
        }
    }
    
    private func checkIfCanAssociateKodak(id: String, completion: @escaping (Bool)->()) {
        
        Copilot.instance
            .manage
            .copilotConnect
            .thing
            .checkIfCanAssociate(withPhysicalId: id)
            .build()
            .execute { (response) in
                switch response {
                case .success(let _):
                    ZLogManagerWrapper.sharedInstance.logDebug(message: "Kodak check if can assosiated successfully")
                    completion(true)
                    break
                    
                case .failure(error: let checkIfCanAssociateeError):
                    
                    ZLogManagerWrapper.sharedInstance.logFatal(message: "Error checkIfCan assosiating kodak: \(checkIfCanAssociateeError.localizedDescription)")
                    completion(false)
                }
        }
    }
    
    //MARK: - Reset
    
    func reset() {
        lastConnectedKodak = nil
        isCurrentKodakNew = true
    }
    
    //MARK: - Private
    
    private func isNewKodak(kodakID: String) -> Bool {
        
        var isNewKodak = false
        
        if let lastConnectedKodakID = lastConnectedKodak?.kodakID, kodakID != lastConnectedKodakID {
            isNewKodak = true
        }
        else if lastConnectedKodak  == nil {
            isNewKodak = true
        }
        
        return isNewKodak
    }
    
    private func saveKodakID(kodakID: String) {
        UserDefaultsManager.set(value: kodakID, forKey: .kodakId)
    }
    
    private func newKodakFound(kodakID: String) {
        self.lastConnectedKodak = KodakThing.init(kodakID: kodakID)
        saveKodakID(kodakID: kodakID)
    }
    
    private func loadLastConnectedKodak() {
        if let kodakID: String = UserDefaultsManager.value(for: .kodakId) {
            self.lastConnectedKodak = KodakThing.init(kodakID: kodakID)
        }
    }
}

extension DeviceManager: DeviceDataFetcherDelegate {
    func didFetchData(firmware: String, batteryLevel: String){
        deviceDataFetcher = nil

        var version = String()
        if appDelegate().Product_ID == "0"{
            version = "1.0"
        } else {
            version = "1.2"
        }
        AppManager.shared.kodakManager.model = version
        self.model = version

        let deviceInfoData = DeviceInfoData(serialID: CONNECT_DEVICE_MAC_ADDRESS, firmware: firmware, model: self.model ?? "unknown", batteryLevel: batteryLevel)
        deviceInfoReceived(data: deviceInfoData)
    }
}

