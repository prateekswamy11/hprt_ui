//
//  UserDefaultsManager.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/27/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation

enum UserDefaultsKeys: String {
    case kodakId
    
    // All the user associated keys that should be purged on logout
    static func userKeys() -> [UserDefaultsKeys] {
        return [kodakId]
    }
}

struct UserDefaultsManager {
    
    //MARK: - Get
    
    static func value<T>(for key: UserDefaultsKeys) -> T? {
        return UserDefaults.standard.value(forKey: key.rawValue) as? T
    }
    
    //MARK: - Set
    
    static func set<T>(value: T, forKey key: UserDefaultsKeys) {
        UserDefaults.standard.set(value, forKey: key.rawValue)
    }
    
    //MARK: - Purging
    
    static func purgeUserData() {
        let keysToPurge = UserDefaultsKeys.userKeys()
        UserDefaultsManager.clear(keys: keysToPurge)
    }
    
    private static func clear(keys: [UserDefaultsKeys]) {
        keys.forEach({ UserDefaults.standard.removeObject(forKey: $0.rawValue) })
    }
}
