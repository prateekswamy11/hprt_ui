//
//  UserManager.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/24/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess
import CopilotLogger

protocol UserAutherizationStateDelegate: NSObjectProtocol {
    func userLoggedOut()
}

typealias KodakGeneralClosure = (Response<Void, KodakError>) -> Void


class UserManager {
    
    //MARK: Properties
    
    private(set) var passwordRulesList: [PasswordRule]?
    private(set) var user: KodakUser?
    weak var userAutherizationStateDelegate: UserAutherizationStateDelegate?
    
    //MARK: - Auth
    
    func attemptToSilentLogin(closure: @escaping KodakGeneralClosure){
        
        Copilot.instance
            .manage
            .copilotConnect
            .auth
            .login()
            .silently
            .build()
            .execute { (response) in
                switch response {
                case .success():
                // Silent login performed with success
                    self.getUser(closure: closure)
                case .failure(error: let silentLoginError):
                    // Failed to silently log in, handle the error
                    ZLogManagerWrapper.sharedInstance.logError(message: "Silent login failed with error: \(silentLoginError.localizedDescription)")
                    let appError = KodakError.fromSilentLoginError(silentLoginError)
                    DispatchQueue.main.async {
                        closure(.failure(error: appError))
                    }
                }
        }
    }
    
    func register(withEmail email:String, password: String, firstName: String, lastName: String, closure: @escaping KodakGeneralClosure) {
        
        Copilot.instance
            .manage
            .copilotConnect
            .auth
            .signup()
            .withNoGDPRConsentRequired
            .with(email: email, password: password, firstname: firstName, lastname: lastName)
            .build()
            .execute { [weak self] (response) in
                switch response {
                case .success:
                // User signed up with success
                    print("User Register Successfully")
                    self?.passwordRulesList = nil
                    self?.getUser(closure: closure)
                case .failure(error: let registerError):

                    // Failed to sign up the user, handle the error
                    print("User Register Failed")
                    ZLogManagerWrapper.sharedInstance.logError(message: "Register with email \(email) failed with error: \(registerError.localizedDescription)")
                    let appError = KodakError.fromRegisterError(registerError,email: email)
                    DispatchQueue.main.async {
                        closure(.failure(error: appError))
                    }
                }
        }
    }
    
    func registerAnonymously(closure: @escaping KodakGeneralClosure) {
        
        Copilot.instance
            .manage
            .copilotConnect
            .auth
            .signup()
            .withNoGDPRConsentRequired
            .anonymously
            .build()
            .execute { [weak self] (response) in
                switch response {
                case .success:
                // User is registered anonymously
                    self?.getUser(closure: closure)
                    self?.passwordRulesList = nil
                case .failure(error: let registerError):
                    // Failed registering anonymous user
                    ZLogManagerWrapper.sharedInstance.logError(message: "Anonymous register failed with error: \(registerError.localizedDescription)")
                    let appError = KodakError.fromRegisterAnonymouslyError(registerError)
                    DispatchQueue.main.async {
                        closure(.failure(error: appError))
                    }
                }
        }
    }
    
    func elevateAnonymous(withEmail email:String, password: String, firstName: String, lastName: String, closure: @escaping KodakGeneralClosure) {
        Copilot.instance
            .manage
            .copilotConnect
            .user
            .elevate()
            .with(email: email, password: password, firstname: firstName, lastname: lastName)
            .build()
            .execute { (response) in
                switch response {
                case .success():
                // User was elevated successfully
                    self.getUser(closure: closure)
                    self.passwordRulesList = nil
                case .failure(error: let elevateError):
                    // Failed to elevate the user, handle the error
                    ZLogManagerWrapper.sharedInstance.logError(message: "Register with email \(email) failed with error: \(elevateError.localizedDescription)")
                    let appError = KodakError.fromElevateAnonymousError(elevateError,email: email)
                    DispatchQueue.main.async {
                        closure(.failure(error: appError))
                    }
                }
        }
    }
    
    func login(withEmail email:String, password: String, closure: @escaping KodakGeneralClosure) {
        
        Copilot.instance
            .manage
            .copilotConnect
            .auth
            .login()
            .with(email: email, password: password)
            .build()
            .execute { [weak self] (response) in
                switch response {
                case .success:
                // Logged in successfully
                    self?.getUser(closure: closure)
                    self?.passwordRulesList = nil
                case .failure(error: let loginError):
                    // Failed to log in user
                    ZLogManagerWrapper.sharedInstance.logError(message: "Login with email \(email) failed with error: \(loginError.localizedDescription)")
                    print("Login with email \(email) failed with error: \(loginError.localizedDescription)")
                    let appError = KodakError.fromLoginError(loginError)
                    DispatchQueue.main.async {
                        closure(.failure(error: appError))
                    }
                }
        }
    }
    
    func logout(with logoutClosure: @escaping KodakGeneralClosure){
        Copilot.instance
            .manage
            .copilotConnect
            .auth
            .logout()
            .build()
            .execute { (response) in
                switch response {
                case .success():
                // User logged out with success
                    self.user = nil
                    logoutClosure(.success(()))
                    self.userAutherizationStateDelegate?.userLoggedOut()
                case .failure(error: let logoutError):
                    // Failed to log out the user
                    ZLogManagerWrapper.sharedInstance.logError(message: "Logout failed with error: \(logoutError.localizedDescription)")
                    let error = KodakError.server
                    logoutClosure(.failure(error: error))
                }
        }
    }
    
    //MARK: Password
    
    func getPasswordRulesPolicy(closure: @escaping KodakGeneralClosure){
        if self.passwordRulesList != nil {
            closure(.success(()))
        }
        else {
            Copilot.instance
                .manage
                .copilotConnect
                .app
                .fetchPasswordPolicyConfig()
                .build()
                .execute { (response) in
                    DispatchQueue.main.async {
                        switch response {
                        case .success(let rules):
                            self.passwordRulesList = rules
                            closure(.success(()))
                        case .failure(error: let error):
                            ZLogManagerWrapper.sharedInstance.logError(message: "Get password rules policy failed with error: \(error.localizedDescription)")
                            let error = KodakError.server
                            closure(.failure(error: error))
                        }
                    }
            }
        }
    }
    
    func resetPassword(forEmail email: String, closure: @escaping KodakGeneralClosure){
        
        Copilot.instance
            .manage
            .copilotConnect
            .user
            .resetPassword(for: email)
            .build()
            .execute { (response) in
                switch response {
                case .success():
                // A request to change user's password was successfully made.
                    closure(.success(()))
                case .failure(error: let resetPasswordError):
                    // Failed to reset the password, handle the error
                    ZLogManagerWrapper.sharedInstance.logError(message: "Reset password failed with error: \(resetPasswordError.localizedDescription)")
                    let error = KodakError.server
                    closure(.failure(error: error))
                }
        }
    }
    
    //MARK: Terms

    public func userAgreedToTOU(closure: @escaping KodakGeneralClosure) {
        
        Copilot.instance
            .manage
            .copilotConnect
            .user
            .updateMe()
            .approveTermsOfUse(forVersion: "1.0")
            .build()
            .execute { (response) in
                switch response {
                case .success():
                    print("Approve Terms of Used Successfully")
                    // Updated user's terms of use approval with success
                    let acceptTermsAnalyticsEvent = AcceptTermsAnalyticsEvent(version: "1.0")
                    Copilot.instance.report.log(event: acceptTermsAnalyticsEvent)
                    closure(.success(()))
                case .failure(error: let updateError):
                    // Failed to update user's terms of use approval
                    print("Approve Terms of Used Error : \(updateError.localizedDescription)")
                    ZLogManagerWrapper.sharedInstance.logError(message: "TOU approval failed with error: \(updateError.localizedDescription)")
                    let error = KodakError.server
                    closure(.failure(error: error))
                }
        }
    }

    //MARK: - User
    
    private func getUser(closure: @escaping KodakGeneralClosure) {
        
        Copilot.instance
            .manage
            .copilotConnect
            .user
            .fetchMe()
            .build()
            .execute { (response) in
                switch response {
                case .success(let userMe):
                // UserMe fetched with success
                    print("Get user method called")
                    guard let polaroidUser = ServerParametersInterpreter().generatePolaroidUserFrom(serverUser: userMe) else {
                        ZLogManagerWrapper.sharedInstance.logFatal(message: "TDUser wasn't created from server user: \(userMe)")
                        let error = KodakError.server
                        closure(.failure(error: error))
                        return
                    }
                    UserDefaults.standard.set(true, forKey: "userLogin")
                    self.user = polaroidUser
                    COPILOT_USER_ID = self.user!.id
                    //AR-->Attached video on already uploaded image by other user doesn't show me AR already attached popup
                    UserDefaults.standard.set(COPILOT_USER_ID, forKey: "user_id")
                    closure(.success(()))
                    
                case .failure(error: let fetchMeError):
                    // Failed to fetch userMe, handle the error
                    ZLogManagerWrapper.sharedInstance.logError(message: "Fetch user failed with error: \(fetchMeError.localizedDescription)")
                    closure(.failure(error: .server))
                }
        }
    }
}
