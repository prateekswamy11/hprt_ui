//
//  DeviceDataFetcher.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/27/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import Foundation
import CopilotAPIAccess

protocol DeviceDataFetcherDelegate: class {
    func didFetchData(firmware: String, batteryLevel: String)
}

class DeviceDataFetcher: ResponceDelegate {
    
    weak var delegate: DeviceDataFetcherDelegate?    
    var batteryLevel: String?
    var firmwareInfo: String?
    var printCount: String?
    
    init(delegate: DeviceDataFetcherDelegate) {
        self.delegate = delegate
    }
    
    func fetchDeviceInfo() {
        //Delaying the requests so it will return and won't get dismissed
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.getZIPInfo, ResponseDelegate: self))
            
           // PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.getBetteryLevel, ResponseDelegate: self))
           // PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.getVersion, ResponseDelegate: self))
            //PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.printCount, ResponseDelegate: self))
        }
    }
    
    func ZIP_accessory_info(_ dict: NSMutableDictionary) {
        
        let dictPrinterData = dict
        
        
        let BatteryPer =   "\(dictPrinterData.value(forKey: "Battery")!)" //+ "%"
        let PhotoPrintedCount =  "\(dictPrinterData.value(forKey: "PhotoPrinted")!)"
        let FirmwareVersion =  "\(dictPrinterData.value(forKey: "FirmWareVersion")!)"
        
        firmwareInfo = FirmwareVersion
        batteryLevel = BatteryPer
        printCount = PhotoPrintedCount
        
        checkIfAllInfoIsFetched()
        
    }
    
    func getBetteryLavelResponse(_ Level:String) {
        batteryLevel = Level
        
//        let batteryLevelAnalyticsEvent = BatteryLevelAnalyticsEvent(value: Level)
//        Copilot.instance.report.log(event: batteryLevelAnalyticsEvent)
        
        checkIfAllInfoIsFetched()
    }
    
    func GetVersionResponse(_ Firmware:String ,Conexan:String,  TMD: String){
        let XFIRMVersion =  Float(Firmware.replacingOccurrences(of: "v", with: ""))!
        firmwareInfo = String(XFIRMVersion)
        checkIfAllInfoIsFetched()
    }
    
    func getPrintCountResponse(_ PrintCount:String) {
        
        printCount = PrintCount
        
        let printCountAnalyticsEvent = PrintCountAnalyticsEvent(value: PrintCount)
        Copilot.instance.report.log(event: printCountAnalyticsEvent)
    }

    private func checkIfAllInfoIsFetched() {
        guard let firmware = firmwareInfo, let battery = batteryLevel else { return }
        delegate?.didFetchData(firmware: firmware, batteryLevel: battery)
    }
}
