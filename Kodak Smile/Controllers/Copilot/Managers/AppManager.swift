//
//  AppManager.swift
//  Kodak Smile
//
//  Created by MacMini002 on 5/23/19.
//  Copyright © 2019 maximess142. All rights reserved.
//

import UIKit
import CopilotLogger

public class AppManager: NSObject {

    //MARK: Properties
    
    static let shared = AppManager()
    let appUpgradeManager = AppUpgradeManager()
    let appConfigurationManager = AppConfigurationManager()
    let userManager = UserManager()
    let kodakManager = DeviceManager()
    
    
    override init() {
        super.init()
    }
    
    //MARK: Public
    
    func appLaunched() {
        //Setting ZLogManager
        ZLogManagerWrapper.sharedInstance.setRuntime(minLogLevelType: .fatalLogLevel, maxLogLevelType: .fatalLogLevel)
        ZLogManagerWrapper.sharedInstance.shouldWriteToFile(true)
    }
}

extension AppManager: UserAutherizationStateDelegate {
    
    func userLoggedOut() {
        // Reset all user associated saved data
        UserDefaultsManager.purgeUserData()
        
        // Reset the device manager and associated sensors
        kodakManager.reset()
    }
}

