//
//  ConnectivityViewConnectionDelegate.swift
//  Polaroid MINT
//
//  Created by MacBook003 on 25/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CoreBluetooth

extension ConnectivityViewController : ResponceDelegate, PrinterStatusDelegate, DialogDelegate {
    //--------MARK:  #printer connection----
    @objc func AccessoriesDisConnect() {
        self.scanForUnpairedDevices()
    }
    
    @objc func Accessories_dis_connected_called() {
        appDelegate().isConnectedToPrinter = false
        appDelegate().isConnectInProgress = false
        self.deviceTableView.reloadData()
    }
    
    @objc func Accessories_Connected_called() {
        appDelegate().isConnectedToPrinter = true
        
        if(appDelegate().currentAccessory?.protocolStrings[0]  == MFI_PROTOCOL_STRING) {
            appDelegate().isConnectInProgress = false
         //   appDelegate().isZIPSNAPConnected = true
            appDelegate().isConnectedToPrinter = true
            self.viewCouldntFindPrinter.isHidden = true
        } else {
            appDelegate().isConnectedToPrinter = true
            appDelegate().isConnectInProgress = false
            appDelegate().isPrintingError = false
            //appDelegate().isZIPSNAPConnected = false
            appDelegate().isConnectInProgress = false
        }
        self.deviceTableView.reloadData()
    }
    
    @objc func printerStatusConnected(){
        self.deviceTableView.reloadData()
    }
}
