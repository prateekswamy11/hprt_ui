//
//  ConnectivityVCConnectionExtension.swift
//  Kodak Smile
//
//  Created by maximess152 on 21/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation
import CoreBluetooth
import CopilotAPIAccess

extension ConnectivityViewController : CBCentralManagerDelegate {
    
    @objc func showDevicePicker() {
        self.view.isUserInteractionEnabled = true
        ConnectionStatus.sharedController.printerStatusDelegate = self
        if EAAccessoryManager.shared().connectedAccessories.count == 0{
            //Code to not show device picker if came from contact support screen.
            if goToMailComposer == "mailComposer" {
                goToMailComposer = ""
                self.viewCouldntFindPrinter.isHidden = false
            } else {
                self.scanForUnpairedDevices()
            }
        }
    }
    
    func openBluetooth() {
        //           self.dismiss(animated: true, completion: nil)
        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
        UIApplication.shared.open(settingsUrl)
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            self.openBluetooth()
//            self.isBluetoothOn = true
            break
        case .poweredOff:
//            self.isBluetoothOn = false
            break
        case .resetting:
            break
        case .unauthorized:
            break
        case .unsupported:
            break
        case .unknown:
            break
        default:
            break
        }
    }
    
    func connectSelectedPrinter(_ tagForConnectBtn: Int) {
        let tapConnectDeviceEvent = TapConnectDeviceAnalyticsEvent()
        Copilot.instance.report.log(event: tapConnectDeviceEvent)
        //EADSessionController.shared().closeSession()
        //if comeFrom == "printPriveVC" {
        if(EAAccessoryManager.shared().connectedAccessories.count > 0)
        {
            let allConnectedAcceosory = EAAccessoryManager.shared().connectedAccessories
            appDelegate().accessoryList = NSMutableArray(array: allConnectedAcceosory)
            self.accessoryList = appDelegate().accessoryList
            DispatchQueue.main.async(execute: {
                if(self.accessoryList.count > 0)
                {
                    let accessory = EAAccessoryManager.shared().connectedAccessories[tagForConnectBtn]
                    print("accessory.protocolStrings[0] = \(accessory.protocolStrings[0])")
                    if(accessory.protocolStrings[0] == MFI_PROTOCOL_STRING)
                    {
                        print("CONNECTED ACESSORY IS ZIP SNAP")
                        appDelegate().isConnectedToPrinter = true
                    }
                    PrinterQueueManager.sharedController.Is_Busy = false
                    UserDefaults.standard.set(accessory.serialNumber, forKey: "lastConnectedAccessory")
                    UserDefaults.standard.synchronize()
                    appDelegate().currentAccessory = accessory
                    connectedAccessory = accessory
                    Accessory_Manager.shared.getDevices()
                    Accessory_Manager.shared.setUpSession(accessory: accessory)
                    appDelegate().isConnectInProgress = false
                    appDelegate().isConnectedToPrinter = true
                    self.connectionObservers()
                    
                    if self.comeFrom == "printPriveVC"{
                        //self.showLoaderView(accessory: accessory)
                    }
                    else
                    {
                        if self.comeFrom == "galleryVc"
                        {
                            self.dismiss(animated: true, completion: nil)
                        }
                        else{
                            if self.comeFromStr == "updateFailedVC"
                            {
                                if let navController = self.navigationController {
                                    for controller in navController.viewControllers {
                                        if controller is FirmwareUpdateAvailableViewController { // Change to suit your menu view controller subclass
                                            //                                            NotificationCenter.default.post(name: NSNotification.Name("dismissCantSeePrinterVC"), object: nil)
                                            navController.popToViewController(controller, animated:true)
                                            break
                                        }
                                    }
                                }
                            }
                            else if self.comeFromStr == "firmwareVC" {
                                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: false)
                            }
                            else if comeFromOnboardingPageVC == "onboardingVC"
                            {
                                NotificationCenter.default.post(name: Notification.Name("notificationToLoadPrinterScreen"), object: nil)
                            }
                            else if goToQuickTipsOnboarding == "quickTipsOnboarding"
                            {
                                goToQuickTipsOnboarding = ""
                                let printerConnectedVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "PrinterConnectedViewController") as! PrinterConnectedViewController
                                self.view.addSubview(printerConnectedVC.view)
                                self.addChildViewController(printerConnectedVC)
                            }
                            else
                            {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                }
                else
                {
                    //Utils.AlertMessage(message: lbl_RE_CONNECT)
                }
            })
        }
        //        }else{
        //            self.dismiss(animated: true, completion: nil)
        //        }
    }
    
    //Scan for unpaire devices
    func scanForUnpairedDevices() {
        
        let defaults = UserDefaults.standard
        defaults.set(false, forKey: "ShowAll")
//        if isBluetoothOn{
            //Akshay Solve crash detect on crashanalitics Add guard
            //            let predicate = NSPredicate(format: "SELF CONTAINS[c] 'Smile'")
            //            EAAccessoryManager.shared().showBluetoothAccessoryPicker(withNameFilter: predicate) { (error) -> Void in
            //                if error != nil {
            //                    NSLog("Error code: \(error!.localizedDescription) \(error.debugDescription)")
            //                } else {
            //                    self.setupFirstConnection()
            //                }
            //            }
            
            if !isBluetoothOn {
                self.bluetoothManager = CBCentralManager()
                self.bluetoothManager.delegate = self
                isBluetoothOn = true
            }else{
                let popUpConnSettings = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpConnectivityThroughSettings") as! PopUpConnectivityThroughSettings
                self.present(popUpConnSettings, animated: true, completion: nil)
            }
            
//        }else{
//            self.bluetoothManager = CBCentralManager()
//            self.bluetoothManager.delegate = self
//        }
        
        
        
        let tapConnectDeviceEvent = TapConnectDeviceAnalyticsEvent()
        Copilot.instance.report.log(event: tapConnectDeviceEvent)
        //Change connectivity flow
        
        //        let tapConnectDeviceEvent = TapConnectDeviceAnalyticsEvent()
        //        Copilot.instance.report.log(event: tapConnectDeviceEvent)
        //        print("isBluetoothOn\(isBluetoothOn)")
        //        if isBluetoothOn{
        //            let defaults = UserDefaults.standard
        //            defaults.set(false, forKey: "ShowAll")
        //            self.viewCouldntFindPrinter.isHidden = true
        //            self.reloadDeviceTableView()
        //            let predicateSmile = NSPredicate(format: "SELF CONTAINS[c] 'Smile'")
        //            manager.showBluetoothAccessoryPicker(withNameFilter: predicateSmile) { (error) -> Void in
        //                self.reloadDeviceTableView()
        //                if error != nil {
        //                    if EAAccessoryManager.shared().connectedAccessories.count == 0{
        //                        self.viewCouldntFindPrinter.isHidden = false
        //                        self.lblFindYourDevice.text = "Searching for".localisedString()
        //                        self.lblFindYourDevice2.text = "your printer…".localisedString()
        //                    }
        //                }
        //                else{
        //                    self.timerCounter()
        //                }
        //            }
        //        }else{
        //            //            let bluetoothPopup = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpBTRequestViewController") as! PopUpBTRequestViewController
        //            //            self.present(bluetoothPopup, animated: true, completion: nil)
        //            self.bluetoothManager = CBCentralManager()
        //            self.bluetoothManager.delegate = self
        //        }
    }
    
    func setupFirstConnection()
    {
        appDelegate().isConnectInProgress = true
        appDelegate().isConnectedToPrinter = false
        PrinterQueueManager.sharedController.Is_Busy = false
        connectedAccessory = EAAccessoryManager.shared().connectedAccessories[0]
        appDelegate().currentAccessory = connectedAccessory
        if(connectedAccessory?.protocolStrings[0] == MFI_PROTOCOL_STRING)
        {
            print("CONNECTED ACESSORY IS ZIP SNAP")
            // appDelegate().isZIPSNAPConnected = true
            appDelegate().isConnectedToPrinter = true
        }
        UserDefaults.standard.set(connectedAccessory?.serialNumber, forKey: "lastConnectedAccessory")
        UserDefaults.standard.synchronize()
        Accessory_Manager.shared.getDevices()
        Accessory_Manager.shared.setUpSession(accessory: connectedAccessory!)
        appDelegate().isConnectInProgress = false
        appDelegate().isConnectedToPrinter = true
        self.connectionObservers()
        NotificationCenter.default.post(name: Notification.Name("AccessoryConnect"), object: nil)
        if self.comeFrom == "galleryVc"
        {
            self.dismiss(animated: true, completion: nil)
        }
        else{
            if self.comeFromStr == "updateFailedVC"
            {
                if let navController = self.navigationController {
                    for controller in navController.viewControllers {
                        if controller is FirmwareUpdateAvailableViewController { // Change to suit your menu view controller subclass
                            //                            NotificationCenter.default.post(name: NSNotification.Name("dismissCantSeePrinterVC"), object: nil)
                            navController.popToViewController(controller, animated:true)
                            break
                        }
                    }
                }
            }
            else if self.comeFromStr == "firmwareVC" {
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: false)
            }
            else if goToQuickTipsOnboarding == "quickTipsOnboarding"
            {
                let printerConnectedVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "PrinterConnectedViewController") as! PrinterConnectedViewController
                self.view.addSubview(printerConnectedVC.view)
                self.addChildViewController(printerConnectedVC)
            }
        }
    }
}

extension ConnectivityViewController: showDevicePicker {
    func showBluetoothDevicePicker() {
        self.showDevicePicker()
    }
}
