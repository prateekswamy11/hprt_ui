//
//  ConnectivityViewTableviewDelegate.swift
//  Polaroid MINT
//
//  Created by MacBook003 on 25/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CopilotAPIAccess

extension ConnectivityViewController : UITableViewDelegate, UITableViewDataSource {
    
    func hideUnhideUIControls(isHidden: Bool) {
        self.lblPrinterFound.isHidden = isHidden == false ? false : true
        self.lblPrinterFound2.isHidden = isHidden == false ? false : true
        self.lblFindYourDevice.isHidden = isHidden == false ? true : false
        self.lblFindYourDevice2.isHidden = isHidden == false ? true : false
    }
    
    //MARK: - // ----------------- UITableView Delegate -------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let devicesCount = EAAccessoryManager.shared().connectedAccessories.count
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reloadDeviceTableView),
            name: NSNotification.Name(rawValue: "AccessoryDisConnect"),
            object: nil)
        if devicesCount == 0 {
            self.hideUnhideUIControls(isHidden: true)
            //            self.lblPrinterFound.isHidden = true
            //            self.lblPrinterFound2.isHidden = true
            //            self.lblFindYourDevice.isHidden = false
            //            self.lblFindYourDevice2.isHidden = false
            tableView.isScrollEnabled = false
            if goToQuickTipsOnboarding == "quickTipsOnboarding"{
                self.lblFindYourDevice.textAlignment = .center
                self.lblFindYourDevice2.textAlignment = .center
                self.lblFindYourDevice2.font = UIFont(name:"Graphik-Regular", size: 32.0)
                self.lblWeCouldntFindPrinter.textAlignment = .center
                self.lblWeCouldntFindPrinter.font = UIFont(name:"Graphik-Regular", size: 32.0)
            }
        } else if devicesCount == 1 {
            if goToQuickTipsOnboarding == "quickTipsOnboarding"{
                self.lblPrinterFound.textAlignment = .center
                self.lblPrinterFound2.textAlignment = .center
                self.lblPrinterFound2.font = UIFont(name:"Graphik-Regular", size: 32.0)
            }
            self.hideUnhideUIControls(isHidden: false)
            tableView.isScrollEnabled = true
        } else {
            self.hideUnhideUIControls(isHidden: false)
            //            self.lblPrinterFound.isHidden = false
            //            self.lblPrinterFound2.isHidden = false
            //            self.lblFindYourDevice.isHidden = true
            //            self.lblFindYourDevice2.isHidden = true
            tableView.isScrollEnabled = true
        }
        
        return devicesCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ConnectivityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ConnectivityTableViewCell", for: indexPath) as! ConnectivityTableViewCell
        if(EAAccessoryManager.shared().connectedAccessories.count > 0)
        {
            let accessory = EAAccessoryManager.shared().connectedAccessories[indexPath.section]
            cell.lblDeviceName.text =  accessory.name.localisedString()
            cell.btnConnectPrinter.tag = indexPath.section
            if let macId = accessory.value(forKey: "macAddress")
            {
                let macStr = macId as! String
                print("mac id bbbb", macId)
                let substring = macStr.split(separator: ":")
                let macLastChar = "\(substring[4]):" + "\(substring[5])"
                cell.lblDeviceName.text?.append(" \(macLastChar)".localisedString())
                let sepMacID = macStr.components(separatedBy: ":").joined()
                CONNECT_DEVICE_MAC_ADDRESS = sepMacID
                Copilot.instance.report.log(event:
                    ThingDiscoveredAnalyticsEvent(thingID: CONNECT_DEVICE_MAC_ADDRESS))
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ConnectivityTableViewCell
        cell.contentView.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    }
    
    //MARK: - Scroll View Delegate Functions -
    private func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
}
