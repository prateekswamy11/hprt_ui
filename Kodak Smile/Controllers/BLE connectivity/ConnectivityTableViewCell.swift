//
//  ConnectivityTableViewCell.swift
//  Snaptouch_Polaroid
//
//  Created by MAXIMESS114 on 22/11/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit

class ConnectivityTableViewCell: UITableViewCell {

    //MARK: - IBOutlets -
    @IBOutlet weak var lblDeviceName: UILabel!
    @IBOutlet weak var imgViewBluetoothIcon: UIImageView!
    @IBOutlet weak var btnConnectPrinter: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnConnectPrinter.titleLabel?.text = "connect".localisedString()
    }
}
