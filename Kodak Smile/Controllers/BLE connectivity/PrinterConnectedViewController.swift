//
//  PrinterConnectedViewController.swift
//  Polaroid MINT
//
//  Created by MacMini001 on 18/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PrinterConnectedViewController: UIViewController {

    //MARK: - Outlets -
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblPrinterConnected: UILabel!
    
    //MARK: - View LifeCycle Functions-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
    }

    //MARK: - IBActions -
    @IBAction func goBackToQuickTipsBtnClicked(_ sender: UIButton)
    {
        if let navController = self.navigationController {
            for controller in navController.viewControllers {
                if controller is QuickTipsViewController{ // Change to suit your menu view controller subclass
                    navController.popToViewController(controller, animated:true)
                    goToQuickTipsOnboarding = ""
                    self.view.removeFromSuperview()
                    self.removeFromParentViewController()
                    break
                }
            }
        }
    }
    
    @IBAction func goBackBtnClicked(_ sender: UIButton)
    {
        if let navController = self.navigationController {
            for controller in navController.viewControllers {
                if controller is QuickTipsPrinterConnectViewController{ // Change to suit your menu view controller subclass
                    navController.popToViewController(controller, animated:true)
                    goToQuickTipsOnboarding = ""
                    self.view.removeFromSuperview()
                    self.removeFromParentViewController()
                    break
                }
            }
        }
    }
    
    //MARK: - Functions -
    func localizedStrings()
    {
        self.lblPrinterConnected.text = "Your printer is connected".localisedString()
        self.btnOk.setTitle("Create SMILE Images".localisedString(), for: .normal)
    }
    
}
