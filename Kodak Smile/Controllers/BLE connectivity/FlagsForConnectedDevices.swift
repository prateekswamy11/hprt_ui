//
//  FlagsForConnectedDevices.swift
//  Kodak Smile
//
//  Created by maximess175 on 03/06/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import UIKit

enum ConnectedDevice {
    case smile
    case classic
    case stepTouch
    case stepPrinter
    case mini
    case dock
}

enum HardwareProtocol {
    case dsg
    case foxconn
}
