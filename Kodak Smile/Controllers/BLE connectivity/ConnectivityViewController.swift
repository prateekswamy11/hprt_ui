//
//  ConnectivityViewController.swift
//  Snaptouch_Polaroid
//
//  Created by MAXIMESS114 on 21/11/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit
import CopilotAPIAccess
import CoreBluetooth

var connectedAccessory : EAAccessory?
var isInitailConnect: Bool = true
var isBluetoothOn: Bool = false

class ConnectivityViewController: UIViewController {
    
    //MARK: - IBOutlets -
    @IBOutlet weak var deviceTableView: UITableView!
    @IBOutlet weak var lblPrinterFound: UILabel!
    @IBOutlet weak var lblPrinterFound2: UILabel!
    @IBOutlet weak var lblFindYourDevice: UILabel!
    @IBOutlet weak var lblFindYourDevice2: UILabel!
    @IBOutlet weak var lblPrinterFoundSmall: UILabel!
    @IBOutlet weak var btnSearchAgain: UIButton!
    @IBOutlet weak var btnContactSupport: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnSkip2: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgVwForBtnBack: UIImageView!
    @IBOutlet weak var viewConnectingGif: UIView!
    @IBOutlet weak var imgVwConnectingPrinter: UIImageView!
    @IBOutlet weak var lblDevice: UILabel!
    @IBOutlet weak var viewHorizontalLine: UIView!
    @IBOutlet weak var constraintTopDeviceTableView: NSLayoutConstraint!
    @IBOutlet weak var constraintTopLabel: NSLayoutConstraint!
    @IBOutlet weak var lblWeCouldntFindPrinter: UILabel!
    @IBOutlet weak var viewCouldntFindPrinter: RadialGradientView!
    @IBOutlet weak var btnSearchAgainCouldntFind: UIButton!
    @IBOutlet weak var btnCntactSupportCouldntFind: UIButton!
    @IBOutlet weak var lblMakeSure: UILabel!
    @IBOutlet weak var lblPlugYourPrinter: UILabel!
    @IBOutlet weak var lblPlaceYourPrinter: UILabel!
    @IBOutlet weak var btnBackCouldntFind: UIButton!
    
    //MARK: - Variables -
    var timer : Timer?
    var manager = EAAccessoryManager.shared()
    var comeFrom : String?
    var accessoryList = NSMutableArray()
    var imageDataArray = [[String : Any]]()
    var printImageArray = [UIImage]()
    var countOfCopies = 1
    var isApplyForAll = true
    var lastContentOffset: CGFloat = 0
    let loaderConnectA = UIImage(gifName: "Connecting_A.gif")
    let loaderConnectB = UIImage(gifName: "Connecting_B.gif")
    var loaderGifTimer: Timer!
    var counter = 5
    var comeFromStr = String()
    var goToMailComposer = String()
    var bluetoothManager: CBCentralManager!
    //var viewHelpOverlay = ConnectivityHintViewController()
    
    // Zeplin - attributed string couldn't find printer.
    let attributedString = NSMutableAttributedString(string: "We couldn\'t find your printer", attributes: [
        .font: UIFont(name: "Graphik-Regular", size: 32.0)!,
        .foregroundColor: UIColor(white: 1.0, alpha: 1.0),
        .kern: 0.0
    ])
    
    //MARK: - View LifeCycle Delegates -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblPrinterFound.alpha = 1
        self.lblPrinterFound2.alpha = 1
        self.lblPrinterFoundSmall.alpha = 0
//        self.bluetoothManager = CBCentralManager()
//        self.bluetoothManager.delegate = self
        self.detectIphone5sDevice()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // it is used for reload the tableview to show connected device
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.timerCounter), userInfo: nil, repeats: true)
        self.localizedStrings()
        self.imgVwConnectingPrinter.isHidden = true
        self.lblDevice.isHidden = false
        self.viewHorizontalLine.isHidden = false
        self.deviceTableView.isHidden = false
        self.hideBackBtn()
        self.hideCouldntFindView()
        attributedString.addAttribute(.font, value: UIFont(name: "Graphik-Bold", size: 32.0)!, range: NSRange(location: 17, length: 12))
        self.lblWeCouldntFindPrinter.attributedText = attributedString
//        NotificationCenter.default.addObserver(self, selector: #selector(checkBluetooth), name: Notification.Name("checkBluetooth"), object: nil)
        self.addObservers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
         NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
//         if isBluetoothOn{
          self.showDevicePicker()
          self.deviceTableView.reloadData()
//         }

        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.findYourDevice.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.timer?.invalidate()
        self.comeFrom = ""
        deviceTableView.isUserInteractionEnabled = true
        customLoader.stopGifLoader()
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.btnBack.isHidden = false
//        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "checkBluetooth"), object: nil)
    }
    
    //MARK: - IBActions -
    @IBAction func cancelButtonClicked(_ sender: Any) {
        
        if comeFrom == "galleryVc"
        {
            self.dismiss(animated: true, completion: nil)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func searchAgainButtonClicked(_ sender: Any) {
        self.scanForUnpairedDevices()
    }
    
    @IBAction func connectPrinterBtnClicked(_ sender: UIButton) {
        print("connect btn clicked")
        customLoader.showConnectivityActivityIndicator(viewController: self.viewConnectingGif)
        self.lblPrinterFound.text = "Connecting to".localisedString()
        self.lblPrinterFound2.text = "your printer…".localisedString()
        self.lblPrinterFoundSmall.text = "Connecting to your printer…".localisedString()
        self.deviceTableView.isHidden = true
        self.lblDevice.isHidden = true
        self.viewHorizontalLine.isHidden = true
        self.btnBack.isHidden = true
        self.btnSkip2.isHidden = true
        self.viewConnectingGif.isHidden = false
        self.imgVwConnectingPrinter.isHidden = false
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
            customLoader.hideActivityIndicator()
            if UserDefaults.standard.bool(forKey: "onboarding_started"){
                Copilot.instance.report.log(event: OnboardingEndedAnalyticsEvent(flowID: "fromPush", screenName: "onboarding_ended"))
                UserDefaults.standard.set(false, forKey: "onboarding_started")
            }
            self.viewConnectingGif.isHidden = true
            self.connectSelectedPrinter(sender.tag)
        }
    }
    
    @IBAction func goToMainGalleryBtnClicked(_ sender: UIButton)
    {
        if comeFromOnboardingPageVC == "onboardingVC"
        {
            let tutorialSkipAnalyticsEvent = TutorialSkipAnalyticsEvent(screenNumber: String(2))
            Copilot.instance.report.log(event: tutorialSkipAnalyticsEvent)
            UserDefaults.standard.set(true, forKey: "onboardingSkipped")
            if SingletonClass.sharedInstance.actionInitiatedByUser {
                let quickTipsVC = storyboards.QuickTips.instantiateViewController(withIdentifier: "QuickTipsViewController") as! QuickTipsViewController
                self.navigationController?.pushViewController(quickTipsVC, animated: true)
            } else {
                let galleryVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(galleryVC, animated: true)
            }
        }
        else if goToQuickTipsOnboarding == "quickTipsOnboarding"
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnContactSupportClicked(_ sender: Any)
    {
        goToMailComposer = "mailComposer"
        MailComposer().openContactMailer(viewController: self)
    }
    
    //MARK: - Functions -
//    @objc func checkBluetooth() {
//        isBluetoothOn = true
//        self.showDevicePicker()
//    }
    
    @objc func reloadDeviceTableView() {
        self.deviceTableView.reloadData()
        if EAAccessoryManager.shared().connectedAccessories.count == 0
        {
            self.lblFindYourDevice.text = "Searching for".localisedString()
            self.lblFindYourDevice2.text = "your printer…".localisedString()
        }
    }
    
    @objc func willEnterForeground() {
        if EAAccessoryManager.shared().connectedAccessories.count == 0{
            self.scanForUnpairedDevices()
        }
    }
    
    func hideBackBtn() {
        if comeFromOnboardingPageVC == "onboardingVC"{
            self.btnBack.isHidden = true
            self.btnBackCouldntFind.isHidden = true
        }else if self.comeFromStr == "HomeVCorSettingsVC"{
            self.btnBack.isHidden = false
            self.btnBackCouldntFind.isHidden = false
        }
        if comeFromOnboardingPageVC == "onboardingVC"{
            self.btnSkip.isHidden = false
            self.btnSkip2.isHidden = false
        }else{
            self.btnSkip.isHidden = true
            self.btnSkip2.isHidden = true
        }
    }
    
    
    func hideCouldntFindView() {
        if comeFromOnboardingPageVC == "onboardingVC" || goToQuickTipsOnboarding == "quickTipsOnboarding" {
            self.viewCouldntFindPrinter.isHidden = true
        }
    }
    
    func localizedStrings() {
        self.lblFindYourDevice.text = "Searching for".localisedString()
        self.lblFindYourDevice2.text = "your printer".localisedString() + "…"
        self.lblPrinterFound.text = "Printer found".localisedString()
        self.lblPrinterFound2.text = "let's hook it up!".localisedString()
        self.lblPrinterFoundSmall.text = "Printer found".localisedString()
        self.btnSkip.setTitle("Skip".localisedString(), for: .normal)
        self.btnSkip2.setTitle("Skip".localisedString(), for: .normal)
        self.btnSearchAgain.setTitle("search again".localisedString(), for: .normal)
        self.btnContactSupport.setTitle("Contact support".localisedString(), for: .normal)
        self.lblDevice.text = "Devices".localisedString()
        self.lblWeCouldntFindPrinter.text = "We couldn't find your printer".localisedString()
        self.lblMakeSure.text = "Make sure your \nprinter is turned on".localisedString()
        self.lblPlugYourPrinter.text = "Plug your printer into\na power source".localisedString()
        self.lblPlaceYourPrinter.text = "Place your printer \nclose to your mobile \ndevice".localisedString()
        self.btnSearchAgainCouldntFind.setTitle("Search Again".localisedString(), for: .normal)
        self.btnCntactSupportCouldntFind.setTitle("Contact support".localisedString(), for: .normal)
    }
    
    func detectIphone5sDevice() {
        if getCurrentIphone() == "5"{
            self.constraintTopLabel.constant = 30.0
            self.constraintTopDeviceTableView.constant = 220.0
        }else{
            print("Unknown")
        }
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(AccessoriesDisConnect),
            name: NSNotification.Name(rawValue: "AccessoryDisConnectAfterCallBack"),
            object: nil)
    }
    
    func connectionObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.Accessories_dis_connected_called),
            name: NSNotification.Name(rawValue: "Accessories_dis_connected"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.Accessories_Connected_called),
            name: NSNotification.Name(rawValue: "AccessoryConnectFinish"),
            object: nil)
    }
    
    func GoToGallery() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func timerCounter()  {
        if (timer != nil) {
            //if self.comeFrom == "printPriveVC"{
            Accessory_Manager.shared.getDevices()
            if EAAccessoryManager.shared().connectedAccessories.count == 1 && appDelegate().isConnectedToPrinter == false{
                //self.dismiss(animated: true, completion: nil)
                self.setupFirstConnection()
                self.reloadDeviceTableView()
            }
            //}
        }
    }
}
