//
//  FacebookAlbum_Para.swift
//  FacebookSwift
//
//  Created by maximess142 on 30/01/18.
//  Copyright © 2018 maximess142. All rights reserved.
//

import UIKit

class FacebookAlbum_Para: NSObject
{
    var fbAlbumName = String()
    var fbAlbumPhotosCount = Int()
    var fbAlbumId = String()
    var fbAlbumPhotoLinkArray = [String]()
    var fbAlbumThumbnailUrl = String()
}
