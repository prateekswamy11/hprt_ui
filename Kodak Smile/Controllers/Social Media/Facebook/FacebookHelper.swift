//
//  FacebookHelper.swift
//  Snaptouch_Polaroid
//
//  Created by maximess142 on 01/02/18.
//  Copyright © 2018 maximess142. All rights reserved.
//

import UIKit
//import FacebookCore
//import FacebookLogin
import FBSDKCoreKit
import FBSDKLoginKit


class FacebookHelper: NSObject {

    let myGroup = DispatchGroup()
    let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)

    func loginToFacebook(viewController : UIViewController)
    {
        
        let storage = HTTPCookieStorage.shared
        for cookie in storage.cookies! {
            let domainstr = cookie.domain
            if domainstr == ".facebook.com"
            {
                storage.deleteCookie(cookie)
            }
            
        }
        
        let loginManager = LoginManager()
        // Added this so that after logout it asks you to enter new credentials.
        // Check - https://stackoverflow.com/a/30093967/6037876
         loginManager.logOut()
        
        AccessToken.current = nil
        Profile.current = nil
        
        loginManager.logIn(permissions: ["email","public_profile","user_photos"], from: nil) {
            (result, error) -> Void in
            customLoader.stopGifLoader()
            //if we have an error display it and abort
            if let error = error {
                print(error)
                return
            }
            //make sure we have a result, otherwise abort
            guard let result = result else { return }
            //if cancelled nothing todo
            if result.isCancelled {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "fbUserCancelledLogin"), object: nil)
                return
            }
            else {
                print("Logged in!")
                customLoader.showActivityIndicator(viewController: viewController.view)
                self.fetchAlbumsFromFacebook()
                self.fetchUserProfileFacebook()
                if isFacebookLogin == false {
                    UserDefaults.standard.set(true, forKey: "fetchFbAlbum")
                }
                UserDefaults.standard.setValue(true, forKey: "isLoggedInToFacebookKodak")
            }
        }
    }
    
    func logoutOffFacebook()
    {
        let loginManager = LoginManager()
        loginManager.logOut()
        AccessToken.current = nil
        Profile.current = nil
        self.clearFacebookCookies()
        
        let dbLayer = DBLayer()
        dbLayer.deleteFacebookAlbums()
        dbLayer.deleteFacebookPhotos()
        
        UserDefaults.standard.setValue(false, forKey: "isLoggedInToFacebookKodak")
        UserDefaults.standard.setValue("", forKey: "facebookUserName")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "userLoggedOut"), object: "Facebook")
    }
    
    func clearFacebookCookies() {
        
        let storage = HTTPCookieStorage.shared
        for cookie in storage.cookies! {
            let domainstr = cookie.domain
            if domainstr == ".facebook.com"
            {
                storage.deleteCookie(cookie)
            }
        }
    }
    
    func fetchUserProfileFacebook()
    {
        if AccessToken.current != nil {
            //login successfull, now request the fields we like to have in this case first name and last name
            GraphRequest(graphPath: "me", parameters: ["fields" : "first_name, last_name, email"]).start() {
                (connection, result, error) in
                //if we have an error display it and abort
                if error != nil {
                    return
                }
                //parse the fields out of the result
                if
                    let fields = result as? [String:Any],
                    let firstName = fields["first_name"] as? String
                {
                    //Check user is login from facebook or  not.
                    if isFacebookLogin == true {
                        let userDict : [String:Any] = [ "data": fields]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fblogin"), object: nil, userInfo: userDict)
                    }
                    UserDefaults.standard.setValue(firstName, forKey: "facebookUserName")
                    NotificationCenter.default.post(name: NSNotification.Name("reloadSocialMedia"), object: nil)
                }
            }
        }
    }
    
    func fetchAlbumsFromFacebook()
    {
        // For more complex open graph stories, use `FBSDKShareAPI`
        // with `FBSDKShareOpenGraphContent`
        /* make the API call */
        let graphRequest : GraphRequest = GraphRequest(graphPath: "/me/albums?fields=count,id,name,picture&limit=1000", parameters: [:] )
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            guard let resultDict = result as? [String:Any] else {
                return
            }
            
            guard let dataArray = resultDict["data"] as? [[String:Any]] else {
                return
            }
            
            if let albumArray = dataArray as? [[String:Any]]
            {
                for album in albumArray
                {
                    self.myGroup.enter()
                    guard let album_id = album["id"] as? String else {
                        return
                    }
                    guard let album_name = album["name"] as? String else {
                        return
                    }
                    guard let album_count = album["count"] as? Int else {
                        return
                    }
                    let paraFacebook = FacebookAlbum_Para()
                    paraFacebook.fbAlbumId = album_id
                    paraFacebook.fbAlbumName = album_name
                    paraFacebook.fbAlbumPhotosCount = album_count
                    
                    guard let pictureDict = album["picture"] as? [String:Any] else {
                        return
                    }
                    guard let pictureData = pictureDict["data"] as? [String:Any] else {
                        return
                    }
                    
                    if let dataDict = pictureData as? [String:Any]
                    {
                        if let url = dataDict["url"] as? String
                        {
                            paraFacebook.fbAlbumThumbnailUrl = url
                        }
                    }
                    
                    self.queue.sync() { () -> Void in
                        self.fetchAlbumDetailsByID(albumPara: paraFacebook)
                    }
                }
            }
            
            self.myGroup.notify(queue: .main) {
                print("Finished all requests.")
                if isFacebookLogin == false {
                    UserDefaults.standard.set(true, forKey: "fetchFbAlbum")
                }
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "fbUserChanged"), object: nil)

                    customLoader.hideActivityIndicator()
                }
            }
        })
    }
    
    func fetchAlbumDetailsByID(albumPara : FacebookAlbum_Para)
    {
        let album_id = albumPara.fbAlbumId
        
        let graphRequest : GraphRequest = GraphRequest(graphPath: "/\(album_id)/photos?fields=id,picture,images,album&limit=1000", parameters: [:] )
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            //print(result)
            if let resultDict = result as? [String:Any]
            {
                self.queue.sync() { () -> Void in

                var photoLinkArray = [String]()
                if let dataArray = resultDict["data"] as? [[String:Any]]
                {
                    //print("dataArray : \(dataArray)")
                    
                    for data in dataArray
                    {
                        //print("data : \(data)")
                        
                        if let imagesArray = data["images"] as? [[String:Any]]
                        {
                            if imagesArray.count > 0
                            {
                                if let imageDict = imagesArray[0] as? [String:Any]
                                {
                                    if let photoUrl = imageDict["source"] as? String
                                    {
                                        photoLinkArray.append(photoUrl)
                                    }
                                }
                            }
                        }
                    }
                }
                
                albumPara.fbAlbumPhotoLinkArray = photoLinkArray
                //self.fbPhotosArray.append(albumPara)
                }
                self.queue.sync() { () -> Void in
                    DBLayer().insertFacebookAlbum(facebookAlbumPara: albumPara)
                }
                self.queue.sync() { () -> Void in
                    self.myGroup.leave()
                }
            }
        })
        
    }
}
