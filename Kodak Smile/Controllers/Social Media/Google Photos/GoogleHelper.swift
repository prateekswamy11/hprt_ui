//
//  GoogleHelper.swift
//  gPhotos
//
//  Created by maximess142 on 14/02/18.
//  Copyright © 2018 Maximess. All rights reserved.
//

import UIKit
import GoogleSignIn
import Alamofire
import GTMSessionFetcher
import GoogleAPIClientForREST

let kClientID = "770157866693-hckvhlj9n6ajaigm5s90sqv47k28vto5.apps.googleusercontent.com"
//let kClientID = "129929426917-5j38lvdgojul9o54bqqlc1fbgm2u50uu.apps.googleusercontent.com"
//let kClientID = "770157866693-d77494vp8kfefu1tf8qm5niqovo1eria.apps.googleusercontent.com"


class GoogleHelper: NSObject, URLSessionDelegate
{
    var viewController = UIViewController()
    var arrayAlbums = [GooglePhotosPara]()
    var arrayOfAllPhotos = [String]()
    
    let myGroup = DispatchGroup()
    let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
    
    func logoutOffGoogle()
    {
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance()?.disconnect()
        UserDefaults.standard.setValue(false, forKey: "isLoggedInToGoogleKodak")
        UserDefaults.standard.setValue("", forKey: "googleUserName")
        
        let dblayer = DBLayer()
        dblayer.deleteGooglePhotosUrl()
        dblayer.deleteGoogleAlbums()
        dblayer.deleteGooglePhotos()
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "userLoggedOut"), object: "Google")
    }
    
    ///API
    
    func getAlbums(accessToken : String, strUrl : String)
    {
        print(strUrl)
        let request = NSMutableURLRequest(url: URL(string:strUrl)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 120)
        
        request.httpMethod = "GET" // POST ,GET, PUT What you want
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
            
            if data != nil
            {
                do {
                    self.myGroup.enter()
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        guard let responseDictNew = jsonResult.value(forKey: "mediaItems") as? NSArray
                            else {
                                customLoader.hideActivityIndicator()
                                return
                        }
                        self.parseGoogleArrayResponse(responseDictNew: responseDictNew)
                        
                        // If there is more photos, call this method recursively
                        if let nextPageToken = jsonResult.value(forKey: "nextPageToken")
                        {
                            let strUrl = "https://photoslibrary.googleapis.com/v1/mediaItems?alt=json&pageSize=100&access_token=\(accessToken)&pageToken=\(nextPageToken)"
                            
                            self.getAlbums(accessToken: accessToken, strUrl: strUrl)
                        }
                        else
                        {
                            // Now we got all the photos, so insert them in DB
                            let glAlbumPara = GooglePhotosPara()
                            glAlbumPara.googleAlbumPhotoLinkArray = self.arrayOfAllPhotos
                            
                            // Keeping this array for the album logic in ST and ZIP
                            self.arrayAlbums.append(glAlbumPara)
                            
                            let operationQueue = OperationQueue()
                            let operation1 = BlockOperation(block: { () -> Void in
                                DispatchQueue.main.async(execute: {
//                                    DBLayer().insertGoogleAlbum(glAlbumPara: glAlbumPara)
                                    DBLayer().insertGooglePhotosUrl(urlArray: self.arrayOfAllPhotos)
                                })
                            })
                            
                            operationQueue.addOperation(operation1)
                            
                            operation1.completionBlock =
                                {
                                    self.myGroup.leave()
                            }
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "googleImagesFetched"), object: self.arrayAlbums)
                        }
                    }
                }
                catch let error as NSError {
                    print("hello", error.localizedDescription)
                }
                //                self.myGroup.leave()
            }
            
            self.myGroup.notify(queue: .main) {
                print("Finished all requests.")
                
                //                        DispatchQueue.main.async(execute: {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "googleImagesFetched"), object: self.arrayAlbums)
                //                        })
            }
            //            self.myGroup.leave()
        }
        dataTask.resume()
    }
    
    func parseGoogleArrayResponse(responseDictNew : NSArray)
    {
        if let localArrayOfAlbums = responseDictNew as? [[String:Any]]
        {
            for dict in localArrayOfAlbums
            {
                guard let mediaGroup = dict["baseUrl"] as? String
                   
                    else {
                        return
                }
               
                if !(((dict["mimeType"] as? String)?.contains("mp4")) ?? false || (dict["mimeType"] as? String)?.contains("mov") ?? false) {
                    arrayOfAllPhotos.append(mediaGroup)
                }
            }
            print(arrayOfAllPhotos.count)
        }
    }
    /*
    func getPhotosFromAlbums(userId : String, accessToken : String, glAlbumPara: GooglePhotosPara)
    {
        let strUrl = "https://picasaweb.google.com/data/feed/api/user/\(userId)/albumid/\(glAlbumPara.googleAlbumId)?alt=json&access_token=\(accessToken)&imgmax=d"

        print(strUrl)
        let request = NSMutableURLRequest(url: URL(string:strUrl)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
        
        request.httpMethod = "GET" // POST ,GET, PUT What you want
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
            //Akshay Solve crash detect on crashanalitics
            
            if data != nil
            {
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        let responseDict = jsonResult.value(forKey: "feed") as! NSDictionary
                        
                        let responseDictNew = responseDict as! [String:Any]
                        //print(responseDictNew)
                        if let localArrayOfAlbums = responseDictNew["entry"] as? [[String:Any]]
                        {
                            var arrayOfURL = [String]()
                            for dict in localArrayOfAlbums
                            {
                                let mediaGroup = dict["media$group"] as? [String:Any]
                                let mediaContent = mediaGroup!["media$content"] as? [[String:Any]]
                                
                                // 0 - Lowest quality
                                // 1 - Medium quality
                                // 2 - Highest quality
                                
                                let imagelURL = mediaContent![0]["url"] as! String
                                // print(imagelURL)
                                
                                // Exclude videos from the list
                                if !imagelURL.contains(".mp4")
                                {
                                    arrayOfURL.append(imagelURL)
                                }
                            }
                            
                            glAlbumPara.googleAlbumPhotoLinkArray = arrayOfURL
                            self.arrayAlbums.append(glAlbumPara)
                            print(arrayOfURL.count)
                            
                            let operationQueue = OperationQueue()
                            let operation1 = BlockOperation(block: { () -> Void in
                                DispatchQueue.main.async(execute: {
                                    DBLayer().insertGoogleAlbum(glAlbumPara: glAlbumPara)
                                })
                            })
                            
                            operationQueue.addOperation(operation1)
                            
                            operation1.completionBlock =
                                {
                                    self.myGroup.leave()
                            }
                        }
                            // When there are no images in folder, at least save the folder
                        else
                        {
                            let operationQueue = OperationQueue()
                            let operation1 = BlockOperation(block: { () -> Void in
                                DispatchQueue.main.async(execute: {
                                    self.arrayAlbums.append(glAlbumPara)
                                    
                                    DBLayer().insertGoogleAlbum(glAlbumPara: glAlbumPara)
                                })
                            })
                            
                            operationQueue.addOperation(operation1)
                            
                            operation1.completionBlock =
                                {
                                    self.myGroup.leave()
                            }
                        }
                    }
                } catch let error as NSError {
                    print("hey", error.localizedDescription)
                    self.myGroup.leave()
                    customLoader.hideActivityIndicator()
                }
                
            }
            
        }
        dataTask.resume()
    }
    */
    func refreshAccessTokenAndFetchAlbums()
    {
        let requestString = "https://accounts.google.com/o/oauth2/token"
        
        let refreshToken = UserDefaults.standard.value(forKey: "googleRefreshToken") as! String
        let string = "client_id=\(kClientID)&refresh_token=\(refreshToken)&grant_type=refresh_token"
        let postData = string.data(using: .utf8)
        
        let request = NSMutableURLRequest(url: URL(string:requestString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
        request.httpMethod = "POST" // POST ,GET, PUT What you want
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
            
            do {
                if let receivedData = data
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: receivedData, options: []) as? [String : Any]
                    {
                        let accessToken = jsonResult["access_token"] as? String
                        //print("New access token = ", accessToken)
                        UserDefaults.standard.setValue(accessToken, forKey: "googleAccessToken")
                        UserDefaults.standard.setValue(true, forKey: "isLoggedInToGoogleKodak")
                        // Fetch google photos
                        let strUrl = "https://photoslibrary.googleapis.com/v1/mediaItems?alt=json&pageSize=100&access_token=\(accessToken!)"
                        
                        GoogleHelper().getAlbums(accessToken: accessToken ?? string, strUrl: strUrl)
                        
                        
                    }
                }
            } catch let error as NSError {
                print("hey", error.localizedDescription)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "googleRefreshTokenExpired"), object: nil)
            }
        }
        dataTask.resume()
    }
    
    func getCurrentViewController() -> UIViewController? {
        
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                
                 customLoader.hideActivityIndicator()
                currentController = currentController.presentedViewController
               
            }
              customLoader.hideActivityIndicator()
            return currentController
          
        }
          customLoader.hideActivityIndicator()
        return nil
        
    }
    
    func removeGoogleData() {
        UserDefaults.standard.removeObject(forKey: "isLoggedInToGoogleKodak")
        DBLayer().deleteGooglePhotosUrl()
        DBLayer().deleteGoogleAlbums()
        DBLayer().deleteGooglePhotos()
        GoogleHelper().logoutOffGoogle()
    }
}
