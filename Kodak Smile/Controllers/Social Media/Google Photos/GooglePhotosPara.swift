//
//  GooglePhotosPara.swift
//  gPhotos
//
//  Created by maximess142 on 13/02/18.
//  Copyright © 2018 Maximess. All rights reserved.
//

import UIKit

class GooglePhotosPara: NSObject
{
    var googleAlbumName = String()
    var googleAlbumId = String()
    var googleAlbumPhotoLinkArray = [String]()
}
