//
//  DropboxHelper.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 03/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//
/*
import UIKit
import SwiftyDropbox

class DropboxHelper: NSObject {
    
    var arrDropboxImages = [String]()
    var dropboxImages = [Data]()
    let client = DropboxClientsManager.authorizedClient
    
    //Fetch all images from DropBox
    func getImageFromDropbox(path: String, completionHandler: @escaping([String]) -> ()) {
        if UserDefaults.standard.value(forKey: "isLoggedInToDropboxKodak") as? Bool == true
        {
            //Get list of folder of dropbox by set (path: "/")
            //Or you can get folder inside a folder by set (path: "/Photos")
            client?.files.listFolder(path: path).response(completionHandler: { (objList, error) in
                if let resultList = objList {
                    
                    //Create a for loop for getting all the entities individually
                    for entry in resultList.entries {
                        
                        //Check if file have metadata or not
                        if let fileMetadata = entry as? Files.FileMetadata {
                            if self.isFileImage(filename: fileMetadata.name) == true {
                                self.arrDropboxImages.append(fileMetadata.pathDisplay!)
                                DBLayer().insertDropboxFilepath(urlArray: fileMetadata.pathDisplay!)
                            }
                        } else {
                            self.getImageFromDropbox(path: entry.pathDisplay!, completionHandler: { (data) in
                                for i in data.unique() {
                                    if self.isFileImage(filename: entry.name) == true {
                                        self.arrDropboxImages.append(i)
                                        DBLayer().insertDropboxFilepath(urlArray: i)
                                    }
                                }
                                completionHandler(self.arrDropboxImages)
                            })
                        }
                    }
                completionHandler(self.arrDropboxImages)
                } else {
                    print(error!)
                }
            })
        }
    }
    
    func getImages(filePath: String, completionHandler: @escaping(String) -> ()) {
        
        var imageData = [Data]()
//        var photos = [String]()
//        for data in filePath {
            //Get Path for save image in document directory
            client!.files.download(path: filePath).response { (response, error) in
                if let metadata = response?.1 {
                    imageData.append(metadata)
                    let resultData = metadata.base64EncodedData(options: NSData.Base64EncodingOptions.endLineWithLineFeed)
                    //Convert NSData to NSString
                    let resultNSString = NSString(data: resultData as Data, encoding: String.Encoding.utf8.rawValue)!
                    
                    //Convert NSString to String
                    let resultString = resultNSString as String
                    DBLayer().insertDropboxPhotos(url: resultString, filePath: filePath)
                    completionHandler(resultString)
//                    photos.append(resultString)
                } else {
                    print(error!)
                }
//            }
//            if isSingleDownload == true {
//                break
//            }
        }
//        DBLayer().insertDropboxPhotos(url: photos, filePath: filePath)
//        completionHandler(imageData)
    }
    
    func logoutDropbox() {
        self.dropboxImages.removeAll()
        self.arrDropboxImages.removeAll()
        DBLayer().deleteDropboxAlbums()
        DropboxClientsManager.unlinkClients()
        DropboxClientsManager.resetClients()
    }

    
    //MARK: check for file type
    private func isFileImage(filename:String) -> Bool {
        let lastPathComponent = (filename as NSString).pathExtension.localizedLowercase
        return lastPathComponent == "jpg" || lastPathComponent == "png" || lastPathComponent == "jpeg"
    }
    
    //to get document directory path
    func getDocumentDirectoryPath(fileName:String) -> NSURL {
        let fileManager = FileManager.default
        let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let UUID = NSUUID().uuidString
        let pathComponent = "\(UUID)-\(fileName)"
        return directoryURL.appendingPathComponent(pathComponent) as NSURL
    }

}
*/
