//
//  InstagramViewController.swift
//  Snaptouch_Polaroid
//
//  Created by maximess142 on 07/02/18.
//  Copyright © 2018 maximess142. All rights reserved.
//


import UIKit
import Alamofire
import WebKit

class InstagramViewController: UIViewController,WKUIDelegate,WKNavigationDelegate {
    
    
    @IBOutlet weak var loginIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loginWebView: WKWebView!
    var longLivedAccesstoken = ""
    var arrayInstagramImages = [String]()
    var newids = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loginIndicator.isHidden = true
        loginIndicator.stopAnimating()
        
        self.loginWebView.uiDelegate = self
        self.loginWebView.navigationDelegate = self
        unSignedRequest()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func doneBtnClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - unSignedRequest
    func unSignedRequest () {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=code&scope=%@", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI, INSTAGRAM_IDS.INSTAGRAM_SCOPE ])
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        loginWebView.load(urlRequest)
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
            if let range: Range<String.Index> = requestURLString.range(of: "?code=")
            {
                let replaced = requestURLString.substring(from: range.upperBound).dropLast(2)
                getAccessToken(code: String(replaced))
            }
            
            return false;
        }
        return true
    }
    
    func getAccessToken(code: String){
        var access_token : String?
        var user_id : String?
        let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        let params : Parameters =   [ "client_id":"\(INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID)", "client_secret":"\(INSTAGRAM_IDS.INSTAGRAM_CLIENTSERCRET)", "grant_type":"authorization_code", "redirect_uri":"\(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI)", "code":"\(code)"]
        
        let url =  URL(string:"https://api.instagram.com/oauth/access_token" as String)
        UserDefaults.standard.setValue(true, forKey: "isLoggedInToInstagramKodak")
        Alamofire.request(url!, method: .post, parameters: params, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
            response in
            if response.value != nil{
                let jsonResponse = response.value as! NSDictionary
                
                if jsonResponse["access_token"] != nil
                {
                    access_token = String(describing: jsonResponse["access_token"]!)
                    //                    UserDefaults.standard.setValue(access_token, forKey: "instagramAccessToken")
                }
                
                if jsonResponse["user_id"] != nil{
                    user_id = String(describing: jsonResponse["user_id"]!)
                }
                self.getLongLivedAccessToken(accessToken: access_token!)
                //                self.getMediaId(access_token: access_token!)
            }
            else if (response.error != nil){
                var error = response.error!.localizedDescription
                if error.contains("JSON could not") {
                    error = "Could not connect to server at the moment."
                }
            }
        })
    }
    
    func getLongLivedAccessToken(accessToken: String){
        var mediaId : String?
        var caption : String?
        var access_token : String?
        let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        let url = URL(string:"https://graph.instagram.com/access_token?grant_type=ig_exchange_token&client_secret=\(INSTAGRAM_IDS.INSTAGRAM_CLIENTSERCRET)&access_token=\(accessToken)" as String )
        
        Alamofire.request(url!, method: .get, parameters: nil, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
            response in
            if response.value != nil{
                
                let jsonResponse = response.value as! NSDictionary
                
                if jsonResponse["access_token"] != nil
                {
                    access_token = String(describing: jsonResponse["access_token"]!)
                    self.longLivedAccesstoken = accessToken
                    UserDefaults.standard.setValue(self.longLivedAccesstoken, forKey: "instagramAccessToken")
                    self.getMediaId(access_token: self.longLivedAccesstoken)
                    self.loginWebView.isHidden = true
                }
                
            }
            else if (response.error != nil){
                var error = response.error!.localizedDescription
                if error.contains("JSON could not") {
                    error = "Could not connect to server at the moment."
                }
            }
        })
    }
    func getMediaId(access_token: String){
        var mediaId : String?
        var caption : String?
        let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        let url =  URL(string:"https://graph.instagram.com/me/media?fields=id,caption&access_token=\(access_token)" as String)
        
        Alamofire.request(url!, method: .get, parameters: nil, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
            response in
            if response.value != nil{
                let fetchedData = response.value as! [String:Any]
                if let getids = fetchedData["data"] as? [[String:Any]] {
                    for i in getids {
                        if let ids = i["id"] as? String {
                            self.newids.append(ids)
                            self.getImagesUsingId(access_token: access_token, id: ids)
                        }
                    }
                }
                if let paging = fetchedData["paging"] as? [String:Any]{
                    if (fetchedData["paging"] as! [String:Any]).count > 0
                    {
                        let cursors = paging["cursors"] as? [String:Any]
                        let after = cursors?["after"]
                        if let url2 = after
                        {
//                            self.fetchPhotosFromInstagramWithAccessToken(url: url2 as! String)
                        }
                    }
                        
                    else
                    {
                        DispatchQueue.main.async {
                            self.saveImagesInDBAndMoveToNext()
                        }
                    }
                    
                }
                
            }
            else if (response.error != nil){
                var error = response.error!.localizedDescription
                if error.contains("JSON could not") {
                    error = "Could not connect to server at the moment."
                }
            }
        })
    }
    func getImagesUsingId(access_token : String,id : String){
        var imageid : String?
        var imageUrl : String?
        let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        let url =  URL(string:"https://graph.instagram.com/\(id)?fields=id,media_type,media_url,username,timestamp&access_token=\(access_token)" as String)
        Alamofire.request(url!, method: .get, parameters: nil, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
            response in
            if response.value != nil{
                let jsonResponse = response.value as! NSDictionary
                
                if jsonResponse["username"] != nil
                {
                    let username = String(describing: jsonResponse["username"]!)
                    UserDefaults.standard.setValue(username, forKey: "instagramUserName")
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "instagramUserChanged"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name("reloadSocialMedia"), object: nil)
                    
                }
                if jsonResponse["id"] != nil
                {
                    imageid = String(describing: jsonResponse["id"]!)
                }
                if jsonResponse["media_type"] as? String == "CAROUSEL_ALBUM" {
                    self.getCarosalAlbumImagesUsingId(access_token: access_token, id: imageid!)
                }else if jsonResponse["media_type"] as? String == "IMAGE"{
                    if jsonResponse["media_url"] != nil{
                        imageUrl = String(describing: jsonResponse["media_url"]!)
                        self.arrayInstagramImages.append(imageUrl!)
                        DispatchQueue.main.async {
                            self.saveImagesInDBAndMoveToNext()
                        }
                    }
                }else{
                    print("Video")
                }
            }
            else if (response.error != nil){
                var error = response.error!.localizedDescription
                if error.contains("JSON could not") {
                    error = "Could not connect to server at the moment."
                }
            }
        })
        //        NotificationCenter.default.post(name: Notification.Name(rawValue: "instagramUserChanged"), object: nil)
    }
    
    func getCarosalAlbumImagesUsingId(access_token : String,id : String){
        var imageid : String?
        var imageUrl : String?
        let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        let url = URL(string:"https://graph.instagram.com/\(id)/children?fields=media_type,media_url,timestamp&access_token=\(access_token)")
        Alamofire.request(url!, method: .get, parameters: nil, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
            response in
            if response.value != nil{
                let jsonResponse = response.value as! NSDictionary
                let fetchedData = response.value as! [String:Any]
                if let getids = fetchedData["data"] as? [[String:Any]] {
                    for i in getids {
                        if let media_url = i["media_url"] as? String {
                            self.arrayInstagramImages.append(media_url)
                        }
                    }
                    DispatchQueue.main.async {
                        self.saveImagesInDBAndMoveToNext()
                    }
                }
            }
            else if (response.error != nil){
                var error = response.error!.localizedDescription
                if error.contains("JSON could not") {
                    error = "Could not connect to server at the moment."
                }
            }
        })
    }
    
    
    
    // MARK: - UIWebViewDelegate
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        loginIndicator.isHidden = false
        loginIndicator.startAnimating()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loginIndicator.isHidden = true
        loginIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        //        print("BODY", navigationResponse.response)
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        //        print("BODY", navigationAction.request)
        self.checkRequestForCallbackURL(request: navigationAction.request)
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        print("redirect")
    }
    
    func fetchPhotosFromInstagramWithAccessToken(url: String) -> Void
    {
        let request = NSMutableURLRequest(url: URL(string:url)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 120)
        print("request\(request)")
        request.httpMethod = "GET" // POST ,GET, PUT What you want
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
            //Akshay Solve crash detect on crashanalitics
            if data != nil
            {
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        print("jsonResultData instagram photo : \(jsonResult)")
                        
                        //Akshay Solve crash detect on crashanalitics Add try catch
                        
                        if let jsonResultData = jsonResult["data"] as? [[String:Any]]
                        {
                            
                            print(jsonResult["pagination"])
                            
                            for jsonDict in jsonResultData
                            {
                                if jsonDict["type"] as! String == "image"
                                {
                                    let imagedict = jsonDict["images"] as! [String:Any]
                                    let imageresolutiondict = imagedict["standard_resolution"] as!  [String:Any]
                                    let stringURL = imageresolutiondict["url"] as! String
                                    self.arrayInstagramImages.append(stringURL)
                                }
                                else if jsonDict["type"] as! String == "carousel"
                                {
                                    let imageArray = jsonDict["carousel_media"] as! [[String:Any]]
                                    for object in imageArray
                                    {
                                        //Akshay Solve crash detect on crashanalitics
                                        if let imagedict = object["images"] as? [String:Any]
                                        {
                                            let imageresolutiondict = imagedict[ "standard_resolution"] as! [String:Any]
                                            let stringURL = imageresolutiondict["url"] as! String
                                            self.arrayInstagramImages.append(stringURL)
                                            
                                        }
                                    }
                                }
                            }
                            
                        }
                        //Akshay Solve crash detect on crashanalitics
                        if let pagination = jsonResult["pagination"] as? [String:Any]
                        {
                            
                            if (jsonResult["pagination"] as! [String:Any]).count > 0
                            {
                                let pageDict = jsonResult["pagination"] as! [String:Any]
                                let next_url = pageDict["next_url"] as? String
                                if let url2 = next_url
                                {
                                    self.fetchPhotosFromInstagramWithAccessToken(url: url2)
                                }
                            }
                                
                            else
                            {
                                DispatchQueue.main.async {
//                                    self.saveImagesInDBAndMoveToNext()
                                }
                            }
                            
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
                
            }
        }
        dataTask.resume()
    }
    
    func saveImagesInDBAndMoveToNext()
    {
        self.navigationController?.popViewController(animated: false)
        
        if self.arrayInstagramImages.count > 0
        {
            DBLayer().insertInstagramPhotos(urlArray: self.arrayInstagramImages)
            self.arrayInstagramImages.removeAll()
        }
        else
        {
            /*
             let alert = UIAlertController(title: NSLocalizedString("Alert".localisedString(), comment:""), message: NSLocalizedString("Could not fetch images".localisedString(), comment:""), preferredStyle: UIAlertControllerStyle.alert)
             alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
             self.present(alert, animated: true, completion: nil)*/
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "instagramUserChanged"), object: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
