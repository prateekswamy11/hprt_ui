//
//  InstagramHelper.swift
//  Snaptouch_Polaroid
//
//  Created by maximess142 on 07/02/18.
//  Copyright © 2018 maximess142. All rights reserved.
//

import UIKit
import WebKit

struct INSTAGRAM_IDS {
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    static let INSTAGRAM_CLIENT_ID  = "155886062337734"
    static let INSTAGRAM_CLIENTSERCRET = "9a25a9b3578e64c330afcf9251476143"
    static let INSTAGRAM_REDIRECT_URI = "https://www.kodakphotoprinter.com/"
    static let INSTAGRAM_ACCESS_TOKEN =  "access_token"
    static let INSTAGRAM_SCOPE = "user_profile,user_media"
}

class InstagramHelper: NSObject
{
    func loginToInstagram(viewController : UIViewController)
    {
        self.logoutOffInstagram()
        let instagramLoginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InstagramViewController") as! InstagramViewController
        viewController.navigationController?.pushViewController(instagramLoginVC, animated: true)
    }
    
    func logoutOffInstagram()
    {
        self.clearInstagramCookies()
        
        let dbLayer = DBLayer()
        dbLayer.deleteInstagramPhotos()
        
        UserDefaults.standard.setValue(false, forKey: "isLoggedInToInstagramKodak")
        UserDefaults.standard.setValue("", forKey: "instagramUserName")
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "userLoggedOut"), object: "Instagram")
    }
    
    
    func clearInstagramCookies() {
        InstagramHelper.clean()
        let cookieStore = HTTPCookieStorage.shared
        guard let cookies = cookieStore.cookies, cookies.count > 0 else { return }
        
        for cookie in cookies {
            if cookie.domain == "www.instagram.com" || cookie.domain == "api.instagram.com" || cookie.domain == ".instagram.com" {
                cookieStore.deleteCookie(cookie)
            }
        }
    }
    
    // Clear cookie for WKwebkit
          class func clean() {
              guard #available(iOS 9.0, *) else {return}

              HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)

              WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
                  records.forEach { record in
                      WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                      #if DEBUG
                          print("WKWebsiteDataStore record deleted:", record)
                      #endif
                  }
              }
          }
}
