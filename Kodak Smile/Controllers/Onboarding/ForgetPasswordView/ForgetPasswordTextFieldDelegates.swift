//
//  ForgetPasswordTextFieldDelegates.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 15/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class ForgetPasswordTextFieldDelegates: NSObject {

}

extension ForgetPasswordViewController: UITextFieldDelegate,UITextViewDelegate{
    //MARK: - TextField Delegates -
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return string == " " ? false : true
    }
    
    //MARK: - TextView Delegate -
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        if !WebserviceModelClass().isInternetAvailable()
        {
            isPrivacyPolicyPageOpened = true
            let legalVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
            self.navigationController?.pushViewController(legalVC, animated: true)
            return false
        }
        else
        {
            print("CLICKED")
            isPrivacyPolicyPageOpened = true
            let legalVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
            self.navigationController?.pushViewController(legalVC, animated: true)
            return false
        }
    }
}
