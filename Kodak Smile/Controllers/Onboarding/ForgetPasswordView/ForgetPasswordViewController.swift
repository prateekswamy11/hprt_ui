//
//  ForgetPasswordViewController.swift
//  Polaroid MINT
//
//  Created by MAXIMESS142 on 12/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CopilotAPIAccess
import CopilotLogger

class ForgetPasswordViewController: UIViewController {

    //MARK: - Outlets -
    @IBOutlet weak var btnSendRecoveryPassword: UIButton!
    @IBOutlet weak var txtEmail: PaddingTextField!
    @IBOutlet weak var txtViewPrivacyPolicy: UITextView!
    @IBOutlet weak var constraintPrivacyPolicyTxtVwLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintPrivacyPolicyTxtVwTrailing: NSLayoutConstraint!
    @IBOutlet weak var btncheckBok: UIButton!
    @IBOutlet weak var lblForgotPassword: UILabel!
    @IBOutlet weak var lblEnterEmail: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    // - Constraint Outlets -
    @IBOutlet weak var constraintForgetPwdTop: NSLayoutConstraint!
    
    //MARK: - Variables -
    var wholeString = "By signing up you agree to our\nTerms & Conditions and "
    var privacyPolicyStr = "Privacy Policy."
    var isPrivacyPolicyPageOpened = false    // **Flag to show that Privacy Policy Page is Opened.
    var unchecked = true

    //MARK: - View LifeCycle Functions -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addObservers()
       // self.view.addLinearGradient()
        self.addLineUnderAllTxtFields()
//        self.detectIphone5sDevice()
        self.privacyPolicyAction()
        localizedStrings()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.removeObservers()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (txtEmail.text?.isEmpty)!{
            disableBtnAnimated(btnSendRecoveryPassword)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        isPrivacyPolicyPageOpened = false
    }
    
    //MARK: - IBActions -
    @IBAction func btnCheckBoxClicked(_ sender: Any) {
        if unchecked {
            self.btncheckBok.setImage(UIImage(named: "checkBoxSelected"), for: .normal)
            unchecked = false
        }
        else {
            self.btncheckBok.setImage(UIImage(named: "checkBoxEmpty"), for: .normal)
            unchecked = true
        }
        if (txtEmail.text?.isEmpty)! || unchecked {
            disableBtnAnimated(btnSendRecoveryPassword)
        }else{
            enableBtnAnimated(btnSendRecoveryPassword)
        }
    }
    
    @IBAction func responseForgetPasswordBtnClicked(_ sender: UIButton) {
        if txtEmail.text!.isEmpty{
            self.showAlert(title: "Alert", message: "Please enter your email.")
        }else if !(txtEmail.text?.isValidEmail)!{
            self.showAlert(title: "Alert", message: "Please enter valid email.")
        }else{
            performResetPassword()
        }
    }
    
    @IBAction func goBackBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // IB Action on textFields for editing changed
    // Enables button after all textfields are filled
    @IBAction func editingChangedForTxtField(_ sender: PaddingTextField) {
        if (txtEmail.text?.isEmpty)! //|| unchecked
        {
            disableBtnAnimated(btnSendRecoveryPassword)
        }else{
            enableBtnAnimated(btnSendRecoveryPassword)
        }
    }
    
    //MARK: - Functions -
    func localizedStrings() {
        self.lblForgotPassword.text = "Forgot Password".localisedString()
        self.lblEmail.text = "Email".localisedString()
        self.lblEnterEmail.text = "Enter your email. We’ll send you a password recovery link.".localisedString()
        self.btnSendRecoveryPassword.setTitle("Send".localisedString(), for: .normal)
    }
    
    func addLineUnderAllTxtFields()
    {
        self.txtEmail.addLineToView(view: txtEmail, position: .LINE_POSITION_BOTTOM, color: UIColor.white, width: 1)
    }
    
    func clearTextFields(){
        if isPrivacyPolicyPageOpened{
            print("Keeping the text in field.")
        }else{
            self.txtEmail.text = ""
        }
    }
    
    func performResetPassword() {
        guard let email = txtEmail.text else { return }
        if WebserviceModelClass().isInternetAvailable() {
            customLoader.showActivityIndicator(viewController: self.view)
            self.btnSendRecoveryPassword.isEnabled = false
            AppManager.shared.userManager.resetPassword(forEmail: txtEmail.text!) {[weak self] (response) in
                customLoader.hideActivityIndicator()
                switch response {
                case .failure(error: let error):
                    ZLogManagerWrapper.sharedInstance.logError(message: "got error with error: \(error)")
                case .success(_):
                    ZLogManagerWrapper.sharedInstance.logDebug(message: "success reseting password")
                    let forgotPasswordAnalyticsEvent = ForgotPasswordAnalyticsEvent()
                    Copilot.instance.report.log(event: forgotPasswordAnalyticsEvent)
                    print("success reseting password")
                    //                    self?.checkForgetPassword(email)
                    DispatchQueue.main.async {
                        self?.goToPasswordRecoveryVC()
                    }
                }
            }
        } else {
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            comeFromStrNoInternetSocialMedia = "forgetNoInternet"
            self.present(alertPopUp, animated: true, completion: nil)
        }
    }
    
    func checkForgetPassword(_ email: String)
    {
        let requestParameters = ["email" : email] as [String : Any]
        if WebserviceModelClass().isInternetAvailable(){
            self.btnSendRecoveryPassword.isEnabled = false
            customLoader.showActivityIndicator(viewController: self.view)
            WebserviceModelClass().postDataFor(module: "LoginViewController", subUrl: FORGETPASSWORD_URL, parameter: requestParameters) { (isSuccess, responseMsg, arrayJSONObj) in
                DispatchQueue.main.async {
                    customLoader.hideActivityIndicator()
                    if isSuccess{
                        self.goToPasswordRecoveryVC()
                    }else{
                        self.showAlert(title: "Alert", message: responseMsg)
                    }
                }
            }
        }else{
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            comeFromStrNoInternetSocialMedia = "forgetNoInternet"
            self.present(alertPopUp, animated: true, completion: nil)
        }
    }
    
    func goToPasswordRecoveryVC()
    {
        let passwordRecoveryVC = storyboards.onboardingStoryboard.instantiateViewController(withIdentifier: "PasswordRecoveryViewController") as! PasswordRecoveryViewController
        self.navigationController?.pushViewController(passwordRecoveryVC, animated: true)
    }
    
    func addObservers()
    {
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
            self.keyboardWillShow(notification: notification)
        }
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) { (notification) in
            self.keyboardWillHide(notification: notification)
        }
    }
    
    func removeObservers()
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(notification: Notification)
    {
//        UIView.animate(withDuration: 1.0) {
//            self.constraintForgetPwdTop.constant = 15.0
//            self.view.layoutIfNeeded()
//        }
    }
    
    func keyboardWillHide(notification: Notification)
    {
//        UIView.animate(withDuration: 1.0) {
//            self.constraintForgetPwdTop.constant = 90.0
////            self.detectIphone5sDevice()
//            self.view.layoutIfNeeded()
//        }
    }
    
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            self.constraintForgetPwdTop.constant = 50.0
            self.constraintPrivacyPolicyTxtVwLeading.constant = 50.0
            self.constraintPrivacyPolicyTxtVwTrailing.constant = 50.0
        }else{
            print("Unknown")
        }
    }
    
    func privacyPolicyAction(){
        let text = NSMutableAttributedString(string: wholeString)
        text.addAttributes([NSAttributedStringKey.font:UIFont(name: "SFProDisplay-Regular", size: 12.0)!, NSAttributedStringKey.foregroundColor:UIColor.white], range: NSMakeRange(0, text.length))
        let selectablePart = NSMutableAttributedString(string:privacyPolicyStr)
        selectablePart.addAttributes([NSAttributedStringKey.font:UIFont(name: "SFProDisplay-Regular", size: 12.0)!, NSAttributedStringKey.underlineStyle:NSUnderlineStyle.styleSingle.rawValue, NSAttributedStringKey.underlineColor:UIColor.white, NSAttributedStringKey.link:"privacyPolicy"], range: NSMakeRange(0, selectablePart.length))
        txtViewPrivacyPolicy.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue:UIColor.white] as [String:Any]
        text.append(selectablePart)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        text.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, text.length))
        txtViewPrivacyPolicy.attributedText = text
    }
}
