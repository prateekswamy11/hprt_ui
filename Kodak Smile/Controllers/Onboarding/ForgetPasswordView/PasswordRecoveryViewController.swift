//
//  PasswordRecoveryViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 15/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PasswordRecoveryViewController: UIViewController {

    @IBOutlet weak var lblCheckInbox: UILabel!
    @IBOutlet weak var lblCheckEmail: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.addLinearGradient()
        localizedStrings()
    }
    
    func localizedStrings() {
        self.lblCheckInbox.text = "Check Your Inbox".localisedString()
        self.lblCheckEmail.text = "If this address exists, you will get an email shortly".localisedString()
        self.btnDone.setTitle("Done".localisedString(), for: .normal)
    }
    
    //MARK: - IBActions -
    @IBAction func goToLoginVCBtnClicked(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LoginViewController.self) {
                guard ((self.navigationController?.popToViewController(controller, animated: true)) != nil) else{
                    return
                }
                break
            }
        }
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LoginViewController.self) {
                guard ((self.navigationController?.popToViewController(controller, animated: true)) != nil) else{
                    return
                }
                break
            }
        }
    }
    
}
