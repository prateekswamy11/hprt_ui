//
//  ViewController.swift
//  MintLoginScreen
//
//  Created by MAXIMESS142 on 19/09/18.
//  Copyright © 2018 MAXIMESS142. All rights reserved.
//

import UIKit
import Photos
import CopilotAPIAccess

class LoginViewController: UIViewController {

    //MARK: - IBOutlets -
    @IBOutlet weak var txtEmail: PaddingTextField!
    @IBOutlet weak var txtPassword: PaddingTextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var viewLoginBottom: UIView!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var txtTermAndCondition: MyTextView!
     @IBOutlet weak var lblLogIn: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
     @IBOutlet weak var btnForgotPassword: UIButton!
    // - Constraint IBOutlets -
    @IBOutlet weak var constraintViewBottomLogin: NSLayoutConstraint!
    @IBOutlet weak var constraintLoginBtnBottom: NSLayoutConstraint!
    
    //MARK: - Variables -
    let saveSignInData:UserDefaults = UserDefaults.standard
    var unchecked = true
    var wholeString = "By signing up you agree to our \nTerms & Conditions and "
    var privacyPolicyStr = "Privacy Policy."
    var isPrivacyPolicyPageOpened = false    // **Flag to show that Privacy Policy Page is Opened.
    
    //MARK: - View LifeCycle Functions -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.addObservers()
        self.addLineUnderAllTxtFields()
        self.privacyPolicyAction()
        self.detectIphone5sDevice()
        localizedStrings()
    }

    override func viewWillAppear(_ animated: Bool) {
        if (txtEmail.text?.isEmpty)! || (txtPassword.text?.isEmpty)!{
            disableBtnAnimated(btnLogin)
        }
        self.clearTextFields()
        //self.addLinearGradient()
    }

    override func viewDidAppear(_ animated: Bool) {
        isPrivacyPolicyPageOpened = false
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.login.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
        NotificationCenter.default.addObserver(self,selector: #selector(self.dismissBtnClicked(_:)),
                                               name: NSNotification.Name(rawValue: "goToWelcome"),object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        removeObservers()
        customLoader.hideActivityIndicator()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: - IBActions -
    @IBAction func btnCheckBoxClicked(_ sender: Any) {
        if unchecked {
            self.btnCheckBox.setImage(UIImage(named: "checkBoxSelected"), for: .normal)
            unchecked = false
        }
        else {
            self.btnCheckBox.setImage(UIImage(named: "checkBoxEmpty"), for: .normal)
            unchecked = true
        }
        if (txtEmail.text?.isEmpty)! || (txtPassword.text?.isEmpty)! //|| unchecked
        {
            disableBtnAnimated(btnLogin)
        }
        else
        {
            enableBtnAnimated(btnLogin)
        }
    }
    
    @IBAction func dismissBtnClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showPswdBtnClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.txtPassword.isSecureTextEntry = !self.txtPassword.isSecureTextEntry
    }
    
    @IBAction func loginBtnClicked(_ sender: UIButton)
    {
        let tapLoginAnalyticsEvent = TapLoginAnalyticsEvent()
        Copilot.instance.report.log(event: tapLoginAnalyticsEvent)
        txtEmail.resignFirstResponder()
        guard !(txtEmail.text?.isEmpty)! else
        {
            self.showAlert(title: "Alert", message: "Please enter email.")
            return
        }
        guard !(txtPassword.text?.isEmpty)! else
        {
            self.showAlert(title: "Alert", message: "Please enter password.")
            return
        }
        guard (txtEmail.text?.isValidEmail)! else {
            self.showAlert(title: "Alert", message: "Please enter valid email.")
            return
        }
//        self.checkUserLogin(txtEmail.text!, txtPassword.text!)
        print("Valid Email and Password")
        if !WebserviceModelClass().isInternetAvailable()
        {
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            comeFromStrNoInternetSocialMedia = "loginNoInternet"
            self.present(alertPopUp, animated: true, completion: nil)
            return
        }
        self.performLogin(txtEmail.text!, txtPassword.text!)
    }

    @IBAction func forgotPasswordBtnClicked(_ sender: UIButton)
    {
        let ForgotPasswordVC = storyboards.onboardingStoryboard.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
        self.navigationController?.pushViewController(ForgotPasswordVC, animated: true)
    }

    // IB Action on textFields for editing changed
    // Enables button after all textfields are filled
    @IBAction func editingChangedForTextField(_ sender: PaddingTextField) {
        if (txtEmail.text?.isEmpty)! || (txtPassword.text?.isEmpty)! //|| unchecked
        {
            disableBtnAnimated(btnLogin)
        }
        else
        {
            enableBtnAnimated(btnLogin)
        }
    }

    @IBAction func termPolicyBtnClicked(_ sender: UIButton)
    {
        isPrivacyPolicyPageOpened = true
        let legalVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
        self.navigationController?.pushViewController(legalVC, animated: true)
    }
    //MARK: - Functions -
    func privacyPolicyAction(){
        let text = NSMutableAttributedString(string: wholeString)
        text.addAttributes([NSAttributedStringKey.font:UIFont(name: "SFProDisplay-Regular", size: 12.0)!, NSAttributedStringKey.foregroundColor:UIColor.white], range: NSMakeRange(0, text.length))
        let selectablePart = NSMutableAttributedString(string:privacyPolicyStr)
        selectablePart.addAttributes([NSAttributedStringKey.font:UIFont(name: "SFProDisplay-Regular", size: 12.0)!, NSAttributedStringKey.underlineStyle:NSUnderlineStyle.styleSingle.rawValue, NSAttributedStringKey.underlineColor:UIColor.white, NSAttributedStringKey.link:"privacyPolicy"], range: NSMakeRange(0, selectablePart.length))
        txtTermAndCondition.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue:UIColor.white] as [String:Any]
        text.append(selectablePart)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        text.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, text.length))
        txtTermAndCondition.attributedText = text
    }

    func localizedStrings() {
        self.lblLogIn.text = "Log In".localisedString()
        self.lblEmail.text = "Email".localisedString()
        self.lblPassword.text = "Password".localisedString()
        self.btnLogin.setTitle("Log In".localisedString(), for: .normal)
        self.btnForgotPassword.setTitle("Forgot password".localisedString(), for: .normal)
    }

    @objc func clearTextFields(){
        self.txtEmail.text = ""
        self.txtPassword.text = ""
        disableBtnAnimated(btnLogin)
    }
    
    func addLinearGradient()
    {
        // - Colors -
        let color1 = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
        let color2 = UIColor(red: 49/255, green: 49/255, blue: 49/255, alpha: 1.0)
        let color3 = UIColor(red: 84/255, green: 84/255, blue: 84/255, alpha: 1.0)
        
        // - Color Layers -
        let newLayer1 = CAGradientLayer()
        newLayer1.colors = [color1.cgColor,color2.cgColor,color3.cgColor]
        newLayer1.frame = self.view.frame
        let newLayer2 = CAGradientLayer()
        newLayer2.colors = [color1.cgColor,color2.cgColor,color3.cgColor]
        newLayer2.frame = self.view.frame
        self.viewLoginBottom.layer.insertSublayer(newLayer2, at: 0)
    }
    
    func addLineUnderAllTxtFields()
    {
        self.txtEmail.addLineToView(view: txtEmail, position: .LINE_POSITION_BOTTOM, color: UIColor.white, width: 1)
        self.txtPassword.addLineToView(view: txtPassword, position: .LINE_POSITION_BOTTOM, color: UIColor.white, width: 1)
    }
    
    func addObservers()
    {
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
            self.keyboardWillShow(notification: notification)
        }
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) { (notification) in
            self.keyboardWillHide(notification: notification)
        }
    }

    func removeObservers()
    {
        NotificationCenter.default.removeObserver(self)
    }

    func keyboardWillShow(notification: Notification)
    {
        UIView.animate(withDuration: 1.0) {
            // Getting the height of Keyboard
            let userInfo:NSDictionary = notification.userInfo! as NSDictionary
            let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
            let keyboardHeight = keyboardFrame.cgRectValue.height
            // Moving bottom view containing textfields up
            self.constraintViewBottomLogin.constant = -(keyboardHeight-160.0)
            self.view.layoutIfNeeded()
        }
    }

    func keyboardWillHide(notification: Notification)
    {
        UIView.animate(withDuration: 1.0){
            self.constraintViewBottomLogin.constant = 0
            self.view.layoutIfNeeded()
        }
    }

    func setUserInformation(email:String,password:String){
        UserDefaults.standard.set(false, forKey: "iWillDoItLater")
        saveSignInData.set(email, forKey: "emailKodak")
        saveSignInData.set(password, forKey: "passwordKodak")
        saveSignInData.set(true, forKey: "switchStateKodak")
        UserDefaults.standard.set(false, forKey: "isJoinKodak")
//        saveSignInData.set(false,forKey: "onboardingSkipped")
        saveSignInData.synchronize()
    }
    
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            self.constraintLoginBtnBottom.constant = 40.0
        }else{
            print("Unknown")
        }
    }
    
    //MARK: - API Calling Functions -
    func checkUserLogin(_ email: String,_ password: String, _ isCopilotUser: Bool)
    {
        let requestParameters = ["email" : email,"password": password] as [String : Any]
        if WebserviceModelClass().isInternetAvailable(){
//            customLoader.showActivityIndicator(viewController: self.view)
            WebserviceModelClass().postDataFor(module: "LoginViewController", subUrl: LOGIN_URL, parameter: requestParameters) { (isSuccess, responseMsg, arrayJSONObj) in
                DispatchQueue.main.async {
                    if isSuccess{
                        if !isCopilotUser {
                            self.showPrivacyPolicyPopup()
                            customLoader.hideActivityIndicator()
                            return
                        }
                        //Get the name of user from response.
                        let data = arrayJSONObj["data"] as! [String:Any]
                        self.saveSignInData.set(data["name"] as! String, forKey: "userNameLoggedIn")
                        self.saveSignInData.synchronize()
                        self.goToHomeVC(email, password)
                    }else{
                        if isCopilotUser {
                            guard let user = AppManager.shared.userManager.user else {
                                self.goToHomeVC(email, password)
                                return
                            }
                            self.saveSignInData.set("\(user.firstName ?? "") \(user.lastName ?? "")", forKey: "userNameLoggedIn")
                            self.saveSignInData.synchronize()
                            self.goToHomeVC(email, password)
                            return
                        }else{
                            customLoader.hideActivityIndicator()
                            self.showAlert(title: "Alert", message: responseMsg)
                        }
                        
                    }
                }
            }
        }else{
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            comeFromStrNoInternetSocialMedia = "loginNoInternet"
            self.present(alertPopUp, animated: true, completion: nil)
        }
    }

    func goToHomeVC(_ email: String,_ password: String)
    {
        // Clearing all the facebook, google, Instagram, Dropbox information of previous user or clearing cache for same user.
        FacebookHelper().logoutOffFacebook()
        GoogleHelper().logoutOffGoogle()
        InstagramHelper().logoutOffInstagram()
        self.setUserInformation(email: email, password: password)
        // Go to PrinterOnboarding screens.
        self.goToHomeController()
    }

}


