//
//  PrivacyPolicyViewController.swift
//  Polaroid MINT
//
//  Created by Macmini003 on 26/07/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController , UITextViewDelegate {
    
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var lblDear: UILabel!
    @IBOutlet weak var txtViewMessage: MyTextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
    }
    
    //MARK: - IBActions -
    @IBAction func cancelBtnClicked(_ sender: Any) {
        removeFromParentViewController()
        self.view.removeFromSuperview()
        NotificationCenter.default.post(name: NSNotification.Name("dismisspopup"), object: nil)
    }
    
    @IBAction func okBtnClicked(_ sender: UIButton) {
        removeFromParentViewController()
        self.view.removeFromSuperview()
        NotificationCenter.default.post(name: NSNotification.Name("goToWelcome"), object: nil)
    }
    
    //MARK: - Functions -
    func localizedStrings() {
        self.lblDear.text = "Dear customer,".localisedString()
        self.btnOK.setTitle("OK".localisedString(), for: .normal)
        //MARK: - Variables -
        let main_string = "we have recently updated our privacy policy. Please sign up again to continue enjoying our app. We apologize for the inconvenience.".localisedString()
        let string_to_color = "privacy policy".localisedString()
        let range = (main_string as NSString).range(of: string_to_color)
        let regularAttributes = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.5921568627, green: 0.6117647059, blue: 0.5568627451, alpha: 1),NSAttributedStringKey.font:UIFont(name: "SFProDisplay-Regular", size: 15.0)!] as [NSAttributedStringKey : Any]
        let largeAttributes = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.5921568627, green: 0.6117647059, blue: 0.5568627451, alpha: 1),NSAttributedStringKey.font:UIFont(name: "SFProDisplay-Regular", size: 15.0)!, NSAttributedStringKey.link:"privacyPolicy",NSAttributedStringKey.underlineStyle:NSUnderlineStyle.styleSingle.rawValue, NSAttributedStringKey.underlineColor:UIColor.black] as [NSAttributedStringKey : Any]
        let attributedSentence = NSMutableAttributedString(string: main_string, attributes: regularAttributes)
        attributedSentence.setAttributes(largeAttributes, range: range)
        txtViewMessage.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue:UIColor.black] as [String:Any]
        txtViewMessage.attributedText = attributedSentence
        txtViewMessage.textAlignment = .center
    }
    
    //MARK: - TextView Delegate -
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        let legalVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
        legalVC.isFrom = "Login"
        self.navigationController?.pushViewController(legalVC, animated: true)
        return false
    }
}
