//
//  LoginViewController+Auth.swift
//  Kodak Smile
//
//  Created by MacMini002 on 6/12/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation
import CopilotAPIAccess
import CopilotLogger

extension LoginViewController {
    
    func performLogin(_ email:String, _  password: String) {
        customLoader.showActivityIndicator(viewController: self.view)
        AppManager.shared.userManager.login(withEmail: email, password: password) { [weak self] (response) in
//            customLoader.hideActivityIndicator()
            switch response {
            case .failure(error: let error):
                ZLogManagerWrapper.sharedInstance.logError(message: "failed to login with error: \(error)")
                let loginFailedAnalyticsEvent = LoginFailedAnalyticsEvent()
                Copilot.instance.report.log(event: loginFailedAnalyticsEvent)
                let errorReportAnalyticsEvent = ErrorReportAnalyticsEvent(errorType: AnalyticsValue.errorReport.loginFailed.rawValue)
                Copilot.instance.report.log(event: errorReportAnalyticsEvent)
                self?.checkUserLogin(email, password, false)
                /*switch error {
                case .login:
                    // That's the case when credentials are incorrect (wrong or does not exist)
                    // TODO present the new popup with the forgot password button
                    self?.presentAlertControllerWithPopupRepresentable(error, cancelButtonText: "OK".localisedString())
                    
                default:
                    self?.presentAlertControllerWithPopupRepresentable(error, cancelButtonText: "OK".localisedString())
                }*/
            case .success(_):
                let loginEvent = LoginAnalyticsEvent()
                Copilot.instance.report.log(event: loginEvent)
                ZLogManagerWrapper.sharedInstance.logInfo(message: "success in login")
                UserDefaults.standard.setValue(false, forKey: "isLoggedInToIllDoItLater")
                self?.checkUserLogin(email, password, true)
            }
        }
    }
    
    func showPrivacyPolicyPopup() {
        if let ppViewVC = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as? PrivacyPolicyViewController
        {
            addChildViewController(ppViewVC)
            self.view.addSubview(ppViewVC.view)
             NotificationCenter.default.addObserver(self,selector: #selector(self.clearTextFields),name: NSNotification.Name(rawValue: "dismisspopup"),object: nil)
        }
    }
}
