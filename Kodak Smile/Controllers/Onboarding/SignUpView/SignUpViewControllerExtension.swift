//
//  SignUpViewControllerExtension.swift
//  Kodak Smile
//
//  Created by MAXIMESS183 on 10/02/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import UIKit
import GoogleSignIn
import GTMSessionFetcher
import GoogleAPIClientForREST
import CopilotLogger
import CopilotAPIAccess

extension SignUpViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let accessToken = user.authentication.accessToken
            print("user.authentication.accessTokenExpirationDate = \(user.authentication.accessTokenExpirationDate)")
            customLoader.showActivityIndicator(viewController: self.view)
            //self.view.isUserInteractionEnabled = false
//            UserDefaults.standard.setValue(true, forKey: "isLoggedInToGoogleKodak")
            let fullName = user.profile.name
            let email = user.profile.email
            UserDefaults.standard.setValue(fullName, forKey: "googleUserName")
            UserDefaults.standard.setValue(userId, forKey: "googleUserId")
            UserDefaults.standard.setValue(user.authentication.refreshToken, forKey: "googleRefreshToken")
            UserDefaults.standard.setValue(user.authentication.accessToken, forKey: "googleAccessToken")
            UserDefaults.standard.setValue(user.authentication.accessTokenExpirationDate, forKey: "googleAccessTokenExpirationDate")
            UserDefaults.standard.synchronize()
            self.goToHomeVC(email!, "")
            checkSocialUserLogin(email!, fullName!)
        }
        else
        {
            print("\(error.localizedDescription)")
        }
    }
    
    func configureGooglePhotos()
    {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().scopes = ["https://www.googleapis.com/auth/photoslibrary"]
    }
    
    func performAnonymousRegister(successClosure: @escaping ()-> Void) {
        customLoader.showActivityIndicator(viewController: self.view)
        AppManager.shared.userManager.registerAnonymously { [weak self] (response) in
            customLoader.hideActivityIndicator()
            switch response {
            case .failure(error: let error):
                ZLogManagerWrapper.sharedInstance.logError(message: "failed to register anonymously with error: \(error)")
                
                self?.presentAlertControllerWithPopupRepresentable(error, cancelButtonText: "OK".localisedString())
            case .success(_):
                ZLogManagerWrapper.sharedInstance.logInfo(message: "success in anonymous register")
                Copilot.instance.report.log(event: SignupAnalyticsEvent())
                successClosure()
            }
        }
    }
}
