//
//  ViewController.swift
//  Mint_Login
//
//  Created by MAXIMESS142 on 22/08/18.
//  Copyright © 2018 MAXIMESS142. All rights reserved.
//

import UIKit
import Photos
import CopilotAPIAccess

class SignUpViewController: UIViewController,CallToActiveCreateBtnDelegate{
    
    //MARK: - IBOutlets -
    @IBOutlet weak var viewBottomSignUp: UIView!
    @IBOutlet weak var txtFullName: PaddingTextField!
    @IBOutlet weak var txtEmail: PaddingTextField!
    @IBOutlet weak var txtPassword: PaddingTextField!
    @IBOutlet weak var txtDateOfBirth: PaddingTextField!
    @IBOutlet weak var btnCreateUser: UIButton!
    @IBOutlet weak var lblSignUp: UILabel!
    @IBOutlet weak var txtViewPrivacyPolicy: MyTextView!
    @IBOutlet weak var lblValidationMsgName: UILabel!
    @IBOutlet weak var lblValidationMsgEmail: UILabel!
    @IBOutlet weak var lblValidatePw: UILabel!
    @IBOutlet weak var lblValidateDob: UILabel!
    // - Constraint IBOutlets -
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var lblAlreadyHaveAccount: UILabel!
    @IBOutlet weak var lblIllDoItLater: UILabel!
    
    @IBOutlet weak var constraintViewBottomSignUpTop: NSLayoutConstraint!
    @IBOutlet weak var constraintViewFullNameTop: NSLayoutConstraint!
    @IBOutlet weak var constraintViewEmailTop: NSLayoutConstraint!
    @IBOutlet weak var constraintViewPasswordTop: NSLayoutConstraint!
    @IBOutlet weak var constraintViewDOBTop: NSLayoutConstraint!
    @IBOutlet weak var constraintLblSignUpBtm: NSLayoutConstraint!
    @IBOutlet weak var constraintPrivacyPolicyTxtVwLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintPrivacyPolicyTxtVwTrailing: NSLayoutConstraint!
    
    //MARK: - Variables -
    var wholeString = "By signing up you agree to our\nTerms & Conditions and "
    var privacyPolicyStr = "Privacy Policy."
    var isPrivacyPolicyPageOpened = false    // **Flag to show that Privacy Policy Page is Opened.
    let saveSignInData:UserDefaults = UserDefaults.standard
    var selectedDate = Date()
    var unchecked = true
  
    
    //MARK: Constants for password policy.
    
    private let minNameCharacters = 2
    var maxPasswordLength: Int? = nil
    var rejectWhiteSpace = false

    //MARK: - View LifeCycle Functions -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.privacyPolicyAction()
        self.addObservers()
        self.localizedStrings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // self.addLinearGradient()
        self.addLineUnderAllTxtFields()
        self.clearTextFields()
        self.disableBtnOnFieldsEmpty()
        self.detectIphone5sDevice()
        self.lblValidationMsgName.isHidden = true
        self.lblValidatePw.isHidden = true
        self.lblValidationMsgEmail.isHidden = true
        self.lblValidateDob.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        isPrivacyPolicyPageOpened = false
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.signUp.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        self.removeObservers()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    //MARK: - IBActions -
    
    @IBAction func btnIwillDoItClicked(_ sender: UIButton) {
        
        UserDefaults.standard.set(false, forKey: "isJoinKodak")
        
        //If user already login using i'll do it later then direct go to home.
        if UserDefaults.standard.bool(forKey: "iWillDoItLater") {
            if WebserviceModelClass().isInternetAvailable() {
                customLoader.showActivityIndicator(viewController: self.view)
                AppManager.shared.userManager.attemptToSilentLogin {[weak self] (response) in
                    DispatchQueue.main.async {
                        customLoader.hideActivityIndicator()
                        self?.moveToLegalPage()
                    }
                }
            } else {
                moveToLegalPage()
            }
            return
        }
        if WebserviceModelClass().isInternetAvailable() {
            performAnonymousRegister { [weak self] in
                DispatchQueue.main.async {
                    self?.moveToLegalPage()
                }
            }
        } else {
            moveToLegalPage()
        }
        UserDefaults.standard.set(true, forKey: "iWillDoItLater")
    }
    
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        isFacebookLogin = false
        let loginVC = storyboards.onboardingStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    @IBAction func btnCheckBoxClicked(_ sender: Any) {
        
        if self.unchecked {
            self.btnCheckBox.setImage(UIImage(named: "mark"), for: .normal)
            self.unchecked = false
        }
        else {
            self.btnCheckBox.setImage(UIImage(named: "checkbox"), for: .normal)
            self.unchecked = true
        }
        
         if ((txtFullName.text?.isEmpty)! || (txtEmail.text?.isEmpty)! || (txtPassword.text?.isEmpty)! ||  self.unchecked) || (!self.lblValidatePw.isHidden)
        {
            disableBtnAnimated(btnCreateUser)
        }
        else
        {
            enableBtnAnimated(btnCreateUser)
        }
        
    }
    
    @IBAction func showPswdBtnClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.txtPassword.isSecureTextEntry = !self.txtPassword.isSecureTextEntry
    }
    
    @IBAction func dismissBtnClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createUserBtnClicked(_ sender: UIButton)
    {
        let tapCreateAccountAnalyticsEvent = TapCreateAccountAnalyticsEvent(screenName: AnalyticsEventName.Screen.signUp.rawValue)
        Copilot.instance.report.log(event: tapCreateAccountAnalyticsEvent)
        txtEmail.resignFirstResponder()
        guard !(txtFullName.text?.isEmpty)! else
        {
            self.showAlert(title: "Alert", message: "Please enter full name.")
            return
        }
        guard !(txtEmail.text?.isEmpty)! else
        {
            self.showAlert(title: "Alert", message: "Please enter email.")
            return
        }
        guard !(txtPassword.text?.isEmpty)! else
        {
            self.showAlert(title: "Alert", message: "Please enter password.")
            return
        }
        guard (txtEmail.text?.isValidEmail)! else
        {
            self.showAlert(title: "Alert", message: "Please Enter Your Email-Id".localisedString())
            return
        }
        guard validatePassword(txtPassword.text!) else
        {
            self.showAlert(title: "Alert", message: passwordErrorMsg)
            return
        }
        if !txtDateOfBirth.text!.isEmpty {
           self.isDateMatch()
        }
//        self.checkRegisteredUserInSignUp(txtFullName.text!, txtEmail.text!, txtPassword.text!, txtDateOfBirth.text!)
        print("Valid Email and Password")
        if WebserviceModelClass().isInternetAvailable(){
            self.performRegister()
        }else{
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            comeFromStrNoInternetSocialMedia = "signUpNoInternet"
            self.present(alertPopUp, animated: true, completion: nil)
        }
    }
    
    @IBAction func dateTextFieldEditing(_ sender: PaddingTextField) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        sender.inputView = datePickerView
        sender.inputAccessoryView = createToolBar()
        datePickerView.date = selectedDate
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    // IB Action on textFields for editing changed
    // Enables button after all textfields are filled
    @IBAction func editingChangedForTxtField(_ sender: PaddingTextField) {
         if ((txtFullName.text?.isEmpty)! || (txtEmail.text?.isEmpty)! || (txtPassword.text?.isEmpty)! ||  self.unchecked) 
        {
            disableBtnAnimated(btnCreateUser)
        }
        else
        {
            enableBtnAnimated(btnCreateUser)
        }
    }
    
    //MARK: - Functions -
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard !(txtFullName.text?.isEmpty)! else
        {
            self.showAlert(title: "Alert", message: "Please enter full name.")
            return
        }
        guard (txtEmail.text?.isValidEmail)! else
        {
            self.showAlert(title: "Alert", message: "Please enter valid email".localisedString())
            return
        }
        guard !(txtPassword.text?.isEmpty)! else
        {
            self.showAlert(title: "Alert", message: "Please enter password.")
            return
        }
        guard validatePassword(txtPassword.text!) else
        {
            self.showAlert(title: "Alert", message: passwordErrorMsg)
            return
        }
    }
    
    func localizedStrings() {
        self.lblSignUp.text = "Sign Up".localisedString()
        self.lblFullName.text = "Full Name".localisedString()
        self.lblEmail.text = "Email".localisedString()
        self.lblPassword.text = "Password".localisedString()
        self.lblDOB.text = "Date of Birth".localisedString()
        self.btnCreateUser.setTitle("Create Account".localisedString(), for: .normal)
        self.lblLogin.text = "Log In".localisedString()
        self.lblAlreadyHaveAccount.text = "Already have an account?".localisedString()
        self.lblIllDoItLater.text = "I'll do it later".localisedString()
    }
    
    func addLinearGradient()
    {
        // - Colors -
        let color1 = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
        let color2 = UIColor(red: 49/255, green: 49/255, blue: 49/255, alpha: 1.0)
        let color3 = UIColor(red: 84/255, green: 84/255, blue: 84/255, alpha: 1.0)
        // - Color Layers -
        let newLayer1 = CAGradientLayer()
        newLayer1.colors = [color1.cgColor,color2.cgColor,color3.cgColor]
        newLayer1.frame = self.view.frame
        let newLayer2 = CAGradientLayer()
        newLayer2.colors = [color1.cgColor,color2.cgColor,color3.cgColor]
        newLayer2.frame = self.view.frame
        self.viewBottomSignUp.layer.insertSublayer(newLayer2, at: 0)
    }
    
    func addLineUnderAllTxtFields()
    {
        self.txtEmail.addLineToView(view: txtEmail, position: .LINE_POSITION_BOTTOM, color: UIColor.white, width: 1)
        self.txtPassword.addLineToView(view: txtPassword, position: .LINE_POSITION_BOTTOM, color: UIColor.white, width: 1)
        self.txtFullName.addLineToView(view: txtFullName, position: .LINE_POSITION_BOTTOM, color: UIColor.white, width: 1)
        self.txtDateOfBirth.addLineToView(view: txtDateOfBirth, position: .LINE_POSITION_BOTTOM, color: UIColor.white, width: 1)
    }
    
    func disableBtnOnFieldsEmpty(){
        if (txtFullName.text?.isEmpty)! || (txtEmail.text?.isEmpty)! || (txtPassword.text?.isEmpty)! {
            disableBtnAnimated(btnCreateUser)
        }
    }
    
    private func performRegister() {
        guard let fullName = txtFullName.text,
            let email = txtEmail.text,
            let password = txtPassword.text else { return }
        let dob = txtDateOfBirth.text! ?? ""
        UserDefaults.standard.set( false, forKey: "acceptTermsAndConditions")
        UserDefaults.standard.synchronize()
        if let user = AppManager.shared.userManager.user, user.isAnonymous {
            performElevateAnonymous(withEmail: email, password: password, fullName: fullName, dob: dob)
        } else {
            if UserDefaults.standard.bool(forKey: "isJoinKodak"){
                performElevateAnonymous(withEmail: email, password: password, fullName: fullName, dob: dob)
            }else{
                performRegister(withEmail: email, password: password, fullName: fullName, dob: dob)
            }
        }
    }
    
    func setUserInformation(email:String,password:String){
         UserDefaults.standard.set(false, forKey: "iWillDoItLater")
        saveSignInData.set(email, forKey: "emailKodak")
        saveSignInData.set(password, forKey: "passwordKodak")
        saveSignInData.set(true, forKey: "switchStateKodak")
        UserDefaults.standard.set(false, forKey: "isJoinKodak")
        saveSignInData.synchronize()
        self.goToHomeController()
    }

    func createToolBar() -> UIToolbar {
        let toolBar = UIToolbar(frame: CGRect(x: 200, y: 10, width: 320, height: 44))
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.clear
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(dateResignResponder))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        doneButton.tintColor = UIColor(red: 0/255, green: 155/255, blue: 250/255, alpha: 1.0)           //Color :- Dark Sky Blue
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }

    func privacyPolicyAction(){
        let text = NSMutableAttributedString(string: wholeString)
        text.addAttributes([NSAttributedStringKey.font:UIFont(name: "SFProDisplay-Regular", size: 12.0)!, NSAttributedStringKey.foregroundColor:UIColor.black], range: NSMakeRange(0, text.length))
        let selectablePart = NSMutableAttributedString(string:privacyPolicyStr)
        selectablePart.addAttributes([NSAttributedStringKey.font:UIFont(name: "SFProDisplay-Regular", size: 12.0)!, NSAttributedStringKey.underlineStyle:NSUnderlineStyle.styleSingle.rawValue, NSAttributedStringKey.underlineColor:UIColor.black, NSAttributedStringKey.link:"privacyPolicy"], range: NSMakeRange(0, selectablePart.length))
        txtViewPrivacyPolicy.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue:UIColor.black] as [String:Any]
        text.append(selectablePart)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        text.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, text.length))
        txtViewPrivacyPolicy.attributedText = text
    }

    func addObservers()
    {
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
            self.keyboardWillShow(notification: notification)
        }
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) { (notification) in
            self.keyboardWillHide(notification: notification)
        }
    }

    func removeObservers()
    {
        NotificationCenter.default.removeObserver(self)
    }

    func keyboardWillShow(notification: Notification)
    {
//        UIView.animate(withDuration: 1.0) {
//            // Getting the height of Keyboard
//            let userInfo:NSDictionary = notification.userInfo! as NSDictionary
//            let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
//            let keyboardHeight = keyboardFrame.cgRectValue.height
//            // Moving bottom view containing textfields up.
//            self.constraintViewBottomSignUpTop.constant = -(keyboardHeight-140.0)
//            self.constraintLblSignUpBtm.constant = 25.0
//            self.detectIphone5sDevice()
//            self.view.layoutIfNeeded()
//        }
    }

    func keyboardWillHide(notification: Notification)
    {
//        UIView.animate(withDuration: 1.0) {
//            self.constraintViewBottomSignUpTop.constant = 0.0
//            self.detectIphone5sDevice()
//            self.view.layoutIfNeeded()
//        }
    }
    
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            self.constraintViewFullNameTop.constant = 20.0
            self.constraintViewEmailTop.constant = 5.0
            self.constraintViewPasswordTop.constant = 5.0
            self.constraintViewDOBTop.constant = 5.0
        }else{
            print("Unknown")
        }
    }
    
    //MARK: - API Calling Functions -
    func checkRegisteredUserInSignUp(_ fullname: String,_ email: String,_ password: String,_ dateOfBirth: String)
    {
        let signupEvent = SignupCustomAnalyticsEvent(dateOfBirth: dateOfBirth)
        Copilot.instance.report.log(event: signupEvent)
        let requestParameters = ["name": fullname, "email" : email, "password": password, "dob": dateOfBirth ,"login_with": "normal"] as [String : Any]
        if WebserviceModelClass().isInternetAvailable(){
            customLoader.showActivityIndicator(viewController: self.view)
            WebserviceModelClass().postDataFor(module: "SignUpViewController", subUrl: SIGNUP_URL, parameter: requestParameters) { (isSuccess, responseMsg, arrayJSONObj) in
                DispatchQueue.main.async {
                    if isSuccess{
                        customLoader.hideActivityIndicator()
                        print("Success")
                        self.saveSignInData.set(fullname, forKey: "userNameLoggedIn")
                        self.saveSignInData.synchronize()
                        self.goToHomeVC(email, password)
                    }else{
                        customLoader.hideActivityIndicator()
                        self.showAlert(title: "Alert", message: responseMsg)
                    }
                }
            }
        }else{
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            comeFromStrNoInternetSocialMedia = "signUpNoInternet"
            self.present(alertPopUp, animated: true, completion: nil)
        }
    }

    func goToHomeVC(_ email: String,_ password: String)
    {
        if !UserDefaults.standard.bool(forKey: "onboardingSkipped"){
        let onboardingStartedEvent = OnboardingStartedAnalayticsEvent(flowID: "fromPush", screenName: "onboarding_started")
        Copilot.instance.report.log(event: onboardingStartedEvent)
        UserDefaults.standard.set(true, forKey: "onboarding_started")
        }
            let onboardingOneVC = storyboards.printerOnBoardingStoryboard.instantiateInitialViewController()
            self.navigationController?.present(onboardingOneVC!, animated: false, completion: nil)
            self.setUserInformation(email: email, password: password)
    }

    func clearTextFields(){
        if isPrivacyPolicyPageOpened{
            print("Keeping the text in field.")
        }else{
            self.txtFullName.text = ""
            self.txtEmail.text = ""
            self.txtPassword.text = ""
            self.txtDateOfBirth.text = ""
        }
    }

    func checkSocialUserLogin(_ email: String, _ fullName: String)
       {
           let requestParameters = ["email" : email,"login_with": "social"] as [String : Any]
           if WebserviceModelClass().isInternetAvailable(){
               WebserviceModelClass().postDataFor(module: "WelcomeViewController", subUrl: SIGNUP_URL, parameter: requestParameters) { (isSuccess, responseMsg, arrayJSONObj) in
                   DispatchQueue.main.async {
                       if isSuccess{
                           print(arrayJSONObj)
                           self.saveSignInData.set(fullName, forKey: "userNameLoggedIn")
                           self.saveSignInData.synchronize()
                           self.goToHomeVC(email, "")
                       }else{
                           customLoader.hideActivityIndicator()
                           self.showAlert(title: "Alert", message: responseMsg)
                       }
                   }
               }
           }else{
               self.showAlert()
           }
       }
    
    func showAlert() {
        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
        comeFromStrNoInternetSocialMedia = "NoInternet"
        self.present(alertPopUp, animated: true, completion: nil)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        let currentDate = Date()
        sender.minimumDate = dateFormatter.date(from: "01-Jan-1950")
//        sender.maximumDate = currentDate
        selectedDate = sender.date
        txtDateOfBirth.text = dateFormatter.string(from: selectedDate)
    }
    
    func isDateMatch(){
        txtDateOfBirth.resignFirstResponder()
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"
        let currentDate = Date()
        var current_date = dateFormatterPrint.string(from: currentDate)
        var selected_date = dateFormatterPrint.string(from: selectedDate)
        if current_date.caseInsensitiveCompare(selected_date) == .orderedSame{
            self.lblValidateDob.isHidden = false
            self.lblValidateDob.text = "Please enter valid date"
            print("Both dates are same")
            self.txtDateOfBirth.setLineColor(color: UIColor(red: 10/255.0, green: 165/255.0, blue: 190/255.0, alpha: 1.0))
           // self.showAlert(title: "Alert", message: "Please enter valid date.")
            txtDateOfBirth.text = ""
        }
        else if currentDate < selectedDate{
            self.lblValidateDob.isHidden = false
            self.lblValidateDob.text = "Please enter valid date"
            print("Both dates are same")
            self.txtDateOfBirth.setLineColor(color: UIColor(red: 10/255.0, green: 165/255.0, blue: 190/255.0, alpha: 1.0))
           // self.showAlert(title: "Alert", message: "Please enter valid date.")
            txtDateOfBirth.text = ""
        }else if currentDate > selectedDate{
            self.lblValidateDob.isHidden = true
            self.txtDateOfBirth.setLineColor(color: .white)
        }
    }
    
    func callToActiveCreateBtnResponseCompletion(_ status: Bool) {
        if self.unchecked {
            self.btnCheckBox.setImage(UIImage(named: "checkBoxSelected"), for: .normal)
            self.unchecked = false
        }
        else {
            self.btnCheckBox.setImage(UIImage(named: "checkBoxEmpty"), for: .normal)
            self.unchecked = true
        }

        if ((txtFullName.text?.isEmpty)! || (txtEmail.text?.isEmpty)! || (txtPassword.text?.isEmpty)! ||  self.unchecked) || (!self.lblValidatePw.isHidden)
        {
            disableBtnAnimated(btnCreateUser)
        }
        else
        {
            enableBtnAnimated(btnCreateUser)

        }
    }
    
    @objc func dateResignResponder()
    {
       self.isDateMatch()
    }
    
}
