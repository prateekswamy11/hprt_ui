//
//  StartViewControllerTextFieldDelegates.swift
//  Kodak Smile
//
//  Created by MAXIMESS183 on 10/02/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation
import UIKit

extension StartViewController: UITextFieldDelegate, UITextViewDelegate{

    //MARK: - TextField Delegates -
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }

      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         if string == " " && (textField == txtEmail || textField == txtPassword)
         {
             return false
         }
         else
         {
             if txtPassword.isPasswordFieldValidated() && textField == txtPassword{
                 self.lblValidatePw.isHidden = true
             }else if textField == txtPassword{
                 self.lblValidatePw.isHidden = false
                 self.lblValidatePw.text = "At least 8 characters, at least one lowercase and one uppercase letter".localisedString()
             }
             return true
         }
     }

    //MARK: - TextView Delegate -
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        if !WebserviceModelClass().isInternetAvailable()
        {
            isComeFrom = "SignUPVC"
            isPrivacyPolicyPageOpened = true
            let legalVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
            legalVC.delegateActiveCreateBtn = self
            self.present(legalVC, animated: false, completion: nil)

            return false
        }
        else
        {
            isComeFrom = "SignUPVC"
            isPrivacyPolicyPageOpened = true
            let legalVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
            legalVC.delegateActiveCreateBtn = self
            self.present(legalVC, animated: false, completion: nil)
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        print("textFieldDidEndEditing")
        
        if textField.tag == 1
        {
            //self.showAlert(title: "Alert", message: "Please enter full name.")
            if textField.text?.isEmpty ?? false {
                self.lblValidationMsgName.isHidden = false
                self.lblValidationMsgName.text = "Please enter full name."
                self.txtFullName.setLineColor(color: UIColor(red: 10/255.0, green: 165/255.0, blue: 190/255.0, alpha: 1.0))
            } else {
                self.lblValidationMsgName.isHidden = true
                self.txtFullName.setLineColor(color: .white)
            }
        }
        if textField.tag == 2 {
            if textField.text?.isEmpty ?? false
            {
                // self.showAlert(title: "Alert", message: "Please enter email.")
                self.lblValidationMsgEmail.isHidden = false
                self.lblValidationMsgEmail.text = "Please enter email."
                self.txtEmail.setLineColor(color: UIColor(red: 10/255.0, green: 165/255.0, blue: 190/255.0, alpha: 1.0))
            }else if !(textField.text?.isValidEmail ?? false) {
                self.lblValidationMsgEmail.isHidden = false
                self.lblValidationMsgEmail.text = "Please enter valid email-id".localisedString()
                self.txtEmail.setLineColor(color: UIColor(red: 10/255.0, green: 165/255.0, blue: 190/255.0, alpha: 1.0))
            }else {
                self.lblValidationMsgEmail.isHidden = true
                self.txtEmail.setLineColor(color: .white)
            }
        }
        if textField.tag == 3 {
            if textField.text?.isEmpty ?? false
            {
                // self.showAlert(title: "Alert", message: "Please enter password.")
                self.lblValidatePw.isHidden = false
                self.lblValidatePw.text = "Please enter password."
                self.txtPassword.setLineColor(color: UIColor(red: 10/255.0, green: 165/255.0, blue: 190/255.0, alpha: 1.0))
            }else if !validatePassword(textField.text!) {
                self.lblValidatePw.isHidden = false
                self.lblValidatePw.text = passwordErrorMsg
                self.txtPassword.setLineColor(color: UIColor(red: 10/255.0, green: 165/255.0, blue: 190/255.0, alpha: 1.0))
            } else {
                self.lblValidatePw.isHidden = true
                self.txtPassword.setLineColor(color: .white)
            }
        }
        if textField.tag == 4 && !textField.text!.isEmpty {
            self.isDateMatch()
        }
    }
}

//class MyTextView: UITextView {
//    override func becomeFirstResponder() -> Bool {
//        return false
//    }
//
//    override var selectedTextRange: UITextRange? {
//        get { return nil }
//        set {}
//    }
//}

