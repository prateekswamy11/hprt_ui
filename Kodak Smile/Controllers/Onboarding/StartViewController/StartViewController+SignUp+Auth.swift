//
//  StartViewController+SignUp+Auth.swift
//  Kodak Smile
//
//  Created by MAXIMESS183 on 10/02/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation
import CopilotLogger
import CopilotAPIAccess

extension StartViewController {
    
    func performRegister(withEmail email:String, password: String, fullName: String, dob: String) {
        customLoader.showActivityIndicator(viewController: self.view)
        AppManager.shared.userManager.register(withEmail: email, password: password, firstName: getFirstOrLastNameFrom(fullName: fullName, firstName: true), lastName: getFirstOrLastNameFrom(fullName: fullName, firstName: false)) { [weak self] (response) in
            switch response {
            case .failure(error: let error):
                ZLogManagerWrapper.sharedInstance.logError(message: "failed to register with error: \(error)")
                var analyticsFailureReason = ""
                switch error {
                case .server:
                    analyticsFailureReason = AnalyticsValue.accountCreationFailed.serverError.rawValue
                case .signUpUserExists:
                    analyticsFailureReason = AnalyticsValue.accountCreationFailed.userAlreadExist.rawValue
                    // Show user already exist message popup.
                    let alert = UIAlertController(title: analyticsFailureReason.localisedString(), message: error.title, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK".localisedString(), style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Forgot Password".localisedString(), style: .default, handler: { (action) in
                        let ForgotPasswordVC = storyboards.onboardingStoryboard.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
                        self?.navigationController?.pushViewController(ForgotPasswordVC, animated: true)
                    }))
                    self?.present(alert, animated: true, completion: nil)
                    
                default:
                    break
                }
                let accountCreationFailedAnalyticsEvent = AccountCreationFailedAnalyticsEvent(failureReason: analyticsFailureReason)
                Copilot.instance.report.log(event:accountCreationFailedAnalyticsEvent)
                let errorReportAnalyticsEvent = ErrorReportAnalyticsEvent(errorType: AnalyticsValue.errorReport.accountCreationFailed.rawValue)
                Copilot.instance.report.log(event: errorReportAnalyticsEvent)
                customLoader.hideActivityIndicator()
            case .success(_):
                self?.acceptTermsAndPolicy()
                ZLogManagerWrapper.sharedInstance.logInfo(message: "success in register")
                Copilot.instance.report.log(event: SignupAnalyticsEvent())
                self?.saveSignInData.set(fullName, forKey: "userNameLoggedIn")
                self?.saveSignInData.synchronize()
                DispatchQueue.main.async {
                    UserDefaults.standard.set(false, forKey: "onboardingSkipped")
                    self?.setUserInformation(email: email, password: password)
                    FacebookHelper().logoutOffFacebook()
                    GoogleHelper().logoutOffGoogle()
                    InstagramHelper().logoutOffInstagram()
                    customLoader.hideActivityIndicator()
                }
            }
        }
    }
    
    func performElevateAnonymous(withEmail email:String, password: String, fullName: String, dob: String) {
        customLoader.showActivityIndicator(viewController: self.view)
        AppManager.shared.userManager.elevateAnonymous(withEmail: email, password: password, firstName: getFirstOrLastNameFrom(fullName: fullName, firstName: true), lastName: getFirstOrLastNameFrom(fullName: fullName, firstName: false)) { [weak self] (response) in
            customLoader.hideActivityIndicator()
            switch response {
            case .failure(error: let error):
                ZLogManagerWrapper.sharedInstance.logError(message: "failed to elevate anonymous with error: \(error)")
                self?.presentAlertControllerWithPopupRepresentable(error, cancelButtonText: "OK".localisedString())
            case .success(_):
                ZLogManagerWrapper.sharedInstance.logInfo(message: "success in  elevate anonymous")
                let successfulElevateAnonymousAnalyticEvent = SuccessfulElevateAnonymousAnalyticEvent()
                Copilot.instance.report.log(event: successfulElevateAnonymousAnalyticEvent)
            
                //Comment for remove it from app. No require to register on app server.
                //self?.checkRegisteredUserInSignUp(fullName, email, password, dob)
                self?.acceptTermsAndPolicy()
                print("Success")
                self?.saveSignInData.set(fullName, forKey: "userNameLoggedIn")
                self?.saveSignInData.synchronize()
                self?.setUserInformation(email: email, password: password)
            }
        }
    }
    
    func acceptTermsAndPolicy() {
        AppManager.shared.userManager.userAgreedToTOU { (response) in
            switch response {
            case .success():
                ZLogManagerWrapper.sharedInstance.logFatal(message: "Success to accept TOU")
            case .failure(error: let error):
                ZLogManagerWrapper.sharedInstance.logFatal(message: "Failed to accept TOU with error: \(error.localizedDescription)")
            }
        }
    }

    // MARK: - Private Functions
    private func getFirstOrLastNameFrom(fullName: String, firstName: Bool) -> String {
        let fullNameWithoutWhitepaces = fullName.trimmingCharacters(in: .whitespacesAndNewlines)
        var fullNameArray = fullNameWithoutWhitepaces.components(separatedBy: CharacterSet.whitespaces)
        if firstName {
            var returnedFirstName = ""
            
            if let firstName = fullNameArray.first {
                returnedFirstName = firstName
            }
            return returnedFirstName
        }
        else {
            //Remove the first name
            fullNameArray.removeFirst()
            var returnedLastName = ""
            if fullNameArray.count > 0 {
                for index in 0...fullNameArray.count - 1 {
                    returnedLastName.append(fullNameArray[index])
                    returnedLastName.append(" ")
                }
            }
            return returnedLastName
        }
    }
}

