//
//  Start+SignUp+AuthPasswordValidator.swift
//  Kodak Smile
//
//  Created by MAXIMESS183 on 10/02/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation

var passwordErrorMsg = ""

extension StartViewController : AuthPasswordValidator {
    
    // MARK: - Password Policy
    func validatePassword(_ pass: String?) -> Bool {
        guard let passwordRulesList = AppManager.shared.userManager.passwordRulesList else {
            //Local validation
            let isPasswordValid = isPasswordFieldValidated(pass!)
            handlePasswordFieldValidateState(isPasswordValid)
            return isPasswordValid
        }
        var error = "Password must include: ".localisedString()
        var errorMessage = ""
        var isValid = true
        for rule in passwordRulesList {
            switch rule.ruleId {
            case .minimumLength:
                errorMessage = "at least %@ characters".localisedString() + ", "
                let minPasswordLength = rule.numericalValue
                errorMessage = String(format: errorMessage, String(minPasswordLength))
            case .maximumLength:
                let maxPasswordLengthByServer = rule.numericalValue
                maxPasswordLength = maxPasswordLengthByServer
            case .maximumConsecutiveIdentical:
                errorMessage = "max %@ consetutive identical latters".localisedString() + ", "
                let maximumConsecutiveIdentical = rule.numericalValue
                errorMessage = String(format: errorMessage, String(maximumConsecutiveIdentical))
            case .complexity_MinimumUpperCase:
                errorMessage = "at least %@ uppercase letters".localisedString() + ", "
                let minimumUpperCase = rule.numericalValue
                errorMessage = String(format: errorMessage, String(minimumUpperCase))
            case .complexity_MinimumLowerCase:
                errorMessage = "at least %@ lowercase letters".localisedString() + ", "
                let minimumLowerCase = rule.numericalValue
                errorMessage = String(format: errorMessage, String(minimumLowerCase))
            case .complexity_MinimumDigits:
                errorMessage = "at least %@ numbers".localisedString() + ", "
                let minimumDigits = rule.numericalValue
                errorMessage = String(format: errorMessage, String(minimumDigits))
            case .complexity_MinimumSpecialChars:
                errorMessage = "at least %@ symbols".localisedString() + ", "
                let minimumSpecialChars = rule.numericalValue
                errorMessage = String(format: errorMessage, String(minimumSpecialChars))
            case .rejectWhiteSpace:
                self.rejectWhiteSpace = true
            }
            if let password = pass {
                if !rule.isValid(password: password) {
                    isValid = false
                    error += errorMessage
                }
            } else {
                isValid = false
                error += errorMessage
            }
        }
        if !isValid {
            //Remove Last Two Characters ", " in a String
            let endIndex = error.index(error.endIndex, offsetBy: -2)
            let finalError = String(error[..<endIndex])
            passwordErrorMsg = finalError
        }
        return isValid
    }
}

