//
//  StartViewController.swift
//  Kodak Smile
//
//  Created by MacMini001 on 10/01/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import GoogleSignIn
import GTMSessionFetcher
import GoogleAPIClientForREST
import Photos
import CopilotAPIAccess

class StartViewController: UIViewController ,CallToActiveCreateBtnDelegate{
    //MARK: - IBOutlets -
    @IBOutlet weak var imgVwKodakIcon: UIImageView!
    @IBOutlet weak var lblMakeItLast: UILabel!
    @IBOutlet weak var viewBottomImages: UIView!
    @IBOutlet weak var viewILLDoItLater: UIView!
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var viewFacebookLink: UIView!
    @IBOutlet weak var viewGoogleLink: UIView!
    @IBOutlet weak var viewEmailLink: UIView!
    @IBOutlet weak var lblSignUpWithEmail: UILabel!
    @IBOutlet weak var lblSignUpWithGoogle: UILabel!
    @IBOutlet weak var lblSignUpWithFacebook: UILabel!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var lblAlreadyHaveAccount: UILabel!
    @IBOutlet weak var lblIllDoItLater: UILabel!
    @IBOutlet weak var lblPocketPrinter: UILabel!
    @IBOutlet weak var lblAppName: UILabel!
    
    // signUp page outlet
    //MARK: - IBOutlets -
    @IBOutlet weak var viewBottomSignUp: UIView!
    @IBOutlet weak var txtFullName: PaddingTextField!
    @IBOutlet weak var txtEmail: PaddingTextField!
    @IBOutlet weak var txtPassword: PaddingTextField!
    @IBOutlet weak var txtDateOfBirth: PaddingTextField!
    @IBOutlet weak var btnCreateUser: UIButton!
    @IBOutlet weak var lblSignUp: UILabel!
    @IBOutlet weak var txtViewPrivacyPolicy: MyTextView!
    @IBOutlet weak var lblValidationMsgName: UILabel!
    @IBOutlet weak var lblValidationMsgEmail: UILabel!
    @IBOutlet weak var lblValidatePw: UILabel!
    @IBOutlet weak var lblValidateDob: UILabel!
    // - Constraint IBOutlets -
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    
    // - Constraint IBOutlets -
    @IBOutlet weak var constraintKodakIconHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintKodakIconWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintTopViewYAxis: NSLayoutConstraint!
    @IBOutlet weak var constraintKodakIconBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintImgSplashKidsTop: NSLayoutConstraint!
    @IBOutlet weak var constraintImgSplashGirlsTop: NSLayoutConstraint!
    
    @IBOutlet weak var constraintViewBottomSignUpTop: NSLayoutConstraint!
    @IBOutlet weak var constraintViewFullNameTop: NSLayoutConstraint!
    @IBOutlet weak var constraintViewEmailTop: NSLayoutConstraint!
    @IBOutlet weak var constraintViewPasswordTop: NSLayoutConstraint!
    @IBOutlet weak var constraintViewDOBTop: NSLayoutConstraint!
    @IBOutlet weak var constraintLblSignUpBtm: NSLayoutConstraint!
    @IBOutlet weak var constraintViewIwillDoItTop: NSLayoutConstraint!
    @IBOutlet weak var constraintViewLoginTop: NSLayoutConstraint!
    
    //MARK: - Variables -
        var wholeString = "By signing up you agree to our\nTerms & Conditions and "
        var privacyPolicyStr = "Privacy Policy."
        var isPrivacyPolicyPageOpened = false    // **Flag to show that Privacy Policy Page is Opened.
        let saveSignInData:UserDefaults = UserDefaults.standard
        var selectedDate = Date()
        var uncheckedBox = true
       
        
        //MARK: Constants for password policy.
        
        private let minNameCharacters = 2
        var maxPasswordLength: Int? = nil
        var rejectWhiteSpace = false
    
    //MARK: - View LifeCycle Functions -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNotificationObserver()
        self.privacyPolicyAction()
        self.addObservers()
        self.configureGooglePhotos()
        self.localizedStringsSignUp()
        self.localizedStrings()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        isPrivacyPolicyPageOpened = false
        //self.slideKodakIconUp()
        //self.viewBottomImages.addLinearGradient()
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.start.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // self.addLinearGradient()
        self.addLineUnderAllTxtFields()
        self.clearTextFields()
        self.disableBtnOnFieldsEmpty()
        self.detectIphone5sDevice()
        self.lblValidationMsgName.isHidden = true
        self.lblValidatePw.isHidden = true
        self.lblValidationMsgEmail.isHidden = true
        self.lblValidateDob.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        self.removeObservers()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
        //MARK: - IBActions -
        @IBAction func btnCheckBoxClicked(_ sender: Any) {
            if uncheckedBox {
                self.btnCheckBox.setImage(UIImage(named: "mark"), for: .normal)
                uncheckedBox = false
            }
            else {
                self.btnCheckBox.setImage(UIImage(named: "checkbox"), for: .normal)
                uncheckedBox = true
            }
            
            if ((txtFullName.text?.isEmpty)! || (txtEmail.text?.isEmpty)! || (txtPassword.text?.isEmpty)! ||  self.uncheckedBox) || (!self.lblValidatePw.isHidden)
            {
                disableBtnAnimated(btnCreateUser)
            }
            else
            {
                enableBtnAnimated(btnCreateUser)
               
            }
            
        }
        
        @IBAction func showPswdBtnClicked(_ sender: UIButton) {
            sender.isSelected = !sender.isSelected
            self.txtPassword.isSecureTextEntry = !self.txtPassword.isSecureTextEntry
        }
        
        @IBAction func dismissBtnClicked(_ sender: UIButton)
        {
            self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func createUserBtnClicked(_ sender: UIButton)
        {
            let tapCreateAccountAnalyticsEvent = TapCreateAccountAnalyticsEvent(screenName: AnalyticsEventName.Screen.signUp.rawValue)
            Copilot.instance.report.log(event: tapCreateAccountAnalyticsEvent)
            txtEmail.resignFirstResponder()
            guard !(txtFullName.text?.isEmpty)! else
            {
                self.showAlert(title: "Alert", message: "Please enter full name.")
                return
            }
            guard !(txtEmail.text?.isEmpty)! else
            {
                self.showAlert(title: "Alert", message: "Please enter email.")
                return
            }
            guard !(txtPassword.text?.isEmpty)! else
            {
                self.showAlert(title: "Alert", message: "Please enter password.")
                return
            }
            guard (txtEmail.text?.isValidEmail)! else
            {
                self.showAlert(title: "Alert", message: "Please Enter Your Email-Id".localisedString())
                return
            }
            guard validatePassword(txtPassword.text!) else
            {
                self.showAlert(title: "Alert", message: passwordErrorMsg)
                return
            }
            if !txtDateOfBirth.text!.isEmpty {
               self.isDateMatch()
            }
    //        self.checkRegisteredUserInSignUp(txtFullName.text!, txtEmail.text!, txtPassword.text!, txtDateOfBirth.text!)
            print("Valid Email and Password")
            if WebserviceModelClass().isInternetAvailable(){
                self.performRegister()
            }else{
                let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                comeFromStrNoInternetSocialMedia = "signUpNoInternet"
                self.present(alertPopUp, animated: true, completion: nil)
            }
        }
        
        @IBAction func dateTextFieldEditing(_ sender: PaddingTextField) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            let datePickerView:UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.date
            sender.inputView = datePickerView
            sender.inputAccessoryView = createToolBar()
            datePickerView.date = selectedDate
            datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
        }
        
        // IB Action on textFields for editing changed
        // Enables button after all textfields are filled
        @IBAction func editingChangedForTxtField(_ sender: PaddingTextField) {
            if ((txtFullName.text?.isEmpty)! || (txtEmail.text?.isEmpty)! || (txtPassword.text?.isEmpty)! ||  self.uncheckedBox) //|| (!self.lblValidatePw.isHidden)
            {
                disableBtnAnimated(btnCreateUser)
            }
            else
            {
                enableBtnAnimated(btnCreateUser)
            }
        }

    
    //MARK: - IBActions -
    @IBAction func goToEmailRegistrationBtnClicked(_ sender: UIButton)
    {
        AppManager.shared.userManager.getPasswordRulesPolicy { (response) in
            isFacebookLogin = false
            let signUpVC = storyboards.onboardingStoryboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }
    }
    
    @IBAction func goToLoginVCBtnClicked(_ sender: UIButton)
    {
        isFacebookLogin = false
        let loginVC = storyboards.onboardingStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func goToFacebookBtnClicked(_ sender: UIButton)
    {
        if WebserviceModelClass().isInternetAvailable() {
            FacebookHelper().loginToFacebook(viewController: self)
//            UserDefaults.standard.set("facebookLogin", forKey: "emailKodak")
            isFacebookLogin = true
        }
        else {
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            comeFromStrNoInternetSocialMedia = "facebookLoginNoInternet"
            self.present(alertPopUp, animated: true, completion: nil)
        }
    }
    
    @IBAction func goToGoogleBtnClicked(_ sender: UIButton)
    {
        isFacebookLogin = false
        if WebserviceModelClass().isInternetAvailable() {
            GIDSignIn.sharedInstance().presentingViewController = self
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance()?.signIn()
//            UserDefaults.standard.set("googleLogin", forKey: "emailKodak")
        } else {
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            comeFromStrNoInternetSocialMedia = "googleLoginNoInternet"
            self.present(alertPopUp, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnIWillDoItlaterClicked(_ sender: UIButton) {
        
    
        UserDefaults.standard.set(false, forKey: "isJoinKodak")
        //If user already login using i'll do it later then direct go to home.
        if UserDefaults.standard.bool(forKey: "iWillDoItLater") {
            if WebserviceModelClass().isInternetAvailable() {
                customLoader.showActivityIndicator(viewController: self.view)
                AppManager.shared.userManager.attemptToSilentLogin {[weak self] (response) in
                    DispatchQueue.main.async {
                        customLoader.hideActivityIndicator()
                        self?.moveToLegalPage()
                    }
                }
            } else {
                moveToLegalPage()
            }
            return
        }
        if WebserviceModelClass().isInternetAvailable() {
            performAnonymousRegister { [weak self] in
                DispatchQueue.main.async {
                    self?.moveToLegalPage()
                }
            }
        } else {
            moveToLegalPage()
        }
        UserDefaults.standard.set(true, forKey: "iWillDoItLater")
    }
    
    //MARK: - Functions -
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard !(txtFullName.text?.isEmpty)! else
        {
            self.showAlert(title: "Alert", message: "Please enter full name.")
            return
        }
        
        guard (txtEmail.text?.isValidEmail)! else
        {
            self.showAlert(title: "Alert", message: "Please enter valid email".localisedString())
            return
        }
        
        guard !(txtPassword.text?.isEmpty)! else
        {
            self.showAlert(title: "Alert", message: "Please enter password.")
            return
        }
        
        guard validatePassword(txtPassword.text!) else
        {
            self.showAlert(title: "Alert", message: passwordErrorMsg)
            return
        }
        
    }

    func localizedStringsSignUp() {
        self.lblSignUp.text = "Sign Up".localisedString()
        self.lblFullName.text = "Full Name".localisedString()
        self.lblEmail.text = "Email".localisedString()
        self.lblPassword.text = "Password".localisedString()
        self.lblDOB.text = "Date of Birth".localisedString()
        self.btnCreateUser.setTitle("Create Account".localisedString(), for: .normal)
    }
    
    func addLinearGradient()
          {
              // - Colors -
              let color1 = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
              let color2 = UIColor(red: 49/255, green: 49/255, blue: 49/255, alpha: 1.0)
              let color3 = UIColor(red: 84/255, green: 84/255, blue: 84/255, alpha: 1.0)
              // - Color Layers -
              let newLayer1 = CAGradientLayer()
              newLayer1.colors = [color1.cgColor,color2.cgColor,color3.cgColor]
              newLayer1.frame = self.view.frame
              let newLayer2 = CAGradientLayer()
              newLayer2.colors = [color1.cgColor,color2.cgColor,color3.cgColor]
              newLayer2.frame = self.view.frame
              self.viewBottomSignUp.layer.insertSublayer(newLayer2, at: 0)
          }
    
    func clearTextFields(){
        if isPrivacyPolicyPageOpened{
            print("Keeping the text in field.")
        }else{
            self.txtFullName.text = ""
            self.txtEmail.text = ""
            self.txtPassword.text = ""
            self.txtDateOfBirth.text = ""
        }
    }
    
   
    
    func addLineUnderAllTxtFields()
    {
        self.txtEmail.addLineToView(view: txtEmail, position: .LINE_POSITION_BOTTOM, color: UIColor.white, width: 1)
        self.txtPassword.addLineToView(view: txtPassword, position: .LINE_POSITION_BOTTOM, color: UIColor.white, width: 1)
        self.txtFullName.addLineToView(view: txtFullName, position: .LINE_POSITION_BOTTOM, color: UIColor.white, width: 1)
        self.txtDateOfBirth.addLineToView(view: txtDateOfBirth, position: .LINE_POSITION_BOTTOM, color: UIColor.white, width: 1)
    }
    
    func disableBtnOnFieldsEmpty(){
        //Call form is date matching function.
        if (txtFullName.text?.isEmpty)! || (txtEmail.text?.isEmpty)! || (txtPassword.text?.isEmpty)! {
            disableBtnAnimated(btnCreateUser)
        }
    }
    
      func createToolBar() -> UIToolbar {
          let toolBar = UIToolbar(frame: CGRect(x: 200, y: 10, width: 320, height: 44))
          toolBar.barStyle = UIBarStyle.default
          toolBar.isTranslucent = true
          toolBar.tintColor = UIColor.clear
          toolBar.sizeToFit()
          let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(dateResignResponder))
          let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
          doneButton.tintColor = UIColor(red: 0/255, green: 155/255, blue: 250/255, alpha: 1.0)           //Color :- Dark Sky Blue
          toolBar.setItems([ spaceButton, doneButton], animated: false)
          toolBar.isUserInteractionEnabled = true
          return toolBar
      }

      func privacyPolicyAction(){
          let text = NSMutableAttributedString(string: wholeString)
          text.addAttributes([NSAttributedStringKey.font:UIFont(name: "SFProDisplay-Regular", size: 12.0)!, NSAttributedStringKey.foregroundColor:UIColor.black], range: NSMakeRange(0, text.length))
          let selectablePart = NSMutableAttributedString(string:privacyPolicyStr)
          selectablePart.addAttributes([NSAttributedStringKey.font:UIFont(name: "SFProDisplay-Regular", size: 12.0)!, NSAttributedStringKey.underlineStyle:NSUnderlineStyle.styleSingle.rawValue, NSAttributedStringKey.underlineColor:UIColor.black, NSAttributedStringKey.link:"privacyPolicy"], range: NSMakeRange(0, selectablePart.length))
         txtViewPrivacyPolicy.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue:UIColor.black] as [String:Any]
          text.append(selectablePart)
          let paragraphStyle = NSMutableParagraphStyle()
          paragraphStyle.alignment = NSTextAlignment.left
          text.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, text.length))
          txtViewPrivacyPolicy.attributedText = text
      }

      func addObservers()
      {
          NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
              self.keyboardWillShow(notification: notification)
          }
          NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) { (notification) in
              self.keyboardWillHide(notification: notification)
          }
      }

      func removeObservers()
      {
          NotificationCenter.default.removeObserver(self)
      }

      func keyboardWillShow(notification: Notification)
      {
          UIView.animate(withDuration: 1.0) {
              // Getting the height of Keyboard
              let userInfo:NSDictionary = notification.userInfo! as NSDictionary
              let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
              let keyboardHeight = keyboardFrame.cgRectValue.height
              // Moving bottom view containing textfields up.
//              self.constraintViewBottomSignUpTop.constant = -(keyboardHeight-140.0)
//            print("self.constraintTopViewYAxis.constant -- ",self.constraintTopViewYAxis.constant)
            //  self.constraintTopViewYAxis.constant = -(self.view.frame.height - 0.01)
           //   self.constraintLblSignUpBtm.constant = 25.0
              self.detectIphone5sDevice()
              self.view.layoutIfNeeded()
          }
      }

      func keyboardWillHide(notification: Notification)
      {
          UIView.animate(withDuration: 1.0) {
            //  self.constraintViewBottomSignUpTop.constant = 0.0
             // self.constraintTopViewYAxis.constant = -(self.view.frame.height - 85.0)
              self.detectIphone5sDevice()
              self.view.layoutIfNeeded()
          }
      }
      
      func detectIphone5sDevice()
      {
          if getCurrentIphone() == "5"{
              self.constraintViewFullNameTop.constant = 20.0
              self.constraintViewEmailTop.constant = 5.0
              self.constraintViewPasswordTop.constant = 5.0
              self.constraintViewDOBTop.constant = 5.0
              self.constraintViewIwillDoItTop.constant = 2.0
            self.constraintViewLoginTop.constant = 2.0
            
          }else{
              print("Unknown")
          }
      }
    
    func localizedStrings() {
//        self.lblSignUpWithEmail.text = "Sign Up With EMAIL".localisedString()
//        self.lblSignUpWithGoogle.text = "Sign Up With GOOGLE".localisedString()
//        self.lblSignUpWithFacebook.text = "Sign Up With FACEBOOK".localisedString()
        self.lblLogin.text = "Log In".localisedString()
        self.lblAlreadyHaveAccount.text = "Already have an account?".localisedString()
        self.lblIllDoItLater.text = "I'll do it later".localisedString()
//        self.lblPocketPrinter.text = "Instant Digital Printer".localisedString()
//        self.lblAppName.text = "KODAK SMILE".localisedString()
        //self.lblMakeItLast.text = "Make it last.".localisedString()
    }
    
    
    
    
    func slideKodakIconUp()
    {
//        UIView.animate(withDuration: 4.0, delay: 0.0, options: .beginFromCurrentState, animations: {
//            self.constraintKodakIconWidth.constant = 50.0
//            self.constraintKodakIconHeight.constant = 40.0
//
//            if UIDevice.current.hasNotch {
//                //... consider notch
//                self.constraintKodakIconBottom.constant = 12.0
//            } else {
//                //... don't have to consider notch
//                self.constraintKodakIconBottom.constant = 21.0
//            }
//            self.constraintTopViewYAxis.constant = -(self.view.frame.height - 85.0)
//            self.lblMakeItLast.alpha = 0.0
//            self.view.layoutIfNeeded()
//
//            Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: { (timer) in
//                UIView.animate(withDuration: 2.0, delay: 0.0, options: .curveLinear, animations: {
//                    self.constraintImgSplashKidsTop.constant = -(self.view.frame.height/4.5)
//                    self.view.layoutIfNeeded()
//
//                    Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false, block: { (timer) in
//                        UIView.animate(withDuration: 1.5, delay: 0.0, options: .curveLinear, animations: {
//                            self.constraintImgSplashGirlsTop.constant = -(self.view.frame.height/4.5)
//                            self.view.layoutIfNeeded()
//                        }, completion: nil)
//                    })
//                }, completion: nil)
//            })
//        }, completion: nil)
    }
    
    func showAlert() {
        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
        comeFromStrNoInternetSocialMedia = "NoInternet"
        self.present(alertPopUp, animated: true, completion: nil)
    }
    
    // MARK: - Logical Methods
    func addNotificationObserver()
    {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.facebookLogin(_:)),
            name: NSNotification.Name(rawValue: "fblogin"),
            object: nil)
    }
    
    @objc func facebookLogin(_ notification: NSNotification)
    {
        guard  let userInfo = notification.userInfo?["data"] as? [String:Any] else{
            return
        }
        if UserDefaults.standard.value(forKey: "isLoggedInToFacebookKodak") as? Bool == true
        {
            // Clearing all the google, Instagram information of previous user or clearing cache for same user.
//            GoogleHelper().logoutOffGoogle()
//            InstagramHelper().logoutOffInstagram()
            self.goToHomeVC(userInfo["email"] as! String ,"")
            self.checkSocialUserLogin(userInfo["email"] as! String, userInfo["first_name"] as! String)
        }
    }
    
    func goToHomeVC(_ email: String,_ password: String)
    {
        // Clearing all the Dropbox information of previous user or clearing cache for same user.
//        DropboxHelper().logoutDropbox()
        customLoader.hideActivityIndicator()
        UserDefaults.standard.set("login", forKey: "emailKodak")
        // Go to PrinterOnboarding screens.
        goToHomeController()
        UserDefaults.standard.set(false, forKey: "iWillDoItLater")
        LoginViewController().setUserInformation(email: email, password: password)
    }
    
    func checkSocialUserLogin(_ email: String, _ fullName: String)
    {
        let requestParameters = ["email" : email,"login_with": "social"] as [String : Any]
        if WebserviceModelClass().isInternetAvailable(){
            WebserviceModelClass().postDataFor(module: "WelcomeViewController", subUrl: SIGNUP_URL, parameter: requestParameters) { (isSuccess, responseMsg, arrayJSONObj) in
                DispatchQueue.main.async {
                    if isSuccess{
                        print(arrayJSONObj)
                        self.saveSignInData.set(fullName, forKey: "userNameLoggedIn")
                        self.saveSignInData.synchronize()
                        self.goToHomeVC(email, "")
                    }else{
                        customLoader.hideActivityIndicator()
                        self.showAlert(title: "Alert", message: responseMsg)
                    }
                }
            }
        }else{
            self.showAlert()
        }
    }
  
     private func performRegister() {
            guard let fullName = txtFullName.text,
                let email = txtEmail.text,
                let password = txtPassword.text else { return }
            let dob = txtDateOfBirth.text! ?? ""
            UserDefaults.standard.set( false, forKey: "acceptTermsAndConditions")
            UserDefaults.standard.synchronize()
            if let user = AppManager.shared.userManager.user, user.isAnonymous {
                performElevateAnonymous(withEmail: email, password: password, fullName: fullName, dob: dob)
            } else {
                if UserDefaults.standard.bool(forKey: "isJoinKodak"){
                    performElevateAnonymous(withEmail: email, password: password, fullName: fullName, dob: dob)
                }else{
                    performRegister(withEmail: email, password: password, fullName: fullName, dob: dob)
                }
            }
        }
    
    func setUserInformation(email:String,password:String){
         UserDefaults.standard.set(false, forKey: "iWillDoItLater")
        saveSignInData.set(email, forKey: "emailKodak")
        saveSignInData.set(password, forKey: "passwordKodak")
        saveSignInData.set(true, forKey: "switchStateKodak")
        UserDefaults.standard.set(false, forKey: "isJoinKodak")
        saveSignInData.synchronize()
        self.goToHomeController()
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            let currentDate = Date()
            sender.minimumDate = dateFormatter.date(from: "01-Jan-1950")
    //        sender.maximumDate = currentDate
            selectedDate = sender.date
            txtDateOfBirth.text = dateFormatter.string(from: selectedDate)
        }
    
    func isDateMatch(){
         txtDateOfBirth.resignFirstResponder()
         let dateFormatterPrint = DateFormatter()
         dateFormatterPrint.dateFormat = "dd MMM yyyy"
         let currentDate = Date()
         var current_date = dateFormatterPrint.string(from: currentDate)
         var selected_date = dateFormatterPrint.string(from: selectedDate)
         if current_date.caseInsensitiveCompare(selected_date) == .orderedSame{
             self.lblValidateDob.isHidden = false
             self.lblValidateDob.text = "Please enter valid date"
             print("Both dates are same")
             self.txtDateOfBirth.setLineColor(color: UIColor(red: 10/255.0, green: 165/255.0, blue: 190/255.0, alpha: 1.0))
            // self.showAlert(title: "Alert", message: "Please enter valid date.")
             txtDateOfBirth.text = ""
         }
         else if currentDate < selectedDate{
             self.lblValidateDob.isHidden = false
             self.lblValidateDob.text = "Please enter valid date"
             print("Both dates are same")
              self.txtDateOfBirth.setLineColor(color: UIColor(red: 10/255.0, green: 165/255.0, blue: 190/255.0, alpha: 1.0))
            // self.showAlert(title: "Alert", message: "Please enter valid date.")
             txtDateOfBirth.text = ""
         }else if currentDate > selectedDate{
             self.lblValidateDob.isHidden = true
             self.txtDateOfBirth.setLineColor(color: .white)
         }
     }
     
     @objc func dateResignResponder()
     {
        self.isDateMatch()
     }
    
    func callToActiveCreateBtnResponseCompletion(_ status: Bool) {
        if uncheckedBox {
            self.btnCheckBox.setImage(UIImage(named: "checkBoxSelected"), for: .normal)
            uncheckedBox = false
        }
        else {
            self.btnCheckBox.setImage(UIImage(named: "checkBoxEmpty"), for: .normal)
            uncheckedBox = true
        }

        if ((txtFullName.text?.isEmpty)! || (txtEmail.text?.isEmpty)! || (txtPassword.text?.isEmpty)! ||  self.uncheckedBox) || (!self.lblValidatePw.isHidden)
        {
            disableBtnAnimated(btnCreateUser)
        }
        else
        {
            enableBtnAnimated(btnCreateUser)

        }
         print("uncheckedBox\(uncheckedBox)")
    }
       
}

extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
