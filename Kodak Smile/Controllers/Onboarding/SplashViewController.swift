//
//  SplashViewController.swift
//  Polaroid MINT
//
//  Created by MacMini002 on 7/10/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import CopilotAPIAccess
import CopilotLogger
import AlamofireImage
import Alamofire

var SMLEventData = [String: Any]()

class SplashViewController: UIViewController {
    
    private var rechabilityObserver: ReachabilityHandler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // Redirect login user to home screen

        if UserDefaults.standard.bool(forKey:"userLogin"){
            self.moveToLegalPage()
        }else{
            self.checkInternet()
        }
        
        if WebserviceModelClass().isInternetAvailable(){
          self.checkForNewEvent()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Private Functions
    
    private func checkForAppUpgrade() {
        if WebserviceModelClass().isInternetAvailable(){
//            self.checkForNewEvent()
            AppManager.shared.appUpgradeManager.checkIfUpgradeIsAvailable(with: self)
        }
        else {
            self.checkInternet()
        }
    }
    
    func moveToInitialController () {
        //Move to Welcome Screen.
        let welcomeVC = storyboards.onboardingStoryboard.instantiateInitialViewController()
        self.navigationController?.present(welcomeVC!, animated: false, completion: nil)
        rechabilityObserver = ReachabilityHandler()
    }
    
    func checkInternet() {
        // set observer for UIApplication.willEnterForegroundNotification
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        if WebserviceModelClass().isInternetAvailable() {
            checkForAppUpgrade()
        }
        else {
            // Check For Offline Flow.
            
            if UserDefaults.standard.bool(forKey: "isJoinKodak"){
                self.moveToInitialController()
                return
            }
            if UserDefaults.standard.bool(forKey: "switchStateKodak") || UserDefaults.standard.bool(forKey: "iWillDoItLater") {
                if UserDefaults.standard.bool(forKey: "switchStateKodak") {
                    self.goToHomeController()
                } else {
                    DispatchQueue.main.async {
                        self.moveToLegalPage()
                    }
                }
                rechabilityObserver = ReachabilityHandler()
            } else {
                self.moveToInitialController()
            }
        }
    }
    
    func checkForNewEvent(){
        // Call Api for for event message
        // After success or fail check for app upgrade and move to next controller
        WebserviceModelClass().getDataForomUrl(module: "EventMessage", url: EventsUrl.message) { (success, message, data) in
            if success{
                guard let eventData = data["event_message"] as? [String: Any] else{
                    UserDefaults.standard.set(UserDefaults.standard.value(forKey: "eventId") as? Int ?? 0, forKey: "eventId")
//                    AppManager.shared.appUpgradeManager.checkIfUpgradeIsAvailable(with: self)
                    return
                }
                //                if UserDefaults.standard.value(forKey: "eventId") as? Int != eventData["id"] as? Int{
                SMLEventData["message"] = eventData["message"] as! String
                SMLEventData["id"] = eventData["id"] as! Int
                let imgView = UIImageView()
                imgView.af_setImage(
                    withURL: URL(string:eventData["thumbnail"] as! String)!,
                    imageTransition: .crossDissolve(0.2),
                    completion: { (img) in
                        SMLEventData["image"] = img.value ?? UIImage.init()
//                        AppManager.shared.appUpgradeManager.checkIfUpgradeIsAvailable(with: self)
                })
            }else{
//                AppManager.shared.appUpgradeManager.checkIfUpgradeIsAvailable(with: self)
            }
            
        }
    }
    
    // my selector that was defined above
    @objc func willEnterForeground() {
        self.checkInternet()
    }
    
    private func attemptSilentLogin() {
        if UserDefaults.standard.bool(forKey: "isJoinKodak"){
            self.moveToInitialController()
            return
        }
        if UserDefaults.standard.bool(forKey: "switchStateKodak") || Copilot.instance.manage.copilotConnect.defaultAuthProvider.canLoginSilently {
            
            self.performSilentLogin()
            
        } else if UserDefaults.standard.bool(forKey: "iWillDoItLater"){
            if Copilot.instance.manage.copilotConnect.defaultAuthProvider.canLoginSilently{
                
                self.performSilentLogin()
            }
            else{
                performAnonymousRegister { [weak self] in
                    DispatchQueue.main.async {
                        self?.moveToLegalPage()
                    }
                }
            }
        }else{
            self.moveToInitialController()
        }

    }
    
    func performSilentLogin() {
        
        AppManager.shared.userManager.attemptToSilentLogin {[weak self] (response) in
            
            switch response {
            case .success :
                self?.rechabilityObserver = ReachabilityHandler()
                if UserDefaults.standard.bool(forKey: "switchStateKodak") {
                    self?.goToHomeController()
                } else {
                    DispatchQueue.main.async {
                        self?.moveToLegalPage()
                    }
                }
                break
            case .failure(error: let error):
                switch error {
                case .server:
                    self?.rechabilityObserver = ReachabilityHandler()
                    if UserDefaults.standard.bool(forKey: "switchStateKodak") {
                        self?.goToHomeController()
                    } else {
                        DispatchQueue.main.async {
                            self?.moveToLegalPage()
                        }
                    }
                case .communication:
                    self?.showSilentLoginErrorMessage(error.message)
                case .silentlogin:
                    self?.showSessionExpiredMessage()
                default:
                    ZLogManagerWrapper.sharedInstance.logError(message: "Got a server error")
                }
            }
        }
    }
    
    func showSessionExpiredMessage() {
        
        UserDefaults.standard.set(true, forKey: "isJoinKodak")
        UserDefaults.standard.set(false, forKey: "switchStateKodak")
        UserDefaults.standard.set(false, forKey: "iWillDoItLater")
        SettingsViewController().removeUserInformation()
        
        let alert = UIAlertController(title: "Dear user,".localisedString(), message: "Your last login session has expired. Please login or register to app to continue using it.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK".localisedString(), style: .destructive, handler: { action in
            
            self.moveToInitialController()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSilentLoginErrorMessage(_ message:String) {
        
        let alert = UIAlertController(title: "Dear user,".localisedString(), message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Try Again".localisedString(), style: .destructive, handler: { action in
            self.performSilentLogin()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localisedString(), style: .default, handler: { action in
            exit(0)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showUpdateAlert(message:String, btnTitle:String = "YES" ){
        
        // create the alert
        let alert = UIAlertController(title: "New version available".localisedString(), message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: btnTitle.localisedString(), style: .destructive, handler: { action in
            AppManager.shared.appUpgradeManager.visitStore()
        }))
        
        self.present(alert, animated: true, completion: nil)
        
        if btnTitle == "OK" { return }
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "NO".localisedString(), style: .cancel , handler: { action in
            self.attemptSilentLogin()
        }))
    }
    
    func performAnonymousRegister(successClosure: @escaping ()-> Void) {
        
        customLoader.showActivityIndicator(viewController: self.view)
        AppManager.shared.userManager.registerAnonymously { [weak self] (response) in
            customLoader.hideActivityIndicator()
            switch response {
            case .failure(error: let error):
                ZLogManagerWrapper.sharedInstance.logError(message: "failed to register anonymously with error: \(error)")
                
                self?.presentAlertControllerWithPopupRepresentable(error, cancelButtonText: "OK".localisedString())
            case .success(_):
                ZLogManagerWrapper.sharedInstance.logInfo(message: "success in anonymous register")
                successClosure()
            }
        }
    }
}

extension SplashViewController : AppUpgradeManagerDelegate {
    
    func fetchUpgradeStatusFailed(error: KodakError) {
        presentAlertControllerWithPopupRepresentable(error, cancelButtonText: "Try Again".localisedString(), cancelCompletionHandler: { [weak self] in
            self?.checkForAppUpgrade()
        })
    }
    
    func upgradeRequired(upgradeType: ApplicationUpgradeType) {
        
        switch upgradeType {
            
        case .required:
            print("upgrade required")
            self.showUpdateAlert(message: "There is a required app update available. Please update the app to continue using it.".localisedString(), btnTitle: "OK")
            
        case .optional:
            print("upgrade available, but not required")
            self.showUpdateAlert(message:"There is an app update available. Would you like to update?".localisedString())
            
        case .notRequired:
            print("upgrade not required")
            attemptSilentLogin()
        }
    }
}
