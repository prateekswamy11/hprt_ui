//
//  WelcomeViewController.swift
//  Polaroid MINT
//
//  Created by MAXIMESS142 on 28/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import GoogleSignIn
import GTMSessionFetcher
import GoogleAPIClientForREST
import Photos
import CopilotAPIAccess

var isFacebookLogin = false

class WelcomeViewController: UIViewController {
    
    //MARK: - IBOutlets -
    @IBOutlet weak var viewBottomWelcomeVC: UIView!
    @IBOutlet weak var lblSignUpWithEmail: UILabel!
    @IBOutlet weak var lblSignUpWithGoogle: UILabel!
    @IBOutlet weak var lblSignUpWithFacebook: UILabel!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var lblAlreadyHaveAccount: UILabel!
    @IBOutlet weak var btnSignupEmail: UIButton!
    @IBOutlet weak var btnIllDoItLater: UIButton!
    @IBOutlet weak var btnSignupFB: UIButton!
    @IBOutlet weak var btnsignupGoogle: UIButton!
    
    //MARK: - Variables -
    let saveSignInData:UserDefaults = UserDefaults.standard
    
    //MARK: - View LifeCycle functions -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNotificationObserver()
        self.configureGooglePhotos()
        self.localizedStrings()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //        self.checkForNewEvent()
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.welcome.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    //MARK: - IBActions -
    @IBAction func goToSignUpScreenBtnClicked(_ sender: UIButton) {
        self.checkForNewEvent()
        AppManager.shared.userManager.getPasswordRulesPolicy { (response) in
            isFacebookLogin = false
            let signUpVC = storyboards.onboardingStoryboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }
    }
    
    @IBAction func goToLoginScreenBtnClicked(_ sender: UIButton) {
        isFacebookLogin = false
        self.checkForNewEvent()
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func continueWithFacebookBtnClicked(_ sender: UIButton) {
        
        if WebserviceModelClass().isInternetAvailable() {
            FacebookHelper().loginToFacebook(viewController: self)
            //            UserDefaults.standard.set("facebookLogin", forKey: "emailKodak")
            isFacebookLogin = true
        }
        else {
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            comeFromStrNoInternetSocialMedia = "facebookLoginNoInternet"
            self.present(alertPopUp, animated: true, completion: nil)
        }
    }
    
    @IBAction func continueWithGoogleBtnClicked(_ sender: UIButton) {
        isFacebookLogin = false
        self.checkForNewEvent()
        if WebserviceModelClass().isInternetAvailable() {
            GIDSignIn.sharedInstance().presentingViewController = self
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance()?.signIn()
            //            UserDefaults.standard.set("googleLogin", forKey: "emailKodak")
        } else {
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            comeFromStrNoInternetSocialMedia = "googleLoginNoInternet"
            self.present(alertPopUp, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnIWillDoItLaterClicked(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "isJoinKodak")
        //If user already login using i'll do it later then direct go to home.
        if UserDefaults.standard.bool(forKey: "iWillDoItLater") {
            if WebserviceModelClass().isInternetAvailable() {
                AppManager.shared.userManager.attemptToSilentLogin {[weak self] (response) in
                    DispatchQueue.main.async {
                        self?.moveToLegalPage()
                    }
                }
            } else {
                moveToLegalPage()
            }
            return
        }
        if WebserviceModelClass().isInternetAvailable() {
            performAnonymousRegister { [weak self] in
                DispatchQueue.main.async {
                    self?.moveToLegalPage()
                }
            }
        } else {
            self.moveToLegalPage()
        }
        UserDefaults.standard.set(true, forKey: "iWillDoItLater")
    }
    
    // MARK: - Logical Methods
    func localizedStrings() {
        self.lblSignUpWithEmail.text = "Sign Up With EMAIL".localisedString()
        self.lblSignUpWithGoogle.text = "Sign Up With GOOGLE".localisedString()
        self.lblSignUpWithFacebook.text = "Sign Up With FACEBOOK".localisedString()
        self.lblLogin.text = "Log In".localisedString()
        self.lblAlreadyHaveAccount.text = "Already have an account?".localisedString()
        self.btnIllDoItLater.setTitle("I'll do it later".localisedString(), for: .normal)
        //        self.btnsignupGoogle.setTitle("Sign Up With GOOGLE", for: .normal)
        //        self.btnSignupFB.setTitle("Sign Up With FACEBOOK", for: .normal)
        //        self.btnSignupEmail.setTitle("Sign Up With EMAIL", for: .normal)
    }
    
    func checkForNewEvent(){
        WebserviceModelClass().getDataForomUrl(module: "EventMessage", url: EventsUrl.message) { (success, message, data) in
            if success{
                guard let eventData = data["event_message"] as? [String: Any] else{
                    UserDefaults.standard.set(UserDefaults.standard.value(forKey: "eventId") as? Int ?? 0, forKey: "eventId")
                    return
                }
                //                if UserDefaults.standard.value(forKey: "eventId") as? Int != eventData["id"] as? Int{
                SMLEventData["message"] = eventData["message"] as! String
                SMLEventData["id"] = eventData["id"] as! Int
                let imgView = UIImageView()
                imgView.af_setImage(
                    withURL: URL(string:eventData["thumbnail"] as! String)!,
                    imageTransition: .crossDissolve(0.2),
                    completion: { (img) in
                        SMLEventData["image"] = img.value!
                })
                //                }else{
                //                    AppManager.shared.appUpgradeManager.checkIfUpgradeIsAvailable(with: self)
                //                }
            }
        }
    }
    
    func addNotificationObserver()
    {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.facebookLogin(_:)),
            name: NSNotification.Name(rawValue: "fblogin"),
            object: nil)
    }
    
    @objc func facebookLogin(_ notification: NSNotification)
    {
        guard  let userInfo = notification.userInfo?["data"] as? [String:Any] else{
            return
        }
        if UserDefaults.standard.value(forKey: "isLoggedInToFacebookKodak") as? Bool == true
        {
            // Clearing all the google, Instagram information of previous user or clearing cache for same user.
            //            GoogleHelper().logoutOffGoogle()
            //            InstagramHelper().logoutOffInstagram()
            self.goToHomeVC(userInfo["email"] as! String ,"")
            self.checkSocialUserLogin(userInfo["email"] as! String, userInfo["first_name"] as! String)
        }
    }
    
    func goToHomeVC(_ email: String,_ password: String)
    {
        // Clearing all the Dropbox information of previous user or clearing cache for same user.
        //        DropboxHelper().logoutDropbox()
        UserDefaults.standard.set("login", forKey: "emailKodak")
        customLoader.hideActivityIndicator()
        goToHomeController()
        UserDefaults.standard.set(false, forKey: "iWillDoItLater")
        LoginViewController().setUserInformation(email: email, password: password)
    }
    
    func showAlert() {
        //        let alert = UIAlertController(title: "No Internet", message: "Seems like you don't have an active internet connection.", preferredStyle: .alert)
        //        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        //        self.present(alert, animated: true, completion: nil)
        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
        comeFromStrNoInternetSocialMedia = "NoInternet"
        self.present(alertPopUp, animated: true, completion: nil)
    }
    
    //MARK: - API Calling Functions -
    func checkSocialUserLogin(_ email: String, _ fullName: String)
    {
        let requestParameters = ["email" : email,"login_with": "social"] as [String : Any]
        if WebserviceModelClass().isInternetAvailable(){
            WebserviceModelClass().postDataFor(module: "WelcomeViewController", subUrl: SIGNUP_URL, parameter: requestParameters) { (isSuccess, responseMsg, arrayJSONObj) in
                DispatchQueue.main.async {
                    if isSuccess{
                        print(arrayJSONObj)
                        self.saveSignInData.set(fullName, forKey: "userNameLoggedIn")
                        self.saveSignInData.synchronize()
                        self.goToHomeVC(email, "")
                    }else{
                        customLoader.hideActivityIndicator()
                        self.showAlert(title: "Alert", message: responseMsg)
                    }
                }
            }
        }else{
            self.showAlert()
        }
    }
}
