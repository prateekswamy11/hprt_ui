//
//  WelcomeWithNoInternetViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 09/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class WelcomeWithNoInternetViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.welcome.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    //MARK: - IBActions -
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "No Internet", message: "Seems like you don't have an active internet connection.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            let mainScrnNavCntrllr = storyboards.mainStoryboard.instantiateInitialViewController()
            self.present(mainScrnNavCntrllr!, animated: true, completion: {
                UserDefaults.standard.set(true, forKey: "isFirstTime")
            })
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
