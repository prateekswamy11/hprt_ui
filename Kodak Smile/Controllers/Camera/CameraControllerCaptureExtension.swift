//
//  CameraControllerCaptureExtension.swift
//  Kodak Smile
//
//  Created by maximess152 on 29/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation

extension CameraViewController : AVCapturePhotoCaptureDelegate, DismissARAlertScan
{
    func makeChanges() {
//        if #available(iOS 11.311.3, *){
//            if !Thread.current.isMainThread{
//            DispatchQueue.main.async {
//                if self.captureSessionLive.isRunning
//                {
//                    self.captureSessionLive.stopRunning()
//                }
//                let ScanVC = storyboards.arStoryboard.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
//                self.navigationController?.pushViewController(ScanVC, animated: false)
//            }
//            }else{
//                if self.captureSessionLive.isRunning
//                {
//                    self.captureSessionLive.stopRunning()
//                }
//                let ScanVC = storyboards.arStoryboard.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
//                self.navigationController?.pushViewController(ScanVC, animated: false)
//            }
//        }
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput,
                     didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?,
                     previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?,
                     resolvedSettings: AVCaptureResolvedPhotoSettings,
                     bracketSettings: AVCaptureBracketedStillImageSettings?,
                     error: Error?) {
        // get captured image
        // Make sure we get some photo sample buffer
        guard error == nil,
            let photoSampleBuffer = photoSampleBuffer else {
                print("Error capturing photo: \(String(describing: error))")
                return
        }
        // Convert photo same buffer to a jpeg image data by using // AVCapturePhotoOutput
        guard let imageData =
            AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer, previewPhotoSampleBuffer: previewPhotoSampleBuffer) else
        {
            return
        }
        //Stop Session to show captured image for a second
        captureSessionLive.stopRunning()
        // Initialise a UIImage with our image data
        let capturedImage = UIImage.init(data: imageData , scale: 1.0)
        if let image = capturedImage {
            
            // Captured image show in bottom gallery preview.
            self.btnCapturedImg.isHidden = false
            isPhotosAllowed = true
            UIGraphicsBeginImageContextWithOptions(self.viewBackgroundPaperSize.bounds.size, false, 0.0)
            self.viewBackgroundPaperSize.layer.render(in: UIGraphicsGetCurrentContext()!)
            let imagetest = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            if imagetest != nil{
                self.btnCapturedImg.setImage(imagetest, for: .normal)
            }
            UserDefaults.standard.set(true, forKey: "isShowThumbnail")
            forImagePreview = imagetest!
            //Save captured image in photo album
            UIImageWriteToSavedPhotosAlbum(imagetest!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            // Assuming cameraView contains your previewLayer...
            viewCameraPreview.alpha = 0
            // Animate the "flash"
            UIView.animate(withDuration: 0.1, delay: 0, options: .transitionCrossDissolve, animations: { () -> Void in
                self.viewCameraPreview.alpha = 1
                if !Thread.current.isMainThread{
                DispatchQueue.main.async {
                self.captureSessionLive.startRunning()
                }
                }else{
                self.captureSessionLive.startRunning()
                }
                //                self.updateImagePreview()
            }, completion: nil)
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save Error".localisedString(), message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK".localisedString(), style: .default))
            present(ac, animated: true)
        } else {
            let name = image.accessibilityIdentifier;
            
            let ac = UIAlertController(title: "Saved!".localisedString(), message: "Your altered image has been saved to your photos.".localisedString(), preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            //present(ac, animated: true)
        }
    }
    
    func cameraWithPosition(position: AVCaptureDevice.Position) -> AVCaptureDevice?
    {
        let devices = AVCaptureDevice.devices(for: AVMediaType.video)
        for device in devices {
            let device = device
            if device.position == position {
                return device
            }
        }
        return nil
    }
    
    func captureWithFrontFlash(capture: AVCapturePhotoOutput, setting: AVCapturePhotoSettings){
        capture.capturePhoto(with: setting, delegate: self)
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        connection.videoOrientation = .portrait
        let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue.main)
        let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        let cameraImage = CIImage(cvImageBuffer: pixelBuffer!)
        if currentFilterIndex != 0
        {
            // If filter is selected, apply it to camera input images
            let filterEffect = arrayFilters[currentFilterIndex]
            filterEffect.setValue(cameraImage, forKey: kCIInputImageKey)
            let cgImage = self.context.createCGImage(filterEffect.outputImage!, from: cameraImage.extent)!
            if !Thread.current.isMainThread{
                DispatchQueue.main.async {
                    let filteredImage = UIImage(cgImage: cgImage)
                    self.flipImageForFrontCamera(filteredImage: filteredImage)
                }
            }else{
                let filteredImage = UIImage(cgImage: cgImage)
                self.flipImageForFrontCamera(filteredImage: filteredImage)
            }
        }
        else
        {
            let cgImage = self.context.createCGImage(cameraImage, from: cameraImage.extent) ?? UIImage.init() as! CGImage
            if !Thread.current.isMainThread{
                DispatchQueue.main.async {
                    let filteredImage = UIImage(cgImage: cgImage)
                    self.flipImageForFrontCamera(filteredImage: filteredImage)
                }
            }else{
                let filteredImage = UIImage(cgImage: cgImage)
                self.flipImageForFrontCamera(filteredImage: filteredImage)
            }
        }
    }
}
