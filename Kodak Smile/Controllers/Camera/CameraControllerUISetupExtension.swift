//
//  CameraControllerUISetupExtension.swift
//  Kodak Smile
//
//  Created by maximess152 on 29/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation
import Photos

extension CameraViewController {
    @objc func removeScreenFlash() {
        self.screenFlashView.removeFromSuperview()
        UIScreen.main.brightness = screenBrightness
    }
    
    func flipImageForFrontCamera(filteredImage: UIImage){
        if !Thread.current.isMainThread{
            DispatchQueue.main.async {
                if self.currentCamera == self.frontCamera{
                    self.imageViewBackground.image = filteredImage.withHorizontallyFlippedOrientation()
                }else{
                    self.imageViewBackground.image = filteredImage
                }
            }
        }else{
            if self.currentCamera == self.frontCamera{
                self.imageViewBackground.image = filteredImage.withHorizontallyFlippedOrientation()
            }else{
                self.imageViewBackground.image = filteredImage
            }
        }
    }
    
    func setupFiltersArray()
    {
        // for applying filters
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        arrayFilters.append(CIFilter(name: "CIFalseColor")!) // This is dummy filter, not in use. Added to increase index of the array as `original camera input`
        arrayFilters.append(CIFilter(name: "CIPhotoEffectProcess")!)
        arrayFilters.append(CIFilter(name: "CIPhotoEffectInstant")!)
        arrayFilters.append(CIFilter(name: "CIFalseColor")!)
        arrayFilters.append(CIFilter(name: "CIColorMonochrome")!)
        arrayFilters.append(CIFilter(name: "CIPhotoEffectMono")!)
        arrayFilters.append(CIFilter(name: "CIPhotoEffectTransfer")!)
        arrayFilters.append(CIFilter(name: "CIPhotoEffectNoir")!)
        arrayFilters.append(CIFilter(name: "CIPhotoEffectTonal")!)
        arrayFilters.append(CIFilter(name: "CISepiaTone")!)
        // Now unhide the pagecontrol as all filters are loaded
        pageControlFilters.isHidden = false
        pageControlFilters.backgroundColor = UIColor.clear
        pageControlFilters.numberOfPages = arrayFilters.count
    }
    
    func checkPhotoLibraryPermission() -> Bool {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            print("authorized")
            return true
        case .denied, .restricted :
            //handle denied status
            print("denied")
            return false
        case .notDetermined:
            return false
        }
    }
    
    func localizationStrings(){
        self.lblScan.text = "Scan".localisedString()
        self.btnCross.setTitle("Close".localisedString(), for: .normal)
    }
    
    func updateImagePreview() {
        if UserDefaults.standard.bool(forKey: "isShowThumbnail"){
            self.showPreviewBtn()
            self.fetchCapturedImageFromGallery()
        }
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if checkOverlayIsPresent == false{
            if gesture.direction == UISwipeGestureRecognizerDirection.right {
                if currentFilterIndex > 0
                {
                    currentFilterIndex = currentFilterIndex - 1
                }
            }
            if gesture.direction == UISwipeGestureRecognizerDirection.left {
                if currentFilterIndex < arrayFilters.count - 1
                {
                    currentFilterIndex = currentFilterIndex + 1
                }
            }
            pageControlFilters.currentPage = currentFilterIndex
        }
    }
    
    func checkAndRequestCameraPermission()
    {
        self.BtnCaptureCamera.isEnabled = false
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus == AVAuthorizationStatus.denied {
            // Denied access to camera
            // Explain that we need camera access and how to change it.
            //            let dialog = UIAlertController(title: "Unable To Access The Camera".localisedString(), message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
            //
            //            let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
            //
            //            dialog.addAction(okAction)
            //            self.present(dialog, animated:true, completion:nil)
            
        } else if authStatus == AVAuthorizationStatus.notDetermined {
            // The user has not yet been presented with the option to grant access to the camera hardware.
            // Ask for it.
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (grantd) in
                // If access was denied, we do not set the setup error message since access was just denied.
                if grantd && !self.captureSessionLive.isRunning{
                    // Allowed access to camera, go ahead and present the UIImagePickerController.
                    self.setupInputOutput()
                }
            })
        } else {
            //Adding loader deliberately for safe loading of camera
            //            customLoader.showActivityIndicator(viewController: self.view)
            let deadlineTime = DispatchTime.now() + .milliseconds(5) // .seconds(1)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
                // Allowed access to camera, go ahead and present the UIImagePickerController.
                self.setupDevice()
                self.setupCaptureFrame()
                self.setupInputOutput()
                let childOverlayViewVC = storyboards.imgPreviewStoryboard.instantiateViewController(withIdentifier: "OverlayViewController") as? OverlayViewController
                print("childOverlayViewVC!.isFrom\(childOverlayViewVC!.isFrom)")
                // if childOverlayViewVC!.isFrom == ""
                if self.arrayFilters.count == 0
                {
                    self.setupFiltersArray()
                }
            })
        }
    }
}
