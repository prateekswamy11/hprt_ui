//
//  CameraModel.swift
//  Kodak Smile
//
//  Created by MacMini001 on 15/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import AVFoundation

class CameraModel: NSObject {
    var flashMode = 0 // 0 - off, 1 - on
    var cameraInputMode = 0 // 0 for back camera & 1 for Front camera
    var hdrMode = 0 // 0 - HDR Off , 1 - HDR On
    
    func getFlashMode() -> AVCaptureDevice.FlashMode
    {
        print("capturing photo with flashmode - ", flashMode)
        switch flashMode {
        case 0:
            return AVCaptureDevice.FlashMode.off
        case 1:
            return AVCaptureDevice.FlashMode.on
            
        default:
            return AVCaptureDevice.FlashMode.off
        }
    }
    
    func getHDRMode() -> AVCaptureSession.Preset
    {
        if hdrMode == 1
        {
            return AVCaptureSession.Preset.photo //resolution like the integrated camera app from apple.
        }
        else
        {
            return AVCaptureSession.Preset.high //Second highest resolution after Photo.
        }
    }
}
