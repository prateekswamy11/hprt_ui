//
//  CameraViewController.swift
//  Snaptouch_Polaroid
//
//  Created by maximess142 on 13/11/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import AssetsLibrary
import ARKit
import CopilotAPIAccess

var isPhotosAllowed = false

class CameraViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate
{
    //MARK:- Outlets
    @IBOutlet weak var imageViewBackground: UIImageView!
    @IBOutlet weak var viewBackgroundPaperSize: UIView!
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var btnCameraFlip: UIButton!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var pageControlFilters: UIPageControl!
    @IBOutlet weak var lblScan: UILabel!
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var BtnCaptureCamera: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewRight: UIView!
    @IBOutlet weak var viewLeft: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewPaperSize: UIView!
    @IBOutlet weak var viewCameraPreview: UIView!
    @IBOutlet weak var btnCapturedImg: UIButton!
    @IBOutlet weak var constraintWidthViewPaperSize: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightViewPaperSize: NSLayoutConstraint!
    @IBOutlet weak var constraintWidthViewBackgroundPaperSize: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightViewBackgroundPaperSize: NSLayoutConstraint!
    
    //MARK:- Variables
    var capturePhotoOutput: AVCapturePhotoOutput? //to snap a photo from capture session.
    
    /*
     var flashMode = 0 // 0 - off, 1 - on
     var cameraInputMode = 0 // 0 for back camera & 1 for Front camera
     var hdrMode = 0 // 0 - HDR Off , 1 - HDR On
     */
    var isIntialCameraLaunch = true
    var screenFlashView = UIView()
    var cameraModel = CameraModel()
    // MARK: - Live camera filter vars
    var captureSessionLive = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    let context = CIContext()
    let checkCameraPermission = false
    var arrayFilters = [CIFilter]()
    var currentFilterIndex = Int()
    var comeFromStr = String()
    let screenBrightness = UIScreen.main.brightness
    var previewImage = UIImage()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.isUserInteractionEnabled = true
        self.pageControlFilters.backgroundColor = UIColor.clear
        self.pageControlFilters.pageIndicatorTintColor = UIColor.init(white: 255.0/255.0, alpha: 0.5)
        self.pageControlFilters.currentPageIndicatorTintColor = UIColor.init(white: 255.0/255.0, alpha: 1.0)
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        if let hasFlash = device?.hasFlash
        {
            if hasFlash == false
            {
                self.btnFlash.isEnabled = false
            }
        }
        self.checkAndRequestCameraPermission()
        isScanningAR = true
        self.localizationStrings()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.setInitialConstraintToPaperSizeView()
        self.updateImagePreview()
        let volume = AVAudioSession.sharedInstance().outputVolume
        if !captureSessionLive.isRunning && !isIntialCameraLaunch
        {
            captureSessionLive.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        print("Calling view will Disappear")
        isScanningAR = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.camera.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if self.captureSessionLive.isRunning
        {
            self.captureSessionLive.stopRunning()
        }
        customLoader.hideActivityIndicator()
    }
    
    override func viewDidLayoutSubviews() {
        //orientation = AVCaptureVideoOrientation(rawValue: UIApplication.shared.statusBarOrientation.rawValue)!
    }
    
    //Hide Status bar
    override var prefersStatusBarHidden: Bool {
        return status_Bar_Hidden
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("received memory warning in Camera View")
    }
    
    // MARK: - IBActions
    @IBAction func arBtnClicked(_ sender: Any) {
        //        check AR compabitity
//        if (ARConfiguration.isSupported)  {
//            if #available(iOS 11.311.3, *) {
//                if WebserviceModelClass().isInternetAvailable() {
//                    //                    DispatchQueue.main.async {
//                    if self.captureSessionLive.isRunning
//                    {
//                        self.captureSessionLive.stopRunning()
//                    }
//                    let ScanVC = storyboards.arStoryboard.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
//                    self.navigationController?.pushViewController(ScanVC, animated: false)
//                    //                    }
//                }else {
//                    customLoader.hideActivityIndicator()
//                    let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
//                    comeFromStrNoInternetSocialMedia = "NormalNointernet"
//                    isComeFrom = "Rectangle"
//                    alertPopUp.view.frame = self.view.bounds
//                    self.addChildViewController(alertPopUp)
//                    self.view.addSubview(alertPopUp.view)
//                    alertPopUp.didMove(toParentViewController: self)
//                    return
//                }
//            }
//        }else{
//            
//            let alert = UIAlertController(title: "Alert", message: "Your Device does not support AR", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: false, completion: nil)
//        }
    }
    
    @IBAction func backBtnClicked(_ sender: Any)
    {
        var isHomeVCInStack = false
        if let navVC = self.navigationController {
            // Pop view controller to home.
            for item in navVC.viewControllers {
                // Filter for your desired view controller:
                if item.isKind(of: HomeViewController.self) {
                    isHomeVCInStack = true
                    self.navigationController?.popToViewController(item, animated: false)
                }
            }
        }
        // When directly came from onboarding, home VC is not in stack so navigate by pushing to it
        if isHomeVCInStack == false {
            let homeVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
    }
    
    @IBAction func goToSettingsBtnClicked(_ sender: UIButton)
    {
        var isSettingsVCInStack = false
        if let navVC = self.navigationController
        {
            // Pop view controller to home.
            for item in navVC.viewControllers {
                // Filter for your desired view controller:
                if item.isKind(of: SettingsViewController.self) {
                    isSettingsVCInStack = true
                    self.navigationController?.popToViewController(item, animated: false)
                }
            }
        }
        // When directly came from onboarding, home VC is not in stack so navigate by pushing to it
        if isSettingsVCInStack == false
        {
            let settingsVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            self.navigationController?.pushViewController(settingsVC, animated: false)
        }
    }
    
    @IBAction func showCapturedImgPreviewBtnClicked(_ sender: UIButton) {
        let opertaionQueue = OperationQueue()
        opertaionQueue.maxConcurrentOperationCount = 1
        let operationForImgPreview = BlockOperation {
            autoreleasepool{
                let takePhotoAnalyticsEvent = TakePhotoAnalyticsEvent()
                Copilot.instance.report.log(event: takePhotoAnalyticsEvent)
                self.getStickers { (success) in
                    let imagePreviewVC = storyboards.imgPreviewStoryboard.instantiateViewController(withIdentifier: "ImagePreviewViewController") as! ImagePreviewViewController
                    imgMainEdit = forImagePreview
                    isComeFrom = "cameraVC"
                    customLoader.hideActivityIndicator()
                    self.navigationController?.pushViewController(imagePreviewVC, animated: false)
                }
            }
            print("stickers fetched")
            //            opertaionQueue.cancelAllOperations()
        }
        opertaionQueue.addOperation(operationForImgPreview)
    }
    
    @IBAction func captureBtnClicked(_ sender: Any)
    {
        if self.checkPhotoLibraryPermission() {
            // Make sure capturePhotoOutput is valid
            guard let capturePhotoOutput = self.capturePhotoOutput else { return }
            // Get an instance of AVCapturePhotoSettings class
            let photoSettings = AVCapturePhotoSettings()
            // Set photo settings for our need
            photoSettings.isAutoStillImageStabilizationEnabled = true
            photoSettings.isHighResolutionPhotoEnabled = true
            // For flash settings , choose only back camera
            if cameraModel.cameraInputMode == 0
            {
                photoSettings.flashMode = cameraModel.getFlashMode()
            }
            // Call capturePhoto method by passing our photo settings and a
            // delegate implementing AVCapturePhotoCaptureDelegate
            capturePhotoOutput.capturePhoto(with: photoSettings, delegate: self)
        }else {
            if  UserDefaults.standard.bool(forKey: "EnablePhotoAccess"){
                let alert = UIAlertController(title: "Permission required", message: "You must allow the access to gallery to take pictures.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    print("ok clicked on camera permission pop up.")
                    //                    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
                    //                    UIApplication.shared.open(settingsUrl)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else if checkPhotoLibraryPermission() == false
            {
                PHPhotoLibrary.requestAuthorization { (status) in
                    if status == .authorized
                    {
                        //                        self.getPhotoAssetsAlbumWise()
                    }
                    
                }
                UserDefaults.standard.set(true, forKey: "EnablePhotoAccess")
            }
        }
    }
    
    @IBAction func flashButnClicked(_ sender: Any)
    {
        switch cameraModel.flashMode
        {
        case 0:
            cameraModel.flashMode = 1
            btnFlash.isSelected = true
            print("flash on")
        case 1:
            cameraModel.flashMode = 0
            btnFlash.isSelected = false
            print("flash off")
        default:
            cameraModel.flashMode = 0
        }
    }
    
    @IBAction func flipCameraBtnClicked(_ sender: Any)
    {
        // Disabling as this will reduce memory issue when clicked multiple times in a row
        // Disable user interaction until the process is completed
        self.view.isUserInteractionEnabled = false
        captureSessionLive.stopRunning()
        //Indicate that some changes will be made to the session
        captureSessionLive.beginConfiguration()
        if let inputs = captureSessionLive.inputs as? [AVCaptureDeviceInput] {
            for input in inputs {
                captureSessionLive.removeInput(input)
            }
        }
        if (cameraModel.cameraInputMode == 0) {
            currentCamera = cameraWithPosition(position: .front)
            cameraModel.cameraInputMode = 1
            // Make flash off
            cameraModel.flashMode = 0
            btnFlash.isSelected = false
            // Disable the flash button when front camera is ON
            //            btnFlash.isEnabled = false
        } else {
            currentCamera = cameraWithPosition(position: .back)
            cameraModel.cameraInputMode = 0
            // Enable the flash button when back camera is ON
            btnFlash.isEnabled = true
        }
        do
        {
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
            if captureSessionLive.canAddInput(captureDeviceInput) {
                captureSessionLive.addInput(captureDeviceInput)
            }
        }
        catch
        {
            print("MMMK error", error)
        }
        captureSessionLive.commitConfiguration()
        UIView.transition(with: imageViewBackground, duration: 1.0, options: .transitionFlipFromRight, animations: {
            if !Thread.current.isMainThread{
                DispatchQueue.main.async {
                    self.captureSessionLive.startRunning()
                }
            }else{
                self.captureSessionLive.startRunning()
            }
        }, completion:
            {
                Void in
                // Enable user interaction when process is completed
                self.view.isUserInteractionEnabled = true
        })
        print("done flipping")
    }
    
    @IBAction func hdrBtnClicked(_ sender: Any)
    {
        //        // Previously was On
        //        if hdrMode == 1
        //        {
        //            hdrMode = 0
        //        }
        //        else
        //        {
        //            hdrMode = 1
        //        }
        //
        //        captureSession?.sessionPreset = getHDRMode()
    }
    
    //MARK:- Functions
    func fetchCapturedImageFromGallery()
    {
        self.btnCapturedImg.setImage(forImagePreview, for: .normal)
    }
    
    func showPreviewBtn(){
        if isPhotosAllowed{
            self.btnCapturedImg.isHidden = false
        }
    }
}
