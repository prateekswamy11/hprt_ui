//
//  CameraControllerSetupExtension.swift
//  Kodak Smile
//
//  Created by maximess152 on 29/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation

extension CameraViewController {
    
    func setupDevice() {
        if currentCamera == nil
        {
            let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
            let devices = deviceDiscoverySession.devices
            for device in devices {
                if device.position == AVCaptureDevice.Position.back {
                    backCamera = device
                }
                else if device.position == AVCaptureDevice.Position.front {
                    frontCamera = device
                }
            }
            print("MMM setup device finished")
            currentCamera = backCamera
        }
    }
    
    func setupInputOutput() {
        if currentCamera != nil {
            do {
                guard let currentCamera = currentCamera else { return }
                
                setupCorrectFramerate(currentCamera: currentCamera)
                let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera)
                if currentCamera.supportsSessionPreset(AVCaptureSession.Preset.high) {
                    captureSessionLive.sessionPreset = AVCaptureSession.Preset.high
                } else {
                    captureSessionLive.sessionPreset = AVCaptureSession.Preset.medium
                }
                captureSessionLive.automaticallyConfiguresCaptureDeviceForWideColor = false
                
                if !captureSessionLive.inputs.contains(captureDeviceInput)
                {
                    if captureSessionLive.canAddInput(captureDeviceInput)
                    {
                        captureSessionLive.addInput(captureDeviceInput)
                    }
                    
                }
                self.isIntialCameraLaunch = false
                let videoOutput = AVCaptureVideoDataOutput()
                
                videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "sample buffer delegate", attributes: []))
                
                
                
                if !captureSessionLive.outputs.contains(videoOutput)
                {
                    if captureSessionLive.canAddOutput(videoOutput) {
                        captureSessionLive.addOutput(videoOutput)
                        
                    }
                    
                }
                captureSessionLive.startRunning()
                
                let deadlineTime = DispatchTime.now() + .milliseconds(10) // .seconds(1)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
                    // When view is loaded, stop the loader and enable capture button
                    customLoader.hideActivityIndicator()
                    self.BtnCaptureCamera.isEnabled = true
                    
                    if !UserDefaults.standard.bool(forKey: "overlayCameraPresent") {
                        self.prepareChildOverlayView()
                    }
                })
                print("MMM setup input output finished")
                
            } catch {
                print("MMM error", error)
            }
        }
    }
    
    func setupCorrectFramerate(currentCamera: AVCaptureDevice) {
        for vFormat in currentCamera.formats {
            //see available types
            //print("\(vFormat) \n")
            var ranges = vFormat.videoSupportedFrameRateRanges as [AVFrameRateRange]
            let frameRates = ranges[0]
            do {
                //set to 240fps - available types are: 30, 60, 120 and 240 and custom
                // lower framerates cause major stuttering
                if frameRates.maxFrameRate == 240 {
                    try currentCamera.lockForConfiguration()
                    currentCamera.activeFormat = vFormat as AVCaptureDevice.Format
                    //for custom framerate set min max activeVideoFrameDuration to whatever you like, e.g. 1 and 180
                    currentCamera.activeVideoMinFrameDuration = frameRates.minFrameDuration
                    currentCamera.activeVideoMaxFrameDuration = frameRates.maxFrameDuration
                }
            }
            catch {
                print("Could not set active format")
                print(error)
            }
        }
    }
    
    func stopCaptureSession () {
        self.captureSessionLive.stopRunning()
        if let inputs = captureSessionLive.inputs as? [AVCaptureDeviceInput] {
            for input in inputs {
                self.captureSessionLive.removeInput(input)
            }
        }
    }
    
    func setScreenFlash() {
        self.screenFlashView = UIView(frame: UIScreen.main.bounds)
        UIScreen.main.brightness = 1.0
        self.view.addSubview(self.screenFlashView)
        self.screenFlashView.backgroundColor = .white
        //        self.screenFlashView.alpha = 0.8
        self.perform(#selector(self.removeScreenFlash), with: nil, afterDelay: 1.0)
        
    }
    
    private func prepareChildOverlayView() {
        if let childOverlayViewVC = storyboards.imgPreviewStoryboard.instantiateViewController(withIdentifier: "OverlayViewController") as? OverlayViewController
        {
            childOverlayViewVC.isFrom = "Camera"
            UserDefaults.standard.set(true, forKey: "overlayCameraPresent")
            addChildViewController(childOverlayViewVC)
            self.view.addSubview(childOverlayViewVC.view)
        }
    }
    
    func setInitialConstraintToPaperSizeView()  {
        //Add blure effect
        self.viewTop.layer.backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0.4).cgColor
        self.viewLeft.layer.backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0.4).cgColor
        self.viewRight.layer.backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0.4).cgColor
        self.viewBottom.layer.backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0.4).cgColor
        self.viewCameraPreview.backgroundColor = .clear
    }
    
    func setupCaptureFrame()
    {
        if capturePhotoOutput == nil
        {
            // Get an instance of ACCapturePhotoOutput class
            capturePhotoOutput = AVCapturePhotoOutput()
            capturePhotoOutput?.isHighResolutionCaptureEnabled = true
            // Set the output on the capture session
            captureSessionLive.addOutput(capturePhotoOutput!)
        }
    }
}
