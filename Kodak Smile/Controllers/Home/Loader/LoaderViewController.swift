//
//  LoaderViewController.swift
//  Snaptouch_Polaroid
//
//  Created by MAXIMESS082 on 20/12/17.
//  Copyright © 2017 maximess142. All rights reserved.
//

import UIKit
import Photos
//import SwiftyGif

class LoaderViewController: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var imgViewGIF: UIImageView!
    
    //MARK: - Variables
    
    //MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.loadImages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //gifManager.clear()
        //self.imgViewGIF.image = nil
    }
    
    func loadImages()
    {
        DispatchQueue.global(qos: .utility).async
            {
                DispatchQueue.main.async {
                    let scrollViewVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(scrollViewVC, animated: false)
                }
        }
        
    }
}
