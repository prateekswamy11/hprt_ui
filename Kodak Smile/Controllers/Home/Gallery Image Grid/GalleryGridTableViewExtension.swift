//
//  GalleryGridTableViewExtension.swift
//  Kodak Smile
//
//  Created by maximess152 on 29/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation
import Photos

//MARK:- UITableViewDelegates
extension GalleryImageGridViewController: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.suggestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AutoSuggestionTableViewCell") as! AutoSuggestionTableViewCell
        cell.textLabel?.text = self.suggestions[indexPath.row]
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.font = .systemFont(ofSize: 12.0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! AutoSuggestionTableViewCell
        self.txtSearch.text = cell.textLabel?.text
        //        self.fetchClustering(keyword: (cell.textLabel?.text!)!)
        //        self.collectionViewGrid.reloadData()
        self.tblViewSuggestions.isHidden = true
        searchActive = true
        self.view.endEditing(true)
        //        self.multiTagArray.append((cell.textLabel?.text)!)
        tagsField.addTags([(cell.textLabel?.text)!])
        //        self.fetchClustering(keyword: self.multiTagArray)
        //        self.multiTagArray.append((cell.textLabel?.text)!)
        //        tagsField.addTag((cell.textLabel?.text)!)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func fetchClustering(keyword: [String]) {
        var array = [String]()
        if self.usersSelectedCell == nil && self.searchActive {
            array = DataBaseHelper().fetchClusteringFromDB(ofClass: "1111", search: keyword, tableName: "AllImagesTable", previousData: filteredArray)
            //            self.lblSearchedPhotosCount.text = "\(self.filteredArray.count) photos found"
        }
        else if self.combineSearch {
            let str: Int = self.usersSelectedCell!
            array = DataBaseHelper().fetchClusteringFromDB(ofClass: String(str), search: keyword, tableName: "ClusterTable", previousData: filteredArray)
            //            self.lblSearchedPhotosCount.text = "\(self.filteredArray.count) photos found"
        }
        else {
            self.searchActive = false
            self.lblSearchedPhotosCount.text = ""
        }
        self.filteredArray = array.unique
        if self.filteredArray.isEmpty {
            self.heightForLblSearchPhotosCount.constant = 50
            self.lblSearchedPhotosCount.numberOfLines = 2
            self.lblSearchedPhotosCount.textAlignment = .center
            let formattedString = NSMutableAttributedString()
            formattedString
                .bold("No Result")
                .normal("\nTry a new Search")
            //                .bold("Bold Text")
            self.lblSearchedPhotosCount.attributedText = formattedString
            //            let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "AvenirNext-Bold", size: 14)!]
            //            self.lblSearchedPhotosCount.attributedText = NSAttributedString(string: "No Result \n Try a new Search", attributes: attrs)
        }else {
            self.heightForLblSearchPhotosCount.constant = 20
            self.lblSearchedPhotosCount.text = "\(self.filteredArray.count) photos found"
        }
        self.collectionViewGrid.reloadData()
    }
    
    //MARK:- Multi Tag textfields delegates
    
    func textFieldEvents() {
        tagsField.onDidAddTag = { (tag,text) in
            //            print("DidAddTag")
            self.multiTagArray.append(text.text)
            if self.multiTagArray.isEmpty {
                self.btnDeleteSearchKeyword.isHidden = true
            }
            else {
                self.btnDeleteSearchKeyword.isHidden = false
            }
            if self.usersSelectedCell != nil {
                self.combineSearch = true
                self.searchActive = true
            }
            else {
                self.searchActive = true
            }
            self.fetchClustering(keyword: self.multiTagArray)
        }
        tagsField.onDidRemoveTag = { (tag,text) in
            //            print("DidRemoveTag")
            let indexOfString = self.multiTagArray.index(of: text.text)
            if let index = indexOfString {
                tag.removeTag(text)
                self.multiTagArray.remove(at: index)
                if !tag.tags.isEmpty {
                    self.fetchClustering(keyword: self.multiTagArray)
                }
                else {
                    //                    self.combineSearch = false
                    self.searchActive = false
                    self.lblSearchedPhotosCount.text = ""
                    self.collectionViewGrid.reloadData()
                    self.fetchClustering(keyword: self.multiTagArray)
                }
                
            }
            else {
                //unComment when AI implementd
                //                self.searchActive = false
                //                self.combineSearch = false
                //                self.fetchClustering(keyword: self.multiTagArray)
                //                self.txtSearch.text = ""
                //                self.tblViewSuggestions.reloadData()
                //                self.collectionViewGrid.reloadData()
            }
            if self.multiTagArray.isEmpty {
                self.btnDeleteSearchKeyword.isHidden = true
                self.lblSearchedPhotosCount.text = ""
            }
            else {
                self.btnDeleteSearchKeyword.isHidden = false
            }
            self.heightForLblSearchPhotosCount.constant = 20
        }
        tagsField.onDidChangeText = { tag, text in
            self.hideKeyboardWhenTappedAround()
            //            print("onDidChangeText")
            var tags = [String]()
            for data in self.tagsField.tags {
                tags.append(data.text)
            }
            if !(text!.isEmpty) {
                //                if text!.count == 1 {
                //                    self.tblViewSuggestions.isHidden = true
                //                }
                //                else {
                self.suggestions.removeAll()
                //                    if textArray.count == 0 {
                if self.usersSelectedCell == nil {
                    self.suggestions = DataBaseHelper().fetchSuggestions(ofClass: "1111", search: (text!).localizedCapitalized, tableName: "AllImagesTable", previousSearch: tags, assetIdArray: self.filteredArray)
                }
                else {
                    let str: Int = self.usersSelectedCell!
                    if self.combineSearch {
                        self.suggestions = DataBaseHelper().fetchSuggestions(ofClass: String(str), search: (text!).localizedCapitalized, tableName: "ClusterTable", previousSearch: tags, assetIdArray: self.filteredArray)
                    }else {
                        self.suggestions = DataBaseHelper().fetchSuggestions(ofClass: String(str), search: (text!).localizedCapitalized, tableName: "ClusterTable", previousSearch: tags, assetIdArray: self.faceData)
                    }
                }
                if self.suggestions.isEmpty {
                    self.tblViewSuggestions.isHidden = true
                }
                else {
                    self.tblViewSuggestions.isHidden = false
                    if self.suggestions.count < 3 {
                        self.heightSuggestionTable.constant = CGFloat(30 * self.suggestions.count)
                    }
                }
                //                }
                self.view.bringSubview(toFront: self.tblViewSuggestions)
            }
            else {
                self.tblViewSuggestions.isHidden = true
            }
            self.tblViewSuggestions.reloadData()
        }
        tagsField.onDidChangeHeightTo = { _, height in
            //            print("HeightTo \(height)")
            //302 + 40
            if self.usersImageDataForAI.isEmpty {
                self.searchViewHeight.constant = 100 + height
            }
            else {
                self.searchViewHeight.constant = 182 + height
            }
            self.heightTextfield.constant = height + 10
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        tagsField.onDidSelectTagView = { _, tagView in
            print("Select \(tagView)")
        }
        tagsField.onDidUnselectTagView = { _, tagView in
            print("Unselect \(tagView)")
        }
    }
}

extension GalleryImageGridViewController: PHPhotoLibraryChangeObserver {
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized //&& fetchResult != nil
        {
            let operation = BlockOperation(block: {
                self.fetchAlbumPhotos()
            })
            
            operation.completionBlock = {
                // main queue before acting on the change as we'll be updating the UI.
                DispatchQueue.main.async {
                    self.collectionViewGrid.reloadData()
                }
                
            }
            loadGalleryImagesqueue.addOperation(operation)
        }
    }
    
    func fetchAlbumPhotos()
    {
        let options = PHFetchOptions()
        //options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        //Added by akshay to exclude the video screenshots from assets
        if collectionItem.localizedTitle == "Videos"{
            options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        }
        else{
            // check album with atleast 1 image in galary
            options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        }
       
        let newFetchResult = PHAsset.fetchAssets(in: collectionItem, options: options)
        self.imagesArray = newFetchResult
    }
    
    func setImage(assetId: String, completionHandler: @escaping (UIImage) -> () ) {
        //        DispatchQueue.global(qos: .background).async {
        let requestOption = PHImageRequestOptions()
        requestOption.isSynchronous = true
        requestOption.deliveryMode = .opportunistic
        if let asset = PHAsset.fetchAssets(withLocalIdentifiers: [assetId], options: .none).firstObject {
            self.imageManager.requestImageData(for: asset, options: requestOption, resultHandler: { (data, _, _, _) in
                if let image = data {
                    completionHandler(UIImage(data: image)!)
                }
            })
        }
    }
}
