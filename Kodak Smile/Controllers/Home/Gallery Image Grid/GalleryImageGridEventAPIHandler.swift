//
//  GalleryImageGridEventAPIHandler.swift
//  Kodak Smile
//
//  Created by MAXIMESS152 on 02/10/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

extension GalleryImageGridViewController{
    
    func getStickers(completion: @escaping (Bool)->()){
        if WebserviceModelClass().isInternetAvailable(){
            WebserviceModelClass().getDataForomUrl(module: "EventStickers", url: EventsUrl.stickers) { (success, message, data) in
                guard let events = data["events"] as? [[String: Any]] else{
                    return
                }
                if UserDefaults.standard.value(forKey: "eventVersion") as? Double != data["version"] as? Double {
                    var apiIds = [Int]()
                    var apiStickerIds = [Int]()
                    print("data-----\(events)")
                    for stickersData in events{
                        var localStickerIds = [Int]()
                        apiIds.append(stickersData["id"] as! Int)
                        DBModelForEvents.shared.saveEventData(id: stickersData["id"] as! Int, eventVersion: data["version"] as! Double, name: stickersData["name"] as! String, updateVersion: stickersData["version"] as! Double)
                        let stickers = stickersData["stickers"] as! [[String: Any]]
                        localStickerIds.removeAll()
                        for data in stickers{
                            apiStickerIds.append(data["id"] as! Int)
                            localStickerIds.append(data["id"] as! Int)
                            DBModelForEvents.shared.saveStickersFrames(id: data["id"] as! Int, eventId: data["event_id"] as! Int, name: data["name"] as! String, thumbnail: data["thumbnail"] as! String, image: data["image"] as! String, eventName: stickersData["name"] as! String, isSticker: true)
                        }
                        self.deleteEventsData(id: stickersData["id"] as! Int, idArray: localStickerIds, isSticker:  true)
//                        let differentStickerIds = DBModelForEvents.shared.fetchEventStickerID(eventId: stickersData["id"] as! Int, isSticker: true).difference(from: localStickerIds)
//                        if !differentStickerIds.isEmpty{
//                            for id in differentStickerIds{
//                                DBModelForEvents.shared.deleteEventStickers(id: id)
//                            }
//                        }
                    }
                    self.getFrames(completion: { (success) in
                        UserDefaults.standard.set(data["version"] as? Double , forKey: "eventVersion")
                        let differentIds = DBModelForEvents.shared.fetchEventID().difference(from: apiIds)
//                        self.deleteEventsData(idArray1: differentIds, idArray2: apiStickerIds, isSticker: true)
//                        for eventId in differentIds{
//                            let differentStickerIds = DBModelForEvents.shared.fetchEventStickerID(eventId: eventId, isSticker: true).difference(from: apiStickerIds)
//                            print("differentIds----\(differentIds)")
//                            if !differentStickerIds.isEmpty{
//                                for id in differentStickerIds{
//                                    DBModelForEvents.shared.deleteEventStickers(id: id)
//                                }
//                            }
//                        }
                        if !differentIds.isEmpty{
                            for id in differentIds{
                                DBModelForEvents.shared.deleteEventsData(id: id)
                                DBModelForEvents.shared.deleteEvent(id: id)
                            }
                        }
                        completion(true)
                    })
                }else{
                    completion(true)
                }
            }
        }else{
            completion(false)
        }
    }
    
    
    func getFrames(completion: @escaping (Bool)->()){
        if WebserviceModelClass().isInternetAvailable(){
            WebserviceModelClass().getDataForomUrl(module: "EventFrames", url: EventsUrl.frames) { (success, message, data) in
                guard let events = data["events"] as? [[String: Any]] else{
                    return
                }
                var apiIds = [Int]()
                print("data-----\(events)")
                for framesData in events{
                    var localFramesIds = [Int]()
//                    apiIds.append(framesData["id"] as! Int)
                    let frames = framesData["frames"] as! [[String: Any]]
                    localFramesIds.removeAll()
                    for data in frames{
                        apiIds.append(data["id"] as! Int)
                        localFramesIds.append(data["id"] as! Int)
                        DBModelForEvents.shared.saveStickersFrames(id: data["id"] as! Int, eventId: data["event_id"] as! Int, name: data["name"] as! String, thumbnail: data["thumbnail"] as! String, image: data["image"] as! String, eventName: framesData["name"] as! String, isSticker: false)
                    }
                    self.deleteEventsData(id: framesData["id"] as! Int, idArray: localFramesIds, isSticker: false)
//                    let differentFramesIds = DBModelForEvents.shared.fetchEventStickerID(eventId: framesData["id"] as! Int, isSticker: false).difference(from: localFramesIds)
//                    if !differentFramesIds.isEmpty{
//                        for id in differentFramesIds{
//                            DBModelForEvents.shared.deleteEventFrames(id: id)
//                        }
//                    }
                }
//                let differentIds = DBModelForEvents.shared.fetchEventID().difference(from: apiIds)
//                self.deleteEventsData(idArray1: differentIds, idArray2: apiIds, isSticker: false)
//                for eventId in differentIds{
//                    let differentFramesIds = DBModelForEvents.shared.fetchEventStickerID(eventId: eventId, isSticker: false).difference(from: apiIds)
//                    if !differentFramesIds.isEmpty{
//                        for id in differentFramesIds{
//                            DBModelForEvents.shared.deleteEventFrames(id: id)
//                        }
//                    }
//                }
                completion(true)
            }
        }else{
            completion(false)
        }
    }
    
    func deleteEventsData(id: Int, idArray: [Int], isSticker: Bool){
        let differentIds = DBModelForEvents.shared.fetchEventStickerID(eventId: id, isSticker: isSticker).difference(from: idArray)
        if !differentIds.isEmpty{
            for id in differentIds{
                if isSticker{
                    DBModelForEvents.shared.deleteEventStickers(id: id)
                }else{
                    DBModelForEvents.shared.deleteEventFrames(id: id)
                }
            }
        }
    }
}
