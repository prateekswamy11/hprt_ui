//
//  GalleryGridCollectionViewCell.swift
//  Polaroid MINT
//
//  Created by maximess142 on 24/07/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import Photos

class GalleryGridCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imageViewThumbnail: UIImageView!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var imgViewVideo: UIImageView!
    
    var representedAssetIdentifier: String!
    var data: String? {
        didSet {
            setImage()
        }
    }
    let imgManager = PHImageManager.default()
    let requestOptions = PHImageRequestOptions()
    
    override func prepareForReuse() {
        self.imageViewThumbnail.image = nil
    }
    
    func showSelectedCell()
    {
        self.btnSelect.isHidden = false
        //        self.viewSelectionDark.isHidden = false
    }
    
    func showNormalCell()
    {
        self.btnSelect.isHidden = true
        //        self.viewSelectionDark.isHidden = true
    }
    
    func setImage() {
        self.requestOptions.isSynchronous = true
        self.requestOptions.deliveryMode = .fastFormat
        self.requestOptions.resizeMode = .fast
        //        DispatchQueue.global(qos: .background).async {
        if let asset = PHAsset.fetchAssets(withLocalIdentifiers: [self.data!], options: .none).firstObject {
//            self.imgManager.requestImageData(for: asset, options: self.requestOptions) { (data, _, _, _) in
//                if let image = data {
//                    self.imageViewThumbnail.image = UIImage(data: image)
//                }
//            }
            self.imgManager.requestImage(for:  asset, targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFit, options: self.requestOptions, resultHandler: { result, info in
                //                DispatchQueue.main.async() {
                if let image = result {
                    self.imageViewThumbnail.image = image
                }
                //                }
            })
        }
        else {
            DataBaseHelper().deleteImageData(assetId: self.data!)
        }
        //        }
    }

}
