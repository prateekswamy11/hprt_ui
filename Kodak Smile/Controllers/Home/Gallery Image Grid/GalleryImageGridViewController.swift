//
//  GalleryImageGridViewController.swift
//  Polaroid MINT
//
//  Created by maximess142 on 24/07/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import Photos
import Alamofire
import WSTagsField
import CopilotAPIAccess

class GalleryImageGridViewController: UIViewController
{
    //MARK:- Outlets
    @IBOutlet weak var lblAlbumName: UILabel!
    @IBOutlet weak var viewBottomOptions: UIView!
    @IBOutlet weak var lblAlbumNameLower: UILabel!
    @IBOutlet weak var collectionViewGrid: UICollectionView!
    @IBOutlet weak var collectionViewUsers: UICollectionView!
    @IBOutlet weak var constraintCollectionViewGridTop: NSLayoutConstraint!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnCollage: UIButton!
    @IBOutlet weak var btnPrint: UIButton!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var lblSearchedPhotosCount: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnDeleteSearchKeyword: UIButton!
    @IBOutlet weak var lblSearch: UILabel!
    @IBOutlet weak var btnSearchViewClose: UIButton!
    @IBOutlet weak var tagsView: UIView!
    @IBOutlet weak var heightTextfield: NSLayoutConstraint!
    @IBOutlet weak var heightUsersCOllectionView: NSLayoutConstraint!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblViewSuggestions: UITableView!
    @IBOutlet weak var heightSuggestionTable: NSLayoutConstraint!
    @IBOutlet weak var heightForLblSearchPhotosCount: NSLayoutConstraint!
    @IBOutlet weak var constraintCollectionViewGridBottom: NSLayoutConstraint!
    
    //MARK:- Variables
    var imagesArray: PHFetchResult<PHAsset>!
    var collectionItem = PHAssetCollection()
    let tagsField = WSTagsField()
    let inset: CGFloat = 3
    let minimumLineSpacing: CGFloat = 5
    let minimumInteritemSpacing: CGFloat = 5
    let cellsPerRow = 4
    var imagesFrom = String()
    /*fileprivate*/ let imageManager = PHCachingImageManager()
    /*fileprivate*/ var thumbnailSize: CGSize!
    fileprivate var previousPreheatRect = CGRect.zero
    let cellImgoptions = PHImageRequestOptions()
    var socialImages = [String]()
    var dropboxImages = [UIImage]()
    var arraySelectedImgURL = [String]()
    var imageDataArray = [Int]()
    var headerName = ""
    var isSelectOptionsActive = false
    var selectedCellsArray = [Int]()
    var usersSelectedCell: Int? = nil
    var scroll = ""
    var loadGalleryImagesqueue = OperationQueue()
    // variables for AI
    var selectedIndex = Int()
    var usersImageDataForAI = [UIImage]()
    var allImages = [String]()
    var filteredArray = [String]()
    var faceData = [String]()
    var suggestions = [String]()
    var multiTagArray = [String]()
    var searchActive = false
    var combineSearch = false
    // we set a variable to hold the contentOffSet before scroll view scrolls
    var lastContentOffset: CGFloat = 0
    var isVideos = false
    var cameFrom = ""

    //Hide Status bar
    //    override var prefersStatusBarHidden: Bool {
    //        return status_Bar_Hidden
    //    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtSearch.delegate = self
        PHPhotoLibrary.shared().register(self)
        //        self.dropboxImages = DBLayer().fetchDropboxPhotosFromDB()
        // Do any additional setup after loading the view.
        self.initialSetup()
        self.checkBottomOptions()
        self.setupAI()
        self.setTagToTextField()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        self.view.isUserInteractionEnabled = true
        self.tblViewSuggestions.isHidden = true
            //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.gallery.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.view.isUserInteractionEnabled = true
        self.stopLoader()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        if self.cameFrom != "HomeViewController"{
//            if SMLFlagForAI && shouldStopAIOperation.boolValue{
//                SMLFlagForAI = !SMLFlagForAI
//            }
//            AIModel().stopAIOperation(flag: ObjCBool(false))
////            PredictionController().getPredictionFromImageAsset(assetArray: imagesArray)
//        }
//        if self.cameFrom != "HomeViewController"{
//            PredictionController().getPredictionFromImageAsset(assetArray: imagesArray)
//        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchBtnClicked(_ sender: UIButton) {
        self.tagsField.removeTags()
        sender.isHidden = true
        self.lblSearch.isHidden = false
        self.btnSearchViewClose.isHidden = false
        self.searchView.backgroundColor = UIColor(red: 84, green: 84, blue: 84, a: 0.2)
        if self.usersImageDataForAI.isEmpty {
            self.searchViewHeight.constant = 100
            self.heightUsersCOllectionView.constant = 0
        }
        else {
            self.searchViewHeight.constant = 182
        }
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        self.heightForLblSearchPhotosCount.constant = 20
    }
    
    @IBAction func closeBtnClicked(_ sender: UIButton) {
        self.searchViewHeight.constant = 35
        self.heightForLblSearchPhotosCount.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            sender.isHidden = true
            
            self.lblSearch.isHidden = true
            self.btnSearch.isHidden = false
            self.searchView.backgroundColor = UIColor.clear
        }
        self.combineSearch = false
        self.searchActive = false
        self.usersSelectedCell = nil
        self.tagsField.text = ""
        self.txtSearch.text = ""
        self.lblSearchedPhotosCount.text = ""
        self.tblViewSuggestions.reloadData()
        self.collectionViewGrid.reloadData()
        self.collectionViewUsers.reloadData()
        self.tagsField.removeTags()
        self.view.endEditing(true)
    }
    
    @IBAction func selectBtnClicked(_ sender: UIButton) {
        self.isSelectOptionsActive = self.isSelectOptionsActive == true ? false : true
        self.checkSelectOptions()
        self.checkBottomOptions()
    }
    
    @IBAction func shareBtnClicked(_ sender: UIButton) {
        let tapSharePhotoAnalyticsEvent = TapSharePhotoAnalyticsEvent()
        Copilot.instance.report.log(event: tapSharePhotoAnalyticsEvent)
        if self.arraySelectedImgURL.count == 0
        {
            //            showAlert(title:"Alert".localisedString(), message: No_Seleted_Image.localisedString())
            self.view.makeToast(No_Seleted_Image.localisedString())
        }
        else
        {
            if self.imagesFrom == "Local"
            {
                let imgArray = self.getImagesFromAssets()
                let activityViewController = UIActivityViewController(activityItems: imgArray, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = sender // so that iPads won't crash
                // present the view controller
                self.present(activityViewController, animated: true, completion: nil)
            }
            else
            {
                if WebserviceModelClass().isInternetAvailable()
                {
                    let imgArray = self.getImagesFromUrls()
                    let activityViewController = UIActivityViewController(activityItems: imgArray, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = sender // so that iPads won't crash
                    // present the view controller
                    self.present(activityViewController, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func collageBtnClicked(_ sender: UIButton) {
//        AIModel().stopAIOperation(flag: ObjCBool(true))
//        AIModel().stopAIThread()
        let tapCreateCollageAnalyticsEvent = TapCreateCollageAnalyticsEvent()
        Copilot.instance.report.log(event: tapCreateCollageAnalyticsEvent)
        if self.arraySelectedImgURL.isEmpty {
            self.view.makeToast(No_Seleted_Image.localisedString())
        }
        else {
            if self.arraySelectedImgURL.count > 4 {
                self.view.makeToast("You can not select more than 4 images to make collage".localisedString())
            }
            else {
//                AIModel().stopAIOperation(flag: ObjCBool(true))
//                AIModel().stopAIThread()
                let selectLayoutVC = storyboards.collageStoryboard.instantiateViewController(withIdentifier: "SelectLayoutViewController") as! SelectLayoutViewController
                selectLayoutVC.imagesFrom = self.imagesFrom
                selectLayoutVC.imagesArray = self.imagesArray
                selectLayoutVC.socialImages = self.socialImages
                selectLayoutVC.collectionItem = self.collectionItem
                selectLayoutVC.selAssetsIdentifiersArray = self.arraySelectedImgURL
                if self.imagesFrom == "Local"
                {
                    if self.headerName == "Camera Roll".localisedString() || self.headerName == "All Photos".localisedString() {
                        if self.searchActive {
                            selectLayoutVC.filteredArray = self.filteredArray
                        }
                        else if self.combineSearch {
                            if self.multiTagArray.isEmpty {
                                selectLayoutVC.filteredArray = self.filteredArray
                            }else {
                                selectLayoutVC.filteredArray = self.faceData
                            }
                        }
                    }
                    selectLayoutVC.selectedImages = self.getImagesFromAssets()
                } else {
                    if WebserviceModelClass().isInternetAvailable(){
                        selectLayoutVC.selectedImages = self.getImagesFromUrls()
                    }
                }
                selectLayoutVC.fetchResult = self.imagesArray
                selectLayoutVC.imageDataArray = self.imageDataArray
                selectLayoutVC.selectedCellsArray = self.selectedCellsArray
                self.navigationController?.pushViewController(selectLayoutVC, animated: true)
            }
        }
    }
    
    @IBAction func crossBtnClicked(_ sender: UIButton) {
        if usersSelectedCell != nil {
            self.searchActive = true
            self.combineSearch = true
        }
        else {
            self.searchActive = false
        }
        self.txtSearch.text = ""
        self.suggestions.removeAll()
        self.tblViewSuggestions.reloadData()
        self.collectionViewGrid.reloadData()
        self.view.endEditing(true)
        self.tagsField.removeTags()
        self.tagsField.text = ""
        self.lblSearchedPhotosCount.text = ""
    }
    
    @IBAction func printBtnClicked(_ sender: UIButton) {
//        AIModel().stopAIOperation(flag: ObjCBool(true))
//        AIModel().stopAIThread()
        let TapPrinPreviewtAnalyticsEvent = TapPrintPreviewAnalyticsEvent()
        Copilot.instance.report.log(event: TapPrinPreviewtAnalyticsEvent)
        if self.arraySelectedImgURL.count == 0
        {
            //            showAlert(title:"Alert".localisedString(), message: No_Seleted_Image.localisedString())
            self.view.makeToast(No_Seleted_Image.localisedString())
        }
        else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier:  "MultipleImagePreviewViewController") as! MultipleImagePreviewViewController
            if self.imagesFrom != "Local" {
                vc.imageSocialArray = self.arraySelectedImgURL
                vc.selectedImages = self.getImagesFromUrls()
            }
            else {
                vc.imageLocalArray = self.imagesArray
                vc.selectedImages = self.getImagesFromAssets()
            }
//            AIModel().stopAIOperation(flag: ObjCBool(true))
//            AIModel().stopAIThread()
            vc.imagesFrom = self.imagesFrom
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    // MARK: UIScrollView
    
    private func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        if self.imagesFrom == "Local" {
            updateCachedAssets()
        }
        if (self.lastContentOffset < scrollView.contentOffset.y) {
            UIView.animate(withDuration: 0.2)
            {
                // it's going down
                self.constraintCollectionViewGridTop.constant = -20 //20
                self.lblAlbumName.alpha = 1
                self.lblAlbumNameLower.alpha = 0
                //                self.view.layoutIfNeeded()
                //                self.lblAlbumName.isHidden = false
                //                self.lblAlbumNameLower.isHidden = true
                self.scroll = "up"
            }
            
        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
            UIView.animate(withDuration: 0.2){
                // it's going up
                //                print("At the top")
                self.constraintCollectionViewGridTop.constant = 10 // 70
                self.lblAlbumName.alpha = 0
                self.lblAlbumNameLower.alpha = 1
                self.scroll = "down"
                //                self.view.layoutIfNeeded()
                //                self.lblAlbumName.isHidden = true
                //                self.lblAlbumNameLower.isHidden = false
                
            }
        }
        self.updateHeaderText()
    }
    // MARK: Asset Caching
    fileprivate func resetCachedAssets() {
        imageManager.stopCachingImagesForAllAssets()
        previousPreheatRect = .zero
    }
    
    fileprivate func updateCachedAssets() {
        //        if is_Showing_Social_Photos == false
        //        {
        if checkPhotoLibraryPermission() == true
        {
            // Update only if the view is visible.
            guard isViewLoaded && view.window != nil else { return }
            // The preheat window is twice the height of the visible rect.
            let visibleRect = CGRect(origin: collectionViewGrid!.contentOffset, size: collectionViewGrid!.bounds.size)
            let preheatRect = visibleRect.insetBy(dx: 0, dy: -0.5 * visibleRect.height)
            // Update only if the visible area is significantly different from the last preheated area.
            let delta = abs(preheatRect.midY - previousPreheatRect.midY)
            guard delta > view.bounds.height / 3 else { return }
            // Compute the assets to start caching and to stop caching.
            let (addedRects, removedRects) = differencesBetweenRects(previousPreheatRect, preheatRect)
            let addedAssets = addedRects
                .flatMap { rect in collectionViewGrid!.indexPathsForElements(in: rect) }
                .map { indexPath in imagesArray.object(at: indexPath.item) }
            let removedAssets = removedRects
                .flatMap { rect in collectionViewGrid!.indexPathsForElements(in: rect) }
                .map { indexPath in imagesArray.object(at: indexPath.item) }
            // Update the assets the PHCachingImageManager is caching.
            imageManager.startCachingImages(for: addedAssets,
                                            targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
            imageManager.stopCachingImages(for: removedAssets,
                                           targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
            // Store the preheat rect to compare against in the future.
            previousPreheatRect = preheatRect
        }
        //        }
    }
    
    fileprivate func differencesBetweenRects(_ old: CGRect, _ new: CGRect) -> (added: [CGRect], removed: [CGRect]) {
        if old.intersects(new) {
            var added = [CGRect]()
            if new.maxY > old.maxY {
                added += [CGRect(x: new.origin.x, y: old.maxY,
                                 width: new.width, height: new.maxY - old.maxY)]
            }
            if old.minY > new.minY {
                added += [CGRect(x: new.origin.x, y: new.minY,
                                 width: new.width, height: old.minY - new.minY)]
            }
            var removed = [CGRect]()
            if new.maxY < old.maxY {
                removed += [CGRect(x: new.origin.x, y: new.maxY,
                                   width: new.width, height: old.maxY - new.maxY)]
            }
            if old.minY < new.minY {
                removed += [CGRect(x: new.origin.x, y: old.minY,
                                   width: new.width, height: new.minY - old.minY)]
            }
            return (added, removed)
        } else {
            return ([new], [old])
        }
    }
}

private extension UICollectionView {
    func indexPathsForElements(in rect: CGRect) -> [IndexPath] {
        let allLayoutAttributes = collectionViewLayout.layoutAttributesForElements(in: rect)!
        return allLayoutAttributes.map { $0.indexPath }
    }
}
