import Foundation
import UIKit
import Photos
import Alamofire
import AlamofireImage
import CopilotAPIAccess

extension GalleryImageGridViewController : UICollectionViewDelegate, UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == collectionViewUsers {
            return self.usersImageDataForAI.count
        }
        else {
            if (self.headerName == "Camera Roll".localisedString() || self.headerName == "All Photos".localisedString()) {
                if searchActive && combineSearch {
                    return filteredArray.count
                }
                else if searchActive {
                    return filteredArray.count
                }
                else if self.combineSearch && !self.usersImageDataForAI.isEmpty {
                    return self.faceData.count
                }
                else {
                    return imagesArray.count
                }
            }
            if self.imagesFrom == "Local" {
                return imagesArray.count
            }
            else if self.headerName == "Dropbox" {
                return self.dropboxImages.count
            }
            else {
                return self.socialImages.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewUsers {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UsersCollectionViewCell", for: indexPath) as! UsersCollectionViewCell
            cell.imgProfile.image =  self.usersImageDataForAI[indexPath.item]
            if self.usersSelectedCell != nil {
                if self.usersSelectedCell == indexPath.item {
                    cell.borderColor = UIColor.red
                }
                else  {
                    cell.borderColor = UIColor.clear
                }
            }
            else {
                cell.borderColor = UIColor.clear
            }
            return cell
        }
        else {
            let cell : GalleryGridCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryGridCollectionViewCell", for: indexPath) as! GalleryGridCollectionViewCell
            var imageURL = ""
            if (self.headerName == "Camera Roll".localisedString() || self.headerName == "All Photos".localisedString()) {
                if searchActive{
                    if self.filteredArray.indices.contains(indexPath.row) {
                        cell.data = self.filteredArray[indexPath.item]
                        cell.representedAssetIdentifier = self.filteredArray[indexPath.item]
                    }
                    //                self.setupImages(data: filteredArray, indexPath: indexPath, cell: cell)
                }
                else if self.combineSearch {
                    cell.data = self.faceData[indexPath.item]
                    cell.representedAssetIdentifier = self.faceData[indexPath.item]
                }
                else {
                    //                    if self.allImages.indices.contains(indexPath.row) {
                    //                        cell.data = self.allImages[indexPath.item]
                    //                        cell.representedAssetIdentifier = self.allImages[indexPath.item]
                    //                    }
                    let asset = getSelectedImageAsset(index: indexPath.item)
                    cell.representedAssetIdentifier = asset.localIdentifier
                    imageManager.requestImage(for: asset, targetSize: thumbnailSize, contentMode: .aspectFill, options: cellImgoptions, resultHandler: { image, _ in
                        // The cell may have been recycled by the time this handler gets called;
                        // set the cell's thumbnail image only if it's still showing the same asset.
                        if cell.representedAssetIdentifier == asset.localIdentifier && image != nil {
                            cell.imageViewThumbnail.image = image
                            imageURL = asset.localIdentifier
                        }
                    })
                    //                self.setupImages(data: allImages, indexPath: indexPath, cell: cell)
                }
            }
            else if self.imagesFrom == "Local" {
                //let asset = imagesArray[indexPath.item]
                let asset = getSelectedImageAsset(index: indexPath.item)
                cell.representedAssetIdentifier = asset.localIdentifier
                imageManager.requestImage(for: asset, targetSize: thumbnailSize, contentMode: .aspectFill, options: cellImgoptions, resultHandler: { image, _ in
                    // The cell may have been recycled by the time this handler gets called;
                    // set the cell's thumbnail image only if it's still showing the same asset.
                    if cell.representedAssetIdentifier == asset.localIdentifier && image != nil {
                        cell.imageViewThumbnail.image = image
                        imageURL = asset.localIdentifier
                    }
                })
            }
            else {
                if cell.imageViewThumbnail.image == nil
                {
                    cell.loader.startAnimating()
                    let url = self.socialImages[indexPath.row]
                    print("ImageURL\(url)")
                    imageURL = url
                    cell.imageViewThumbnail.af_setImage(
                        withURL: URL(string:url)!,
                        imageTransition: .crossDissolve(0),
                        completion: { Void in
                            cell.loader.stopAnimating()
                    })
                }
            }
            if arraySelectedImgURL.contains(imageURL)
            {
                cell.showSelectedCell()
                //            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .top)
                //            collectionView.delegate?.collectionView!(collectionView, didSelectItemAt: indexPath)
            }
            else
            {
                cell.showNormalCell()
            }
            if self.headerName == "Videos" {
                cell.imgViewVideo.isHidden = false
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        if self.arraySelectedImgURL.count == 10 && !self.selectedCellsArray.contains(indexPath.item) {
            self.view.makeToast("You can select maximum 10 images".localisedString(), duration: 2.0, position: .bottom)
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        isComeFrom = "GallertGrid"
        if collectionView == collectionViewUsers {
            if usersSelectedCell == indexPath.item {
                if (self.txtSearch.text?.isEmpty)! {
                    self.combineSearch = false
                    self.searchActive = false
                }
                else {
                    self.combineSearch = false
                }
                self.usersSelectedCell = nil
            }
            else {
                if (self.txtSearch.text?.isEmpty)! {
                    let data = faceData
                    self.faceData.removeAll()
                    self.faceData = DataBaseHelper().fetchClusteringFromDB(ofClass: "\(indexPath.item)",search: [""], tableName: "ClusterTable", previousData: data)
                    if self.faceData.isEmpty {
                        self.heightForLblSearchPhotosCount.constant = 50
                        self.lblSearchedPhotosCount.numberOfLines = 2
                        self.lblSearchedPhotosCount.textAlignment = .center
                        let formattedString = NSMutableAttributedString()
                        formattedString
                            .bold("No Result")
                            .normal("\nTry a new Search")
                        //                .bold("Bold Text")
                        self.lblSearchedPhotosCount.attributedText = formattedString
                    }else {
                        self.lblSearchedPhotosCount.text = "\(self.faceData.count) photos found"
                    }
                }
                else {
                    let data = filteredArray
                    self.filteredArray.removeAll()
                    self.filteredArray = DataBaseHelper().fetchClusteringFromDB(ofClass: "\(indexPath.item)",search: self.multiTagArray, tableName: "ClusterTable", previousData: data)
                    if self.filteredArray.isEmpty {
                        self.heightForLblSearchPhotosCount.constant = 50
                        self.lblSearchedPhotosCount.numberOfLines = 2
                        self.lblSearchedPhotosCount.textAlignment = .center
                        let formattedString = NSMutableAttributedString()
                        formattedString
                            .bold("No Result")
                            .normal("\nTry a new Search")
                        //                .bold("Bold Text")
                        self.lblSearchedPhotosCount.attributedText = formattedString
                    }
                    else {
                        self.lblSearchedPhotosCount.text = "\(self.filteredArray.count) photos found"
                    }
                    self.searchActive = true
                }
                //            for image in data {
                //                var dict = [String: Any]()
                //                dict["Assets"] = image
                //                self.faceData.append(dict)
                //            }
                if self.faceData.isEmpty{
                    self.combineSearch = false
                }else{
                    self.combineSearch = true
                }
                self.usersSelectedCell = indexPath.item
            }
            //            self.lblSearchedPhotosCount.text = "\(self.filteredArray.count) photos found"
            self.collectionViewUsers.reloadData()
            self.collectionViewGrid.reloadData()
        }
        else {
            let tapPhotoAnalyticsEvent = TapPhotoAnalyticsEvent()
            Copilot.instance.report.log(event: tapPhotoAnalyticsEvent)
            if self.isSelectOptionsActive {
                if self.imagesFrom == "Local"
                {
                    if self.headerName == "Camera Roll".localisedString() || self.headerName == "All Photos".localisedString() {
                        if self.usersSelectedCell != nil && (self.searchActive && self.combineSearch) {
                            self.updateSelectedAssetArray(assetIdentifier: self.filteredArray[indexPath.item], indexpath: indexPath)
                        }else if self.usersSelectedCell != nil && !(self.searchActive && self.combineSearch) {
                            self.updateSelectedAssetArray(assetIdentifier: self.faceData[indexPath.item], indexpath: indexPath)
                        }else if self.searchActive {
                            self.updateSelectedAssetArray(assetIdentifier: self.filteredArray[indexPath.item], indexpath: indexPath)
                        }
                        else {
                            let asset = getSelectedImageAssetBySelectedOrder(index: indexPath.item)
                            let assetIdentifier = asset.localIdentifier
                            self.updateSelectedAssetArray(assetIdentifier: assetIdentifier, indexpath: indexPath, asset: asset)
                        }
                        
                    }else {
                        //let asset = fetchResult.object(at: indexPath.item)
                        let asset = getSelectedImageAssetBySelectedOrder(index: indexPath.item)
                        let assetIdentifier = asset.localIdentifier
                        self.updateSelectedAssetArray(assetIdentifier: assetIdentifier, indexpath: indexPath, asset: asset)
                    }
                }
                else
                {
                    if WebserviceModelClass().isInternetAvailable() {
                        //                let asset = getSelectedImageAssetBySelectedOrder(index: indexPath.item)
                        //                let url = arraySelectedImgURL[indexPath.row]
                        let url = self.socialImages[indexPath.row]
                        self.updateSelectedAssetArray(assetIdentifier: url, indexpath: indexPath)
                    }else{
                        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                        comeFromStrNoInternetSocialMedia = "NormalNointernet"
                        self.present(alertPopUp, animated: true, completion: nil)
                    }
                }
            }
            else
            {
                self.startLoader()
                //                AIModel().stopAIOperation(flag: ObjCBool(true))
                //                AIModel().stopAIThread()
                let imagePreviewVC = storyboards.imgPreviewStoryboard.instantiateViewController(withIdentifier: "ImagePreviewViewController") as! ImagePreviewViewController
//                let VideoTrimmerVC = storyboards.arStoryboard.instantiateViewController(withIdentifier: "VideoTrimmerViewController") as! VideoTrimmerViewController
                var imageToSend = UIImage()
                self.getStickers { (success) in
                    if self.imagesFrom == "Local"
                    {
                        let operationQueue = OperationQueue()
                        let operation1 = BlockOperation(block: { () -> Void in
                            let asset = self.getSelectedImageAsset(index: indexPath.item)
                            imageToSend = self.getImageFromAsset(asset: asset)
                        })
                        operationQueue.addOperation(operation1)
                        //                    if self.lblAlbumNameLower.text == "Videos" {
                        operation1.completionBlock =
                            {
                                DispatchQueue.main.async()
                                    {
                                        self.view.isUserInteractionEnabled = true
                                        self.stopLoader()
                                        imgMainEdit = imageToSend
                                        if self.lblAlbumNameLower.text == "Videos"
                                        {
                                            let asset = self.getSelectedImageAsset(index: indexPath.item)
                                            let requestOptions = PHVideoRequestOptions()
                                            requestOptions.deliveryMode = PHVideoRequestOptionsDeliveryMode.automatic
                                            requestOptions.isNetworkAccessAllowed = true
                                            PHCachingImageManager().requestAVAsset(forVideo: asset, options: requestOptions) { (avAsset, _, info) in
                                                DispatchQueue.main.async {
                                                    if let isIcloudIMg = (info![PHImageResultIsInCloudKey] as? Int)  {
                                                        if (isIcloudIMg == 0) {
                                                            if let avAsset = avAsset {
//                                                                VideoTrimmerVC.videoAsset = avAsset
//                                                                VideoTrimmerVC.isComeFromImagePreview = false
//                                                                self.navigationController?.pushViewController(VideoTrimmerVC, animated: true)
                                                            }
                                                        }
                                                        else{
                                                            if let avAsset = avAsset {
//                                                                VideoTrimmerVC.videoAsset = avAsset
//                                                                VideoTrimmerVC.isComeFromImagePreview = false
//                                                                self.navigationController?.pushViewController(VideoTrimmerVC, animated: true)
                                                            }else{
                                                                self.view.isUserInteractionEnabled = true
                                                                self.stopLoader()
                                                                let dialog = UIAlertController(title: "Cloud Error", message: "This video could not be downloaded from iCloud. Please connect to internet.", preferredStyle: UIAlertControllerStyle.alert)
                                                                let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                                                                dialog.addAction(okAction)
                                                                self.present(dialog, animated:true, completion:nil)
                                                            }
                                                        }
                                                    }
                                                    else{
                                                        if let avAsset = avAsset {
//                                                            VideoTrimmerVC.videoAsset = avAsset
//                                                            VideoTrimmerVC.isComeFromImagePreview = false
//                                                            self.navigationController?.pushViewController(VideoTrimmerVC, animated: false)
                                                        }
                                                    }
                                                }
                                            }
                                        }else{
                                            //                                          self.present(imagePreviewVC, animated: true, completion: nil)
                                            // Don't do animation false it occures regrassion in the app
                                            self.navigationController?.pushViewController(imagePreviewVC, animated: false)
                                        }
                                }
                        }
                        //                    }
                        //                    }
                    }
                    else
                    {
                        if self.headerName == "Dropbox"
                        {
                           imgMainEdit = self.dropboxImages[indexPath.item]
                        }
                        else
                        {
                            if WebserviceModelClass().isInternetAvailable() {
                                let url = self.socialImages[indexPath.row]
                                Alamofire.request(url).responseImage
                                    { response in
                                        if let image = response.result.value
                                        {
                                            if let data:Data = UIImageJPEGRepresentation(image, 1.0) {
                                                let img = UIImage.init(data: data)
                                                
                                                imgMainEdit = img!
                                            }
                                            self.stopLoader()
                                            self.navigationController?.pushViewController(imagePreviewVC, animated: false)
                                            //                                        self.present(imagePreviewVC, animated: true, completion: nil)
                                        }
                                }
                            }
                            else {
                                self.view.isUserInteractionEnabled = true
                                self.stopLoader()
                                let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                                comeFromStrNoInternetSocialMedia = "NormalNointernet"
                                self.present(alertPopUp, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getSelectedImageAssetBySelectedOrder(index : Int) -> PHAsset {
        //this condition for oldest and newest first function
        var asset = PHAsset()
        if isSortByOldestFirst {
            asset = imagesArray.object(at: index)
        }
        else{
            
            asset = imagesArray.object(at: imagesArray.count - (index + 1))
        }
        
        return asset
    }
    
    func updateSelectedAssetArray(assetIdentifier : String, indexpath: IndexPath, asset: PHAsset? = nil)
    {
        if arraySelectedImgURL.contains(assetIdentifier)
        {
            if let indexOfCell = self.selectedCellsArray.index(of: indexpath.item) {
                self.selectedCellsArray.remove(at: indexOfCell)
            }
            if let index = arraySelectedImgURL.index(of: assetIdentifier)
            {
                self.arraySelectedImgURL.remove(at: index)
            }
        }
        else
        {
            self.selectedCellsArray.append(indexpath.item)
            self.arraySelectedImgURL.append(assetIdentifier)
        }
        let cell = collectionViewGrid.cellForItem(at: indexpath) as! GalleryGridCollectionViewCell
        if self.arraySelectedImgURL.contains(assetIdentifier)
        {
            cell.showSelectedCell()
        }
        else
        {
            cell.showNormalCell()
        }
        self.checkBottomOptions()
    }
}

extension GalleryImageGridViewController : UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView != collectionViewUsers {
            var marginsAndInsets = CGFloat()
            if #available(iOS 11.0, *) {
                marginsAndInsets = inset * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
            } else {
                // Fallback on earlier versions
                marginsAndInsets = inset * 2 + 0 + 0 + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
            }
            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
            return CGSize(width: itemWidth, height: itemWidth)
        }
        else {
            return CGSize(width: 70, height: 70)
        }
    }
}

