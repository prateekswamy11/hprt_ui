//
//  UsersCollectionViewCell.swift
//  ObjectAndFacesDetection
//
//  Created by MacMini003 on 23/10/18.
//  Copyright © 2018 MAXIMESS142. All rights reserved.
//

import UIKit

class UsersCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
}
