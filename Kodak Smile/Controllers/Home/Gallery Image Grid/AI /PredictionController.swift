
//
//  PredictionController.swift
//  Kodak Smile
//
//  Created by MAXIMESS152 on 01/04/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import Photos
import Vision
import FaceCropper
import ImageIO

var SMLIsUploadStatusChecked = true // for check status
var SMLIsImageUploaded = true   // for image upload status
var SMLIsResultReady = false   // for get result
var SMLFlagForAI = false

var SMLIsObjectNotificationSend = false
var SMLIsFaceCroppingNotificationSend = false
var shouldStopAIOperation : ObjCBool = false    // for stop array enumeration


class PredictionController: NSObject {
    
    //MARK:- Variables
    let imgManager = PHImageManager.default()
    let requestOptions = PHImageRequestOptions()
    var result: Result?
    let modelDataHandler: ModelDataHandler? = ModelDataHandler(modelFileName: "detect", labelsFileName: "labelmap", labelsFileExtension: "txt")
//    var dbHelper = DataBaseHelper()
    var usersData = [String]()
    var imageCount = 0
    lazy var predictionQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Prediction queue"
        queue.qualityOfService = .background
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    
    //MARK:- Functions
    func getPredictionFromImageAsset(assetArray:PHFetchResult<PHAsset>){
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .fastFormat
        requestOptions.resizeMode = .fast
        //        requestOptions.isNetworkAccessAllowed = true
        var imageIndex = -1
        print((self.predictionQueue.operationCount))
        self.predictionQueue.cancelAllOperations()
        //        QUEUEFORAI.qualityOfService = .background
        if !UserDefaults.standard.bool(forKey: "isObjectDetected") || UserDefaults.standard.value(forKey: "isObjectDetected") == nil {
            let operation : BlockOperation = BlockOperation (block: {
                //        QUEUEFORAI = DispatchWorkItem(qos: .background, flags: .enforceQoS, block: {
                //        QUEUEFORAI.async {
                SMLFlagForAI = true
                //            UserDefaults.standard.set(false, forKey: "isRegularLoad")
                //            autoreleasepool {
                
                
                //                    if !SMLIsObjectNotificationSend {
                //                        appDelegate().scheduleNotification(notificationType: "Object and Location detection stared.")
                //                    }
                print("Prediction started")
                self.registerBackgroundTask()
                assetArray.enumerateObjects { (asset, index, stop) in
                    stop.pointee = shouldStopAIOperation
                    autoreleasepool{
                        if !DataBaseHelper().checkAssetIdAvailability(assetID: asset.localIdentifier) {
                            print("index:-",index)
                            if index != imageIndex {
                                autoreleasepool {
                                    var locationData = ""
                                    imageIndex = index
                                    self.imgManager.requestImageData(for: asset, options: self.requestOptions, resultHandler: { (imgData,_,_,_) in
                                        if let img = imgData {
                                            if let imageLocation = asset.location {
                                                self.lookUpCurrentLocation(location: imageLocation, completionHandler: { (data) in
                                                    if let location = data {
                                                        var data = [String]()
                                                        if let locality = location.locality {
                                                            data.append(locality.uppercased())
                                                        }
                                                        if let administrativeArea = location.administrativeArea {
                                                            //                                                    if administrativeArea.count == 2 {
                                                            if LocationAbbriviation().stateDictionary.keys.contains(administrativeArea) {
                                                                data.append(administrativeArea.uppercased() + "+" +  LocationAbbriviation().stateDictionary[administrativeArea]!.uppercased())
                                                                //                                                        }
                                                            }
                                                            else {
                                                                data.append(administrativeArea.uppercased())
                                                            }
                                                        }
                                                        if let country = location.country {
                                                            //                                                    if country.count == 2{
                                                            if LocationAbbriviation().countryDictionary.keys.contains(country){
                                                                data.append(country.uppercased()
                                                                    + "+" + (LocationAbbriviation().countryDictionary[country]!.uppercased()))
                                                                //                                                        }
                                                            }else {
                                                                data.append(country.uppercased())
                                                            }
                                                        }
                                                        if !data.isEmpty {
                                                            locationData = data.joined(separator: ",")
                                                        }
                                                    }
                                                    DataBaseHelper().insertAllPhotos(assetId: asset.localIdentifier, faceId: "", objects: "", location: locationData)
                                                    DataBaseHelper().updateFacesDetectionStatus(assetId: asset.localIdentifier, status: 3)
                                                })
                                            }else{
                                                //                                            let objects = self.runModel(onPixelBuffer: UIImage(data: img)!.pixelBuffer()!)
                                                //                                            //                                            self.detectingFaces(asset: asset, image: UIImage(data: img)!)
                                                DataBaseHelper().insertAllPhotos(assetId: asset.localIdentifier, faceId: "", objects: "", location: locationData)
                                                DataBaseHelper().updateFacesDetectionStatus(assetId: asset.localIdentifier, status: 3)
                                            }
                                        }
                                    })
                                }
                            }
                        }
                    }
                    UserDefaults.standard.set(0, forKey: "recentImageWithFace")
                }
                if imageIndex + 1 == assetArray.count || imageIndex == -1{
                    self.predictionQueue.cancelAllOperations()
                    self.endBackgroundTask()
                    self.cropFacesFromAsset(assetArray: assetArray)
                    UserDefaults.standard.set(true, forKey: "isObjectDetected")
                }
                //                }
                //                self.predictionQueue.cancelAllOperations()
                ////                QUEUEFORAI.suspend()
                //                if imageIndex + 1 == assetArray.count || imageIndex == -1{
                //                    self.endBackgroundTask()
                //                    self.cropFacesFromAsset(assetArray: assetArray)
                //                    UserDefaults.standard.set(true, forKey: "isObjectDetected")
                //                }
                
                //                self.allImages = DataBaseHelper().fetchAllImagesDataFromDB()
                //                print("Time taken to Predict:-",self.seconds)
                
                //                self.faceData = DataBaseHelper().fetchDataFromDB()
            })
            //            DispatchQueue.main.async {
            //                if SMLIsUploadStatusChecked && UserDefaults.standard.bool(forKey: "isRegularLoad") {
            //                    SMLIsUploadStatusChecked = false
            //                    self.checkStatus()
            //                }
            //            }
            //        }
            predictionQueue.addOperation(operation)
        }
            
            //                QUEUEFORAI.suspend()
        else if imageIndex + 1 == assetArray.count || imageIndex == -1{
            self.predictionQueue.cancelAllOperations()
            self.endBackgroundTask()
            self.cropFacesFromAsset(assetArray: assetArray)
            UserDefaults.standard.set(true, forKey: "isObjectDetected")
        }
        
        //        DispatchQueue.global().async(execute: QUEUEFORAI!)
        
        //        })
        
        //        operation1.
    }
    
    func cropFacesFromAsset(assetArray:PHFetchResult<PHAsset>) {
        var imageIndex = -1
        self.endBackgroundTask()
        SMLIsUploadStatusChecked = true
//        queue.async {
        if (!UserDefaults.standard.bool(forKey: "isRegularLoad") || UserDefaults.standard.value(forKey: "isRegularLoad") == nil) {
        let operation : BlockOperation = BlockOperation (block: {
//            autoreleasepool {
            
                
                //                if !SMLIsFaceCroppingNotificationSend {
                //                    appDelegate().scheduleNotification(notificationType: "Face detection stared.")
                //                }
                print("Face cropping started")
                assetArray.enumerateObjects { (asset, index, stop) in
                    stop.pointee = shouldStopAIOperation
                    autoreleasepool{
                        if !DataBaseHelper().checkAssetIdAvailabilityFaceCropping(assetID: asset.localIdentifier) && DataBaseHelper().fetchFaceDetectionStatus(assetId: asset.localIdentifier) == 3{
                            
                            print("face cropping index:- \(index)")
                            if index >= UserDefaults.standard.value(forKey: "recentImageWithFace") as! Int {
                                self.imageCount = index + 1
                                
                                if index != imageIndex {
                                    DataBaseHelper().updateFacesDetectionStatus(assetId: asset.localIdentifier, status: 2)
                                    autoreleasepool {
                                        imageIndex = index
                                        self.imgManager.requestImageData(for: asset, options: self.requestOptions, resultHandler: { (imgData, _, _, _) in
                                            if let img = imgData {
                                                
                                                //                                            let objects = self.runModel(onPixelBuffer: UIImage(data: img)!.pixelBuffer()!)
                                                self.detectingFaces(asset: asset, image: UIImage(data: img)!, imageIndex: index)
                                                //                                            DataBaseHelper().insertAllPhotos(assetId: asset.localIdentifier, faceId: "", faceCount: 0, objects: objects, location: locationData)
                                            }
                                        })
                                    }
                                }
                            }
                        }
                    }
                }
                SMLIsUploadStatusChecked = true
                if imageIndex + 1 == assetArray.count {
                    UserDefaults.standard.set(true, forKey: "isRegularLoad")
                }
                
                //                self.allImages = DataBaseHelper().fetchAllImagesDataFromDB()
                //                print("Time taken to Predict:-",self.seconds)
                
                //                self.faceData = DataBaseHelper().fetchDataFromDB()
                //            }
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Done", message: "Face cropping is done\n device ID:- \(UserDefaults.standard.value(forKey: "UUID"))", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                    self.getCurrentViewController()?.present(alert, animated: true, completion: nil)
                    if SMLIsUploadStatusChecked && UserDefaults.standard.bool(forKey: "isRegularLoad"){
                        SMLIsUploadStatusChecked = false
                        if !(self.getCurrentViewController() is CameraViewController || self.getCurrentViewController() is ImagePreviewViewController) {
                            DispatchQueue.global(qos: .background).async{
                                self.checkStatus()
                            }
                            self.predictionQueue.cancelAllOperations()
                        }
                    }
                }
//            }
        })

        
        //        DispatchQueue.global().async(execute: QUEUEFORAI!)
        
        //        })
        predictionQueue.addOperation(operation)
        }
        
        else if SMLIsUploadStatusChecked && UserDefaults.standard.bool(forKey: "isRegularLoad"){
            SMLIsUploadStatusChecked = false
            if !(self.getCurrentViewController() is CameraViewController || self.getCurrentViewController() is ImagePreviewViewController) {
                DispatchQueue.global(qos: .background).async{
                    self.checkStatus()
                }
                self.predictionQueue.cancelAllOperations()
            }
        }
    }
    
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
//            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    
    func getCurrentViewController() -> UIViewController? {
        
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                
//                customLoader.hideActivityIndicator()
                currentController = currentController.presentedViewController
                
            }
//            customLoader.hideActivityIndicator()
            return currentController
        }
//        customLoader.hideActivityIndicator()
        return nil
    }
    
    func lookUpCurrentLocation(location: CLLocation, completionHandler: @escaping (CLPlacemark?)
        -> Void ) {
        // Use the last reported location.
        let geocoder = CLGeocoder()
        
        // Look up the location and pass it to the completion handler
        geocoder.reverseGeocodeLocation(location,
                                        completionHandler: { (placemarks, error) in
                                            if error == nil {
                                                let firstLocation = placemarks?[0]
                                                completionHandler(firstLocation)
                                            }
                                            else {
                                                // An error occurred during geocoding.
                                                completionHandler(nil)
                                            }
        })
    }
    
    //Detecting faces in images and pass to crop faces method
    func detectingFaces(asset: PHAsset, image: UIImage, imageIndex: Int){
        let faceDetectionRequest = VNDetectFaceRectanglesRequest { (request, error) in
            guard let observations = request.results as? [VNFaceObservation]
                else {
                    print("unexpected result type from VNFaceObservation")
                    return
            }
            if observations.count != 0 {
                //                self.dbHelper.updateFacesCount(assetId: asset.localIdentifier, count: observations.count)
                // get faces cropped from image
                autoreleasepool{
                self.cropFaces(image: image, completionHandler: { (imageData) in
                    UserDefaults.standard.set(imageIndex, forKey: "recentImageWithFace")
                    for image in 0..<imageData.count {
                        let assetId = asset.localIdentifier + "_\(image)"
                        let base64String = self.convertImageToBase64(image: imageData[image])
                        if !DataBaseHelper().checkAssetIdAvailabilityFaceDetection(assetID: assetId) {
                            DataBaseHelper().insertAssetId(assetId: assetId, fromAssetId: asset.localIdentifier, imageData: base64String)
                            
                        }
                        DataBaseHelper().updateFacesDetectionStatus(assetId: assetId, status: 1)
                        //                        self.getVectors(image: imageData[image], conatinerImageId: asset.localIdentifier, id: image)
                    }
                })
                }
            }
        }
        
        let handler = VNImageRequestHandler(cgImage: image.cgImage!, options: [:])
        do {
            try handler.perform([faceDetectionRequest])
        } catch {
            print("Failed to perform classification.\n\(error.localizedDescription)")
        }
    }
    
    // Cropping faces from image
    func cropFaces(image: UIImage, completionHandler: @escaping([UIImage]) -> ()) {
        //        DispatchQueue.global(qos: .background).async {
        //            autoreleasepool {
        var imageArray: [UIImage] = []
        image.face.crop { result in
            //                    autoreleasepool {
            switch result {
            case .success(let faces):
                //print("Found")
                imageArray = faces
                completionHandler(imageArray)
            case .notFound:
                print("not found")
            //                self.showAlert("couldn't find any face")
            case .failure(let error):
                print("failure:- \(error.localizedDescription)")
                //                self.showAlert(error.localizedDescription)
            }
            //                    }
        }
        //            }
        //        }
    }
    
    @objc func runModel(onPixelBuffer pixelBuffer: CVPixelBuffer) -> String {
        var objectsArray = [String]()
        
        //        DispatchQueue.main.async {
        self.result = self.modelDataHandler?.runModel(onFrame: pixelBuffer)
        
        if let displayResult = self.result {
            for object in displayResult.inferences {
                if !objectsArray.contains(object.className) && object.confidence > 0.3 {
                    objectsArray.append(object.className.uppercased())
                }
            }
        }
        
        //        }
        
        return objectsArray.joined(separator: ", ")
        
        //        DispatchQueue.main.async {
        //            // Draws the bounding boxes and displays class names and confidence scores.
        ////            self.drawAfterPerformingCalculations(onInferences: displayResult.inferences, withImageSize: CGSize(width: CGFloat(width), height: CGFloat(height)))
        //        }
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.uncompressedPNGData
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
    func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    func hitCheckStatusWithTimer() {
        self.predictionQueue.cancelAllOperations()
        self.perform(#selector(self.checkStatus), with: nil, afterDelay: 60)
    }
    
    //MARK:- API's
    //uploading images ie. cropped faces chunkwise(100)
    func uplaodImages(lastID: Int, limit: Int) {
        autoreleasepool {
//            var imageData = [String: Any]()
            var count = lastID
            //            imageData = DataBaseHelper().fetchOnlyImageDataFromDB(offset: lastID, limit: 6)
            if lastID == -1 {
                //                imageData = DataBaseHelper().fetchOnlyImageDataFromDB(offset: 0, limit: limit)
                count = 0
            }
            //            else {
//            imageData = DataBaseHelper().fetchOnlyImageDataFromDB(offset: lastID, limit: limit)
            //                //                imageData = DataBaseHelper().fetchOnlyImageDataFromDB(offset: DataBaseHelper().fetchPerticularP_key(assetId: lastID), limit: 100)
            //            }
            var hasMore = 1
            if DataBaseHelper().fetchOnlyImageDataFromDB(offset: lastID, limit: limit).count % 50 != 0 {
                hasMore = 0
            }
            if !DataBaseHelper().fetchOnlyImageDataFromDB(offset: lastID, limit: limit).isEmpty {
                if SMLIsImageUploaded {
                    SMLIsImageUploaded = false
                    let parameter = ["payload": DataBaseHelper().fetchOnlyImageDataFromDB(offset: lastID, limit: limit),
                                     "device_id": UserDefaults.standard.value(forKey: "UUID"),
                                     "last_id": DataBaseHelper().fetchAssetId(p_key: limit + lastID),
                                     "has_more" : hasMore,
                                     "deleted_items" : DataBaseHelper().fetchDeletedImages()] as [String : Any]
                    ViewControllerNetwork.uploadImages(imagesData: parameter, hasMore: hasMore, lastID: count + DataBaseHelper().fetchOnlyImageDataFromDB(offset: lastID, limit: limit).count)
                }
            }
        }
    }
    
    // checking status of uploading it returns last uploaded image data still count further it'll change
    @objc func checkStatus() {
        self.predictionQueue.cancelAllOperations()
        //        autoreleasepool {
        var param = [String: String]()
        param["device_id"] = UserDefaults.standard.value(forKey: "UUID") as? String
        if let uuid = UserDefaults.standard.value(forKey: "initialInstall") as? String {
            param["initial_install"] = uuid
        }
        else {
            param["initial_install"] = "True"
        }
        
        if WebserviceModelClass().isInternetAvailable() {
            WebserviceModelClass().postDataTo(module: "Upload Status", subUrl: "uploadStatus", parameter: param, completionHandler: { (success, message, data) in
                if success {
//                    SMLIsUploadStatusChecked = true
                    let asset = data["payload"] as! String
                    if UserDefaults.standard.value(forKey: "initialInstall") == nil {
                        UserDefaults.standard.setValue("false", forKey: "initialInstall")
                    }
                    if asset == "-1" {
                        self.uplaodImages(lastID: 0, limit: 50)
                        //                        UserDefaults.standard.set(-1, forKey: "LastID")
                    }
                    else if DataBaseHelper().fetchLastAssetId() != asset {
                        self.uplaodImages(lastID: DataBaseHelper().fetchPerticularP_key(assetId: asset.replacingOccurrences(of: "#", with: "/")), limit: 50)
                    }
                    else if DataBaseHelper().fetchLastAssetId() == asset {
                        if self.usersData.isEmpty {
                            SMLIsUploadStatusChecked = !SMLIsUploadStatusChecked
                            ViewControllerNetwork.getResult(completionHandler: { (data) in
                                self.saveClustering(data: data)
                            })
                        }
                    }
                }
                else {
                    WebserviceModelClass().showAlertOn(error: message)
                }
            })
        }
        else {
//            let vc = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
//            appDelegate().getCurrentViewController()?.present(vc, animated: true, completion: nil)
//            let alert = UIAlertController(title: "Error", message: "No internet access", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
//                PredictionController().checkStatus()
//            }))
//            appDelegate().getCurrentViewController()?.present(alert, animated: true, completion: nil)
            //            WebserviceModelClass().showAlertOn(error: "No internet access")
            return
        }
        //        }
    }
    
    func saveClustering(data: [[String]]) {
        DispatchQueue.global(qos: .background).async {
            for res in 0..<data.count {
                var fromAssetId = ""
                for i in 0..<data[res].count {
                    if i != data[res].endIndex - 1 {
                        let resString = data[res][i].replacingOccurrences(of: "#", with: "/")
                        let string = resString.components(separatedBy: "_")
                        fromAssetId = string[0]
                        let imageData = DataBaseHelper().fetchDataFromDB(assetId: fromAssetId)
                        
                        //                    let image = self.convertBase64ToImage(imageString: imageData)
                        //                    print(string[0])
                        //                    DataBaseHelper().insertClusteredData(assetId: data[res][i], fromAssetId: string[0], ImageData: imageData, ofClass: "\(res)", vetcor: [])
                        DataBaseHelper().insertClusteredData(assetId: data[res][i], fromAssetId: string[0], ImageData: "", ofClass: "\(res)", vetcor: [], object: imageData["object"]!, location: [imageData["city"]!,imageData["state"]!,imageData["country"]!])
                    }
                    if i == data[res].endIndex - 1 {
                        DataBaseHelper().insertUserData(imageData: data[res][i], ofClass: "\(res)", assetId: fromAssetId)
                    }
                }
            }
            
            var users = [AnyHashable: Any]()
            users["users"] = DataBaseHelper().fetchUsersDataFromDB()
            //        if self.usersData.isEmpty {
            ////            self.userCollectionviewHeight.constant = 0
            //        }
            //        else {
            //            self.userCollectionviewHeight.constant = 100
            //            self.customGalleryCollectionView.reloadData()
            //        }
            //        self.collectionViewUsers.reloadData()
            UserDefaults.standard.set(true, forKey: "isRegularLoad")
            UserDefaults.standard.set(true, forKey: "isDataClustered")
            NotificationCenter.default.post(name: NSNotification.Name("updateUsersTableData"), object: nil, userInfo: users)
//            if !SMLIsResultReady {
//                appDelegate().scheduleNotification(notificationType: "Detection is completed now you can use AI feature.")
//            }
        }
        
        
        //        NotificationCenter.default.post(name: NSNotification.Name("updateUsersTableData"), object: nil)
        //        self.timerForResponse.invalidate()
    }
    
}
