//
//  AIModel.swift
//  Kodak Smile
//
//  Created by MAXIMESS152 on 09/04/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

class AIModel: NSObject {
    
    func stopAIOperation(flag: ObjCBool) {
        shouldStopAIOperation = flag
    }
    
    func stopAIThread() {
        PredictionController().predictionQueue.cancelAllOperations()
//        DispatchQueue.global().async {
//            QUEUEFORAI.cancelAllOperations()
//        }
        
    }

}
