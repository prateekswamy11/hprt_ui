
//
//  ViewControllerNetwork.swift
//  FaceAndObjectRecognitionInImage
//
//  Created by MAXIMESS152 on 06/02/19.
//  Copyright © 2019 MAXIMESS152. All rights reserved.
//

import UIKit

class ViewControllerNetwork: NSObject {

    private static func showAlert(error: String, viewController: UIViewController) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
        
    }
    
    static func getClusteredData(imageData: [String: Any], completionHandler: @escaping ([[String]]) -> ()) {
        if WebserviceModelClass().isInternetAvailable() {
//            print(imageData)
            WebserviceModelClass().postDataTo(module: "PostVectorData", subUrl: "", parameter: imageData) { (success, message, data) in
                if success {
                    let response = data["payload"] as! [[String]]
//                    print(response)
                    completionHandler(response)
                }
                else {
                    WebserviceModelClass().showAlertOn(error: message)
                }
            }
        }
        else {
            let alert = UIAlertController(title: "Error", message: "No internet access", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                PredictionController().checkStatus()
            }))
            appDelegate().getCurrentViewController()?.present(alert, animated: true, completion: nil)
//            WebserviceModelClass().showAlertOn(error: "No internet access")
            return
        }
    }
    
    static func uploadImages(imagesData: [String: Any], hasMore: Int, lastID: Int) {
        if WebserviceModelClass().isInternetAvailable() {
            print("Last id-------\(lastID)")
            WebserviceModelClass().postDataTo(module: "Upload Images Data", subUrl: "uploadPics", parameter: imagesData) { (success, message, data) in
                if success {
                    SMLIsUploadStatusChecked = true
                    SMLIsImageUploaded = true
                    print(data)
                    if hasMore == 1 {
                        if SMLIsUploadStatusChecked {
                            SMLIsUploadStatusChecked = false
                            PredictionController().checkStatus()
                        }
                        
                    }
                    else {
                        PredictionController().hitCheckStatusWithTimer()
                        NotificationCenter.default.post(name: NSNotification.Name("UpdateUserData"), object: nil)

                    }
                }
                else {
                    SMLIsUploadStatusChecked = true
                    PredictionController().hitCheckStatusWithTimer()
                }
            }
        }
        else {
            WebserviceModelClass().showAlertOn(error: "No internet access")
            return
        }
    }
    
    static func getResult(completionHandler: @escaping ([[String]]) -> ()) {
        if WebserviceModelClass().isInternetAvailable() {
            WebserviceModelClass().postDataTo(module: "Get Result", subUrl: "getResults", parameter: ["device_id" : UserDefaults.standard.value(forKey: "UUID")]) { (success, message, data) in
                if success {
                    if message == "Success" {
                        completionHandler(data["payload"] as! [[String]])
                    }
                    else {
                        if SMLIsUploadStatusChecked {
                            SMLIsUploadStatusChecked = false
                            PredictionController().hitCheckStatusWithTimer()
                        }
                    }
                }
                else {
                    WebserviceModelClass().showAlertOn(error: message)
                }
            }
        }
        else {
            let alert = UIAlertController(title: "Error", message: "No internet access", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                PredictionController().checkStatus()
            }))
            appDelegate().getCurrentViewController()?.present(alert, animated: true, completion: nil)
//            WebserviceModelClass().showAlertOn(error: "No internet access")
            return
        }
    }
    
    @objc func hitCheckStatusAPI() {
        if SMLIsUploadStatusChecked {
            SMLIsUploadStatusChecked = false
            PredictionController().checkStatus()
        }
    }
}
