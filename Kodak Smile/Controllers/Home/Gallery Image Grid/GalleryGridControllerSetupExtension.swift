//
//  GalleryGridControllerSetupExtension.swift
//  Kodak Smile
//
//  Created by maximess152 on 29/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation
import Photos

extension GalleryImageGridViewController {
    
    func initialSetup() {
        calledWhenNavigatingFromImages()
        
        if #available(iOS 11.0, *) {
            collectionViewGrid?.contentInsetAdjustmentBehavior = .always
        } else {
            // Fallback on earlier versions
            
        }
        
        // Allow to download image from iCloud
        cellImgoptions.isSynchronous = true
        cellImgoptions.isNetworkAccessAllowed = true
        
        // Determine the size of the thumbnails to request from the PHCachingImageManager
        thumbnailSize = CGSize(width:100, height: 100)
        
        // Adjusting the size of lblAlbumNameLower label according to content
        lblAlbumNameLower.adjustsFontSizeToFitWidth = true
        lblAlbumName.adjustsFontSizeToFitWidth = false
        lblAlbumName.lineBreakMode = .byTruncatingTail
        lblAlbumName.alpha = 0
        self.btnSelect.setTitle("Select".localisedString(), for: .normal)
        self.lblSearch.text = "Search by".localisedString()
        self.txtSearch.placeholder = "Places, Objects…".localisedString()
        self.btnSearch.setTitle("Search".localisedString(), for: .normal)
        self.btnPrint.setTitle("Print".localisedString(), for: .normal)
        self.btnCollage.setTitle("Collage".localisedString(), for: .normal)
        self.btnShare.setTitle("Share".localisedString(), for: .normal)
    }
    
    func calledWhenNavigatingFromImages() {
        if self.imagesFrom == "Local" {
            
            if isVideos == false
            {
                self.headerName = collectionItem.localizedTitle!
            }
            else
            {
                self.headerName = "Videos".localisedString()
                self.btnSelect.isHidden = true
            }
            
            lblAlbumName.text = headerName.localisedString()
            lblAlbumNameLower.text = headerName.localisedString()
        }
        else {
            lblAlbumName.text = self.headerName.localisedString()
            lblAlbumNameLower.text = self.headerName.localisedString()
        }
    }
    
    func setTagToTextField(){
        tagsField.frame = tagsView.bounds
        tagsField.frame.size.width = tagsView.frame.width - tagsField.frame.height - 20
        tagsView.addSubview(tagsField)
        
        //Set tag to identify field in textField delegate methods
        tagsView.tag = 878
        tagsField.tag = 878
        
        //tagsField.translatesAutoresizingMaskIntoConstraints = false
        //tagsField.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        //        tagsField.cornerRadius = 5.0
        tagsField.spaceBetweenLines = 10
        tagsField.spaceBetweenTags = 10
        
        //tagsField.numberOfLines = 3
        //tagsField.maxHeight = 100.0
        
        tagsField.layoutMargins = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        tagsField.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0) //old padding
        
        
        //        tagsField.placeholder = "Places, Objects...".localisedString()
        tagsField.placeholder = "Location...".localisedString()
        tagsField.placeholderColor = UIColor.lightGray
        //        tagsField.placeholderAlwaysVisible = true
        tagsField.backgroundColor = .clear
        tagsField.returnKeyType = .next
        tagsField.delimiter = ""
        tagsField.removeTags()
        tagsField.numberOfLines = 2
        tagsField.enableScrolling = true
        tagsField.textDelegate = self
        tagsField.acceptTagOption = .return
        
        tagsField.font = .systemFont(ofSize: 12.0)
        tagsField.tintColor = UIColor.darkGray
        tagsField.textColor = .white
        tagsField.fieldTextColor = .white
        
        tagsField.selectedColor = UIColor(red: 0, green: 122, blue: 255)
        tagsField.selectedTextColor = .white
        tagsField.delimiter = ""
        tagsField.isDelimiterVisible = true
        tagsField.returnKeyType = .next
        tagsField.acceptCurrentTextAsTag()
        
        tagsField.autocapitalizationType = .words
        textFieldEvents()
    }
    
    func checkBottomOptions()
    {
        if self.isSelectOptionsActive == true
        {
            let countOfSelectedImg = self.arraySelectedImgURL.count
            
            if countOfSelectedImg == 0
            {
                //                self.btnPrint.isEnabled = false
                self.btnPrint.alpha = 0.5
                //                self.btnCollage.isEnabled = false
                self.btnCollage.alpha = 0.5
                //                self.btnShare.isEnabled = false
                self.btnShare.alpha = 0.5
            }
            else
            {
                //                self.btnPrint.isEnabled = true
                self.btnPrint.alpha = 1.0
                //                self.btnCollage.isEnabled = true
                self.btnCollage.alpha = 1.0
                //                self.btnShare.isEnabled = true
                self.btnShare.alpha = 1.0
                
                if countOfSelectedImg > 10
                {
                    //                    self.btnShare.isEnabled = false
                    self.btnShare.alpha = 0.5
                    self.btnPrint.alpha = 0.5
                }
                if countOfSelectedImg > 4
                {
                    //                    self.btnCollage.isEnabled = false
                    self.btnCollage.alpha = 0.5
                }
                //                if countOfSelectedImg != 1
                //                {
                ////                    self.btnPrint.isEnabled = false
                //                    self.btnPrint.alpha = 0.5
                //                }
            }
        }
    }
    
    func checkSelectOptions()
    {
        // If select button is not clicked
        if self.isSelectOptionsActive == false
        {
            self.viewBottomOptions.isHidden = true
            self.btnSelect.setTitle("select".localisedString(), for: .normal)
            self.lblAlbumName.alpha = 0
            self.lblAlbumName.text = "".localisedString()
        }
        else if self.isSelectOptionsActive == true
        {
            self.viewBottomOptions.isHidden = false
            self.btnSelect.setTitle("cancel".localisedString(), for: .normal)
            self.lblAlbumName.alpha = 1.0
            self.lblAlbumName.text = "Select Items".localisedString()
        }
        if !arraySelectedImgURL.isEmpty {
            self.arraySelectedImgURL.removeAll()
            self.imageDataArray.removeAll()
            self.selectedCellsArray.removeAll()
            self.collectionViewGrid.reloadData()
        }
        self.updateHeaderText()
        if self.viewBottomOptions.isHidden {
            self.constraintCollectionViewGridBottom.constant = 0
        } else {
            self.constraintCollectionViewGridBottom.constant = -50
        }
    }
    
    func updateHeaderText() {
        if self.isSelectOptionsActive == true  && scroll == "down"
        {
            self.lblAlbumName.alpha = 0.0
            self.lblAlbumNameLower.alpha = 1.0
            self.lblAlbumNameLower.text = "Select Items".localisedString()
        }
        else if self.isSelectOptionsActive == false  && scroll == "up"
        {
            self.lblAlbumName.alpha = 1.0
            self.lblAlbumName.text = self.headerName
        }
        else if self.isSelectOptionsActive == true  && scroll == "up"
        {
            self.lblAlbumName.alpha = 1.0
            self.lblAlbumName.text = "Select Items".localisedString()
        } else {
            self.lblAlbumName.alpha = 0.0
            if self.isSelectOptionsActive == true {
                self.lblAlbumNameLower.text = "Select Items".localisedString()
            } else {
                self.lblAlbumNameLower.text = self.headerName
            }
        }
    }
    
    func getAssetsDatewise()
    {
        var yourArray = [String : String]() // = [String : [PHAsset]]()
        imagesArray.enumerateObjects({(object: AnyObject!,
            count: Int,
            stop: UnsafeMutablePointer<ObjCBool>) in
            if object is PHAsset {
                let asset = object as! PHAsset
                print(asset)
                yourArray["\(asset.creationDate!)"]?.append(asset.localIdentifier)
                //                var dateArray = yourArray["\(asset.creationDate!)"]
                //                dateArray?.append(asset)
                //                yourArray["\(asset.creationDate!)"] = dateArray
                //
                //yourArray.append(asset)
            }
        })
    }
    
    func setupAI() {
        self.selectedIndex = 1111
        //        self.allImages = DataBaseHelper().fetchAllImagesDataFromDB()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateUserDataForAI), name: NSNotification.Name("updateUsersTableData"), object: nil)
        self.usersImageDataForAI = DataBaseHelper().fetchUsersDataFromDB()
        //        if usersImageDataForAI.isEmpty {
        //            self.heightUsersCOllectionView.constant = 0
        //            self.searchViewHeight.constant -= 80
        //        }
        if self.headerName != "Camera Roll".localisedString() && self.headerName != "All Photos".localisedString() {
            self.searchViewHeight.constant = 0
        }
        else {
            //            if self.imagesArray.count == DataBaseHelper().fetchAllImagesDataFromDB().count && !usersImageDataForAI.isEmpty {
            //                self.searchViewHeight.constant = 182
            //            }
            if UserDefaults.standard.bool(forKey: "isObjectDetected") {
                self.searchViewHeight.constant = 35
            }
            else if UserDefaults.standard.bool(forKey: "isRegularLoad") {
                self.searchViewHeight.constant = 182
            }
            else {
                self.searchViewHeight.constant = 0
            }
            self.txtSearch.attributedPlaceholder = NSAttributedString(string: "Places, Objects",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        }
    }
    
    func checkPhotoLibraryPermission() -> Bool {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            //print("authorized")
            
            return true
        case .denied, .restricted, .notDetermined :
            //handle denied status
            print("denied")
            
            //            let dialog = UIAlertController(title: "Unable To Access The Photos".localisedString(), message: "To enable access, go to Settings > Privacy > Photos and turn on Photos access for this app.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
            //
            //            let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
            //
            //            dialog.addAction(okAction)
            //            self.present(dialog, animated:true, completion:nil)
            //
            return false
        }
    }
    
    func getImageFromAsset(asset : PHAsset) -> UIImage
    {
        //let imageSize = CGSize(width: asset.pixelWidth,
        //  height: asset.pixelHeight)
        
        let options = PHImageRequestOptions()
        
        //Photos automatically provides one or more results in order to balance image quality and responsiveness.
        options.deliveryMode = .opportunistic
        
        //Request the original, highest-fidelity version of the image asset.
        //options.version = .original
        
        //Set it to true to block the calling thread until either the requested image is ready or an error occurs, at which time Photos calls your result handler.
        options.isSynchronous = true
        
        // Allow to download image from iCloud
        options.isNetworkAccessAllowed = true
        
        var imgToSend = UIImage()
        let imagemhng = PHImageManager()
        
        var imgSize = 1000.0
        var loopCount = 5
        
        imagemhng.requestImage(for: asset,
                               targetSize: PHImageManagerMaximumSize,
                               contentMode: PHImageContentMode.aspectFit,
                               options: options,
                               resultHandler:
            { (image, info ) -> Void in
                
                
                //Solve crashlytics crash
                
                guard let dict : Dictionary = info else{
                    if let receivedImg = image
                    {
                        /* The image is now available to us */
                        imgToSend = receivedImg
                    }
                    else{
                        DispatchQueue.main.async {
                            //                            self.stopLoader()
                        }
                        
                        let dialog = UIAlertController(title: "Cloud Error".localisedString(), message: "This photo could not be downloaded from iCloud. Please connect to internet.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                        
                        dialog.addAction(okAction)
                        self.present(dialog, animated:true, completion:nil)
                    }
                    return
                }
                
                
                if let isIcloudIMg = dict["PHImageResultIsInCloudKey"] as? Int
                {
                    if (isIcloudIMg == 0) {
                        
                        if let receivedImg = image
                        {
                            /* The image is now available to us */
                            imgToSend = receivedImg
                        }
                    }
                    else{
                        
                        if let receivedImg = image
                        {
                            /* The image is now available to us */
                            imgToSend = receivedImg
                        }
                        else{
                            DispatchQueue.main.async {
                                //                                self.stopLoader()
                            }
                            let dialog = UIAlertController(title: "Cloud Error".localisedString(), message: "This photo could not be downloaded from iCloud. Please connect to internet.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
                            
                            let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                            
                            dialog.addAction(okAction)
                            self.present(dialog, animated:true, completion:nil)
                        }
                    }
                }
                else{
                    
                    if let receivedImg = image
                    {
                        /* The image is now available to us */
                        imgToSend = receivedImg
                    }
                    else{
                        DispatchQueue.main.async {
                            //                            self.stopLoader()
                        }
                        let dialog = UIAlertController(title: "Cloud Error".localisedString(), message: "This photo could not be downloaded from iCloud. Please connect to internet.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                        
                        dialog.addAction(okAction)
                        self.present(dialog, animated:true, completion:nil)
                    }
                }
                
                //                var dict : Dictionary = info!
                //                let isIcloudIMg = dict["PHImageResultIsInCloudKey"] as! Int
                //                if (isIcloudIMg == 0) {
                //
                //                    if let receivedImg = image
                //                    {
                //                        /* The image is now available to us */
                //                        imgToSend = receivedImg
                //                    }
                //                }
                //                else{
                //
                //                    if let receivedImg = image
                //                    {
                //                        /* The image is now available to us */
                //                        imgToSend = receivedImg
                //                    }
                //                    else{
                //                        let dialog = UIAlertController(title: "Cloud Error", message: "This photo could not be downloaded from iCloud. Please connect to internet.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
                //
                //                        let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                //
                //                        dialog.addAction(okAction)
                //                        self.present(dialog, animated:true, completion:nil)
                //                    }
                //                }
                //
                
                
        })
        
        
        return imgToSend
    }
    
    func getImagesFromAssets() -> [UIImage]
    {
        let options = PHImageRequestOptions()
        
        //Photos automatically provides one or more results in order to balance image quality and responsiveness.
        options.deliveryMode = .fastFormat
        
        //Request the original, highest-fidelity version of the image asset.
        //options.version = .original
        
        //Set it to true to block the calling thread until either the requested image is ready or an error occurs, at which time Photos calls your result handler.
        options.isSynchronous = true
        
        // Allow to download image from iCloud
        options.isNetworkAccessAllowed = true
        
        let imagemhng = PHImageManager()
        
        let photoOptions = PHFetchOptions()
        var imgArray = [UIImage]()
        
        // Added following logic by Pratiksha date 12 Mar 2019
        // Get images in for loop as the images we are gettings from selAssetsIdentifiersArray's URLs, are automatically sorted by date
        for url in arraySelectedImgURL
        {
            let assetsFetchResult = PHAsset.fetchAssets(withLocalIdentifiers: [url], options: photoOptions)
            
            assetsFetchResult.enumerateObjects{(object: AnyObject!,
                count: Int,
                stop: UnsafeMutablePointer<ObjCBool>) in
                
                if object is PHAsset{
                    let asset = object as! PHAsset
                    let imageSize = CGSize(width: asset.pixelWidth,
                                           height: asset.pixelHeight)
                    
                    let options = PHImageRequestOptions()
                    options.deliveryMode = .opportunistic
                    options.isSynchronous = true
                    options.isNetworkAccessAllowed = true
                    imagemhng.requestImage(for: asset,
                                           targetSize: imageSize,
                                           contentMode: .aspectFill,
                                           options: options,
                                           resultHandler:
                        {
                            (image, info) -> Void in
                            if let receivedImg = image
                            {
                                /* The image is now available to us */
                                imgArray.append(receivedImg)
                            }
                            else
                            {
                                var imgSize = 1000.0
                                var loopCount = 5
                                for _ in 0..<loopCount
                                {
                                    imagemhng.requestImage(for: asset,
                                                           targetSize: CGSize(width: imgSize, height: imgSize),
                                                           contentMode: PHImageContentMode.aspectFit,
                                                           options: options,
                                                           resultHandler:
                                        { image, _ in
                                            
                                            if let receivedImg = image
                                            {
                                                /* The image is now available to us */
                                                imgArray.append(receivedImg)
                                                loopCount = 0
                                            }
                                    })
                                    imgSize = imgSize - 200
                                    
                                }
                            }
                    })
                }
            }
        }
        
        return imgArray
    }
    
    func getImagesFromUrls() -> [UIImage]
    {
        var imgArray = [UIImage]()
        
        for stringUrl in arraySelectedImgURL
        {
            let url = URL(string:stringUrl)
            if let data = try? Data(contentsOf: url!)
            {
                if let image: UIImage = UIImage(data: data)
                {
                    imgArray.append(image)
                }
            }
        }
        
        return imgArray
    }
    
    func getSelectedImageAsset(index : Int) -> PHAsset
    {
        var asset = PHAsset()
        asset = imagesArray.object(at: imagesArray.count - (index + 1))
        
        return asset
    }
    
    func stopLoader(){
        
        customLoader.hideActivityIndicator()
    }
    
    func startLoader()  {
        customLoader.showActivityIndicator(viewController: self.view)
    }
    
    @objc func updateUserDataForAI(notification: Notification) {
        //        print(notification.userInfo)
        //        self.usersImageDataForAI = [notification.userInfo?.values as! String]
        
    }
    
    //Dismiss keyboard on outside touch even it's on scroll view or table view or UIView.
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardq))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboardq() {
        //Remove all gesture added  on the UIView
        if let recognizers = self.view.gestureRecognizers{
            for recognizer in recognizers{
                self.view.removeGestureRecognizer(recognizer)
            }
        }
        view.endEditing(true)
    }
}
