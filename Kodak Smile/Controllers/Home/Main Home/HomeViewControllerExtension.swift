//
//  HomeViewControllerExtension.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 28/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import GoogleSignIn
import GTMSessionFetcher
import GoogleAPIClientForREST
import CopilotAPIAccess

class HomeViewControllerExtension: NSObject {

}

extension HomeViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let accessToken = user.authentication.accessToken
            customLoader.showActivityIndicator(viewController: self.view)
            //self.view.isUserInteractionEnabled = false
            UserDefaults.standard.setValue(true, forKey: "isLoggedInToGoogleKodak")
            let fullName = user.profile.name
            UserDefaults.standard.setValue(fullName, forKey: "googleUserName")
            UserDefaults.standard.setValue(userId, forKey: "googleUserId")
            UserDefaults.standard.setValue(user.authentication.refreshToken, forKey: "googleRefreshToken")
            UserDefaults.standard.setValue(user.authentication.accessToken, forKey: "googleAccessToken")
            NotificationCenter.default.post(name: NSNotification.Name("reloadSocialMedia"), object: nil)
            UserDefaults.standard.synchronize()
//            customLoader.hideActivityIndicator()
            // Fetch google photos
            let strUrl = "https://photoslibrary.googleapis.com/v1/mediaItems?alt=json&pageSize=100&access_token=\(accessToken!)"
            
            GoogleHelper().getAlbums(accessToken: accessToken!, strUrl: strUrl)
        }
        else
        {
            print("\(error.localizedDescription)")
        }
    }
    
    func configureGooglePhotos()
    {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().scopes = ["https://www.googleapis.com/auth/photoslibrary"]
    }
    
    @objc func reloadForFacebook()
    {
        let albumConnectedAnalyticsEvent = AlbumConnectedAnalyticsEvent(albumType: AnalyticsValue.albumType.facebook.rawValue)
        Copilot.instance.report.log(event: albumConnectedAnalyticsEvent)
        if UserDefaults.standard.value(forKey: "fetchFbAlbum") as? Bool == false {
            return
        }
        let fbAlbums = DBLayer().fetchFacebookAlbumFromDB()
        self.savedFbPhotosArray.removeAll()
        for albums in fbAlbums {
            for photos in albums.fbAlbumPhotoLinkArray {
                self.savedFbPhotosArray.append(photos)
            }
        }
        self.folderCollectionView.reloadData()
    }
    
    @objc func reloadForInstagram()
    {
        let albumConnectedAnalyticsEvent = AlbumConnectedAnalyticsEvent(albumType: AnalyticsValue.albumType.instagram.rawValue)
        Copilot.instance.report.log(event: albumConnectedAnalyticsEvent)
        DispatchQueue.main.async(execute:
            {
                customLoader.showActivityIndicator(viewController: self.view)
                self.instagramPhotosArray = DBLayer().fetchInstagramPhotosFromDB()
//                self.folderCollectionView.reloadData()
                customLoader.hideActivityIndicator()
                self.folderCollectionView.reloadData()
        })
        
    }
    
    @objc func getDropboxData()
    {
        self.arrDropboxImages = DBLayer().fetchDropboxPhotosNamesFromDB().unique()
        self.dropboxImages = DBLayer().fetchDropboxPhotosFromDB()
//        if data.indices.contains(0) {
//            self.dropboxImages = UIImageJPEGRepresentation(data[0], 1)!
//        }
        self.folderCollectionView.reloadData()
    }
    
    @objc func reloadForDropbox()
    {
        self.arrDropboxImages = DBLayer().fetchDropboxPhotosNamesFromDB().unique()
        self.dropboxImages = DBLayer().fetchDropboxPhotosFromDB()
        customLoader.hideActivityIndicator()
        self.folderCollectionView.reloadData()
    }
    
    
    @objc func reloadForGoogle()
    {
        let albumConnectedAnalyticsEvent = AlbumConnectedAnalyticsEvent(albumType: AnalyticsValue.albumType.google.rawValue)
        Copilot.instance.report.log(event: albumConnectedAnalyticsEvent)
        DispatchQueue.main.async(execute:
            {
                customLoader.showActivityIndicator(viewController: self.view)
                self.savedGooglePhotosArray.removeAll()
                self.savedGooglePhotosArray = DBLayer().fetchGooglePhotosFromDB()
                customLoader.hideActivityIndicator()
                self.folderCollectionView.reloadData()
        })
        
    }
    
    @objc func googleImagesFetched(noti : Notification)
    {
        DispatchQueue.main.async(execute:
            {
                if let arrayAlbum = noti.object as? [GooglePhotosPara]
                {
                    self.savedGooglePhotosArray.removeAll()
                    for album in arrayAlbum {
                        for photos in album.googleAlbumPhotoLinkArray {
                            self.savedGooglePhotosArray.append(photos)
                        }
                    }
                }
                self.folderCollectionView.reloadData()
                customLoader.hideActivityIndicator()
        })
    }
    
    @objc func userLoggedOut(_ notification: NSNotification)
    {
        let account = notification.object as! String
        switch account {
        case "Google":
            print("Google")
            self.reloadForGoogle()
        case "Facebook":
            print("Facebook")
            self.reloadForFacebook()
        case "Instagram":
            print("Instagram")
            self.reloadForInstagram()
        default:
            print("Default")
        }
    }
}

extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: [Iterator.Element: Bool] = [:]
        return self.filter { seen.updateValue(true, forKey: $0) == nil }
    }
}
