//
//  HomeViewControllerCollectionViewDatasource.swift
//  Polaroid MINT
//
//  Created by maximess142 on 19/07/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation
import UIKit
import Photos
import AlamofireImage
import GoogleSignIn
import GTMSessionFetcher
import GoogleAPIClientForREST
import Alamofire
import CopilotAPIAccess

extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource, LiquidLayoutDelegate
{
    
    //MARK:- Functions
    
    func socialMediaNormalCell(indexPath: IndexPath, collectionView: UICollectionView) -> HomeSocialMediaCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeSocialMediaCollectionViewCell", for: indexPath) as! HomeSocialMediaCollectionViewCell
        if self.socialMediaAlbumNames.indices.contains(indexPath.item - albumsArray.count) &&  self.socialMediaLogo.indices.contains(indexPath.item - albumsArray.count) {
            cell.lblAlbumName.text = self.socialMediaAlbumNames[indexPath.item - albumsArray.count]
            cell.lblAddFolder.text = "Add Folder".localisedString()
            cell.imgViewLogo.image = UIImage(named: self.socialMediaLogo[indexPath.item - albumsArray.count])
        }
        return cell
    }
    
    //MARK:- UICollectionViewDelegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if checkPhotoLibraryPermission()
        {
            return albumsArray.count + 3 // +1 for videos folder
        }
        else
        {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item < albumsArray.count
        {
            isPhotosAllowed = true
                let cell : HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                
                cell.imageViewThumbnailSocial.isHidden = true
                cell.imageViewThumbnail.isHidden = false
                
                var collectionItem = PHAssetCollection()
                        collectionItem = albumsArray[indexPath.item]

                cell.lblAlbumName.text = collectionItem.localizedTitle
                cell.lblAlbumName.adjustsFontSizeToFitWidth = false
                cell.lblAlbumName.lineBreakMode = .byTruncatingTail
                cell.imageViewThumbnail.contentMode = .scaleAspectFill
                
                let fetchOptions = PHFetchOptions()
                //fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                if collectionItem.localizedTitle != "Videos"{
                    fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
                }
                let assetsFetchResult = PHAsset.fetchAssets(in: collectionItem, options: fetchOptions)
                if(assetsFetchResult.count>0)
                {
                    let img:UIImage = self.getAssetThumbnail(asset: assetsFetchResult[assetsFetchResult.count-1], size: cell.imageViewThumbnail.frame.size.height)
                    let count = assetsFetchResult.count
                    cell.lblImgCount.text = "\(count)"
                    cell.imageViewThumbnail.image = img
                    if indexPath.item == 0{
                        print(img)
                        forImagePreview = img
                    }
                    
                    }
                else
                {
                    cell.lblImgCount.text = "0"
                    cell.imageViewThumbnail.image = nil
                }
                
                return cell

        }
        else
        {
            switch(indexPath.item){
            case (albumsArray.count): //For Google photos // +1
                
                let cell : HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                cell.imageViewThumbnail.isHidden = true
                cell.imageViewThumbnailSocial.isHidden = false
                
                if UserDefaults.standard.bool(forKey: "isLoggedInToGoogleKodak") {
                    if self.savedGooglePhotosArray.indices.contains(0) {
                        let url = self.savedGooglePhotosArray[0]
                            cell.imageViewThumbnailSocial.af_setImage(
                            withURL: URL(string:url)!,
                            imageTransition: .crossDissolve(0.2),
                            completion: nil)
                        print("NOBlank")
                    }else{
                         let cell = self.socialMediaNormalCell(indexPath: indexPath, collectionView: collectionView)
                        return cell
                    }
                    
                    cell.lblAlbumName.text = self.socialMediaAlbumNames[0]
                    let count: Int = self.savedGooglePhotosArray.count
                    cell.lblImgCount.text = String(count)
                  
                    return cell
                }
                else {
                    let cell = self.socialMediaNormalCell(indexPath: indexPath, collectionView: collectionView)
                    print("Blank")
                    return cell
                }
            case (albumsArray.count + 1): //For Instagram // +2
                
                let cell : HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                cell.imageViewThumbnail.isHidden = true
                cell.imageViewThumbnailSocial.isHidden = false
                
                if UserDefaults.standard.bool(forKey: "isLoggedInToInstagramKodak")
                {
//                    self.instagramPhotosArray = Array(Set(instagramPhotosArray))
                    if self.instagramPhotosArray.indices.contains(0) {
                        let url = self.instagramPhotosArray[0]
                      
                        cell.imageViewThumbnailSocial.af_setImage(
                            withURL: URL(string:url)!,
                            imageTransition: .crossDissolve(0.2),
                            completion: nil)
                      
                    }
                    cell.lblAlbumName.text = self.socialMediaAlbumNames[1]
                    let count: Int = self.instagramPhotosArray.count
                    cell.lblImgCount.text = String(count)
                    return cell
                }
                else {
                    let cell = self.socialMediaNormalCell(indexPath: indexPath, collectionView: collectionView)
                    return cell
                }
            case (albumsArray.count + 2): //For Facebook isLoggedInToFacebookKodak // +3
                
                let cell : HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                cell.imageViewThumbnail.isHidden = true
                cell.imageViewThumbnailSocial.isHidden = false
                
              if UserDefaults.standard.value(forKey: "isLoggedInToFacebookKodak") as? Bool == true && UserDefaults.standard.value(forKey: "fetchFbAlbum") as? Bool == true
                {
                    if self.savedFbPhotosArray.indices.contains(0) {
                        let url = self.savedFbPhotosArray[0]
                        cell.imageViewThumbnailSocial.af_setImage(
                            withURL: URL(string:url)!,
                            imageTransition: .crossDissolve(0.2),
                            completion: nil)
                    }
                    cell.lblAlbumName.text = self.socialMediaAlbumNames[2]
                    
                    let count: Int = self.savedFbPhotosArray.count
                    cell.lblImgCount.text = String(count)
                    return cell
                }
                else {
                    let cell = self.socialMediaNormalCell(indexPath: indexPath, collectionView: collectionView)
                    return cell
                }
        
            default:
                return UICollectionViewCell()
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, customHeightForCellAtIndexPath indexPath: IndexPath, width: CGFloat) -> CGFloat
    {
//        let indexOfImage = indexPath.item
        let height = (collectionView.frame.size.width / 1.5)           //indexOfImage != 1 ? (236.0 + 50.0) : (158.0 + 50.0)
        
      
            return CGFloat(height)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.registerAnonymousUser()
        let tapPhotoAnalyticsEvent = TapPhotoAnalyticsEvent()
        Copilot.instance.report.log(event: tapPhotoAnalyticsEvent)
        if indexPath.item < albumsArray.count {
            
            let galleryGridVC = self.storyboard?.instantiateViewController(withIdentifier: "GalleryImageGridViewController") as! GalleryImageGridViewController
            galleryGridVC.cameFrom = "HomeViewController"
            
//            if indexPath.item != 1
//            {
                var collectionItem = PHAssetCollection()
//                if indexPath.item == 0
//                {
//                    collectionItem = albumsArray[0]
//                }
//                else
//                {
                    collectionItem = albumsArray[indexPath.item]
//                }

                let options = PHFetchOptions()
                //Added by akshay to exclude the video screenshots from assets
                if collectionItem.localizedTitle != "Videos"{
                    options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
                }

                let newFetchResult = PHAsset.fetchAssets(in: collectionItem, options: options)

                if newFetchResult.count > 0
                {
                    galleryGridVC.imagesArray = newFetchResult
                    galleryGridVC.collectionItem = collectionItem
                    galleryGridVC.imagesFrom = "Local"
                    customLoader.startGifLoader(viewController: self.view, gif: loaderGif, width: 40.0, height: 40.0)
                    self.navigationController?.pushViewController(galleryGridVC, animated: true)
                    customLoader.stopGifLoader()
                }
        }
        else {
            switch(indexPath.item){
            case (albumsArray.count): // +1
                if UserDefaults.standard.bool(forKey: "isLoggedInToGoogleKodak") && WebserviceModelClass().isInternetAvailable()
                {
                        if self.savedGooglePhotosArray.count != 0{
                        print("Google is logged in")
                        customLoader.startGifLoader(viewController: self.view, gif: loaderGif, width: 40.0, height: 40.0)
                        let galleryGridVC = self.storyboard?.instantiateViewController(withIdentifier: "GalleryImageGridViewController") as! GalleryImageGridViewController
                        galleryGridVC.cameFrom = "HomeViewController"
                        galleryGridVC.socialImages = self.savedGooglePhotosArray
                        galleryGridVC.imagesFrom = "Social"
                        galleryGridVC.headerName = "Google Photos"
                        self.navigationController?.pushViewController(galleryGridVC, animated: true)
                        customLoader.stopGifLoader()
                        }else{
                            
                            
                            let alertController = UIAlertController(title: "Alert", message: "No image available", preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                            // Add the actions
                            alertController.addAction(okAction)
                            
                            // Present the controller
                            self.present(alertController, animated: false, completion: nil)
                            // }
                            
                    }
                    
                    
                }
                else {
                    print("Google not logged in")
                    if WebserviceModelClass().isInternetAvailable() {
                        
                        if UserDefaults.standard.value(forKey: "googleUserName") as? String != "" {
                            //To Check google access token expire or not. If it expire it refresh access token.
                            
                            if let expireDate = UserDefaults.standard.value(forKey: "googleAccessTokenExpirationDate") as? NSDate {
                                let currentDate = NSDate()
                                let compareResult = currentDate.compare(expireDate as Date)
                                if compareResult == ComparisonResult.orderedDescending
                                {
                                    print("Google AccessToken Expired")
                                    GoogleHelper().refreshAccessTokenAndFetchAlbums()
                                } else {
                                    print("Google AccessToken not Expired")
                                    GIDSignIn.sharedInstance().presentingViewController = self
                                    GIDSignIn.sharedInstance().delegate = self
                                    GIDSignIn.sharedInstance()?.signIn()
                                }
                            } else {
                                GIDSignIn.sharedInstance().presentingViewController = self
                                GIDSignIn.sharedInstance().delegate = self
                                GIDSignIn.sharedInstance()?.signIn()
                            }
                        } else {
                            let connectAlbumAnalyticsEvent = ConnectAlbumAnalyticsEvent(albumType: AnalyticsValue.albumType.google.rawValue)
                            Copilot.instance.report.log(event: connectAlbumAnalyticsEvent)
                            GIDSignIn.sharedInstance().presentingViewController = self
                            GIDSignIn.sharedInstance().delegate = self
                            GIDSignIn.sharedInstance()?.signIn()
                        }
                    }
                    else {
                        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                        comeFromStrNoInternetSocialMedia = "googleNoInternet"
                        self.present(alertPopUp, animated: true, completion: nil)
                    }
                }
            case (albumsArray.count + 1): // +2
                if UserDefaults.standard.bool(forKey: "isLoggedInToInstagramKodak") && WebserviceModelClass().isInternetAvailable()
                {
                    if self.instagramPhotosArray.count != 0{
                    
                    print("Instagram is logged in")
                    customLoader.startGifLoader(viewController: self.view, gif: loaderGif, width: 40.0, height: 40.0)
                    let galleryGridVC = self.storyboard?.instantiateViewController(withIdentifier: "GalleryImageGridViewController") as! GalleryImageGridViewController
                    galleryGridVC.cameFrom = "HomeViewController"
                    galleryGridVC.socialImages = self.instagramPhotosArray
                    galleryGridVC.imagesFrom = "Social"
                    galleryGridVC.headerName = "Instagram"
                    self.navigationController?.pushViewController(galleryGridVC, animated: true)
                    customLoader.stopGifLoader()
                    }else{
                        
                        
                        let alertController = UIAlertController(title: "Alert", message: "No image available", preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        self.present(alertController, animated: false, completion: nil)
                        // }
                        
                    }
                }
                else {
                    print("Instagram not logged in")
                    if WebserviceModelClass().isInternetAvailable() {
                        let connectAlbumAnalyticsEvent = ConnectAlbumAnalyticsEvent(albumType: AnalyticsValue.albumType.instagram.rawValue)
                        Copilot.instance.report.log(event: connectAlbumAnalyticsEvent)
                        InstagramHelper().loginToInstagram(viewController: self)
                    }
                    else {
                        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                        comeFromStrNoInternetSocialMedia = "instagramNoInternet"
                        self.present(alertPopUp, animated: true, completion: nil)
                    }
                }
            case (albumsArray.count + 2): // +2
                if UserDefaults.standard.bool(forKey: "isLoggedInToFacebookKodak") && WebserviceModelClass().isInternetAvailable()
                {
                    if self.savedFbPhotosArray.count != 0{
                    if UserDefaults.standard.value(forKey: "fetchFbAlbum") as? Bool == true {
                    print("Facebook is logged in")
                    customLoader.startGifLoader(viewController: self.view, gif: loaderGif, width: 40.0, height: 40.0)
                    let galleryGridVC = self.storyboard?.instantiateViewController(withIdentifier: "GalleryImageGridViewController") as! GalleryImageGridViewController
                    galleryGridVC.cameFrom = "HomeViewController"
                    galleryGridVC.socialImages = self.savedFbPhotosArray
                    galleryGridVC.imagesFrom = "Social"
                    galleryGridVC.headerName = "Facebook"
                    self.navigationController?.pushViewController(galleryGridVC, animated: true)
                    customLoader.stopGifLoader()
                    }else{
                        //If login through facebook & facebook album not fetched.
                        UserDefaults.standard.set(true, forKey: "fetchFbAlbum")
                        self.reloadForFacebook()
                    }
                    }else{
                        
                        
                        let alertController = UIAlertController(title: "Alert", message: "No image available", preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        self.present(alertController, animated: false, completion: nil)
                        // }
                        
                    }
                }
                else {
                    print("Facebook not logged in")
                    if WebserviceModelClass().isInternetAvailable() {
                        FacebookHelper().loginToFacebook(viewController: self)
                        let connectAlbumAnalyticsEvent = ConnectAlbumAnalyticsEvent(albumType: AnalyticsValue.albumType.facebook.rawValue)
                        Copilot.instance.report.log(event: connectAlbumAnalyticsEvent)
                    }
                    else {
                        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                        comeFromStrNoInternetSocialMedia = "facebookNoInternet"
                        self.present(alertPopUp, animated: true, completion: nil)
                    }
                    
                }
            case (albumsArray.count + 3): // +4
                //                self.view.makeToast("Coming soon", duration: 2.0, position: .bottom)
                if UserDefaults.standard.bool(forKey: "isLoggedInToDropboxKodak")
                {
                    print("Dropbox is logged in")
                    customLoader.startGifLoader(viewController: self.view, gif: loaderGif, width: 40.0, height: 40.0)
                    let galleryGridVC = self.storyboard?.instantiateViewController(withIdentifier: "GalleryImageGridViewController") as! GalleryImageGridViewController
                    //                    galleryGridVC.dropboxImages = self.dropboxImages
                    galleryGridVC.cameFrom = "HomeViewController"
                    galleryGridVC.imagesFrom = "Social"
                    galleryGridVC.headerName = "Dropbox"
                    self.navigationController?.pushViewController(galleryGridVC, animated: true)
                    customLoader.stopGifLoader()
                }
                else {
                    print("Dropbox not logged in")
                    if WebserviceModelClass().isInternetAvailable() {
//                        DropboxClientsManager.authorizeFromController(UIApplication.shared, controller: self, openURL: { (url) in
//                            UIApplication.shared.openURL(url)
//                        })
                    }
                    else {
                        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                        comeFromStrNoInternetSocialMedia = "dropboxNoInternet"
                        self.present(alertPopUp, animated: true, completion: nil)
                    }
                    
                }
            default:
                print("Default")
            }
        }
    }
    private func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
    
        if (self.lastContentOffset < scrollView.contentOffset.y) {
            
            
            UIView.animate(withDuration: 0.2)
            {
                // it's going down
//                self.view.layoutIfNeeded()
                self.lblSmallAppName.alpha = 1.0
                self.constraintHeightHeaderView.constant = 30
                 self.viewAfterCollectionScroll.isHidden = false
               // self.constraintCollectionTop.constant = -64
                self.viewHeader.isHidden = true
            }
            
        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
            
            
            UIView.animate(withDuration: 0.2){
                // it's going up
//                print("At the top")
//                self.view.layoutIfNeeded()
                self.lblSmallAppName.alpha = 0
                self.viewAfterCollectionScroll.isHidden = true
               //  self.constraintCollectionTop.constant = 64
                self.constraintHeightHeaderView.constant = 70
                self.viewHeader.isHidden = false
                
            }
            
        } else {
            // didn't move
        }
        
    }

}

