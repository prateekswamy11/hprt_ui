//
//  HomeViewController+Connectivity.swift
//  Kodak Smile
//
//  Created by maximess142 on 29/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

extension HomeViewController : ResponceDelegate
{
    //MARK: - Printer Connection Delegate
    @objc func AccessoriesDisConnect()
    {
        //        connectStatus = 1
        UserDefaults.standard.set(false, forKey: "cancelFirmwarePopUP")
        IS_GET_FIRMWARE = false
        self.checkConnectivityStatus()
        self.showSolveItBtn()
    }
    
    @objc func AccessoriesConnect()
    {
        self.checkConnectivityStatus()

        // Set the printer to Always ON Mode
        api_changePowerOffStatus(status: 0)

        PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.get_ZIP_FIRMWARE, ResponseDelegate: self))
        isInitailConnect = false
    }
    
    @objc func showSolveItBtn() {
        if !isInitailConnect{
            self.btnConnect.setTitle("Solve it".localisedString(), for: .normal)
        }else {
            self.btnConnect.setTitle("Connect".localisedString(), for: .normal)
        }
    }
    
    // MARK: - Printer Response Delegate
    func ZIP_accessory_info(_ dict: NSMutableDictionary) {
        
        ModelPrinterDetail().setModelPrinterDetails(dict)
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "batteryPercentageChanged"), object: nil)

        self.setBatteryPercentageIcon()
    }
    
    func setBatteryPercentageIcon()
    {
        //print("Updating battery icon")
        self.imgViewBattery.isHidden = false
        let batteryPercentage = modelPrinterDetails.batteryPercentage
        switch batteryPercentage {
        case 0:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_0")
            break
            
        case 1..<10:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_1")
            break
            
        case 10..<20:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_2")
            break
            
        case 20..<30:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_3")
            break
            
        case 30..<40:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_4")
            break
            
        case 40..<50:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_5")
            break
            
        case 60..<70:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_6")
            break
            
        case 70..<80:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_7")
            break
            
        case 80..<90:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_8")
            break
            
        default:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_8")
            break
        }
    }
}
