//
//  HomeCollectionViewCell.swift
//  Polaroid MINT
//
//  Created by maximess142 on 19/07/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewThumbnail: UIImageView!
    
    @IBOutlet weak var viewImageShadow: UIView!
    @IBOutlet weak var lblAlbumName: UILabel!
    @IBOutlet weak var lblImgCount: UILabel!
    
    @IBOutlet weak var imageViewThumbnailSocial: UIImageView!
    var thumbnailImage: UIImage! {
        didSet {
            imageViewThumbnail.image = thumbnailImage
            
        }
    }
    
    
    
    override func prepareForReuse() {
        self.imageViewThumbnail.image = nil
        self.imageViewThumbnail.contentMode = .scaleAspectFill
        self.lblImgCount.text = ""
    }
}
