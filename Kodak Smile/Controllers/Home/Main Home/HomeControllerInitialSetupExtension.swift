//
//  HomeControllerInitialSetupExtension.swift
//  Kodak Smile
//
//  Created by maximess152 on 29/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation
import CopilotLogger
import CopilotAPIAccess

extension HomeViewController {
    func localizedStrings()
        {
    //        self.lblExpandedAppName.text = "KODAK SMILE".localisedString()
    //        self.lblSmallAppName.text = "KODAK SMILE".localisedString()
            self.lblConnectToPrinter.text = "Disconnected".localisedString()
            self.lblConnectToBluetooth.text = "Connect to Bluetooth".localisedString()
            self.lblConnectedToPrinter.text = "Connected".localisedString()
            self.lblPrinterInProgress.text = "Printing…".localisedString()
            self.btnConnect.setTitle("Connect".localisedString(), for: .normal)
            self.lblShowPhoto.text = "We would love to show you your photos but...".localisedString()
            self.lblAllowKodak.text = "First, allow KODAK SMILE to access your photos".localisedString()
            self.btnEnablePhoto.setTitle("Enable photos access".localisedString(), for: .normal)
        }
        
        // MARK: - Initialisation of views
        func initialiseBluetoothStatusVars()
        {
            ConnectionStatus.sharedController.printerStatusDelegate = self
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(AccessoriesDisConnect),
                name: NSNotification.Name(rawValue: "AccessoryDisConnectAfterCallBack"),
                object: nil)
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(AccessoriesConnect),
                name: NSNotification.Name(rawValue: "AccessoryConnect"),
                object: nil)
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(self.printFinishCalled),
                name: NSNotification.Name(rawValue: "PrintFinished"),
                object: nil)
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(self.printStartedCalled),
                name: NSNotification.Name(rawValue: "PrintStarted"),
                object: nil)
        }
        
        func initialiseCollectionView()
        {
            self.folderCollectionView.collectionViewLayout.invalidateLayout()
            let mosaicLayout = LiquidCollectionViewLayout()
            mosaicLayout.delegate = self
            mosaicLayout.cellWidth = (UIScreen.main.bounds.width - 50)  / 2
            self.folderCollectionView.collectionViewLayout = mosaicLayout
        }
        
        func checkForNewEvent(){
            if UserDefaults.standard.value(forKey: "eventId") as? Int != SMLEventData["id"] as? Int && WebserviceModelClass().isInternetAvailable() && UserDefaults.standard.value(forKey: "eventId") != nil && UserDefaults.standard.value(forKey: "eventId") as? Int != 0{
                UserDefaults.standard.set(SMLEventData["id"], forKey: "eventId")
                let vc = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "EventSplashViewController") as! EventSplashViewController
                appDelegate().getCurrentViewController()!.addChildViewController(vc)
                appDelegate().getCurrentViewController()!.view.addSubview(vc.view)
            }else{
                if (UserDefaults.standard.value(forKey: "eventId") as? Int == 0 || UserDefaults.standard.value(forKey: "eventId") == nil) && WebserviceModelClass().isInternetAvailable(){
                    WebserviceModelClass().getDataForomUrl(module: "EventMessage", url: EventsUrl.message) { (success, message, data) in
                        if success{
                            guard let eventData = data["event_message"] as? [String: Any] else{
                                UserDefaults.standard.set(UserDefaults.standard.value(forKey: "eventId") as? Int ?? 0, forKey: "eventId")
                                return
                            }
                            SMLEventData["message"] = eventData["message"] as! String
                            SMLEventData["id"] = eventData["id"] as! Int
                            let imgView = UIImageView()
                            imgView.af_setImage(
                                withURL: URL(string:eventData["thumbnail"] as! String)!,
                                imageTransition: .crossDissolve(0.2),
                                completion: { (img) in
                                    SMLEventData["image"] = img.value!
                                    let vc = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "EventSplashViewController") as! EventSplashViewController
                                    appDelegate().getCurrentViewController()!.addChildViewController(vc)
                                    appDelegate().getCurrentViewController()!.view.addSubview(vc.view)
                            })
                        }
                    }
                }
            }
        }
    
    func registerAnonymousUser(){
        if WebserviceModelClass().isInternetAvailable() && !Copilot.instance.manage.copilotConnect.defaultAuthProvider.canLoginSilently{
            if UserDefaults.standard.bool(forKey: "iWillDoItLater"){
                AppManager.shared.userManager.registerAnonymously { [weak self] (response) in
                    switch response {
                    case .failure(error: let error):
                        ZLogManagerWrapper.sharedInstance.logError(message: "failed to register anonymously with error: \(error)")
                    case .success(_):
                        Copilot.instance.report.log(event: SignupAnalyticsEvent())
                        ZLogManagerWrapper.sharedInstance.logInfo(message: "success in anonymous register")
                        let onboardingStartedEvent = OnboardingStartedAnalayticsEvent(flowID: "fromPush", screenName: "onboarding_started")
                        Copilot.instance.report.log(event: onboardingStartedEvent)
                        UserDefaults.standard.set(true, forKey: "onboarding_started")
                    }
                }
            }else
            {
                AppManager.shared.userManager.attemptToSilentLogin {[weak self] (response) in
                    switch response {
                    case .success :
                        break
                    case .failure(error: let error):
                       ZLogManagerWrapper.sharedInstance.logError(message: "failed to silent login with error: \(error)")
                    }
                }
            }
        }
    }
    
    func showCameraPermissionPopup() {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
            DispatchQueue.main.async(execute: {
                let cameraVC = storyboards.cameraStoryboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                self.navigationController?.pushViewController(cameraVC, animated: false)
            })
        }else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    DispatchQueue.main.async(execute: {
                        UserDefaults.standard.set(true, forKey: "onboardingSkipped")
                        let cameraVC = storyboards.cameraStoryboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                        self.navigationController?.pushViewController(cameraVC, animated: false)
                    })
                } else {
                    //access denied
                    if UserDefaults.standard.bool(forKey: "enableCameraAccessCount") {
                        DispatchQueue.main.async {
                            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
                            UIApplication.shared.open(settingsUrl)
                        }
                    }
                    UserDefaults.standard.set(true, forKey: "enableCameraAccessCount")
                }
            })
        }
    }
    
    func addNotificationObserver()
    {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.reloadForFacebook),
            name: NSNotification.Name(rawValue: "fbUserChanged"),
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.reloadForInstagram),
            name: NSNotification.Name(rawValue: "instagramUserChanged"),
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.reloadForDropbox),
            name: NSNotification.Name(rawValue: "dropboxUserChanged"),
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.googleImagesFetched(noti:)),
            name: NSNotification.Name(rawValue: "googleImagesFetched"),
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reloadGallery),
            name: NSNotification.Name(rawValue: "ReloadGallery"),
            object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userLoggedOut(_:)), name: NSNotification.Name(rawValue: "userLoggedOut"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(goToCameraVC), name: Notification.Name("notificationToOpenCamera"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setFlagOutOfPaper), name: Notification.Name("ShowBuyPaperButton"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSolveItBtn), name: NSNotification.Name("ShowSolveIt"), object: nil)
    }
    
    @objc func setFlagOutOfPaper() {
        isOutOfPaper = true
    }
    
    @objc func showBuyPaperBtn() {
        self.btnBuyPaper.isHidden = false
        self.lblConnectedToPrinter.text = "Out of paper".localisedString()
    }
    
    @objc func hideBuyPaperBtn() {
        self.btnBuyPaper.isHidden = true
        self.lblConnectedToPrinter.text = "Connected".localisedString()
    }
    
    @objc func goToCameraVC()
    {
        let cameraVC = storyboards.cameraStoryboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
        self.navigationController?.pushViewController(cameraVC, animated: false)
    }
    
    func checkAndRequestCameraPermission () -> Bool
    {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus == AVAuthorizationStatus.denied {
            // Denied access to camera
            // Explain that we need camera access and how to change it.
            return false
        } else if authStatus == AVAuthorizationStatus.notDetermined {
            // The user has not yet been presented with the option to grant access to the camera hardware.
            // Ask for it.
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (grantd) in
                // If access was denied, we do not set the setup error message since access was just denied.
                if grantd {
                    // Allowed access to camera, go ahead and present the UIImagePickerController.
                    // self.setupInputOutput()
                }
            })
            return false
        } else {
            return true
            //Adding loader deliberately for safe loading of camera
        }
    }

    @objc func reloadGallery() {
        self.folderCollectionView.reloadData()
    }
}
