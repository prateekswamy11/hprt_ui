//
//  HomeSocialMediaCollectionViewCell.swift
//  Polaroid MINT
//
//  Created by maximess120 on 20/09/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class HomeSocialMediaCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewImageShadow: UIView!
    @IBOutlet weak var imageViewThumbnail: UIImageView!
    @IBOutlet weak var lblAlbumName: UILabel!
    @IBOutlet weak var lblImgCount: UILabel!
    @IBOutlet weak var imgViewLogo: UIImageView!
    @IBOutlet weak var lblAddFolder: UILabel!
}
