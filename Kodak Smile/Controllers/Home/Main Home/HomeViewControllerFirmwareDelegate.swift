//
//  HomeViewControllerFirmwareDelegate.swift
//  Kodak Smile
//
//  Created by maximess175 on 21/06/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

extension HomeViewController{
    
    func GetVersionResponse(_ Firmware:String ,Conexan:String,  TMD: String){
        
        let CNXstring =  Conexan.replacingOccurrences(of: ".", with: "")
        XCNXVersion = (Float(CNXstring.replacingOccurrences(of: "v", with: ""))!)//*10
        
        let TMDVstring =  TMD.replacingOccurrences(of: ".", with: "")
        XTMDVersion =  (Float(TMDVstring.replacingOccurrences(of: "v", with: ""))!)//*10
        
        let FIRMstring =  Firmware.replacingOccurrences(of: ".", with: "")
        XFIRMVersion =  Float(FIRMstring.replacingOccurrences(of: "v", with: ""))!
        
        XFIRMVersionString = Firmware
        
        if(appDelegate().isConnectedToPrinter == true)
        {
            if IS_GET_FIRMWARE == false{
            if WebserviceModelClass().isInternetAvailable(){
                self.getFirmware()
                }
                IS_GET_FIRMWARE = true
            }
        }
    }
    
    func getFirmware() {
        customLoader.showActivityIndicator(viewController: self.view)
        
        WebserviceModelClass().getDataFor(module:"PlansViewController",subUrl:FIRMWARE_URL, completionHandler:{
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess {
                customLoader.hideActivityIndicator()
                print("responseData = \(responseData)")
                
                if (responseData as NSDictionary != nil) {
                    
                    guard let data = (responseData as AnyObject).value(forKey: "data") as? NSDictionary else {return}
                    guard (data.value(forKey: "tmd") as? NSDictionary) != nil else {return}
                    guard let firmwareDict = data.value(forKey: "firmware") as? NSDictionary else {return}
                    guard var FIRMVstring = firmwareDict.value(forKey: "version") as? String else {return}
                    guard let textForceFirmwareFlag = firmwareDict["force_update"] as? String else {return}
                    
                    IS_FORCE_UPDATE = textForceFirmwareFlag
                    
                    if FIRMVstring == "" {
                        FIRMVstring = "0"
                    }
                    
                    let FIRMVersion = float_t(FIRMVstring.replacingOccurrences(of: ".", with: ""))
                    
                    print("\(FIRMVersion!) > \(XFIRMVersion)  ")
                    
                    if( FIRMVersion! > XFIRMVersion && XFIRMVersion != 0.0 &&  IS_FORCE_UPDATE == "yes" && IS_POP_SHOWN == false) {
                        appDelegate().isFirmwareUpdateAvailable = true
                        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNewFirmwareAvailableViewController") as! PopUpNewFirmwareAvailableViewController
                        alertPopUp.view.frame = self.view.bounds
                        self.addChildViewController(alertPopUp)
                        self.view.addSubview(alertPopUp.view)
                        alertPopUp.didMove(toParentViewController: self)
                    }else if( FIRMVersion! > XFIRMVersion && XFIRMVersion != 0.0 &&  IS_FORCE_UPDATE == "no" && UserDefaults.standard.bool(forKey: "cancelFirmwarePopUP") == false && IS_POP_SHOWN == false) {
                        appDelegate().isFirmwareUpdateAvailable = true
                        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNewFirmwareAvailableViewController") as! PopUpNewFirmwareAvailableViewController
                        alertPopUp.view.frame = self.view.bounds
                        self.addChildViewController(alertPopUp)
                        self.view.addSubview(alertPopUp.view)
                        alertPopUp.didMove(toParentViewController: self)
                    }else{
                        appDelegate().isFirmwareUpdateAvailable = false
                    }
                }
            }else{
                print("Error")
                customLoader.hideActivityIndicator()
            }
        })
    }
}
