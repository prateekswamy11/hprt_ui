//
//  EventSplashViewController.swift
//  Kodak Smile
//
//  Created by MAXIMESS152 on 09/10/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class EventSplashViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var imgEventImage: UIImageView!
    @IBOutlet weak var lblEventMessage: UITextView!
    
    //MARK:- Variables
    var strMessage: String = ""
    var imgEvent = UIImage()
    
    //MARK:- View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblEventMessage.text = SMLEventData["message"] as? String
        self.imgEventImage.image = SMLEventData["image"] as? UIImage
    }
    
    //MARK:- IBActions
    @IBAction func okBtnClicked(_ sender: UIButton) {
//        self.dismiss(animated: false, completion: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        UserDefaults.standard.set(SMLEventData["id"], forKey: "eventId")
    }
}
