//
//  HomeViewController.swift
//  Polaroid MINT
//
//  Created by maximess142 on 17/07/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import Photos
import CoreBluetooth
import GoogleSignIn
import GTMSessionFetcher
import GoogleAPIClientForREST
import SwiftyGif
import CopilotAPIAccess
import AlamofireImage
import Alamofire
import CopilotLogger

var image_For_AI = PHFetchResult<PHAsset>()
var forImagePreview = UIImage()
var isOutOfPaper = false
//var connectStatus = 0 // 0 - connect to bluetooth, 1 - connect to printer, 2 - connected to printer
class HomeViewController: UIViewController, PrinterStatusDelegate {
    
    //MARK:- IBOutlets
    @IBOutlet weak var imgDotConnectedToPrinter: UIImageView!
    @IBOutlet weak var folderCollectionView: UICollectionView!
    @IBOutlet weak var constraintHeightHeaderView: NSLayoutConstraint!
    @IBOutlet weak var constraintTitleTop: NSLayoutConstraint!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblExpandedAppName: UILabel!
    @IBOutlet weak var lblSmallAppName: UILabel!
    @IBOutlet weak var viewAfterCollectionScroll: UIView!
    
    // Connected to printer
    @IBOutlet weak var viewConnectedToPrinter: UIView!
    @IBOutlet weak var imgConnectedToPrinter: UIImageView!
    @IBOutlet weak var lblConnectedToPrinter: UILabel!
    @IBOutlet weak var imgViewBattery: UIImageView!
    @IBOutlet weak var btnBuyPaper: UIButton!
    
    // Connect to bluetooth
    @IBOutlet weak var viewConnectToBluetooth: UIView!
    @IBOutlet weak var lblConnectToBluetooth: UILabel!
    @IBOutlet weak var imgConnectToBluetooth: UIImageView!
    
    // Connect to printer
    @IBOutlet weak var viewConnectToPrinter: UIView!
    @IBOutlet weak var lblConnectToPrinter: UILabel!
    @IBOutlet weak var imgConnectToPrinter: UIImageView!
    @IBOutlet weak var btnConnect: UIButton!
    
    // Printer in Progress
    @IBOutlet weak var viewPrinterInProgress: UIView!
    @IBOutlet weak var lblPrinterInProgress: UILabel!
    @IBOutlet weak var imgPrinterInProgress: UIImageView!
    
    @IBOutlet weak var viewPhotosPermission: UIView!
    @IBOutlet weak var lblShowPhoto: UILabel!
    @IBOutlet weak var lblAllowKodak: UILabel!
    @IBOutlet weak var btnEnablePhoto: UIButton!
    
    // AutoMinimize
    @IBOutlet weak var constraintBottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewBottomMenu: UIView!
    @IBOutlet weak var viewBtnCamera: UIView!
    @IBOutlet weak var btnCamera: UIButton!
    
    //MARK:- Variables
    //var connectStatus = 0 // 0 - connect to bluetooth, 1 - connect to printer, 2 - connected to printer
    var albumsArray = [PHAssetCollection]()//: [PHAssetCollection]!
    var socialAlbumArray = [PHAssetCollection]()
    var imageArrayForAI = PHFetchResult<PHAsset>()
    let socialMediaAlbumNames = [ "GOOGLE".localisedString(),"INSTAGRAM".localisedString(), "FACEBOOK".localisedString(), "DROPBOX".localisedString()]
    let socialMediaLogo = ["googlePhotos1.png", "instegram-2.pdf", "facebook-2.pdf", "ropbox.pdf"]
    //private var lastContentOffset: CGFloat = 0     // variable to save the last position visited, default to zero
    var lastContentOffset: CGFloat = 0
    var savedFbPhotosArray = [String]()
    var savedGooglePhotosArray = [String]()
    var instagramPhotosArray = [String]()
    var arrDropboxImages = [String]()
    var dropboxImages = [UIImage]()
    var timer : Timer! //Timer for autoconnect
    let loaderGif = UIImage(gifName: "loader.gif")
    var checkPhotoPermissionPopUp = 0
    var imagesCountArray = [Int]()
    var thumbnailImagesArray = [PHAsset]()
    var batteryTimer : Timer?
    var videoFetchResult: PHFetchResult<PHAsset>?
    var reachability : Reachability!
    var networkStatus : Reachability.NetworkStatus!
    let maxHeaderHeight: CGFloat = 88
    let minHeaderHeight: CGFloat = 44
    var previousScrollOffset: CGFloat = 0
    var rechabilityObserver: ReachabilityHandler?
    let saveSignInData:UserDefaults = UserDefaults.standard
    
    // MARK: - View Delegates
    override func viewDidLoad() {
        //        NotificationCenter.default.addObserver(
        //            self,
        //            selector: #selector(self.getPredictions),
        //            name: NSNotification.Name(rawValue: "GetPredictions"),
        //            object: nil)
        super.viewDidLoad()
        self.localizedStrings()
        self.initialiseCollectionView()
        //        self.getPhotoAssetsAlbumWise()
        self.addTimerForAutoConnect()
        self.addTimerForBatteryStatus()
        //self.getDropboxData()
//        self.addNotificationObserver()
        self.lblSmallAppName.alpha = 0
        self.reloadForGoogle()
        self.reloadForFacebook()
        self.reloadForInstagram()
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        //Code for padding to button text
         self.btnEnablePhoto.titleEdgeInsets = UIEdgeInsetsMake(3,3,3,3)
        self.checkConnectivityStatus()
//        self.checkForNewEvent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.networkReachability()
        self.checkConnectivityStatus()
        self.initialiseBluetoothStatusVars()
        self.configureGooglePhotos()
        self.showSolveItBtn()
//        self.showBuyPaperBtn()
       
        
        self.addNotificationObserver()
        if checkPhotoLibraryPermission(){
            DispatchQueue.main.async(execute: {
                self.getPhotoAssetsAlbumWise()
            })
            self.viewPhotosPermission.isHidden = true
        }
        else{
            self.viewPhotosPermission.isHidden = false
        }
        PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.get_ZIP_FIRMWARE, ResponseDelegate: self))
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.checkInternet()
        self.checkConnectivityStatus()
        //        QUEUEFORAI.resume()
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.home.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        customLoader.stopGifLoader()
        NotificationCenter.default.removeObserver(self, name: Notification.Name("notificationToOpenCamera"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
//        NotificationCenter.default.removeObserver(self)
        //        QUEUEFORAI.suspend()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    deinit
    {
        print("deinit memory gallery called")
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
    
    // MARK: - IBActions
    @IBAction func buyPaperBtnClicked(_ sender: UIButton) {
        if !WebserviceModelClass().isInternetAvailable()
        {
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            comeFromStrNoInternetSocialMedia = "buyPaperNoInternet"
            self.present(alertPopUp, animated: true, completion: nil)
        }
        else
        {
            let buyPaperVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "BuyPaperViewController") as! BuyPaperViewController
            self.navigationController?.pushViewController(buyPaperVC, animated: true)
            sender.isHidden = true
        }
        
    }
    
    @IBAction func btnEnablePhotoAccess(_ sender: Any) {
        if  UserDefaults.standard.bool(forKey: "EnablePhotoAccess"){
            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
            UIApplication.shared.open(settingsUrl)
        }
        else if checkPhotoLibraryPermission() == false
        {
            PHPhotoLibrary.requestAuthorization { (status) in
                if status == .authorized
                {
                    DispatchQueue.main.async(execute: {
                        self.getPhotoAssetsAlbumWise()
                    })
                }
            }
            UserDefaults.standard.set(true, forKey: "EnablePhotoAccess")
        }
    }
    
    @IBAction func btnConnectedToPrinterClicked(_ sender: Any) {
        
        let connectivityVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController
        connectivityVC.comeFromStr = "HomeVCorSettingsVC"
        self.navigationController?.pushViewController(connectivityVC, animated: true)
    }
    
    @IBAction func btnConnectToBluetoothClicked(_ sender: Any)
    {
        self.showAlert(title: nil, message: "Please turn on Bluetooth on your phone from settings.".localisedString())
        //showAlert(message: "Please turn on Bluetooth on your phone from settings.")
    }
    
    @IBAction func btnConnectToPrinterClicked(_ sender: Any)
    {
        let connectivityVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController
        connectivityVC.comeFromStr = "HomeVCorSettingsVC"
        self.navigationController?.pushViewController(connectivityVC, animated: true)
    }
    
    @IBAction func btnPrinterInProgressClicked(_ sender: UIButton)
    {
        
    }
    
    @IBAction func btnCameraClicked(_ sender: Any)
    {
    //        DispatchQueue.main.async {
                if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                    
                        //already authorized
                        let cameraVC = storyboards.cameraStoryboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                        cameraVC.previewImage = forImagePreview
                        self.navigationController?.pushViewController(cameraVC, animated: false)
                    
                      } else {
                     let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpWeWouldLikeToOpenCameraViewController") as! PopUpWeWouldLikeToOpenCameraViewController
                    alertPopUp.delegate = self
                    self.present(alertPopUp, animated: true, completion: nil)
                    
                }
            
    //        }
        }
    
    @IBAction func btnSettingsClicked(_ sender: Any)
    {
        let tapMenuEvent = TapMenuAnalyticsEvent(screenName: AnalyticsEventName.GeneralEvents.tapMenu.rawValue)
        Copilot.instance.report.log(event: tapMenuEvent)
        var isSettingsVCInStack = false
        
        if let navVC = self.navigationController
        {
            // Pop view controller to home.
            for item in navVC.viewControllers {
                // Filter for your desired view controller:
                if item.isKind(of: SettingsViewController.self) {
                    isSettingsVCInStack = true
                    self.navigationController?.popToViewController(item, animated: false)
                }
            }
        }
        // When directly came from onboarding, home VC is not in stack so navigate by pushing to it
        if isSettingsVCInStack == false
        {
            let settingsVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            self.navigationController?.pushViewController(settingsVC, animated: false)
        }
    }
    
    // MARK: - Logical Methods
    
    func getPhotoAssetsAlbumWise()
    {
        if checkPhotoLibraryPermission() == true
        {
            var count = -1
            DispatchQueue.main.async {
                self.viewPhotosPermission.isHidden = true
            }
            
            PHPhotoLibrary.shared().register(self)
            autoreleasepool
                {
                    var albumArray = [PHAssetCollection]()
                    let options = PHFetchOptions()
                    let smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: PHAssetCollectionSubtype.albumRegular, options: options)
                    let topLevelUserCollections = PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.album, subtype: PHAssetCollectionSubtype.any, options: nil)
                    let allAlbums = [topLevelUserCollections, smartAlbums] as NSArray
                    for i in 0 ..< allAlbums.count
                    {
                        let result = allAlbums[i]
                        (result as AnyObject).enumerateObjects { (asset, index, stop) in
                            if let collection = asset as? PHAssetCollection
                            {
                                let albumTitle = collection.localizedTitle
                                print("albumTitle = \(albumTitle)")
                                if(collection.estimatedAssetCount > 0 && albumTitle != "Videos" && albumTitle != "Slo-mo" && albumTitle != "Panoramas" && albumTitle != "Hidden" && albumTitle != "Time-lapse" && albumTitle != "Recently Deleted" && albumTitle != "Long Exposure" && albumTitle != "Live Photos" && albumTitle != "Bursts")
                                {
                                    
//                                    if albumTitle == "Videos"{
//
//                                        options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
//
//                                    }
//                                    else{
                                    
                                        // check album with atleast 1 image in galary
                                        options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
//                                    }
                                    
                                    let assetsFetchResult = PHAsset.fetchAssets(in: collection, options: options)
                                    if(assetsFetchResult.count>0)
                                    {
                                        if collection.localizedTitle?.localisedString() == "Camera Roll".localisedString() || collection.localizedTitle == "All Photos".localisedString() {
                                            image_For_AI = assetsFetchResult
//                                            self.imageArrayForAI = assetsFetchResult
                                        }
                                        // put the "all photos" in the first index
                                        // It will go in this if loop only for all photos or Camera Roll
                                        if (collection.assetCollectionSubtype == .smartAlbumUserLibrary)
                                        {
                                            //print("Hi, I came here %@", collection.localizedTitle)
                                            albumArray.insert(collection, at: 0)
                                            self.imagesCountArray.insert(assetsFetchResult.count, at: 0)
                                            self.thumbnailImagesArray.insert(assetsFetchResult[assetsFetchResult.count-1], at: 0)
                                            count = assetsFetchResult.count
                                        }else if collection.localizedTitle == "SMILE" && !albumArray.contains(collection){
                                            albumArray.insert(collection, at: 0)
                                            self.imagesCountArray.insert(assetsFetchResult.count, at: 0)
                                            self.thumbnailImagesArray.insert(assetsFetchResult[assetsFetchResult.count-1], at: 0)
                                            count = assetsFetchResult.count
                                        }
                                        else
                                        {
                                            // For other all SUb albums
                                            //print("hello, I came here %@", collection.localizedTitle)
                                            albumArray.append(collection)
                                            self.imagesCountArray.append(assetsFetchResult.count)
                                            self.thumbnailImagesArray.append(assetsFetchResult[assetsFetchResult.count-1])
                                        }
                                        
                                    }
                                    
                                }
                            }
                        }
                    }
       
                    self.videoFetchResult = PHAsset.fetchAssets(with: .video, options: nil)
                    
                    self.albumsArray = albumArray
                    DispatchQueue.main.async {
                        self.folderCollectionView.reloadData()
                    }
            }
            print("all finished----------")
         customLoader.showActivityIndicator(viewController: self.view)
            DispatchQueue.main.async {
                customLoader.hideActivityIndicator()
            }
        }
        else
        {
            PHPhotoLibrary.requestAuthorization { (status) in
                if status == .authorized
                {
                    DispatchQueue.main.async(execute: {
                        self.getPhotoAssetsAlbumWise()
                    })
                }
            }
        }
    }
    
    
}
