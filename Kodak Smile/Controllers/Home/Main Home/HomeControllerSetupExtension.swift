//
//  HomeControllerSetupExtension.swift
//  Kodak Smile
//
//  Created by maximess152 on 29/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import Foundation
import Photos
import CopilotAPIAccess
import CopilotLogger

extension HomeViewController: CameraPermissinNativePopupDelegate {
    
    func addTimerForAutoConnect()
    {
        self.timer = Timer.scheduledTimer(timeInterval: 0, target: self, selector: #selector(HomeViewController.autoConnectIfOneDeviceIsFound), userInfo: nil, repeats: true)
    }
    
    func addTimerForBatteryStatus()
    {
        self.batteryTimer = Timer.scheduledTimer(timeInterval: 20.0, target: self, selector: #selector(self.getBatteryPercentage), userInfo: nil, repeats: true)
    }
    
    @objc func autoConnectIfOneDeviceIsFound(){
        Accessory_Manager.shared.getDevices()
        if Accessory_Manager.shared.arrayDeviceList.count == 1 && appDelegate().isConnectedToPrinter == false{
            appDelegate().currentAccessory = Accessory_Manager.shared.arrayDeviceList[0]
            Accessory_Manager.shared.setUpSession(accessory: Accessory_Manager.shared.arrayDeviceList[0])
            appDelegate().isConnectedToPrinter = true
            if let device = appDelegate().currentAccessory {
                if let macId = device.value(forKey: "macAddress")
                {
                    let macStr = macId as! String
                    let sepMacID = macStr.components(separatedBy: ":").joined()
                    CONNECT_DEVICE_MAC_ADDRESS = sepMacID
                    Copilot.instance.report.log(event: ThingConnectedAnalyticsEvent(thingID: CONNECT_DEVICE_MAC_ADDRESS))
                }
            }
        }
    }
    
    @objc func getBatteryPercentage(){
        if appDelegate().isConnectedToPrinter == true && appDelegate().isPrintingInProgress == false
        {
            PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.getZIPInfo, ResponseDelegate: self))
        }
    }
    
    func checkConnectivityStatus()
    {
        self.viewConnectToBluetooth.isHidden = true
        self.viewConnectedToPrinter.isHidden = true
        self.viewConnectToPrinter.isHidden = true
        self.viewPrinterInProgress.isHidden = true
        if (appDelegate().isConnectedToPrinter == true)
        {
            if (appDelegate().isPrintingInProgress == true)
            {
                self.viewPrinterInProgress.isHidden = false
            }
            else
            {
                self.viewConnectedToPrinter.isHidden = false
                //self.imgDotConnectedToPrinter.backgroundColor = UIColor(red: 103.0/256, green: 192.0/256, blue: 119.0/256, alpha: 1.0)//green
            }
        }
        else if (appDelegate().isConnectedToPrinter == false)
        {
            self.viewConnectToPrinter.isHidden = false
            //self.imgDotConnectedToPrinter.backgroundColor = UIColor(red: 241.0/256, green: 88.0/256, blue: 73.0/256, alpha: 1.0)//red
        }
        
        if isOutOfPaper{
            self.showBuyPaperBtn()
        }else{
            self.hideBuyPaperBtn()
        }
        self.setNeedsFocusUpdate()
    }
    
    func getAssetThumbnail(asset: PHAsset,size:CGFloat) -> UIImage
    {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.deliveryMode = .opportunistic
        option.isSynchronous = true
        //        CGSize(width: size, height: size)
        manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler:
            {(result, info)->Void in
                
                if let result = result
                {
                    thumbnail = result
                }
        })
        return thumbnail
    }
    
    func checkPhotoLibraryPermission() -> Bool {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            print("authorized")
            return true
        case .denied, .restricted :
            //handle denied status
            print("denied")
            return false
        case .notDetermined:
            return false
        }
    }
    
    func expandHeaderView()
    {
        // Normal with custom animation in view
        UIView.animate(withDuration: 0.5, animations: {
            self.constraintHeightHeaderView.constant = 30
            self.view.layoutIfNeeded()
        }, completion:
            {
                Void in
        })
    }
    
    func compressHeaderView()
    {
        // Normal with custom animation in view
        UIView.animate(withDuration: 0.5, animations: {
            self.constraintHeightHeaderView.constant = 70
            
            self.view.layoutIfNeeded()
            
        }, completion:
            {
                Void in
        })
    }
    
    // MARK: - Printer Connection Methods
    
    @objc func printStartedCalled()
    {
        self.checkConnectivityStatus()
    }
    
    @objc func printFinishCalled()
    {
        //        appDelegate().isPrintingInProgress = false
        //NotificationCenter.default.removeObserver(self, name: Notification.Name("notificationToEndTimer"), object: nil)
        self.checkConnectivityStatus()
    }
    
    @objc func stopLoader(){
        self.viewPrinterInProgress.isHidden = true
        self.viewConnectToPrinter.isHidden = true
        self.viewConnectToBluetooth.isHidden = true
        self.viewConnectedToPrinter.isHidden = false
        self.lblConnectedToPrinter.isHidden = false
        //        customLoader.hideActivityIndicator()
    }
    
    @objc func startLoader()  {
        self.viewPrinterInProgress.isHidden = false
        self.viewConnectToPrinter.isHidden = true
        self.viewConnectedToPrinter.isHidden = true
        self.viewConnectToBluetooth.isHidden = true
        self.lblConnectedToPrinter.isHidden = true
    }
    
    @objc func willEnterForeground() {
        self.folderCollectionView.reloadData()
    }
    
    func networkReachability() {
        reachability = Reachability()
        NotificationCenter.default.addObserver(self, selector:#selector(self.checkNetworkStatus), name: ReachabilityChangedNotification, object: nil);
        do {
            try reachability?.startNotifier()
        } catch {
            print("This is not working.")
            return
        }
    }
    
    @objc func checkNetworkStatus()
    {
        networkStatus = Reachability()?.currentReachabilityStatus
        if (networkStatus == Reachability.NetworkStatus.notReachable)
        {
            print("Not Reachable")
            DispatchQueue.main.async {
                if self.networkStatus.description == "No Connection"{
                    customLoader.hideActivityIndicator()
                   // FacebookHelper().logoutOffFacebook()
                    //InstagramHelper().logoutOffInstagram()
                    GoogleHelper().logoutOffGoogle()
                }
                self.folderCollectionView.reloadData()
            }
        }
        else
        {
            DispatchQueue.main.async {
                self.checkForNewEvent()
            }
        }
    }
}

// MARK: PHPhotoLibraryChangeObserver
extension HomeViewController: PHPhotoLibraryChangeObserver {
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized //&& fetchResult != nil
        {
            DispatchQueue.main.async {
                self.getPhotoAssetsAlbumWise()
            }
            guard let changes = changeInstance.changeDetails(for: imageArrayForAI)
                else { return }
            print(changes)
            // Change notifications may be made on a background queue. Re-dispatch to the
            // main queue before acting on the change as we'll be updating the UI.
            DispatchQueue.main.sync {
                // Reload the thumbnails
                self.folderCollectionView.reloadData()
            }
        }
    }
}

