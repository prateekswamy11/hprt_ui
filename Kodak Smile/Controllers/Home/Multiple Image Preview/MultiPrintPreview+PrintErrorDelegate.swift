//
//  MultiPrintPreview+PrintErrorDelegate.swift
//  Polaroid MINT
//
//  Created by maximess142 on 22/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

extension MultipleImagePreviewViewController : PrintErrorProtocol
{
    func receivedPrintingError(errorCode: Int)
    {
        customLoader.hideActivityIndicator()
        let errorHandlerHelper = PrintErrorCodesHandler()
        errorHandlerHelper.currentViewController = self
        errorHandlerHelper.showErrorMsgWithCode(errorCode: errorCode)
        // Remove the delegate when error is shown, assign it again when next print command is given. This is done because we are constantly hitting GetAccessoryInfo, so it will show the error again and again which is undesired. We require the error to show only when print is given
        PrinterQueueManager.sharedController.errorDelegate = nil
    }
    
    // MARK: - Printing Status Methods
    
    @objc func handlePrintCompletion()
    {
        customLoader.hideActivityIndicator()
        // We can enable the print button now
        self.lblPrint.text = "Print".localisedString()+" \(self.selectedImages.count) "+"Images".localisedString()
        self.lblPrint.textColor = UIColor.white
        self.btnPrint.isEnabled = true
        self.checkPrinterStatus()
    }
    
}
