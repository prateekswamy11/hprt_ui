//
//  MultipleImagePreviewCollectionViewCell.swift
//  Kodak Classic
//
//  Created by MAXIMESS152 on 10/01/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

class MultipleImagePreviewCollectionViewCell: UICollectionViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet weak var imgViewPreview: UIImageView!
    @IBOutlet weak var imgViewLeadingConstraint: NSLayoutConstraint!
    
    //MARK:- Variables
    var representedAssetIdentifier: String!
    
    override func prepareForReuse() {
        self.imgViewPreview.image = nil
    }
    
}
