//
//  MultiPrintPreview+ResponceDelegate.swift
//  Polaroid MINT
//
//  Created by maximess142 on 20/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

extension MultipleImagePreviewViewController : ResponceDelegate
{
    func ZIP_accessory_info(_ dict: NSMutableDictionary) {
        let dictPrinterData = dict
        let batteryPerStr  =   "\(dictPrinterData.value(forKey: "Battery")!)"
        print("batteryPerStr = \(batteryPerStr)")
        let batteryPer = Int(batteryPerStr)!
        print("batteryPer = \(batteryPer)")
        customLoader.hideActivityIndicator()
        // Print Error delegate
        PrinterQueueManager.sharedController.errorDelegate = self
        self.saveEditedImagesInGallery()
        let errorCode = dictPrinterData.value(forKey: "ErrorCode") as? Int
        if errorCode == 0
        {
            self.startPrinting()
        }
        else if batteryPer < 11
        {
            customLoader.hideActivityIndicator()
            // This is not an error, so explicitely show it
            self.receivedPrintingError(errorCode: 8)
        }
    }
}
