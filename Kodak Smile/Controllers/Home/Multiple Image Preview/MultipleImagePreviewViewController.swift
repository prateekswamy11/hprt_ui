//
//  MultipleImagePreviewViewController.swift
//  Kodak Classic
//
//  Created by MAXIMESS152 on 10/01/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import Photos
import CopilotAPIAccess

class MultipleImagePreviewViewController: UIViewController, MultiPrintEditImageSaveDelegate
{
    //MARK:- IBOutlets
    @IBOutlet weak var collectionViewImagePreview: UICollectionView!
    @IBOutlet weak var collectionViewLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var lblItemsSelectedCount: UILabel!
    @IBOutlet weak var viewEditButton: UIView!
    @IBOutlet weak var constraintBackBtnTop: NSLayoutConstraint!
    @IBOutlet weak var btnPrint: UIButton!
    @IBOutlet weak var lblPrint: UILabel!
    @IBOutlet weak var imgVwPrint: UIImageView!
    
    //MARK:- Variables
    var indexOfCellBeforeDragging = 0
    let imageManager = PHCachingImageManager()
    var imageLocalArray: PHFetchResult<PHAsset>!
    var imageSocialArray = [String]()
    let cellImgoptions = PHImageRequestOptions()
    var imagesFrom = String()
    var thumbnailSize: CGSize!
    var selectedImages = [UIImage]()
    var selectedImageIndex = Int()
    var editedImagesIndices = [Int]()
    var printBtnClickCount = 0

    //MARK:- View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initialiseBluetoothStatusVars()
        self.changeUIAccordingToDevice()
        self.checkPrinterStatus()
        self.checkIsPrintingInProgress()
        self.initialisePrintingVars()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.multipleImagePreview.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureCollectionViewLayoutItemSize()
    }
    
    //MARK:- IBActions
    @IBAction func backBtnClicked(_ sender: UIButton)
    {
        NotificationCenter.default.removeObserver(self)
        self.navigationController?.popViewController(animated: true)
//        if appDelegate().isPrintingInProgress{
            IS_HOLD_ON_POP_SHOWN = false
//        }
    }
    
    @IBAction func goToImagePreviewVCBtnClicked(_ sender: UIButton) {
        let imagePreviewVC = storyboards.imgPreviewStoryboard.instantiateViewController(withIdentifier: "ImagePreviewViewController") as! ImagePreviewViewController
        imagePreviewVC.isComeFromMultiPrintScreen = true
        imgMainEdit = selectedImages[selectedImageIndex]
        imagePreviewVC.delegateMultiPrint = self
        self.navigationController?.pushViewController(imagePreviewVC, animated: true)
    }
    
    @IBAction func btnPrintClicked(_ sender: Any)
    {
        appDelegate().isGivenToPrint = true
        finalCopiesToCheckdelete = 0
        noOfCopies = selectedImages.count
        let tapPrintAnalyticsEvent = TapPrintAnalyticsEvent(numOfCopies: selectedImages.count, timestamp: 0)
        Copilot.instance.report.log(event: tapPrintAnalyticsEvent)
        print("Print button clicked")
        if WebserviceModelClass().isInternetAvailable() && appDelegate().isConnectedToPrinter {
                customLoader.showActivityIndicator(viewController: self.view)
                // This is called to check battery percentage before starting printing
                PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.getZIPInfo, ResponseDelegate: self))
        } else {
            let connectivityVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController
            self.navigationController?.pushViewController(connectivityVC, animated: true)
        }
    }
    
    //MARK:- Functions
    func initialisePrintingVars()
    {
        if (appDelegate().isConnectedToPrinter == true)
        {
            if (appDelegate().isPrintingInProgress == true)
            {
                // Disable print button once printing is started
                self.btnPrint.isEnabled = false
                self.btnPrint.isSelected = false
                self.lblPrint.text = "Printing...".localisedString()
                isOutOfPaper = false
                self.lblPrint.textColor = UIColor.white.withAlphaComponent(0.4)
            }
        }
    }
    
    func startPrinting()
    {
        btnPrint.isEnabled = true
        print("Starting Multiprint with count -", self.selectedImages.count)
        // Get back from the view once printing is started
        self.navigationController?.popViewController(animated: true)
        // All images ready for print are in selectedImages array
        // Create objects to be sent for printing
        let multiPrintHelper = MultiPrintHelper()
        var imageDataArray = [[String : Any]]()
        for image in selectedImages
        {
            let imageDict = ["image" :image,
                             "copies" : 1, // keeping hardcoded count 1, as multicopies not implemented
                "isTimestampOn": false,
                "isColorAdjusted" : false] as [String : Any]
            imageDataArray.append(imageDict)
        }
        multiPrintHelper.printImageDataArray = imageDataArray
        multiPrintHelper.countOfCopies = selectedImages.count//countOfCopies
        multiPrintHelper.isApplyForAll = false
        // Start the printing
        multiPrintHelper.startPrinting()
    }
    
    func saveEditedImagesInGallery()
    {
        for index in self.editedImagesIndices
        {
            if self.selectedImages.count > index
            {
                let finalImage = self.selectedImages[index]
                let saveImage = saveImageInCustomAlbum()
                print("Save image of index ", index)
                saveImage.saveImageInAlbum(name: PolaroidSnaptouchEditAlbum, withImage: finalImage)
            }
        }
    }
    
    func initialiseBluetoothStatusVars()
    {
        ConnectionStatus.sharedController.printerStatusDelegate = self
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(AccessoriesDisConnect),
            name: NSNotification.Name(rawValue: "AccessoryDisConnect"),
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(AccessoriesConnect),
            name: NSNotification.Name(rawValue: "AccessoryConnect"),
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handlePrintCompletion),
            name: NSNotification.Name(rawValue: "PrintFinished"),
            object: nil)
    }
    
    @objc func checkIsPrintingInProgress(){
             if appDelegate().isPrintingInProgress && IS_HOLD_ON_POP_SHOWN == false{
                 let vc = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpHeldOnViewController") as! PopUpHeldOnViewController
                 self.addChildViewController(vc)
                 self.view.addSubview(vc.view)
             }
         }
    
    func checkPrinterStatus()
    {
        print("Printer connection status changed")
        if (appDelegate().isConnectedToPrinter == true)
        {
            self.btnPrint.isSelected = true
            self.imgVwPrint.image = #imageLiteral(resourceName: "greenIcon")
        }
        else if (appDelegate().isConnectedToPrinter == false)
        {
            self.btnPrint.isSelected = false
            self.imgVwPrint.image = #imageLiteral(resourceName: "redIcon")
        }
    }
    
    private func calculateSectionInset() -> CGFloat {
        let deviceIsIpad = UIDevice.current.userInterfaceIdiom == .pad
        let deviceOrientationIsLandscape = UIDevice.current.orientation.isLandscape
        let cellBodyViewIsExpended = deviceIsIpad || deviceOrientationIsLandscape
        let cellBodyWidth: CGFloat = 236 + (cellBodyViewIsExpended ? 174 : 0)
        let buttonWidth: CGFloat = 50
        let inset = (collectionViewLayout.collectionView!.frame.width - cellBodyWidth + buttonWidth) / 4
        return inset
    }
    
    private func configureCollectionViewLayoutItemSize() {
        let inset: CGFloat = calculateSectionInset() // This inset calculation is some magic so the next and the previous cells will peek from the sides. Don't worry about it
        collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        let newHeight = (980 / 640) * (collectionViewLayout.collectionView!.frame.size.width - inset * 2)
        collectionViewLayout.itemSize = CGSize(width: collectionViewLayout.collectionView!.frame.size.width - inset * 2, height: newHeight) // collectionViewLayout.collectionView!.frame.size.height
    }
    
    func indexOfMajorCell() -> Int {
        let itemWidth = collectionViewLayout.itemSize.width
        let proportionalOffset = collectionViewLayout.collectionView!.contentOffset.x / itemWidth
        let index = Int(round(proportionalOffset))
        var safeIndex = Int()
        if self.imagesFrom == "Local" {
            safeIndex = max(0, min(self.imageLocalArray.count - 1, index))
        }
        else {
            safeIndex = max(0, min(self.imageSocialArray.count - 1, index))
        }
        return safeIndex
    }
    
    func getSelectedImageAsset(index : Int) -> PHAsset
    {
        var asset = PHAsset()
        asset = imageLocalArray.object(at: imageLocalArray.count - (index + 1))
        return asset
    }
    
    func initialSetup() {
        thumbnailSize = CGSize(width:1488, height: 1232)
        cellImgoptions.isNetworkAccessAllowed = true
        self.selectedImageIndex = selectedImages.count
        if self.selectedImages.count == 1{
            self.lblItemsSelectedCount.text = "\(self.selectedImages.count) "+"Item selected".localisedString()
            self.lblPrint.text = "Print".localisedString()+" \(self.selectedImages.count) "+"Image".localisedString()
        }else{
            self.lblItemsSelectedCount.text = "\(self.selectedImages.count) "+"Items Selected".localisedString()
            self.lblPrint.text = "Print".localisedString()+" \(self.selectedImages.count) "+"Images".localisedString()
        }
    }
    
    func returnImageFromSocialURL(imageView : UIImageView, index : Int) -> UIImageView
    {
        if self.imagesFrom == "Local"
        {
            imageView.image = self.selectedImages[index]
            return imageView
        }
        else
        {
            imageView.af_setImage(
                withURL: URL(string:imageSocialArray[index])!,
                imageTransition: .crossDissolve(0.2),
                completion: nil)
            return imageView
        }
    }
    
    func startLoader(){
        customLoader.showActivityIndicator(viewController: self.view)
    }
    
    func stopLoader(){
        customLoader.hideActivityIndicator()
    }
    
    func changeUIAccordingToDevice(){
        if getCurrentIphone() == "5"{
            self.constraintBackBtnTop.constant = 30.0
        }
    }
    
    // Multi print edited image delegate functions
    func getSelectedImageEditedBack(_ image: UIImage) {
        self.selectedImages[selectedImageIndex] = image
        // Save the index of edited image so that we can save edited images in camera roll
        if !self.editedImagesIndices.contains(selectedImageIndex)
        {
            self.editedImagesIndices.append(selectedImageIndex)
        }
        self.collectionViewImagePreview.reloadData()
    }
}
