//
//  MultipleImagePreviewDatasource.swift
//  Kodak Classic
//
//  Created by MAXIMESS152 on 10/01/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

extension MultipleImagePreviewViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MultipleImagePreviewCollectionViewCell", for: indexPath) as! MultipleImagePreviewCollectionViewCell
        var imageURL = ""
        cell.imgViewPreview.image = self.selectedImages[indexPath.row]
        if let multiimage = cell.imgViewPreview.image{

            if multiimage.size.width > multiimage.size.height
            {
                cell.imgViewPreview.image = multiimage.rotateImageByDegrees(90.0)
            }
        }
        
        // Sushant - code done to solve multiple selection issue.
        if selectedImageIndex == indexPath.item{
            cell.imgViewPreview.borderWidth = 4.0
            cell.imgViewPreview.borderColor = .white
        }else{
            cell.imgViewPreview.borderWidth = 0.0
            cell.imgViewPreview.borderColor = nil
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            self.viewEditButton.isHidden = false
            self.selectedImageIndex = indexPath.item
            self.collectionViewImagePreview.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        indexOfCellBeforeDragging = indexOfMajorCell()
    }
    
}
