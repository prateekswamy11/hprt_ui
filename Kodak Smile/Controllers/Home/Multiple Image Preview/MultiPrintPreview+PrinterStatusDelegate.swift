//
//  MultiPrintPreview+PrinterStatusDelegate.swift
//  Polaroid MINT
//
//  Created by maximess142 on 20/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

extension MultipleImagePreviewViewController : PrinterStatusDelegate
{
    // MARK: - Printer Connection Methods
    @objc func AccessoriesDisConnect()
    {
        self.checkPrinterStatus()
    }
    
    @objc func AccessoriesConnect()
    {
        self.checkPrinterStatus()
        PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.get_ZIP_FIRMWARE, ResponseDelegate: self))
    }
}
