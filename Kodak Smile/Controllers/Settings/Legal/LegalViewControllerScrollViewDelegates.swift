//
//  LegalViewControllerScrollViewDelegates.swift
//  Polaroid MINT
//
//  Created by MAXIMESS142 on 05/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

extension LegalViewController: UIScrollViewDelegate{
    
    //MARK: - Scroll View Delegate Functions -
    private func scrollViewWillBeginDragging(scrollView: UIScrollView)
    {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if (self.lastContentOffset < scrollView.contentOffset.y) {
            UIView.animate(withDuration: 0.15) {
                self.constraintLegalBeforeBttm.constant = -40.0
                self.lblLegalAfterScroll.alpha = 1.0
                self.lblLegalBeforeScroll.alpha = 0
            }
        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
            UIView.animate(withDuration: 0.15) {
                self.constraintLegalBeforeBttm.constant = 21.0
                self.lblLegalBeforeScroll.alpha = 1.0
                self.lblLegalAfterScroll.alpha = 0
            }
        }
    }    
}
