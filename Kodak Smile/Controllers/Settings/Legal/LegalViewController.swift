//
//  LegalViewController.swift
//  Polaroid MINT
//
//  Created by MAXIMESS142 on 03/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import WebKit

protocol CallToActiveCreateBtnDelegate {
    func callToActiveCreateBtnResponseCompletion(_ status:Bool)
}

class LegalViewController: UIViewController, WKUIDelegate ,WKNavigationDelegate {
    
    //MARK: - Outlets -
    @IBOutlet weak var lblLegalAfterScroll: UILabel!
    @IBOutlet weak var lblLegalBeforeScroll: UILabel!
   
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var btnAgree: UIButton!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var constraintLegalBeforeBttm: NSLayoutConstraint!
    @IBOutlet weak var constraintWebViewBottom: NSLayoutConstraint!
    
    //MARK: - Variables -
    var lastContentOffset: CGFloat = 0
    var isFrom = ""
    var islinkLoading = false
    // loader image
    let loaderGif = UIImage(gifName: "loader.gif")
     var delegateActiveCreateBtn : CallToActiveCreateBtnDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        self.initialSetup()
        self.createWebView()
    }
    
    //MARK: - IBActions -
    @IBAction func goBackBtnClicked(_ sender: UIButton) {
        if isFrom == "DoItLater" && islinkLoading == true {
            self.delegateActiveCreateBtn?.callToActiveCreateBtnResponseCompletion(true)
            self.btnAgree.isHidden = false
            self.btnback.isHidden = true
            self.constraintWebViewBottom.constant = self.btnAgree.isHidden ? 0 : 50.0
            islinkLoading = false
            createWebView()
        } else {
            if isComeFrom == "SignUPVC"{
                self.dismiss(animated: false, completion: nil)
                isComeFrom = ""
            }else{
             self.navigationController?.popViewController(animated: false)
            }
        }
    }
   
    @IBAction func agreeBtnClicked(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "isAcceptTermsOfUse")
        StartViewController().acceptTermsAndPolicy()
        goToHomeController()
    }
    
    //MARK: - Functions -
    func initialSetup() {
        self.lblLegalBeforeScroll.text = "Legal".localisedString()
        self.lblLegalAfterScroll.text = "Legal".localisedString()
        self.btnAgree.isHidden = true
        self.btnback.isHidden = false
        if isFrom == "DoItLater" {
            self.lblLegalBeforeScroll.text = "Terms and Conditions".localisedString()
            self.lblLegalAfterScroll.text = "Terms and Conditions".localisedString()
            self.btnAgree.setTitle("Agree Terms and Conditions", for: .normal)
            self.constraintWebViewBottom.constant = 50.0
            self.btnAgree.isHidden = false
            self.btnback.isHidden = true
        }
        if isFrom == "Login" {
            self.lblLegalBeforeScroll.text = "Privacy policy updated July 1st 2019".localisedString()
            self.lblLegalAfterScroll.text = "Privacy policy updated July 1st 2019".localisedString()
        }
    }
    
    func createWebView() {
        lblLegalAfterScroll.alpha = 0
        webView.scrollView.delegate = self
        // Adding webView content
        do {
            guard let filePath = Bundle.main.path(forResource: "legal_terms_conditions", ofType: "html")
                else {
                    // File Error
                    print ("File reading error")
                    return
            }
            let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
            let baseUrl = URL(fileURLWithPath: filePath)
            webView.loadHTMLString(contents as String, baseURL: baseUrl)
        }
        catch {
            print ("File HTML error")
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        customLoader.hideActivityIndicator()
        if !webView.isLoading
        {
            print("Finish called")
            self.view.isUserInteractionEnabled = true
           
        }
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        customLoader.hideActivityIndicator()
    }
   
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        customLoader.showActivityIndicator(viewController: self.view)
        if isFrom == "DoItLater" && islinkLoading == true {
            setupUI()
        }
    }
   
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
           if navigationAction.navigationType == WKNavigationType.linkActivated {
            if let urlStr = navigationAction.request.url?.absoluteString {
                if urlStr == "http://www.kodakphotoplus.com/" && WebserviceModelClass().isInternetAvailable() == false{
                    let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                    comeFromStrNoInternetSocialMedia = "legalNoInternet"
                    self.present(alertPopUp, animated: true, completion: nil)
                }else{
                    islinkLoading = true
                }
            }
//               decisionHandler(WKNavigationActionPolicy.allow)
               decisionHandler(.allow)
               return
           }
           print("no link")
           decisionHandler(WKNavigationActionPolicy.allow)
    }
    

//    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
////        if WebserviceModelClass().isInternetAvailable() {
//        if navigationType == .linkClicked  {
//            if request.url?.absoluteString == "http://www.kodakphotoplus.com/"  && !WebserviceModelClass().isInternetAvailable() {
//                let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
//                comeFromStrNoInternetSocialMedia = "legalNoInternet"
//                self.present(alertPopUp, animated: true, completion: nil)
//            } else {
//                islinkLoading = true
//            }
//        }
//        return true
//    }
    
    func setupUI() {
        if WebserviceModelClass().isInternetAvailable(){
            self.btnback.isHidden = false
            self.btnAgree.isHidden = true
            self.constraintWebViewBottom.constant = self.btnAgree.isHidden ? 0 : 50.0
        }
    }
}
