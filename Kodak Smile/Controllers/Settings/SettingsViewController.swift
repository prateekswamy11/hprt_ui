//
//  SettingsViewController.swift
//  Polaroid MINT
//
//  Created by MAXIMESS142 on 02/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CoreBluetooth
import AVFoundation
import CopilotAPIAccess
import CopilotLogger
var contentOffsetScrollSettings:CGFloat = 0.0

class SettingsViewController: UIViewController , CameraPermissinNativePopupDelegate{
    
    //MARK: - Outlets -
    @IBOutlet weak var lblAppName: UILabel!
    @IBOutlet weak var btnConnect: UIButton!
    
    // Connected to printer
    @IBOutlet weak var viewConnectedToPrinter: UIView!
    @IBOutlet weak var imgConnectedToPrinter: UIImageView!
    @IBOutlet weak var lblConnectedToPrinter: UILabel!
    @IBOutlet weak var imgViewBattery: UIImageView!
    
    // Connect to printer
    @IBOutlet weak var viewConnectToPrinter: UIView!
    @IBOutlet weak var lblConnectToPrinter: UILabel!
    @IBOutlet weak var imgConnectToPrinter: UIImageView!
    
    // Printer in Progress
    @IBOutlet weak var viewPrinterInProgress: UIView!
    @IBOutlet weak var lblPrinterInProgress: UILabel!
    @IBOutlet weak var imgPrinterInProgress: UIImageView!
    @IBOutlet weak var tableVwSettingsList: UITableView!
    
    // Autominimize
    @IBOutlet weak var viewBottomMenu: UIView!
    @IBOutlet weak var constraintBottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewBtnCamera: UIView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var viewTopScrollSmall: UIView!
    @IBOutlet weak var viewTopHeader: UIView!
    
    //MARK: - Variables -
    let iconImages = ["zink","tips","support-1", "legal-1","firmware-1", "about-1","social",""]
    let listNames = ["Buy KODAK SMILE ZINK Photo Paper","Quick Tips","Support", "Legal","Firmware", "About","Social Accounts",""]
    let saveSignInData:UserDefaults = UserDefaults.standard
    var bluetoothManager: CBCentralManager!
    var lastContentOffset: CGFloat = 0     // variable to save the last position visited,
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
        self.addObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initialiseStatusVars()
        self.checkConnectivityStatus()
        self.setBatteryPercentageIcon()
        self.showSolveItBtn()
//        self.showBuyPaperBtn()
        PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.get_ZIP_FIRMWARE, ResponseDelegate: self))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.settings.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("notificationFirmwareUpdateTrue"), object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIApplicationWillEnterForeground, object: nil)
    }
    
    @objc func willEnterForeground(_ notification: NSNotification!) {
        self.checkConnectivityStatus()
    }

    //MARK: - IBActions -
    @IBAction func goToCameraBtnClicked(_ sender: UIButton) {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            DispatchQueue.main.async {
                //already authorized
                let cameraVC = storyboards.cameraStoryboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                self.navigationController?.pushViewController(cameraVC, animated: false)
            }
        } else {
            
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpWeWouldLikeToOpenCameraViewController") as! PopUpWeWouldLikeToOpenCameraViewController
            alertPopUp.delegate = self
            self.present(alertPopUp, animated: true, completion: nil)
        }
    }
    
    @IBAction func goToHomeVCBtnClicked(_ sender: UIButton) {
        NotificationCenter.default.removeObserver(self)
        var isHomeVCInStack = false
        if let navVC = self.navigationController
        {
            // Pop view controller to home.
            for item in navVC.viewControllers {
                // Filter for your desired view controller:
                if item.isKind(of: HomeViewController.self) {
                    isHomeVCInStack = true
                    self.navigationController?.popToViewController(item, animated: false)
                }
            }
        }
        // When directly came from onboarding, home VC is not in stack so navigate by pushing to it
        if isHomeVCInStack == false
        {
            let homeVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
    }
    
    @IBAction func connectToPrinterBtnClicked(_ sender: UIButton) {
        let connectivityVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController
        connectivityVC.comeFromStr = "HomeVCorSettingsVC"
        self.navigationController?.pushViewController(connectivityVC, animated: true)
    }
    
    @IBAction func connectedToPrinterBtnClicked(_ sender: UIButton) {
        let connectivityVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController
        connectivityVC.comeFromStr = "HomeVCorSettingsVC"
        self.navigationController?.pushViewController(connectivityVC, animated: true)
    }
    
    @IBAction func logOutBtnClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "Logout".localisedString(), message: "Are you sure you want to logout?".localisedString(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel".localisedString(), style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Logout".localisedString(), style: .destructive, handler: { (action) in
            print("logout")
            self.performLogout()
            
        }))
        self.present(alert, animated: true, completion: nil)
        UserDefaults.standard.set(0, forKey: "eventId")
    }
    
    @IBAction func btnJoinKodakClicked(_ sender: UIButton) {
//        UserDefaults.standard.set(false, forKey: "iWillDoItLater")
        UserDefaults.standard.set(true, forKey: "isJoinKodak")
        self.logoutUser()
        UserDefaults.standard.set(0, forKey: "eventId")
    }
    
    //MARK: - Functions -
    func localizedStrings()
    {
//        self.lblAppName.text = "KODAK SMILE".localisedString()
        self.lblConnectToPrinter.text = "Disconnected".localisedString()
        self.lblConnectedToPrinter.text = "Connected".localisedString()
        self.lblPrinterInProgress.text = "Printing…".localisedString()
        self.btnConnect.setTitle("Connect".localisedString(), for: .normal)
    }
    
    func logoutUser()
    {
        customLoader.showActivityIndicator(viewController: self.view)
        self.tableVwSettingsList.isHidden = true
        self.view.isUserInteractionEnabled = false
        UserDefaults.standard.set(false, forKey: "userLogin")
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { timer in
            self.view.isUserInteractionEnabled = true
            customLoader.hideActivityIndicator()
            self.checkForNewEvent()
            let welcomeVC = storyboards.onboardingStoryboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            self.navigationController?.pushViewController(welcomeVC, animated: false)
            self.removeUserInformation()
        }
    }
    
    func performLogout() {
        if WebserviceModelClass().isInternetAvailable()
        {
//            customLoader.showActivityIndicator(viewController: self.view)
            AppManager.shared.userManager.logout { [weak self] (response) in
//                customLoader.hideActivityIndicator()
                switch response {
                case .failure(error: let error):
                    ZLogManagerWrapper.sharedInstance.logError(message: "Failed to logout with error: \(error)")
                case .success(_):
                    self?.logoutUser()
                    Copilot.instance.report.log(event: LogoutAnalyticsEvent())
                    print("logout")
                    let tapMenuItemAnalyticsEvent = TapMenuItemAnalyticsEvent(menuItem: AnalyticsValue.MenuItem.logout.rawValue, screenName: "settings(logout)")
                }
            }
        } else {
            self.logoutUser()
            Copilot.instance.report.log(event: LogoutAnalyticsEvent())
        }
    }
    
    func checkForNewEvent(){
        WebserviceModelClass().getDataForomUrl(module: "EventMessage", url: EventsUrl.message) { (success, message, data) in
            if success{
                guard let eventData = data["event_message"] as? [String: Any] else{
                    UserDefaults.standard.set(UserDefaults.standard.value(forKey: "eventId") as? Int ?? 0, forKey: "eventId")
                    return
                }
                //                if UserDefaults.standard.value(forKey: "eventId") as? Int != eventData["id"] as? Int{
                SMLEventData["message"] = eventData["message"] as! String
                SMLEventData["id"] = eventData["id"] as! Int
                let imgView = UIImageView()
                imgView.af_setImage(
                    withURL: URL(string:eventData["thumbnail"] as! String)!,
                    imageTransition: .crossDissolve(0.2),
                    completion: { (img) in
                        SMLEventData["image"] = img.value!
                })
                //                }else{
                //                    AppManager.shared.appUpgradeManager.checkIfUpgradeIsAvailable(with: self)
                //                }
            }
        }
    }
    
    //Removes user information for logout
    func removeUserInformation(){
        // remove user data
        saveSignInData.removeObject(forKey: "emailKodak")
        saveSignInData.removeObject(forKey: "passwordKodak")
        saveSignInData.removeObject(forKey: "switchStateKodak")
        saveSignInData.removeObject(forKey: "isLoggedInToDropboxKodak")
        saveSignInData.removeObject(forKey: "isLoggedInToFacebookKodak")
        saveSignInData.removeObject(forKey: "isLoggedInToInstagramKodak")
        saveSignInData.removeObject(forKey: "isLoggedInToGoogleKodak")
        saveSignInData.removeObject(forKey: "fetchFbAlbum")
        saveSignInData.removeObject(forKey: "userNameLoggedIn")
        saveSignInData.synchronize()
        // Clearing cache or deleting data for the user after logging out.
        DBLayer().deleteGooglePhotosUrl()
        DBLayer().deleteGoogleAlbums()
        DBLayer().deleteGooglePhotos()
        DBLayer().deleteDropboxAlbums()
        DBLayer().deleteFacebookAlbums()
        DBLayer().deleteFacebookPhotos()
        DBLayer().deleteInstagramPhotos()
        GoogleHelper().logoutOffGoogle()
        InstagramHelper().logoutOffInstagram()
        FacebookHelper().logoutOffFacebook()
//        DropboxHelper().logoutDropbox()
    }
    
    func initialiseStatusVars()
    {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(AccessoriesDisConnect),
            name: NSNotification.Name(rawValue: "AccessoryDisConnect"),
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(AccessoriesConnect),
            name: NSNotification.Name(rawValue: "AccessoryConnect"),
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.printFinishCalled),
            name: NSNotification.Name(rawValue: "PrintFinished"),
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.printStartedCalled),
            name: NSNotification.Name(rawValue: "PrintStarted"),
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.setBatteryPercentageIcon),
            name: NSNotification.Name(rawValue: "batteryPercentageChanged"),
            object: nil)
    }
    
    func addObservers()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadSettingsTable), name: Notification.Name("notificationFirmwareUpdateTrue"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground(_:)), name: .UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSolveItBtn), name: NSNotification.Name("ShowSolveIt"), object: nil)
    }
    
    func checkConnectivityStatus()
    {
        self.viewConnectedToPrinter.isHidden = true
        self.viewConnectToPrinter.isHidden = true
        self.viewPrinterInProgress.isHidden = true
        if (appDelegate().isConnectedToPrinter == true)
        {
            if (appDelegate().isPrintingInProgress == true)
            {
                self.viewPrinterInProgress.isHidden = false
            }
            else
            {
                self.viewConnectedToPrinter.isHidden = false
                //self.imgDotConnectedToPrinter.backgroundColor = UIColor(red: 103.0/256, green: 192.0/256, blue: 119.0/256, alpha: 1.0)//green
            }
            self.tableVwSettingsList.reloadData()
        }
        else if (appDelegate().isConnectedToPrinter == false)
        {
            self.viewConnectToPrinter.isHidden = false
            self.tableVwSettingsList.reloadData()
            //self.imgDotConnectedToPrinter.backgroundColor = UIColor(red: 241.0/256, green: 88.0/256, blue: 73.0/256, alpha: 1.0)//red
        }
        if isOutOfPaper{
            self.showBuyPaperBtn()
        }else{
            self.hideBuyPaperBtn()
        }
    }
    
    func showBuyPaperVC()
    {
        if !WebserviceModelClass().isInternetAvailable()
        {
            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
             comeFromStrNoInternetSocialMedia = "buyPaperNoInternet"
            self.present(alertPopUp, animated: true, completion: nil)
        }
        else
        {
            let tapPurchasePaperAnalyticsEvent = TapPurchasePaperAnalyticsEvent()
            Copilot.instance.report.log(event: tapPurchasePaperAnalyticsEvent)
            
            let buyPaperVC = self.storyboard?.instantiateViewController(withIdentifier: "BuyPaperViewController") as! BuyPaperViewController
            self.navigationController?.pushViewController(buyPaperVC, animated: true)
        }
    }
    func showCameraPermissionPopup() {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
            DispatchQueue.main.async(execute: {
                let cameraVC = storyboards.cameraStoryboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                self.navigationController?.pushViewController(cameraVC, animated: false)
            })
        }else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    DispatchQueue.main.async(execute: {
                        UserDefaults.standard.set(true, forKey: "onboardingSkipped")
                        let cameraVC = storyboards.cameraStoryboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                        self.navigationController?.pushViewController(cameraVC, animated: false)
                    })
                } else {
                    //access denied
                    if UserDefaults.standard.bool(forKey: "enableCameraAccessCount") {
                        DispatchQueue.main.async {
                            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
                            UIApplication.shared.open(settingsUrl)
                        }
                    }
                    UserDefaults.standard.set(true, forKey: "enableCameraAccessCount")
                }
            })
        }
    }
    @objc func reloadSettingsTable()
    {
        self.tableVwSettingsList.reloadData()
    }
}

