//
//  QuickTipsViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 11/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class QuickTipsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lblWelcomeToKodak: UILabel!
    @IBOutlet weak var tblQuickTips: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblQuickTips: UILabel!
    @IBOutlet weak var btnWelcomeKodak: UIButton!
    
    //MARK: - Variables -
    let listNames = ["How to connect my printer".localisedString(),"How to load KODAK SMILE ZINK Photo Paper".localisedString(),"Welcome to KODAK".localisedString()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let lblString = "Quick Tips".localisedString()
        self.lblTitle.text = ""
        self.lblQuickTips.text = lblString
        self.tblQuickTips.tableFooterView = UIView(frame: .zero)
    }
    
    //MARK: - UITableViewDelegate & UITableViewDataSource -
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let quickTipsCell : QuickTipsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "QuickTipsTableViewCell", for: indexPath) as! QuickTipsTableViewCell
        quickTipsCell.lblListNames.text = listNames[indexPath.row]
        if(indexPath.row  == 2) {
            quickTipsCell.lblWelcomeToKodak.font = UIFont(name: "SFProText-Regular", size: 26)
            quickTipsCell.btnForward.isHidden = true
            quickTipsCell.lblWelcomeToKodak.isHidden = false
            quickTipsCell.lblListNames.isHidden = true
        }
        return quickTipsCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let printerVC = storyboards.QuickTips.instantiateViewController(withIdentifier: "QuickTipsPrinterConnectViewController") as! QuickTipsPrinterConnectViewController
            self.navigationController?.pushViewController(printerVC, animated: true)
        } else if indexPath.row == 1{
            let printerVC = storyboards.QuickTips.instantiateViewController(withIdentifier: "QuickTipsLoadPaperViewController") as! QuickTipsLoadPaperViewController
            self.navigationController?.pushViewController(printerVC, animated: true)
        }else if indexPath.row == 2 {
            SingletonClass.sharedInstance.actionInitiatedByUser = true
            let printerOnboardingVC = storyboards.printerOnBoardingStoryboard.instantiateInitialViewController()
            self.navigationController?.present(printerOnboardingVC!, animated: false, completion: nil)
        }
    }
    
    //MARK: - IBActions -
    @IBAction func goBackBtnClicked(_ sender: UIButton) {
        if SingletonClass.sharedInstance.actionInitiatedByUser{
            var isSettingVCInStack = false
            if let navVC = self.navigationController
            {
                // Pop view controller to home.
                for item in navVC.viewControllers {
                    // Filter for your desired view controller:
                    if item.isKind(of: SettingsViewController.self) {
                        isSettingVCInStack = true
                        self.navigationController?.popToViewController(item, animated: false)
                    }
                }
            }
            // When directly came from onboarding, home VC is not in stack so navigate by pushing to it
            if isSettingVCInStack == false
            {
                let settingsVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
                self.navigationController?.pushViewController(settingsVC, animated: false)
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}


