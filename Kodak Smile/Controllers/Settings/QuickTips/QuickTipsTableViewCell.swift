//
//  QuickTipsTableViewCell.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 11/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class QuickTipsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblWelcomeToKodak: UILabel!
    @IBOutlet weak var lblListNames: UILabel!
    @IBOutlet weak var btnForward: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
