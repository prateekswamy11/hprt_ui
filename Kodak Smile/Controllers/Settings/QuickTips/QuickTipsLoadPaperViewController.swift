//
//  QuickTipsLoadPaperViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 11/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class QuickTipsLoadPaperViewController: UIViewController {

    //MARK: - Outlets -
    @IBOutlet weak var constraintLblNavigationTop: NSLayoutConstraint!
    @IBOutlet weak var constraintTopLoaderImgView: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightLoaderImgView: NSLayoutConstraint!
    @IBOutlet weak var lblLoadPrinter: UILabel!
    @IBOutlet weak var lblStep: UILabel!
    @IBOutlet weak var imgViewLoadPrinter: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
        self.detectIphone5sDevice()
        self.showGifLoader()
        // Do any additional setup after loading the view.
    }
    
    //Assign values to fields.
    func localizedStrings() {
        self.lblLoadPrinter.text = "Load your printer with Zink Paper".localisedString()
        self.lblStep.text = "Open the top and place the pack of KODAK SMILE ZINK Photos Paper inside, blue sheet facing down".localisedString()
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
          //  constraintLblNavigationTop.constant = 30.0
            constraintTopLoaderImgView.constant = 0.0
            constraintHeightLoaderImgView.constant = 150.0
        }else{
            print("Unknown")
        }
    }
    
    func showGifLoader()
    {
        let gif = UIImage(gifName: "paper-load_17_12.gif")
        imgViewLoadPrinter.setGifImage(gif)
        imgViewLoadPrinter.startAnimatingGif()
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { timer in
        }
    }
    
    // MARK: - Action Methods
    @IBAction func backBtnClicked(_ sender:Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
}
