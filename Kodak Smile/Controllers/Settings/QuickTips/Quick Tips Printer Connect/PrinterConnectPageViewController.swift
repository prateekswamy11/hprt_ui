//
//  PrinterConnectPageViewController.swift
//  Polaroid MINT
//
//  Created by maximess142 on 12/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PrinterConnectPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    //MARK: - Variables -
    var subViewControllers:[UIViewController] = {
        return[
            storyboards.QuickTips.instantiateViewController(withIdentifier: "HavingTroubleOne_ViewController") as! HavingTroubleOne_ViewController,
            
            storyboards.QuickTips.instantiateViewController(withIdentifier: "HavingTroubleViewController") as! HavingTroubleViewController,
            
            storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController,
        ]
    }()
    let appearance = UIPageControl.appearance()
    
    //MARK: - View LifeCycle Functions -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        setViewControllers([subViewControllers[0]], direction: .forward, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addingNotificationObservers()
        goToQuickTipsOnboarding = "quickTipsOnboarding"
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        goToQuickTipsOnboarding = ""
    }
    
    //MARK: - Functions to trigger notifications' functions -
    @objc func goToBackToFirstPage()
    {
        setViewControllers([subViewControllers[0]], direction: .reverse, animated: true, completion: nil)
    }
    
    @objc func goTofindPrinterPage()
    {
        setViewControllers([subViewControllers[2]], direction: .forward, animated: true, completion: nil)
    }
    
    @objc func goBackFromfindPrinterPage()
    {
        setViewControllers([subViewControllers[1]], direction: .reverse, animated: true, completion: nil)
    }
    
    //MARK: - Functions -
    func addingNotificationObservers()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(goToBackToFirstPage), name: Notification.Name("notificationBackToFirstPage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(goTofindPrinterPage), name: Notification.Name("notificationToFindPrinterQuickTips"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(goBackFromfindPrinterPage), name: Notification.Name("notificationBackFromFindPrinter"), object: nil)
    }
    
    private func setupPageControl() {
        appearance.pageIndicatorTintColor = UIColor(red: 151/255, green: 156/255, blue: 142/255, alpha: 0.3)
        appearance.currentPageIndicatorTintColor = UIColor(red: 237/255, green: 0/255, blue: 0/255, alpha: 1.0)
        appearance.backgroundColor = UIColor.white
    }
    
    //MARK: - UIPageViewControllerDataSource -
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        self.setupPageControl()
        return subViewControllers.count
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = subViewControllers.index(of: viewController) ?? 0
        if currentIndex <= 0
        {
            return nil
        }
        return subViewControllers[currentIndex-1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = subViewControllers.index(of: viewController) ?? 0
        if currentIndex >= subViewControllers.count-1
        {
            return nil
        }
        return subViewControllers[currentIndex+1]
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        let currentIndex = subViewControllers.index(of: (pageViewController.viewControllers?.first)!)
        return currentIndex!
    }
    
}
