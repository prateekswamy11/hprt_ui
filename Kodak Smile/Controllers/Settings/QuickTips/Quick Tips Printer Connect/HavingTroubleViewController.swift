//
//  HavingTroubleViewController.swift
//  Polaroid MINT
//
//  Created by maximess142 on 12/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class HavingTroubleViewController: UIViewController
{
    
    let attributedString = NSMutableAttributedString(string: "Don’t have a printer yet? Kodakphotoplus.com".localisedString(), attributes: [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .regular),
        .foregroundColor: UIColor(white: 1.0, alpha: 1.0),
        .kern: 0.0
        ])
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblNavigationTop: NSLayoutConstraint!
    @IBOutlet weak var constraintImgSmileWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintImgSmileHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnDontHaveBottom: NSLayoutConstraint!
    @IBOutlet weak var lblWelComeText: UILabel!
    @IBOutlet weak var lblConnectPrinter: UILabel!
    @IBOutlet weak var btnFindPrinter: UIButton!
    @IBOutlet weak var btnDontHave: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.attributedString.addAttributes([
            .font: UIFont.systemFont(ofSize: 12.0, weight: .bold),
            .foregroundColor: UIColor.init(red: 252, green: 183, blue: 20)
            ], range: NSRange(location: 26, length: 18))
        self.localizedStrings()
       // self.detectIphone5sDevice()
    }
    
    //Assign values to fields.
    func localizedStrings() {
        self.lblWelComeText.text = "Having trouble\nconnecting to your printer?".localisedString()
        self.lblConnectPrinter.text = "Don’t worry, we’ve got you covered".localisedString()
        self.btnFindPrinter.setTitle( "Find My Printer".localisedString(), for: .normal)
//        self.btnDontHave.setTitle( "Don’t have a printer yet?".localisedString(), for: .normal)
        self.btnDontHave.setAttributedTitle(attributedString, for: .normal)
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblNavigationTop.constant = 30.0
            constraintImgSmileWidth.constant = 110.0
            constraintImgSmileHeight.constant = 125.0
            constraintBtnDontHaveBottom.constant = 20.0
        }else{
            print("Unknown")
        }
    }
    
    // MARK: - Action Methods
    @IBAction func backBtnClicked(_ sender:Any)
    {
        NotificationCenter.default.post(name: Notification.Name("notificationBackToFirstPage"), object: nil)
    }
    
    @IBAction func btnFindPrinterClicked(_ sender: Any)
    {
        NotificationCenter.default.post(name: Notification.Name("notificationToFindPrinterQuickTips"), object: nil)
    }
    
    @IBAction func btnDontHaveClicked(_ sender: Any)
    {
        let buyPrinterVC = storyboards.printerOnBoardingStoryboard.instantiateViewController(withIdentifier: "BuyPrinterViewController") as! BuyPrinterViewController
        self.navigationController?.pushViewController(buyPrinterVC, animated: true)
    }
}
