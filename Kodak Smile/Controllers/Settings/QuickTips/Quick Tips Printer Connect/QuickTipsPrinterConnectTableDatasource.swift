//
//  QuickTipsPrinterConnectTableDatasource.swift
//  Polaroid MINT
//
//  Created by maximess142 on 12/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation

extension QuickTipsPrinterConnectViewController : UITableViewDelegate, UITableViewDataSource 
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return listNames.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
         return self.view.frame.width * 0.33
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let otherCell : PrinterNotFoundTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PrinterNotFoundTableViewCell", for: indexPath) as! PrinterNotFoundTableViewCell
        otherCell.imgVwIcons.image = UIImage(named: iconImages[indexPath.row])
       // otherCell.lblRowCount.backgroundColor = bgColors[indexPath.row]
        otherCell.lblSuggestion.text = listNames[indexPath.row].localisedString()
       // otherCell.lblRowCount.text = "\(indexPath.row+1)"
        return otherCell
    }
}
