//
//  QuickTipsPrinterConnectViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 11/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
var goToQuickTipsOnboarding = String()
class QuickTipsPrinterConnectViewController: UIViewController{
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblNavigationTop: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSearchAgain: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnLetGoTogether: UIButton!
    @IBOutlet weak var tblHelp: UITableView!
    @IBOutlet weak var viewBottomGradient: UIView!
    
    //MARK: - Variables -
    var lastContentOffset: CGFloat = 0
    let iconImages = ["bluetooth-1", "printerTurnedOn-1", "plugPowerSource", "printerDevice-1"]
    let bgColors = [#colorLiteral(red: 0, green: 0.5764705882, blue: 0.8392156863, alpha: 1), #colorLiteral(red: 0.4039215686, green: 0.7529411765, blue: 0.4666666667, alpha: 1), #colorLiteral(red: 0.9450980392, green: 0.3450980392, blue: 0.2862745098, alpha: 1), #colorLiteral(red: 0.5803921569, green: 0.4117647059, blue: 0.6823529412, alpha: 1)]
    let listNames = ["First, check that your BLUETOOTH is turned on", "Make sure your printer is turned on","Plug your printer into a power source", "Place your printer close to your mobile device"]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
        //self.detectIphone5sDevice()
        self.tblHelp.tableFooterView = UIView(frame: .zero)
    }

    //Assign values to fields.
    func localizedStrings() {
        self.lblTitle.text = "How to connect your printer".localisedString()
        self.btnLetGoTogether.setTitle( "Search Again".localisedString(), for: .normal)
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblNavigationTop.constant = 30.0
            print("Unknown")
        }
    }
    
    private func scrollViewWillBeginDragging(scrollView: UIScrollView)
    {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if (self.lastContentOffset < scrollView.contentOffset.y) {
            // moved to top
                self.constraintLblNavigationTop.constant = 17.0
                self.lblTitle.text = "Connect your printer".localisedString()
                self.lblTitle.font = lblTitle.font.withSize(18)
            
        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
            // moved to bottom
                self.constraintLblNavigationTop.constant = 100.0
                self.lblTitle.text = "How to connect your printer".localisedString()
                self.lblTitle.font = lblTitle.font.withSize(30)
        }
    }
    
    //MARK: - IBActions -
    @IBAction func goBackBtnClicked(_ sender: UIButton) {
        goToQuickTipsOnboarding = ""
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - IBActions -
    @IBAction func letsDoTogetherBtnClicked(_ sender: UIButton)
    {
        // Go to PrinterOnboarding screens.
        let printerConnectPageVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController
        goToQuickTipsOnboarding = "quickTipsOnboarding"
        self.navigationController?.pushViewController(printerConnectPageVC, animated: true)
//        NotificationCenter.default.post(name: Notification.Name("notificationToFindPrinterQuickTips"), object: nil)        
    }
}
