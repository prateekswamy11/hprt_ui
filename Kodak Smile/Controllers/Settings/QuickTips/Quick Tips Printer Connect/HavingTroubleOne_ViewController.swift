//
//  HavingTroubleOne_ViewController.swift
//  Polaroid MINT
//
//  Created by MacMini001 on 14/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class HavingTroubleOne_ViewController: UIViewController {

    //MARK: - Outlets -
    @IBOutlet weak var constraintImgSmileHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintImgSmileWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintLblNavigationTop: NSLayoutConstraint!
    @IBOutlet weak var lblHavingTrouble: UILabel!
    @IBOutlet weak var lblDontWorry: UILabel!
    @IBOutlet weak var lblTurnOnBluetooth: UILabel!
    @IBOutlet weak var lblBluetoothText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
       // self.detectIphone5sDevice()
    }
    
    //MARK: - Functions -
    
    //Assign values to fields.
    func localizedStrings() {
        self.lblHavingTrouble.text = "Having trouble\nconnecting to your printer?".localisedString()
        self.lblDontWorry.text = "Don’t worry, we’ve got you covered".localisedString()
        self.lblTurnOnBluetooth.text = "Turn on your BLUETOOTH".localisedString()
        self.lblBluetoothText.text = "Your phone uses BLUETOOTH to connect to your printer. Head over to your control center or settings and turn on your BLUETOOTH.".localisedString()
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    @IBAction func backBtnClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblNavigationTop.constant = 30.0
            constraintImgSmileWidth.constant = 110.0
            constraintImgSmileHeight.constant = 125.0
        }else{
            
            print("Unknown")
        }
    }
}
