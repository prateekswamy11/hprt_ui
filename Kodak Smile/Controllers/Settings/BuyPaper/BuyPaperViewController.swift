//
//  BuyPaperViewController.swift
//  Polaroid MINT
//
//  Created by MAXIMESS142 on 08/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import WebKit

class BuyPaperViewController: UIViewController, WKUIDelegate ,WKNavigationDelegate{

    //MARK: - Outlets -
    
    @IBOutlet weak var webView: WKWebView!
    
    @IBOutlet weak var lblPurchasePaper: UILabel!
    // loader image
    let loaderGif = UIImage(gifName: "loader.gif")
    
    //MARK: - Variables -
 //   var url = "https://amzn.to/2FFc8zk"
    
    override func viewDidLoad() {
        super.viewDidLoad()
         createWebView()
        // Do any additional setup after loading the view.
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        lblPurchasePaper.text = "Purchase Paper".localisedString()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IS_OUT_OF_PAPER_POP_SHOWN = false
        isOutOfPaper = false

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    // MARK: - IBActions -
    @IBAction func backBtnClicked(_ sender:Any)
    {
        if webView.isLoading {
            webView.stopLoading()
        }
    
        self.webView.uiDelegate = nil
        self.webView = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Functions
    func createWebView() {
        customLoader.showActivityIndicator(viewController: self.view)
        //Document file url
        let docUrl = NSURL(string: "https://www.amazon.com/Kodak-Sticky-Backed-Photo-Paper-Sheets/dp/B075WY7RRY/ref=pd_bxgy_421_img_2?_encoding=UTF8&psc=1&refRID=TNK8ZR1WVBM4EC8J0JSA")
        let req = NSURLRequest(url: docUrl! as URL)
        //here is the sole part
//        webView.scalesPageToFit = true
        webView.contentMode = .scaleAspectFit
        webView.load(req as URLRequest)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        customLoader.hideActivityIndicator()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        customLoader.stopGifLoader()
    }
    
}
