//
//  AboutViewController.swift
//  Polaroid MINT
//
//  Created by MAXIMESS142 on 08/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    //MARK: - Outlets -
    
    @IBOutlet weak var txtVwAboutUs: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.txtVwAboutUs.text =
            "Kodak\nBrand Licensee\nC&A Marketing, Inc.\n\nAbout C&A Marketing, Inc.\nHeadquatered in Edison, New Jersey, with offices in London, China,and around the world, C&A Marketing engages in the design, manufacturing and distribution of consumer products and photographic equipment. We offer one of the industry's most extensive and diverse inventories of products, ranging from innovative electronics to intelligent housewares to fun gadgets.Buildings upon our expertise in the camera business and photographic product development, we are the authorized licensee for the KODAK SMILE INSTANT DIGITAL PRINTER.\n\nC&A Marketing continues to enjoy unprecedented growth as one of the world's largest and most diverse online retailers, expanding our portfolio of brands and offering consumers imaginative, high-quality products that exceed their expectations.\n\nFor more information about our company, Please visit www.caglobal.com".localisedString()
//        txtVwAboutUs.setLineSpacing(lineSpacing: 7.0, lineHeightMultiple: 0.0)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 7.0
        let attributes = [NSAttributedStringKey.paragraphStyle : style]
        txtVwAboutUs.attributedText = NSAttributedString(string: txtVwAboutUs.text, attributes: attributes)
        txtVwAboutUs.textAlignment = .justified
        txtVwAboutUs.textColor = .white
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    //MARK: - IBActions -
    @IBAction func btnGoBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
