//
//  SettingsViewController+Connectivity.swift
//  Kodak Smile
//
//  Created by maximess142 on 10/04/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

extension SettingsViewController: ResponceDelegate
{
    // MARK: - Printer Connection Methods
    @objc func AccessoriesDisConnect()
    {
        UserDefaults.standard.set(false, forKey: "cancelFirmwarePopUP")
        IS_GET_FIRMWARE = true
        self.checkConnectivityStatus()
        self.showSolveItBtn()
    }
    
    @objc func AccessoriesConnect()
    {
        self.checkConnectivityStatus()
        PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.get_ZIP_FIRMWARE, ResponseDelegate: self))
        isInitailConnect = false
    }
    
    @objc func printStartedCalled()
    {
        self.checkConnectivityStatus()
    }
    
    @objc func printFinishCalled()
    {
        //        appDelegate().isPrintingInProgress = false
        self.checkConnectivityStatus()
    }
    
    @objc func showSolveItBtn() {
        if !isInitailConnect{
            self.btnConnect.setTitle("Solve it".localisedString(), for: .normal)
        }else {
            self.btnConnect.setTitle("Connect".localisedString(), for: .normal)
        }
    }
    
    @objc func showBuyPaperBtn() {
        self.lblConnectedToPrinter.text = "Out of paper".localisedString()
    }
    
    @objc func hideBuyPaperBtn() {
        self.lblConnectedToPrinter.text = "Connected".localisedString()
    }
    // MARK: - Printer Response Delegate
    @objc func setBatteryPercentageIcon()
    {
        //print("Updating battery icon in settings")
        self.imgViewBattery.isHidden = false
        let batteryPercentage = modelPrinterDetails.batteryPercentage
        switch batteryPercentage {
        case 0:
            self.imgViewBattery.image = nil//UIImage.init(imageLiteralResourceName: "battery_0")
            break
        case 1..<10:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_1")
            break
        case 10..<20:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_2")
            break
        case 20..<30:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_3")
            break
        case 30..<40:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_4")
            break
        case 40..<50:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_5")
            break
        case 60..<70:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_6")
            break
        case 70..<80:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_7")
            break
        case 80..<90:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_8")
            break
        default:
            self.imgViewBattery.image = UIImage.init(imageLiteralResourceName: "battery_8")
            break
        }
    }
}
