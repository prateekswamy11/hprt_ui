//
//  SocialAccountViewControllerExtension.swift
//  Kodak Smile
//
//  Created by MacMini002 on 4/9/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import GoogleSignIn
import GTMSessionFetcher
import GoogleAPIClientForREST


extension SocialAccountViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let accessToken = user.authentication.accessToken
            customLoader.showActivityIndicator(viewController: self.view)
            //self.view.isUserInteractionEnabled = false
            UserDefaults.standard.setValue(true, forKey: "isLoggedInToGoogleKodak")
            let fullName = user.profile.name
            UserDefaults.standard.setValue(fullName, forKey: "googleUserName")
            UserDefaults.standard.setValue(userId, forKey: "googleUserId")
            UserDefaults.standard.setValue(user.authentication.refreshToken, forKey: "googleRefreshToken")
            UserDefaults.standard.setValue(user.authentication.accessToken, forKey: "googleAccessToken")
            NotificationCenter.default.post(name: NSNotification.Name("reloadSocialMedia"), object: nil)
            UserDefaults.standard.synchronize()
            //            customLoader.hideActivityIndicator()
            // Fetch google photos
            let strUrl = "https://photoslibrary.googleapis.com/v1/mediaItems?alt=json&pageSize=100&access_token=\(accessToken!)"
            GoogleHelper().getAlbums(accessToken: accessToken!, strUrl: strUrl)
        }
        else
        {
            print("\(error.localizedDescription)")
        }
    }
    
    func configureGooglePhotos()
    {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().scopes = ["https://www.googleapis.com/auth/photoslibrary"]
    }
    
    @objc func googleImagesFetched(noti : Notification)
    {
        DispatchQueue.main.async(execute:
            {
                customLoader.hideActivityIndicator()
        })
    }
    
    
    func networkReachability() {
        reachability = Reachability()
        NotificationCenter.default.addObserver(self, selector:#selector(self.checkNetworkStatus), name: ReachabilityChangedNotification, object: nil);
        do {
            try reachability?.startNotifier()
        } catch {
            print("This is not working.")
            return
        }
    }
    
    @objc func checkNetworkStatus()
    {
        networkStatus = Reachability()?.currentReachabilityStatus
        if (networkStatus == Reachability.NetworkStatus.notReachable)
        {
            print("Not Reachable")
            DispatchQueue.main.async {
                if self.networkStatus.description == "No Connection"{
                    customLoader.hideActivityIndicator()
                    //FacebookHelper().logoutOffFacebook()
                    //InstagramHelper().logoutOffInstagram()
                    GoogleHelper().logoutOffGoogle()
                }
                self.updateUserNameData()
            }
        }
        else
        {
           
        }
    }
    
}
