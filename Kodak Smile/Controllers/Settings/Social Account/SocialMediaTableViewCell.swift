//
//  SocialMediaTableViewCell.swift
//  Polaroid MINT
//
//  Created by MAXIMESS183 on 07/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

class SocialMediaTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSocialMediaName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnLoginStatus: UIButton!
    @IBOutlet weak var constraintHeightSocialMediaName: NSLayoutConstraint!
    @IBOutlet weak var vWSocialMediaCell: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
