//
//  SocialAccountViewController.swift
//  Polaroid MINT
//
//  Created by MAXIMESS183 on 07/03/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import GoogleSignIn
import GTMSessionFetcher
import GoogleAPIClientForREST

class SocialAccountViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    //MARK:- Outlets
    @IBOutlet weak var tableVwSocialAccountList: UITableView!
    @IBOutlet weak var lblSocialAccount: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    //MARK: - Variables
    let loaderGif = UIImage(gifName: "loader.gif")
    let listSocialMediaName  = ["FACEBOOK","INSTAGRAM","GOOGLE"]
    var listUserNameArray = ["","",""]
    var listSignInArray = [String]()
    var LoginStatusTimer = Timer()
    var reachability : Reachability!
    var networkStatus : Reachability.NetworkStatus!
    var rechabilityObserver: ReachabilityHandler?
    
    //MARK:- life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblSocialAccount.text = "Social Accounts".localisedString()
        self.updateUserNameData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateUserNameData), name: NSNotification.Name("reloadSocialMedia"), object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.googleImagesFetched(noti:)),
            name: NSNotification.Name(rawValue: "googleImagesFetched"),
            object: nil)
        self.tableVwSocialAccountList.tableFooterView = UIView(frame: .zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.networkReachability()
        self.configureGooglePhotos()
    }
    
    @objc func updateUserNameData(){
        if UserDefaults.standard.value(forKey: "fetchFbAlbum") as? Bool == true
        {
            listUserNameArray .insert(UserDefaults.standard.value(forKey: "facebookUserName") as! String, at: 0)
        }
        else
        {
            listUserNameArray .insert("", at: 0)
        }
        if UserDefaults.standard.value(forKey: "isLoggedInToInstagramKodak") as? Bool == true        {
            listUserNameArray .insert(UserDefaults.standard.value(forKey: "instagramUserName") as! String, at: 1)
        }else
        {
            listUserNameArray .insert("", at: 1)
        }
        if UserDefaults.standard.value(forKey: "isLoggedInToGoogleKodak") as? Bool == true
        {
            listUserNameArray .insert(UserDefaults.standard.value(forKey: "googleUserName") as! String, at: 2)
        }
        else
        {
            listUserNameArray .insert("", at: 2)
        }
        self.tableVwSocialAccountList.reloadData()
    }
    
    //MARK: - UITableViewDelegate & UITableViewDataSource -
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listSocialMediaName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let socialMediaCell : SocialMediaTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SocialMediaTableViewCell", for: indexPath) as! SocialMediaTableViewCell
        socialMediaCell.lblSocialMediaName.text = listSocialMediaName[indexPath.row].localisedString()
        socialMediaCell.lblUserName.text = listUserNameArray[indexPath.row].localisedString()
        socialMediaCell.btnLoginStatus.tag = indexPath.row
        socialMediaCell.btnLoginStatus.addTarget(self, action: #selector(signUpButtonSelected), for: .touchUpInside)
        if socialMediaCell.lblUserName.text == ""
        {
            socialMediaCell.btnLoginStatus.setTitle("Sign in".localisedString(), for: .normal)
            socialMediaCell.btnLoginStatus.setTitleColor(UIColor(red: 10, green: 165, blue: 190), for: .normal)
            socialMediaCell.constraintHeightSocialMediaName.constant = 0
        }
        else{
            socialMediaCell.btnLoginStatus.setTitle("Sign out".localisedString(), for: .normal)
            socialMediaCell.btnLoginStatus.setTitleColor(UIColor(red: 10, green: 165, blue: 190), for: .normal)
            socialMediaCell.constraintHeightSocialMediaName.constant = 32
        }
        return socialMediaCell
    }
    
    @IBAction func btnGoBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func signUpButtonSelected(sender: UIButton){
        print(sender.tag)
        if sender.tag == 0 && sender.titleLabel?.text == "Sign in".localisedString()
        {
            if WebserviceModelClass().isInternetAvailable() {
                isFacebookLogin = false
                FacebookHelper().loginToFacebook(viewController: self)
            }
            else {
                let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                comeFromStrNoInternetSocialMedia = "facebookNoInternet"
                self.present(alertPopUp, animated: true, completion: nil)
            }
        }
        else if sender.tag == 0 && sender.titleLabel?.text == "Sign out".localisedString()
        {
            FacebookHelper().logoutOffFacebook()
        }
        
        if sender.tag == 1 && sender.titleLabel?.text == "Sign in".localisedString()
        {
            if WebserviceModelClass().isInternetAvailable() {
                InstagramHelper().loginToInstagram(viewController: self)
            }
            else {
                let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                comeFromStrNoInternetSocialMedia = "instagramNoInternet"
                self.present(alertPopUp, animated: true, completion: nil)
            }
        }
        else if sender.tag == 1 && sender.titleLabel?.text == "Sign out".localisedString()
        {
            InstagramHelper().logoutOffInstagram()
        }
        if sender.tag == 2 && sender.titleLabel?.text == "Sign in".localisedString()
        {
            if WebserviceModelClass().isInternetAvailable() {
                if UserDefaults.standard.value(forKey: "googleUserName") as? String != "" {
                    //To Check google access token expire or not. If it expire it refresh access token.
                    if let expireDate = UserDefaults.standard.value(forKey: "googleAccessTokenExpirationDate") as? NSDate {
                        let currentDate = NSDate()
                        let compareResult = currentDate.compare(expireDate as Date)
                        if compareResult == ComparisonResult.orderedDescending
                        {
                            print("Google AccessToken Expired")
                            GoogleHelper().refreshAccessTokenAndFetchAlbums()
                        } else {
                            print("Google AccessToken not Expired")
                            GIDSignIn.sharedInstance().presentingViewController = self
                            GIDSignIn.sharedInstance().delegate = self
                            GIDSignIn.sharedInstance()?.signIn()
                        }
                    } else {
                        GIDSignIn.sharedInstance().presentingViewController = self
                        GIDSignIn.sharedInstance().delegate = self
                        GIDSignIn.sharedInstance()?.signIn()
                    }
                } else {
                    GIDSignIn.sharedInstance().presentingViewController = self
                    GIDSignIn.sharedInstance().delegate = self
                    GIDSignIn.sharedInstance()?.signIn()
                }
            }
            else {
                let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                comeFromStrNoInternetSocialMedia = "googleNoInternet"
                self.present(alertPopUp, animated: true, completion: nil)
            }
        }
        else if sender.tag == 2 && sender.titleLabel?.text == "Sign out".localisedString()
        {
            GoogleHelper().logoutOffGoogle()
        }
        self.updateUserNameData()
    }
}
