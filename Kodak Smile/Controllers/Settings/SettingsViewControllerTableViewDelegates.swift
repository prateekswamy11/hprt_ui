//
//  SettingsViewControllerTableViewDelegates.swift
//  Polaroid MINT
//
//  Created by MAXIMESS142 on 02/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CopilotAPIAccess

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 7{
            if let _ = UserDefaults.standard.object(forKey: "emailKodak") {
                return 50.0
            }
            else if UserDefaults.standard.bool(forKey: "iWillDoItLater"){
                return 50.0
            }else{
                return 0
            }
            
        }else if indexPath.row == 4{
            if appDelegate().isConnectedToPrinter{
//                Hide firmware
                return 80.0
//                return 0.0
            }else{
                return 0.0
            }
        }else{
            return 80.0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let otherCell : SettingsOtherInfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SettingsOtherInfoTableViewCell", for: indexPath) as! SettingsOtherInfoTableViewCell
        otherCell.imgVwIcons.image = UIImage(named: iconImages[indexPath.row])
        otherCell.lblListNames.text = listNames[indexPath.row].localisedString()
        otherCell.btnGoToPage.tag = indexPath.row
        if let _ = UserDefaults.standard.object(forKey: "emailKodak") {
            if indexPath.row == 7{
                otherCell.btnLogOut.isHidden = false
                otherCell.lblVersionNo.isHidden = false
            }else{
                otherCell.btnLogOut.isHidden = true
                otherCell.lblVersionNo.isHidden = true
            }
        }
        // Hiding next arrow and logout button according to cell
        if indexPath.row == 7{
            otherCell.btnGoToPage.isHidden = true
           // otherCell.imgVwBG.isHidden = true
            if UserDefaults.standard.bool(forKey: "iWillDoItLater")
            {
                otherCell.btnJoinKodak.isHidden = false
                otherCell.lblVersionNo.isHidden = false
            }else{
                otherCell.btnLogOut.isHidden = false
                otherCell.lblVersionNo.isHidden = false
            }
        }else
        {
            otherCell.btnGoToPage.isHidden = false
          //  otherCell.imgVwBG.isHidden = false
            if UserDefaults.standard.bool(forKey: "iWillDoItLater")
            {
                otherCell.btnJoinKodak.isHidden = true
                otherCell.lblVersionNo.isHidden = true
                
            }else{
                otherCell.btnLogOut.isHidden = true
                otherCell.lblVersionNo.isHidden = true
            }
        }
        return otherCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let tapMenuItemAnalyticsEvent = TapMenuItemAnalyticsEvent(menuItem: AnalyticsValue.MenuItem.orderPaper.rawValue,screenName: "settings(order_paper)")
            Copilot.instance.report.log(event: tapMenuItemAnalyticsEvent)
           self.showBuyPaperVC()
        }else if indexPath.row == 1{
            let tapMenuItemAnalyticsEvent = TapMenuItemAnalyticsEvent(menuItem: AnalyticsValue.MenuItem.tips.rawValue,screenName: "settings(tips)")
            Copilot.instance.report.log(event: tapMenuItemAnalyticsEvent)
            let quickTipsVC = storyboards.QuickTips.instantiateViewController(withIdentifier: "QuickTipsViewController") as! QuickTipsViewController
            self.navigationController?.pushViewController(quickTipsVC, animated: true)
        }else if indexPath.row == 2{
            let tapMenuItemAnalyticsEvent = TapMenuItemAnalyticsEvent(menuItem: AnalyticsValue.MenuItem.support.rawValue,screenName: "settings(support)")
            Copilot.instance.report.log(event: tapMenuItemAnalyticsEvent)
            MailComposer().openContactMailer(viewController: self)
        }else if indexPath.row == 3 {
//            if !WebserviceModelClass().isInternetAvailable()
//            {
//                let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
//                comeFromStrNoInternetSocialMedia = "legalNoInternet"
//                self.present(alertPopUp, animated: true, completion: nil)
//            }
//            else
//            {
//                let legalVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
//                self.navigationController?.pushViewController(legalVC, animated: true)
//            }
            let tapMenuItemAnalyticsEvent = TapMenuItemAnalyticsEvent(menuItem: AnalyticsValue.MenuItem.legal.rawValue,screenName: "settings(legal)")
            Copilot.instance.report.log(event: tapMenuItemAnalyticsEvent)
            let legalVC = storyboards.settingsStoryboard.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
            self.navigationController?.pushViewController(legalVC, animated: true)
        }else if indexPath.row == 4 {
            let tapMenuItemAnalyticsEvent = TapMenuItemAnalyticsEvent(menuItem: AnalyticsValue.MenuItem.firmware.rawValue,screenName: "settings(firmware)")
            Copilot.instance.report.log(event: tapMenuItemAnalyticsEvent)
            if appDelegate().isConnectedToPrinter {
                if WebserviceModelClass().isInternetAvailable() {
                    self.getFirmwareFromSetting()
                }else{
                    let connectToInternetVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "ConnectToInternetViewController") as! ConnectToInternetViewController
                    self.navigationController?.pushViewController(connectToInternetVC, animated: true)
                }
            }else{
                let cantSeeYourPrinterVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "CantSeeYourPrinterViewController") as! CantSeeYourPrinterViewController
                cantSeeYourPrinterVC.comeFromStr = "firmwareVC"
                self.navigationController?.pushViewController(cantSeeYourPrinterVC, animated: true)
            }
        }else if indexPath.row == 5{
            let tapMenuItemAnalyticsEvent = TapMenuItemAnalyticsEvent(menuItem: AnalyticsValue.MenuItem.about.rawValue, screenName: "settings(about)")
            Copilot.instance.report.log(event: tapMenuItemAnalyticsEvent)
            let aboutUsVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
            self.navigationController?.pushViewController(aboutUsVC, animated: true)
        }else if indexPath.row == 6{
            let tapMenuItemAnalyticsEvent = TapMenuItemAnalyticsEvent(menuItem: AnalyticsValue.MenuItem.socialAccounts.rawValue,screenName: "settings(social)")
            Copilot.instance.report.log(event: tapMenuItemAnalyticsEvent)
            let socialAccountVC = self.storyboard?.instantiateViewController(withIdentifier: "SocialAccountViewController") as! SocialAccountViewController
            self.navigationController?.pushViewController(socialAccountVC, animated: true)
        }
    }
    
    // MARK: - Scroll View Delegates
    private func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        for cell in tableVwSettingsList!.visibleCells  as [UITableViewCell]
        {
            let indexPath = tableVwSettingsList.indexPath(for: cell as UITableViewCell)
            if (self.lastContentOffset < scrollView.contentOffset.y) {
                print("Scrolling up")
                //                if indexPath!.row < 1
                //                {
                UIView.animate(withDuration: 0.2) {
                    self.viewTopScrollSmall.isHidden = false
                    self.viewTopHeader.isHidden = true
                    // Autominimize code
//                    self.constraintBottomViewHeight.constant = 46.0
//                    self.viewBtnCamera.cornerRadius = 36.0
//                    self.btnCamera.cornerRadius = 21.0
//                    self.viewBottomMenu.layoutIfNeeded()
                    contentOffsetScrollSettings = scrollView.contentOffset.y
                }
                //                }
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y) {
                print("Scrolling down")
                UIView.animate(withDuration: 0.2) {
                    self.viewTopScrollSmall.isHidden = true
                    self.viewTopHeader.isHidden = false
                    // Autominimize code
//                    self.constraintBottomViewHeight.constant = 70.0
//                    self.viewBtnCamera.cornerRadius = 55.5
//                    self.btnCamera.cornerRadius = 32.5
//                    self.viewBottomMenu.layoutIfNeeded()
                    contentOffsetScrollSettings = scrollView.contentOffset.y
                }
            }
        }
    }

    func getFirmwareFromSetting()  {
        customLoader.showActivityIndicator(viewController: self.view)
        let version = "1.0"
        /*
         if appDelegate().Product_ID == "0"{
         version = "1.0"
         }else{
         version = "1.2"
         }
         */
        //Old live url
        //            (isSuccess,responseMessage,responseData) -> Void in
        
        //http://polaroidapps.ml/dev/polaroid-new/mint/get_firmware/1.0
        //http://polaroidapps.ml/dev/
        WebserviceModelClass().getDataFor(module:"PlansViewController",subUrl:FIRMWARE_URL, completionHandler:{
            (isSuccess,responseMessage,responseData) -> Void in
            if isSuccess
            {
                print("responseData = \(responseData)")
                if (responseData as! NSDictionary != nil)
                {
                    guard let data = (responseData as AnyObject).value(forKey: "data") as? NSDictionary
                        else {
                            return
                    }
                    
                    //let firmwareDict = data!.value(forKey: "firmware") as? NSDictionary
                    
                    //let tmdDict = data!.value(forKey: "tmd") as? NSDictionary
                    guard let tmdDict = data.value(forKey: "tmd") as? NSDictionary
                        else {
                            return
                    }
                    //let cnxDict = (responseData as AnyObject).value(forKey: "cnx") as? NSDictionary
                    guard  let firmwareDict = data.value(forKey: "firmware") as? NSDictionary
                        else {
                            return
                    }
                    guard var FIRMVstring = firmwareDict.value(forKey: "version") as? String
                        else {
                            return
                    }
                    FIRMVstring = FIRMVstring == "" ? "0" : FIRMVstring
                    let FIRMVersion = float_t(FIRMVstring.replacingOccurrences(of: ".", with: ""))
                    //(firmwareDict?.value(forKey: "version") as AnyObject).floatValue
                    //print("CNXVersion \(CNXVersion) > self.XCNXVersion \(self.XCNXVersion)")
                    customLoader.hideActivityIndicator()
                    //print("\(FIRMVersion!) > \(XFIRMVersion) || \(TMDVersion!) > \(XTMDVersion) ")
                    print("\(FIRMVersion!) > \(XFIRMVersion)  ")
                    if( FIRMVersion! > XFIRMVersion) //|| TMDVersion! > XTMDVersion )
                    {
                        appDelegate().isFirmwareUpdateAvailable = true
                        let fwUpdateAvailableVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "FirmwareUpdateAvailableViewController") as! FirmwareUpdateAvailableViewController
                        self.navigationController?.pushViewController(fwUpdateAvailableVC, animated: true)
                    }
                    else{
                        appDelegate().isFirmwareUpdateAvailable = false
                        let fwUptoDateVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "FirmwareUptoDateViewController") as! FirmwareUptoDateViewController
                        self.navigationController?.pushViewController(fwUptoDateVC, animated: true)
                    }
                }
            }
            else{
                print("Error")
                customLoader.hideActivityIndicator()
            }
        })
    }
}

