//
//  SettingsOtherInfoTableViewCell.swift
//  Polaroid MINT
//
//  Created by MAXIMESS142 on 02/10/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class SettingsOtherInfoTableViewCell: UITableViewCell {

    //MARK: - Outlets -
    @IBOutlet weak var imgVwBuyPaper: UIImageView!
    @IBOutlet weak var imgVwIcons: UIImageView!
    @IBOutlet weak var lblListNames: UILabel!
    @IBOutlet weak var imgVwBG: UIImageView!
    @IBOutlet weak var btnGoToPage: UIButton!
    @IBOutlet weak var btnLogOut: UIButton!
    @IBOutlet weak var lblVersionNo: UILabel!
    @IBOutlet weak var btnJoinKodak: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        // Initialization code
        self.btnLogOut.setTitle("Sign out\nKODAK SMILE".localisedString(), for: .normal)
        self.btnJoinKodak.setTitle("Sign up\nKODAK SMILE".localisedString(), for: .normal)
        self.lblVersionNo.lineBreakMode = NSLineBreakMode.byCharWrapping
        self.lblVersionNo.numberOfLines = 0
        let versionString = "\(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String)"
        self.lblVersionNo.text = "Version".localisedString() + "\n\(versionString)"
    }

}
