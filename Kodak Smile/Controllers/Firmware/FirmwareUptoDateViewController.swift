//
//  FirmwareUptoDateViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 20/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class FirmwareUptoDateViewController: UIViewController {
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblFirmwareTop: NSLayoutConstraint!
    @IBOutlet weak var constraintImgFirmwareUpdateTop: NSLayoutConstraint!
    
    @IBOutlet weak var imgFirmwareUpdate: UIImageView!
    @IBOutlet weak var lblOnit: UILabel!
    @IBOutlet weak var lblUpToDate: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    var cameFrom = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()

        // Do any additional setup after loading the view.
    }
    
    //Assign values to fields.
    func localizedStrings() {
        self.lblTitle.text = "Firmware".localisedString() + " " + "Update".localisedString()
        self.lblOnit.text = "Your'e on it".localisedString()
        self.lblUpToDate.text = "Your printer firmware is up to date".localisedString()
        self.lblVersion.text = "Version".localisedString() + " " + XFIRMVersionString
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblFirmwareTop.constant = 30.0
            constraintImgFirmwareUpdateTop.constant = 30.0
        }else{
            
            print("Unknown")
        }
    }
    
    // MARK: - Action Methods
    
    @IBAction func backBtnClicked(_ sender:Any)
    {
        if self.cameFrom == "ConnectToInternet" {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: false)
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
