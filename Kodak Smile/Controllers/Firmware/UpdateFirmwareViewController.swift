//
//  UpdateFirmwareViewController.swift
//  ContainerView
//
//  Created by MAXIMESS082 on 18/12/17.
//  Copyright © 2017 MACMINI01. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyGif
import CopilotAPIAccess
class UpdateFirmwareViewController: UIViewController,ResponceDelegate,DialogDelegate {

    //MARK: - Outlets
    
    @IBOutlet weak var lblAboutUpdate: UILabel!
    @IBOutlet weak var lblNewFirmaware: UILabel!
    @IBOutlet weak var imgViewGifLoader: UIImageView!
    @IBOutlet weak var imgFirmware: UIImageView!
    
    @IBOutlet weak var yConstraintsViewFirmwareDetails: NSLayoutConstraint!
    @IBOutlet weak var viewFirmwareDetails: UIView!
    @IBOutlet weak var btnInstall: UIButton!
    @IBOutlet weak var lblInstallNote: UILabel!
    
    @IBOutlet var lblFVersion: UILabel!
    @IBOutlet var lblTMDVersion: UILabel!
    @IBOutlet var lblConexantVersion: UILabel!
    
    @IBOutlet weak var lblFirmwareVersionNum: UILabel!
    @IBOutlet weak var lblTMDVersionNum: UILabel!
    @IBOutlet weak var lblConexantVersionNum: UILabel!
    
    //MARK:- Variables
    let gifManager = SwiftyGifManager(memoryLimit:10)
    
    var isErrorCalled = false
    
    var countOfDownloadItems = 0
    var downloadedItemsCount = 0
    
    var xCNXVersion = Float()
    var xFIRMVersion = Float()
    var xTMDVersion = Float()
    
    var generateCode = ""
    
    var isDownloadFileCNX = false
    var isDownloadFilexTMD = false
    var isDownloadFileFirm = false
    
    var isDownloadFWfile = true
    
    var isupdateFirmware = false{
        didSet{
            
        }
    }
    var isupdateConexant = false{
        didSet{
            if self.isupdateConexant == false{
            }else{
            }
            
        }
    }
    var isupdateTMD = false{
        didSet{
            if self.isupdateTMD == false{
            }else{
            }
            
        }
    }
    
    //MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblFVersion.text = lbl_FIRMWARE_VERSION
        self.lblTMDVersion.text = lbl_TMD_VERSION
        self.lblConexantVersion.text = lbl_CONEXANT_VERSION
        
        self.checkUpdateAvilability()
        self.btnInstall.layer.cornerRadius = 100
       
        self.lblInstallNote.text = "Download Firmware".localisedString()
        
        let gifImage = UIImage(gifName:"loader@1x")
        self.imgViewGifLoader.setGifImage(gifImage, manager: gifManager, loopCount: -1)
        
        self.isupdateFirmware = false
        self.isupdateTMD = false
        self.isupdateConexant = false
        self.localizedStrings()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
            
       customLoader.showActivityIndicator(viewController: self.view)
            self.lblTMDVersionNum.text = ""
            self.lblConexantVersionNum.text = ""
            self.lblFirmwareVersionNum.text = ""
        
            self.checkUpdateAvilability()
            btnInstall.isUserInteractionEnabled = true
      
        EADSessionController.shared().delegate = PrinterQueueManager.sharedController
        if(appDelegate().isConnectedToPrinter == true)
        {
            
            PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.get_ZIP_FIRMWARE, ResponseDelegate: self))
            
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.updateFirmware.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    
    @IBAction func backButtonAction(_ sender: Any) {
//        if appDelegate().Product_ID == "0"
//        {
//    
//            if !( FW_Latest_1_0 > self.xFIRMVersion )
//            {
//                self.navigationController?.popViewController(animated: true)
//            }
//            
//        }
//        else{
//            if !( FW_Latest_1_2 > self.xFIRMVersion )
//            {
//                self.navigationController?.popViewController(animated: true)
//            }
//        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func installBtnAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "", message: "The firmware update may take up to 20 minutes. During the update, please don’t minimize or close the app, or you’ll need to restart the process. Do you want to continue with the update process?".localisedString(), preferredStyle: .alert)
        let alertActionYes = UIAlertAction(title: "Yes".localisedString(), style: .default) { (alert) in
            
            if appDelegate().isConnectedToPrinter == true {
                if appDelegate().isFirmwareUpdateAvailable == true
                {
                    if WebserviceModelClass().isInternetAvailable()
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DownloadFirmwareViewController") as! DownloadFirmwareViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else
                    {
                        Utils.AlertMessage(message: "Please connect to internet".localisedString())
                    }
                }
            }
            else
            {
                Utils.AlertMessage(message: "Please connect your device".localisedString())
            }
        }
        
        let alertActionNo = UIAlertAction(title: "No".localisedString(), style: .cancel) { (alert) in
            
        }

        alert.addAction(alertActionYes)
        alert.addAction(alertActionNo)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Functions
    func localizedStrings() {
        self.lblFVersion.text = "Firmware Version".localisedString()
        self.lblTMDVersion.text = "TMD Version".localisedString()
        self.lblConexantVersion.text = "Conexant Version".localisedString()
        self.lblNewFirmaware.text = "NEW FIRMWARE".localisedString()
        self.lblAboutUpdate.text = "About Update".localisedString()

    }
    func checkUpdateAvilability() {
        
        if appDelegate().isFirmwareUpdateAvailable == true {
             self.imgViewGifLoader.isHidden = true
            yConstraintsViewFirmwareDetails.constant = 249
            imgFirmware.isHidden = false
            btnInstall.isHidden = false
            lblInstallNote.isHidden = false
        }
        else {
             self.imgViewGifLoader.isHidden = true
            yConstraintsViewFirmwareDetails.constant = 15
            imgFirmware.isHidden = true
            btnInstall.isHidden = true
            lblInstallNote.isHidden = true
        }
    }
    
    func firmwareInstall() {
        
        
        let obj = RequestOperation(type: RequestType.updateVersion, ResponseDelegate: self)
        
        obj.UpdateVersionCode = ""
        
        if self.isupdateFirmware{
            obj.UpdateVersionCode = "00"
        }
        if self.isupdateConexant{
            obj.UpdateVersionCode = "01"
        }
        if self.isupdateTMD{
            obj.UpdateVersionCode = "02"
        }
        if self.isupdateFirmware && self.isupdateConexant{
            obj.UpdateVersionCode = "03"
        }
        if self.isupdateFirmware && self.isupdateTMD{
            obj.UpdateVersionCode = "04"
        }
        if self.isupdateConexant && self.isupdateTMD{
            obj.UpdateVersionCode = "05"
        }
        if self.isupdateFirmware && self.isupdateConexant && self.isupdateTMD{
            //obj.UpdateVersionCode = "06"
            obj.UpdateVersionCode = "00"
        }
        print(obj.UpdateVersionCode)
        if obj.UpdateVersionCode == ""{
            //select any one
        }else{
            PrinterQueueManager.RequestArray.append(obj)
        }

    }
    
    func downloadFirmware_TMD(_ download_Type: Int)
    {
        if WebserviceModelClass().isInternetAvailable() {
            var urlPath: String = ""
            var download_FileName:String = ""
            
            // select dowonload file name and url
            if download_Type == 0 && appDelegate().firmware_version_URL != ""{
                urlPath = appDelegate().firmware_version_URL
                print("appDelegate().firmware_version_URL : \(urlPath)")
                print("download_FileName = \(newFIRMfileName)")
               
                download_FileName = newFIRMfileName
            }else if download_Type == 1 && appDelegate().tmd_version_URL != ""{
                urlPath =  appDelegate().tmd_version_URL
                print("appDelegate().tmd_version_URL : \(urlPath)")
                
                download_FileName = newTMDfileName
            }else if download_Type == 2 && appDelegate().cnx_version_URL != ""{
                urlPath =  appDelegate().cnx_version_URL
                print("appDelegate().cnx_version_URL : \(urlPath)")
                
                download_FileName = newCNXfileName
            }
            
          
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                documentsURL.appendPathComponent(download_FileName)
                return (documentsURL, [.removePreviousFile])
            }
            
            let url = URL(string: urlPath)
            
            //let url = URL(string: "https://testurapp.com/skliq/public/Polaroid/firmware/Snap_touch_1.2_V1.00.16.04/PRINTER2.BRN")
            
            Alamofire.download(url!, method: .get, parameters: nil, encoding: JSONEncoding.default, to: destination)
                .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                   
                    if progress.isFinished {
                        
                        self.downloadedItemsCount += 1
                        
                        print("self.downloadedItemsCount = \(self.downloadedItemsCount)")
                        
                        print("self.countOfDownloadItems = \(self.countOfDownloadItems)")
                        
                        if(self.downloadedItemsCount == self.countOfDownloadItems)
                        {
                            self.btnInstall.isUserInteractionEnabled = true
                            self.lblInstallNote.text = "Install Firmware".localisedString()
                            self.isDownloadFWfile = false
                        }
                        Utils.stopActivityIndicatorOnView(self.view)
                    }
                }
                .validate { request, response, temporaryURL, destinationURL in
                    // Custom evaluation closure now includes file URLs (allows you to parse out error messages if necessary)
                    return .success
                }
                .responseJSON { response in
                    debugPrint(response)
                    print(response.temporaryURL)
                    print(response.destinationURL)
            }

          
           
        }
    }

    //MARK: - GetVersionResponse Delegate
    
    func GetVersionResponse(_ Firmware:String ,Conexan:String,  TMD: String){
        
        let CNXstring =  Conexan.replacingOccurrences(of: ".", with: "")
        xCNXVersion = (Float(CNXstring.replacingOccurrences(of: "v", with: ""))!)//*10
        
        let TMDVstring =  TMD.replacingOccurrences(of: ".", with: "")
        xTMDVersion =  (Float(TMDVstring.replacingOccurrences(of: "v", with: ""))!)//*10
        
        let FIRMstring =  Firmware.replacingOccurrences(of: ".", with: "")
        xFIRMVersion =  Float(FIRMstring.replacingOccurrences(of: "v", with: ""))!
        
        print("xCNXVersion = \(xCNXVersion)")
        print("xTMDVersion = \(xTMDVersion)")
        print("xFIRMVersion = \(xFIRMVersion)")
        
        
        let productCode = FIRMstring.character(at: 1)
        
        if productCode == "1"
        {
            appDelegate().Product_ID = "0"
        }
        else{
            appDelegate().Product_ID = "1"
        }
        
        //MARK:- error solve change by maximess
        var cnxVersion = String(xTMDVersion)
        cnxVersion.insert("v", at: cnxVersion.startIndex)
        var tmdVersion = String(xCNXVersion)
        tmdVersion.insert("v", at: tmdVersion.startIndex)
        
        self.lblTMDVersionNum.text = "\(TMD)"
        self.lblConexantVersionNum.text = "\(Conexan)"
        self.lblFirmwareVersionNum.text = "\(Firmware)"
        
        if(appDelegate().isConnectedToPrinter == true)
        {
            btnInstall.isUserInteractionEnabled = true
            
            if WebserviceModelClass().isInternetAvailable() {
            self.getFirmware()
            }
            else{
                Utils.AlertMessage(message: "Please connect to internet".localisedString())
            }
            
            
        }
        else
        {
            btnInstall.isUserInteractionEnabled = false
            
        }
       
        customLoader.hideActivityIndicator()
    }
    
    // MARK: -  API
    
    func getFirmware()  {
        
        self.imgViewGifLoader.isHidden = true
        
        var version = String()
        if appDelegate().Product_ID == "0"{
            version = "1.0"
            
        }else{
            version = "1.2"
            
        }
        
        //OLd live url
//        WebserviceModelClass().getDataFor(module:"PlansViewController",subUrl:"snaptouch_firmware.php?ver=\(version)", completionHandler:{
//            (isSuccess,responseMessage,responseData) -> Void in
        
        //New url
        WebserviceModelClass().getDataFor(module:"PlansViewController",subUrl:"get_file_version2.php?ver=\(version)", completionHandler:{
            (isSuccess,responseMessage,responseData) -> Void in
        
            if isSuccess
            {
                print("responseData = \(responseData)")
                if (responseData as! NSDictionary != nil)
                {
                    let tmdDict = (responseData as AnyObject).value(forKey: "tmd") as? NSDictionary
                    let cnxDict = (responseData as AnyObject).value(forKey: "cnx") as? NSDictionary
                    let firmwareDict = (responseData as AnyObject).value(forKey: "firmware") as? NSDictionary
                    
                    //New FW files download url
                    
                    if let url = tmdDict?.value(forKey: "url") as? String{
                        print("tmd_version_URL : \(url)")
                       
                        appDelegate().tmd_version_URL = url
                        // remove cookies
                        let cstorage = HTTPCookieStorage.shared
                        if let cookies = cstorage.cookies(for: URL(string: url)!) {
                            for cookie in cookies {
                                cstorage.deleteCookie(cookie)
                            }
                        }
                        
                        ///
                    }

                    if let url = firmwareDict?.value(forKey: "url") as? String{
                        
                        appDelegate().firmware_version_URL = url
                        print("firmware_version_URL : \(url)")
                        // remove cookies
                        let cstorage = HTTPCookieStorage.shared
                        if let cookies = cstorage.cookies(for: URL(string: url)!) {
                            for cookie in cookies {
                                cstorage.deleteCookie(cookie)
                            }
                        }
                        
                        ///
                    }
                    
                    //New FW Files name
                    
                    if let TMDfileName = tmdDict?.value(forKey: "filename") as? String {
                        
                        newTMDfileName =   TMDfileName
                    }
                    
                    
                    if let FIRMfileName = firmwareDict?.value(forKey: "filename") as? String{
                        newFIRMfileName =   FIRMfileName
                    }
                    
                    //change by akshay
                    var TMDstring = tmdDict?.value(forKey: "version") as! String
                    
                    if TMDstring == ""
                    {
                        TMDstring = "0"
                    }
                    
                    let TMDVersion =  float_t(TMDstring.replacingOccurrences(of: ".", with: ""))
                   
                    var FIRMVstring = firmwareDict?.value(forKey: "version") as! String
                    
                    if FIRMVstring == ""
                    {
                        FIRMVstring = "0"
                    }
                    
                    let FIRMVersion = float_t(FIRMVstring.replacingOccurrences(of: ".", with: ""))
                    
                    print("FIRMVersion \(FIRMVersion) > self.XFIRMVersion \(self.xFIRMVersion)")
                    if(FIRMVersion! > self.xFIRMVersion) || (TMDVersion! > self.xTMDVersion)
                        //if(30 > self.XFIRMVersion)
                    {
                        self.isupdateFirmware = true
                        self.countOfDownloadItems += 1
                    }
                    print("TMDVersion \(TMDVersion) > self.XTMDVersion \(self.xTMDVersion)")
                    
                    print("Firmware Boolean \(self.isupdateFirmware) : Tmd Boolean \(self.isupdateTMD) : CNXVErsion Boolean \(self.isupdateConexant)")
                    
                    if(FIRMVersion! > self.xFIRMVersion) || (TMDVersion! > self.xTMDVersion)//(self.countOfDownloadItems > 0)
                    {
                        DispatchQueue.main.async(execute: {
                            //Utils.stopActivityIndicatorOnView(self.view)
                            self.btnInstall.isUserInteractionEnabled = true
                           
                            self.imgViewGifLoader.isHidden = true
                        })
                    }
                    
                    
                    
                }
            }
            else{
                print("Error")
                Utils.stopActivityIndicatorOnView(self.view)
            }
            
        })
    }

}
