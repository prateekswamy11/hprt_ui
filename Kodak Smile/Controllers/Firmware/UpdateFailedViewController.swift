//
//  UpdateFailedViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 21/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class UpdateFailedViewController: UIViewController {
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblFirmwareTop: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnTryAgainBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnTryAgainHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintImgUpdateFailTop: NSLayoutConstraint!
    
    @IBOutlet weak var imgUpdateFail: UIImageView!
    @IBOutlet weak var lblFirmware: UILabel!
    @IBOutlet weak var lblUpdateFailed: UILabel!
    @IBOutlet weak var lblUpdateFailedMsg: UILabel!
    @IBOutlet weak var btntryAgain: UIButton!
    
    //MARK: - Variables -
    var isFirmwareUpdateFailed: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.detectIphone5sDevice()
    }
    
    //Assign values to fields.
    func localizedStrings() {
        self.lblFirmware.text = "Firmware".localisedString()
        self.lblUpdateFailed.text = "Update Failed".localisedString()
        if isFirmwareUpdateFailed{
            self.lblUpdateFailedMsg.text = "Please reconnect your printer to continue with the firmware update".localisedString()
            self.btntryAgain.setTitle( "Find Printer".localisedString(), for: .normal)
        }else{
            self.lblUpdateFailedMsg.text = "Make sure your printer is connected".localisedString()
            self.btntryAgain.setTitle( "Give it another try".localisedString(), for: .normal)
        }
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblFirmwareTop.constant = 30.0
            constraintBtnTryAgainBottom.constant = 20.0
            constraintImgUpdateFailTop.constant = 30.0
//            constraintBtnTryAgainHeight.constant = 30.0
//            self.btntryAgain.cornerRadius = 15.0
        }else{
            
            print("Unknown")
        }
    }
    
    // Function to show alert of device disconnected and pop back to settings viewcontroller
    func showAlertDeviceDisconnected()
    {
        let alert = UIAlertController(title: AppName, message: "Your device was disconnected".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok".localisedString(), style: UIAlertActionStyle.default, handler: { action in
            let cantSeeYourPrinterVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "CantSeeYourPrinterViewController") as! CantSeeYourPrinterViewController
            cantSeeYourPrinterVC.comeFromStr = "updateFailedVC"
            self.navigationController?.pushViewController(cantSeeYourPrinterVC, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Action Methods
    
    @IBAction func backBtnClicked(_ sender:Any)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: FirmwareUpdateAvailableViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func btnTryAgainClicked(_ sender: Any)
    {
        //        let downloadFirmwareVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "DownloadFirmwareViewController") as! DownloadFirmwareViewController
        //
        //        self.navigationController?.pushViewController(downloadFirmwareVC, animated: true)
//        if appDelegate().isConnectedToPrinter{
        if isFirmwareUpdateFailed{
            let cantSeeYourPrinterVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "CantSeeYourPrinterViewController") as! CantSeeYourPrinterViewController
            cantSeeYourPrinterVC.comeFromStr = "updateFailedVC"
            self.navigationController?.pushViewController(cantSeeYourPrinterVC, animated: true)
        }else{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: FirmwareUpdateAvailableViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
//        }else{
//            self.showAlertDeviceDisconnected()
//        }
    }
}
