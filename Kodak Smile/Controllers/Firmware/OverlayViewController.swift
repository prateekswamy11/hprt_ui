//
//  OverlayViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 06/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
var checkOverlayIsPresent = false
class OverlayViewController: UIViewController {
    
    // MARK: - Edit Overlay view
    @IBOutlet weak var viewEditOverlayBG: UIView!
    @IBOutlet weak var viewEditOverlayDark: UIView!
    @IBOutlet weak var viewEditOverlayHighlight: UIView!
    @IBOutlet weak var viewEditOverlay: UIView!
    @IBOutlet weak var lblEditTitle: UILabel!
    @IBOutlet weak var lblEditDetail: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    @IBOutlet weak var constraintLblEditDetailBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintImgEditArrowLeading: NSLayoutConstraint!
    
    // MARK: - Printer Overlay view
    @IBOutlet weak var viewPrinterOverlay: UIView!
    @IBOutlet weak var lblPrinterTitle: UILabel!
    @IBOutlet weak var lblPrinterDetail: UILabel!
    @IBOutlet weak var lblPrinterStatus: UILabel!
    @IBOutlet weak var btnGotIt: UIButton!
    
    // MARK: - Sticker Overlay view
    @IBOutlet weak var viewStickerOverlayBG: UIView!
    @IBOutlet weak var viewStickerOverlay: UIView!
    @IBOutlet weak var lblStickerTitle: UILabel!
    @IBOutlet weak var lblStickerDetail: UILabel!
    @IBOutlet weak var btnKeepMinting: UIButton!
    
    // MARK: - Filter Overlay view
    @IBOutlet weak var viewFilterOverlayBG: UIView!
    @IBOutlet weak var viewFilterOverlay: UIView!
    @IBOutlet weak var lblFilterText: UILabel!
    @IBOutlet weak var imgFilter: UIImageView!
    
    @IBOutlet weak var viewFilterOverlay2: UIView!
    @IBOutlet weak var lblFilterText2: UILabel!
    @IBOutlet weak var imgFilter2: UIImageView!
    
    var isFrom = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(_:)))
        swipe.direction = [.right,.left]
        
//        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(_:)))
//        swipe.direction = UISwipeGestureRecognizerDirection.left
        viewFilterOverlay2.addGestureRecognizer(swipe)
        viewFilterOverlay2.isUserInteractionEnabled = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.showOverlay()
    }
    
    // function which is triggered when handleTap is called
    @objc func handleSwipe(_ sender: UISwipeGestureRecognizer) {
        print("First time camera open")
        removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    
    
    func showOverlay() {
        self.viewEditOverlayDark.alpha = 0.8
        self.viewPrinterOverlay.alpha = 0
        self.viewStickerOverlay.alpha = 0
        self.viewFilterOverlay.alpha = 0
        self.viewFilterOverlay2.alpha = 0
        self.viewStickerOverlayBG.isHidden = true
        self.viewFilterOverlayBG.isHidden = true
        self.overlaylocalizingString()
        self.overlaydetectIphone5sDevice()
        if isFrom == "Camera"{
            self.viewEditOverlay.isHidden = true
            self.viewFilterOverlayBG.isHidden = false
            self.viewFilterOverlayBG.alpha = 0.8
            self.viewEditOverlayDark.alpha = 0
            self.viewEditOverlay.alpha = 0
            self.viewFilterOverlay2.alpha = 1
        }
        
        if isFrom == "Sticker"{
            self.viewEditOverlay.isHidden = true
            self.viewFilterOverlay.isHidden = true
            self.viewFilterOverlay2.isHidden = true
            self.viewEditOverlayBG.isHidden = true
            self.viewPrinterOverlay.isHidden = true
            self.viewStickerOverlayBG.isHidden = false
            self.self.viewStickerOverlayBG.alpha = 0.8
            self.viewStickerOverlay.alpha = 1
        }
    }
    
    func overlaylocalizingString() {
        
        //Set Line spacing
        self.lblEditDetail.setLineSpacing(lineSpacing: 7.0)
        self.lblPrinterDetail.setLineSpacing(lineSpacing: 7.0)
        self.lblPrinterStatus.setLineSpacing(lineSpacing: 7.0)
        self.lblStickerDetail.setLineSpacing(lineSpacing: 7.0)
        
        //MARK : For Edit.
        self.lblEditTitle.text = "Scribble, Doddle, Snip!".localisedString()
        self.lblEditDetail.text = "You can edit the filter, edit and add on stickers to your picture.".localisedString()
        self.btnOk.setTitle("OK".localisedString(), for: .normal)
        
        //MARK : For Print.
        self.lblPrinterTitle.text = "Time to print!".localisedString()
        self.lblPrinterDetail.text = "Tap the printer button to see your photo turned into mint print.".localisedString()
        self.lblPrinterStatus.text = "The green dot confirms your printer is connected!".localisedString()
        self.btnGotIt.setTitle("Got it!".localisedString(), for: .normal)
        
        //MARK : For Sticker.
        self.lblStickerTitle.text = "Print Successful ".localisedString()
        self.lblStickerDetail.text = "Just peel the back off your print,\nand turn it into a sticker".localisedString()
        self.btnKeepMinting.setTitle("Create More SMILE Images".localisedString(), for: .normal)
        
        //MARK : For Filter.
        self.lblFilterText.text = "Swipe down to control the opacity level".localisedString()
        let frameImg = UIImage.init(named: "coachMarks")
        let flippedImage = frameImg?.rotated(by: Measurement(value: -90.0, unit: .degrees), options: [])
        self.imgFilter.image = flippedImage
        
        //MARK : For Camera Filter.
        self.lblFilterText2.text = "Swipe left to apply live filters".localisedString()
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func overlaydetectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintImgEditArrowLeading.constant = 0.0
            constraintLblEditDetailBottom.constant = 10.0
        }
    }
    
    // MARK: - Overlay Action Methods
    
    @IBAction func btnOkClicked(_ sender: Any)
    {
        self.viewEditOverlayBG.removeFromSuperview()
        self.viewEditOverlay.removeFromSuperview()
        self.viewEditOverlay.alpha = 0
        self.viewPrinterOverlay.alpha = 1.0
        self.viewStickerOverlayBG.isHidden = false
        self.viewStickerOverlayBG.alpha = 0.8
    }
    
    @IBAction func btnGotItClicked(_ sender: Any)
    {
        checkOverlayIsPresent = false
        self.viewPrinterOverlay.removeFromSuperview()
        self.viewPrinterOverlay.alpha = 0
//        self.viewStickerOverlay.alpha = 1.0
        self.viewStickerOverlayBG.removeFromSuperview()
        removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    
    @IBAction func btnKeepMintingClicked(_ sender: Any)
    {
        if isComeFrom == "LoadPrinter"{
            let HomeViewController = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            isComeFrom = ""
            self.navigationController?.pushViewController(HomeViewController, animated: true)
        }else{
             self.viewStickerOverlay.removeFromSuperview()
                    self.viewStickerOverlayBG.removeFromSuperview()
                    self.viewStickerOverlay.alpha = 0
                    removeFromParentViewController()
                    self.view.removeFromSuperview()
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "StickerOverlayDismiss"), object:nil);
            //        self.viewFilterOverlayBG.alpha = 0.7
            //        self.viewFilterOverlayBG.isHidden = false
            //        self.viewFilterOverlay2.alpha = 1.0
        }
    }
    
    @IBAction func btnCrossClicked(_ sender: Any)
    {
        checkOverlayIsPresent = false
        removeFromParentViewController()
        self.view.removeFromSuperview()
//        self.viewFilterOverlayBG.removeFromSuperview()
//        self.viewFilterOverlay.removeFromSuperview()
//        self.viewFilterOverlay.alpha = 0
//        self.viewFilterOverlay2.alpha = 1.0
    }
    
    @IBAction func btnXClicked(_ sender: Any)
    {
        checkOverlayIsPresent = false
        removeFromParentViewController()
        self.view.removeFromSuperview()
//        self.viewFilterOverlayBG.removeFromSuperview()
//        self.viewFilterOverlay2.removeFromSuperview()
//        self.viewFilterOverlay2.alpha = 0
    }

}


extension UILabel {
    
    func setLineSpacing(lineSpacing: CGFloat) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.alignment = .center
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        // Line spacing attribute
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
}
