//
//  FirmwareUpdateAvailableViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 20/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class FirmwareUpdateAvailableViewController: UIViewController {
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblFirmwareTop: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnUpdateBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnUpdateHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintImgFirmwareUpdateTop: NSLayoutConstraint!
    
    @IBOutlet weak var lblFirmware: UILabel!
    @IBOutlet weak var imgFirmwareUpdate: UIImageView!
    @IBOutlet weak var lblUpdateAvailable: UILabel!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnCross: UIButton!
    
    var cameFrom = ""

    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.forceFirmwarePopupCustomization()
    }
    
    //Assign values to fields.
    func localizedStrings() {
        
        let normalFont = UIFont(name: "SFProDisplay-Regular", size: 15)
        let boldFont = UIFont(name: "SFProDisplay-Bold", size: 15)
        
        let msgString = "A new firmware update is available for your mint printer".localisedString()
        let boldString = "mint".localisedString()
        
        self.lblUpdateAvailable.attributedText = addBoldText(fullString: msgString as NSString, boldPartsOfString: [boldString as NSString], font: normalFont!, boldFont: boldFont!)
        
        self.lblFirmware.text = "Firmware".localisedString()
        self.lblUpdateAvailable.text = "A new firmware update is available for your printer".localisedString()
        self.btnUpdate.setTitle( "Update".localisedString(), for: .normal)
    }
    
    func addBoldText(fullString: NSString, boldPartsOfString: Array<NSString>, font: UIFont!, boldFont: UIFont!) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedStringKey.font:font!]
        let boldFontAttribute = [NSAttributedStringKey.font:boldFont!]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
        }
        return boldString
    }
    
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblFirmwareTop.constant = 30.0
            constraintBtnUpdateBottom.constant = 20.0
            constraintImgFirmwareUpdateTop.constant = 30.0
            //If want to change height of update button uncomment it.
//            constraintBtnUpdateHeight.constant = 30.0
//            self.btnUpdate.cornerRadius = 15.0

        }else{
            
            print("Unknown")
        }
    }
    
    func forceFirmwarePopupCustomization() {
        //is_Force_Firmware_Available Flag used to show/hide cross btn
        if is_Force_Firmware_Available == "yes"{
            self.btnCross.isHidden = true
        }
    }
    
    // MARK: - Action Methods
    
    @IBAction func backBtnClicked(_ sender:Any)
    {
        if self.cameFrom == "ConnectToInternet" {
//            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
//            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: false)
            for controller in navigationController!.viewControllers {
                if controller is SettingsViewController {
                    self.navigationController!.popToViewController(controller, animated:false)
                    break
                }
            }
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnUpdateClicked(_ sender: Any)
    {
        if !WebserviceModelClass().isInternetAvailable()
        {
            let ConnectInternet = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "ConnectToInternetViewController") as! ConnectToInternetViewController
            self.navigationController?.pushViewController(ConnectInternet, animated: true)
        }
        else
        {
            let downloadFWVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "DownloadFirmwareViewController") as! DownloadFirmwareViewController
            self.navigationController?.pushViewController(downloadFWVC, animated: true)
        }
    }
    
    // MARK: - APIs
    /*
    @objc func getFirmware()  {
        
        
        //self.imgViewGifLoader.isHidden = false
        
        var version = String()
        if appDelegate().Product_ID == "0"{
            version = "1.0"
            
        }else{
            version = "1.2"
            
        }
        
        //Old live url
        //WebserviceModelClass().getDataFor(module:"PlansViewController",subUrl:"snaptouch_firmware.php?ver=\(version)", completionHandler:{
        //(isSuccess,responseMessage,responseData) -> Void in
        
        //new url
        WebserviceModelClass().getDataFor(module:"PlansViewController",subUrl:"get_file_version2.php?ver=\(version)", completionHandler:{
            (isSuccess,responseMessage,responseData) -> Void in
            
            
            if isSuccess
            {
                print("responseData = \(responseData)")
                if (responseData as! NSDictionary != nil)
                {
                    let tmdDict = (responseData as AnyObject).value(forKey: "tmd") as? NSDictionary
                    //let cnxDict = (responseData as AnyObject).value(forKey: "cnx") as? NSDictionary
                    let firmwareDict = (responseData as AnyObject).value(forKey: "firmware") as? NSDictionary
                    
                    //New FW files download url
                    
                    if let url = tmdDict?.value(forKey: "url") as? String{
                        print("tmd_version_URL : \(url)")
                        
                        appDelegate().tmd_version_URL = url
                    }
                    
                    if let url = firmwareDict?.value(forKey: "url") as? String{
                        
                        appDelegate().firmware_version_URL = url
                        print("firmware_version_URL : \(url)")
                    }
                    
                    //New FW Files name
                    
                    if let TMDfileName = tmdDict?.value(forKey: "filename") as? String {
                        
                        newTMDfileName =   TMDfileName
                    }
                    
                    
                    
                    if let FIRMfileName = firmwareDict?.value(forKey: "filename") as? String{
                        newFIRMfileName =   FIRMfileName
                    }
                    
                    
                    //change by akshay
                    var TMDstring = tmdDict?.value(forKey: "version") as! String
                    
                    if TMDstring == ""
                    {
                        TMDstring = "0"
                    }
                    
                    let TMDVersion =  float_t(TMDstring.replacingOccurrences(of: ".", with: ""))
                    
                    var FIRMVstring = firmwareDict?.value(forKey: "version") as! String
                    
                    if FIRMVstring == ""
                    {
                        FIRMVstring = "0"
                    }
                    
                    let FIRMVersion = float_t(FIRMVstring.replacingOccurrences(of: ".", with: ""))
                    
                    print("FIRMVersion \(FIRMVersion) > self.XFIRMVersion \(self.xFIRMVersion)")
                    if(FIRMVersion! > self.xFIRMVersion)
                        //if(600 > self.xFIRMVersion)
                    {
                        self.isupdateFirmware = true
                        self.flagForFirmware = true
                        self.countOfDownloadItems += 1
                    }
                    print("TMDVersion \(TMDVersion) > self.XTMDVersion \(self.xTMDVersion)")
                    if(TMDVersion! > self.xTMDVersion)
                        //if(600 > self.xTMDVersion)
                    {
                        self.isupdateTMD = true
                        self.flagForTMD = true
                        self.countOfDownloadItems += 1
                    }
                    print("Firmware Boolean \(self.isupdateFirmware) : Tmd Boolean \(self.isupdateTMD) : CNXVErsion Boolean \(self.isupdateConexant)")
                    
                    if(self.countOfDownloadItems > 0)
                    {
                        self.imgViewGifLoader.isHidden = true
                        
                        DispatchQueue.main.async(execute: {
                            
                            self.btnInstall.isUserInteractionEnabled = true
                            
                            self.imgViewGifLoader.isHidden = true
                        })
                    }
                    
                    self.chekingAvailability()
                    self.downloadFirmware()
                }
            }
            else{
                print("Error")
                Utils.stopActivityIndicatorOnView(self.view)
            }
            
        })
    }*/
}
