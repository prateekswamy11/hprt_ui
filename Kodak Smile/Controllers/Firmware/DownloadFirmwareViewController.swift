//
//  DownloadFirmwareViewController.swift
//  Snaptouch_Polaroid
//
//  Created by MAXIMESS082 on 02/01/18.
//  Copyright © 2018 maximess142. All rights reserved.
//

import UIKit
import SwiftyGif
import Alamofire
import CopilotAPIAccess

class DownloadFirmwareViewController: UIViewController,ResponceDelegate,DialogDelegate  {

    @IBOutlet weak var btnRefresh: UIButton!
    
    @IBOutlet weak var lblFWfileSendingPercent: UILabel!
    @IBOutlet weak var lblVCtitle: UILabel!
    @IBOutlet weak var btnInstallBackground: UIButton!
    @IBOutlet weak var imgViewCheckMarkFW: UIImageView!
    @IBOutlet weak var imgViewCheckMarkTMD: UIImageView!
    @IBOutlet weak var imgViewCheckMarkCNX: UIImageView!
    @IBOutlet weak var imgViewGifLoader: UIImageView!
    @IBOutlet weak var btnInstall: UIButton!
    @IBOutlet weak var lblInstallNote: UILabel!
    @IBOutlet weak var lblDownloadDetail: UILabel!
    @IBOutlet weak var view1DistanceFromAbove: NSLayoutConstraint!
    
    @IBOutlet weak var constraintInstallLblBottom: NSLayoutConstraint!
    
    @IBOutlet weak var lblFwPercent: UILabel!
    @IBOutlet weak var lblTmdPercent: UILabel!
    @IBOutlet weak var lblCnxPercent: UILabel!
    
    @IBOutlet weak var imgViewFwLoader: UIImageView!
    @IBOutlet weak var imgViewTmdLoader: UIImageView!
    @IBOutlet weak var imgViewCnxLoader: UIImageView!
    
    @IBOutlet weak var TMDViewHeight: NSLayoutConstraint!
    @IBOutlet weak var CNXViewHeight: NSLayoutConstraint!
    @IBOutlet weak var FirmwareViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewForTMD: UIView!
    @IBOutlet weak var viewForCNX: UIView!
    @IBOutlet weak var viewForFirmware: UIView!
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var btuInstallTMD: UIButton!
    //MARK :- Variables
    //  this initialization for use api in background mode
    //static let sharedInstance = DownloadFirmwareViewController()
    
    // New variables for Mint
    @IBOutlet weak var constraintBtnLetsGoHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintFirmwareTop: NSLayoutConstraint!
    @IBOutlet weak var constraintImgHeadUpTop: NSLayoutConstraint!
    @IBOutlet weak var constraintLblHeadUpTop: NSLayoutConstraint!
    @IBOutlet weak var constraintLetsGoBtnBottom: NSLayoutConstraint!
    
    @IBOutlet weak var lblFirmware: UILabel!
    @IBOutlet weak var imgLowBattery: UIImageView!
    @IBOutlet weak var lblLowBattery: UILabel!
    @IBOutlet weak var lblLowBatteryMsg: UILabel!
    @IBOutlet weak var lblShutDownMsg: UILabel!
    @IBOutlet weak var lblShutDownMsg2: UILabel!
    
    @IBOutlet weak var viewReadyToInstall: UIView!
    //    @IBOutlet weak var btnLetsGo: UIButton!

    @IBOutlet weak var imgHeadUp: UIImageView!
    @IBOutlet weak var lblHeadUp: UILabel!
    @IBOutlet weak var lblHeadUpMsg: UILabel!
    @IBOutlet weak var lblFirmwareUpdate: UILabel!
    @IBOutlet weak var lblDownloadPercentage: UILabel!
    
    var alamoFireManager : Alamofire.SessionManager!
    var request: Alamofire.Request?
    ///
    let gifManager = SwiftyGifManager(memoryLimit:10)
    var flagForFirmware = false
    var flagForCNX = false
    var flagForTMD = false
    
    ///
    
    var countOfDownloadItems = 0
    var downloadedItemsCount = 0
    
    var xCNXVersion = Float()
    var xFIRMVersion = Float()
    var xTMDVersion = Float()
    
     var newFIRMVersion = String()
    
    var generateCode = ""
    
    var isDownloadFileCNX = false
    var isDownloadFilexTMD = false
    var isDownloadFileFirm = false
    
    var isDownloadFWfile = true
    
    var isupdateFirmware = false{
        didSet{
            
        }
    }
    var isupdateConexant = false{
        didSet{
            if self.isupdateConexant == false{
            }else{
            }
            
        }
    }
    var isupdateTMD = false{
        didSet{
            if self.isupdateTMD == false{
            }else{
            }
            
        }
    }
    
    var updateVersionCode = ""
    var dictPrinterData:NSMutableDictionary?
    var batteryPer = Int()
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
//        self.imgViewFwLoader.isHidden = true
//
//        let gifImageLoader = UIImage(gifName:"loader")
//        self.imgViewGifLoader.backgroundColor = UIColor.clear
//        self.imgViewGifLoader.setGifImage(gifImageLoader, manager: gifManager, loopCount: -1)
        
        let gifImage = UIImage(gifName:"Rolling")
        self.imgViewFwLoader.setGifImage(gifImage, manager: gifManager, loopCount: -1)
        
         self.imgViewCnxLoader.setGifImage(gifImage, manager: gifManager, loopCount: -1)
        
         self.imgViewTmdLoader.setGifImage(gifImage, manager: gifManager, loopCount: -1)
        
        
        // Do any additional setup after loading the view.
        
        self.localizedStrings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
                
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(firmwareUpdateCancel),
            name: NSNotification.Name(rawValue: "FirmwareUpdateCancel"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(firmwareUpdateCancel),
            name: NSNotification.Name(rawValue: "FirmwareUpdateFaild"),
            object: nil)
        
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(AccessoriesDisConnect),
//            name: NSNotification.Name(rawValue: "AccessoryDisConnect"),
//            object: nil)
        
        /*NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateSendingFwDataPercent),
            name: NSNotification.Name(rawValue: "FWsendDataPercent"),
            object: nil)
 */
        self.btnRefresh.isHidden = true
        
        // disable phone lock feature
        
        UIApplication.shared.isIdleTimerDisabled = true
        //self.refresh()
        
        self.lblVCtitle.text = "Firmware Update".localisedString()
        self.lblDownloadDetail.text = "Please wait a while".localisedString()+"..."
//        self.hideInstallBtn()
        self.btuInstallTMD.isHidden = true
        self.imgViewTmdLoader.isHidden = true
        self.imgViewCnxLoader.isHidden = true
        self.imgViewFwLoader.isHidden = true
        
        self.imgViewCheckMarkFW.isHidden = true
        self.imgViewCheckMarkCNX.isHidden = true
        self.imgViewCheckMarkTMD.isHidden = true
        
        self.isupdateFirmware = false
        self.isupdateTMD = false
        self.isupdateConexant = false
        
         self.chekingAvailability()
//        self.imgViewGifLoader.isHidden = true
        self.lblFWfileSendingPercent.isHidden = true
//
//        EADSessionController.shared().delegate = PrinterQueueManager.sharedController
//        if(appDelegate().isConnectedToPrinter == true)
//        {
//
//            PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.get_ZIP_FIRMWARE, ResponseDelegate: self))
//
//        }
        self.getFirmware()
        self.hideInstallBtn()
        
        customLoader.showActivityIndicator(viewController: self.view)
        self.view.isUserInteractionEnabled = true
//        self.detectIphone5sDevice()
        
//        Utils.AlertMessage(message: "Note:- The firmware update may take up to 20 minutes. During the update, please don’t minimize or close the app, or you’ll need to restart the process.".localisedString())
         self.lblShutDownMsg.text = "Note:- The firmware update may take up to 20 minutes. During the update, please don’t minimize or close the app, or you’ll need to restart the process.".localisedString()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.downloadFirmware.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidDisappear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    
    @IBAction func refreshBtnClicked(_ sender: Any) {
        
        self.refresh()
        
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.flagForFirmware = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func installBtnAction(_ sender: Any) {
        
        if(appDelegate().isConnectedToPrinter == true)
        {
            PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.getZIPInfo, ResponseDelegate: self))
        }
        else
        {
//            let connectivityVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController
//            self.navigationController?.pushViewController(connectivityVC, animated: true)
            let cantSeeYourPrinter = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "CantSeeYourPrinterViewController") as! CantSeeYourPrinterViewController
            cantSeeYourPrinter.comeFromStr = "updateFailedVC"
            self.navigationController?.pushViewController(cantSeeYourPrinter, animated: true)
        }
    }
    
    @IBAction func installTMDbtnClicked(_ sender: Any) {
        
        if appDelegate().isConnectedToPrinter == true {
            
        
        if self.flagForTMD == true{
            
                self.lblFWfileSendingPercent.isHidden = false
                self.backBtn.isUserInteractionEnabled = false
            
                self.lblVCtitle.text = "Installing".localisedString()
                self.lblDownloadDetail.text = "Please wait a while".localisedString()+"..."
//                self.imgViewGifLoader.isHidden = false
                ZIP_SELECTED_FIRMWARE_FILE = "TMD"
                ZIP_FIRMWARE_FILE_NAME = newTMDfileName
                PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.updateZIPFirmware, ResponseDelegate: self))
           
        }
            
        }
        else{
            Utils.AlertMessage(message: "Please connect your device".localisedString())
        }

        
    }
    //MARK: - Functions

    //Assign values to fields.
    func localizedStrings() {
        self.lblFirmwareUpdate.text = "Firmware Update".localisedString()
        self.lblHeadUp.text = "Heads Up!".localisedString()
        self.lblHeadUpMsg.text = "We recommend connecting your printer to a power source".localisedString()
        self.lblShutDownMsg2.text = "Note:- The firmware update may take up to 20 minutes. During the update, please don’t minimize or close the app, or you’ll need to restart the process.".localisedString()
        self.btnInstall.setTitle( "Let's Go".localisedString(), for: .normal)
        self.btuInstallTMD.setTitle( "Install TMD".localisedString(), for: .normal)
    }
    
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintFirmwareTop.constant = 30.0
            constraintImgHeadUpTop.constant = 30.0
            constraintLblHeadUpTop.constant = 20.0
            constraintLetsGoBtnBottom.constant = 20.0
        }else{
            print("Unknown")
        }
    }
    
    func refresh()
    {
        self.request?.cancel()
        self.lblVCtitle.text = "FIRMWARE".localisedString()
        self.lblDownloadDetail.text = "Please wait a while...".localisedString()
//        self.hideInstallBtn()
        
        self.downloadedItemsCount = 0
        self.countOfDownloadItems = 0
        
        self.lblFwPercent.isHidden = false
        self.lblCnxPercent.isHidden = false
        self.lblTmdPercent.isHidden = false
        
        self.lblFwPercent.text = "Pending".localisedString()
        self.lblCnxPercent.text = "Pending".localisedString()
        self.lblTmdPercent.text = "Pending".localisedString()
        
        self.imgViewTmdLoader.isHidden = true
        self.imgViewCnxLoader.isHidden = true
        self.imgViewFwLoader.isHidden = true
        
        self.imgViewCheckMarkFW.isHidden = true
        self.imgViewCheckMarkCNX.isHidden = true
        self.imgViewCheckMarkTMD.isHidden = true
        
        self.isupdateFirmware = false
        self.isupdateTMD = false
        self.isupdateConexant = false
        
        self.chekingAvailability()
//        self.imgViewGifLoader.isHidden = true
        self.lblFWfileSendingPercent.isHidden = true
        self.updateFocusIfNeeded()
        PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.getVersion, ResponseDelegate: self))
    }
    
    
    func hideInstallBtn() {
        self.btnInstall.isHidden = true
        self.viewReadyToInstall.isHidden = true
    }
    
    func unHideInstallBtn() {
        self.btnInstall.isHidden = false
        self.viewReadyToInstall.isHidden = false
        customLoader.hideActivityIndicator()
    }
    
    
    func chekingAvailability() {
        
        if flagForCNX == false {
            CNXViewHeight.constant = 0
            viewForCNX.isHidden = true
        }
        else {
            CNXViewHeight.constant = 40
            viewForCNX.isHidden = false
        }
        
        
//        if flagForFirmware == false {
//            FirmwareViewHeight.constant = 0
//            viewForFirmware.isHidden = true
//        }
//        else {
//            FirmwareViewHeight.constant = 0
//            viewForFirmware.isHidden = true
//        }
        
        
        if flagForTMD == false {
            TMDViewHeight.constant = 0
            viewForTMD.isHidden = true
        }
        else {
            TMDViewHeight.constant = 40
            viewForTMD.isHidden = false
        }
    }
    
    
    func firmwareInstall() {
        let fwUpgradeStartedEvent = FirmwareUpgradeStartedAnalyticsEvent()
        Copilot.instance.report.log(event: fwUpgradeStartedEvent)

        
//        Utils.AlertMessage(message: "Note:- The firmware update may take up to 20 minutes. During the update, please don’t minimize or close the app, or you’ll need to restart the process.".localisedString())
        
        self.lblVCtitle.text = "Installing".localisedString()
        self.lblDownloadDetail.text = "Please wait a while...".localisedString()
        
        let obj = RequestOperation(type: RequestType.updateVersion, ResponseDelegate: self)
        
        obj.UpdateVersionCode = ""
        
        if self.isupdateFirmware{
            obj.UpdateVersionCode = "00"
            self.updateVersionCode = "00"
        }
        if self.isupdateConexant{
            obj.UpdateVersionCode = "01"
            self.updateVersionCode = "01"
        }
        if self.isupdateTMD{
            obj.UpdateVersionCode = "02"
            self.updateVersionCode = "02"
        }
        if self.isupdateFirmware && self.isupdateConexant{
            obj.UpdateVersionCode = "03"
            self.updateVersionCode = "03"
        }
        if self.isupdateFirmware && self.isupdateTMD{
            obj.UpdateVersionCode = "04"
            self.updateVersionCode = "04"
        }
        if self.isupdateConexant && self.isupdateTMD{
            obj.UpdateVersionCode = "05"
            self.updateVersionCode = "05"
        }
        if self.isupdateFirmware && self.isupdateConexant && self.isupdateTMD{
            obj.UpdateVersionCode = "06"
            self.updateVersionCode = "06"
            
        }

       /*
        if self.flagForFirmware == true{
            self.imgViewGifLoader.isHidden = false
            self.btnInstall.isUserInteractionEnabled = false
            self.btuInstallTMD.isUserInteractionEnabled = false
            ZIP_SELECTED_FIRMWARE_FILE = "Firmware"
            ZIP_FIRMWARE_FILE_NAME = newFIRMfileName
             PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.updateZIPFirmware, ResponseDelegate: self))
        }
        */

        
       
    }
    
    func downloadFirmware()  {
        if(isupdateFirmware)
        {
            self.downloadFirmware_TMD(0)
            
        }
        
        if(isupdateConexant)
        {
            self.downloadFirmware_TMD(2)
            
        }
    
        if(isupdateTMD)
        {
            self.downloadFirmware_TMD(1)
           
        }
        
    }
    
    func disMissView()  {
        self.lblFWfileSendingPercent.isHidden = true
//        self.imgViewGifLoader.isHidden = true
        self.btnInstall.isUserInteractionEnabled = true
        self.countOfDownloadItems = 0
        self.downloadedItemsCount = 0
        self.isDownloadFileCNX = false
        self.isDownloadFilexTMD = false
        self.isDownloadFileFirm = false
        clearOldValues()
//        let loaderVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "LoaderViewController") as! LoaderViewController
        let loaderVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        self.navigationController?.pushViewController(loaderVC, animated: true)
    }
    
    func clearOldValues() {
        UserDefaults.standard.set(true, forKey:  "clearValues")
        UserDefaults.standard.synchronize()
    }
    
    func downloadFirmware_TMD(_ download_Type: Int)
    {
        
        if WebserviceModelClass().isInternetAvailable() {
            var urlPath: String = ""
            var download_FileName:String = ""
            
            // select dowonload file name and url
            if download_Type == 0 && appDelegate().firmware_version_URL != ""{
                urlPath = appDelegate().firmware_version_URL
                print("appDelegate().firmware_version_URL : \(urlPath)")
                print("download_FileName = \(newFIRMfileName)")
                
                download_FileName = newFIRMfileName
            }else if download_Type == 1 && appDelegate().tmd_version_URL != ""{
                urlPath =  appDelegate().tmd_version_URL
                print("appDelegate().tmd_version_URL : \(urlPath)")
                
                download_FileName = newTMDfileName
            }else if download_Type == 2 && appDelegate().cnx_version_URL != ""{
                urlPath =  appDelegate().cnx_version_URL
                print("appDelegate().cnx_version_URL : \(urlPath)")
                
                download_FileName = newCNXfileName
            }
            
           
            
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                documentsURL.appendPathComponent(download_FileName)
                return (documentsURL, [.removePreviousFile])
            }
            
            let url = URL(string: urlPath)
            //let url = URL(string: "https://testurapp.com/skliq/public/Polaroid/firmware/Snap_touch_1.2_V1.00.16.04/PRINTER2.BRN")
            //Alamofire
            //self.alamoFireManager
           request = Alamofire.download(url!, method: .get, parameters: nil, encoding: JSONEncoding.default, to: destination)
                .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                    //print("Progress: \(progress.fractionCompleted)")
                    
                   
                    let per = String( Int (progress.fractionCompleted * 100))
                    print("per = \(per)")
                    DispatchQueue.main.async(execute: {
                        self.lblDownloadPercentage.text = "\(per)%"
                    })
                    /*/
                    DispatchQueue.main.async {
                        if URL(string: appDelegate().firmware_version_URL) == url && self.lblFwPercent.text != "100%"{
                            if per == "100"
                            {
                                self.downloadedItemsCount += 1
                                
                            }
                            self.imgViewFwLoader.isHidden = false
                            self.lblFwPercent.text = "\(per)%"
                        }
                        else if URL(string: appDelegate().cnx_version_URL) == url && self.lblCnxPercent.text != "100%"{
                            if per == "100"
                            {
                                self.downloadedItemsCount += 1
                            }
                            self.imgViewCnxLoader.isHidden = false
                            self.lblCnxPercent.text = "\(per)%"
                        }
                        
                        else if URL(string: appDelegate().tmd_version_URL) == url && self.lblTmdPercent.text != "100%"{
                            if per == "100"
                            {
                                self.downloadedItemsCount += 1
                                
                            }
                            self.imgViewTmdLoader.isHidden = false
                            self.lblTmdPercent.text = "\(per)%"
                        }
                      
                        
                    }
*/
                    
                    if progress.isFinished {
                         DispatchQueue.main.async {
                        if URL(string: appDelegate().firmware_version_URL) == url{
                            self.lblFwPercent.isHidden = true
                            self.imgViewFwLoader.isHidden = true
                            self.imgViewCheckMarkFW.isHidden = false
                             self.downloadedItemsCount += 1
                        }
                        else if URL(string: appDelegate().cnx_version_URL) == url{
                            self.lblCnxPercent.isHidden = true
                            self.imgViewCnxLoader.isHidden = true
                            self.imgViewCheckMarkCNX.isHidden = false
                             self.downloadedItemsCount += 1
                        }
                        else if URL(string: appDelegate().tmd_version_URL) == url{
                            self.lblTmdPercent.isHidden = true
                            self.imgViewTmdLoader.isHidden = true
                            self.imgViewCheckMarkTMD.isHidden = false
                             self.downloadedItemsCount += 1
                        }
                        
                        }
                        DispatchQueue.main.async {
                        
                        print("self.downloadedItemsCount = \(self.downloadedItemsCount)")
                        
                        print("self.countOfDownloadItems = \(self.countOfDownloadItems)")
                        
                        if(self.downloadedItemsCount == self.countOfDownloadItems)
                        {
                            if self.flagForFirmware == true{
                                self.imgViewFwLoader.isHidden = true
                                self.btnInstall.isHidden = false
                                self.viewReadyToInstall.isHidden = false
                                self.btnInstall.isUserInteractionEnabled = true
                                customLoader.hideActivityIndicator()
                            }
                            if self.flagForTMD == true{
                                self.btuInstallTMD.isHidden = false
                                self.btuInstallTMD.isUserInteractionEnabled = true
                            }
                           // self.unHideInstallBtn()
                            //self.btnInstall.isUserInteractionEnabled = true
                            if getCurrentIphone() == "X"
                            {
                                //self.constraintInstallLblBottom.constant = -10
                            }
                            else{
                                //self.constraintInstallLblBottom.constant = 14
                            }
                            
                            //self.lblInstallNote.text = "Install to Camera".localisedString()
                            self.lblDownloadDetail.text = "Ready to install".localisedString()
                            
                            self.isDownloadFWfile = false
                            
                        }
                            
                        }
                        
                        //Utils.stopActivityIndicatorOnView(self.view)
                    }
                }
                .validate { request, response, temporaryURL, destinationURL in
                    // Custom evaluation closure now includes file URLs (allows you to parse out error messages if necessary)
//                    customLoader.hideActivityIndicator()
                    return .success
                }
                .responseJSON { response in
                    debugPrint(response)
                    print(response.temporaryURL)
                    print(response.destinationURL)
//                    customLoader.hideActivityIndicator()
                    response.result.ifFailure {
                        if !WebserviceModelClass().isInternetAvailable(){
                            let ConnectInternet = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "ConnectToInternetViewController") as! ConnectToInternetViewController
                            ConnectInternet.isComeFromDownloadFWVC = true
                            self.navigationController?.pushViewController(ConnectInternet, animated: true)
                        }
                    }
            }
            
            
            
        }
    }
    
    //MARK: - GetVersionResponse Delegate
    
    func GetVersionResponse(_ Firmware:String ,Conexan:String,  TMD: String){
        
        let CNXstring =  Conexan.replacingOccurrences(of: ".", with: "")
        xCNXVersion = (Float(CNXstring.replacingOccurrences(of: "v", with: ""))!)//*10
        
        let TMDVstring =  TMD.replacingOccurrences(of: ".", with: "")
        xTMDVersion =  (Float(TMDVstring.replacingOccurrences(of: "v", with: ""))!)//*10
        
        let FIRMstring =  Firmware.replacingOccurrences(of: ".", with: "")
        xFIRMVersion =  Float(FIRMstring.replacingOccurrences(of: "v", with: ""))!
        
        let productCode = FIRMstring.character(at: 1)
        
        if productCode == "1"
        {
            appDelegate().Product_ID = "0"
        }
        else{
            appDelegate().Product_ID = "1"
        }
        
        //MARK:- error solve change by maximess
        var cnxVersion = String(xTMDVersion)
        cnxVersion.insert("v", at: cnxVersion.startIndex)
        var tmdVersion = String(xCNXVersion)
        tmdVersion.insert("v", at: tmdVersion.startIndex)
        
        
        
        if(appDelegate().isConnectedToPrinter == true)
        {
            btnInstall.isUserInteractionEnabled = true
            
            perform(#selector(self.getFirmware), with: nil, afterDelay: 0.3)
            
            
        }
        else
        {
            btnInstall.isUserInteractionEnabled = false
           
        }
    
    }
    
    func ZIP_accessory_info(_ dict: NSMutableDictionary) {
        
        print("dict= \(dict)")
        dictPrinterData = dict
        
        let batteryPerStr  =   "\(dictPrinterData!.value(forKey: "Battery")!)"
        print("batteryPerStr = \(batteryPerStr)")
        batteryPer = Int(batteryPerStr)!
        print("batteryPer = \(batteryPer)")
        
        if batteryPer < 11 {
            let lowBatteryVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "LowBatteryViewController") as! LowBatteryViewController
            self.navigationController?.pushViewController(lowBatteryVC, animated: true)
        }
        else{
            self.lblFWfileSendingPercent.isHidden = false
            self.backBtn.isUserInteractionEnabled = false
            self.firmwareInstall()
            
            let instalationProgressVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "InstalationProgressViewController") as! InstalationProgressViewController
            instalationProgressVC.newFIRMVersion =  self.newFIRMVersion
            instalationProgressVC.updateVersionCode = self.updateVersionCode
            
            self.navigationController?.pushViewController(instalationProgressVC, animated: true)
        }
    }
    
    func Update_Version_Suceess() {
        let fwUpgradeCompletedEvent = FirmwareUpgradeCompletedAnalyticsEvent(firmwareUpgradeStatus: .Success)
        Copilot.instance.report.log(event: fwUpgradeCompletedEvent)
        print("Update_Version_Suceess")
        appDelegate().isFirmwareUpdateAvailable = false
        self.backBtn.isUserInteractionEnabled = true
        Utils.AlertMessage(message: "Transfer firmware files successfully".localisedString())
        self.disMissView()
    }
    
    func firmwareUpdateFinished() {
        
        self.Update_Version_Suceess()
    }

    @objc func AccessoriesDisConnect()
    {
        let fwUpgradeCompletedEvent = FirmwareUpgradeCompletedAnalyticsEvent(firmwareUpgradeStatus: .Failure)
        Copilot.instance.report.log(event: fwUpgradeCompletedEvent)
        self.backBtn.isUserInteractionEnabled = true
        Utils.AlertMessage(message: "Device Disconnected".localisedString())
        self.disMissView()
    }
    
    @objc func firmwareUpdateCancel()
    {
        let fwUpgradeCompletedEvent = FirmwareUpgradeCompletedAnalyticsEvent(firmwareUpgradeStatus: .Failure)
        Copilot.instance.report.log(event: fwUpgradeCompletedEvent)
        self.backBtn.isUserInteractionEnabled = true
        self.lblFWfileSendingPercent.isHidden = true
//        self.imgViewGifLoader.isHidden = true
        self.btnInstall.isUserInteractionEnabled = true
        Utils.AlertMessage(message: "Firmware update cancel".localisedString())
        
    }
    
    @objc func firmwareUpdateFaild()
    {
        let fwUpgradeCompletedEvent = FirmwareUpgradeCompletedAnalyticsEvent(firmwareUpgradeStatus: .Failure)
        Copilot.instance.report.log(event: fwUpgradeCompletedEvent)
        self.backBtn.isUserInteractionEnabled = true
        self.lblFWfileSendingPercent.isHidden = true
//        self.imgViewGifLoader.isHidden = true
        self.btnInstall.isUserInteractionEnabled = true
        Utils.AlertMessage(message: "Firmware update faild".localisedString())
        
    }
    
    @objc func updateSendingFwDataPercent(_ notification: Notification)
    {
        print("notification.userInfo \(notification.userInfo)")
        
        if let strPercent = notification.userInfo?["percent"] as? String  {
            if Int(strPercent)! < 101
            {
            self.lblFWfileSendingPercent.text = strPercent + " %"
                
            }
        }
        
    }
    
    // MARK: -  API
    
    @objc func getFirmware()  {
        
        
//        self.imgViewGifLoader.isHidden = false
        
        var version = String()
        if appDelegate().Product_ID == "0"{
            version = "1.0"
            
        }else{
            version = "1.2"
            
        }
        
        //Old live url
        //WebserviceModelClass().getDataFor(module:"PlansViewController",subUrl:"snaptouch_firmware.php?ver=\(version)", completionHandler:{
            //(isSuccess,responseMessage,responseData) -> Void in
        
        //new url http://polaroidapps.ml/dev/get_mint_firmware.php?ver=1.0
        WebserviceModelClass().getDataFor(module:"PlansViewController",subUrl:FIRMWARE_URL, completionHandler:{
            (isSuccess,responseMessage,responseData) -> Void in
            
            
            if isSuccess
            {
                print("responseData = \(responseData)")
                if (responseData as! NSDictionary != nil)
                {
                    let data = (responseData as AnyObject).value(forKey: "data") as? NSDictionary
                    //let cnxDict = (responseData as AnyObject).value(forKey: "cnx") as? NSDictionary
                    let firmwareDict = data!.value(forKey: "firmware") as? NSDictionary
                    
                    let tmdDict = data!.value(forKey: "tmd") as? NSDictionary
                    
                    //New FW files download url
                    
                    if let url = tmdDict?.value(forKey: "url") as? String{
                        print("tmd_version_URL : \(url)")
                        
                        appDelegate().tmd_version_URL = url
                    }

                    if let url = firmwareDict?.value(forKey: "url") as? String{
                        
                        appDelegate().firmware_version_URL = url
                        print("firmware_version_URL : \(url)")
                    }
                    
                    //New FW Files name
                    
                    if let TMDfileName = tmdDict?.value(forKey: "filename") as? String {
                        
                        newTMDfileName =   TMDfileName
                    }
                    

                    
                    if let FIRMfileName = firmwareDict?.value(forKey: "filename") as? String{
                        newFIRMfileName =   FIRMfileName
                    }
                    
                    
                    //change by akshay
                    /*
                    var TMDstring = tmdDict?.value(forKey: "version") as! String
                    
                    if TMDstring == ""
                    {
                        TMDstring = "0"
                    }
                    
                    let TMDVersion =  float_t(TMDstring.replacingOccurrences(of: ".", with: ""))
                    */
                    var FIRMVstring = firmwareDict?.value(forKey: "version") as! String
                    
                    self.newFIRMVersion = FIRMVstring
                    
                    if FIRMVstring == ""
                    {
                        FIRMVstring = "0"
                    }
                    
                    let FIRMVersion = float_t(FIRMVstring.replacingOccurrences(of: ".", with: ""))
                    
                    print("FIRMVersion \(FIRMVersion) > self.XFIRMVersion \(self.xFIRMVersion)")
//                    if(FIRMVersion! > self.xFIRMVersion)
                    //if(600 > self.xFIRMVersion)
//                    {
                        self.isupdateFirmware = true
                        self.flagForFirmware = true
                        self.countOfDownloadItems += 1
//                    }
                    /*
                    print("TMDVersion \(TMDVersion) > self.XTMDVersion \(self.xTMDVersion)")
                    if(TMDVersion! > self.xTMDVersion)
                    //if(600 > self.xTMDVersion)
                    {
                        self.isupdateTMD = true
                         self.flagForTMD = true
                        self.countOfDownloadItems += 1
                    }
 */
                    print("Firmware Boolean \(self.isupdateFirmware) : Tmd Boolean \(self.isupdateTMD) : CNXVErsion Boolean \(self.isupdateConexant)")
                    
                    if(self.countOfDownloadItems > 0)
                    {
                        //self.imgViewGifLoader.isHidden = true

                        DispatchQueue.main.async(execute: {
                            
                            self.btnInstall.isUserInteractionEnabled = true
                            
                           // self.imgViewGifLoader.isHidden = true
                        })
                    }
                    
                    self.chekingAvailability()
                    self.downloadFirmware()
                }
            }
            else{
                print("Error")
                Utils.stopActivityIndicatorOnView(self.view)
                self.chekingAvailability()
                self.downloadFirmware()
            }
            
        })
    }

    
    

}
