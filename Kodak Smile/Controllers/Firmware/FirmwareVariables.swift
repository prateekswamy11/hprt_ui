//
//  FirmwareVariables.swift
//  Polaroid MINT
//
//  Created by maximess142 on 09/01/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation

// Firmware variables
var XCNXVersion = Float()
var XFIRMVersion = Float()
var XTMDVersion = Float()

var XFIRMVersionString = String()
