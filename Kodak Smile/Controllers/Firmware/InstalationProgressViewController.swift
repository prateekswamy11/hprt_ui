//
//  InstalationProgressViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 20/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class InstalationProgressViewController: UIViewController ,ResponceDelegate,DialogDelegate{
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblHangtightTop: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnDoneBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnDoneHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblFirmware: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgProgressBG: UIImageView!
    @IBOutlet weak var lblHangtight: UILabel!
    @IBOutlet weak var lblProgressMsg: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var progressBar: ProgressBarView!
    @IBOutlet weak var lblProgressCount: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    //MARK: - Variables
    var timer: Timer!
    var progressCounter:Float = 0
    let duration:Float = 100.0
    var progressIncrement:Float = 0
    var counter : Int = 0
    var updateVersionCode = ""
    var newFIRMVersion = String()
    var updateCompletedSuccessful : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
        // Do any additional setup after loading the view.
        
        
        btnDone.isUserInteractionEnabled = false
        btnBack.isUserInteractionEnabled = false
        btnDone.alpha = 0.5
        btnBack.alpha = 0.5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        currentVC = "InstalationProgressViewController"
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateSendingFwDataPercent),
            name: NSNotification.Name(rawValue: "FWsendDataPercent"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(firmwareUpdateCancel),
            name: NSNotification.Name(rawValue: "FirmwareUpdateFaild"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(AccessoriesDisConnect),
            name: NSNotification.Name(rawValue: "AccessoryDisConnect"),
            object: nil)
        
        self.detectIphone5sDevice()
        
        //Set Border & timer to show progress.
//        progressBar.layer.borderWidth = 20.0
        progressBar.layer.cornerRadius = 60.0
//        progressBar.layer.borderColor = UIColor(red: 0.0, green: 180.0, blue: 179.0, alpha: 0.3).cgColor
        
//        progressIncrement = 1.0/duration
        //Start timer to show installation progress.
//        counter = 0
//        if  ((timer) != nil) {
//            timer.invalidate()
//            progressCounter = 0
//        }
        
        self.lblProgressCount.text = "0%"
        progressBar.progress = 0

        //timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.showProgress), userInfo: nil, repeats: true)
        
        firmwareInstall()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.installFirmware.rawValue)
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Function
    //Assign values to fields.
    func localizedStrings() {
//        customLoader.startGifLoader(viewController: self.view)
        self.lblFirmware.text = "Firmware Update".localisedString()
        self.viewContainer.isHidden = false
        self.lblHangtight.text = "Hang Tight, It’s Worth the Wait".localisedString()
        self.lblProgressMsg.text = "Keep the app open until the upgrade is completed ".localisedString()
        self.btnDone.setTitle("Done".localisedString(), for: .normal)
    }
    
    func clearOldValues() {
        UserDefaults.standard.set(true, forKey:  "clearValues")
        UserDefaults.standard.synchronize()
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            self.constraintLblHangtightTop.constant = 20.0
//            constraintBtnDoneHeight.constant = 30.0
//            self.btnDone.cornerRadius = 15.0
        }else{
            
            print("Unknown")
        }
    }
    
    func enableButton(){
        
        btnDone.isUserInteractionEnabled = true
        btnDone.alpha = 1.0
        btnBack.isUserInteractionEnabled = true
        btnBack.alpha = 1.0
        updateCompletedSuccessful = true
        
    }
    //Start Progress Bar
    @objc func showProgress()
    {
        if(counter >= 101)
        {
//            enableButton()
           
        }
        else
        {
        self.lblProgressCount.text = "\(String(counter))%"
        counter = 1 + counter
        progressBar.progress = progressCounter
        progressCounter = progressCounter + progressIncrement
        }
    }
    
    
    func firmwareInstall() {
        let obj = RequestOperation(type: RequestType.updateVersion, ResponseDelegate: self)
        
        obj.UpdateVersionCode = self.updateVersionCode
        
            ZIP_SELECTED_FIRMWARE_FILE = "Firmware"
            ZIP_FIRMWARE_FILE_NAME = newFIRMfileName
            PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.updateZIPFirmware, ResponseDelegate: self))
        
    }
    
    @objc func updateSendingFwDataPercent(_ notification: Notification)
    {
        print("notification.userInfo \(notification.userInfo)")
        
        if let strPercent = notification.userInfo?["percent"] as? String  {
            if Int(strPercent)! <= 100
            {
                if Int(strPercent)! == 99{
                    self.lblProgressCount.text = "\(100)%"
                }
                self.lblProgressCount.text = "\(String(Int(strPercent)!))%"
                progressBar.progress = (Float(strPercent)!/100.0)
            }
        }
        
    }
    
    @objc func AccessoriesDisConnect()
    {
        //self.backBtn.isUserInteractionEnabled = true
//        Utils.AlertMessage(message: "Device Disconnected".localisedString())
        if !updateCompletedSuccessful{
            let UpdateFailedVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "UpdateFailedViewController") as! UpdateFailedViewController
            UpdateFailedVC.isFirmwareUpdateFailed = false
            self.navigationController?.pushViewController(UpdateFailedVC, animated: true)
        }else{
            // Don't Go Anywhere
        }
        //self.disMissView()
//        self.clearOldValues()
    }
    
    @objc func firmwareUpdateFaild()
    {
        
        //Utils.AlertMessage(message: "Firmware update faild".localisedString())
        let UpdateFailedVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "UpdateFailedViewController") as! UpdateFailedViewController
        UpdateFailedVC.isFirmwareUpdateFailed = true
        self.navigationController?.pushViewController(UpdateFailedVC, animated: true)
    }
    
    func Update_Version_Suceess() {
        print("Update_Version_Suceess")
//         self.clearOldValues()
        if progressBar.progress < 1.0
        {
            self.lblProgressCount.text = "100%"
            progressBar.progress = 1.0
        }
        self.enableButton()
        appDelegate().isFirmwareUpdateAvailable = false
       

        //Utils.AlertMessage(message: "Transfer firmware files successfully".localisedString())
//        let UpdateCompleteVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "UpdateCompleteViewController") as! UpdateCompleteViewController
//        UpdateCompleteVC.newFIRMVersion = newFIRMVersion
//        self.navigationController?.pushViewController(UpdateCompleteVC, animated: true)
    }
    
    func firmwareUpdateFinished() {
        
        self.Update_Version_Suceess()
    }
    
    @objc func firmwareUpdateCancel()
    {
    
//        Utils.AlertMessage(message: "Firmware update cancel".localisedString())
        let UpdateFailedVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "UpdateFailedViewController") as! UpdateFailedViewController
        UpdateFailedVC.isFirmwareUpdateFailed = true
        self.navigationController?.pushViewController(UpdateFailedVC, animated: true)
        
    }
    
    // MARK: - Action Methods
    
    @IBAction func backBtnClicked(_ sender:Any)
    {
        if(!updateCompletedSuccessful)
        {
        self.navigationController?.popViewController(animated: true)
        }
         self.Update_Version_Suceess()
        
    }
    
    @IBAction func doneBtnClicked(_ sender:Any)
    {
//        customLoader.stopGifLoader()
        self.clearOldValues()
        let UpdateComplete = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "UpdateCompleteViewController") as! UpdateCompleteViewController
        UpdateComplete.newFIRMVersion = newFIRMVersion
        self.navigationController?.pushViewController(UpdateComplete, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
