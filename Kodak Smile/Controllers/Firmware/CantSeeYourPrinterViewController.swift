//
//  CantSeeYourPrinterViewController.swift
//  Polaroid MINT
//
//  Created by MAXIMESS152 on 23/01/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

class CantSeeYourPrinterViewController: UIViewController {

    @IBOutlet weak var lblFirmware: UILabel!
    @IBOutlet weak var lblCantSee: UILabel!
    @IBOutlet weak var lblConnectPrinter: UILabel!
    @IBOutlet weak var btnConnect: UIButton!
    //MARK: - Variables -
    var comeFromStr = String()
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.localizedStrings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        if appDelegate().isConnectedToPrinter {
//            self.dismissVC()
//        }
    }
    
    //Assign values to fields.
    func localizedStrings() {
        self.lblFirmware.text = "Firmware".localisedString()
        self.lblCantSee.text = "Can’t see your printer".localisedString()
        self.lblConnectPrinter.text = "Connect your printer to view its firmware version".localisedString()
        self.btnConnect.setTitle( "Connect my printer".localisedString(), for: .normal)
    }
    
    //MARK:- IBActions
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.dismissVC()
    }
    
    @IBAction func connectBtnClicked(_ sender: UIButton) {
        let connectivityVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController
        connectivityVC.comeFromStr = self.comeFromStr
        self.navigationController?.pushViewController(connectivityVC, animated: true)
    }
    
    //MARK:- Functions
    
    @objc func initialSetup() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.dismissVC), name: NSNotification.Name("dismissCantSeePrinterVC"), object: nil)
    }
    @objc func dismissVC() {
        self.navigationController?.popViewController(animated: true)
    }
}
