//
//  PrinterNotAvailableViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 20/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PrinterNotAvailableViewController: UIViewController {
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblFirmwareTop: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnConnectMyPrinterBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnConnectMyPrinterHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintImgFirmwareUpdateTop: NSLayoutConstraint!
    
    @IBOutlet weak var imgFirmwareUpdate: UIImageView!
    @IBOutlet weak var lblFirmware: UILabel!
    @IBOutlet weak var lblFirmwareStatus: UILabel!
    @IBOutlet weak var lblUpdateAvailable: UILabel!
    @IBOutlet weak var btnConnectMyPrinter: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.detectIphone5sDevice()
    }
    
    //Assign values to fields.
    func localizedStrings() {
         self.lblFirmware.text = "Firmware".localisedString()
        self.lblFirmwareStatus.text = "Can't see your printer".localisedString()
        self.lblUpdateAvailable.text = "Connect your printer to view its firmware version".localisedString()
        self.btnConnectMyPrinter.setTitle( "Connect my printer".localisedString(), for: .normal)
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblFirmwareTop.constant = 30.0
            constraintBtnConnectMyPrinterBottom.constant = 20.0
            constraintImgFirmwareUpdateTop.constant = 30.0
//            constraintBtnConnectMyPrinterHeight.constant = 30.0
//            self.btnConnectMyPrinter.cornerRadius = 15.0
        }else{
            
            print("Unknown")
        }
    }
    
    // MARK: - Action Methods
    
    @IBAction func backBtnClicked(_ sender:Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnConnectToPrinterClicked(_ sender: Any)
    {        
        //This navigation only for testing purpose.
//        let FirmwareUpdateAvailable = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "FirmwareUpdateAvailableViewController") as! FirmwareUpdateAvailableViewController
//        let FirmwareUpdateAvailable = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "FirmwareUptoDateViewController") as! FirmwareUptoDateViewController
//        let FirmwareUpdateAvailable = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "HeadUpPrinterViewController") as! HeadUpPrinterViewController
//        self.navigationController?.pushViewController(FirmwareUpdateAvailable, animated: true)
        
        let connectivityVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController
        connectivityVC.comeFromStr = "firmwareVC"
        self.navigationController?.pushViewController(connectivityVC, animated: true)
        
    }
}
