//
//  ConnectToInternetViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 20/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class ConnectToInternetViewController: UIViewController {
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblFirmwareTop: NSLayoutConstraint!
    @IBOutlet weak var constraintImgFirmwareUpdateTop: NSLayoutConstraint!
    
    @IBOutlet weak var imgFirmwareUpdate: UIImageView!
    @IBOutlet weak var imgInternetConnect: UIImageView!
    @IBOutlet weak var lblFirmwareVersionTitle: UILabel!
    @IBOutlet weak var lblFirmwareVersion: UILabel!
    @IBOutlet weak var lblFirmware: UILabel!
    @IBOutlet weak var lblConnectToInternet: UILabel!
    
    //MARK: - Variables -
    var reachability : Reachability!
    var networkStatus : Reachability.NetworkStatus!
    var isComeFromDownloadFWVC : Bool! = false
    
    //MARK: - View LifeCycle Functions -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
        self.networkReachability()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.detectIphone5sDevice()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        isComeFromDownloadFWVC = false
    }
    
    //Assign values to fields.
    func localizedStrings() {
        self.lblFirmware.text = "Firmware".localisedString()
        self.lblFirmwareVersionTitle.text = "Your printer firmware version:".localisedString()
        self.lblFirmwareVersion.text = "Version".localisedString() + " " + XFIRMVersionString
        self.lblConnectToInternet.text = "Connect to the internet to check for  a firmware update".localisedString()
    }
    
    func networkReachability() {
        reachability = Reachability()
        NotificationCenter.default.addObserver(self, selector:#selector(self.checkNetworkStatus), name: ReachabilityChangedNotification, object: nil);
        do {
            try reachability?.startNotifier()
        } catch {
            print("This is not working.")
            return
        }
    }
    
    @objc func checkNetworkStatus()
    {
        networkStatus = Reachability()?.currentReachabilityStatus
        
        if (networkStatus == Reachability.NetworkStatus.notReachable)
        {
            print("Not Reachable")
        }
        else
        {
            self.view.makeToast("Connected".localisedString())
            self.getFirmware()
        }
    }
    
    func getFirmware()  {
        
        customLoader.showActivityIndicator(viewController: self.view)
        let version = "1.0"
        /*
         if appDelegate().Product_ID == "0"{
         version = "1.0"
         }else{
         version = "1.2"
         }
         */
        //Old live url
        //            (isSuccess,responseMessage,responseData) -> Void in
        
        //http://polaroidapps.ml/dev/polaroid-new/mint/get_firmware/1.0
        //http://polaroidapps.ml/dev/
        
        WebserviceModelClass().getDataFor(module:"PlansViewController",subUrl:FIRMWARE_URL, completionHandler:{
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                print("responseData = \(responseData)")
                if (responseData as! NSDictionary != nil)
                {
                    //Akshay Solve crash detect on crashanalitics Add guard
                    
                    
                    
                    
                    guard let data = (responseData as AnyObject).value(forKey: "data") as? NSDictionary
                        else {
                            return
                    }
                    
                    //let firmwareDict = data!.value(forKey: "firmware") as? NSDictionary
                    
                    //let tmdDict = data!.value(forKey: "tmd") as? NSDictionary
                    guard let tmdDict = data.value(forKey: "tmd") as? NSDictionary
                        else {
                            return
                    }
                    
                    //let cnxDict = (responseData as AnyObject).value(forKey: "cnx") as? NSDictionary
                    guard  let firmwareDict = data.value(forKey: "firmware") as? NSDictionary
                        else {
                            return
                    }
                    
                    // let TMDVersion = (tmdDict?.value(forKey: "version") as AnyObject).floatValue
                    
                    //let CNXVersion = (cnxDict?.value(forKey: "version") as AnyObject).floatValue
                    
                    // let FIRMVersion = (firmwareDict?.value(forKey: "version") as AnyObject).floatValue
                    
                    
                    //change by akshay
                    /*
                     guard var TMDstring = tmdDict.value(forKey: "version") as? String
                     else {
                     return
                     }
                     
                     if TMDstring == ""
                     {
                     TMDstring = "0"
                     }
                     
                     let TMDVersion =  float_t(TMDstring.replacingOccurrences(of: ".", with: ""))
                     */
                    
                    //(tmdDict?.value(forKey: "version") as AnyObject).floatValue
                    //  let CNXLastUpdate = tmdDict?.value(forKey: "lastUpdateOn")
                    /*
                     var CNXstring = cnxDict?.value(forKey: "version") as! String
                     
                     if CNXstring == ""
                     {
                     CNXstring = "0"
                     }
                     
                     let CNXVersion = float_t(CNXstring.replacingOccurrences(of: ".", with: ""))//(cnxDict?.value(forKey: "version") as AnyObject).floatValue
                     */
                    guard var FIRMVstring = firmwareDict.value(forKey: "version") as? String
                        else {
                            return
                    }
                    if FIRMVstring == ""
                    {
                        FIRMVstring = "0"
                    }
                    
                    let FIRMVersion = float_t(FIRMVstring.replacingOccurrences(of: ".", with: ""))
                    //(firmwareDict?.value(forKey: "version") as AnyObject).floatValue
                    //print("CNXVersion \(CNXVersion) > self.XCNXVersion \(self.XCNXVersion)")
                    
                    customLoader.hideActivityIndicator()
                    //print("\(FIRMVersion!) > \(XFIRMVersion) || \(TMDVersion!) > \(XTMDVersion) ")
                    
                    print("\(FIRMVersion!) > \(XFIRMVersion)  ")
                    
                    if( FIRMVersion! > XFIRMVersion) //|| TMDVersion! > XTMDVersion )
                    {
                        appDelegate().isFirmwareUpdateAvailable = true
                        let fwUpdateAvailableVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "FirmwareUpdateAvailableViewController") as! FirmwareUpdateAvailableViewController
                        fwUpdateAvailableVC.cameFrom = "ConnectToInternet"
                        self.navigationController?.pushViewController(fwUpdateAvailableVC, animated: false)
                        //Utils.AlertMessage(message: "New Firmware is available")
                        
//                        let alert = UIAlertController(title: AppName, message: "New Firmware is available".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
//
//
//                        alert.addAction(UIAlertAction(title: "Update".localisedString(), style: UIAlertActionStyle.default, handler: { action in
//
//                            let fwUpdateVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "FirmwareUpdateAvailableViewController") as! FirmwareUpdateAvailableViewController
//
//                            self.navigationController?.pushViewController(fwUpdateVC, animated: true)
//
//                        }))
//                        alert.addAction(UIAlertAction(title: "Cancel".localisedString(), style: UIAlertActionStyle.cancel, handler: nil))
//
//
//                        self.present(alert, animated: true, completion: nil)
                    }
                    else{
                        appDelegate().isFirmwareUpdateAvailable = false
                        let fwUptoDateVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "FirmwareUptoDateViewController") as! FirmwareUptoDateViewController
                        fwUptoDateVC.cameFrom = "ConnectToInternet"
                        self.navigationController?.pushViewController(fwUptoDateVC, animated: true)
                    }
                    
                }
            }
            else{
                print("Error")
                customLoader.hideActivityIndicator()
            }
            
        })
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblFirmwareTop.constant = 30.0
            constraintImgFirmwareUpdateTop.constant = 30.0
        }else{
            print("Unknown")
        }
    }
    
    // MARK: - Action Methods
    
    @IBAction func backBtnClicked(_ sender:Any)
    {
        if isComeFromDownloadFWVC{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: FirmwareUpdateAvailableViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
}
