//
//  FirmwareUpdatePopupViewController.swift
//  Polaroid ZIP
//
//  Created by MacBook003 on 20/11/18.
//  Copyright © 2018 maximess142. All rights reserved.
//

import UIKit

class FirmwareUpdatePopupViewController: UIViewController {

    @IBOutlet weak var lblUpdateFirmaware: UILabel!
    @IBOutlet weak var btnUpdateFirmware: UIButton!
    @IBOutlet weak var lblNoCopies: UILabel!
    @IBOutlet weak var lblUseSelectToPrint: UILabel!
    @IBOutlet weak var lblPrintMulticopy: UILabel!
    @IBOutlet weak var lblNecesaryFirmareUpgrade: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
        // Do any additional setup after loading the view.
    }
    //MARK: - Functions
    func localizedStrings() {
                self.lblNecesaryFirmareUpgrade.text = "Necessary Firmware Upgrade".localisedString()
                self.lblUpdateFirmaware.text = "Update you firmware to gain access to Multi-Print:".localisedString()
                self.lblPrintMulticopy.text = "PRINT MULTIPLE COPIES FASTER!".localisedString()
                self.lblNoCopies.text = "Number of Copies".localisedString()
        self.lblUseSelectToPrint.text = "USE SELECT TO PRINT MULTIPLE IMAGES AT ONCE!".localisedString()
         self.btnUpdateFirmware.setTitle("UPDATE FIRMWARE".localisedString(), for: .normal)
    }
    
    //MARK: - IBActions
    @IBAction func updateFirmwareBtnClicked(_ sender: Any) {
        
        let fwUpdateVC = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "UpdateFirmwareViewController") as! UpdateFirmwareViewController
        
        self.navigationController?.pushViewController(fwUpdateVC, animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
