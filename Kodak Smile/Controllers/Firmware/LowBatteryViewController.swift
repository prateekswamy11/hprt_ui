//
//  LowBatteryViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 21/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class LowBatteryViewController: UIViewController, ResponceDelegate{
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblFirmwareTop: NSLayoutConstraint!
    @IBOutlet weak var constraintImgLowBatteryTop: NSLayoutConstraint!
    @IBOutlet weak var constraintLblLowBatteryTop: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnLetsGoBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnLetsGoHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblFirmware: UILabel!
    @IBOutlet weak var imgLowBattery: UIImageView!
    @IBOutlet weak var lblLowBattery: UILabel!
    @IBOutlet weak var lblLowBatteryMsg: UILabel!
    @IBOutlet weak var lblShutDownMsg: UILabel!
    @IBOutlet weak var btnLetsGo: UIButton!

    //MARK: - Variables -
    var dictPrinterData:NSMutableDictionary?
    var batteryPer = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.detectIphone5sDevice()
    }
    
    //Assign values to fields.
    func localizedStrings() {
        self.lblFirmware.text = "Firmware Update".localisedString()
        self.lblLowBattery.text = "Low Battery".localisedString()
        self.lblLowBatteryMsg.text = "Your printer’s battery is low.\nPlease connect it to a power source.".localisedString()
        self.lblShutDownMsg.text = "Once the firmware update is complete, your printer will shut down".localisedString()
        self.btnLetsGo.setTitle( "Let's Go".localisedString(), for: .normal)
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblFirmwareTop.constant = 30.0
            constraintImgLowBatteryTop.constant = 30.0
            constraintLblLowBatteryTop.constant = 30.0
            constraintBtnLetsGoBottom.constant = 20.0
//            constraintBtnLetsGoHeight.constant = 30.0
//            self.btnLetsGo.cornerRadius = 15.0
        }else{
            
            print("Unknown")
        }
    }
    
    // MARK: - Action Methods
    
    @IBAction func backBtnClicked(_ sender:Any) {
        if let navVC = self.navigationController {
            for item in navVC.viewControllers {
                if item.isKind(of: FirmwareUpdateAvailableViewController.self) {
                    self.navigationController?.popToViewController(item, animated: false)
                }
            }
        }
    }
    
    @IBAction func letsGoBtnClicked(_ sender:Any)
    {
//        let InstalationProgress = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "InstalationProgressViewController") as! InstalationProgressViewController
//        self.navigationController?.pushViewController(InstalationProgress, animated: true)

        if(appDelegate().isConnectedToPrinter == true)
        {
            PrinterQueueManager.RequestArray.append(RequestOperation(type: RequestType.getZIPInfo, ResponseDelegate: self))
        }
        else
        {
            let connectivityVC = storyboards.connectivityStoryboard.instantiateViewController(withIdentifier: "ConnectivityViewController") as! ConnectivityViewController
            self.navigationController?.pushViewController(connectivityVC, animated: true)
        }
    }
    
    //MARK: - Functions -
    func ZIP_accessory_info(_ dict: NSMutableDictionary) {
        
        print("dict= \(dict)")
        dictPrinterData = dict
        
        let batteryPerStr  =   "\(dictPrinterData!.value(forKey: "Battery")!)"
        print("batteryPerStr = \(batteryPerStr)")
        batteryPer = Int(batteryPerStr)!
        print("batteryPer = \(batteryPer)")
        
        if batteryPer < 11 {
            let batteryLowVC = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpLowBatteryViewController") as! PopUpLowBatteryViewController
            showPopupViewController(viewController: batteryLowVC)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func showPopupViewController(viewController : UIViewController)
    {
        // Show the pop up
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
        
        rootViewController?.view.addSubview(viewController.view)
        rootViewController?.addChildViewController(viewController)
        viewController.view.frame = CGRect(x: 0, y: 0, width: rootViewController?.view.frame.width ?? 0.0, height: rootViewController?.view.frame.height ?? 0.0)
    }
}
