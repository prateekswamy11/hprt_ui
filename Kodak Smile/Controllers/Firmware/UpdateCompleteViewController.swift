//
//  UpdateCompleteViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 20/11/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class UpdateCompleteViewController: UIViewController {
    
    //MARK: - Outlets -
    @IBOutlet weak var constraintLblFirmwareTop: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnDoneBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnDoneHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintImgUpdateCompleteTop: NSLayoutConstraint!
    
    @IBOutlet weak var lblFirmware: UILabel!
    @IBOutlet weak var imgeUpdateComplete: UIImageView!
    @IBOutlet weak var lblUpdateComplete: UILabel!
    @IBOutlet weak var lblUpdateCompleteMsg: UILabel!
    @IBOutlet weak var lblUpdateVersion: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var lblPrinterShutDownMsg: UILabel!
    
    var newFIRMVersion = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizedStrings()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.detectIphone5sDevice()
    }
    
    //Assign values to fields.
    func localizedStrings() {
        self.lblFirmware.text = "Firmware Update".localisedString()
        self.lblUpdateComplete.text = "Update Complete".localisedString()
        self.lblUpdateCompleteMsg.text = "Your printer firmware is up to date".localisedString()
        self.lblUpdateVersion.text = "Version".localisedString() + newFIRMVersion
        self.btnDone.setTitle("Print".localisedString(), for: .normal)
        self.lblPrinterShutDownMsg.text = "Your printer will shut down now. You can turn it back on and continue printing".localisedString()
    }
    
    //Function to detect smaller device(iphone5s) to adjust UIelements on screen
    func detectIphone5sDevice()
    {
        if getCurrentIphone() == "5"{
            constraintLblFirmwareTop.constant = 30.0
            constraintBtnDoneBottom.constant = 20.0
            constraintImgUpdateCompleteTop.constant = 30.0
            //            constraintBtnDoneHeight.constant = 30.0
            //            self.btnDone.cornerRadius = 15.0
        }else{
            
            print("Unknown")
        }
    }
    
    // MARK: - Action Methods
    
    @IBAction func backBtnClicked(_ sender:Any)
    {
        appDelegate().isFirmwareUpdateAvailable = false
        let homeVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @IBAction func doneBtnClicked(_ sender:Any)
    {
        appDelegate().isFirmwareUpdateAvailable = false
        let homeVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
