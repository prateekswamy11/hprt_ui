//
//  PopUpARAlertScanFirstTimeViewController.swift
//  Kodak Smile
//
//  Created by MAXIMESS194 on 8/28/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

protocol DismissARAlertScan {
    func makeChanges()
}

class PopUpARAlertScanFirstTimeViewController: UIViewController, CameraPermissinNativePopupDelegate{

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    var delegate: DismissARAlertScan!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnOkClicked(_ sender: Any)
    {
        if WebserviceModelClass().isInternetAvailable(){
            self.delegate.makeChanges()
        }else{
            let vc = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
            self.present(vc, animated: true, completion: nil)
            vc.iscameFromScanNow = true
        }
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    @IBAction func btnCrossClicked(_ sender: Any)
    {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    func showCameraPermissionPopup() {
        print("")
    }
}
