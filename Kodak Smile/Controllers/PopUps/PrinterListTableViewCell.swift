//
//  PrinterListTableViewCell.swift
//  Kodak Smile
//
//  Created by maximess206 on 08/06/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import UIKit

class PrinterListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPrinterName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
