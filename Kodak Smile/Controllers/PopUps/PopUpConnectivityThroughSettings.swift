//
//  File.swift
//  Kodak Classic
//
//  Created by maximess175 on 19/03/20.
//  Copyright © 2020 maximess. All rights reserved.
//


import UIKit

class PopUpConnectivityThroughSettings: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = "Connect the printer"
        lblText.text = "Please follow below steps to connect printer"
        btnOk.setTitle("Go to settings", for: .normal)
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnOkClicked(_ sender: Any) {
        self.openBluetooth()
    }
    
    func openBluetooth(){
        self.dismiss(animated: true, completion: nil)
        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
        UIApplication.shared.open(settingsUrl)
    }
}
