//
//  PopUpNewFirmwareAvailableViewController.swift
//  Kodak Smile
//
//  Created by maximess175 on 25/06/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

class PopUpNewFirmwareAvailableViewController: UIViewController, PrinterStatusDelegate{

    @IBOutlet weak var lblFirmwareUpdateAvailable: UILabel!
    @IBOutlet weak var lblANewFirmUpdateIsAvailable: UILabel!
    @IBOutlet weak var btnUpdateNow: UIButton!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var imgVwCross: UIImageView!
    
    //MARK: - ViewLifeCycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        ConnectionStatus.sharedController.printerStatusDelegate = self
        lblFirmwareUpdateAvailable.text = "Firmware Update Available".localisedString()
        lblANewFirmUpdateIsAvailable.text = "A new firmware update is available for your KODAK SMILE Instant Digital Printer.".localisedString()
        self.btnUpdateNow.setTitle("Update Now".localisedString(), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.forceFirmwarePopupCustomization()
        IS_POP_SHOWN = true
    }
    
    //MARK: - IBActions -
    @IBAction func dismissPopupBtnClicked(_ sender: UIButton) {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        UserDefaults.standard.set(true, forKey: "cancelFirmwarePopUP")
        IS_POP_SHOWN = false
    }
    
    @IBAction func showUpdateFirmwareScreenBtnClicked(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "cancelFirmwarePopUP")
        IS_POP_SHOWN = false
        let fWUpdateScreen = storyboards.firmwareUpdateStoryboard.instantiateViewController(withIdentifier: "FirmwareUpdateAvailableViewController") as! FirmwareUpdateAvailableViewController
        self.navigationController?.pushViewController(fWUpdateScreen, animated: true)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    //MARK: - Functions -
    func forceFirmwarePopupCustomization() {
        //is_Force_Firmware_Available Flag used to show/hide cross btn
        if IS_FORCE_UPDATE == "yes"{
            self.imgVwCross.isHidden = true
            self.btnCross.isHidden = true
            self.lblANewFirmUpdateIsAvailable.text = "A new firmware update is available for your KODAK SMILE Instant Digital Printer. Please update in order to use the app.".localisedString()
        }
    }
}
