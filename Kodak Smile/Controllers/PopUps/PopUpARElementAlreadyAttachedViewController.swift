//
//  PopUpARElementAlreadyAttachedViewController.swift
//  Kodak Smile
//
//  Created by MAXIMESS194 on 7/19/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import Alamofire

protocol PrintStartAfterARDelegate {
    func printAfterResponseCompletion(_ status:Bool)
}
var ischeckupdate_ARPopup = false
class PopUpARElementAlreadyAttachedViewController: UIViewController {

    @IBOutlet weak var lblARElementDesc: UILabel!
    @IBOutlet weak var lblARElementAttached: UILabel?
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var btnSelectPhoto: UIButton!
    var results : Any?
    let imagePreVC = ImagePreviewViewController()
    var userID :String = ""

    var delegate : PrintStartAfterARDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        if SingletonClass.sharedInstance.targetAlreadyPresent ?? false{
            lblARElementDesc.text = "Do you want to update AR video?"
            lblARElementAttached?.text = "AR Element Already Attached"
            btnSelectPhoto.setTitle("Update AR Element", for: UIControlState.normal)

        }else{
            lblARElementDesc.text = "An AR Element Already Attached to this photo"
            lblARElementAttached?.text = "AR Element Already Attached"
            btnSelectPhoto.setTitle("Select Another Photo", for: UIControlState.normal)
        }

        // Do any additional setup after loading the view.
    }

    //MARK: - Function -
    func modify_EasyAR(){
        DispatchQueue.global(qos: .background).async {
        if WebserviceModelClass().isInternetAvailable() {
            let upload_modify = BASE_URL_SERVER_END+MODIFY_TARGET+"\(SingletonClass.sharedInstance.targetId ?? "")"
            let meta_Str = self.stringify(json:UserDefaults.standard.value(forKey: "meta_To_modifyImg")!)
            let rawdata = "active" + "1" + "appKey" + "\(cloud_key)" + "meta" + "\(meta_Str)" + "name" + "\(LiveAppName)" + "size" + "20" + "timestamp" + "\(SingletonClass.sharedInstance.timestamp ?? 0)" + "\(cloud_secret)"
            let data1 = self.ccSha256(data: (rawdata.data(using: .utf8)!))
            let parameter = ["active":"1","appKey":cloud_key,"meta":"\(meta_Str)","name":"\(LiveAppName)","size":"20","timestamp":SingletonClass.sharedInstance.timestamp ?? 0,"signature":data1.map { String(format: "%02hhx", $0) }.joined()] as [String : Any] //Optional
            Alamofire.request(upload_modify,method: .put, parameters:parameter, encoding: JSONEncoding.default, headers: nil).responseJSON
                { response in
                    switch(response.result)
                    {
                    case .success(_):
                        if response.result.value != nil
                        {
                            isComeFrom = "PopUpARElementAlreadyAttachedVC"
                            let dict = response.result.value! as! [String:Any]
                            print("dict\(dict)")
                            let statusCode = dict["statusCode"] as? Int
                            if(statusCode == 0){
                                self.delegate?.printAfterResponseCompletion(true)
                     //           is_Ar_Video_Available = true
                                print("Data modified successfully.")
                            }else{
                                self.delegate?.printAfterResponseCompletion(false)
                                print("Data not modified")
                            }
                        }else{
                            self.showErrorPopup()
                        }

                        break

                    case .failure(let error):
                        var error = error.localizedDescription
                         self.showErrorPopup()
                        if error.contains("JSON could not") {
                            error = "Could not connect to server at the moment."
                        }
                    }
            }
        }else {
            self.showNoInternetPopup()
            return
        }
        }
    }
    func showErrorPopup()
    {
        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpARServerErrorViewController") as! PopUpARServerErrorViewController
        alertPopUp.view.frame = self.view.bounds
        self.addChildViewController(alertPopUp)
        self.view.addSubview(alertPopUp.view)
        alertPopUp.didMove(toParentViewController: self)
        customLoader.hideActivityIndicator()
//        //        Enable print button on error popup
        NotificationCenter.default.post(name: Notification.Name("enablePrintBtnOnPopupCross"), object: nil)
    }
  
    func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
            options = JSONSerialization.WritingOptions.prettyPrinted
        }

        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: options)
            if let string = String(data: data, encoding: String.Encoding.utf8) {
                return string
            }
        } catch {
            print(error)
        }

        return ""
    }
    func ccSha256(data: Data) -> Data {
        var digest = Data(count: Int(CC_SHA256_DIGEST_LENGTH))

        _ = digest.withUnsafeMutableBytes { (digestBytes) in
            data.withUnsafeBytes { (stringBytes) in
                CC_SHA256(stringBytes, CC_LONG(data.count), digestBytes)
            }
        }
        return digest
    }
    func showNoInternetPopup()
    {
        customLoader.hideActivityIndicator()
        //        Enable print button on error popup
        NotificationCenter.default.post(name: Notification.Name("enablePrintBtnOnPopupCross"), object: nil)
        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
        alertPopUp.view.frame = self.view.bounds
         comeFromStrNoInternetSocialMedia = "NormalNointernet"
        isComeFrom = "Rectangle"
        self.addChildViewController(alertPopUp)
        self.view.addSubview(alertPopUp.view)
        alertPopUp.didMove(toParentViewController: self)
    }
//    func uploadPhotoAndVideoToServer(image : UIImage, videoData : Data)  {
//        customLoader.showActivityIndicator(viewController: self.parent!.view)
//        if WebserviceModelClass().isInternetAvailable() {
//            let uploadURL = LIVE_AR_URL_FFMPEG+UPLOAD_SUB_URL
//            var imageTosever = UIImage()
//           self.userID = UIDevice.current.identifierForVendor?.uuidString ?? ""
//            if video_Is_Landscape == "1"{
//                imageTosever = image.rotateImageByDegrees(270.0)
//            }else{
//                imageTosever = image
//            }
//            print("imageTosever\(imageTosever)")
//            let imgData = UIImageJPEGRepresentation(imageTosever, 0.1)!
////             DispatchQueue.global(qos: .background).async {
//            let image_id = UserDefaults.standard.value(forKey:"image_id") as? String ?? ""
//            if image_id == ""{
//                SingletonClass.sharedInstance.parameters_id = ["user_id": self.userID,"Video_Is_Landscape": video_Is_Landscape] //Optional for extra parameter unique_user_id
//            }else{
//                SingletonClass.sharedInstance.parameters_id = ["user_id": self.userID,"Video_Is_Landscape": video_Is_Landscape,"_id":image_id] //Optional for extra parameter unique_user_id
//            }
//            let parameters_Data = SingletonClass.sharedInstance.parameters_id
//            Alamofire.upload(multipartFormData: { multipartFormData in
//                // Upload image
//                multipartFormData.append(imgData, withName: "photo",fileName: "imageFile.jpg", mimeType: "image/jpg")
//                // Upload video
//                multipartFormData.append(videoData, withName: "video", fileName: "videoFile.mp4", mimeType: "video/mp4")
//                for (key, value) in parameters_Data! {
//                    multipartFormData.append((value.data(using: String.Encoding.utf8)!), withName: key)
//                }
//                //Optional for extra parameters
//            },
//
//                             to:uploadURL)
//            { (result) in
//                switch result {
//                case .success(let upload, _, _):
//                    upload.responseJSON { response in
//                        if response.result.value != nil
//                        {
//                            let dict = response.result.value! as! NSDictionary
//                            let responseDict = dict as! [String:Any]
//                            let status = responseDict["status"] as! Int
//                            let meta = dict["meta_data"] as? [String:Any]
//                            UserDefaults.standard.set(meta, forKey: "meta_To_modifyImg")
//                            let deadlineTime = DispatchTime.now() + .milliseconds(1) // .seconds(1)
//                            DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
////                                Call modify AR api
//                                self.modify_EasyAR()
//                            })
//                            if status == 200 {
////                                customLoader.hideActivityIndicator()
////                                let printHandler = PrintImageHandler()
//                            }else{
//                                self.showErrorPopup()
//                                print("Upload fail")
//                            }
//                        }else{
//                            self.showErrorPopup()
//                        }
//                    }
//
//                case .failure( _):
//                    self.showErrorPopup()
//                }
//            }
//        }else {
//            self.showNoInternetPopup()
//            return
//        }
//    }
    func uploadImageData(){
        ischeckupdate_ARPopup = false
        let image = UserDefaults.standard.value(forKey:"imgData_ToMo")
        let image1 = UIImage(data: image as! Data)
        //            Function to modify video on target image
//        self.uploadPhotoAndVideoToServer(image:image1! , videoData: UserDefaults.standard.value(forKey:"AR_Video")! as! Data)
        
        //            Upload new image and video on OK click of Update popup instead of mofifying target to remove issue of - AR vertical videos change to horizontal. Vertical videos need to remain vertical.(landscape image-->portrait video  - Strech found ,landscape image-->landscape video - Strech found)
        NotificationCenter.default.post(name: Notification.Name("updateNewTargetAfterdeletingOld"), object: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    //MARK: - IBActions -
    @IBAction func crossBtnClicked(_ sender: Any) {
         NotificationCenter.default.post(name: Notification.Name("enablePrintBtnOnPopupCross"), object: nil)
       // is_Ar_Video_Available = true
        ischeckupdate_ARPopup = true
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    @IBAction func selectAnotherPhotoBtnClicked(_ sender: Any) {
        if lblARElementDesc.text?.count != 0{
        if lblARElementDesc.text == "Do you want to update AR video?"{
//
            isComeFrom = "PopUpARElementAlreadyAttachedVC"
            uploadImageData()
        }else{
            let homeVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeVC, animated: true)

                self.view.removeFromSuperview()
                self.removeFromParentViewController()
        }
        }
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

}
