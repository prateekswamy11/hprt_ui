//
//  PhotoAccessRequiredViewController.swift
//  Polaroid MINT
//
//  Created by MAXIMESS183 on 28/02/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import Photos

class PopUpPhotoAccessRequiredViewController: UIViewController {
    //MARK: - Outlets -
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText1: UILabel!
    @IBOutlet weak var lblText2: UILabel!
    @IBOutlet weak var btnEnablePhotoAccess: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTitle.text = "Photos Access Required".localisedString()
        self.lblText1.text = "We would love to show you your photos so you can make cool prints!".localisedString()
        
        self.lblText2.text = "Please allow access to your photo gallery to enjoy KODAK SMILE.".localisedString()
        self.btnEnablePhotoAccess.setTitle("Enable photos access".localisedString(), for: .normal)
        // Do any additional setup after loading the view.
      
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK: - Actions -
    
    @IBAction func btnEnablePhotoAccessClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
        
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
        PHPhotoLibrary.requestAuthorization({status in
            if status == .authorized{
                
            }
            else {
                UserDefaults.standard.set(true, forKey: "EnablePhotoAccess")
            }
            })
        }
    }
    
    @IBAction func btnCancel(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }

    //MARK: - Functions -
}
