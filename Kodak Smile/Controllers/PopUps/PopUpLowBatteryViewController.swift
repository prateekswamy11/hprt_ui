//
//  PopUpLowBatteryViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 04/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PopUpLowBatteryViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnOk: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
        
        lblTitle.text = "Low Battery".localisedString()
        lblText.text = "Your printer's battery is low.\nPlease recharge your device.".localisedString()
        btnOk.setTitle("OK".localisedString(), for: .normal)
    }
    

    @IBAction func btnOkClicked(_ sender: Any)
    {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    @IBAction func btnCrossClicked(_ sender: Any)
    {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
