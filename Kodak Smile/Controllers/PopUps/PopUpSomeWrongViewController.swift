//
//  PopUpSomeWrongViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 04/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PopUpSomeWrongViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCross: UIButton!
    var strTitle = "Oops!\nSomething went wrong"
    var strDescription = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblTitle.text = strTitle.localisedString()
        lblText.text = strDescription.localisedString()
        btnOk.setTitle("OK".localisedString(), for: .normal)
    }
    
    // MARK: - Function
    func showNoInternetPopup()
    {
        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
        alertPopUp.view.frame = self.view.bounds
        self.parent?.addChildViewController(alertPopUp)
        isComeFrom = "Rectangle"
         comeFromStrNoInternetSocialMedia = "NormalNointernet"
        self.parent?.view.addSubview(alertPopUp.view)
        alertPopUp.didMove(toParentViewController: self.parent!)
    }
    // MARK: - IBAction
    @IBAction func btnDismissClicked(_ sender: Any)
    {
        //        Check target id and delete target
        if UserDefaults.standard.value(forKey:"targetId") != nil && finalCopiesToCheckdelete == 0{
            //                    Delete target from EasyAR server
            if WebserviceModelClass().isInternetAvailable() {
                SingletonClass.sharedInstance.healthCheck_EasyARForDeleteTarget()
            }else {
                customLoader.hideActivityIndicator()
//                self.showNoInternetPopup()
            }
        }
//        if (is_Ar_Video_Available){
//            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpRetryPrintViewController") as! PopUpRetryPrintViewController
//            alertPopUp.view.frame = self.view.bounds
//            self.parent?.addChildViewController(alertPopUp)
//            self.parent?.view.addSubview(alertPopUp.view)
//            alertPopUp.didMove(toParentViewController: self)
//        }
        //            self.parent?.navigationController?.popViewController(animated: true)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
