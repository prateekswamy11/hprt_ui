//
//  PopUpShowPrinterlistViewController.swift
//  Kodak Smile
//
//  Created by maximess206 on 08/06/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import UIKit

class PopUpShowPrinterlistViewController: UIViewController {

    @IBOutlet weak var tblViewPrinterList: UITableView!
    
    @IBOutlet weak var btnCrossClicked: UIButton!
    var printerNameArray = ["smile","classic"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblViewPrinterList.delegate = self
        self.tblViewPrinterList.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnCrossClicked(_ sender: Any) {
        self.parent?.navigationController?.popViewController(animated: false)
               self.view.removeFromSuperview()
               self.removeFromParentViewController()
    }
    
}
extension PopUpShowPrinterlistViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.printerNameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewPrinterList.dequeueReusableCell(withIdentifier: "PrinterListTableViewCell") as! PrinterListTableViewCell
        cell.lblPrinterName.text = self.printerNameArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            print("1")
        } else {
            print("2")
        }
    }
    
}
