//
//  PopUpNoInternetViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 04/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

var comeFromStrNoInternetSocialMedia = String()

class PopUpNoInternetViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnOk: UIButton!


    var strTitle = ""
    var strSubTitle = ""
   var iscameFromScanNow = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblTitle.text = "No Internet Connection".localisedString()
       // lblText.text = "Connect to the internet to access\nyour Instagram pictures."
        btnOk.setTitle("OK".localisedString(), for: .normal)
        self.sourceOfPopUp()
    }
    
    func initialiseViewLabels()
    {
        self.lblTitle.text = strTitle
        self.lblText.text = strSubTitle
    }
    
    func sourceOfPopUp()
    {
        if comeFromStrNoInternetSocialMedia == "instagramNoInternet"
        {
            lblText.text = "Connect to the internet to access your Instagram pictures.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia == "facebookNoInternet"
        {
            lblText.text = "Connect to the internet to access your Facebook pictures.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia == "dropboxNoInternet"
        {
            lblText.text = "Connect to the internet to access your Dropbox pictures.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia == "googleNoInternet"
        {
            lblText.text = "Connect to the internet to access your Google pictures.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia == "NoInternet"
        {
            lblText.text = "Connect to the internet to print your picture.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia == "loginNoInternet"
        {
            lblText.text = "Connect to the internet for Login.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia == "signUpNoInternet"
        {
            lblText.text = "Connect to the internet for Sign Up.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia == "forgetNoInternet"
        {
            lblText.text = "Connect to the internet for Retrieving Password.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia == "googleLoginNoInternet"
        {
            lblText.text = "Connect to the internet for Login with Google.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia == "facebookLoginNoInternet"
        {
            lblText.text = "Connect to the internet for Login with Facebook.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia == "buyPaperNoInternet"
        {
            lblText.text = "Connect to the internet to Buy Zink Paper.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia == "legalNoInternet"
        {
            lblText.text = "Connect to the internet to view Privacy Policy.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia == "NormalNointernet"
        {
            lblText.text = "Connect to the internet.".localisedString()
        }
        else if comeFromStrNoInternetSocialMedia.isEmpty
        {
            lblText.text = "Connect to the internet.".localisedString()
        }
        
    }
    
    @IBAction func btnOkClicked(_ sender: Any)
    {
        if isComeFrom == "Rectangle"{
//            self.parent?.navigationController?.popViewController(animated: true)
            self.view.removeFromSuperview()
            self.removeFromParentViewController()

        }
//        else if iscameFromScanNow{
//            self.dismiss(animated: true, completion: nil)
//            NotificationCenter.default.post(name: NSNotification.Name("NoInternetOkBtnClicked"), object: nil)
//        }
        else{
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
