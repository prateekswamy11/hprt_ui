//
//  PopUpCommunicationHiccupViewController.swift
//  Kodak Smile
//
//  Created by MAXIMESS183 on 07/05/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

class PopUpCommunicationHiccupViewController: UIViewController{
    
    

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCross: UIButton!
    var strTitle = "Communication Hiccup"
    var strDescription = "There was a problem sending the picture to the printer. Please retry."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        lblTitle.text = strTitle.localisedString()
        lblText.text = strDescription.localisedString()
        btnOk.setTitle("OK".localisedString(), for: .normal)
    }
      // MARK: - Function
    func showNoInternetPopup()
    {
       
        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
        alertPopUp.view.frame = self.view.bounds
        self.parent?.addChildViewController(alertPopUp)
        isComeFrom = "Rectangle"
        comeFromStrNoInternetSocialMedia = "NormalNointernet"
        self.parent?.view.addSubview(alertPopUp.view)
        alertPopUp.didMove(toParentViewController: self.parent!)
    }
      // MARK: - IBAction
    @IBAction func btnOkAndCrossClicked(_ sender: Any)
    {
        
        if UserDefaults.standard.value(forKey:"targetId") != nil && finalCopiesToCheckdelete == 0{
            //                    Delete target from EasyAR server
            if WebserviceModelClass().isInternetAvailable() {
                SingletonClass.sharedInstance.healthCheck_EasyARForDeleteTarget()
            }else {
                customLoader.hideActivityIndicator()
//                self.showNoInternetPopup()
            }
        }
//        if (is_Ar_Video_Available){
//            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpRetryPrintViewController") as! PopUpRetryPrintViewController
//            alertPopUp.view.frame = self.view.bounds
//            self.parent?.addChildViewController(alertPopUp)
//            self.parent?.view.addSubview(alertPopUp.view)
//            alertPopUp.didMove(toParentViewController: self)
//        }
        //            self.parent?.navigationController?.popViewController(animated: true)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
  
}
