//
//  PopUpPaperJamInstructionViewController.swift
//  Polaroid MINT
//
//  Created by MacMini001 on 05/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PopUpPaperJamInstructionViewController: UIViewController {

  
    @IBOutlet weak var constraintToOkView: NSLayoutConstraint!
    @IBOutlet weak var constraintTopLabel: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var lblText1: UILabel!
    @IBOutlet weak var lblText2: UILabel!
    @IBOutlet weak var lblText3: UILabel!
    
    @IBOutlet weak var btnOk: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.changeConstraintsForIphone5()
        // Do any additional setup after loading the view.
        lblTitle.text = "Oops!\nThere's a paper jam".localisedString()
        lblText.text = "To fix the paper jam follow these steps:".localisedString()
        lblText1.text = "1. Turn the printer off".localisedString()
        lblText2.text = "2. Make sure the zink paper is inserted correctly".localisedString()
        lblText3.text = "3. Turn the printer back on".localisedString()
        btnOk.setTitle("OK".localisedString(), for: .normal)
        
    }
    // MARK: - Function
    func showNoInternetPopup()
    {
        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
        alertPopUp.view.frame = self.view.bounds
        self.parent?.addChildViewController(alertPopUp)
        isComeFrom = "Rectangle"
        comeFromStrNoInternetSocialMedia = "NormalNointernet"
        self.parent?.view.addSubview(alertPopUp.view)
        alertPopUp.didMove(toParentViewController: self.parent!)
    }
// MARK: - IBAction
    @IBAction func popUpDismissBtnClicked(_ sender: UIButton) {
        print("finalCopiesToCheckdelete\(finalCopiesToCheckdelete)")
        if UserDefaults.standard.value(forKey:"targetId") != nil && finalCopiesToCheckdelete == 0{
            //                    Delete target from EasyAR server
            if WebserviceModelClass().isInternetAvailable() {
                SingletonClass.sharedInstance.healthCheck_EasyARForDeleteTarget()
            }else {
                customLoader.hideActivityIndicator()
                //                self.showNoInternetPopup()
            }
        }
//        if (is_Ar_Video_Available){
//            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpRetryPrintViewController") as! PopUpRetryPrintViewController
//            alertPopUp.view.frame = self.view.bounds
//            self.parent?.addChildViewController(alertPopUp)
//            self.parent?.view.addSubview(alertPopUp.view)
//            alertPopUp.didMove(toParentViewController: self)
//        }
        //            self.parent?.navigationController?.popViewController(animated: true)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
   
    //MARK: - Functions -
    func changeConstraintsForIphone5()
    {
        if getCurrentIphone() == "5"
        {
            self.constraintToOkView.constant = 15.0
            self.constraintTopLabel.constant = 0.0
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

