//
//  CameraAccessRequiredViewController.swift
//  Polaroid MINT
//
//  Created by MAXIMESS183 on 28/02/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import AVFoundation

protocol CameraPermissinNativePopupDelegate{
    func showCameraPermissionPopup()
}

class PopUpCameraAccessRequiredViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText1: UILabel!
    @IBOutlet weak var lblText2: UILabel!
    @IBOutlet weak var btnEnableCameraAccess: UIButton!
   
    var delegate: CameraPermissinNativePopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblTitle.text = "Camera Access Required".localisedString()
        lblText1.text = "Want to use your camera to create SMILE moments?".localisedString()
        lblText2.text = "Please allow KODAK SMILE to access your camera.".localisedString()
        self.btnEnableCameraAccess.setTitle("Enable camera access".localisedString(), for: .normal)
    }
    
    @IBAction func btnEnableCameraAccessClicked(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
        self.delegate?.showCameraPermissionPopup()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
