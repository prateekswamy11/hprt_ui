//
//  PopUpARServerErrorViewController.swift
//  Kodak Smile
//
//  Created by MAXIMESS194 on 8/7/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit

class PopUpARServerErrorViewController: UIViewController {

    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var btnCross: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - IBActions -
    @IBAction func crossBtnClicked(_ sender: Any) {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    @IBAction func selectAnotherPhotoBtnClicked(_ sender: Any) {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
