//
//  PopUpHeldOnViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 04/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PopUpHeldOnViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnOk: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = "Hold on...".localisedString()
        lblText.text = "Your printer is busy printing your picture. Please wait for the printing to end before sending a new picture.".localisedString()
        btnOk.setTitle("OK".localisedString(), for: .normal)
       
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnOkClicked(_ sender: Any)
    {
        IS_HOLD_ON_POP_SHOWN = true
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
