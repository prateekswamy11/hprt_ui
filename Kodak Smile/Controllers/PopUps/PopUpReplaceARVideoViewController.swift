//
//  PopUpReplaceARVideoViewController.swift
//  Kodak Classic
//
//  Created by maximess194 on 27/01/20.
//  Copyright © 2020 maximess. All rights reserved.
//

import UIKit
import Photos

class PopUpReplaceARVideoViewController: UIViewController {
    
    //MARK: - Variables -
    
    //MARK: - IBOutlets -
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var BtnReplaceVideo: UIButton!
    @IBOutlet weak var BtnCross: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = "Replace AR Video".localisedString()
        lblText.text = "Do you want to change AR video?".localisedString()
        BtnReplaceVideo.setTitle("Replace Video".localisedString(), for: .normal)
    }
    
    //MARK: - IBActions -
    @IBAction func replaceVideoBtnClick(_ sender: Any) {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
         NotificationCenter.default.post(name: NSNotification.Name("ReplaceVideo"), object: nil)
    }
    
    @IBAction func crossBtnClick(_ sender: Any) {
        self.parent?.navigationController?.popViewController(animated: false)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
      // MARK: - Function
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PopUpReplaceARVideoViewController
{
    func getVideoAssetsAlbumWiseInReplaveVideo() -> PHFetchResult<PHAsset>?
       {
           var fetchResult: PHFetchResult<PHAsset>?
           
           fetchResult = PHAsset.fetchAssets(with: .video, options: nil)
           return fetchResult
       }
}
