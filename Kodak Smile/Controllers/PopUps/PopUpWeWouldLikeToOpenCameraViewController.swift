//
//  WeWouldLikeToOpenCameraViewController.swift
//  Polaroid MINT
//
//  Created by MAXIMESS183 on 28/02/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import AVFoundation

class PopUpWeWouldLikeToOpenCameraViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnEnbleCameraAccess: UIButton!
    
    var isCameFromScanOverlay = false

    var delegate: CameraPermissinNativePopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblTitle.text = "We would love to open the camera".localisedString()
        lblText.text = "First, please allow KODAK SMILE to access your camera.".localisedString()
        self.btnEnbleCameraAccess.setTitle("Enable camera access".localisedString(), for: .normal)
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnEnbleCameraAccessClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        if UserDefaults.standard.bool(forKey: "isWeWouldLikeToCameraFirstTimePopped") {
            DispatchQueue.main.async {
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
                UIApplication.shared.open(settingsUrl)
            }
        }else{
            self.delegate?.showCameraPermissionPopup()
        }
    }
}
