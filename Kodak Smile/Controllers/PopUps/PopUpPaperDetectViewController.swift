//
//  PopUpPaperDetectViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 04/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PopUpPaperDetectViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnOk: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblTitle.text = "Oops!\nWrong paper detected".localisedString()
        lblText.text = "Your printer only works with KODAK SMILE ZINK Photo Paper. Please reset your printer and insert KODAK SMILE ZINK Photo Paper to print your picture.".localisedString()
        btnOk.setTitle("OK".localisedString(), for: .normal)
        
    }
    // MARK: - Function
    func showNoInternetPopup()
    {
        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
        alertPopUp.view.frame = self.view.bounds
        self.parent?.addChildViewController(alertPopUp)
        isComeFrom = "Rectangle"
         comeFromStrNoInternetSocialMedia = "NormalNointernet"
        self.parent?.view.addSubview(alertPopUp.view)
        alertPopUp.didMove(toParentViewController: self.parent!)
    }
    @IBAction func btnOkClicked(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey:"targetId") != nil && finalCopiesToCheckdelete == 0{
            //                    Delete target from EasyAR server
            if WebserviceModelClass().isInternetAvailable() {
                SingletonClass.sharedInstance.healthCheck_EasyARForDeleteTarget()
            }else {
                customLoader.hideActivityIndicator()
                self.showNoInternetPopup()
            }
        }
//        if (is_Ar_Video_Available){
//            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpRetryPrintViewController") as! PopUpRetryPrintViewController
//            alertPopUp.view.frame = self.view.bounds
//            self.parent?.addChildViewController(alertPopUp)
//            self.parent?.view.addSubview(alertPopUp.view)
//            alertPopUp.didMove(toParentViewController: self)
//        }
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    @IBAction func btnCrossClicked(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey:"targetId") != nil && finalCopiesToCheckdelete == 0{
            //                    Delete target from EasyAR server
            if WebserviceModelClass().isInternetAvailable() {
                SingletonClass.sharedInstance.healthCheck_EasyARForDeleteTarget()
            }else {
                customLoader.hideActivityIndicator()
//                self.showNoInternetPopup()
            }
        }
//        if (is_Ar_Video_Available){
//            let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpRetryPrintViewController") as! PopUpRetryPrintViewController
//            alertPopUp.view.frame = self.view.bounds
//            self.parent?.addChildViewController(alertPopUp)
//            self.parent?.view.addSubview(alertPopUp.view)
//            alertPopUp.didMove(toParentViewController: self)
//        }
        //            self.parent?.navigationController?.popViewController(animated: true)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
