//
//  PopupOutOfPaperViewController.swift
//  Polaroid MINT
//
//  Created by maximess142 on 04/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PopupOutOfPaperViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblTitle.text = "Oops!\nYou are out of paper".localisedString()
        lblText.text = "Your printer only works with KODAK SMILE ZINK Photo Paper. Load KODAK SMILE ZINK Photo Paper to print your picture.".localisedString()
        btnOk.setTitle("OK".localisedString(), for: .normal)
        
    }
    

    @IBAction func btnOkClicked(_ sender: Any)
    {
        NotificationCenter.default.post(name: NSNotification.Name("ShowBuyPaperButton"), object: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        IS_OUT_OF_PAPER_POP_SHOWN = true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
