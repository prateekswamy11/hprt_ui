//
//  PopUpBTRequestViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 04/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import CoreBluetooth

class PopUpBTRequestViewController: UIViewController{
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnGotIt: UIButton!

//    var bluetoothManager: CBCentralManager!
//    var isBluetoothOn: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
     
        // Do any additional setup after loading the view.
        lblTitle.text = "Bluetooth Required".localisedString()
        lblText.text = "Your phone uses BLUETOOTH to connect to your printer. Head over to your control center or settings and turn on your BLUETOOTH.".localisedString()
        btnGotIt.setTitle("Got it".localisedString(), for: .normal)
    
        
    }

//
//    func centralManagerDidUpdateState(_ central: CBCentralManager) {
//        switch central.state {
//        case .poweredOn:
//            self.isBluetoothOn = true
//            NotificationCenter.default.post(name: Notification.Name("checkBluetooth"), object: nil)
//            break
//        case .poweredOff:
//            self.isBluetoothOn = false
//            break
//        case .resetting:
//            break
//        case .unauthorized:
//            break
//        case .unsupported:
//            break
//        case .unknown:
//            break
//        default:
//            break
//        }
//    }
    

    @IBAction func popUpDismissBtnClicked(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goToBluetoothSettingsBtnClicked(_ sender: UIButton) {
//            self.bluetoothManager = CBCentralManager()
//            self.bluetoothManager.delegate = self
            self.dismiss(animated: true, completion: nil)
        /*
        let settingsUrl = NSURL(string: "App-Prefs:root=Bluetooth")  
        if let url = settingsUrl {
            UIApplication.shared.openURL(url as URL)
        }
        */
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
