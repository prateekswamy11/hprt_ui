//
//  PrintErrorProtocol.swift
//  Polaroid MINT
//
//  Created by maximess142 on 04/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import Foundation

protocol PrintErrorProtocol //: class
{
    func receivedPrintingError(errorCode : Int)
}
