//
//  PopUpMintCoolViewController.swift
//  Polaroid MINT
//
//  Created by MacMini003 on 04/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class PopUpMintCoolViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnOk: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        lblTitle.text = "Printer is cooling down".localisedString()
//        lblText.text = "Your picture will be printed as soon as the printer is cooled down.".localisedString()
        btnOk.setTitle("OK".localisedString(), for: .normal)
    }
    
    @IBAction func btnOkClicked(_ sender: Any)
    {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
