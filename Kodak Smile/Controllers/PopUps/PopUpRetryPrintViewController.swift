//
//  PopUpRetryPrintViewController.swift
//  Kodak Smile
//
//  Created by MAXIMESS194 on 8/6/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit


class PopUpRetryPrintViewController: UIViewController {

   
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var btnCross: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    // MARK: - Function
    func showNoInternetPopup()
    {
        
        let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
        alertPopUp.view.frame = self.view.bounds
        self.parent?.addChildViewController(alertPopUp)
        isComeFrom = "Rectangle"
        comeFromStrNoInternetSocialMedia = "NormalNointernet"
        self.parent?.view.addSubview(alertPopUp.view)
        alertPopUp.didMove(toParentViewController: self.parent!)
    }
    //MARK: - IBActions -
    @IBAction func crossBtnClicked(_ sender: Any) {
        isComeFrom = "PopUpRetryPrintVC"
        var isHomeVCInStack = false
        if let navVC = self.navigationController
        {
            // Pop view controller to home.
            for item in navVC.viewControllers {
                // Filter for your desired view controller:
                if item.isKind(of: HomeViewController.self) {
                    isHomeVCInStack = true
                    self.navigationController?.popToViewController(item, animated: false)
                }
            }
        }
        
        // When directly came from onboarding, home VC is not in stack so navigate by pushing to it
        if isHomeVCInStack == false
        {
            let homeVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeVC, animated: false)

        }
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        customLoader.hideActivityIndicator()
        NotificationCenter.default.post(name: Notification.Name("saveImageOnRetryPrintWithoutLogo"), object: nil)
    }
    @IBAction func OKBtnClicked(_ sender: Any) {
//        self.view.removeFromSuperview()
//        self.removeFromParentViewController()
        if !WebserviceModelClass().isInternetAvailable() {
            customLoader.hideActivityIndicator()
            self.showNoInternetPopup()
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        }
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

