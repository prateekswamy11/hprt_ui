//
//  ReplaceImageViewController.swift
//  KODAK MINI
//
//  Created by MacMini002 on 4/24/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import Photos

class ReplaceImageViewController: UIViewController {
    
    @IBOutlet weak var vwLayout: UIView!
    @IBOutlet weak var collectionViewGrid: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var imagesFrom = String()
    var imagesArray: PHFetchResult<PHAsset>!
    var collectionItem = PHAssetCollection()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- --------------------- @IBActions -----------------------------
    
    @IBAction func backBtnClicked(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
