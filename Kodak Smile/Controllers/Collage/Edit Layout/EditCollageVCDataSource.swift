//
//  EditCollageVCDataSource.swift
//  Polaroid MINT
//
//  Created by MAXIMESS152 on 05/06/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation
import Photos
import Alamofire
import AlamofireImage

extension EditCollageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.cameFrom == "Local"{
            return self.fetchResult.count
        }else{
            return self.socialImagesURL.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryGridCollectionViewCell", for: indexPath) as! GalleryGridCollectionViewCell
        var imageURL = ""
        if self.cameFrom == "Local"{
            let asset = getSelectedImageAsset(index: indexPath.item)
            cell.representedAssetIdentifier = asset.localIdentifier
            imageManager.requestImage(for: asset, targetSize: CGSize(width:100, height: 100), contentMode: .aspectFill, options: cellImgoptions, resultHandler: { image, _ in
                // The cell may have been recycled by the time this handler gets called;
                // set the cell's thumbnail image only if it's still showing the same asset.
                if cell.representedAssetIdentifier == asset.localIdentifier && image != nil {
                    cell.imageViewThumbnail.image = image
                    imageURL = asset.localIdentifier
                }
            })
        }else{
            let url = self.socialImagesURL[indexPath.row]
            imageURL = url
            cell.imageViewThumbnail.af_setImage(
                withURL: URL(string:url)!,
                imageTransition: .crossDissolve(0.2),
                completion: nil)
        }
        if selected_Image_Asset_Id_Array.indices.contains(selected_Collage_Index){
            var imageId = ""
            if self.cameFrom == "Local"{
                imageId = getSelectedImageAsset(index: indexPath.item).localIdentifier
            }else{
                imageId = self.socialImagesURL[indexPath.item]
            }
            if selected_Image_Asset_Id_Array[selected_Collage_Index] == imageId
            {
                cell.showSelectedCell()
                selectedImageIndex = indexPath.item
            }
            else
            {
                cell.showNormalCell()
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryGridCollectionViewCell", for: indexPath) as! GalleryGridCollectionViewCell
        var imageURL = ""
//        if selectedImageIndex != indexPath.item{
            if self.cameFrom == "Local"{
                let asset = getSelectedImageAssetBySelectedOrder(index: indexPath.item)
                // let assetIdentifier = asset.localIdentifier
                self.getSelectedCollageLayout(image: self.getImageFromAsset(asset: asset))
                // let asset = getSelectedImageAsset(index: indexPath.item)
                // cell.representedAssetIdentifier = asset.localIdentifier
                // imageManager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: cellImgoptions, resultHandler: { image, _ in
                // // The cell may have been recycled by the time this handler gets called;
                // // set the cell's thumbnail image only if it's still showing the same asset.
                // if cell.representedAssetIdentifier == asset.localIdentifier && image != nil {
                // cell.imageViewThumbnail.image = image
                // imageURL = asset.localIdentifier
                //
                // self.getSelectedCollageLayout(image: image!)
                // }
                // })
            }else {
                let url = self.socialImagesURL[indexPath.row]
                imageURL = url
                // cell.imageViewThumbnail.af_setImage(
                // withURL: URL(string:url)!,
                // imageTransition: .crossDissolve(0.2),
                // completion: nil)
                Alamofire.request(imageURL).responseImage { response in
                    if let image = response.result.value {
                        self.getSelectedCollageLayout(image: image)
                    }
                }
            }
            var imageId = ""
            if self.cameFrom == "Local"{
                imageId = getSelectedImageAsset(index: indexPath.item).localIdentifier
                selected_Image_Asset_Id_Array.remove(at: selected_Collage_Index)
                selected_Image_Asset_Id_Array.insert(imageId, at: selected_Collage_Index)
            }else{
                imageId = self.socialImagesURL[indexPath.item]
                selected_Image_Asset_Id_Array.remove(at: selected_Collage_Index)
                selected_Image_Asset_Id_Array.insert(imageId, at: selected_Collage_Index)
            }
            if selected_Image_Asset_Id_Array.indices.contains(selected_Collage_Index){
                if selected_Image_Asset_Id_Array[selected_Collage_Index] == imageId
                {
                    cell.showSelectedCell()
                    selectedImageIndex = indexPath.item
                }
                else
                {
                    cell.showNormalCell()
                }
            }
            collectionView.reloadData()
//        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var marginsAndInsets = CGFloat()
        if #available(iOS 11.0, *) {
            marginsAndInsets = inset * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        } else {
            // Fallback on earlier versions
            marginsAndInsets = inset * 2 + 0 + 0 + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        }
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    func getSelectedImageAsset(index : Int) -> PHAsset {
        var asset = PHAsset()
        asset = fetchResult.object(at: fetchResult.count - (index + 1))
        return asset
    }
    
    func getSelectedCollageLayout(image: UIImage){
        switch self.layoutIndex{
        case 0:
            self.setImageInTwoCollageLayout(image: image)
            break
        case 1:
            self.setImageInTwoCollageLayout(image: image)
            break
        case 2:
            self.setImageInThreeCollageLayout(image: image)
            break
        case 3:
            self.setImageInFourCollageLayout(image: image)
            break
        default:
            print("error")
            break
        }
    }
    
    func setImageInTwoCollageLayout(image: UIImage){
        switch selected_Collage_Index{
        case 0:
            image_Scroll_Views_Array[selected_Collage_Index].zoomMode = .fill
            image_Scroll_Views_Array[selected_Collage_Index].image = image
            break
        case 1:
            image_Scroll_Views_Array[selected_Collage_Index].zoomMode = .fill
            image_Scroll_Views_Array[selected_Collage_Index].image = image
            break
        default: break
        }
    }
    
    func setImageInThreeCollageLayout(image: UIImage){
        switch selected_Collage_Index{
        case 0:
            image_Scroll_Views_Array[selected_Collage_Index].zoomMode = .fill
            image_Scroll_Views_Array[selected_Collage_Index].image = image
            break
        case 1:
            image_Scroll_Views_Array[selected_Collage_Index].zoomMode = .fill
            image_Scroll_Views_Array[selected_Collage_Index].image = image
            break
        case 2:
            image_Scroll_Views_Array[selected_Collage_Index].zoomMode = .fill
            image_Scroll_Views_Array[selected_Collage_Index].image = image
            break
        default: break
        }
    }
    
    func setImageInFourCollageLayout(image: UIImage){
        switch selected_Collage_Index{
        case 0:
            image_Scroll_Views_Array[selected_Collage_Index].zoomMode = .fill
            image_Scroll_Views_Array[selected_Collage_Index].image = image
            break
        case 1:
            image_Scroll_Views_Array[selected_Collage_Index].zoomMode = .fill
            image_Scroll_Views_Array[selected_Collage_Index].image = image
            break
        case 2:
            image_Scroll_Views_Array[selected_Collage_Index].zoomMode = .fill
            image_Scroll_Views_Array[selected_Collage_Index].image = image
            break
        case 3:
            image_Scroll_Views_Array[selected_Collage_Index].zoomMode = .fill
            image_Scroll_Views_Array[selected_Collage_Index].image = image
            break
        default: break
        }
    }
    
    
    func getSelectedImageAssetBySelectedOrder(index : Int) -> PHAsset {
        //this condition for oldest and newest first function
        var asset = PHAsset()
        if isSortByOldestFirst {
            asset = imagesArray.object(at: index)
        }
        else{
            
            asset = imagesArray.object(at: imagesArray.count - (index + 1))
        }
        
        return asset
    }
    
    func getImageFromAsset(asset : PHAsset) -> UIImage
    {
        //let imageSize = CGSize(width: asset.pixelWidth,
        // height: asset.pixelHeight)
        
        let options = PHImageRequestOptions()
        
        //Photos automatically provides one or more results in order to balance image quality and responsiveness.
        options.deliveryMode = .opportunistic
        
        //Request the original, highest-fidelity version of the image asset.
        //options.version = .original
        
        //Set it to true to block the calling thread until either the requested image is ready or an error occurs, at which time Photos calls your result handler.
        options.isSynchronous = true
        
        // Allow to download image from iCloud
        options.isNetworkAccessAllowed = true
        
        var imgToSend = UIImage()
        let imagemhng = PHImageManager()
        
        var imgSize = 1000.0
        var loopCount = 5
        
        imagemhng.requestImage(for: asset,
                               targetSize: PHImageManagerMaximumSize,
                               contentMode: PHImageContentMode.aspectFit,
                               options: options,
                               resultHandler:
            { (image, info ) -> Void in
                
                
                //Solve crashlytics crash
                
                guard let dict : Dictionary = info else{
                    if let receivedImg = image
                    {
                        /* The image is now available to us */
                            imgToSend = receivedImg
                    }
                    else{
                        let dialog = UIAlertController(title: "Cloud Error".localisedString(), message: "This photo could not be downloaded from iCloud. Please connect to internet.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                        
                        dialog.addAction(okAction)
                        self.present(dialog, animated:true, completion:nil)
                    }
                    return
                }
                
                
                if let isIcloudIMg = dict["PHImageResultIsInCloudKey"] as? Int
                {
                    if (isIcloudIMg == 0) {
                        
                        if let receivedImg = image
                        {
                            /* The image is now available to us */
                                imgToSend = receivedImg
                        }
                    }
                    else{
                        
                        if let receivedImg = image
                        {
                            /* The image is now available to us */
                                imgToSend = receivedImg
                        }
                        else{
                            let dialog = UIAlertController(title: "Cloud Error".localisedString(), message: "This photo could not be downloaded from iCloud. Please connect to internet.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
                            
                            let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                            
                            dialog.addAction(okAction)
                            self.present(dialog, animated:true, completion:nil)
                        }
                    }
                }
                else{
                    
                    if let receivedImg = image
                    {
                        /* The image is now available to us */
                            imgToSend = receivedImg
                    }
                    else{
                        let dialog = UIAlertController(title: "Cloud Error".localisedString(), message: "This photo could not be downloaded from iCloud. Please connect to internet.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                        
                        dialog.addAction(okAction)
                        self.present(dialog, animated:true, completion:nil)
                    }
                }
                
                // var dict : Dictionary = info!
                // let isIcloudIMg = dict["PHImageResultIsInCloudKey"] as! Int
                // if (isIcloudIMg == 0) {
                //
                // if let receivedImg = image
                // {
                // / The image is now available to us /
                // imgToSend = receivedImg
                // }
                // }
                // else{
                //
                // if let receivedImg = image
                // {
                // / The image is now available to us /
                // imgToSend = receivedImg
                // }
                // else{
                // let dialog = UIAlertController(title: "Cloud Error", message: "This photo could not be downloaded from iCloud. Please connect to internet.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
                //
                // let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                //
                // dialog.addAction(okAction)
                // self.present(dialog, animated:true, completion:nil)
                // }
                // }
                //
                
                
        })
        
        
        return imgToSend
    }

    // override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    // collectionViewGrid?.collectionViewLayout.invalidateLayout()
    // super.viewWillTransition(to: size, with: coordinator)
    // }

    //    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    //        collectionViewGrid?.collectionViewLayout.invalidateLayout()
    //        super.viewWillTransition(to: size, with: coordinator)
    //    }
}
