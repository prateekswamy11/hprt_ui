//
//  AppConstant.swift
//  Polariod_Collage
//
//  Created by MAXIMESS114 on 19/12/17.
//  Copyright © 2017 MAXIMESS114. All rights reserved.
//

import UIKit

class AppConstant: NSObject {}


let Layout_Selecetd_Color = UIColor(red:0.21, green:0.24, blue:0.78, alpha:1)
let Layout_Unselecetd_Color = UIColor(red:0.2, green:0.2, blue:0.24, alpha:1)

var totalImagesInCurrentlayout = Int()
var image_Scroll_Views_Array = [ZoomImageView]()
var selectedCollageFrameIndexInEdit = Int()

struct notificationKeys {
    var verticalLayoutTwo = "VTwo"
    var horizontalLayoutTwo = "HTwo"
    var verticalLayoutThree = "VThree"
    var horizontalLayoutThree = "HThree"
    var verticalLayoutFour = "VFour"
    var horizonyalLayoutFour = "HFour"
}

let selectedLayouts = notificationKeys()
var selectedNotificationKey = selectedLayouts.verticalLayoutTwo
var reloadCollectionKey = "ReloadColelctionView"
var selectedImageIndex = 11
var previousSelectImageIndex = 11

var isForReplacePhoto = false

var selectedImagesData = [[String:Any]]()
var tempImageArray = [UIImage]()

func checkImageForButtonCountLable(image:UIImage?,imageButton:UIButton,imageIndex:String){
    
    let imgIndex = Int(imageIndex)!
    
    if image != nil
    {
        imageButton.setTitle("", for: .normal)
    }else
    {
        imageButton.setTitle(imageIndex, for: .normal)
    }
    
    let imageDict : [String : Any] = ["image":image,"index":imgIndex]
    
    //If image of current image index is present in dict then remove else add it in array
    if let index = selectedImagesData.index(where: {$0["index"] as! Int == imgIndex}) {
        
        selectedImagesData.remove(at: index)
        
        if image != nil{
            selectedImagesData.append(imageDict)
        }
        
    }else{
        
        if selectedImagesData.count <= totalImagesInCurrentlayout
        {
            selectedImagesData.append(imageDict)
        }
    }
    
    //print("selected image Index  = \(imageIndex)")
    print("selected images count  = \(selectedImagesData.count)")
}

func setEffectOn(image:UIImage,effect:String)->UIImage
{
    
    let orientation = image.imageOrientation
    let image = image.cgImage!
    
    
    if effect == "flip"{
        
        //Flip effect
        switch orientation {
            
        case .up:
            return UIImage(cgImage: image, scale: 1.0, orientation: .downMirrored)
            
        case .downMirrored:
            return UIImage(cgImage: image, scale: 1.0, orientation: .up)
            
        case .upMirrored:
            return UIImage(cgImage: image, scale: 1.0, orientation: .down)
            
        case .down:
            return UIImage(cgImage: image, scale: 1.0, orientation: .upMirrored)
            
        default:
            return UIImage(cgImage: image, scale: 1.0, orientation: .up)
        }
    }
    else if effect == "rotate"
    {
        //Rotate effect
        switch orientation {
            
        case .up:
            return UIImage(cgImage: image, scale: 1.0, orientation: .right)
            
        case .right:
            return UIImage(cgImage: image, scale: 1.0, orientation: .down)
            
        case .down:
            return UIImage(cgImage: image, scale: 1.0, orientation: .left)
            
        case .left:
            return UIImage(cgImage: image, scale: 1.0, orientation: .up)
            
        default:
            return UIImage(cgImage: image, scale: 1.0, orientation: .up)
        }
    }
    else
    {
        //Mirror effect
        switch orientation {
            
        case .up:
            return UIImage(cgImage: image, scale: 1.0, orientation: .upMirrored)
            
        case .upMirrored:
            return UIImage(cgImage: image, scale: 1.0, orientation: .up)
            
        case .down:
            return UIImage(cgImage: image, scale: 1.0, orientation: .downMirrored)
            
        case .downMirrored:
            return UIImage(cgImage: image, scale: 1.0, orientation: .down)
            
        default:
            return UIImage(cgImage: image, scale: 1.0, orientation: .upMirrored)
        }
    }
}

func roundedCorner(image:UIImageView){
    image.layer.cornerRadius = 6
    image.clipsToBounds = true
    
    image.contentMode = .scaleAspectFill
}

func setSelecetdBorder(image:UIImageView, isSet:Bool){
    
    if isSet{
        image.layer.borderWidth = 2
        image.layer.borderColor = Layout_Selecetd_Color.cgColor
        image.backgroundColor = Layout_Selecetd_Color
    }else
    {
        image.backgroundColor = Layout_Unselecetd_Color
        image.layer.borderWidth = 0
    }
}
