//
//  TwoHorizantalViewController.swift
//  Snaptouch_Polaroid
//
//  Created by maximess120 on 26/02/18.
//  Copyright © 2018 maximess142. All rights reserved.
//

import UIKit
//import CopilotAPIAccess

class TwoHorizantalViewController: UIViewController {
    
    @IBOutlet weak var imageScrollViewTop: ZoomImageView!
    @IBOutlet weak var imageScrollViewBottom: ZoomImageView!
    
    @IBOutlet weak var collageBackgroundView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var topViewConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomViewConstraintHeight: NSLayoutConstraint!
    
    @IBOutlet weak var topViewConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var bottomViewConstraintWidth: NSLayoutConstraint!
    
    @IBOutlet weak var sepratorIndicatorConstraintBottom: NSLayoutConstraint!
    @IBOutlet weak var sepratorIndicatorView: UIView!
    var isSepratorAvailable = false
    var selImgArray = [UIImage]()
    
    var superViewWidth = CGFloat()
    var superViewHeight = CGFloat()
    var selectedTilesNumber = Int()
    
    @IBOutlet weak var btnTopRotate: UIButton!
    @IBOutlet weak var btnBottomRotate: UIButton!
    
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(TwoHorizantalViewController.singleTapGestureRecognizer(_:)))
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(TwoHorizantalViewController.singleTapGestureRecognizer(_:)))
        
        tapGesture1.numberOfTapsRequired = 1
        tapGesture2.numberOfTapsRequired = 1
        
        topView.addGestureRecognizer(tapGesture1)
        bottomView.addGestureRecognizer(tapGesture2)
        
        
        
        
        
        self.setInitialConstraints()
        
        // Add duplicate images to array as shown on previous screen
        
        // add borders to collage background view
        collageBackgroundView.layer.borderWidth = collage_tiles_border_width
        collageBackgroundView.layer.borderColor = UIColor.clear.cgColor
        
        // add borders to collage tiles
        topView.layer.borderWidth = collage_tiles_border_width
        topView.layer.borderColor = UIColor.clear.cgColor
        
        
        bottomView.layer.borderWidth = collage_tiles_border_width
        bottomView.layer.borderColor = UIColor.clear.cgColor
        
        self.initLongPressGesture()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        print("self.view.superview?.frame.size = \(self.view.superview?.frame.size)")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyMirrorEffect(_:)), name: NSNotification.Name(rawValue: "TwoHorizantalVCMirror"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyFlipEffect(_:)), name: NSNotification.Name(rawValue: "TwoHorizantalVCFlip"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyBorderEffect(_:)), name: NSNotification.Name(rawValue: "TwoHorizantalVCBorder"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.saveCollageImage(_:)), name: NSNotification.Name(rawValue: "TwoHorizantalVCSaveImg"), object: nil)
        
        
        self.addSepratorsToTiles()
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.setInitialConstraints()
            //            self.removeSepratorFromTiles()
            //self.updateViewConstraints()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        image_Scroll_Views_Array = [imageScrollViewTop,
        imageScrollViewBottom]
        if selImgArray.count < 2
        {
            selImgArray.append(selImgArray[0])
            selected_Image_Asset_Id_Array.append(selected_Image_Asset_Id_Array[0])
        }
        
        //Assign images to imageScrollView
        for var i in 0..<selImgArray.count {
            if i < image_Scroll_Views_Array.count{
                image_Scroll_Views_Array[i].zoomMode = .fill
                image_Scroll_Views_Array[i].image = selImgArray[i]
            }
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func initLongPressGesture()  {
        let longpress1 = UILongPressGestureRecognizer(target: self, action: #selector(TwoHorizantalViewController.longPressGestureRecognized1(_:)))
        topView.addGestureRecognizer(longpress1)
        
        let longpress2 = UILongPressGestureRecognizer(target: self, action: #selector(TwoHorizantalViewController.longPressGestureRecognized2(_:)))
        bottomView.addGestureRecognizer(longpress2)
        
        
        
        
    }
    func removeSepratorFromTiles()  {
        if seprator_Colour == UIColor.clear {
            
            
            self.updateTilesFrame()
            for (var view) in self.collageBackgroundView.subviews
            {
                
                if view is SeparatorView
                {
                    view.removeFromSuperview()
                }
                
            }
        }
    }
    
    func setInitialConstraints()  {
        
        /*
         if getCurrentIphone() == "6+"
         {
         tilesHeight = self.collageBackgroundView.frame.size.height / 3.2
         }
         else if getCurrentIphone() == "ipad"
         {
         tilesHeight = self.collageBackgroundView.frame.size.height / 4.2 //
         }
         else if getCurrentIphone() == "5"
         {
         tilesHeight = self.collageBackgroundView.frame.size.height / 4.2//3
         }
         else
         {
         tilesHeight = self.collageBackgroundView.frame.size.height / 2*980/640
         //4 //superViewHeight/2//
         }
         */
        
        //tilesHeight = self.collageBackgroundView.frame.size.height / 2*980/640
        
        self.bottomViewConstraintHeight.constant = self.collageBackgroundView.frame.height / 2
        self.topViewConstraintHeight.constant = self.collageBackgroundView.frame.height / 2
        
        //        self.topViewConstraintWidth.constant = tilesWidth / 2
        //        self.bottomViewConstraintWidth.constant = tilesWidth / 2
        
        
    }
    
    func updateTilesFrame()  {
        
        self.topViewConstraintHeight.constant = self.topView.frame.size.height
        self.bottomViewConstraintHeight.constant = self.bottomView.frame.size.height
    }
    
    
    func addSepratorsToTiles()  {
        
        //vertical seprators
        SeparatorView.addSeparatorBetweenViews(separatorType: .horizontal, primaryView: topView, secondaryView: bottomView, parentView: self.collageBackgroundView)
        
        
        
    }
    
    @objc func longPressGestureRecognized1(_ gestureRecognizer: UIGestureRecognizer) {
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.location(in: self.collageBackgroundView)
        
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        
        
        switch state {
        case UIGestureRecognizerState.began:
            
            
            My.cellSnapshot  = self.snapshotOfCell(topView)
            
            var center = topView.center
            My.cellSnapshot!.center = center
            My.cellSnapshot!.alpha = 0.0
            self.collageBackgroundView.addSubview(My.cellSnapshot!)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellIsAnimating = true
                My.cellSnapshot!.center = center
                My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                My.cellSnapshot!.alpha = 0.98
                // cell?.alpha = 0.0
            }, completion: { (finished) -> Void in
                if finished {
                    My.cellIsAnimating = false
                    if My.cellNeedToShow {
                        My.cellNeedToShow = false
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            //cell?.alpha = 1
                        })
                    } else {
                        //cell?.isHidden = true
                    }
                }
            })
            
            
        case UIGestureRecognizerState.changed:
            print("change")
            
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellSnapshot!.center = center
                
                
                
                
                if My.cellSnapshot!.center.x > (self.bottomView.center.x - (self.bottomView.frame.width/3)) && My.cellSnapshot!.center.x < (self.bottomView.center.x + (self.bottomView.frame.width/3)) && My.cellSnapshot!.center.y > (self.bottomView.center.y - (self.bottomView.frame.height/3)) && My.cellSnapshot!.center.y < (self.bottomView.center.y + (self.bottomView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewBottom.image
                    let img2 = self.imageScrollViewTop.image
                    
                    self.imageScrollViewBottom.image = img2
                    self.imageScrollViewTop.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                
                
                
            }
            
        default:
            print("default")
            if My.cellSnapshot != nil {
                
                My.cellSnapshot!.removeFromSuperview()
                My.cellSnapshot = nil
            }
            
        }
        
    }
    
    @objc func longPressGestureRecognized2(_ gestureRecognizer: UIGestureRecognizer) {
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.location(in: self.collageBackgroundView)
        
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        
        
        switch state {
        case UIGestureRecognizerState.began:
            
            
            My.cellSnapshot  = self.snapshotOfCell(bottomView)
            
            var center = bottomView.center
            My.cellSnapshot!.center = center
            My.cellSnapshot!.alpha = 0.0
            self.collageBackgroundView.addSubview(My.cellSnapshot!)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellIsAnimating = true
                My.cellSnapshot!.center = center
                My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                My.cellSnapshot!.alpha = 0.98
                // cell?.alpha = 0.0
            }, completion: { (finished) -> Void in
                if finished {
                    My.cellIsAnimating = false
                    if My.cellNeedToShow {
                        My.cellNeedToShow = false
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            //cell?.alpha = 1
                        })
                    } else {
                        //cell?.isHidden = true
                    }
                }
            })
            
            
        case UIGestureRecognizerState.changed:
            print("change")
            
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellSnapshot!.center = center
                
                
                
                
                if My.cellSnapshot!.center.x > (self.topView.center.x - (self.topView.frame.width/3)) && My.cellSnapshot!.center.x < (self.topView.center.x + (self.topView.frame.width/3)) && My.cellSnapshot!.center.y > (self.topView.center.y - (self.topView.frame.height/3)) && My.cellSnapshot!.center.y < (self.topView.center.y + (self.topView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewBottom.image
                    let img2 = self.imageScrollViewTop.image
                    
                    self.imageScrollViewBottom.image = img2
                    self.imageScrollViewTop.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                
                
                
            }
            
        default:
            print("default")
            if My.cellSnapshot != nil {
                
                My.cellSnapshot!.removeFromSuperview()
                My.cellSnapshot = nil
            }
            
        }
        
    }
    
    func snapshotOfCell(_ inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    
    @objc func singleTapGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        
        let selectedViewTag = gestureRecognizer.view?.tag
        print("selectedViewTag = \(selectedViewTag)")
        selectedCollageFrameIndexInEdit = selectedViewTag! - 1
        self.addBorderToSelectedTils(tag:selectedViewTag!)
        selected_Collage_Index = selectedViewTag! - 1
        NotificationCenter.default.post(name: NSNotification.Name("ReloadReplaceImageCollectionView"), object: nil)
        /*
         if isSepratorAvailable {
         
         isSepratorAvailable = false
         self.updateTilesFrame()
         self.removeSepratorFromTiles()
         }else{
         isSepratorAvailable = true
         self.addSepratorsToTiles()
         }
         */
        
    }
    
    func addBorderToSelectedTils(tag:Int)  {
        
        
        switch tag {
            
        case 1:
            
            self.sepratorIndicatorConstraintBottom.constant = 2
            if topView.layer.borderColor == collage_tiles_border_colour{
                self.removeBorderOfTiles()
                self.removeSepratorFromTiles()
                topView.layer.borderColor = UIColor.clear.cgColor
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":0])
                
            }else{
                self.removeBorderOfTiles()
                topView.layer.borderColor = collage_tiles_border_colour
                
                sepratorIndicatorView.isHidden = false
                
                if seprator_Colour == UIColor.clear
                {
                    self.removeSepratorFromTiles()
                    self.addSepratorsToTiles()
                }
                self.selectedTilesNumber = 1
                //                image_Scroll_Views_Array = [imageScrollViewTop]
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":1])
                
                self.btnTopRotate.isHidden = false
            }
            
        case 2:
            
            self.sepratorIndicatorConstraintBottom.constant = 5
            
            if bottomView.layer.borderColor == collage_tiles_border_colour{
                self.removeBorderOfTiles()
                self.removeSepratorFromTiles()
                bottomView.layer.borderColor = UIColor.clear.cgColor
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":0])
                
            }else{
                self.removeBorderOfTiles()
                bottomView.layer.borderColor = collage_tiles_border_colour
                sepratorIndicatorView.isHidden = false
                if seprator_Colour == UIColor.clear
                {
                    self.removeSepratorFromTiles()
                    self.addSepratorsToTiles()
                }
                self.selectedTilesNumber = 2
                //                image_Scroll_Views_Array = [imageScrollViewBottom]
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":1])
                
                self.btnBottomRotate.isHidden = false
            }
            
        default:
            print("error")
            self.selectedTilesNumber = 0
        }
    }
    
    func removeBorderOfTiles()  {
        sepratorIndicatorView.isHidden = true
        
        topView.layer.borderColor = UIColor.clear.cgColor
        bottomView.layer.borderColor = UIColor.clear.cgColor
        self.selectedTilesNumber = 0
        
        self.btnTopRotate.isHidden = true
        self.btnBottomRotate.isHidden = true
    }
    
    
    
    @objc func applyMirrorEffect(_ notification: NSNotification)
        
    {
        
        switch selectedTilesNumber {
        case 1:
            let img = self.imageScrollViewTop.image
            
            let flippedImage = setEffectOn(image:img!, effect:"mirror")
            
            self.imageScrollViewTop.image = flippedImage
            
        case 2:
            let img = self.imageScrollViewBottom.image
            
            let flippedImage = setEffectOn(image:img!, effect:"mirror")
            
            self.imageScrollViewBottom.image = flippedImage
            
        default:
            
            print("error")
        }
    }
    
    @objc func applyFlipEffect(_ notification: NSNotification)
        
    {
        
        switch selectedTilesNumber {
        case 1:
            
            
            let img = self.imageScrollViewTop.image
            
            let flippedImage = setEffectOn(image:img!, effect:"flip")
            
            self.imageScrollViewTop.image = flippedImage
            
        case 2:
            //self.imageScrollViewBottom.display(image: #imageLiteral(resourceName: "facebook_icon"))
            
            let img = self.imageScrollViewBottom.image
            
            let flippedImage = setEffectOn(image:img!, effect:"flip")
            
            self.imageScrollViewBottom.image = flippedImage
            
        default:
            
            print("error")
        }
    }
    
    @objc func applyBorderEffect(_ notification: NSNotification)
        
    {
        if seprator_Colour == UIColor.white{
            seprator_Colour = UIColor.clear
            collageBackgroundView.layer.borderColor = UIColor.clear.cgColor
            self.removeSepratorFromTiles()
            self.addSepratorsToTiles()
            
        }else{
            
            
            seprator_Colour = UIColor.white
            collageBackgroundView.layer.borderColor = UIColor.white.cgColor
            self.removeSepratorFromTiles()
            self.updateTilesFrame()
            for (var view) in self.collageBackgroundView.subviews
            {
                
                if view is SeparatorView
                {
                    view.removeFromSuperview()
                }
                
            }
            self.addSepratorsToTiles()
        }
        
    }
    
    @objc func saveCollageImage(_ notification: NSNotification)
    {
//        let saveCollageAnalyticsEvent = SaveCollageAnalyticsEvent(numOfPhotos: 2)
//        Copilot.instance.report.log(event: saveCollageAnalyticsEvent)
        self.updateTilesFrame()
        self.removeSepratorFromTiles()
        self.removeBorderOfTiles()
        //Crop  image
        
        let renderer = UIGraphicsImageRenderer(size: self.collageBackgroundView.bounds.size)
        let image = renderer.image { ctx in
            self.collageBackgroundView.drawHierarchy(in: self.collageBackgroundView.bounds, afterScreenUpdates: true)
        }
        
        if image != nil{
            
            collage_image = image
            
        }
    }
    
    // MARK: - Action Methods
    @IBAction func btnTopRotateClicked(_ sender: Any)
    {
        let img = self.imageScrollViewTop.image
        
        let flippedImage = setEffectOn(image:img!, effect:"rotate")
        
        self.imageScrollViewTop.image = flippedImage
    }
    
    @IBAction func btnBottomRotateClicked(_ sender: Any)
    {
        let img = self.imageScrollViewBottom.image
        
        let flippedImage = setEffectOn(image:img!, effect:"rotate")
        
        self.imageScrollViewBottom.image = flippedImage
    }
    
    
}
