//
//  ThreeVerticalViewController.swift
//  Snaptouch_Polaroid
//
//  Created by maximess120 on 26/02/18.
//  Copyright © 2018 maximess142. All rights reserved.
//

import UIKit
//import CopilotAPIAccess

class ThreeVerticalViewController: UIViewController {
    
    @IBOutlet weak var imageScrollViewRight: ZoomImageView!
    @IBOutlet weak var imageScrollViewLeft: ZoomImageView!
    @IBOutlet weak var imageScrollViewMiddle: ZoomImageView!
    
    @IBOutlet weak var collageBackgroundView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var middleView: UIView!
    
    @IBOutlet weak var rightViewConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var leftViewConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var middleViewConstraintWidth: NSLayoutConstraint!
    
    @IBOutlet weak var sepratorLeftIndicatorConstraintTrailing: NSLayoutConstraint!
    @IBOutlet weak var sepratorLeftIndicatorView: UIView!
    
    @IBOutlet weak var sepratorRightIndicatorConstraintTrailing: NSLayoutConstraint!
    @IBOutlet weak var sepratorRightIndicatorView: UIView!
    
    
    var isSepratorAvailable = false
    var selImgArray = [UIImage]()
    var selectedTilesNumber = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyBorderEffect(_:)), name: NSNotification.Name(rawValue: "ThreeVerticalVCBorder"), object: nil)
        // Do any additional setup after loading the view.
        image_Scroll_Views_Array = [imageScrollViewLeft,
                                    imageScrollViewRight,imageScrollViewMiddle]
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(ThreeVerticalViewController.singleTapGestureRecognizer(_:)))
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(ThreeVerticalViewController.singleTapGestureRecognizer(_:)))
        
        let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(ThreeVerticalViewController.singleTapGestureRecognizer(_:)))
        
        tapGesture1.numberOfTapsRequired = 1
        tapGesture2.numberOfTapsRequired = 1
        tapGesture3.numberOfTapsRequired = 1
        
        leftView.addGestureRecognizer(tapGesture1)
        middleView.addGestureRecognizer(tapGesture2)
        rightView.addGestureRecognizer(tapGesture3)
        
        
        
        
        self.setInitialConstraints()
        
        //Assign images to imageScrollView
        
        for var i in 0..<selImgArray.count {
            if i < image_Scroll_Views_Array.count{
                
                if i == 0{
                    image_Scroll_Views_Array[0].zoomMode = .fill
                    image_Scroll_Views_Array[0].image = selImgArray[0]
                }
                else if i == 1{
                    image_Scroll_Views_Array[2].zoomMode = .fill
                    image_Scroll_Views_Array[2].image = selImgArray[1]
                }
                else if i == 2{
                    image_Scroll_Views_Array[1].zoomMode = .fill
                    image_Scroll_Views_Array[1].image = selImgArray[2]
                }
                
            }
        }
        
        //separator left
        sepratorLeftIndicatorView.layer.cornerRadius = separator_indicator_corner_radius
        sepratorLeftIndicatorView.layer.borderWidth = 0.5
        sepratorLeftIndicatorView.layer.borderColor = UIColor.lightGray.cgColor
        
        //separator right
        sepratorRightIndicatorView.layer.cornerRadius = separator_indicator_corner_radius
        sepratorRightIndicatorView.layer.borderWidth = 0.5
        sepratorRightIndicatorView.layer.borderColor = UIColor.lightGray.cgColor
        
        // add borders to collage background view
        collageBackgroundView.layer.borderWidth = collage_tiles_border_width
        collageBackgroundView.layer.borderColor = UIColor.clear.cgColor
        
        // add borders to collage tiles
        leftView.layer.borderWidth = collage_tiles_border_width
        leftView.layer.borderColor = UIColor.clear.cgColor
        
        middleView.layer.borderWidth = collage_tiles_border_width
        middleView.layer.borderColor = UIColor.clear.cgColor
        
        rightView.layer.borderWidth = collage_tiles_border_width
        rightView.layer.borderColor = UIColor.clear.cgColor
        
        self.initLongPressGesture()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
            URLCache.shared.removeAllCachedResponses()
            URLCache.shared.diskCapacity = 0
            URLCache.shared.memoryCapacity = 0
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyMirrorEffect(_:)), name: NSNotification.Name(rawValue: "ThreeVerticalVCMirror"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyFlipEffect(_:)), name: NSNotification.Name(rawValue: "ThreeVerticalVCFlip"), object: nil)
        
        // NotificationCenter.default.addObserver(self, selector: #selector(self.applyBorderEffect(_:)), name: NSNotification.Name(rawValue: "ThreeVerticalVCBorder"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.saveCollageImage(_:)), name: NSNotification.Name(rawValue: "ThreeVerticalVCSaveImg"), object: nil)
        
        self.addSepratorsToTiles()
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.setInitialConstraints()
            self.updateTilesFrame()
            self.removeSepratorFromTiles()
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    func initLongPressGesture()  {
        let longpress1 = UILongPressGestureRecognizer(target: self, action: #selector(ThreeHorizantalViewController.longPressGestureRecognized1(_:)))
        leftView.addGestureRecognizer(longpress1)
        
        let longpress2 = UILongPressGestureRecognizer(target: self, action: #selector(ThreeHorizantalViewController.longPressGestureRecognized2(_:)))
        middleView.addGestureRecognizer(longpress2)
        
        let longpress3 = UILongPressGestureRecognizer(target: self, action: #selector(ThreeHorizantalViewController.longPressGestureRecognized3(_:)))
        rightView.addGestureRecognizer(longpress3)
        
        
    }
    
    func removeSepratorFromTiles()  {
        
        if seprator_Colour == UIColor.clear {
            self.updateTilesFrame()
            for (var view) in self.collageBackgroundView.subviews
            {
                
                if view is SeparatorView
                {
                    //self.middleViewConstraintLeading.constant = self.middleView.frame.origin.x
                    //print("self.middleView.frame.origin.x = \(self.middleView.frame.origin.x)")
                    self.middleView.frame.origin.x = self.middleView.frame.origin.x
                    view.removeFromSuperview()
                    
                    self.middleView.leadingAnchor.constraint(equalTo: leftView.trailingAnchor).isActive = true
                    
                    //print("2self.middleView.frame.origin.x = \(self.middleView.frame.origin.x)")
                    self.middleView.frame.origin.x = self.middleView.frame.origin.x
                }
                
            }
        }
    }
    
    func setInitialConstraints()  {
        
        var tilesWidth = CGFloat()
        
        if getCurrentIphone() == "6+"
        {
            tilesWidth = self.collageBackgroundView.frame.size.width / 2.8
        }
        else if getCurrentIphone() == "ipad"
        {
            tilesWidth = self.collageBackgroundView.frame.size.width / 3.5
        }
        else if getCurrentIphone() == "5"
        {
            tilesWidth = self.collageBackgroundView.frame.size.width / 3.5
        }
        else{
            tilesWidth = self.collageBackgroundView.frame.size.width / 3.0 //3.2
        }
        
        
        //let tilesHeight = self.collageBackgroundView.frame.size.height / 1.8
        
        self.rightViewConstraintWidth.constant = tilesWidth
        self.leftViewConstraintWidth.constant = tilesWidth
        self.middleViewConstraintWidth.constant = tilesWidth
        
        
        
        
    }
    
    func updateTilesFrame()  {
        
        self.rightViewConstraintWidth.constant = self.rightView.frame.size.width
        self.leftViewConstraintWidth.constant = self.leftView.frame.size.width
        self.middleViewConstraintWidth.constant = self.middleView.frame.size.width
        //self.middleViewConstraintLeading.constant = self.middleView.frame.origin.x
        
        
        
    }
    
    func addSepratorsToTiles()  {
        
        //vertical seprators
        SeparatorView.addSeparatorBetweenViews(separatorType: .vertical, primaryView: leftView, secondaryView: middleView, parentView: self.collageBackgroundView)
        
        SeparatorView.addSeparatorBetweenViews(separatorType: .vertical, primaryView: middleView, secondaryView: rightView, parentView: self.collageBackgroundView)
        
        
        
    }
    
    @objc func longPressGestureRecognized1(_ gestureRecognizer: UIGestureRecognizer) {
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.location(in: self.collageBackgroundView)
        
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        
        
        switch state {
        case UIGestureRecognizerState.began:
            
            
            My.cellSnapshot  = self.snapshotOfCell(leftView)
            
            var center = leftView.center
            My.cellSnapshot!.center = center
            My.cellSnapshot!.alpha = 0.0
            self.collageBackgroundView.addSubview(My.cellSnapshot!)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellIsAnimating = true
                My.cellSnapshot!.center = center
                My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                My.cellSnapshot!.alpha = 0.98
                // cell?.alpha = 0.0
            }, completion: { (finished) -> Void in
                if finished {
                    My.cellIsAnimating = false
                    if My.cellNeedToShow {
                        My.cellNeedToShow = false
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            //cell?.alpha = 1
                        })
                    } else {
                        //cell?.isHidden = true
                    }
                }
            })
            
            
        case UIGestureRecognizerState.changed:
            print("change")
            
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellSnapshot!.center = center
                
                
                if My.cellSnapshot!.center.x > (self.rightView.center.x - (self.rightView.frame.width/3)) && My.cellSnapshot!.center.x < (self.rightView.center.x + (self.rightView.frame.width/3)) && My.cellSnapshot!.center.y > (self.rightView.center.y - (self.rightView.frame.height/3)) && My.cellSnapshot!.center.y < (self.rightView.center.y + (self.rightView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewRight.image
                    let img2 = self.imageScrollViewLeft.image
                    
                    self.imageScrollViewRight.image = img2
                    
                    self.imageScrollViewLeft.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                    
                else if My.cellSnapshot!.center.x > (self.middleView.center.x - (self.middleView.frame.width/3)) && My.cellSnapshot!.center.x < (self.middleView.center.x + (self.middleView.frame.width/3)) && My.cellSnapshot!.center.y > (self.middleView.center.y - (self.middleView.frame.height/3)) && My.cellSnapshot!.center.y < (self.middleView.center.y + (self.middleView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewLeft.image
                    let img2 = self.imageScrollViewMiddle.image
                    
                    self.imageScrollViewLeft.image = img2
                    self.imageScrollViewMiddle.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                
                
                
            }
            
        default:
            print("default")
            if My.cellSnapshot != nil {
                
                My.cellSnapshot!.removeFromSuperview()
                My.cellSnapshot = nil
            }
            
        }
        
    }
    
    @objc func longPressGestureRecognized2(_ gestureRecognizer: UIGestureRecognizer) {
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.location(in: self.collageBackgroundView)
        
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        
        
        switch state {
        case UIGestureRecognizerState.began:
            
            
            My.cellSnapshot  = self.snapshotOfCell(middleView)
            
            var center = middleView.center
            My.cellSnapshot!.center = center
            My.cellSnapshot!.alpha = 0.0
            self.collageBackgroundView.addSubview(My.cellSnapshot!)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellIsAnimating = true
                My.cellSnapshot!.center = center
                My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                My.cellSnapshot!.alpha = 0.98
                // cell?.alpha = 0.0
            }, completion: { (finished) -> Void in
                if finished {
                    My.cellIsAnimating = false
                    if My.cellNeedToShow {
                        My.cellNeedToShow = false
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            //cell?.alpha = 1
                        })
                    } else {
                        //cell?.isHidden = true
                    }
                }
            })
            
            
        case UIGestureRecognizerState.changed:
            print("change")
            
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellSnapshot!.center = center
                
                
                if My.cellSnapshot!.center.x > (self.leftView.center.x - (self.leftView.frame.width/3)) && My.cellSnapshot!.center.x < (self.leftView.center.x + (self.leftView.frame.width/3)) && My.cellSnapshot!.center.y > (self.leftView.center.y - (self.leftView.frame.height/3)) && My.cellSnapshot!.center.y < (self.leftView.center.y + (self.leftView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewLeft.image
                    let img2 = self.imageScrollViewMiddle.image
                    
                    self.imageScrollViewLeft.image = img2
                    
                    self.imageScrollViewMiddle.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                    
                else if My.cellSnapshot!.center.x > (self.rightView.center.x - (self.rightView.frame.width/3)) && My.cellSnapshot!.center.x < (self.rightView.center.x + (self.rightView.frame.width/3)) && My.cellSnapshot!.center.y > (self.rightView.center.y - (self.rightView.frame.height/3)) && My.cellSnapshot!.center.y < (self.rightView.center.y + (self.rightView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewRight.image
                    let img2 = self.imageScrollViewMiddle.image
                    
                    self.imageScrollViewRight.image = img2
                    self.imageScrollViewMiddle.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                
                
                
            }
            
        default:
            print("default")
            if My.cellSnapshot != nil {
                
                My.cellSnapshot!.removeFromSuperview()
                My.cellSnapshot = nil
            }
            
        }
        
    }
    
    @objc func longPressGestureRecognized3(_ gestureRecognizer: UIGestureRecognizer) {
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.location(in: self.collageBackgroundView)
        
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        
        
        switch state {
        case UIGestureRecognizerState.began:
            
            
            My.cellSnapshot  = self.snapshotOfCell(rightView)
            
            var center = rightView.center
            My.cellSnapshot!.center = center
            My.cellSnapshot!.alpha = 0.0
            self.collageBackgroundView.addSubview(My.cellSnapshot!)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellIsAnimating = true
                My.cellSnapshot!.center = center
                My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                My.cellSnapshot!.alpha = 0.98
                // cell?.alpha = 0.0
            }, completion: { (finished) -> Void in
                if finished {
                    My.cellIsAnimating = false
                    if My.cellNeedToShow {
                        My.cellNeedToShow = false
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            //cell?.alpha = 1
                        })
                    } else {
                        //cell?.isHidden = true
                    }
                }
            })
            
            
        case UIGestureRecognizerState.changed:
            print("change")
            
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellSnapshot!.center = center
                
                
                if My.cellSnapshot!.center.x > (self.leftView.center.x - (self.leftView.frame.width/3)) && My.cellSnapshot!.center.x < (self.leftView.center.x + (self.leftView.frame.width/3)) && My.cellSnapshot!.center.y > (self.leftView.center.y - (self.leftView.frame.height/3)) && My.cellSnapshot!.center.y < (self.leftView.center.y + (self.leftView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewLeft.image
                    let img2 = self.imageScrollViewRight.image
                    
                    self.imageScrollViewLeft.image = img2
                    
                    self.imageScrollViewRight.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                    
                else if My.cellSnapshot!.center.x > (self.middleView.center.x - (self.middleView.frame.width/3)) && My.cellSnapshot!.center.x < (self.middleView.center.x + (self.middleView.frame.width/3)) && My.cellSnapshot!.center.y > (self.middleView.center.y - (self.middleView.frame.height/3)) && My.cellSnapshot!.center.y < (self.middleView.center.y + (self.middleView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewMiddle.image
                    let img2 = self.imageScrollViewRight.image
                    
                    self.imageScrollViewMiddle.image = img2
                    self.imageScrollViewRight.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                
                
                
            }
            
        default:
            print("default")
            if My.cellSnapshot != nil {
                
                My.cellSnapshot!.removeFromSuperview()
                My.cellSnapshot = nil
            }
            
        }
        
    }
    
    func snapshotOfCell(_ inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    
    
    @objc func singleTapGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        
        var selectedViewTag = gestureRecognizer.view?.tag
        print("selectedViewTag = \(selectedViewTag)")
        
        selectedCollageFrameIndexInEdit = selectedViewTag! - 1
        
        self.addBorderToSelectedTils(tag:selectedViewTag!)
        /*
         if isSepratorAvailable {
         
         isSepratorAvailable = false
         self.updateTilesFrame()
         self.removeSepratorFromTiles()
         }else{
         isSepratorAvailable = true
         self.addSepratorsToTiles()
         }
         */
        
    }
    
    func addBorderToSelectedTils(tag:Int)  {
        switch tag {
            
        case 1:
            
            self.sepratorLeftIndicatorConstraintTrailing.constant = 2
            if leftView.layer.borderColor == collage_tiles_border_colour{
                self.removeBorderOfTiles()
                self.removeSepratorFromTiles()
                leftView.layer.borderColor = UIColor.clear.cgColor
                
                
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":0])
                
            }else{
                self.removeBorderOfTiles()
                leftView.layer.borderColor = collage_tiles_border_colour
                
                sepratorLeftIndicatorView.isHidden = false
                if seprator_Colour == UIColor.clear
                {
                    self.removeSepratorFromTiles()
                    self.addSepratorsToTiles()
                }
                self.selectedTilesNumber = 1
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":1])
            }
            
        case 2:
            
            self.sepratorLeftIndicatorConstraintTrailing.constant = 5
            self.sepratorRightIndicatorConstraintTrailing.constant = 2
            
            if middleView.layer.borderColor == collage_tiles_border_colour{
                self.removeBorderOfTiles()
                self.removeSepratorFromTiles()
                middleView.layer.borderColor = UIColor.clear.cgColor
                
                
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":0])
                
            }else{
                self.removeBorderOfTiles()
                middleView.layer.borderColor = collage_tiles_border_colour
                sepratorLeftIndicatorView.isHidden = false
                sepratorRightIndicatorView.isHidden = false
                if seprator_Colour == UIColor.clear
                {
                    self.removeSepratorFromTiles()
                    self.addSepratorsToTiles()
                }
                self.selectedTilesNumber = 2
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":1])
            }
            
        case 3:
            
            self.sepratorRightIndicatorConstraintTrailing.constant = 5
            
            if rightView.layer.borderColor == collage_tiles_border_colour{
                self.removeBorderOfTiles()
                self.removeSepratorFromTiles()
                rightView.layer.borderColor = UIColor.clear.cgColor
                
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":0])
                
            }else{
                self.removeBorderOfTiles()
                rightView.layer.borderColor = collage_tiles_border_colour
                sepratorRightIndicatorView.isHidden = false
                if seprator_Colour == UIColor.clear
                {
                    self.removeSepratorFromTiles()
                    self.addSepratorsToTiles()
                }
                self.selectedTilesNumber = 3
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":1])
            }
            
        default:
            print("error")
        }
    }
    
    func removeBorderOfTiles()  {
        sepratorLeftIndicatorView.isHidden = true
        sepratorRightIndicatorView.isHidden = true
        
        leftView.layer.borderColor = UIColor.clear.cgColor
        rightView.layer.borderColor = UIColor.clear.cgColor
        middleView.layer.borderColor = UIColor.clear.cgColor
        self.selectedTilesNumber = 0
    }
    
    @objc func applyMirrorEffect(_ notification: NSNotification)
        
    {
        
        switch selectedTilesNumber {
        case 1:
            let img = self.imageScrollViewLeft.image
            
            let flippedImage = setEffectOn(image:img!, effect:"mirror")
            
            self.imageScrollViewLeft.image = flippedImage
            
        case 2:
            let img = self.imageScrollViewMiddle.image
            
            let flippedImage = setEffectOn(image:img!, effect:"mirror")
            
            self.imageScrollViewMiddle.image = flippedImage
            
        case 3:
            let img = self.imageScrollViewRight.image
            
            let flippedImage = setEffectOn(image:img!, effect:"mirror")
            
            self.imageScrollViewRight.image = flippedImage
            
        default:
            
            print("error")
        }
    }
    
    @objc func applyFlipEffect(_ notification: NSNotification)
        
    {
        
        switch selectedTilesNumber {
        case 1:
            
            
            let img = self.imageScrollViewLeft.image
            
            let flippedImage = setEffectOn(image:img!, effect:"flip")
            
            self.imageScrollViewLeft.image = flippedImage
            
        case 2:
            //self.imageScrollViewBottom.display(image: #imageLiteral(resourceName: "facebook_icon"))
            
            let img = self.imageScrollViewMiddle.image
            
            let flippedImage = setEffectOn(image:img!, effect:"flip")
            
            self.imageScrollViewMiddle.image = flippedImage
            
        case 3:
            //self.imageScrollViewBottom.display(image: #imageLiteral(resourceName: "facebook_icon"))
            
            let img = self.imageScrollViewRight.image
            
            let flippedImage = setEffectOn(image:img!, effect:"flip")
            
            self.imageScrollViewRight.image = flippedImage
            
        default:
            
            print("error")
        }
    }
    
    @objc func applyBorderEffect(_ notification: NSNotification)
        
    {
        if seprator_Colour == UIColor.white{
            seprator_Colour = UIColor.clear
            collageBackgroundView.layer.borderColor = UIColor.clear.cgColor
            self.removeSepratorFromTiles()
            self.addSepratorsToTiles()
            
        }else{
            seprator_Colour = UIColor.white
            collageBackgroundView.layer.borderColor = UIColor.white.cgColor
            self.removeSepratorFromTiles()
            self.updateTilesFrame()
            for (var view) in self.collageBackgroundView.subviews
            {
                
                if view is SeparatorView
                {
                    view.removeFromSuperview()
                }
                
            }
            self.addSepratorsToTiles()
        }
        
    }
    
    @objc func saveCollageImage(_ notification: NSNotification)
    {
//        let saveCollageAnalyticsEvent = SaveCollageAnalyticsEvent(numOfPhotos: 3)
//        Copilot.instance.report.log(event: saveCollageAnalyticsEvent)
        self.updateTilesFrame()
        self.removeSepratorFromTiles()
        self.removeBorderOfTiles()
        //Crop  image
        
        let renderer = UIGraphicsImageRenderer(size: self.collageBackgroundView.bounds.size)
        let image = renderer.image { ctx in
            self.collageBackgroundView.drawHierarchy(in: self.collageBackgroundView.bounds, afterScreenUpdates: true)
        }
        
        if image != nil{
            
            collage_image = image
            
        }
    }
    
    
}
