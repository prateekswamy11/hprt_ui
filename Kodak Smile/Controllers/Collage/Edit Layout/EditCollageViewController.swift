//
//  EditCollageViewController.swift
//  Polaroid MINT
//
//  Created by maximess142 on 06/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit
import Photos
//import CopilotAPIAccess

func saveEditedImage(_ view: UIView, saveImage flag: Bool) -> UIImage {
    
    let size = view.frame.size
    
    UIGraphicsBeginImageContextWithOptions(CGSize(width: round(size.width), height: round(size.height)), false, 0)
    
    let context: CGContext? = UIGraphicsGetCurrentContext()
    view.layer.render(in: context!)
    let capturedScreen: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return capturedScreen ?? UIImage()
}

class EditCollageViewController: UIViewController
{
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnReplaceImg: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet weak var constraintVwLayoutWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintVwLayoutHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewForReplaceImage: UICollectionView!
    
    @IBOutlet weak var constraintForBottomPAddingImageSelection: NSLayoutConstraint!
    @IBOutlet weak var constraintHieghtViewForImageReplace: NSLayoutConstraint!
    @IBOutlet weak var viewForImageReplaceSelection: UIView!
    @IBOutlet weak var vwLayout: UIView!
    
    @IBOutlet weak var btnMirror: UIButton!
    @IBOutlet weak var btnFlip: UIButton!
    @IBOutlet weak var btnBorder: UIButton!
    
    @IBOutlet weak var lblMirror: UILabel!
    @IBOutlet weak var lblFlip: UILabel!
    
    @IBOutlet weak var layoutViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var layoutViewWidthConstraint: NSLayoutConstraint!
    
    var layoutIndex = Int()
    var selImgArray = [UIImage]()
    var fetchResult: PHFetchResult<PHAsset>!
    var imagesArray: PHFetchResult<PHAsset>!
    var cameFrom = "Local"
    var selectedImageIndex = 0
    var socialImagesURL = [String]()
    var selAssetsIdentifiersArray = [String]()
    var savedImageAssetIdentifier = String()
    let imageManager = PHCachingImageManager()
    let cellImgoptions = PHImageRequestOptions()
    let inset: CGFloat = 3
    let minimumLineSpacing: CGFloat = 3
    let minimumInteritemSpacing: CGFloat = 3
    let cellsPerRow = 4
    
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.cellImgoptions.deliveryMode = .fastFormat
        self.cellImgoptions.isSynchronous = true
        //        self.layoutViewHeightConstraint.constant = 500
        //        self.layoutViewWidthConstraint.constant = 333
        self.constraintHieghtViewForImageReplace.constant = self.view.frame.height / 2.5
        NotificationCenter.default.addObserver(self, selector: #selector(self.replaceCollage(_:)), name: NSNotification.Name(rawValue: "ReplaceCollage"), object: nil)
        self.localizedStrings()
        
//        if self.cameFrom == "Local"{
            selected_Image_Asset_Id_Array = self.selAssetsIdentifiersArray
//        }else{
//            selected_Image_Asset_Id_Array = self.socialImagesURL
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        seprator_Colour = UIColor.clear
        NotificationCenter.default.addObserver(self, selector: #selector(self.collageStileSelect(_:)), name: NSNotification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.relaodCollectionView), name: NSNotification.Name("ReloadReplaceImageCollectionView"), object: nil)
        self.disableMirrorFlipBtn()
        self.setVwLayoutOrientation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getAssetIdentifierAfterSave(notification:)), name: NSNotification.Name(rawValue: "getAssetIdentifierAfterSave"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        customLoader.stopGifLoader()
        //Report screen load event
//        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.editCollage.rawValue)
//        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReloadReplaceImageCollectionView"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        seprator_Colour = UIColor.clear
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "getAssetIdentifierAfterSave"), object: nil)
    }
    
    
    //MARK:- --------------------- @IBActions -----------------------------
    
    @IBAction func backBtnClicked(_ sender:UIButton){
        //        isPopover = true
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveBtnClicked(_ sender:UIButton){
        
        switch layoutIndex {
            
        case 0:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "TwoHorizantalVCSaveImg"), object: nil)
            
            
        case 1:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "TwoVerticalVCSaveImg"), object: nil)
            
        case 2:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ThreeHorizantalVCSaveImg"), object: nil)
            
        case 3:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "FourVerticalVCSaveImg"), object: nil)
            
        case 4:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ThreeVerticalVCSaveImg"), object: nil)
            
        case 5:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ThreeHorizantalVCSaveImg"), object: nil)
            
        default:
            print("error")
        }
        
        // Move to image preview cell
        if collage_image != nil
        {
            saveImageInCustomAlbum().saveImageInAlbum(name: PolaroidSnaptouchEditAlbum, withImage: collage_image)
            self.view.makeToast("Collage Saved".localisedString(), duration: 2.0, position: .bottom)
            
            // Move to image preview when notification for image saved is fired. So commenting this code for now.
            for item in self.navigationController!.viewControllers {
                // Filter for your desired view controller:
                if item.isKind(of: HomeViewController.self) {
                    //isMainScrollViewControllerAvailable = true
                    self.navigationController?.popToViewController(item, animated: true)
                }
            }
        }
    }
    
    @IBAction func borderBtnClicked(_ sender:UIButton){
        
        switch layoutIndex {
        case 0:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "TwoHorizantalVCBorder"), object: nil)
            
        case 1:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "TwoVerticalVCBorder"), object: nil)
            
        case 2:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ThreeHorizantalVCBorder"), object: nil)
            
        case 3:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "FourVerticalVCBorder"), object: nil)
            
        case 4:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "FourHorizantalVCBorder"), object: nil)
            
        case 5:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "FourVerticalVCBorder"), object: nil)
            
        default:
            print("error")
        }
        
    }
    
    @IBAction func mirrorBtnClicked(_ sender:UIButton)
    {
        switch layoutIndex {
        case 0:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "TwoVerticalVCMirror"), object: nil)
            
        case 1:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "TwoHorizantalVCMirror"), object: nil)
            
        case 2:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ThreeVerticalVCMirror"), object: nil)
            
        case 3:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ThreeHorizantalVCMirror"), object: nil)
            
        case 4:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "FourHorizantalVCMirror"), object: nil)
            
        case 5:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "FourVerticalVCMirror"), object: nil)
            
        default:
            print("error")
        }
    }
    
    @IBAction func flipBtnClicked(_ sender:UIButton){
        
        switch layoutIndex {
        case 0:
            
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "TwoVerticalVCFlip"), object: nil)
            
            
        case 1:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "TwoHorizantalVCFlip"), object: nil)
            
        case 2:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ThreeVerticalVCFlip"), object: nil)
            
        case 3:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ThreeHorizantalVCFlip"), object: nil)
            
        case 4:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "FourHorizantalVCFlip"), object: nil)
            
        case 5:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "FourVerticalVCFlip"), object: nil)
            
        default:
            print("error")
        }
        
        
    }
    
    @IBAction func replaceBtnClicked(_ sender:UIButton)
    {
        /*
         let vc =  self.storyboard?.instantiateViewController(withIdentifier: "NewCreateCollageViewController") as! NewCreateCollageViewController
         //vc.selectedLayoutIndex = layoutIndex
         vc.isReplaceMode = true
         vc.fetchResult = fetchResult
         vc.selAssetsIdentifiersArray = selAssetsIdentifiersArray
         vc.selectedCollageLayoutIndex = layoutIndex
         
         vc.selectedCollageFrameIndex = selectedCollageFrameIndexInEdit
         //Rest selectedCollageFrameIndex
         selectedCollageFrameIndexInEdit = 0
         self.navigationController?.pushViewController(vc, animated: true)
         */
        //        self.viewForImageReplaceSelection.isHidden = false
        self.updateBottomView(constraint: 0)
        self.collectionViewForReplaceImage.reloadData()
        //        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cancelForImageSelectionClicked(_ sender: UIButton) {
        //        self.viewForImageReplaceSelection.isHidden = true
        self.updateBottomView(constraint: -400)
    }
    
    
    
    //MARK:- --------------------- Func -----------------------------
    
    //Assign values to fields.
    func localizedStrings() {
        self.lblTitle.text = "adjust layout".localisedString()
        self.btnCancel.setTitle( "cancel".localisedString(), for: .normal)
        self.btnReplaceImg.setTitle( "replace image".localisedString(), for: .normal)
        self.btnSave.setTitle( "Save".localisedString(), for: .normal)
        self.btnDone.setTitle("Done".localisedString(), for: .normal)
    }
    
    @objc func relaodCollectionView(){
        self.collectionViewForReplaceImage.reloadData()
    }
    
    
    func updateBottomView(constraint: CGFloat){
        self.constraintForBottomPAddingImageSelection.constant = constraint
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func enableMirrorFlipBtn(){
        self.btnMirror.isEnabled = true
        self.btnFlip.isEnabled = true
        
        self.btnReplaceImg.isEnabled = true
    }
    
    func disableMirrorFlipBtn(){
        self.btnMirror.isEnabled = false
        self.btnFlip.isEnabled = false
        
        self.btnReplaceImg.isEnabled = false
    }
    
    
    func setVwLayoutOrientation(){
        
        let newHeight = (980 / 640) * (self.view.frame.size.width * 0.63)
        
        
        switch layoutIndex {
            
        case 0:
            // Portrait Mode
            // [] []
            self.portrait()
            let layoutVC = TwoHorizantalViewController(nibName: "TwoHorizantalViewController", bundle: nil)
            layoutVC.selImgArray = self.selImgArray
            layoutVC.superViewHeight = self.view.frame.size.width
            layoutVC.superViewWidth = self.view.frame.size.width * 0.68
            //self.addFramelayouts(layoutVc: layoutVC, size: CGSize(width: self.view.frame.size.width * 0.68, height: self.view.frame.size.width))
            self.addFramelayouts(layoutVc: layoutVC, size: CGSize(width: self.view.frame.size.width * 0.63, height: newHeight))
            
        case 1:
            // Portrait Mode
            // []
            // []
            self.portrait()
            let layoutVC = TwoVerticalViewController(nibName: "TwoVerticalViewController", bundle: nil)
            layoutVC.selImgArray = self.selImgArray
            
            //self.addFramelayouts(layoutVc: layoutVC, size: CGSize(width: self.view.frame.size.width, height: self.view.frame.size.width * 0.63))
            self.addFramelayouts(layoutVc: layoutVC, size: CGSize(width: newHeight, height: self.view.frame.size.width * 0.63))
            
        case 2:
            // Portrait Mode
            // []
            // []
            // []
            self.portrait()
            let layoutVC = ThreeHorizantalViewController(nibName: "ThreeHorizantalViewController", bundle: nil)
            layoutVC.selImgArray = self.selImgArray
            //self.addFramelayouts(layoutVc: layoutVC, size: CGSize(width: self.view.frame.size.width * 0.68, height: self.view.frame.size.width))
            self.addFramelayouts(layoutVc: layoutVC, size: CGSize(width: self.view.frame.size.width * 0.63, height: newHeight))
            
        case 3:
            // Portrait Mode
            // [] []
            // [] []
            self.portrait()
            let layoutVC = FourVerticalViewController(nibName: "FourVerticalViewController", bundle: nil)
            layoutVC.selImgArray = self.selImgArray
            
            //self.addFramelayouts(layoutVc: layoutVC, size: CGSize(width: self.view.frame.size.width * 0.68, height: self.view.frame.size.width))
            self.addFramelayouts(layoutVc: layoutVC, size: CGSize(width: self.view.frame.size.width * 0.63, height: newHeight))
            
        case 4:
            self.landscape()
            let layoutVC = FourHorizantalViewController(nibName: "FourHorizantalViewController", bundle: nil)
            layoutVC.selImgArray = self.selImgArray
            //self.addFramelayouts(layoutVc: layoutVC, size: CGSize(width: self.view.frame.size.width, height: self.view.frame.size.width * 0.63))
            self.addFramelayouts(layoutVc: layoutVC, size: CGSize(width: newHeight, height: self.view.frame.size.width * 0.63))
        case 5:
            // previously case 3 in zip
            self.portrait()
            let layoutVC = ThreeHorizantalViewController(nibName: "ThreeHorizantalViewController", bundle: nil)
            layoutVC.selImgArray = self.selImgArray
            //self.addFramelayouts(layoutVc: layoutVC, size: CGSize(width: self.view.frame.size.width * 0.68, height: self.view.frame.size.width))
            self.addFramelayouts(layoutVc: layoutVC, size: CGSize(width: self.view.frame.size.width * 0.63, height: newHeight))
        default:
            print("error")
        }
    }
    
    func landscape()  {
        //        let newHeight = (980 / 640) * (self.view.frame.size.width * 0.63)
        //        self.constraintVwLayoutWidth.constant =  newHeight // 407
        //        self.constraintVwLayoutHeight.constant = self.view.frame.size.width * 0.63//0.63//0.68 251//
    }
    
    func portrait()  {
        //        let newHeight = (980 / 640) * (self.view.frame.size.width * 0.63)
        //        self.constraintVwLayoutWidth.constant =  self.view.frame.size.width * 0.63 //333
        //
        //        self.constraintVwLayoutHeight.constant = newHeight //500
    }
    
    func addFramelayouts(layoutVc:UIViewController, size:CGSize){
        
        if (self.vwLayout.viewWithTag(100) != nil)
        {
            if let layoutView = self.vwLayout.viewWithTag(100){
                layoutView.removeFromSuperview()
            }
        }
        
        layoutVc.view.tag = 100
        
        let point = CGPoint(x: self.vwLayout.frame.width/2, y:  self.vwLayout.frame.height/2)
        layoutVc.view.frame = CGRect(origin: point, size: size)
        layoutVc.view.center = point
        
        self.vwLayout.addSubview(layoutVc.view)
        self.addChildViewController(layoutVc)
        //self.vwLayout.sendSubview(toBack: layoutVc.view)
        layoutVc.view.frame = self.vwLayout.bounds
        layoutVc.didMove(toParentViewController: self)
    }
    
    @objc func collageStileSelect(_ notification: NSNotification)
        
    {
        //let dict = notification.userInfo! as NSDictionary
        let dict = notification.object as! NSDictionary
        
        guard let selected = dict.object(forKey: "selected") as? Int else {
            return
        }
        
        if selected == 1 {
            self.enableMirrorFlipBtn()
        }
        else{
            self.disableMirrorFlipBtn()
        }
        
    }
    
    @objc func replaceCollage(_ notification: NSNotification)
        
    {
        //let dict = notification.userInfo! as NSDictionary
        let dict = notification.object as! NSDictionary
        
        guard let layoutIndexReplace = dict.object(forKey: "layoutIndex") as? Int else {
            return
        }
        
        guard let selImgArrayReplace = dict.object(forKey: "selImgArray") as? [UIImage] else {
            return
        }
        
        guard let fetchResultReplace = dict.object(forKey: "fetchResult") as? PHFetchResult<PHAsset> else {
            return
        }
        
        guard let selAssetsIdentifiersArrayReplace = dict.object(forKey: "selAssetsIdentifiersArray") as? [String] else {
            return
        }
        
        self.layoutIndex = layoutIndexReplace
        self.selImgArray = selImgArrayReplace
        self.fetchResult = fetchResultReplace
        self.selAssetsIdentifiersArray = selAssetsIdentifiersArrayReplace
        
    }
    
    @objc func getAssetIdentifierAfterSave(notification : Notification)
    {
        /*
         if let assetIdentifier = notification.object as? String
         {
         self.savedImageAssetIdentifier = assetIdentifier
         print("Pratiksha's collage identifier = ", assetIdentifier)
         
         let imagedict = notification.userInfo
         var imageToSend = UIImage()
         if let image = imagedict!["image"] as? UIImage
         {
         imageToSend = image
         }
         
         let imagePreviewVC = storyboards.imgPreviewStoryboard.instantiateViewController(withIdentifier: "ImagePreviewViewController") as! ImagePreviewViewController
         
         imagePreviewVC.printImageArray.append(imageToSend)
         imagePreviewVC.pushFrom = "Collage"
         //            singletonFetchResult = fetchResult
         
         // Get all photos assets again to show current image selected when again gone to collage screen from image preview screen directly.
         let allPhotosOptions = PHFetchOptions()
         allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: isSortByOldestFirst)]
         allPhotosOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
         singletonFetchResult = PHAsset.fetchAssets(with: allPhotosOptions)
         imagePreviewVC.imgLocalIdentifier = assetIdentifier
         
         self.navigationController?.pushViewController(imagePreviewVC, animated: true)
         }*/
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
