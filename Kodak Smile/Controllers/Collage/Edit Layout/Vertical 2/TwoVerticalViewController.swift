//
//  TwoVerticalViewController.swift
//  Snaptouch_Polaroid
//
//  Created by maximess120 on 20/02/18.
//  Copyright © 2018 maximess142. All rights reserved.
//

import UIKit
//import CopilotAPIAccess

var selected_Collage_Index: Int = 0 // for selected colage image for replace
var selected_Image_Asset_Id_Array: [String] = [""]  // for saving asset id of images in collage

class TwoVerticalViewController: UIViewController {
    
    @IBOutlet weak var imageScrollViewRight: ZoomImageView!
    @IBOutlet weak var imageScrollViewLeft: ZoomImageView!
    
    @IBOutlet weak var collageBackgroundView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var leftView: UIView!
    
    @IBOutlet weak var rightViewConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var leftViewConstraintWidth: NSLayoutConstraint!
    
    @IBOutlet weak var sepratorIndicatorConstraintTrailing: NSLayoutConstraint!
    @IBOutlet weak var sepratorIndicatorView: UIView!
    
    var isSepratorAvailable = false
    var selImgArray = [UIImage]()
    var selectedTilesNumber = Int()
    
    @IBOutlet weak var btnLeftRotate: UIButton!
    
    @IBOutlet weak var btnRightRotate: UIButton!
    
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(TwoVerticalViewController.singleTapGestureRecognizer(_:)))
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(TwoVerticalViewController.singleTapGestureRecognizer(_:)))
        
        tapGesture1.numberOfTapsRequired = 1
        tapGesture2.numberOfTapsRequired = 1
        
        leftView.addGestureRecognizer(tapGesture1)
        rightView.addGestureRecognizer(tapGesture2)
        
        
        
        
        self.setInitialConstraints()
        
        // Add duplicate images to array as shown on previous screen
        
        
        //Assign images to imageScrollView
        
        
        // add borders to collage background view
        collageBackgroundView.layer.borderWidth = collage_tiles_border_width
        collageBackgroundView.layer.borderColor = UIColor.clear.cgColor
        
        // add borders to collage tiles
        leftView.layer.borderWidth = collage_tiles_border_width
        leftView.layer.borderColor = UIColor.clear.cgColor
        
        
        rightView.layer.borderWidth = collage_tiles_border_width
        rightView.layer.borderColor = UIColor.clear.cgColor
        self.initLongPressGesture()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyMirrorEffect(_:)), name: NSNotification.Name(rawValue: "TwoVerticalVCMirror"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyFlipEffect(_:)), name: NSNotification.Name(rawValue: "TwoVerticalVCFlip"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyBorderEffect(_:)), name: NSNotification.Name(rawValue: "TwoVerticalVCBorder"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.saveCollageImage(_:)), name: NSNotification.Name(rawValue: "TwoVerticalVCSaveImg"), object: nil)
        
        self.addSepratorsToTiles()
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.setInitialConstraints()
            //            self.updateTilesFrame()
            //            self.removeSepratorFromTiles()
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        image_Scroll_Views_Array = [imageScrollViewLeft,imageScrollViewRight]
        if selImgArray.count < 2
        {
            selImgArray.append(selImgArray[0])
            selected_Image_Asset_Id_Array.append(selected_Image_Asset_Id_Array[0])
        }
        for var i in 0..<selImgArray.count {
            if i < image_Scroll_Views_Array.count{
                image_Scroll_Views_Array[i].zoomMode = .fill
                image_Scroll_Views_Array[i].image = selImgArray[i]
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    func initImages()  {
        // Do any additional setup after loading the view.
        
        //image_Scroll_Views_Array = [imageScrollViewLeft,imageScrollViewRight]
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(TwoVerticalViewController.singleTapGestureRecognizer(_:)))
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(TwoVerticalViewController.singleTapGestureRecognizer(_:)))
        
        tapGesture1.numberOfTapsRequired = 1
        tapGesture2.numberOfTapsRequired = 1
        
        leftView.addGestureRecognizer(tapGesture1)
        rightView.addGestureRecognizer(tapGesture2)
        
        
        
        
        self.setInitialConstraints()
        
        //Assign images to imageScrollView
        for var i in 0..<selImgArray.count {
            if i < image_Scroll_Views_Array.count{
                image_Scroll_Views_Array[i].zoomMode = .fill
                image_Scroll_Views_Array[i].image = selImgArray[i]
            }
        }
        
        // add borders to collage background view
        collageBackgroundView.layer.borderWidth = collage_tiles_border_width
        collageBackgroundView.layer.borderColor = UIColor.clear.cgColor
        
        // add borders to collage tiles
        leftView.layer.borderWidth = collage_tiles_border_width
        leftView.layer.borderColor = UIColor.clear.cgColor
        
        
        rightView.layer.borderWidth = collage_tiles_border_width
        rightView.layer.borderColor = UIColor.clear.cgColor
        self.initLongPressGesture()
        
        self.addSepratorsToTiles()
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.setInitialConstraints()
            self.updateTilesFrame()
            self.removeSepratorFromTiles()
        }
    }
    
    func initLongPressGesture()  {
        let longpress1 = UILongPressGestureRecognizer(target: self, action: #selector(TwoVerticalViewController.longPressGestureRecognized1(_:)))
        leftView.addGestureRecognizer(longpress1)
        
        let longpress2 = UILongPressGestureRecognizer(target: self, action: #selector(TwoVerticalViewController.longPressGestureRecognized2(_:)))
        rightView.addGestureRecognizer(longpress2)
        
        
    }
    
    func removeSepratorFromTiles()  {
        if seprator_Colour == UIColor.clear {
            self.updateTilesFrame()
            for (var view) in self.collageBackgroundView.subviews
            {
                
                if view is SeparatorView
                {
                    view.removeFromSuperview()
                }
                
            }
        }
    }
    
    func setInitialConstraints()  {
        
        /*
         if getCurrentIphone() == "6+"
         {
         tilesWidth = self.collageBackgroundView.frame.size.width / 1.8//2.2
         }
         else if getCurrentIphone() == "ipad"
         {
         tilesWidth = self.collageBackgroundView.frame.size.width / 2.4//3
         }
         else if getCurrentIphone() == "5"
         {
         tilesWidth = self.collageBackgroundView.frame.size.width / 2.4//3
         }
         else
         {
         tilesWidth = self.collageBackgroundView.frame.size.width / 2*640/980 // 1.26 /  2//2.2
         }
         */
        
        //let tilesHeight = self.collageBackgroundView.frame.size.height / 1.8
        
        self.rightViewConstraintWidth.constant = self.collageBackgroundView.frame.width / 2
        self.leftViewConstraintWidth.constant = self.collageBackgroundView.frame.width / 2
        
        
        
        
    }
    
    func updateTilesFrame()  {
        
        self.rightViewConstraintWidth.constant = self.rightView.frame.size.width
        self.leftViewConstraintWidth.constant = self.leftView.frame.size.width
        
        
        
        
    }
    
    func addSepratorsToTiles()  {
        
        //vertical seprators
        SeparatorView.addSeparatorBetweenViews(separatorType: .vertical, primaryView: leftView, secondaryView: rightView, parentView: self.collageBackgroundView)
        
        
        
    }
    
    @objc func longPressGestureRecognized1(_ gestureRecognizer: UIGestureRecognizer) {
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.location(in: self.collageBackgroundView)
        
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        
        
        switch state {
        case UIGestureRecognizerState.began:
            
            
            My.cellSnapshot  = self.snapshotOfCell(leftView)
            
            var center = leftView.center
            My.cellSnapshot!.center = center
            My.cellSnapshot!.alpha = 0.0
            self.collageBackgroundView.addSubview(My.cellSnapshot!)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellIsAnimating = true
                My.cellSnapshot!.center = center
                My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                My.cellSnapshot!.alpha = 0.98
                // cell?.alpha = 0.0
            }, completion: { (finished) -> Void in
                if finished {
                    My.cellIsAnimating = false
                    if My.cellNeedToShow {
                        My.cellNeedToShow = false
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            //cell?.alpha = 1
                        })
                    } else {
                        //cell?.isHidden = true
                    }
                }
            })
            
            
        case UIGestureRecognizerState.changed:
            print("change")
            
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellSnapshot!.center = center
                
                
                
                
                if My.cellSnapshot!.center.x > (self.rightView.center.x - (self.rightView.frame.width/3)) && My.cellSnapshot!.center.x < (self.rightView.center.x + (self.rightView.frame.width/3)) && My.cellSnapshot!.center.y > (self.rightView.center.y - (self.rightView.frame.height/3)) && My.cellSnapshot!.center.y < (self.rightView.center.y + (self.rightView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewRight.image
                    let img2 = self.imageScrollViewLeft.image
                    
                    self.imageScrollViewRight.image = img2
                    self.imageScrollViewLeft.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                
                
                
            }
            
        default:
            print("default")
            if My.cellSnapshot != nil {
                
                My.cellSnapshot!.removeFromSuperview()
                My.cellSnapshot = nil
            }
            
        }
        
    }
    
    @objc func longPressGestureRecognized2(_ gestureRecognizer: UIGestureRecognizer) {
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.location(in: self.collageBackgroundView)
        
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        
        
        switch state {
        case UIGestureRecognizerState.began:
            
            
            My.cellSnapshot  = self.snapshotOfCell(rightView)
            
            var center = rightView.center
            My.cellSnapshot!.center = center
            My.cellSnapshot!.alpha = 0.0
            self.collageBackgroundView.addSubview(My.cellSnapshot!)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellIsAnimating = true
                My.cellSnapshot!.center = center
                My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                My.cellSnapshot!.alpha = 0.98
                // cell?.alpha = 0.0
            }, completion: { (finished) -> Void in
                if finished {
                    My.cellIsAnimating = false
                    if My.cellNeedToShow {
                        My.cellNeedToShow = false
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            //cell?.alpha = 1
                        })
                    } else {
                        //cell?.isHidden = true
                    }
                }
            })
            
            
        case UIGestureRecognizerState.changed:
            print("change")
            
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellSnapshot!.center = center
                
                
                
                
                if My.cellSnapshot!.center.x > (self.leftView.center.x - (self.leftView.frame.width/3)) && My.cellSnapshot!.center.x < (self.leftView.center.x + (self.leftView.frame.width/3)) && My.cellSnapshot!.center.y > (self.leftView.center.y - (self.leftView.frame.height/3)) && My.cellSnapshot!.center.y < (self.leftView.center.y + (self.leftView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewRight.image
                    let img2 = self.imageScrollViewLeft.image
                    
                    self.imageScrollViewRight.image = img2
                    self.imageScrollViewLeft.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                
                
                
            }
            
        default:
            print("default")
            if My.cellSnapshot != nil {
                
                My.cellSnapshot!.removeFromSuperview()
                My.cellSnapshot = nil
            }
            
        }
        
    }
    
    func snapshotOfCell(_ inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    
    @objc func singleTapGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        var selectedViewTag = gestureRecognizer.view?.tag
        print("selectedViewTag = \(selectedViewTag)")
        selectedCollageFrameIndexInEdit = selectedViewTag! - 1
        self.addBorderToSelectedTils(tag:selectedViewTag!)
        selected_Collage_Index = selectedViewTag! - 1
        NotificationCenter.default.post(name: NSNotification.Name("ReloadReplaceImageCollectionView"), object: nil)
        /*
         if isSepratorAvailable {
         
         isSepratorAvailable = false
         self.updateTilesFrame()
         self.removeSepratorFromTiles()
         }else{
         isSepratorAvailable = true
         self.addSepratorsToTiles()
         }
         */
    }
    
    func addBorderToSelectedTils(tag:Int)  {
        switch tag {
            
        case 1:
            
            self.sepratorIndicatorConstraintTrailing.constant = 2
            if leftView.layer.borderColor == collage_tiles_border_colour{
                self.removeBorderOfTiles()
                self.removeSepratorFromTiles()
                leftView.layer.borderColor = UIColor.clear.cgColor
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":0])
                
            }else{
                self.removeBorderOfTiles()
                leftView.layer.borderColor = collage_tiles_border_colour
                
                sepratorIndicatorView.isHidden = false
                if seprator_Colour == UIColor.clear
                {
                    self.removeSepratorFromTiles()
                    self.addSepratorsToTiles()
                }
                self.selectedTilesNumber = 1
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":1])
                
                self.btnLeftRotate.isHidden = false
            }
            
        case 2:
            
            self.sepratorIndicatorConstraintTrailing.constant = 5
            
            if rightView.layer.borderColor == collage_tiles_border_colour{
                self.removeBorderOfTiles()
                self.removeSepratorFromTiles()
                rightView.layer.borderColor = UIColor.clear.cgColor
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":0])
                
            }else{
                self.removeBorderOfTiles()
                rightView.layer.borderColor = collage_tiles_border_colour
                sepratorIndicatorView.isHidden = false
                if seprator_Colour == UIColor.clear
                {
                    self.removeSepratorFromTiles()
                    self.addSepratorsToTiles()
                }
                self.selectedTilesNumber = 2
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":1])
                
                self.btnRightRotate.isHidden = false
            }
            
        default:
            print("error")
            self.selectedTilesNumber = 0
        }
    }
    
    func removeBorderOfTiles()  {
        sepratorIndicatorView.isHidden = true
        
        leftView.layer.borderColor = UIColor.clear.cgColor
        rightView.layer.borderColor = UIColor.clear.cgColor
        self.selectedTilesNumber = 0
        
        self.btnLeftRotate.isHidden = true
        self.btnRightRotate.isHidden = true
    }
    
    @objc func applyMirrorEffect(_ notification: NSNotification)
        
    {
        
        switch selectedTilesNumber {
        case 1:
            let img = self.imageScrollViewLeft.image
            
            let flippedImage = setEffectOn(image:img!, effect:"mirror")
            
            self.imageScrollViewLeft.image = flippedImage
            
        case 2:
            let img = self.imageScrollViewRight.image
            
            let flippedImage = setEffectOn(image:img!, effect:"mirror")
            
            self.imageScrollViewRight.image = flippedImage
            
        default:
            
            print("error")
        }
    }
    
    @objc func applyFlipEffect(_ notification: NSNotification)
        
    {
        
        switch selectedTilesNumber {
        case 1:
            
            
            let img = self.imageScrollViewLeft.image
            
            let flippedImage = setEffectOn(image:img!, effect:"flip")
            
            self.imageScrollViewLeft.image = flippedImage
            
        case 2:
            //self.imageScrollViewBottom.display(image: #imageLiteral(resourceName: "facebook_icon"))
            
            let img = self.imageScrollViewRight.image
            
            let flippedImage = setEffectOn(image:img!, effect:"flip")
            
            self.imageScrollViewRight.image = flippedImage
            
        default:
            
            print("error")
        }
    }
    
    @objc func applyBorderEffect(_ notification: NSNotification)
        
    {
        if seprator_Colour == UIColor.white{
            seprator_Colour = UIColor.clear
            collageBackgroundView.layer.borderColor = UIColor.clear.cgColor
            self.removeSepratorFromTiles()
            self.addSepratorsToTiles()
            
        }else{
            seprator_Colour = UIColor.white
            collageBackgroundView.layer.borderColor = UIColor.white.cgColor
            self.removeSepratorFromTiles()
            self.updateTilesFrame()
            for (var view) in self.collageBackgroundView.subviews
            {
                
                if view is SeparatorView
                {
                    view.removeFromSuperview()
                }
                
            }
            self.addSepratorsToTiles()
        }
        
    }
    
    @objc func saveCollageImage(_ notification: NSNotification)
    {
//        let saveCollageAnalyticsEvent = SaveCollageAnalyticsEvent(numOfPhotos: 2)
//        Copilot.instance.report.log(event: saveCollageAnalyticsEvent)
        self.updateTilesFrame()
        self.removeSepratorFromTiles()
        self.removeBorderOfTiles()
        //Crop  image
        
        let renderer = UIGraphicsImageRenderer(size: self.collageBackgroundView.bounds.size)
        let image = renderer.image { ctx in
            self.collageBackgroundView.drawHierarchy(in: self.collageBackgroundView.bounds, afterScreenUpdates: true)
        }
        
        if image != nil{
            
            collage_image = image
            
        }
    }
    
    // MARK: - Action Methods
    
    @IBAction func btnLeftRotateClicked(_ sender: Any)
    {
        let img = self.imageScrollViewLeft.image
        
        let flippedImage = setEffectOn(image:img!, effect:"rotate")
        
        self.imageScrollViewLeft.image = flippedImage
    }
    
    @IBAction func btnRightRotateClicked(_ sender: Any)
    {
        let img = self.imageScrollViewRight.image
        
        let flippedImage = setEffectOn(image:img!, effect:"rotate")
        
        self.imageScrollViewRight.image = flippedImage
    }
}
