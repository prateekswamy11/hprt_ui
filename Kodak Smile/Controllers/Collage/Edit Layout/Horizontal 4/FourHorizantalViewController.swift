//
//  FourHorizantalViewController.swift
//  Snaptouch_Polaroid
//
//  Created by maximess120 on 20/02/18.
//  Copyright © 2018 maximess142. All rights reserved.
//

import UIKit
//import CopilotAPIAccess

class FourHorizantalViewController: UIViewController {
    
    @IBOutlet weak var imageScrollViewBottomRight: ZoomImageView!
    @IBOutlet weak var imageScrollViewBottomLeft: ZoomImageView!
    @IBOutlet weak var imageScrollViewTopRight: ZoomImageView!
    @IBOutlet weak var imageScrollViewTopLeft: ZoomImageView!
    @IBOutlet weak var collageBackgroundView: UIView!
    
    @IBOutlet weak var topRightView: UIView!
    @IBOutlet weak var topLeftView: UIView!
    @IBOutlet weak var bottomLeftView: UIView!
    @IBOutlet weak var bottomRightView: UIView!
    
    @IBOutlet weak var topRightViewConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var topRightViewConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var topLeftViewConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var topLeftViewConstraintHeight: NSLayoutConstraint!
    
    @IBOutlet weak var bottomLeftViewConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var bottomLeftViewConstraintHeight: NSLayoutConstraint!
    
    @IBOutlet weak var bottomRightViewConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var bottomRightViewConstraintHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var sepratorTopIndicatorConstraintTrailing: NSLayoutConstraint!
    @IBOutlet weak var sepratorTopIndicatorView: UIView!
    
    @IBOutlet weak var sepratorBottomIndicatorConstraintTrailing: NSLayoutConstraint!
    @IBOutlet weak var sepratorBottomIndicatorView: UIView!
    
    @IBOutlet weak var sepratorLeftIndicatorConstraintBottom: NSLayoutConstraint!
    @IBOutlet weak var sepratorLeftIndicatorView: UIView!
    
    @IBOutlet weak var sepratorRightIndicatorConstraintBottom: NSLayoutConstraint!
    @IBOutlet weak var sepratorRightIndicatorView: UIView!
    
    
    var isSepratorAvailable = false
    var selImgArray = [UIImage]()
    var selectedTilesNumber = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        image_Scroll_Views_Array = [imageScrollViewTopLeft,
                                    imageScrollViewTopRight,
                                    imageScrollViewBottomLeft,
                                    imageScrollViewBottomRight]
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(FourVerticalViewController.singleTapGestureRecognizer(_:)))
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(FourVerticalViewController.singleTapGestureRecognizer(_:)))
        let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(FourVerticalViewController.singleTapGestureRecognizer(_:)))
        
        let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(FourVerticalViewController.singleTapGestureRecognizer(_:)))
        
        tapGesture1.numberOfTapsRequired = 1
        tapGesture2.numberOfTapsRequired = 1
        tapGesture3.numberOfTapsRequired = 1
        tapGesture4.numberOfTapsRequired = 1
        
        topLeftView.addGestureRecognizer(tapGesture1)
        topRightView.addGestureRecognizer(tapGesture2)
        bottomLeftView.addGestureRecognizer(tapGesture3)
        bottomRightView.addGestureRecognizer(tapGesture4)
        
        // Do any additional setup after loading the view.
        
        self.setInitialConstraints()
        
        //Assign images to imageScrollView
        
        for var i in 0..<selImgArray.count {
            if i < image_Scroll_Views_Array.count{
                image_Scroll_Views_Array[i].zoomMode = .fill
                image_Scroll_Views_Array[i].image = selImgArray[i]
            }
        }
        
        //top separator
        sepratorTopIndicatorView.layer.cornerRadius = separator_indicator_corner_radius
        sepratorTopIndicatorView.layer.borderWidth = 0.5
        sepratorTopIndicatorView.layer.borderColor = UIColor.lightGray.cgColor
        
        //bottom separator
        sepratorBottomIndicatorView.layer.cornerRadius = separator_indicator_corner_radius
        sepratorBottomIndicatorView.layer.borderWidth = 0.5
        sepratorBottomIndicatorView.layer.borderColor = UIColor.lightGray.cgColor
        
        //left separator
        sepratorLeftIndicatorView.layer.cornerRadius = separator_indicator_corner_radius
        sepratorLeftIndicatorView.layer.borderWidth = 0.5
        sepratorLeftIndicatorView.layer.borderColor = UIColor.lightGray.cgColor
        
        //Right separator
        sepratorRightIndicatorView.layer.cornerRadius = separator_indicator_corner_radius
        sepratorRightIndicatorView.layer.borderWidth = 0.5
        sepratorRightIndicatorView.layer.borderColor = UIColor.lightGray.cgColor
        
        // add borders to collage background view
        collageBackgroundView.layer.borderWidth = collage_tiles_border_width
        collageBackgroundView.layer.borderColor = UIColor.clear.cgColor
        
        // add borders to collage tiles
        topLeftView.layer.borderWidth = collage_tiles_border_width
        topLeftView.layer.borderColor = UIColor.clear.cgColor
        
        topRightView.layer.borderWidth = collage_tiles_border_width
        topRightView.layer.borderColor = UIColor.clear.cgColor
        
        bottomLeftView.layer.borderWidth = collage_tiles_border_width
        bottomLeftView.layer.borderColor = UIColor.clear.cgColor
        
        bottomRightView.layer.borderWidth = collage_tiles_border_width
        bottomRightView.layer.borderColor = UIColor.clear.cgColor
        
        self.initLongPressGesture()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyMirrorEffect(_:)), name: NSNotification.Name(rawValue: "FourHorizantalVCMirror"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyFlipEffect(_:)), name: NSNotification.Name(rawValue: "FourHorizantalVCFlip"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyBorderEffect(_:)), name: NSNotification.Name(rawValue: "FourHorizantalVCBorder"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.saveCollageImage(_:)), name: NSNotification.Name(rawValue: "FourHorizantalVCSaveImg"), object: nil)
        self.addSepratorsToTiles()
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.setInitialConstraints()
            self.updateTilesFrame()
            self.removeSepratorFromTiles()
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func initLongPressGesture()  {
        let longpress1 = UILongPressGestureRecognizer(target: self, action: #selector(FourHorizantalViewController.longPressGestureRecognized1(_:)))
        topLeftView.addGestureRecognizer(longpress1)
        
        let longpress2 = UILongPressGestureRecognizer(target: self, action: #selector(FourHorizantalViewController.longPressGestureRecognized2(_:)))
        topRightView.addGestureRecognizer(longpress2)
        
        let longpress3 = UILongPressGestureRecognizer(target: self, action: #selector(FourHorizantalViewController.longPressGestureRecognized3(_:)))
        bottomLeftView.addGestureRecognizer(longpress3)
        
        let longpress4 = UILongPressGestureRecognizer(target: self, action: #selector(FourHorizantalViewController.longPressGestureRecognized4(_:)))
        bottomRightView.addGestureRecognizer(longpress4)
    }
    
    func removeSepratorFromTiles()  {
        if seprator_Colour == UIColor.clear {
            self.updateTilesFrame()
            for (var view) in self.collageBackgroundView.subviews
            {
                
                if view is SeparatorView
                {
                    view.removeFromSuperview()
                }
                
            }
        }
    }
    
    func setInitialConstraints()  {
        
        var tilesWidth = CGFloat()
        var tilesHeight = CGFloat()
        
        if getCurrentIphone() == "6+"
        {
            tilesWidth = self.collageBackgroundView.frame.size.width / 1.8//2.4
            
            tilesHeight = self.collageBackgroundView.frame.size.height / 2.3//2.8
        }
        else{
            tilesWidth = self.collageBackgroundView.frame.size.width / 2//2.4
            
            tilesHeight = self.collageBackgroundView.frame.size.height / 2.6//2.8
        }
        
        
        self.topRightViewConstraintWidth.constant = tilesWidth
        self.topRightViewConstraintHeight.constant = tilesHeight
        
        
        self.topLeftViewConstraintWidth.constant = tilesWidth
        self.topLeftViewConstraintHeight.constant = tilesHeight
        
        self.bottomLeftViewConstraintWidth.constant = tilesWidth
        self.bottomLeftViewConstraintHeight.constant = tilesHeight
        
        self.bottomRightViewConstraintWidth.constant = tilesWidth
        self.bottomRightViewConstraintHeight.constant = tilesHeight
        
    }
    
    func updateTilesFrame()  {
        
        self.topRightViewConstraintWidth.constant = self.topRightView.frame.size.width
        self.topRightViewConstraintHeight.constant = self.topRightView.frame.size.height
        
        
        self.topLeftViewConstraintWidth.constant = self.topLeftView.frame.size.width
        self.topLeftViewConstraintHeight.constant = self.topLeftView.frame.size.height
        
        self.bottomLeftViewConstraintWidth.constant = self.bottomLeftView.frame.size.width
        self.bottomLeftViewConstraintHeight.constant = self.bottomLeftView.frame.size.height
        
        self.bottomRightViewConstraintWidth.constant = self.bottomRightView.frame.size.width
        self.bottomRightViewConstraintHeight.constant = self.bottomRightView.frame.size.height
        
        
    }
    
    func addSepratorsToTiles()  {
        
        //vertical seprators
        SeparatorView.addSeparatorBetweenViews(separatorType: .vertical, primaryView: topLeftView, secondaryView: topRightView, parentView: self.collageBackgroundView)
        
        
        
        //Horizantal seprators
        SeparatorView.addSeparatorBetweenViews(separatorType: .horizontal, primaryView: topLeftView, secondaryView: bottomLeftView, parentView: self.collageBackgroundView)
        
        
        
    }
    
    @objc func longPressGestureRecognized1(_ gestureRecognizer: UIGestureRecognizer) {
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.location(in: self.collageBackgroundView)
        
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        
        
        switch state {
        case UIGestureRecognizerState.began:
            
            
            My.cellSnapshot  = self.snapshotOfCell(topLeftView)
            
            var center = topLeftView.center
            My.cellSnapshot!.center = center
            My.cellSnapshot!.alpha = 0.0
            self.collageBackgroundView.addSubview(My.cellSnapshot!)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellIsAnimating = true
                My.cellSnapshot!.center = center
                My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                My.cellSnapshot!.alpha = 0.98
                // cell?.alpha = 0.0
            }, completion: { (finished) -> Void in
                if finished {
                    My.cellIsAnimating = false
                    if My.cellNeedToShow {
                        My.cellNeedToShow = false
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            //cell?.alpha = 1
                        })
                    } else {
                        //cell?.isHidden = true
                    }
                }
            })
            
            
        case UIGestureRecognizerState.changed:
            print("change")
            
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellSnapshot!.center = center
                
                
                if My.cellSnapshot!.center.x > (self.topRightView.center.x - (self.topRightView.frame.width/3)) && My.cellSnapshot!.center.x < (self.topRightView.center.x + (self.topRightView.frame.width/3)) && My.cellSnapshot!.center.y > (self.topRightView.center.y - (self.topRightView.frame.height/3)) && My.cellSnapshot!.center.y < (self.topRightView.center.y + (self.topRightView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewTopRight.image
                    let img2 = self.imageScrollViewTopLeft.image
                    
                    self.imageScrollViewTopRight.image = img2
                    
                    self.imageScrollViewTopLeft.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                    
                else if My.cellSnapshot!.center.x > (self.bottomLeftView.center.x - (self.bottomLeftView.frame.width/3)) && My.cellSnapshot!.center.x < (self.bottomLeftView.center.x + (self.bottomLeftView.frame.width/3)) && My.cellSnapshot!.center.y > (self.bottomLeftView.center.y - (self.bottomLeftView.frame.height/3)) && My.cellSnapshot!.center.y < (self.bottomLeftView.center.y + (self.bottomLeftView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewBottomLeft.image
                    let img2 = self.imageScrollViewTopLeft.image
                    
                    self.imageScrollViewBottomLeft.image = img2
                    self.imageScrollViewTopLeft.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                    
                else if My.cellSnapshot!.center.x > (self.bottomRightView.center.x - (self.bottomRightView.frame.width/3)) && My.cellSnapshot!.center.x < (self.bottomRightView.center.x + (self.bottomRightView.frame.width/3)) && My.cellSnapshot!.center.y > (self.bottomRightView.center.y - (self.bottomRightView.frame.height/3)) && My.cellSnapshot!.center.y < (self.bottomRightView.center.y + (self.bottomRightView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewBottomRight.image
                    let img2 = self.imageScrollViewTopLeft.image
                    
                    self.imageScrollViewBottomRight.image = img2
                    self.imageScrollViewTopLeft.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                
            }
            
        default:
            print("default")
            if My.cellSnapshot != nil {
                
                My.cellSnapshot!.removeFromSuperview()
                My.cellSnapshot = nil
            }
            
        }
        
    }
    
    @objc func longPressGestureRecognized2(_ gestureRecognizer: UIGestureRecognizer) {
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.location(in: self.collageBackgroundView)
        
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        
        
        switch state {
        case UIGestureRecognizerState.began:
            
            
            My.cellSnapshot  = self.snapshotOfCell(topRightView)
            
            var center = topRightView.center
            My.cellSnapshot!.center = center
            My.cellSnapshot!.alpha = 0.0
            self.collageBackgroundView.addSubview(My.cellSnapshot!)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellIsAnimating = true
                My.cellSnapshot!.center = center
                My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                My.cellSnapshot!.alpha = 0.98
                // cell?.alpha = 0.0
            }, completion: { (finished) -> Void in
                if finished {
                    My.cellIsAnimating = false
                    if My.cellNeedToShow {
                        My.cellNeedToShow = false
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            //cell?.alpha = 1
                        })
                    } else {
                        //cell?.isHidden = true
                    }
                }
            })
            
            
        case UIGestureRecognizerState.changed:
            print("change")
            
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellSnapshot!.center = center
                
                
                if My.cellSnapshot!.center.x > (self.topLeftView.center.x - (self.topLeftView.frame.width/3)) && My.cellSnapshot!.center.x < (self.topLeftView.center.x + (self.topLeftView.frame.width/3)) && My.cellSnapshot!.center.y > (self.topLeftView.center.y - (self.topLeftView.frame.height/3)) && My.cellSnapshot!.center.y < (self.topLeftView.center.y + (self.topLeftView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewTopRight.image
                    let img2 = self.imageScrollViewTopLeft.image
                    
                    self.imageScrollViewTopRight.image = img2
                    
                    self.imageScrollViewTopLeft.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                    
                else if My.cellSnapshot!.center.x > (self.bottomLeftView.center.x - (self.bottomLeftView.frame.width/3)) && My.cellSnapshot!.center.x < (self.bottomLeftView.center.x + (self.bottomLeftView.frame.width/3)) && My.cellSnapshot!.center.y > (self.bottomLeftView.center.y - (self.bottomLeftView.frame.height/3)) && My.cellSnapshot!.center.y < (self.bottomLeftView.center.y + (self.bottomLeftView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewBottomLeft.image
                    let img2 = self.imageScrollViewTopRight.image
                    
                    self.imageScrollViewBottomLeft.image = img2
                    self.imageScrollViewTopRight.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                    
                else if My.cellSnapshot!.center.x > (self.bottomRightView.center.x - (self.bottomRightView.frame.width/3)) && My.cellSnapshot!.center.x < (self.bottomRightView.center.x + (self.bottomRightView.frame.width/3)) && My.cellSnapshot!.center.y > (self.bottomRightView.center.y - (self.bottomRightView.frame.height/3)) && My.cellSnapshot!.center.y < (self.bottomRightView.center.y + (self.bottomRightView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewBottomRight.image
                    let img2 = self.imageScrollViewTopRight.image
                    
                    self.imageScrollViewBottomRight.image = img2
                    self.imageScrollViewTopRight.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                
            }
            
        default:
            print("default")
            if My.cellSnapshot != nil {
                
                My.cellSnapshot!.removeFromSuperview()
                My.cellSnapshot = nil
            }
            
        }
        
    }
    
    @objc func longPressGestureRecognized3(_ gestureRecognizer: UIGestureRecognizer) {
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.location(in: self.collageBackgroundView)
        
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        
        
        switch state {
        case UIGestureRecognizerState.began:
            
            
            My.cellSnapshot  = self.snapshotOfCell(bottomLeftView)
            
            var center = bottomLeftView.center
            My.cellSnapshot!.center = center
            My.cellSnapshot!.alpha = 0.0
            self.collageBackgroundView.addSubview(My.cellSnapshot!)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellIsAnimating = true
                My.cellSnapshot!.center = center
                My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                My.cellSnapshot!.alpha = 0.98
                // cell?.alpha = 0.0
            }, completion: { (finished) -> Void in
                if finished {
                    My.cellIsAnimating = false
                    if My.cellNeedToShow {
                        My.cellNeedToShow = false
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            //cell?.alpha = 1
                        })
                    } else {
                        //cell?.isHidden = true
                    }
                }
            })
            
            
        case UIGestureRecognizerState.changed:
            print("change")
            
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellSnapshot!.center = center
                
                
                if My.cellSnapshot!.center.x > (self.topLeftView.center.x - (self.topLeftView.frame.width/3)) && My.cellSnapshot!.center.x < (self.topLeftView.center.x + (self.topLeftView.frame.width/3)) && My.cellSnapshot!.center.y > (self.topLeftView.center.y - (self.topLeftView.frame.height/3)) && My.cellSnapshot!.center.y < (self.topLeftView.center.y + (self.topLeftView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewBottomLeft.image
                    let img2 = self.imageScrollViewTopLeft.image
                    
                    self.imageScrollViewBottomLeft.image = img2
                    
                    self.imageScrollViewTopLeft.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                    
                else if My.cellSnapshot!.center.x > (self.topRightView.center.x - (self.topRightView.frame.width/3)) && My.cellSnapshot!.center.x < (self.topRightView.center.x + (self.topRightView.frame.width/3)) && My.cellSnapshot!.center.y > (self.topRightView.center.y - (self.topRightView.frame.height/3)) && My.cellSnapshot!.center.y < (self.topRightView.center.y + (self.topRightView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewBottomLeft.image
                    let img2 = self.imageScrollViewTopRight.image
                    
                    self.imageScrollViewBottomLeft.image = img2
                    self.imageScrollViewTopRight.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                    
                else if My.cellSnapshot!.center.x > (self.bottomRightView.center.x - (self.bottomRightView.frame.width/3)) && My.cellSnapshot!.center.x < (self.bottomRightView.center.x + (self.bottomRightView.frame.width/3)) && My.cellSnapshot!.center.y > (self.bottomRightView.center.y - (self.bottomRightView.frame.height/3)) && My.cellSnapshot!.center.y < (self.bottomRightView.center.y + (self.bottomRightView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewBottomRight.image
                    let img2 = self.imageScrollViewBottomLeft.image
                    
                    self.imageScrollViewBottomRight.image = img2
                    self.imageScrollViewBottomLeft.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                
            }
            
        default:
            print("default")
            if My.cellSnapshot != nil {
                
                My.cellSnapshot!.removeFromSuperview()
                My.cellSnapshot = nil
            }
            
        }
        
    }
    
    
    @objc func longPressGestureRecognized4(_ gestureRecognizer: UIGestureRecognizer) {
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.location(in: self.collageBackgroundView)
        
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        
        
        switch state {
        case UIGestureRecognizerState.began:
            
            
            My.cellSnapshot  = self.snapshotOfCell(bottomRightView)
            
            var center = bottomRightView.center
            My.cellSnapshot!.center = center
            My.cellSnapshot!.alpha = 0.0
            self.collageBackgroundView.addSubview(My.cellSnapshot!)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellIsAnimating = true
                My.cellSnapshot!.center = center
                My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                My.cellSnapshot!.alpha = 0.98
                // cell?.alpha = 0.0
            }, completion: { (finished) -> Void in
                if finished {
                    My.cellIsAnimating = false
                    if My.cellNeedToShow {
                        My.cellNeedToShow = false
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            //cell?.alpha = 1
                        })
                    } else {
                        //cell?.isHidden = true
                    }
                }
            })
            
            
        case UIGestureRecognizerState.changed:
            print("change")
            
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                My.cellSnapshot!.center = center
                
                
                if My.cellSnapshot!.center.x > (self.topLeftView.center.x - (self.topLeftView.frame.width/3)) && My.cellSnapshot!.center.x < (self.topLeftView.center.x + (self.topLeftView.frame.width/3)) && My.cellSnapshot!.center.y > (self.topLeftView.center.y - (self.topLeftView.frame.height/3)) && My.cellSnapshot!.center.y < (self.topLeftView.center.y + (self.topLeftView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewBottomRight.image
                    let img2 = self.imageScrollViewTopLeft.image
                    
                    self.imageScrollViewBottomRight.image = img2
                    
                    self.imageScrollViewTopLeft.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                    
                else if My.cellSnapshot!.center.x > (self.bottomLeftView.center.x - (self.bottomLeftView.frame.width/3)) && My.cellSnapshot!.center.x < (self.bottomLeftView.center.x + (self.bottomLeftView.frame.width/3)) && My.cellSnapshot!.center.y > (self.bottomLeftView.center.y - (self.bottomLeftView.frame.height/3)) && My.cellSnapshot!.center.y < (self.bottomLeftView.center.y + (self.bottomLeftView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewBottomLeft.image
                    let img2 = self.imageScrollViewBottomRight.image
                    
                    self.imageScrollViewBottomLeft.image = img2
                    self.imageScrollViewBottomRight.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                    
                else if My.cellSnapshot!.center.x > (self.topRightView.center.x - (self.topRightView.frame.width/3)) && My.cellSnapshot!.center.x < (self.topRightView.center.x + (self.topRightView.frame.width/3)) && My.cellSnapshot!.center.y > (self.topRightView.center.y - (self.topRightView.frame.height/3)) && My.cellSnapshot!.center.y < (self.topRightView.center.y + (self.topRightView.frame.height/3))  {
                    
                    let img1 = self.imageScrollViewBottomRight.image
                    let img2 = self.imageScrollViewTopRight.image
                    
                    self.imageScrollViewBottomRight.image = img2
                    self.imageScrollViewTopRight.image = img1
                    
                    
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
                
            }
            
        default:
            print("default")
            if My.cellSnapshot != nil {
                
                My.cellSnapshot!.removeFromSuperview()
                My.cellSnapshot = nil
            }
            
        }
        
    }
    
    
    func snapshotOfCell(_ inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    
    @objc func singleTapGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        
        var selectedViewTag = gestureRecognizer.view?.tag
        print("selectedViewTag = \(selectedViewTag)")
        selectedCollageFrameIndexInEdit = selectedViewTag! - 1
        self.addBorderToSelectedTils(tag:selectedViewTag!)
        selected_Collage_Index = selectedViewTag! - 1
        NotificationCenter.default.post(name: NSNotification.Name("ReloadReplaceImageCollectionView"), object: nil)
        /*
         if isSepratorAvailable {
         
         isSepratorAvailable = false
         self.updateTilesFrame()
         self.removeSepratorFromTiles()
         }else{
         isSepratorAvailable = true
         self.addSepratorsToTiles()
         }
         */
    }
    
    func addBorderToSelectedTils(tag:Int)  {
        switch tag {
            
        case 1:
            
            self.sepratorTopIndicatorConstraintTrailing.constant = 2
            self.sepratorLeftIndicatorConstraintBottom.constant = 2
            
            if topLeftView.layer.borderColor == collage_tiles_border_colour{
                self.removeBorderOfTiles()
                self.removeSepratorFromTiles()
                topLeftView.layer.borderColor = UIColor.clear.cgColor
                
                
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":0])
                
            }else{
                self.removeBorderOfTiles()
                topLeftView.layer.borderColor = collage_tiles_border_colour
                
                sepratorTopIndicatorView.isHidden = false
                sepratorLeftIndicatorView.isHidden = false
                if seprator_Colour == UIColor.clear
                {
                    self.removeSepratorFromTiles()
                    self.addSepratorsToTiles()
                }
                self.selectedTilesNumber = 1
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":1])
            }
            
        case 2:
            
            self.sepratorTopIndicatorConstraintTrailing.constant = 5
            self.sepratorRightIndicatorConstraintBottom.constant = 2
            
            if topRightView.layer.borderColor == collage_tiles_border_colour{
                self.removeBorderOfTiles()
                self.removeSepratorFromTiles()
                topRightView.layer.borderColor = UIColor.clear.cgColor
                
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":0])
                
            }else{
                self.removeBorderOfTiles()
                topRightView.layer.borderColor = collage_tiles_border_colour
                sepratorTopIndicatorView.isHidden = false
                sepratorRightIndicatorView.isHidden = false
                if seprator_Colour == UIColor.clear
                {
                    self.removeSepratorFromTiles()
                    self.addSepratorsToTiles()
                }
                self.selectedTilesNumber = 2
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":1])
            }
            
        case 3:
            
            self.sepratorLeftIndicatorConstraintBottom.constant = 5
            self.sepratorBottomIndicatorConstraintTrailing.constant = 2
            
            if bottomLeftView.layer.borderColor == collage_tiles_border_colour{
                self.removeBorderOfTiles()
                self.removeSepratorFromTiles()
                bottomLeftView.layer.borderColor = UIColor.clear.cgColor
                
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":0])
                
            }else{
                self.removeBorderOfTiles()
                bottomLeftView.layer.borderColor = collage_tiles_border_colour
                sepratorBottomIndicatorView.isHidden = false
                sepratorLeftIndicatorView.isHidden = false
                if seprator_Colour == UIColor.clear
                {
                    self.removeSepratorFromTiles()
                    self.addSepratorsToTiles()
                }
                self.selectedTilesNumber = 3
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":1])
            }
            
        case 4:
            
            self.sepratorBottomIndicatorConstraintTrailing.constant = 5
            self.sepratorRightIndicatorConstraintBottom.constant = 5
            
            if bottomRightView.layer.borderColor == collage_tiles_border_colour{
                self.removeBorderOfTiles()
                self.removeSepratorFromTiles()
                bottomRightView.layer.borderColor = UIColor.clear.cgColor
                
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":0])
                
            }else{
                self.removeBorderOfTiles()
                bottomRightView.layer.borderColor = collage_tiles_border_colour
                sepratorBottomIndicatorView.isHidden = false
                sepratorRightIndicatorView.isHidden = false
                if seprator_Colour == UIColor.clear
                {
                    self.removeSepratorFromTiles()
                    self.addSepratorsToTiles()
                }
                self.selectedTilesNumber = 4
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditCollageVcCollagestileSelect"), object: ["selected":1])
            }
            
        default:
            print("error")
        }
    }
    
    func removeBorderOfTiles()  {
        sepratorTopIndicatorView.isHidden = true
        sepratorBottomIndicatorView.isHidden = true
        sepratorLeftIndicatorView.isHidden = true
        sepratorRightIndicatorView.isHidden = true
        
        topLeftView.layer.borderColor = UIColor.clear.cgColor
        topRightView.layer.borderColor = UIColor.clear.cgColor
        bottomLeftView.layer.borderColor = UIColor.clear.cgColor
        bottomRightView.layer.borderColor = UIColor.clear.cgColor
        self.selectedTilesNumber = 0
    }
    
    @objc func applyMirrorEffect(_ notification: NSNotification)
        
    {
        
        switch selectedTilesNumber {
        case 1:
            let img = self.imageScrollViewTopLeft.image
            
            let flippedImage = setEffectOn(image:img!, effect:"mirror")
            
            self.imageScrollViewTopLeft.image = flippedImage
            
        case 2:
            let img = self.imageScrollViewTopRight.image
            
            let flippedImage = setEffectOn(image:img!, effect:"mirror")
            
            self.imageScrollViewTopRight.image = flippedImage
            
        case 3:
            let img = self.imageScrollViewBottomLeft.image
            
            let flippedImage = setEffectOn(image:img!, effect:"mirror")
            
            self.imageScrollViewBottomLeft.image = flippedImage
            
        case 4:
            let img = self.imageScrollViewBottomRight.image
            
            let flippedImage = setEffectOn(image:img!, effect:"mirror")
            
            self.imageScrollViewBottomRight.image = flippedImage
            
        default:
            
            print("error")
        }
    }
    
    @objc func applyFlipEffect(_ notification: NSNotification)
        
    {
        
        switch selectedTilesNumber {
        case 1:
            
            
            let img = self.imageScrollViewTopLeft.image
            
            let flippedImage = setEffectOn(image:img!, effect:"flip")
            
            self.imageScrollViewTopLeft.image = flippedImage
            
        case 2:
            //self.imageScrollViewBottom.display(image: #imageLiteral(resourceName: "facebook_icon"))
            
            let img = self.imageScrollViewTopRight.image
            
            let flippedImage = setEffectOn(image:img!, effect:"flip")
            
            self.imageScrollViewTopRight.image = flippedImage
            
        case 3:
            //self.imageScrollViewBottom.display(image: #imageLiteral(resourceName: "facebook_icon"))
            
            let img = self.imageScrollViewBottomLeft.image
            
            let flippedImage = setEffectOn(image:img!, effect:"flip")
            
            self.imageScrollViewBottomLeft.image = flippedImage
            
        case 4:
            //self.imageScrollViewBottom.display(image: #imageLiteral(resourceName: "facebook_icon"))
            
            let img = self.imageScrollViewBottomRight.image
            
            let flippedImage = setEffectOn(image:img!, effect:"flip")
            
            self.imageScrollViewBottomRight.image = flippedImage
            
        default:
            
            print("error")
        }
    }
    
    @objc func applyBorderEffect(_ notification: NSNotification)
        
    {
        
        
        if seprator_Colour == UIColor.white{
            seprator_Colour = UIColor.clear
            collageBackgroundView.layer.borderColor = UIColor.clear.cgColor
            self.removeSepratorFromTiles()
            self.addSepratorsToTiles()
            
        }else{
            seprator_Colour = UIColor.white
            collageBackgroundView.layer.borderColor = UIColor.white.cgColor
            self.removeSepratorFromTiles()
            self.updateTilesFrame()
            for (var view) in self.collageBackgroundView.subviews
            {
                
                if view is SeparatorView
                {
                    view.removeFromSuperview()
                }
                
            }
            self.addSepratorsToTiles()
        }
        
    }
    
    @objc func saveCollageImage(_ notification: NSNotification)
    {
//        let saveCollageAnalyticsEvent = SaveCollageAnalyticsEvent(numOfPhotos: 4)
//        Copilot.instance.report.log(event: saveCollageAnalyticsEvent)
        self.updateTilesFrame()
        self.removeSepratorFromTiles()
        self.removeBorderOfTiles()
        //Crop  image
        
        let renderer = UIGraphicsImageRenderer(size: self.collageBackgroundView.bounds.size)
        let image = renderer.image { ctx in
            self.collageBackgroundView.drawHierarchy(in: self.collageBackgroundView.bounds, afterScreenUpdates: true)
        }
        
        if image != nil{
            
            collage_image = image
            
        }
    }
    
    
    
}
