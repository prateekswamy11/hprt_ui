//
//  SelectLayoutCollectionDatasource.swift
//  KODAK DOCK
//
//  Created by MAXIMESS152 on 14/01/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation
import UIKit
import Photos
import Alamofire
import AlamofireImage

extension SelectLayoutViewController : UICollectionViewDelegate, UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        switch collectionView {
        case collectionViewGrid:
            if self.imagesFrom == "Local" {
                if self.filteredArray.isEmpty {
                    return imagesArray.count
                }else {
                    return self.filteredArray.count
                }
                
            }
            else if self.headerName == "Dropbox" {
                return self.dropboxImages.count
            }
            else
            {
                return self.socialImages.count
            }
            
        case frameCollectionView:
            return 4
            
        default:
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        switch collectionView
        {
        case collectionViewGrid:
            let cell : GalleryGridCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryGridCollectionViewCell", for: indexPath) as! GalleryGridCollectionViewCell
            
            var imageURL = ""
            if self.imagesFrom == "Local"
            {
                if self.filteredArray.isEmpty{
                    let asset = getSelectedImageAsset(index: indexPath.item)
                    cell.representedAssetIdentifier = asset.localIdentifier
                    imageManager.requestImage(for: asset, targetSize: thumbnailSize, contentMode: .aspectFill, options: cellImgoptions, resultHandler: { image, _ in
                        // The cell may have been recycled by the time this handler gets called;
                        // set the cell's thumbnail image only if it's still showing the same asset.
                        if cell.representedAssetIdentifier == asset.localIdentifier && image != nil {
                            cell.imageViewThumbnail.image = image
                            imageURL = asset.localIdentifier
                        }
                    })
                }else {
                    cell.data = self.filteredArray[indexPath.item]
                    cell.representedAssetIdentifier = self.filteredArray[indexPath.item]
                    imageURL = self.filteredArray[indexPath.item]
                }
            }
            else
            {
                let url = self.socialImages[indexPath.row]
                imageURL = url
                cell.imageViewThumbnail.af_setImage(
                    withURL: URL(string:url)!,
                    imageTransition: .crossDissolve(0.2),
                    completion: nil)
            }
            
            if selAssetsIdentifiersArray.contains(imageURL)
            {
                cell.showSelectedCell()
                //                collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .top)
                //                collectionView.delegate?.collectionView!(collectionView, didSelectItemAt: indexPath)
            }
            else
            {
                cell.showNormalCell()
            }
            
            return cell
            
        case frameCollectionView:
            
            switch indexPath.item
            {
            case 0:
                let cell : LayoutOneCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "LayoutOneCollectionViewCell", for: indexPath) as! LayoutOneCollectionViewCell
                
                if selAssetsIdentifiersArray.count > 1
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: 0)
                    cell.imageViewSelected1 = returnImageFromSocialURL(imageView: cell.imageViewSelected1, index: 0)
                    
                    cell.imageViewNormal2 = returnImageFromSocialURL(imageView: cell.imageViewNormal2, index: 1)
                    cell.imageViewSelected2 = returnImageFromSocialURL(imageView: cell.imageViewSelected2, index: 1)
                }
                else if selAssetsIdentifiersArray.count > 0
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: 0)
                    cell.imageViewSelected1 = returnImageFromSocialURL(imageView: cell.imageViewSelected1, index: 0)
                    
                    cell.imageViewNormal2 = returnImageFromSocialURL(imageView: cell.imageViewNormal2, index: 0)
                    cell.imageViewSelected2 = returnImageFromSocialURL(imageView: cell.imageViewSelected2, index: 0)
                }
                else if selAssetsIdentifiersArray.count == 0
                {
                    cell.imageViewNormal1.image = nil
                    cell.imageViewSelected1.image = nil
                    
                    cell.imageViewNormal2.image = nil
                    cell.imageViewSelected2.image = nil
                }
                
                if selectedCollageLayoutIndex == indexPath.item
                {
                    cell.showSelectedCell()
                }
                else
                {
                    cell.showNormalCell()
                }
                
                return cell
                
            case 1:
                let cell : LayoutTwoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "LayoutTwoCollectionViewCell", for: indexPath) as! LayoutTwoCollectionViewCell
                
                if selAssetsIdentifiersArray.count > 1
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: 0)
                    cell.imageViewSelected1 = returnImageFromSocialURL(imageView: cell.imageViewSelected1, index: 0)
                    
                    cell.imageViewNormal2 = returnImageFromSocialURL(imageView: cell.imageViewNormal2, index: 1)
                    cell.imageViewSelected2 = returnImageFromSocialURL(imageView: cell.imageViewSelected2, index: 1)
                }
                else if selAssetsIdentifiersArray.count > 0
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: 0)
                    cell.imageViewSelected1 = returnImageFromSocialURL(imageView: cell.imageViewSelected1, index: 0)
                    
                    cell.imageViewNormal2 = returnImageFromSocialURL(imageView: cell.imageViewNormal2, index: 0)
                    cell.imageViewSelected2 = returnImageFromSocialURL(imageView: cell.imageViewSelected2, index: 0)
                }
                else if selAssetsIdentifiersArray.count == 0
                {
                    cell.imageViewNormal1.image = nil
                    cell.imageViewSelected1.image = nil
                    
                    cell.imageViewNormal2.image = nil
                    cell.imageViewSelected2.image = nil
                }
                
                if selectedCollageLayoutIndex == indexPath.item
                {
                    cell.showSelectedCell()
                }
                else
                {
                    cell.showNormalCell()
                }
                
                return cell
                
            case 2:
                let cell : LayoutThreeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "LayoutThreeCollectionViewCell", for: indexPath) as! LayoutThreeCollectionViewCell
                
                if selAssetsIdentifiersArray.count > 2
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: 0)
                    cell.imageViewSelected1 = returnImageFromSocialURL(imageView: cell.imageViewSelected1, index: 0)
                    
                    cell.imageViewNormal2 = returnImageFromSocialURL(imageView: cell.imageViewNormal2, index: 1)
                    cell.imageViewSelected2 = returnImageFromSocialURL(imageView: cell.imageViewSelected2, index: 1)
                    
                    cell.imageViewNormal3 = returnImageFromSocialURL(imageView: cell.imageViewNormal3, index: 2)
                    cell.imageViewSelected3 = returnImageFromSocialURL(imageView: cell.imageViewSelected3, index: 2)
                }
                else if selAssetsIdentifiersArray.count > 1
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: 0)
                    cell.imageViewSelected1 = returnImageFromSocialURL(imageView: cell.imageViewSelected1, index: 0)
                    
                    cell.imageViewNormal2 = returnImageFromSocialURL(imageView: cell.imageViewNormal2, index: 1)
                    cell.imageViewSelected2 = returnImageFromSocialURL(imageView: cell.imageViewSelected2, index: 1)
                    
                    cell.imageViewNormal3 = returnImageFromSocialURL(imageView: cell.imageViewNormal3, index: 0)
                    cell.imageViewSelected3 = returnImageFromSocialURL(imageView: cell.imageViewSelected3, index: 0)
                }
                else if selAssetsIdentifiersArray.count > 0
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: 0)
                    cell.imageViewSelected1 = returnImageFromSocialURL(imageView: cell.imageViewSelected1, index: 0)
                    
                    cell.imageViewNormal2 = returnImageFromSocialURL(imageView: cell.imageViewNormal2, index: 0)
                    cell.imageViewSelected2 = returnImageFromSocialURL(imageView: cell.imageViewSelected2, index: 0)
                    
                    cell.imageViewNormal3 = returnImageFromSocialURL(imageView: cell.imageViewNormal3, index: 0)
                    cell.imageViewSelected3 = returnImageFromSocialURL(imageView: cell.imageViewSelected3, index: 0)
                }
                else if selAssetsIdentifiersArray.count == 0
                {
                    cell.imageViewNormal1.image = nil
                    cell.imageViewSelected1.image = nil
                    
                    cell.imageViewNormal2.image = nil
                    cell.imageViewSelected2.image = nil
                    
                    cell.imageViewNormal3.image = nil
                    cell.imageViewSelected3.image = nil
                }
                
                if selectedCollageLayoutIndex == indexPath.item
                {
                    cell.showSelectedCell()
                }
                else
                {
                    cell.showNormalCell()
                }
                
                return cell
                
            case 3:
                let cell : LayoutFourCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "LayoutFourCollectionViewCell", for: indexPath) as! LayoutFourCollectionViewCell
                
                if selAssetsIdentifiersArray.count > 3
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: 0)
                    cell.imageViewSelected1 = returnImageFromSocialURL(imageView: cell.imageViewSelected1, index: 0)
                    
                    cell.imageViewNormal2 = returnImageFromSocialURL(imageView: cell.imageViewNormal2, index: 1)
                    cell.imageViewSelected2 = returnImageFromSocialURL(imageView: cell.imageViewSelected2, index: 1)
                    
                    cell.imageViewNormal3 = returnImageFromSocialURL(imageView: cell.imageViewNormal3, index: 2)
                    cell.imageViewSelected3 = returnImageFromSocialURL(imageView: cell.imageViewSelected3, index: 2)
                    
                    cell.imageViewNormal4 = returnImageFromSocialURL(imageView: cell.imageViewNormal4, index: 3)
                    cell.imageViewSelected4 = returnImageFromSocialURL(imageView: cell.imageViewSelected4, index: 3)
                }
                else if selAssetsIdentifiersArray.count > 2
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: 0)
                    cell.imageViewSelected1 = returnImageFromSocialURL(imageView: cell.imageViewSelected1, index: 0)
                    
                    cell.imageViewNormal2 = returnImageFromSocialURL(imageView: cell.imageViewNormal2, index: 1)
                    cell.imageViewSelected2 = returnImageFromSocialURL(imageView: cell.imageViewSelected2, index: 1)
                    
                    cell.imageViewNormal3 = returnImageFromSocialURL(imageView: cell.imageViewNormal3, index: 2)
                    cell.imageViewSelected3 = returnImageFromSocialURL(imageView: cell.imageViewSelected3, index: 2)
                    
                    cell.imageViewNormal4 = returnImageFromSocialURL(imageView: cell.imageViewNormal4, index: 0)
                    cell.imageViewSelected4 = returnImageFromSocialURL(imageView: cell.imageViewSelected4, index: 0)
                }
                else if selAssetsIdentifiersArray.count > 1
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: 0)
                    cell.imageViewSelected1 = returnImageFromSocialURL(imageView: cell.imageViewSelected1, index: 0)
                    
                    cell.imageViewNormal2 = returnImageFromSocialURL(imageView: cell.imageViewNormal2, index: 1)
                    cell.imageViewSelected2 = returnImageFromSocialURL(imageView: cell.imageViewSelected2, index: 1)
                    
                    cell.imageViewNormal3 = returnImageFromSocialURL(imageView: cell.imageViewNormal3, index: 0)
                    cell.imageViewSelected3 = returnImageFromSocialURL(imageView: cell.imageViewSelected3, index: 0)
                    
                    cell.imageViewNormal4 = returnImageFromSocialURL(imageView: cell.imageViewNormal4, index: 1)
                    cell.imageViewSelected4 = returnImageFromSocialURL(imageView: cell.imageViewSelected4, index: 1)
                }
                else if selAssetsIdentifiersArray.count > 0
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: 0)
                    cell.imageViewSelected1 = returnImageFromSocialURL(imageView: cell.imageViewSelected1, index: 0)
                    
                    cell.imageViewNormal2 = returnImageFromSocialURL(imageView: cell.imageViewNormal2, index: 0)
                    cell.imageViewSelected2 = returnImageFromSocialURL(imageView: cell.imageViewSelected2, index: 0)
                    
                    cell.imageViewNormal3 = returnImageFromSocialURL(imageView: cell.imageViewNormal3, index: 0)
                    cell.imageViewSelected3 = returnImageFromSocialURL(imageView: cell.imageViewSelected3, index: 0)
                    
                    cell.imageViewNormal4 = returnImageFromSocialURL(imageView: cell.imageViewNormal4, index: 0)
                    cell.imageViewSelected4 = returnImageFromSocialURL(imageView: cell.imageViewSelected4, index: 0)
                }
                else if selAssetsIdentifiersArray.count == 0
                {
                    cell.imageViewNormal1.image = nil
                    cell.imageViewSelected1.image = nil
                    
                    cell.imageViewNormal2.image = nil
                    cell.imageViewSelected2.image = nil
                    
                    cell.imageViewNormal3.image = nil
                    cell.imageViewSelected3.image = nil
                    
                    cell.imageViewNormal4.image = nil
                    cell.imageViewSelected4.image = nil
                }
                
                if selectedCollageLayoutIndex == indexPath.item
                {
                    cell.showSelectedCell()
                }
                else
                {
                    cell.showNormalCell()
                }
                
                return cell
                
            default:
                let cell : LayoutOneCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "LayoutOneCollectionViewCell", for: indexPath) as! LayoutOneCollectionViewCell
                
                if selAssetsIdentifiersArray.count == 1
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: indexPath.item)
                }
                else if selAssetsIdentifiersArray.count == 2
                {
                    cell.imageViewNormal1 = returnImageFromSocialURL(imageView: cell.imageViewNormal1, index: 0)
                    cell.imageViewNormal2 = returnImageFromSocialURL(imageView: cell.imageViewNormal2, index: 1)
                }
                
                return cell
            }
            
        default:
            return UICollectionViewCell()
        }
    }
    
    func returnImageFromSocialURL(imageView : UIImageView, index : Int) -> UIImageView
    {
        if self.imagesFrom == "Local"
        {
            imageView.image = self.selectedImages[index]
            return imageView
        }
        else
        {
            imageView.af_setImage(
                withURL: URL(string:selAssetsIdentifiersArray[index])!,
                imageTransition: .crossDissolve(0.2),
                completion: nil)
            return imageView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        if collectionView == collectionViewGrid {
            if self.selAssetsIdentifiersArray.count == 4 && !self.selectedCellsArray.contains(indexPath.item) {
                self.view.makeToast("You can select only 4 images for collage".localisedString(), duration: 2.0, position: .bottom)
                return false
            }
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        switch(collectionView) {
        case collectionViewGrid:
            if self.imagesFrom == "Local"
            {
                if self.filteredArray.isEmpty {
                    //let asset = fetchResult.object(at: indexPath.item)
                    let asset = getSelectedImageAssetBySelectedOrder(index: indexPath.item)
                    let assetIdentifier = asset.localIdentifier
                    self.updateSelectedAssetArray(assetIdentifier: assetIdentifier, indexpath: indexPath, asset: asset)
                }else {
                    let assetIdentifier = self.filteredArray[indexPath.item]
                    let asset = PHAsset.fetchAssets(withLocalIdentifiers: [assetIdentifier], options: .none).firstObject
                    self.updateSelectedAssetArray(assetIdentifier: assetIdentifier, indexpath: indexPath, asset: asset)
                }
            }
            else
            {
                if WebserviceModelClass().isInternetAvailable(){
//                let url = selAssetsIdentifiersArray[indexPath.row]
                let url = self.socialImages[indexPath.row]
                self.updateSelectedAssetArray(assetIdentifier: url, indexpath: indexPath)
                }
                else
                {
                    let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                    comeFromStrNoInternetSocialMedia = "NormalNointernet"
                    self.present(alertPopUp, animated: true, completion: nil)
                }
            }
        case frameCollectionView:
            self.selectedCollageLayoutIndex = indexPath.item
           
            // Scroll to selected item path when selected
            self.frameCollectionView.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: true)
            self.frameCollectionView.reloadData()
            
        default:
            print("")
            
        }
        self.setItemsSelectedLbl()
    }
    
    //    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    //        if self.imagesFrom == "Local"
    //        {
    //            //let asset = fetchResult.object(at: indexPath.item)
    //            let asset = getSelectedImageAssetBySelectedOrder(index: indexPath.item)
    //            let assetIdentifier = asset.localIdentifier
    //            self.updateSelectedAssetArray(assetIdentifier: assetIdentifier, indexpath: indexPath, asset: asset)
    //        }
    //        else
    //        {
    //            let url = arraySelectedImgURL[indexPath.row]
    ////            self.updateSelectedAssetArray(assetIdentifier: url, indexpath: indexPath, asset: PHAsset)
    //        }
    //    }
    
    func getSelectedImageAssetBySelectedOrder(index : Int) -> PHAsset {
        //this condition for oldest and newest first function
        var asset = PHAsset()
        if isSortByOldestFirst {
            asset = imagesArray.object(at: index)
        }
        else{
            
            asset = imagesArray.object(at: imagesArray.count - (index + 1))
        }
        
        return asset
    }
    
    func updateSelectedAssetArray(assetIdentifier : String, indexpath: IndexPath, asset: PHAsset? = nil)
    {
        if selAssetsIdentifiersArray.contains(assetIdentifier)
        {
            if let indexOfCell = self.selectedCellsArray.index(of: indexpath.item) {
                self.selectedCellsArray.remove(at: indexOfCell)
            }
            if let index = selAssetsIdentifiersArray.index(of: assetIdentifier)
            {
                self.selAssetsIdentifiersArray.remove(at: index)
                if asset != nil {
                    self.selectedImages.remove(at: index)
                }
            }
        }
        else
        {

            self.selectedCellsArray.append(indexpath.item)
            self.selAssetsIdentifiersArray.append(assetIdentifier)
        }
        let cell = collectionViewGrid.cellForItem(at: indexpath) as! GalleryGridCollectionViewCell
        if self.selAssetsIdentifiersArray.contains(assetIdentifier)
        {
            cell.showSelectedCell()
            if asset != nil {
                self.selectedImages.append(getImageFromAsset(asset : asset!))
            }
        }
        else
        {
            cell.showNormalCell()
        }
        self.frameCollectionView.reloadData()
    }
    
    func resizeImage(image: UIImage) -> UIImage {
        let size = image.size
        let targetSize = CGSize(width: 1000, height: 1000)
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension SelectLayoutViewController : UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        switch collectionView {
        case collectionViewGrid:
            
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let numberOfColumns = 4
            
            let totalSpace = flowLayout.minimumInteritemSpacing * CGFloat(numberOfColumns)
            let size = Int((collectionView.frame.width - totalSpace) / CGFloat(4))
            
            return CGSize(width: size, height: size)
            
        case frameCollectionView:
            
            return CGSize(width: frameCollectionView.frame.size.height * 0.8, height: frameCollectionView.frame.size.height)
            
        default:
            print("No cell set")
        }
        
        return CGSize(width: 0, height: 0)
    }
}
