//
//  SelectLayout+PHLibraryObserver.swift
//  KODAK MINI
//
//  Created by MAXIMESS152 on 14/01/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import Foundation
import Photos

extension SelectLayoutViewController: PHPhotoLibraryChangeObserver {
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized //&& fetchResult != nil
        {
            
            self.fetchAlbumPhotos()
            //guard let changes = changeInstance.changeDetails(for: fetchResult)
            //    else { return }
            
            // Change notifications may be made on a background queue. Re-dispatch to the
            // main queue before acting on the change as we'll be updating the UI.
            DispatchQueue.main.sync {
                // Reload the thumbnails
                self.collectionViewGrid.reloadData()
            }
        }
    }
    
    func fetchAlbumPhotos()
    {
        // Added by Pratiksha to solve issue of showing blank gallery when any image is saved.
        let options = PHFetchOptions()
        //options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        //Added by akshay to exclude the video screenshots from assets
        options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        let newFetchResult = PHAsset.fetchAssets(in: collectionItem, options: options)
        
        self.imagesArray = newFetchResult
    }
}

