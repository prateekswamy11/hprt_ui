//
//  SelectLayoutViewController.swift
//  Kodak Smile
//
//  Created by MAXIMESS152 on 14/01/19.
//  Copyright © 2019 maximess. All rights reserved.
//

import UIKit
import Photos
//import CopilotAPIAccess

class SelectLayoutViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var lblAlbumName: UILabel!
    @IBOutlet weak var viewGestureContainer: UIView!
    
    @IBOutlet weak var collectionViewGrid: UICollectionView!
    @IBOutlet weak var gridViewHeightContraint: NSLayoutConstraint!
    
    @IBOutlet weak var upperConainerView: UIView!
    @IBOutlet weak var viewForGesture: UIView!
    @IBOutlet weak var frameCollectionView: UICollectionView!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var viewBottomView: UIView!
    @IBOutlet weak var lblItemsSelected: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    
    var imagesArray: PHFetchResult<PHAsset>!
    var collectionItem = PHAssetCollection()
    var topViewInitialHieght: CGFloat!
    var trayOriginalCenter: CGPoint!
    var startPosition: CGPoint?
    var trayDownOffset: CGFloat!
    var trayUp: CGPoint!
    var trayDown: CGPoint!
    let inset: CGFloat = 3
    let minimumLineSpacing: CGFloat = 3
    let minimumInteritemSpacing: CGFloat = 3
    let cellsPerRow = 4
    var imagesFrom = String()
    /*fileprivate*/ let imageManager = PHCachingImageManager()
    /*fileprivate*/ var thumbnailSize: CGSize!
    fileprivate var previousPreheatRect = CGRect.zero
    let cellImgoptions = PHImageRequestOptions()
    var socialImages = [String]()
    var dropboxImages = [UIImage]()
    var selectedImages = [UIImage]()
    var selectedCellsArray = [Int]()
    var filteredArray = [String]()
    var headerName = ""
    
    // we set a variable to hold the contentOffSet before scroll view scrolls
    var lastContentOffset: CGFloat = 0
    
    // Select variables
    var isSelectOptionsActive = false
    var selAssetsIdentifiersArray = [String]()
    var initialSelection = [String]()
    var imageDataArray = [Int]()
    var selectedCollageLayoutIndex = Int()
    var initaiLoad = true
    var fetchResult: PHFetchResult<PHAsset>!
    var isReplaceMode = false
    
    //Hide Status bar
    override var prefersStatusBarHidden: Bool {
        return status_Bar_Hidden
    }
    
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSelection = self.selAssetsIdentifiersArray
        // Do any additional setup after loading the view.
//        self.collectionViewGrid.allowsMultipleSelection = true
        PHPhotoLibrary.shared().register(self)
        self.setupGestures()
        if #available(iOS 11.0, *) {
            collectionViewGrid?.contentInsetAdjustmentBehavior = .always
        } else {
            // Fallback on earlier versions
            
        }
        
        // Allow to download image from iCloud
        cellImgoptions.isNetworkAccessAllowed = true
        
        // Determine the size of the thumbnails to request from the PHCachingImageManager
        thumbnailSize = CGSize(width:100, height: 100)
        
        //self.getAssetsDatewise()
        
        // Adjusting the size of lblAlbumNameLower label according to content
        lblAlbumName.adjustsFontSizeToFitWidth = false
        lblAlbumName.lineBreakMode = .byTruncatingTail
        
        self.setViewTexts()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateCachedAssets()
        
//        let colorTop = UIColor(red: 238/255.0, green: 238/255.0, blue: 238/255.0, alpha: 100.0)
//        let colorBottom = UIColor(red: 216/255.0, green: 216/255.0, blue: 216/255.0, alpha: 0.0)
//        viewBottomView.setGradientBackground(colorTop: colorBottom, colorBottom: colorTop)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Report screen load event
//        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: AnalyticsEventName.Screen.createCollage.rawValue)
//        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.stopLoader()
    }
    
    // MARK: - Initialisation
    func setViewTexts()
    {
        self.lblAlbumName.text = "Select Layout".localisedString()
        self.btnCancel.setTitle("Cancel".localisedString(), for: .normal)
        self.btnNext.setTitle("Next".localisedString(), for: .normal)
        
        self.setItemsSelectedLbl()
    }
    
    // MARK: - Action methods
    @IBAction func btnBackClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancelClicked(_ sender: Any)
    {
        
    }
    
    @IBAction func btnNextClicked(_ sender: Any)
    {
        // Restrict user from goingto EditCollage view if there are no images selected
        if self.selAssetsIdentifiersArray.count == 0
        {
            self.view.makeToast("Please select at least one image".localisedString(), duration: 2.0, position: .bottom)
        }
        else
        {

            //        isPopover = true
            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "EditCollageViewController") as! EditCollageViewController
            vc.layoutIndex = selectedCollageLayoutIndex

            var selImgArray = [UIImage]()
            if self.imagesFrom == "Local"
            {
                selImgArray = self.getImagesFromAssets()
            }
            else
            {
                if WebserviceModelClass().isInternetAvailable(){
                selImgArray = self.getImagesFromUrls()
                    vc.socialImagesURL = self.socialImages
                }else{
                    let alertPopUp = storyboards.popUpStoryboard.instantiateViewController(withIdentifier: "PopUpNoInternetViewController") as! PopUpNoInternetViewController
                     comeFromStrNoInternetSocialMedia = "NormalNointernet"
                    self.present(alertPopUp, animated: true, completion: nil)
                }
            }

            vc.selImgArray = selImgArray

            print("selImgArray.count = \(selImgArray.count)")

            // Pass fetchresult so that when we come from replace screen we have same set of images to show
            vc.fetchResult = fetchResult
            vc.imagesArray = self.imagesArray
            vc.cameFrom = self.imagesFrom
            vc.selAssetsIdentifiersArray = selAssetsIdentifiersArray


            let dict = ["layoutIndex":selectedCollageLayoutIndex,"selImgArray":selImgArray,"fetchResult":fetchResult,"selAssetsIdentifiersArray":selAssetsIdentifiersArray] as [String : Any]

            if isReplaceMode
            {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "ReplaceCollage"), object: dict)

                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func fillInSelectedImageArray() -> [String]
    {
        var selArray = [String]()
        switch selAssetsIdentifiersArray.count
        {
        case 1:
            selArray.append(selAssetsIdentifiersArray[0])
            selArray.append(selAssetsIdentifiersArray[0])
            selArray.append(selAssetsIdentifiersArray[0])
            selArray.append(selAssetsIdentifiersArray[0])
            
        case 2:
            selArray.append(selAssetsIdentifiersArray[0])
            selArray.append(selAssetsIdentifiersArray[1])
            selArray.append(selAssetsIdentifiersArray[0])
            selArray.append(selAssetsIdentifiersArray[1])
            
        case 3:
            selArray.append(selAssetsIdentifiersArray[0])
            selArray.append(selAssetsIdentifiersArray[1])
            selArray.append(selAssetsIdentifiersArray[2])
            selArray.append(selAssetsIdentifiersArray[0])
            
        case 4:
            selArray.append(selAssetsIdentifiersArray[0])
            selArray.append(selAssetsIdentifiersArray[1])
            selArray.append(selAssetsIdentifiersArray[2])
            selArray.append(selAssetsIdentifiersArray[3])
            
        default:
            print("error")
        }
        
        return selArray
    }
    
    func getImagesFromAssets() -> [UIImage]
    {
        let imgManager = PHImageManager()
        var imgArray = [UIImage]()
        
        let imagemhng = PHImageManager()
        
        // Get images in for loop as the images we are gettings from selAssetsIdentifiersArray's URLs, are automatically sorted by date
        for url in selAssetsIdentifiersArray
        {
            //customLoader.showActivityIndicator(viewController: self.view)
            let assetsFetchResult = PHAsset.fetchAssets(withLocalIdentifiers: [url], options: nil)
            
            assetsFetchResult.enumerateObjects{(object: AnyObject!,
                count: Int,
                stop: UnsafeMutablePointer<ObjCBool>) in
                
                if object is PHAsset{
                    let asset = object as! PHAsset
                    let imageSize = CGSize(width: asset.pixelWidth,
                                           height: asset.pixelHeight)
                    
                    let options = PHImageRequestOptions()
                    options.deliveryMode = .opportunistic
                    options.isSynchronous = true
                    options.isNetworkAccessAllowed = true
                    
                    imagemhng.requestImage(for: asset,
                                           targetSize: imageSize,
                                           contentMode: .aspectFill,
                                           options: options,
                                           resultHandler:
                        {
                            (image, info) -> Void in
                            if let receivedImg = image
                            {
                                // customLoader.hideActivityIndicator()
                                /* The image is now available to us */
                                imgArray.append(receivedImg)
                            }
                            else
                            {
                                var imgSize = 1000.0
                                var loopCount = 5
                                for _ in 0..<loopCount
                                {
                                    imagemhng.requestImage(for: asset,
                                                           targetSize: CGSize(width: imgSize, height: imgSize),
                                                           contentMode: PHImageContentMode.aspectFit,
                                                           options: options,
                                                           resultHandler:
                                        { image, _ in
                                            
                                            if let receivedImg = image
                                            {
                                                /* The image is now available to us */
                                                imgArray.append(receivedImg)
                                                loopCount = 0
                                            }
                                    })
                                    imgSize = imgSize - 200
                                }
                            }
                    })
                }
            }
        }
        
        return imgArray
    }
    
    func getImagesFromUrls() -> [UIImage]
    {
        var imgArray = [UIImage]()
        
        for stringUrl in selAssetsIdentifiersArray
        {
            if stringUrl != ""
            {
                let url = URL(string:stringUrl)
                if let data = try? Data(contentsOf: url!)
                {
                    if let image: UIImage = UIImage(data: data)
                    {
                        let img = self.resizeImage(image: image)
                        imgArray.append(img)
                    }
                }
            }
        }
        
        return imgArray
    }
    
    // MARK: - Logical Methods
    
    func setupGestures()
    {
//        trayDownOffset = self.upperConainerView.frame.height/1.45
//        trayUp = CGPoint(x: self.view.frame.midX ,y: upperConainerView.center.y + 25)
//        trayDown = CGPoint(x: self.view.frame.midX ,y: upperConainerView.center.y + trayDownOffset)
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.didPanTray(sender:)))
        self.viewForGesture.addGestureRecognizer(pan)
        self.gridViewHeightContraint.constant = UIScreen.main.bounds.height / 3
//        self.upperConainerView.center = self.trayDown
    }
    
    @objc func didPanTray(sender: UIPanGestureRecognizer) {
        
        let velocity = sender.velocity(in: self.view)
        let translation = sender.location(in: self.view)
        
        if sender.state == UIGestureRecognizerState.began {
            
//            self.gridViewHeightContraint.constant = self.gridViewHeightContraint.constant
            
        } else if sender.state == UIGestureRecognizerState.changed {

            //            if upperConainerView.center.y >= self.trayUp.y && upperConainerView.center.y <= self.trayDown.y
            //            {
            if translation.y - self.upperConainerView.frame.minY >= self.view.frame.height / 3 && translation.y - self.upperConainerView.frame.minY <= self.view.center.y {
                UIView.animate(withDuration: 0.1, animations: { () -> Void in
                    self.gridViewHeightContraint.constant = translation.y  - self.upperConainerView.frame.minY
                    
                    self.frameCollectionView.reloadData()
                })
            }
            else if translation.y  - self.upperConainerView.frame.minY >= self.view.center.y {
                UIView.animate(withDuration: 0.1, animations: { () -> Void in
                    self.gridViewHeightContraint.constant = self.view.center.y
                    
                    self.frameCollectionView.reloadData()
                })
            }
            else if translation.y - self.upperConainerView.frame.minY < self.view.frame.height / 3 {
                UIView.animate(withDuration: 0.1, animations: { () -> Void in
                    self.gridViewHeightContraint.constant = self.view.frame.height / 3
                    
                    self.frameCollectionView.reloadData()
                })
            }

        } else if sender.state == UIGestureRecognizerState.ended {

            self.frameCollectionView.reloadData()
        }
        let indexPath = IndexPath(item: self.selectedCollageLayoutIndex, section: 0)
        self.frameCollectionView.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: false)
    }
    
    func setItemsSelectedLbl()
    {
        let count = self.selAssetsIdentifiersArray.count
        if count == 1
        {
            self.lblItemsSelected.text = "\(count) "+"Item selected".localisedString()
        }
        else
        {
            self.lblItemsSelected.text = "\(count) "+"Items Selected".localisedString()
        }
    }
    
    func checkPhotoLibraryPermission() -> Bool {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            //print("authorized")
            
            return true
        case .denied, .restricted, .notDetermined :
            //handle denied status
            print("denied")
            
            let dialog = UIAlertController(title: "Unable to access the Photos".localisedString(), message: "To enable access, go to Settings > Privacy > Photos and turn on Photos access for this app.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
            
            dialog.addAction(okAction)
            self.present(dialog, animated:true, completion:nil)
            
            return false
        }
    }
    
    func getImageFromAsset(asset : PHAsset) -> UIImage
    {
        //let imageSize = CGSize(width: asset.pixelWidth,
        //  height: asset.pixelHeight)
        
        let options = PHImageRequestOptions()
        
        //Photos automatically provides one or more results in order to balance image quality and responsiveness.
        options.deliveryMode = .opportunistic
        
        //Request the original, highest-fidelity version of the image asset.
        //options.version = .original
        
        //Set it to true to block the calling thread until either the requested image is ready or an error occurs, at which time Photos calls your result handler.
        options.isSynchronous = true
        
        // Allow to download image from iCloud
        options.isNetworkAccessAllowed = true
        
        var imgToSend = UIImage()
        let imagemhng = PHImageManager()
        
        var imgSize = 1000.0
        var loopCount = 5
        
        imagemhng.requestImage(for: asset,
                               targetSize: CGSize(width: 300, height: 300),
                               contentMode: PHImageContentMode.aspectFit,
                               options: options,
                               resultHandler:
            { (image, info ) -> Void in
                
                
                //Solve crashlytics crash
                
                guard let dict : Dictionary = info else{
                    if let receivedImg = image
                    {
                        /* The image is now available to us */
                        imgToSend = receivedImg
                    }
                    else{
                        let dialog = UIAlertController(title: "Cloud Error".localisedString(), message: "This photo could not be downloaded from iCloud. Please connect to internet.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                        
                        dialog.addAction(okAction)
                        self.present(dialog, animated:true, completion:nil)
                    }
                    return
                }
                
                
                if let isIcloudIMg = dict["PHImageResultIsInCloudKey"] as? Int
                {
                    if (isIcloudIMg == 0) {
                        
                        if let receivedImg = image
                        {
                            /* The image is now available to us */
                            imgToSend = receivedImg
                        }
                    }
                    else{
                        
                        if let receivedImg = image
                        {
                            /* The image is now available to us */
                            imgToSend = receivedImg
                        }
                        else{
                            let dialog = UIAlertController(title: "Cloud Error".localisedString(), message: "This photo could not be downloaded from iCloud. Please connect to internet.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
                            
                            let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                            
                            dialog.addAction(okAction)
                            self.present(dialog, animated:true, completion:nil)
                        }
                    }
                }
                else{
                    
                    if let receivedImg = image
                    {
                        /* The image is now available to us */
                        imgToSend = receivedImg
                    }
                    else{
                        let dialog = UIAlertController(title: "Cloud Error".localisedString(), message: "This photo could not be downloaded from iCloud. Please connect to internet.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                        
                        dialog.addAction(okAction)
                        self.present(dialog, animated:true, completion:nil)
                    }
                }
                
                //                var dict : Dictionary = info!
                //                let isIcloudIMg = dict["PHImageResultIsInCloudKey"] as! Int
                //                if (isIcloudIMg == 0) {
                //
                //                    if let receivedImg = image
                //                    {
                //                        /* The image is now available to us */
                //                        imgToSend = receivedImg
                //                    }
                //                }
                //                else{
                //
                //                    if let receivedImg = image
                //                    {
                //                        /* The image is now available to us */
                //                        imgToSend = receivedImg
                //                    }
                //                    else{
                //                        let dialog = UIAlertController(title: "Cloud Error", message: "This photo could not be downloaded from iCloud. Please connect to internet.".localisedString(), preferredStyle: UIAlertControllerStyle.alert)
                //
                //                        let okAction = UIAlertAction(title: "OK".localisedString(), style: UIAlertActionStyle.default, handler: nil)
                //
                //                        dialog.addAction(okAction)
                //                        self.present(dialog, animated:true, completion:nil)
                //                    }
                //                }
                //
                
                
        })
        
        
        return imgToSend
    }
    
    func getSelectedImageAsset(index : Int) -> PHAsset
    {
        var asset = PHAsset()
        asset = imagesArray.object(at: imagesArray.count - (index + 1))
        
        return asset
    }
    
    func stopLoader(){
        
        customLoader.hideActivityIndicator()
    }
    
    func startLoader()  {
        customLoader.showActivityIndicator(viewController: self.view)
    }
    
    
    // MARK: - Caching
    // MARK: UIScrollView
    
    private func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        
        if self.imagesFrom == "Local" {
            updateCachedAssets()
        }
    }
    
    // MARK: Asset Caching
    
    fileprivate func resetCachedAssets() {
        imageManager.stopCachingImagesForAllAssets()
        previousPreheatRect = .zero
    }
    
    fileprivate func updateCachedAssets() {
        //        if is_Showing_Social_Photos == false
        //        {
        if checkPhotoLibraryPermission() == true
        {
            // Update only if the view is visible.
            guard isViewLoaded && view.window != nil else { return }
            
            // The preheat window is twice the height of the visible rect.
            let visibleRect = CGRect(origin: collectionViewGrid!.contentOffset, size: collectionViewGrid!.bounds.size)
            let preheatRect = visibleRect.insetBy(dx: 0, dy: -0.5 * visibleRect.height)
            
            // Update only if the visible area is significantly different from the last preheated area.
            let delta = abs(preheatRect.midY - previousPreheatRect.midY)
            guard delta > view.bounds.height / 3 else { return }
            
            // Compute the assets to start caching and to stop caching.
            let (addedRects, removedRects) = differencesBetweenRects(previousPreheatRect, preheatRect)
            let addedAssets = addedRects
                .flatMap { rect in collectionViewGrid!.indexPathsForElements(in: rect) }
                .map { indexPath in imagesArray.object(at: indexPath.item) }
            let removedAssets = removedRects
                .flatMap { rect in collectionViewGrid!.indexPathsForElements(in: rect) }
                .map { indexPath in imagesArray.object(at: indexPath.item) }
            
            // Update the assets the PHCachingImageManager is caching.
            imageManager.startCachingImages(for: addedAssets,
                                            targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
            imageManager.stopCachingImages(for: removedAssets,
                                           targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
            
            // Store the preheat rect to compare against in the future.
            previousPreheatRect = preheatRect
        }
        //        }
    }
    
    fileprivate func differencesBetweenRects(_ old: CGRect, _ new: CGRect) -> (added: [CGRect], removed: [CGRect]) {
        if old.intersects(new) {
            var added = [CGRect]()
            if new.maxY > old.maxY {
                added += [CGRect(x: new.origin.x, y: old.maxY,
                                 width: new.width, height: new.maxY - old.maxY)]
            }
            if old.minY > new.minY {
                added += [CGRect(x: new.origin.x, y: new.minY,
                                 width: new.width, height: old.minY - new.minY)]
            }
            var removed = [CGRect]()
            if new.maxY < old.maxY {
                removed += [CGRect(x: new.origin.x, y: new.maxY,
                                   width: new.width, height: old.maxY - new.maxY)]
            }
            if old.minY < new.minY {
                removed += [CGRect(x: new.origin.x, y: old.minY,
                                   width: new.width, height: new.minY - old.minY)]
            }
            return (added, removed)
        } else {
            return ([new], [old])
        }
    }
    
    
}

private extension UICollectionView {
    func indexPathsForElements(in rect: CGRect) -> [IndexPath] {
        let allLayoutAttributes = collectionViewLayout.layoutAttributesForElements(in: rect)!
        return allLayoutAttributes.map { $0.indexPath }
    }
}
