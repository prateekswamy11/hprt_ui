//
//  LayoutThreeCollectionViewCell.swift
//  Polaroid MINT
//
//  Created by maximess142 on 14/12/18.
//  Copyright © 2018 maximess. All rights reserved.
//

import UIKit

class LayoutThreeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewNormalCell: UIView!
    @IBOutlet weak var imageViewNormal1: UIImageView!
    @IBOutlet weak var imageViewNormal2: UIImageView!
    @IBOutlet weak var imageViewNormal3: UIImageView!

    @IBOutlet weak var viewSelectedCell: UIView!
    @IBOutlet weak var imageViewSelected1: UIImageView!
    @IBOutlet weak var imageViewSelected2: UIImageView!
    @IBOutlet weak var imageViewSelected3: UIImageView!

    func showNormalCell()
    {
        self.viewNormalCell.isHidden = false
        self.viewSelectedCell.isHidden = true
    }
    
    func showSelectedCell()
    {
        self.viewNormalCell.isHidden = true
        self.viewSelectedCell.isHidden = false
    }
}
