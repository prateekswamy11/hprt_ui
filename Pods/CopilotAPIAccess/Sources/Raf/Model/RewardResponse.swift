//
//  RewardResponse.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 27/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class RewardResponse {
    
    private enum Keys {
        static let currencyCodeKey = "currencyCode"
        static let currencySymbolKey = "currencySymbol"
        static let idKey = "id"
        static let statusKey = "status"
        static let valueKey = "value"
    }
    
    public enum RewardStatus: String {
        case available = "Available"
        case pending = "Pending"
        case claimed = "Claimed"
        case canceled = "Canceled"
    }
    
    public let currencyCode: String
    public let currencySymbol: String
    public let id: String
    public let status: RewardStatus
    public let value: Double
    
    // MARK: - Init
    
    init?(withDictionary dictionary: [String: Any]) {
        guard let currencyCode = dictionary[Keys.currencyCodeKey] as? String,
            let currencySymbol = dictionary[Keys.currencySymbolKey] as? String,
            let id = dictionary[Keys.idKey] as? String,
            let statusString = dictionary[Keys.statusKey] as? String,
            let status = RewardStatus(rawValue: statusString),
            let value = dictionary[Keys.valueKey] as? Double else {
                return nil
        }
        
        self.currencySymbol = currencySymbol
        self.currencyCode = currencyCode
        self.id = id
        self.status = status
        self.value = value
    }
}
