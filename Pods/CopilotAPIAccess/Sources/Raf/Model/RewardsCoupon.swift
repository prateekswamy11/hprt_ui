//
//  DiscountCode.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 28/07/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class RewardsCoupon {
    
    public enum Keys {
        static let codeKey = "code"
        static let currencySymbolKey = "currencySymbol"
        static let valueKey = "value"
    }
    
    public let code: String
    public let currencySymbol: String
    public let value: Double
    
    // MARK: - Init
    
    init?(withDictionary dictionary: [String: Any]) {
        guard let code = dictionary[Keys.codeKey] as? String,
            let currencySymbol = dictionary[Keys.currencySymbolKey] as? String,
            let value = dictionary[Keys.valueKey] as? Double else {
                return nil
        }
        
        self.code = code
        self.currencySymbol = currencySymbol
        self.value = value
    }
}
