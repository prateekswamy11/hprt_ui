//
//  GeneralMetadata.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 27/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class GeneralMetadata {
    
    private enum Keys {
        static let dynamicInfoKey = "dynamicInfo"
        static let footerTitleKey = "footerTitle"
        static let storeNameKey = "storeName"
        static let termsAndConditionsKey = "termsAndConditions"
    }
    
    public let dynamicInfo: DynamicInfo?
    public let footerTitle: String
    public let storeName: String
    public let termsAndConditions: String?
    
    // MARK: - Init
    
    init?(withDictionary dictionary: [String: Any]) {
        guard let footerTitle = dictionary[Keys.footerTitleKey] as? String,
            let storeName = dictionary[Keys.storeNameKey] as? String else {
                return nil
        }
        
        var dynamicInfo: DynamicInfo? = nil
        if let dynamicInfoDict = dictionary[Keys.dynamicInfoKey] as? [String:Any] {
            dynamicInfo = DynamicInfo(withDictionary: dynamicInfoDict)
        }
        
        self.dynamicInfo = dynamicInfo
        self.footerTitle = footerTitle
        self.storeName = storeName
        self.termsAndConditions = dictionary[Keys.termsAndConditionsKey] as? String
    }
}
