//
//  DynamicInfo.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 27/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class DynamicInfo {
    
    private enum Keys {
        static let dynamicContentKey = "content"
        static let templateIdKey = "templateId"
    }
    
    public let dynamicContent: DynamicContent
    public let templateId: String
    
    // MARK: - Init
    
    init?(withDictionary dictionary: [String: Any]) {
        guard let templateId = dictionary[Keys.templateIdKey] as? String,
            let dynamicContentDict = dictionary[Keys.dynamicContentKey] as? [String:Any],
            let dynamicContent = DynamicContent(withDictionary: dynamicContentDict) else {
                return nil
        }
        
        self.dynamicContent = dynamicContent
        self.templateId = templateId
    }
}
