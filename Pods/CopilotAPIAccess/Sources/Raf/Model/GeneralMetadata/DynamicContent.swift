//
//  DynamicContent.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 27/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class DynamicContent {
    
    private enum Keys {
        static let elementsKey = "elements"
        static let titleKey = "title"
    }
    
    public let elements: [DynamicElement]
    public let title: String?
    
    // MARK: - Init
    
    init?(withDictionary dictionary: [String: Any]) {
        guard let elements = dictionary[Keys.elementsKey] as? [[String:Any]] else {
                return nil
        }
        
        self.elements = elements.compactMap({ DynamicElement(withDictionary: $0) })
        self.title = dictionary[Keys.titleKey] as? String
    }
}
