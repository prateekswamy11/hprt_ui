//
//  ReferAFriendData.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 28/07/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotLogger

public class RafDataResponse {
    
    private enum Keys {
        static let altruisticProgramMetadataKey = "altruisticProgramMetadata"
        static let generalMetadataKey = "generalMetadata"
        static let pendingJobsKey = "pendingJobs"
        static let programMetadataKey = "programMetadata"
        static let rewardsKey = "rewards"
        static let rewardsCouponsKey = "rewardsCoupons"
    }
    
    public let altruisticProgramMetadata: AltruisticProgramMetadata
    public let generalMetadata: GeneralMetadata
    public let pendingJobs: [RafJobResponse]?
    public let programMetadata: ProgramMetadata?
    public let rewards: [RewardResponse]
    public let rewardsCoupons: [RewardsCoupon]
    
    
    // MARK: - Init
    
    init?(withDictionary dictionary: [String: Any]) {
        guard let altruisticProgramDict = dictionary[Keys.altruisticProgramMetadataKey] as? [String:Any],
            let altruisticProgramMetadata = AltruisticProgramMetadata(withDictionary: altruisticProgramDict),
            let rewardsArr = dictionary[Keys.rewardsKey] as? [[String:Any]],
            let generalMetadataDict = dictionary[Keys.generalMetadataKey] as? [String:Any],
            let generalMetadata = GeneralMetadata(withDictionary: generalMetadataDict),
            let rewardsCouponsArr = dictionary[Keys.rewardsCouponsKey] as? [[String:Any]] else {
            return nil
        }
        
        var pendingJobs: [RafJobResponse]? = nil
        if let pendingJobsArr = dictionary[Keys.pendingJobsKey] as? [[String:Any]] {
            pendingJobs = pendingJobsArr.compactMap({ RafJobResponse(withDictionary: $0) })
        }
        
        var programMetadata: ProgramMetadata? = nil
        if let programMetadataDict = dictionary[Keys.programMetadataKey] as? [String:Any] {
            programMetadata = ProgramMetadata(withDictionary: programMetadataDict)
        }
        
        self.altruisticProgramMetadata = altruisticProgramMetadata
        self.generalMetadata = generalMetadata
        self.pendingJobs = pendingJobs
        self.programMetadata = programMetadata
        self.rewards = rewardsArr.compactMap({ RewardResponse(withDictionary: $0) })
        self.rewardsCoupons = rewardsCouponsArr.compactMap({ RewardsCoupon(withDictionary: $0) })
    }
}
