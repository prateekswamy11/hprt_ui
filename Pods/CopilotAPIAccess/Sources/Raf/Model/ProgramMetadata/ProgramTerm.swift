//
//  ProgramTerm.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 27/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class ProgramTerm {
    
    private enum Keys {
        static let currencySymbolKey = "currencySymbol"
        static let typeKey = "type"
        static let valueKey = "value"
    }
    
    public enum ProgramTermType: String {
        case percentage = "PERCENTAGE"
        case fixedAmount = "FIXED_AMOUNT"
        case none = "NONE"
    }
    
    public let currencySymbol: String
    public let type: ProgramTermType?
    public let value: Double
    
    // MARK: - Init
    
    init?(withDictionary dictionary: [String: Any]) {
        guard let currencySymbol = dictionary[Keys.currencySymbolKey] as? String,
            let value = dictionary[Keys.valueKey] as? Double else {
                return nil
        }
        
        var type: ProgramTermType? = nil
        if let typeString = dictionary[Keys.typeKey] as? String {
            type = ProgramTermType(rawValue: typeString)
        }
        
        self.currencySymbol = currencySymbol
        self.type = type
        self.value = value
    }
}
