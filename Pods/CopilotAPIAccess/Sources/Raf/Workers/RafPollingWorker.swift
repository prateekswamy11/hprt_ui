//
//  RafPollingWorker.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 02/09/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotLogger

public typealias RafPollingWorkerClosure<T: Error> = (Response<RafData, T>) -> Void

public class RafPollingWorker<T: Error>: StoppableWorker {
    
    private let jobCreator: JobCreator<T>?
    private var jobId: String?
    private let closure: RafPollingWorkerClosure<T>
    
    private let queue = DispatchQueue.global(qos: .background)
    private var pollingTask: DispatchWorkItem?
    private var timeoutTask: DispatchWorkItem?
    
    private let pollingTimeoutSeconds: Double = 30
    private var shouldContinuePolling = true
    private var shouldStartExecution = true
    
    typealias Dependencies = HasRafServiceInteraction
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies, jobCreator: JobCreator<T>? = nil, jobId: String? = nil, closure: @escaping RafPollingWorkerClosure<T>) {
        self.dependencies = dependencies
        self.jobCreator = jobCreator
        self.jobId = jobId
        self.closure = closure
    }
    
    public func startExecution() -> RafPollingWorker<T> {
        if shouldStartExecution {
            shouldStartExecution = false
            timeoutTask = DispatchWorkItem { [weak self] in
                ZLogManagerWrapper.sharedInstance.logInfo(message: "Timeout reached")
                self?.stopExecution()
                if let error = self?.jobCreator?.getGeneralError(withDebugMessage: "Timeout reached") {
                    self?.closure(.failure(error: error))
                }
            }
            if let timeoutTask = self.timeoutTask {
                queue.asyncAfter(deadline: .now() + pollingTimeoutSeconds, execute: timeoutTask)
            }
            
            if jobId != nil {
                // This is the case of job recovery or polling
                pollingTask = DispatchWorkItem { [weak self] in
                    self?.poll()
                }
                if let pollingTask = self.pollingTask {
                    queue.asyncAfter(deadline: .now(), execute: pollingTask)
                }
            } else {
                jobCreator?.createJob { [weak self] (reponse) in
                    guard let strongSelf = self else {
                        return
                    }
                    switch reponse {
                    case .success(let rafJob):
                        strongSelf.jobId = rafJob.id
                        
                        switch rafJob.status {
                        case .inProgress:
                            fallthrough
                        case .success:
                            //We have to poll at least once
                            self?.pollingTask = DispatchWorkItem { [weak self] in
                                self?.poll()
                            }
                            if let pollingTask = strongSelf.pollingTask {
                                strongSelf.queue.asyncAfter(deadline: .now(), execute: pollingTask)
                            }
                            
                        case .failure:
                            if let error = self?.jobCreator?.getGeneralError(withDebugMessage: "Failed creating job") {
                                strongSelf.closure(.failure(error: error))
                            }
                        }
                        
                    case .failure(let error):
                        strongSelf.closure(.failure(error: error))
                    }
                }
            }
        } else {
            ZLogManagerWrapper.sharedInstance.logError(message: "Execution already started")
        }
        
        return self
    }
    
    public func stopExecution() {
        shouldContinuePolling = false
        pollingTask?.cancel()
        timeoutTask?.cancel()
    }
    
    private func poll() {
        if let jobId = self.jobId {
            dependencies.rafServiceInteraction.getJobStatus(jobId: jobId) { [weak self] (response) in
                guard let strongSelf = self else {
                    ZLogManagerWrapper.sharedInstance.logError(message: "Raf polling not exist")
                    return
                }
                switch response {
                case .success(let jobStatus):
                    switch jobStatus.rafJob.status {
                    case .inProgress:
                        ZLogManagerWrapper.sharedInstance.logInfo(message: "Job status in progress")
                        if strongSelf.shouldContinuePolling {
                            let retryAfterTime: Double
                            if let retryAfter = jobStatus.rafJob.retryAfter, retryAfter != 0 {
                                retryAfterTime = Double(retryAfter)
                            } else {
                                retryAfterTime = 1
                            }
                            
                            if let pollingTask = strongSelf.pollingTask {
                                ZLogManagerWrapper.sharedInstance.logError(message: "Polling task added to retry after \(retryAfterTime)")
                                strongSelf.queue.asyncAfter(deadline: .now() + retryAfterTime, execute: pollingTask)
                            }
                            
                        } else {
                            if let error = self?.jobCreator?.getGeneralError(withDebugMessage: "Timeout reached") {
                                self?.closure(.failure(error: error))
                            }
                        }
                        
                    case .success:
                        ZLogManagerWrapper.sharedInstance.logInfo(message: "Success creating job")
                        strongSelf.stopExecution()
                        if let rafData = jobStatus.rafData {
                            strongSelf.closure(.success(rafData))
                        }
                        
                    case .failure:
                        ZLogManagerWrapper.sharedInstance.logError(message: "Failed creating job")
                        strongSelf.stopExecution()
                        if let error = self?.jobCreator?.getGeneralError(withDebugMessage: "Failed creating job") {
                            self?.closure(.failure(error: error))
                        }
                    }
                    
                case .failure(error: let error):
                    ZLogManagerWrapper.sharedInstance.logError(message: "Failed get job status for job id \(jobId)")
                    if let error = self?.jobCreator?.mapPollingError(withGetJobStatusError: error) {
                        self?.closure(.failure(error: error))
                    }
                }
            }
        } else {
            ZLogManagerWrapper.sharedInstance.logError(message: "No job id")
        }
    }
    
}
