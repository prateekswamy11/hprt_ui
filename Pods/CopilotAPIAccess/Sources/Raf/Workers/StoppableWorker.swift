//
//  StoppableWorker.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 05/09/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

protocol StoppableWorker {
    func stopExecution()
}
