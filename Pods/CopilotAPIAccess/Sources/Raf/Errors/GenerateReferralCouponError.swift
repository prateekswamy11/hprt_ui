//
//  GenerateReferralCouponError.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 01/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public enum GenerateReferralCouponError: Error {
    case requiresRelogin(debugMessage: String)
    case generalError(debugMessage: String)
    case connectivityError(debugMessage: String)
}

public class GenerateReferralCouponErrorResolver: ErrorResolver {
    public typealias T = GenerateReferralCouponError
    
    public func fromRequiresReloginError(debugMessage: String) -> GenerateReferralCouponError {
        return .requiresRelogin(debugMessage: debugMessage)
    }
    
    public func fromInvalidParametersError(debugMessage: String) -> GenerateReferralCouponError {
        return .generalError(debugMessage: "Unexpected invalid parameters \(debugMessage)")
    }
    
    public func fromGeneralError(debugMessage: String) -> GenerateReferralCouponError {
        return .generalError(debugMessage: debugMessage)
    }
    
    public func fromConnectivityError(debugMessage: String) -> GenerateReferralCouponError {
        return .connectivityError(debugMessage:debugMessage)
    }
    
    public func fromTypeSpecificError(_ statusCode: Int, _ reason: String, _ message: String) -> GenerateReferralCouponError? {
        return nil
    }
}



extension GenerateReferralCouponError: CopilotLocalizedError {
    public func errorPrefix() -> String {
        return "Generate Referral Coupon"
    }
    
    public var errorDescription: String? {
        switch self {
        case .requiresRelogin(let debugMessage):
            return requiresReloginMessage(debugMessage: debugMessage)
        case .generalError(let debugMessage):
            return generalErrorMessage(debugMessage: debugMessage)
        case .connectivityError(let debugMessage):
            return connectivityErrorMessage(debugMessage: debugMessage)
        }
    }
}
