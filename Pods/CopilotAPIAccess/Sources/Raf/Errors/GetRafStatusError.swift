//
//  GetRafStatusError.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 01/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public enum GetRafStatusError: Error {
    case requiresRelogin(debugMessage: String)
    case generalError(debugMessage: String)
    case connectivityError(debugMessage: String)
}

public class GetRafStatusErrorResolver : ErrorResolver {
    public typealias T = GetRafStatusError
    
    public func fromRequiresReloginError(debugMessage: String) -> GetRafStatusError {
        return .requiresRelogin(debugMessage: debugMessage)
    }
    
    public func fromInvalidParametersError(debugMessage: String) -> GetRafStatusError {
        return .generalError(debugMessage: "Unexpected invalid parameters \(debugMessage)")
    }
    
    public func fromGeneralError(debugMessage: String) -> GetRafStatusError {
        return .generalError(debugMessage: debugMessage)
    }
    
    public func fromConnectivityError(debugMessage: String) -> GetRafStatusError {
        return .connectivityError(debugMessage:debugMessage)
    }
    
    public func fromTypeSpecificError(_ statusCode: Int, _ reason: String, _ message: String) -> GetRafStatusError? {
        return nil
    }
}



extension GetRafStatusError: CopilotLocalizedError {
    public func errorPrefix() -> String {
        return "Get Raf Status"
    }
    
    public var errorDescription: String? {
        switch self {
        case .requiresRelogin(let debugMessage):
            return requiresReloginMessage(debugMessage: debugMessage)
        case .generalError(let debugMessage):
            return generalErrorMessage(debugMessage: debugMessage)
        case .connectivityError(let debugMessage):
            return connectivityErrorMessage(debugMessage: debugMessage)
        }
    }
}
