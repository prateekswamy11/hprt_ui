//
//  FetchRafDataRequestBuilder.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 30/07/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

class FetchRafDataRequestBuilder: RequestBuilder<RafData, FetchRafDataError> {
    
    typealias Dependencies = HasRafServiceInteraction
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        super.init()
    }
    
    override func build() -> RequestExecuter<RafData, FetchRafDataError> {
        return FetchRafDataRequestExecuter(dependencies: dependencies)
    }
}
