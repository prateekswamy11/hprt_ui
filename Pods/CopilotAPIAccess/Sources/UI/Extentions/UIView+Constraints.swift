//
//  UIView+Constraints.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 01/09/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

typealias Constraint = (_ child: UIView, _ parent: UIView) -> NSLayoutConstraint

internal extension UIView {
    
    func addConstraintsToSizeToParent(spacing: CGFloat = 0) {
        guard let view = superview else { fatalError() }
        let top = topAnchor.constraint(equalTo: view.topAnchor)
        let bottom = bottomAnchor.constraint(equalTo: view.bottomAnchor)
        let left = leftAnchor.constraint(equalTo: view.leftAnchor)
        let right = rightAnchor.constraint(equalTo: view.rightAnchor)
        view.addConstraints([top,bottom,left,right])
        if spacing != 0 {
            top.constant = spacing
            left.constant = spacing
            right.constant = -spacing
            bottom.constant = -spacing
        }
    }
    
}
